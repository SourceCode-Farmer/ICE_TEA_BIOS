/** @file
  ACPI uPEP Support for PCIe RP Endpoint Type checking

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

  External (WIST, MethodObj)
  External (WWST, MethodObj)

  External (BCCX)
  External (SCCX)
  External (PIXX)
  External (ISGX, MethodObj)
  External (PNVM, MethodObj)
  External (PAHC, MethodObj)
  External (PRES, MethodObj)

  //
  // Check if EP(End Point) is LAN
  // Arguments: (0)
  // Return:
  //     0->EP is not LAN; 1->EP is LAN
  //
  Method (ISLN, Zero, Serialized) // Check if PCIe LAN device
  {
    If (LEqual (BCCX, 0x02)){ // Check Sub Class Code and Base Class Code
      If (LEqual (SCCX, 0x00)){
        If (LEqual (PIXX, 0x00)){
          Return (0x01)
        }
      }
    }
    Return (0x00)
  }

  //
  // Check if EP(End Point) is DTBT RP
  // Arguments: (0)
  // Return:
  //     0->EP is not DTBT RP; 1->EP is DTBT RP
  //
  Method (ISDT, Zero, Serialized) // Check if PCH PCIe DTBT RP
  {
    If (CondRefOf (\DTFS)) {
      If (LAnd (LEqual (\DTFS, 0x01), LOr ( LAnd (LEqual (\RPS0, SLOT), \RPN0), LAnd (LEqual (\RPS1, SLOT), \RPN1)))) {
        ADBG ( Concatenate( "DTBT PEP Constraint is successfully SET for PCH RP = ", ToDecimalString (SLOT)))
        Return (0x01)
      }
    }
    Return (0x00)
  }

  //
  // Get EP(End Point) device type
  // Arguments: (0)
  // Return:
  //         0->Other
  //         1->Storage
  //         2->LAN -  PCH
  //         3->WLAN - PCH
  //         4->GFX -  CPU, PCH
  //         5->DTBT - PCH
  //         6->WWAN - PCH
  //         99->Invalid (EP absent)
  //
  Method (GRPT, Zero, Serialized) {
    If (CondRefOf (^^PRMV)) {
      If (LEqual (^^PRMV, 1)) {
        Return (99);
      }
    }

    If (PRES ()) {
      If (LOr (PAHC (), PNVM ())) {
        Return (1);
      }

      If (ISGX ()) {
        Return (4);
      }

      If (LEqual (PRTP, PCIE_RP_TYPE_PCH)) { // Currently Only Following devices are supported by PCH PCIE RP
        If (ISLN ()) {
          Return (2);
        }

        If (CondRefOf (WIST)) {
          If (WIST ()) {
            Return (3);
          }
        }

        If (ISDT ()) {
          Return (5);
        }

        If (CondRefOf (WWST)) {
          If (WWST ()) {
            Return (6);
          }
        }
      }

      Return(0);
    }

    Return(99);
  }