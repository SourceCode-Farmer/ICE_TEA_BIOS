/**@file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef __USBTYPEC__H__
#define __USBTYPEC__H__

#define SPLIT_SUPPORTED                         1
#define SPLIT_NOT_SUPPORTED                     0
#define SPLIT_MODE_SUPPORT_MASK                 0x01
#define SPLIT_MODE_SUPPORT_BIT_OFFSET           0x0
#define USB_TYPEC_PCH                           0
#define USB_TYPEC_TBT                           1
#define USB_TYPEC_CPU                           2
#define USB_TYPEA_CPU                           3
#define USB_TYPEC_PORT_TYPE_MASK                0x03
#define USB_TYPEC_PORT_TYPE_BIT_OFFSET          0x1
#define TCSS_TYPEC_ROOT_PORT_INDEX_MASK         0x1F
#define TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET   0x3

#define TCSS_PLATFORM_POR                       0x00
#define TCSS_FORCE_ENABLE                       0x01
#define TCSS_FORCE_DISABLE                      0x02

#define TCSS_UCXX_DRIVER_DISABLE                0x00
#define TCSS_UCSI_DRIVER_ENABLE                 0x01
#define TCSS_UCMX_DRIVER_ENABLE                 0x02

#endif
