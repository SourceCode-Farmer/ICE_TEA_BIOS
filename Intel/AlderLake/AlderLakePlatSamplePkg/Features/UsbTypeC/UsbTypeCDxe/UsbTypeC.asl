/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  ACPI DSDT table for USB Type C

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <UsbTypeC.h>

DefinitionBlock (
  "UsbC.aml",
  "SSDT",
  2,
  "Intel_",
  "UsbCTabl",
  0x1000
  )
{
//[-start-200506-IB17800057-add]//
#if FeaturePcdGet (PcdUseCrbEcFlag)
//[-end-200506-IB17800057-add]//
  External(\_SB.PC00.LPCB.H_EC.MGI0, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI1, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI2, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI3, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI4, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI5, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI6, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI7, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI8, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGI9, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGIA, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGIB, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGIC, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGID, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGIE, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGIF, IntObj)

  External(\_SB.PC00.LPCB.H_EC.CTL0, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL1, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL2, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL3, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL4, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL5, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL6, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CTL7, IntObj)

  External(\_SB.PC00.LPCB.H_EC.MGO0, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO1, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO2, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO3, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO4, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO5, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO6, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO7, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO8, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGO9, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGOA, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGOB, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGOC, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGOD, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGOE, IntObj)
  External(\_SB.PC00.LPCB.H_EC.MGOF, IntObj)

  External(\_SB.PC00.LPCB.H_EC.CCI0, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CCI1, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CCI2, IntObj)
  External(\_SB.PC00.LPCB.H_EC.CCI3, IntObj)

  External(\_SB.PC00.LPCB.H_EC.ECMD, MethodObj)
  External(\_SB.PC00.LPCB.H_EC.ECRD, MethodObj)  // EC Read Method
  External(\_SB.PC00.LPCB.H_EC.ECWT, MethodObj)  // EC Write Method
//[-start-200506-IB17800057-add]//
#endif
//[-end-200506-IB17800057-add]//

  External(UBCB)
  External(XDCE)
  External(USTC)
  External(UCMS)
  External(TTUP)
  External(TP1T)
  External(TP1P)
  External(TP1D)
  External(TP2T)
  External(TP2P)
  External(TP2D)
  External(TP3T)
  External(TP3P)
  External(TP3D)
  External(TP4T)
  External(TP4P)
  External(TP4D)
  External(TP5T)
  External(TP5P)
  External(TP5D)
  External(TP6T)
  External(TP6P)
  External(TP6D)
  External(TP7T)
  External(TP7P)
  External(TP7D)
  External(TP8T)
  External(TP8P)
  External(TP8D)
  External(TP9T)
  External(TP9P)
  External(TP9D)
  External(TPAT)
  External(TPAP)
  External(TPAD)
  External(TP1U)
  External(TP2U)
  External(TP3U)
  External(TP4U)
  External(TP5U)
  External(TP6U)
  External(TP7U)
  External(TP8U)
  External(TP9U)
  External(TPAU)
  External(UDRS)
  External(P8XH, MethodObj)
  External(\_SB.PC00.XHCI.RHUB, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.TPLD, MethodObj)

//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
  External(\_SB.PC00.LPCB.EC0.HGCT)
  External(\_SB.PC00.LPCB.EC0.CHKS, MethodObj)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

  Scope (\_SB)
  {
    Device(UBTC)  // USB type C device
    {
      Name (_HID, EISAID("USBC000"))
      Name (_CID, EISAID("PNP0CA0"))
      Name (_UID, 0)
      Name (_DDN, "USB Type C")

      Name (CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x00000000, 0x1000, USBR)
      })
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
      OperationRegion(PRT0,SystemIO,0x80,2)
      Field(PRT0,WordAcc,Lock,Preserve)
      {
        P80B, 16
      }

      //
      //Add delay time when driver called for USCI yellow bang issue.
      //
      Method(_PS0, 0, Serialized)
      {
        Sleep(1000)
        Sleep(100)
      }
      Method(_PS3, 0, Serialized)
      {
        Sleep(1000)
        Sleep(100)
      }

      OperationRegion (ECMS, SystemIO, 0x72, 0x02)  //EXT. CMOS
      Field (ECMS, ByteAcc, Lock, Preserve)
      {
        INDX, 8,
        DATA, 8
      }
    
      Method(RECM, 1, Serialized)
      {
        Store (Arg0, INDX)
        Return (DATA)
      }
      Method(WECM, 2, Serialized)
      {
        Store (Arg0, INDX)
        Store (Arg1, DATA)
      }
    
      Method(ECMF, 1, Serialized)
      {
        Store (RECM(0x67), INDX)
        Store (Arg0, DATA)
        //Add(RECM(0x67),1, Local0)
        WECM(0X67,(Add(RECM(0x67),1)))
      }
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

      Method(_CRS,0, Serialized)
      {
        CreateDWordField (CRS, \_SB.UBTC.USBR._BAS, CBAS)
        Store (UBCB, CBAS)
        Return(CRS)
      }

      Method(_STA,0)
      {
        If (LEqual(USTC,1)) //Check if USB Type C is supported
        {
          If (LEqual(UCMS,1)) //Check if UCSI is supported
          {
            Return(0x000F)
          }
        }
        Return(0x0000)
      }

      //
      // Return USB Type C Connector (RUCC ) Properties
      // Arg0 : Connector number
      // Arg1 : 1 - UPC, 2 - PLD
      // Uses Name Space Object TURP to know the Current Root Port
      // TPXD [3:7] Root port , [1:2] Port type PCH/TBT/CPU [0] Split mode Support
      //
      Method (RUCC, 2, Serialized) {
        If (LAnd (LLessEqual (Arg0, 10), LGreaterEqual (Arg0, 1))) {
          If(LEqual(Arg1, 1)) {
            Return (\_SB.UBTC.TUPC (1, FTPT (Arg0)))
          } Else {
            Return (\_SB.UBTC.TPLD (1, FPMN (Arg0)))
          }
        } Else {
          If(LEqual(Arg1, 1)) {
            Return (\_SB.UBTC.TUPC (0, 0))
          } Else {
            Return (\_SB.UBTC.TPLD (0, 0))
          }
        }
      }

      //
      // Find TCSS Port Type From Type-C Connector number (FTPT)
      // Arg0 : Connector number
      //
      // TPXD [3:7] Root port , [1:2] Port type PCH/TBT/CPU [0] Split mode Support
      //
      Method (FTPT, 1, Serialized)
      {
        Switch (ToInteger (Arg0)) // UCM Connector
        {
          Case (1)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP1D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (2)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP2D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (3)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP3D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (4)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP4D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (5)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP5D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (6)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP6D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (7)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP7D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (8)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP8D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (9)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TP9D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Case (10)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU) and Port Type.
            //
            ShiftRight (TPAD, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
          }
          Default
          {
            Store (0xFF, Local0)
          }
        }
        Switch (ToInteger (Local0)) // Get Port Type
        {
          Case (USB_TYPEC_PCH)
          {
            Return (9) // Port Type is Type-C
          }
          Case (USB_TYPEC_TBT)
          {
            Return (9) // Port Type is Type-C
          }
          Case (USB_TYPEC_CPU)
          {
            Return (9) // Port Type is Type-C
          }
          Case (USB_TYPEA_CPU)
          {
            Return (0) // Port Type is Type-A
          }
        }
        Return (9) // Defatul TCSS Port Type is Type-C
      }

      //
      // Find Port Mapping Group number (FPMN)
      // Arg0 : Connector number
      // Uses Name Space Object TURP to know the Current Root Port
      // TPXD [3:7] Root port , [1:2] Port type PCH/TBT/CPU [0] Split mode Support
      //
      Method (FPMN, 1, Serialized)
      {
        Switch (ToInteger (Arg0)) // UCM Connector
        {
          Case (1)
          {
            //
            // Bit 1:2 will give the Controller from which this is exposed (PCH/TBT/CPU)
            //
            ShiftRight (TP1D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            // Bit 0 is for Split mode support status
            And (TP1D, SPLIT_MODE_SUPPORT_MASK, Local1)
            // Port mapping on PCH controller if Split mode support
            Store (TP1P, Local2)
            // Port mapping on Respective Controller
            Store (TP1T, Local3)
          }
          Case (2)
          {
            ShiftRight (TP2D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP2D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP2P, Local2)
            Store (TP2T, Local3)
          }
          Case (3)
          {
            ShiftRight (TP3D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP3D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP3P, Local2)
            Store (TP3T, Local3)
          }
          Case (4)
          {
            ShiftRight (TP4D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP4D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP4P, Local2)
            Store (TP4T, Local3)
          }
          Case (5)
          {
            ShiftRight (TP5D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP5D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP5P, Local2)
            Store (TP5T, Local3)
          }
          Case (6)
          {
            ShiftRight (TP6D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP6D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP6P, Local2)
            Store (TP6T, Local3)
          }
          Case (7)
          {
            ShiftRight (TP7D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP7D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP7P, Local2)
            Store (TP7T, Local3)
          }
          Case (8)
          {
            ShiftRight (TP8D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP8D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP8P, Local2)
            Store (TP8T, Local3)
          }
          Case (9)
          {
            ShiftRight (TP9D, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TP9D, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TP9P, Local2)
            Store (TP9T, Local3)
          }
          Case (10)
          {
            ShiftRight (TPAD, USB_TYPEC_PORT_TYPE_BIT_OFFSET, Local0)
            And (Local0, USB_TYPEC_PORT_TYPE_MASK, Local0)
            And (TPAD, SPLIT_MODE_SUPPORT_MASK, Local1)
            Store (TPAP, Local2)
            Store (TPAT, Local3)
          }
          Default
          {
            Store (0xFF, Local0)
            Store (0, Local1)
            Store (0, Local2)
            Store (0, Local3)
          }
        }

        If (LEqual (Local0, USB_TYPEC_PCH)) { // PCH Type C
          Return (Local2)
        } ElseIf (Lor (LEqual (Local0, USB_TYPEC_TBT), LOr (LEqual (Local0, USB_TYPEC_CPU), LEqual (Local0, USB_TYPEA_CPU)))) { // TBT Type-C or CPU Type C/A
          If (LEqual (Local1, SPLIT_SUPPORTED)) { // Split mode supported
            Return (Local2) // Return PCH XHCI Port number
          } Else {
            Return (Local3) // Return PCH/TBT/CPU XHCI Port number
          }
        } Else  {
          Return (0)
        }
      }

      //
      // Method for creating Type C _PLD buffers
      // _PLD contains lots of data, but for purpose of WHLK validation we care only about
      // ports visibility and pairing (this requires group position)
      // so these are the only 2 configurable parameters
      //
      Method (TPLD, 2, Serialized) { // For the port related with USB Type C.
        // Arg0:  Visible
        // Arg1:  Group position
        Name (PCKG, Package () { Buffer (0x10) {} } )
        CreateField (DerefOf (Index (PCKG, 0)), 0, 7, REV)
//[-start-210830-BAIN000032-modify]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
        Store (2, REV)  //ACPI SPEC The current Revision is 0x2
#else
        Store (1, REV)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-modify]//
        CreateField (DerefOf (Index (PCKG, 0)), 64, 1, VISI)
        Store (Arg0, VISI)
        CreateField (DerefOf (Index (PCKG, 0)), 87, 8, GPOS)
        Store (Arg1, GPOS)
        // For USB type C, Standerd values
        CreateField (DerefOf (Index (PCKG, 0)), 74, 4, SHAP)  // Shape set to Oval
//[-start-210830-BAIN000032-modify]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
        Store(7, SHAP)  //Shape  Bit0-Round Bit1-Oval Bit2-Square
#else
        Store(1, SHAP)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-modify]//
        CreateField (DerefOf (Index (PCKG, 0)), 32, 16, WID)  // Width of the connector, 8.34mm
        Store (8, WID)
        CreateField (DerefOf (Index (PCKG, 0)), 48, 16, HGT)  // Height of the connector, 2.56mm
        Store (3, HGT)
        return (PCKG)
      }

      //
      // Method for creating Type C _UPC buffers
      // _UPC contains lots of data, but for purpose of WHLK validation we care only about
      // ports Connectable and Port Type.
      // so these are the only 2 configurable parameters
      //
      Method (TUPC, 2, Serialized) { // For the port related with USB Type C. copied and modified from GUPC
        // Arg0: Connectable
        // Non-Zero value: Connectable

        // Arg1: Type
        // 0x00: Type A connector
        // 0x01: Mini-AB connector
        // 0x02: ExpressCard
        // 0x03: USB 3 Standard-A connector
        // 0x04: USB 3 Standard-B connector
        // 0x05: USB 3 Micro-B connector
        // 0x06: USB 3 Micro-AB connector
        // 0x07: USB 3 Power-B connector
        // 0x08: Type C connector - USB2-only
        // 0x09: Type C connector - USB2 and SS with Switch
        // 0x0A: Type C connector - USB2 and SS without Switch
        // 0x0B - 0xFE: Reserved
        // 0xFF: Proprietary connector
        Name (PCKG, Package (4) { 1, 0x00, 0, 0 } )
        Store (Arg0, Index (PCKG, 0))
        Store (Arg1, Index (PCKG, 1))
        return (PCKG)
      }

      //
      // Method to check if the Port is Type-C Port or Not.
      // As Only Type-C Port will support UCSI Driver Support
      //
      Method (ITCP, 1, Serialized) {
        // Arg0: Type-C Connector No
        // Non-Zero value: Connectable

        Switch (ToInteger (FTPT (Arg0))) // UCM Connector
        {
          // 0x08: Type C connector - USB2-only
          // 0x09: Type C connector - USB2 and SS with Switch
          // 0x0A: Type C connector - USB2 and SS without Switch
          Case (Package () {0x08, 0x09, 0x0A})
          {
            Return (1)
          }
          Default // Non Type-C Port
          {
            Return (0)
          }
        }
      }

      //
      // Expose connector based on Total supported Connectors
      //
      If (LAnd (LGreaterEqual (TTUP,1), LAnd (LEqual (TP1U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (1), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR01) { // USB Type C Connector
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//

// _PLD _UPC Type C descript follow CRB setting 
// If WHLK test fail can check _PLD _UPC these standard descriptions

//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT)||defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-210911-QINGLIN0064-modify]//
          Name (_ADR, 0x2) //SS01 TCSSXHCI.asl
#else
          Name (_ADR, 0x0)
#endif
//[-end-211013-Ching000012-modify]//
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//
          Method (_PLD) {
//[-start-210911-QINGLIN0064-modify]//
//[-start-211111-QINGLIN0113-modify]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT)
//[-end-211111-QINGLIN0113-modify]//
            Return (TPLD(1,2)) //Group Position 2
#else
            Return (RUCC (1, 2))
#endif
//[-end-210911-QINGLIN0064-modify]//
          }
          Method (_UPC) {
//[-start-210911-QINGLIN0064-modify]//
//[-start-211111-QINGLIN0113-modify]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT)
//[-end-211111-QINGLIN0113-modify]//
            Return (TUPC(255, 10)) //Typc C
#else
            Return (RUCC (1, 1))
#endif
//[-end-210911-QINGLIN0064-modify]//
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,2), LAnd (LEqual (TP2U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (2), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR02) { // USB Type C Connector
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-211111-QINGLIN0113-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) 
//[-end-211111-QINGLIN0113-modify]//
          Name (_ADR, 0x03) //SS02 TCSSXHCI.asl
#else
          Name (_ADR, 0x0)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//
          Method (_PLD) {
//[-start-211111-QINGLIN0113-modify]//
#if defined(S570_SUPPORT)
            Return (TPLD(1,3)) //Group Position 3
#else
            Return (RUCC (2, 2))
#endif
//[-end-211111-QINGLIN0113-modify]//
          }
          Method (_UPC) {
//[-start-211111-QINGLIN0113-modify]//
#if defined(S570_SUPPORT)
            Return (TUPC(255, 10)) //Typc C
#else
            Return (RUCC (2, 1))
#endif
//[-end-211111-QINGLIN0113-modify]//
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,3), LAnd (LEqual (TP3U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (3), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR03) { // USB Type C Connector
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
          Name (_ADR, 0x04) //SS03 TCSSXHCI.asl
#else
          Name (_ADR, 0x0)
#endif
//[-end-211115-Ching000016-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//
          Method (_PLD) {
            Return (RUCC (3, 2))
          }
          Method (_UPC) {
            Return (RUCC (3, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,4), LAnd (LEqual (TP4U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (4), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR04) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (4, 2))
          }
          Method (_UPC) {
            Return (RUCC (4, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,5), LAnd (LEqual (TP5U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (5), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR05) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (5, 2))
          }
          Method (_UPC) {
            Return (RUCC (5, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,6), LAnd (LEqual (TP6U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (6), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR06) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (6, 2))
          }
          Method (_UPC) {
            Return (RUCC (6, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,7), LAnd (LEqual (TP7U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (7), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR07) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (7, 2))
          }
          Method (_UPC) {
            Return (RUCC (7, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,8), LAnd (LEqual (TP8U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (8), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR08) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (8, 2))
          }
          Method (_UPC) {
            Return (RUCC (8, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,9), LAnd (LEqual (TP9U, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (9), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR09) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (9, 2))
          }
          Method (_UPC) {
            Return (RUCC (9, 1))
          }
        }
      }
      If (LAnd (LGreaterEqual (TTUP,10), LAnd (LEqual (TPAU, TCSS_UCSI_DRIVER_ENABLE), LEqual (ITCP (10), 1)))) { // If UCxx Driver Support not force Disable
        Device (CR0A) { // USB Type C Connector
          Name (_ADR, 0x0)
          Method (_PLD) {
            Return (RUCC (10, 2))
          }
          Method (_UPC) {
            Return (RUCC (10, 1))
          }
        }
      }

//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
      OperationRegion(E9FF, SystemMemory, 0xFE0B0980, 0x10)
      Field(E9FF, AnyAcc, Lock, Preserve)
      {
        Offset(0x09),
        EC89,8,     // 0x989
      }
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

      OperationRegion (USBC, SystemMemory, UBCB, 0x38)  // 56 bytes Opm Buffer
      Field(USBC,ByteAcc,Lock,Preserve)
      {
        VER1, 8,
        VER2, 8,
        RSV1, 8,
        RSV2, 8,
        Offset(4),
        CCI0, 8,  //  PPM->OPM CCI indicator
        CCI1, 8,
        CCI2, 8,
        CCI3, 8,

        CTL0, 8,  //  OPM->PPM Control message
        CTL1, 8,
        CTL2, 8,
        CTL3, 8,
        CTL4, 8,
        CTL5, 8,
        CTL6, 8,
        CTL7, 8,

        // USB Type C Mailbox Interface
        MGI0, 8,  //  PPM->OPM Message In
        MGI1, 8,
        MGI2, 8,
        MGI3, 8,
        MGI4, 8,
        MGI5, 8,
        MGI6, 8,
        MGI7, 8,
        MGI8, 8,
        MGI9, 8,
        MGIA, 8,
        MGIB, 8,
        MGIC, 8,
        MGID, 8,
        MGIE, 8,
        MGIF, 8,

        MGO0, 8,  //  OPM->PPM Message Out
        MGO1, 8,
        MGO2, 8,
        MGO3, 8,
        MGO4, 8,
        MGO5, 8,
        MGO6, 8,
        MGO7, 8,
        MGO8, 8,
        MGO9, 8,
        MGOA, 8,
        MGOB, 8,
        MGOC, 8,
        MGOD, 8,
        MGOE, 8,
        MGOF, 8,

      }  // end of Field

//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
       OperationRegion(ECSM, SystemMemory, 0xFE0B0460, 0x30)
       Field(ECSM, AnyAcc, Lock, Preserve)
//    OperationRegion(ECRG, EmbeddedControl, 0, 0x100)
//    Field(ECRG, ByteAcc, NoLock, Preserve) 
       {
//         Offset( 0x60 ),
         HMPR,8,     // 60 : SMB_PRTCL (protocol register)
         Offset(0x01),
             ,7,     // 61 : bit 6-0 - Status
         HMDN,1,     //      bit 7   - Done
         HADD,8,     // 62 : SMBus Address
         HCMD,8,     // 63 : SMBus Command
         HD00,8,     // 64 : SMBus Data1
         HD01,8,     // 65 : SMBus Data2
         HD02,8,     // 66 : SMBus Data3
         HD03,8,     // 67 : SMBus Data4
         HD04,8,     // 68 : SMBus Data5
         HD05,8,     // 69 : SMBus Data6
         HD06,8,     // 6A : SMBus Data7
         HD07,8,     // 6B : SMBus Data8
         HD08,8,     // 6C : SMBus Data9
         HD09,8,     // 6D : SMBus Data10
         HD0A,8,     // 6E : SMBus Data11
         HD0B,8,     // 6F : SMBus Data12
         HD0C,8,     // 70 : SMBus Data13
         HD0D,8,     // 71 : SMBus Data14
         HD0E,8,     // 72 : SMBus Data15
         HD0F,8,     // 73 : SMBus Data16
         Offset(0x24),
         HCNT,8,     // 84 : SMBus Count
       }

       Mutex (UBSY, 0x00)
       Method (ECWR, 0, Serialized)
       {
         If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.HGCT, 0xA000), 0x0)) {
           Acquire (UBSY, 0xFFFF)
           \_SB.PC00.LPCB.EC0.CHKS()    // Wait SMBus Task clear
           Store (0x02, HADD) // SMB_ADDR  (EC: 02h)
           Sleep (1)
           Store (0x06, HCMD) // SMB_CMD   (Write MESSAGE OUT of UCSI)
           Sleep (1)
           Store (MGO0, HD00) // SMB_DATA[0]
           Sleep (1)
           Store (MGO1, HD01) // SMB_DATA[1]
           Sleep (1)
           Store (MGO2, HD02) // SMB_DATA[2]
           Sleep (1)
           Store (MGO3, HD03) // SMB_DATA[3]
           Sleep (1)
           Store (MGO4, HD04) // SMB_DATA[4]
           Sleep (1)
           Store (MGO5, HD05) // SMB_DATA[5]
           Sleep (1)
           Store (MGO6, HD06) // SMB_DATA[6]
           Sleep (1)
           Store (MGO7, HD07) // SMB_DATA[7]
           Sleep (1)
           Store (MGO8, HD08) // SMB_DATA[8]
           Sleep (1)
           Store (MGO9, HD09) // SMB_DATA[9]
           Sleep (1)
           Store (MGOA, HD0A) // SMB_DATA[10]
           Sleep (1)
           Store (MGOB, HD0B) // SMB_DATA[11]
           Sleep (1)
           Store (MGOC, HD0C) // SMB_DATA[12]
           Sleep (1)
           Store (MGOD, HD0D) // SMB_DATA[13]
           Sleep (1)
           Store (MGOE, HD0E) // SMB_DATA[14]
           Sleep (1)
           Store (MGOF, HD0F) // SMB_DATA[15]
           Sleep (1)
           Store (0x10, HCNT) // SMB_BCNT  (16 bytes)
           Sleep (1)

           Store (0x0A, HMPR)
//           Store (0x08, \_SB.PCI0.LPCB.EC0.SMPR) // SMB_PRTCL (Write Block)
           Store (0x08, EC89)

           \_SB.PC00.LPCB.EC0.CHKS()             // Wait SMBus Task clear

           Store (0x02, HADD) // SMB_ADDR  (EC: 02h)
           Sleep (1)
           Store (0x04, HCMD) // SMB_CMD   (Write CONTROL of UCSI)
           Sleep (1)
           Store (CTL0, HD00) // SMB_DATA[0]
           Sleep (1)
//           ECMF(CTL0)
           Store (CTL1, HD01) // SMB_DATA[1]
           Sleep (1)
           Store (CTL2, HD02) // SMB_DATA[2]
           Sleep (1)
           Store (CTL3, HD03) // SMB_DATA[3]
           Sleep (1)
           Store (CTL4, HD04) // SMB_DATA[4]
           Sleep (1)
           Store (CTL5, HD05) // SMB_DATA[5]
           Sleep (1)
           Store (CTL6, HD06) // SMB_DATA[6]
           Sleep (1)
           Store (CTL7, HD07) // SMB_DATA[7]
           Sleep (1)
           Store (0x08, HCNT) // SMB_BCNT  (8 bytes)
           Sleep (1)
 
           Store (0x0A, HMPR)
//           Store (0x08, \_SB.PCI0.LPCB.EC0.SMPR) // SMB_PRTCL (Write Block)
           Store (0x08, EC89)
 
           Sleep (1)
           \_SB.PC00.LPCB.EC0.CHKS()
           Release (UBSY)
           Release(\_SB.PC00.LPCB.EC0.HGCT)
         }
       }

       Method (ECRD, 0, Serialized)
       {
         Store(0x4E, P80B)
         If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.HGCT, 0xA000), 0x0)) {
           Acquire (UBSY, 0xFFFF)
           \_SB.PC00.LPCB.EC0.CHKS()             // Wait SMBus Task clear
           Store (0x02, HADD) // SMB_ADDR  (EC: 02h)
           Sleep (1)
           Store (0x05, HCMD) // SMB_CMD   (Write MESSAGE OUT of UCSI)
           Sleep (1)
           Store (0x10, HCNT) // SMB_BCNT  (8 bytes)
           Sleep (1)
           Store (0x0B, HMPR)
//           Store (0x07, \_SB.PCI0.LPCB.EC0.SMPR) // SMB_PRTCL (Read Block)
           Store (0x07, EC89)
           P8XH(0,0xED)       //debug code
           Sleep (1)
           \_SB.PC00.LPCB.EC0.CHKS()             // Wait SMBus Task clear
           Store (HD00, MGI0) // SMB_DATA[0]
           Sleep (1)
           Store (HD01, MGI1) // SMB_DATA[1]
           Sleep (1)
           Store (HD02, MGI2) // SMB_DATA[2]
           Sleep (1)
           Store (HD03, MGI3) // SMB_DATA[3]
           Sleep (1)
           Store (HD04, MGI4) // SMB_DATA[4]
           Sleep (1)
           Store (HD05, MGI5) // SMB_DATA[5]
           Sleep (1)
           Store (HD06, MGI6) // SMB_DATA[6]
           Sleep (1)
           Store (HD07, MGI7) // SMB_DATA[7]
           Sleep (1)
           Store (HD08, MGI8) // SMB_DATA[8]
           Sleep (1)
           Store (HD09, MGI9) // SMB_DATA[9]
           Sleep (1)
           Store (HD0A, MGIA) // SMB_DATA[10]
           Sleep (1)
           Store (HD0B, MGIB) // SMB_DATA[11]
           Sleep (1)
           Store (HD0C, MGIC) // SMB_DATA[12]
           Sleep (1)
           Store (HD0D, MGID) // SMB_DATA[13]
           Sleep (1)
           Store (HD0E, MGIE) // SMB_DATA[14]
           Sleep (1)
           Store (HD0F, MGIF) // SMB_DATA[15]
           Sleep (1)
 
           \_SB.PC00.LPCB.EC0.CHKS()             // Wait SMBus Task clear
           Store (0x02, HADD) // SMB_ADDR  (EC: 02h)
           Sleep (1)
           Store (0x03, HCMD) // SMB_CMD   (Write MESSAGE OUT of UCSI)
           Sleep (1)
           Store (0x04, HCNT) // SMB_BCNT  (8 bytes)
           Sleep (1)
           Store (0x0B, HMPR)
//           Store (0x07, \_SB.PCI0.LPCB.EC0.SMPR) // SMB_PRTCL (Read Block)
           Store (0x07, EC89)

           P8XH(0,0xEC)       //debug code

           Sleep (1)
           \_SB.PC00.LPCB.EC0.CHKS()             // Wait SMBus Task clear
           Store (HD00, CCI0) // SMB_DATA[0]
           Sleep (1)
           Store (HD01, CCI1) // SMB_DATA[1]
           Sleep (1)
           Store (HD02, CCI2) // SMB_DATA[2]
           Sleep (1)
           Store (HD03, CCI3) // SMB_DATA[3]
           Sleep (1)
           \_SB.PC00.LPCB.EC0.CHKS()             // Wait SMBus Task clear
           Release (UBSY)
           Release(\_SB.PC00.LPCB.EC0.HGCT)
//[-start-220128-JEPLIUT214-modify]// 
#if defined(S570_SUPPORT)
           P8XH(0,0xEF)       //debug code , Add flag 
#endif
//[-end-220128-JEPLIUT214-modify]// 
           
          }
       }
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

      Method (_DSM, 4, Serialized, 0, UnknownObj, {BuffObj, IntObj, IntObj, PkgObj} )
      {
        // Compare passed in UUID with supported UUID.
        If (LEqual(Arg0, ToUUID ("6f8398c2-7ca4-11e4-ad36-631042b5008f")))  // UUID for USB type C
        {
          Switch (ToInteger(Arg2))  // Arg2:  0 for query, 1 for write and 2 for read
          {
            Case (0)
            {
              Return (Buffer() {0x1F}) // 4 functions defined other than Query.
            }

            Case (1)  // OPM write to EC
            {
//[-start-200506-IB17800057-add]//
#if FeaturePcdGet (PcdUseCrbEcFlag)    
//[-end-200506-IB17800057-add]//        
              \_SB.PC00.LPCB.H_EC.ECWT( MGO0, RefOf(  \_SB.PC00.LPCB.H_EC.MGO0 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO1, RefOf(  \_SB.PC00.LPCB.H_EC.MGO1 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO2, RefOf(  \_SB.PC00.LPCB.H_EC.MGO2 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO3, RefOf(  \_SB.PC00.LPCB.H_EC.MGO3 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO4, RefOf(  \_SB.PC00.LPCB.H_EC.MGO4 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO5, RefOf(  \_SB.PC00.LPCB.H_EC.MGO5 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO6, RefOf(  \_SB.PC00.LPCB.H_EC.MGO6 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO7, RefOf(  \_SB.PC00.LPCB.H_EC.MGO7 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO8, RefOf(  \_SB.PC00.LPCB.H_EC.MGO8 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGO9, RefOf(  \_SB.PC00.LPCB.H_EC.MGO9 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGOA, RefOf(  \_SB.PC00.LPCB.H_EC.MGOA ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGOB, RefOf(  \_SB.PC00.LPCB.H_EC.MGOB ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGOC, RefOf(  \_SB.PC00.LPCB.H_EC.MGOC ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGOD, RefOf(  \_SB.PC00.LPCB.H_EC.MGOD ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGOE, RefOf(  \_SB.PC00.LPCB.H_EC.MGOE ) )
              \_SB.PC00.LPCB.H_EC.ECWT( MGOF, RefOf(  \_SB.PC00.LPCB.H_EC.MGOF ) )

              \_SB.PC00.LPCB.H_EC.ECWT( CTL0, RefOf(  \_SB.PC00.LPCB.H_EC.CTL0 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL1, RefOf(  \_SB.PC00.LPCB.H_EC.CTL1 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL2, RefOf(  \_SB.PC00.LPCB.H_EC.CTL2 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL3, RefOf(  \_SB.PC00.LPCB.H_EC.CTL3 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL4, RefOf(  \_SB.PC00.LPCB.H_EC.CTL4 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL5, RefOf(  \_SB.PC00.LPCB.H_EC.CTL5 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL6, RefOf(  \_SB.PC00.LPCB.H_EC.CTL6 ) )
              \_SB.PC00.LPCB.H_EC.ECWT( CTL7, RefOf(  \_SB.PC00.LPCB.H_EC.CTL7 ) )

              \_SB.PC00.LPCB.H_EC.ECMD (0xE0)
#endif
//[-start-210804-IB18410075-modify]//
#if FeaturePcdGet (PcdUseCrbEcFlag) == 0           
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
              ECWR ()
              Sleep(10)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

              P8XH(0,0xE0)
#endif
//[-end-210804-IB18410075-modify]// 
            }

            Case (2)  // OPM read from EC
            {
//[-start-200506-IB17800057-add]//
#if FeaturePcdGet (PcdUseCrbEcFlag)            
//[-end-200506-IB17800057-add]//
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI0) ), MGI0 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI1) ), MGI1 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI2) ), MGI2 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI3) ), MGI3 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI4) ), MGI4 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI5) ), MGI5 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI6) ), MGI6 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI7) ), MGI7 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI8) ), MGI8 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGI9) ), MGI9 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGIA) ), MGIA )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGIB) ), MGIB )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGIC) ), MGIC )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGID) ), MGID )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGIE) ), MGIE )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.MGIF) ), MGIF )

              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.CCI0) ), CCI0 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.CCI1) ), CCI1 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.CCI2) ), CCI2 )
              Store(  \_SB.PC00.LPCB.H_EC.ECRD( RefOf (\_SB.PC00.LPCB.H_EC.CCI3) ), CCI3 )
//[-start-200506-IB17800057-add]//
#endif              
//[-end-200506-IB17800057-add]//
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
            ECRD ()
            Sleep(10)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//
            }

            Case (3) //xDCI FN EN/DIS Status
            {
              Return (XDCE) // 0: Disable 1: Enable
            }

            Case (4) //USBC DR_SWAP Platform support
            {
              Return (UDRS) // 0: Enable DR_SWAP 1: Platform disabled DR_SWAP
            }
          }  // End switch
        }   // End UUID check
        Return (Buffer() {0})
      }  // End _DSM Method
    }  // end of Device
  } // end \_SB scope
} // end SSDT