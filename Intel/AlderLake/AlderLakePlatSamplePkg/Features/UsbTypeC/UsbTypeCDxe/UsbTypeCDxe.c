/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  Implements UsbTypeC Dxe driver and publish UsbTypeC Acpi table

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <UsbTypeCDxe.h>

#include <Protocol/AcpiTable.h>

#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/EcMiscLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/PcdLib.h>
#include <Protocol/PlatformNvsArea.h>
#include <SetupVariable.h>
//[-start-181025-IB15590141-add]//
#include <Library/H2OCpLib.h>
#include <Guid/H2OCpChipset.h>
//[-end-181025-IB15590141-add]//

GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA_PROTOCOL    *PlatformNvsAreaProtocol;
GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA             *mPlatformNvsAreaPtr;
//
// Setup variables
//
GLOBAL_REMOVE_IF_UNREFERENCED SETUP_DATA                     mSystemConfiguration;

#define USBTYPEC_DATA_VAR         L"UsbTypeC"
EFI_GUID UsbTypeCVarGuid      =  { 0xfc876842, 0xd8f0, 0x4844, {0xae, 0x32, 0x1f, 0xf8, 0x43, 0x79, 0x7b, 0x17} };

/**
  Allocate MemoryType below 4G memory address.

  @param[in] Size           Size of memory to allocate.
  @param[in] Buffer         Allocated address for output.

  @retval EFI_SUCCESS       Memory successfully allocated.
  @retval Other             Other errors occur.
**/
static EFI_STATUS
AllocateMemoryBelow4G (
  IN   EFI_MEMORY_TYPE MemoryType,
  IN   UINTN           Size,
  OUT  VOID           **Buffer
  )
{
  UINTN                 Pages;
  EFI_PHYSICAL_ADDRESS  Address;
  EFI_STATUS            Status;

  Pages = EFI_SIZE_TO_PAGES (Size);
  Address = 0xffffffff;

  Status  = (gBS->AllocatePages) (
                   AllocateMaxAddress,
                   MemoryType,
                   Pages,
                   &Address
                   );

  *Buffer = (VOID *) (UINTN) Address;

  return Status;
};

static UINT16 GetUcsiRev(VOID)
{
//[-start-181025-IB15590141-modify]//
  UINT16                UcsiRev;

  UcsiRev = 0;

  if (FeaturePcdGet (PcdH2ODxeCpEcGetSendGetUsciVerSupported)) {
    H2O_DXE_CP_EC_SEND_GET_USCI_VER_DATA       DxeGetUcsiRevData;
    EFI_STATUS                                 Status;
    
    DxeGetUcsiRevData.Size    = sizeof (H2O_DXE_CP_EC_SEND_GET_USCI_VER_DATA);
    DxeGetUcsiRevData.Status  = H2O_CP_TASK_NORMAL;
    DxeGetUcsiRevData.UsciVer = 0;

    DEBUG ((DEBUG_INFO, "Checkpoint Trigger: %g\n", &gH2ODxeCpEcGetSendGetUsciVerGuid));
    Status = H2OCpTrigger (&gH2ODxeCpEcGetSendGetUsciVerGuid, &DxeGetUcsiRevData);
    DEBUG ((DEBUG_INFO, "Checkpoint Result: %r\n", DxeGetUcsiRevData.Status));

    if (!EFI_ERROR(Status)&& !EFI_ERROR(DxeGetUcsiRevData.Status)) {
      UcsiRev = DxeGetUcsiRevData.UsciVer;
      if (DxeGetUcsiRevData.Status == H2O_CP_TASK_SKIP) {
        goto Skip;
      }
    }
  }

  if (FeaturePcdGet(PcdUseCrbEcFlag)) {
    UINT8         Rev1, Rev2;

    SendEcCommand( 0xE1);
    ReceiveEcData( &Rev1 );
    ReceiveEcData( &Rev2 );
    return ((Rev1 << 8) + Rev2 );
  }

  Skip:
    DEBUG ((DEBUG_INFO, "Get Ucis Rev End\n"));   
    return UcsiRev;
//[-end-181025-IB15590141-modify]//
}

/**
  Initialize and publish UsbTypeC ACPI table.

  @retval   EFI_SUCCESS     The UsbTypeC ACPI table is published successfully.
  @retval   Others          The UsbTypeC ACPI table is not published.

**/
EFI_STATUS
PublishAcpiTable (
  VOID
  )
{
  EFI_STATUS                     Status;
  EFI_ACPI_TABLE_PROTOCOL        *AcpiTable;
  UINTN                          TableKey;
  EFI_ACPI_DESCRIPTION_HEADER    *Table;
  UINTN                          TableSize;

  Status = GetSectionFromFv (
             &gEfiCallerIdGuid,
             EFI_SECTION_RAW,
             0,
             (VOID **) &Table,
             &TableSize
             );
  ASSERT_EFI_ERROR (Status);

  ASSERT (Table->OemTableId == SIGNATURE_64 ('U', 's', 'b', 'C', 'T', 'a', 'b', 'l'));

  CopyMem (Table->OemId, PcdGetPtr (PcdAcpiDefaultOemId), sizeof (Table->OemId) );

  //
  // Publish UsbTypeC ACPI table
  //
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID **) &AcpiTable);
  ASSERT_EFI_ERROR (Status);

  TableKey = 0;
  Status = AcpiTable->InstallAcpiTable (
                        AcpiTable,
                        Table,
                        TableSize,
                        &TableKey
                        );
  ASSERT_EFI_ERROR (Status);

  return Status;
}

BOOLEAN
EFIAPI
IsUsbTypeCPort (
  IN UINT8         UsbTypeCPortProperties
  )
{
  UINT8      PortType;
  PortType = (UsbTypeCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK;

  switch (PortType) {
    case USB_TYPEC_PCH:
    case USB_TYPEC_TBT:
    case USB_TYPEC_CPU:
      return TRUE;
      break;
    case USB_TYPEA_CPU:
      return FALSE;
      break;
    default:
      return FALSE;
  }
}
/**
  This function is the entry point for this DXE driver.
  It creates ACPI table and publishes it.

  @param[in] ImageHandle       Image handle of this driver.
  @param[in] SystemTable       Global system service table.

  @retval EFI_DEVICE_ERROR     Initialization fails
  @retval EFI_SUCCESS          Initialization completes successfully.

**/
EFI_STATUS
EFIAPI
UsbTypeCEntryPoint (
  IN EFI_HANDLE       ImageHandle,
  IN EFI_SYSTEM_TABLE *SystemTable
  )
{
  EFI_STATUS         Status;
  UINTN              DataSize;
  USBTYPEC_OPM_BUF   *OpmBuffer;
  UINT8              TcssUcxxDriverType;
  UINT8              *TcssPortUcxxDriverSupport;

  DEBUG ((DEBUG_INFO, "UsbTypeC entrypoint.\n"));

  //
  // Publish Acpi tables if Intel ec is present.
  //
//[-start-210830-BAIN000032-modify]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
  Status = PublishAcpiTable ();
  ASSERT_EFI_ERROR (Status);
#else
  if (PcdGetBool (PcdEcPresent) == TRUE) {
    Status = PublishAcpiTable ();
    ASSERT_EFI_ERROR (Status);
  }
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-modify]//

  if (!PcdGetBool (PcdUsbTypeCSupport)) {
    DEBUG ((DEBUG_WARN, "UsbTypeC currently only supported on ICL U, Y.\n"));
    return EFI_UNSUPPORTED;
  }
  //
  // Locate setup variable
  //
  DataSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  PLATFORM_SETUP_VARIABLE_NAME,
                  &gSetupVariableGuid,
                  NULL,
                  &DataSize,
                  &mSystemConfiguration
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gBS->LocateProtocol (&gPlatformNvsAreaProtocolGuid, NULL, (VOID **) &PlatformNvsAreaProtocol);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  TcssUcxxDriverType        = mSystemConfiguration.TcssUcmDevice;
  TcssPortUcxxDriverSupport = &mSystemConfiguration.TestDisableTcssPortUcxxSupport[0];

  mPlatformNvsAreaPtr = PlatformNvsAreaProtocol->Area;
  //
  // Allocate memory in ACPI NVS
  //
  Status = AllocateMemoryBelow4G (EfiACPIMemoryNVS, 0x1000, (VOID **) &OpmBuffer);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  SetMem (OpmBuffer, sizeof (USBTYPEC_OPM_BUF), 0x0);
  if (TcssUcxxDriverType == 0x01){
    OpmBuffer->Version =  GetUcsiRev();
  }

  mPlatformNvsAreaPtr->UsbTypeCOpBaseAddr = (UINT32) (UINTN) OpmBuffer;
  DEBUG ((DEBUG_INFO, "UsbTypeC EntryPoint: mPlatformNvsAreaPtr->UsbTypeCOpBaseAddr = 0x%X\n", mPlatformNvsAreaPtr->UsbTypeCOpBaseAddr));

  Status = gRT->SetVariable (
    USBTYPEC_DATA_VAR,
    &UsbTypeCVarGuid,
    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
    sizeof (UINT32),
    &(mPlatformNvsAreaPtr->UsbTypeCOpBaseAddr)
  );

  // Total Type C ports supported
//[-start-210830-BAIN000032-modify]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-211013-Ching000012-modify]//
#if defined(C970_SUPPORT) 
  mPlatformNvsAreaPtr->TotalTypeCPorts      = 3;
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#elif defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
  mPlatformNvsAreaPtr->TotalTypeCPorts      = 2;
//[-start-210911-QINGLIN0064-add]//
#elif defined(S370_SUPPORT)
  mPlatformNvsAreaPtr->TotalTypeCPorts      = 1;
//[-end-210911-QINGLIN0064-add]//
#else
  mPlatformNvsAreaPtr->TotalTypeCPorts      = PcdGet8 (PcdTypeCPortsSupported);
#endif
//[-end-211013-Ching000012-modify]//
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-modify]//
  // Port 1  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort1        = PcdGet8 (PcdUsbTypeCPort1);
  mPlatformNvsAreaPtr->UsbTypeCPort1Pch     = PcdGet8 (PcdUsbTypeCPort1Pch);
  mPlatformNvsAreaPtr->UsbCPort1Proterties  = PcdGet8 (PcdUsbCPort1Properties);
  mPlatformNvsAreaPtr->UsbCPort1UcxxSupport = ((TcssPortUcxxDriverSupport[0] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort1Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 2  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort2        = PcdGet8 (PcdUsbTypeCPort2);
  mPlatformNvsAreaPtr->UsbTypeCPort2Pch     = PcdGet8 (PcdUsbTypeCPort2Pch);
  mPlatformNvsAreaPtr->UsbCPort2Proterties  = PcdGet8 (PcdUsbCPort2Properties);
  mPlatformNvsAreaPtr->UsbCPort2UcxxSupport = ((TcssPortUcxxDriverSupport[1] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort2Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 3  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort3        = PcdGet8 (PcdUsbTypeCPort3);
  mPlatformNvsAreaPtr->UsbTypeCPort3Pch     = PcdGet8 (PcdUsbTypeCPort3Pch);
  mPlatformNvsAreaPtr->UsbCPort3Proterties  = PcdGet8 (PcdUsbCPort3Properties);
  mPlatformNvsAreaPtr->UsbCPort3UcxxSupport = ((TcssPortUcxxDriverSupport[2] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort3Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 4  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort4        = PcdGet8 (PcdUsbTypeCPort4);
  mPlatformNvsAreaPtr->UsbTypeCPort4Pch     = PcdGet8 (PcdUsbTypeCPort4Pch);
  mPlatformNvsAreaPtr->UsbCPort4Proterties  = PcdGet8 (PcdUsbCPort4Properties);
  mPlatformNvsAreaPtr->UsbCPort4UcxxSupport = ((TcssPortUcxxDriverSupport[3] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort4Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 5  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort5        = PcdGet8 (PcdUsbTypeCPort5);
  mPlatformNvsAreaPtr->UsbTypeCPort5Pch     = PcdGet8 (PcdUsbTypeCPort5Pch);
  mPlatformNvsAreaPtr->UsbCPort5Proterties  = PcdGet8 (PcdUsbCPort5Properties);
  mPlatformNvsAreaPtr->UsbCPort5UcxxSupport = ((TcssPortUcxxDriverSupport[4] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort5Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 6  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort6        = PcdGet8 (PcdUsbTypeCPort6);
  mPlatformNvsAreaPtr->UsbTypeCPort6Pch     = PcdGet8 (PcdUsbTypeCPort6Pch);
  mPlatformNvsAreaPtr->UsbCPort6Proterties  = PcdGet8 (PcdUsbCPort6Properties);
  mPlatformNvsAreaPtr->UsbCPort6UcxxSupport = ((TcssPortUcxxDriverSupport[5] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort6Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 7  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort7        = PcdGet8 (PcdUsbTypeCPort7);
  mPlatformNvsAreaPtr->UsbTypeCPort7Pch     = PcdGet8 (PcdUsbTypeCPort7Pch);
  mPlatformNvsAreaPtr->UsbCPort7Proterties  = PcdGet8 (PcdUsbCPort7Properties);
  mPlatformNvsAreaPtr->UsbCPort7UcxxSupport = ((TcssPortUcxxDriverSupport[6] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort7Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 8  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort8        = PcdGet8 (PcdUsbTypeCPort8);
  mPlatformNvsAreaPtr->UsbTypeCPort8Pch     = PcdGet8 (PcdUsbTypeCPort8Pch);
  mPlatformNvsAreaPtr->UsbCPort8Proterties  = PcdGet8 (PcdUsbCPort8Properties);
  mPlatformNvsAreaPtr->UsbCPort8UcxxSupport = ((TcssPortUcxxDriverSupport[7] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort8Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 9  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPort9        = PcdGet8 (PcdUsbTypeCPort9);
  mPlatformNvsAreaPtr->UsbTypeCPort9Pch     = PcdGet8 (PcdUsbTypeCPort9Pch);
  mPlatformNvsAreaPtr->UsbCPort9Proterties  = PcdGet8 (PcdUsbCPort9Properties);
  mPlatformNvsAreaPtr->UsbCPort9UcxxSupport = ((TcssPortUcxxDriverSupport[8] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPort9Properties)))) ?\
                                              0 : TcssUcxxDriverType;
  // Port 10  mapping and properties
  mPlatformNvsAreaPtr->UsbTypeCPortA        = PcdGet8 (PcdUsbTypeCPortA);
  mPlatformNvsAreaPtr->UsbTypeCPortAPch     = PcdGet8 (PcdUsbTypeCPortAPch);
  mPlatformNvsAreaPtr->UsbCPortAProterties  = PcdGet8 (PcdUsbCPortAProperties);
  mPlatformNvsAreaPtr->UsbCPortAUcxxSupport = ((TcssPortUcxxDriverSupport[9] == TCSS_FORCE_DISABLE) \
                                              || (!IsUsbTypeCPort (PcdGet8 (PcdUsbCPortAProperties)))) ?\
                                              0 : TcssUcxxDriverType;

  return Status;
}