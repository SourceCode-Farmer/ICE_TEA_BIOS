/** @file
  TME DXE driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/TmeInfoLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/TpmMeasurementLib.h>
#include <Uefi/UefiSpec.h>
#include <IndustryStandard/UefiTcgPlatform.h>

// define mktme_tpm strings
#define FEATURE_ENABLE_TME_STRING     "FeatureTME=1"
#define FEATURE_DISABLE_TME_STRING    "FeatureTME=0"

/**
  Update UEFI memory map to add EFI_MEMORY_CPU_CRYPTO attributes for all system memory.

  @retval EFI_SUCCESS       Successfully update system memory map.
  @retval others            Failed to get DS memory map

**/
EFI_STATUS
UpdateMemoryMap (
  VOID
  )
{
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR *MemoryMap;
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR *MemoryDescriptor;
  UINTN                           NumberOfDescriptors;
  UINTN                           Index;
  UINT64                          Attributes;
  UINT64                          Capabilities;
  EFI_STATUS                      Status;

  MemoryMap = NULL;
  NumberOfDescriptors = 0;
  Status = gDS->GetMemorySpaceMap (&NumberOfDescriptors, &MemoryMap);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to get GCD memory map space\n"));
    return Status;
  }

  for (Index = 0; Index < NumberOfDescriptors; Index++) {
    MemoryDescriptor = &MemoryMap[Index];
    if (MemoryDescriptor->GcdMemoryType == EfiGcdMemoryTypeSystemMemory) {
      Capabilities = MemoryDescriptor->Capabilities | EFI_MEMORY_CPU_CRYPTO;
      Status = gDS->SetMemorySpaceCapabilities (
        MemoryDescriptor->BaseAddress,
        MemoryDescriptor->Length,
        Capabilities
      );
      DEBUG ((DEBUG_ERROR,
        "Set Range %016lx-%016lx Cap %016lx Status = %r\n",
        MemoryDescriptor->BaseAddress,
        MemoryDescriptor->BaseAddress + MemoryDescriptor->Length - 1,
        Capabilities,
        Status
        ));

      Attributes = MemoryDescriptor->Attributes | EFI_MEMORY_CPU_CRYPTO;
      Status = gDS->SetMemorySpaceAttributes (
        MemoryDescriptor->BaseAddress,
        MemoryDescriptor->Length,
        Attributes
      );
      DEBUG ((DEBUG_ERROR,
        "Set Range %016lx-%016lx Attr %016lx Status = %r\n",
        MemoryDescriptor->BaseAddress,
        MemoryDescriptor->BaseAddress + MemoryDescriptor->Length - 1,
        Attributes,
        Status
        ));
    }
  }

  return Status;
}

/**
  Measure and log data for TME feature

  @param[in] Event    Event whose notification function is being invoked.
  @param[in] Context  Pointer to the notification function's context.
**/
VOID
EFIAPI
MeasurementTmeState (
  IN  EFI_EVENT     Event,
  IN  VOID          *Context
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  VOID       *String;
  UINT32     StringLen;
  UINT32     PcrIndex = 1;

  if (IsTmeActivated()) {
    String = FEATURE_ENABLE_TME_STRING;
    StringLen = sizeof (FEATURE_ENABLE_TME_STRING) - 1;
    DEBUG ((DEBUG_INFO, "Measure Tme Enabled. \n"));
  } else {
    String = FEATURE_DISABLE_TME_STRING;
    StringLen = sizeof (FEATURE_DISABLE_TME_STRING) - 1;
    DEBUG ((DEBUG_INFO, "Measure Tme Disabled. \n"));
  }

  Status = TpmMeasureAndLogData (
    PcrIndex,
    EV_PLATFORM_CONFIG_FLAGS,
    String,
    StringLen,
    String,
    StringLen
    );

  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "MeasurementTmeState() return error: %r\n", Status));
  }

  return;
}

/**
  Tme Init DXE Module Entry Point.

  @param[in] ImageHandle          Image handle of this driver.
  @param[in] SystemTable          Global system service table.

  @retval EFI_SUCCESS             Initialization complete.
**/
EFI_STATUS
EFIAPI
TmeInitDxeEntryPoint (
  IN EFI_HANDLE            ImageHandle,
  IN EFI_SYSTEM_TABLE      *SystemTable
  )
{
  EFI_STATUS    Status;
  EFI_EVENT     Event;

  Status = EFI_SUCCESS;

  ///
  /// Check for TME status
  ///
  if (IsTmeActivated() == TRUE) {
    ///
    /// Update Memory type to EFI_MEMORY_CPU_CRYPTO
    /// when TME is enabled.
    ///
    Status = UpdateMemoryMap();
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "UpdateMemoryMap() return error: %r\n", Status));
      return Status;
    }
  }

  /// Register a callback only if Tpm is supported
  if (FeaturePcdGet (PcdTpm2Enable) == TRUE) {
    ///
    /// Register callback to measure TME state
    ///
    Status = EfiCreateEventReadyToBootEx (
               TPL_CALLBACK,
               MeasurementTmeState,
               NULL,
               &Event
               );
    ASSERT_EFI_ERROR (Status);
  }

  return Status;
}