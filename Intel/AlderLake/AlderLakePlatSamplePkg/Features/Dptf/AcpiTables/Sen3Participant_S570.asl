/** @file
  Intel ACPI Reference Code for Intel(R) Dynamic Tuning Technology

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

Scope(\_SB.PC00.LPCB.EC0)
{

  Device(SEN3)
  {
//[-start-210818-YUNLEI0121-modify]//
    Name(_HID, "INTC1046")  // Intel DPTF Temperature Sensor Device
//[-end-210818-YUNLEI0121-modify]//
    Name(_UID, "CPUT")
    Name(_STR, Unicode ("CPU DTS")) // CPU DTS Temperature
    Name(PTYP, 0x03)
    Name(CTYP,0)  // Mode
    // PFLG
    //   0 - Default, participant is on the main board
    //   1 - Participant device is on the docking station
    //   2 - Participant device is on the detachable base
    //   3 - Participant device is an external device (such as a USB device, Intel(R) Dynamic Tuning Technology does not support this type of device today)
    Name(PFLG, 0)

    // _STA (Status)
    //
    // This object returns the current status of a device.
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   An Integer containing a device status bitmap:
    //    Bit 0 - Set if the device is present.
    //    Bit 1 - Set if the device is enabled and decoding its resources.
    //    Bit 2 - Set if the device should be shown in the UI.
    //    Bit 3 - Set if the device is functioning properly (cleared if device failed its diagnostics).
    //    Bit 4 - Set if the battery is present.
    //    Bits 5-31 - Reserved (must be cleared).
    //
    Method(_STA)
    {
      //If (LEqual(\S3DE,1)) {
      //  Return(0x0F)
      //} Else {
      //  Return(0x00)
      //}
      Return(0x0F)
    }

    // _TMP (Temperature)
    //
    // This control method returns the thermal zone's current operating temperature.
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   An Integer containing the current temperature of the thermal zone (in tenths of degrees Kelvin)
    //
    Method(_TMP,0,Serialized)
    {
//      If(\_SB.PC00.LPCB.H_EC.ECAV)
//      {
//        Return(\_SB.IETM.C10K(\_SB.PC00.LPCB.H_EC.ECRD(RefOf(\_SB.PC00.LPCB.H_EC.TSR3))))
//      } Else {
//        Return(3000)
//[-start-220415-BAIN000101-modify]//
       If(\_SB.PC00.LPCB.EC0.ECAV){
        If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.LfcM, 0xA000),0x0)){
          Store(\_SB.IETM.C10K(Multiply(\_SB.PC00.LPCB.EC0.CPUT, 10)),Local0)
          Release(\_SB.PC00.LPCB.EC0.LfcM)
          return(Local0)
        }
      }else{
        Return(3000) // EC not available follow RC Return default value 3000
      }
//[-end-220415-BAIN000101-modify]//
    }

    // Number of Aux Trips available
    Name(PATC, 2)

    // PATx (Participant Programmable Auxiliary Trip) - Sets Aux Trip Point
    //
    // The PATx objects shall take a single integer parameter, in tenths of degree Kelvin, which
    // represents the temperature at which the device should notify the participant driver of
    // an auxiliary trip event. A PATx control method returns no value.
    //
    //  Arguments: (1)
    //    Arg0 - temperature in tenths of degree Kelvin
    //  Return Value:
    //    None
    //
    Method(PAT0, 1, Serialized)
    {
//      If (\_SB.PC00.LPCB.H_EC.ECAV)
//      {
//        Store (Acquire(\_SB.PC00.LPCB.H_EC.PATM, 100),Local0)  // Save Acquire result so we can check for Mutex acquired
//        If (LEqual(Local0, Zero))  // Check for Mutex acquired
//        {
//          Store(\_SB.IETM.K10C(Arg0),Local1)
//          \_SB.PC00.LPCB.H_EC.ECWT(0x2, RefOf(\_SB.PC00.LPCB.H_EC.TSI)) // Select Thermal Sensor
//          \_SB.PC00.LPCB.H_EC.ECWT(0x2, RefOf(\_SB.PC00.LPCB.H_EC.HYST)) // Thermal Sensor Hysteresis, 2 degrees
//          \_SB.PC00.LPCB.H_EC.ECWT(Local1, RefOf(\_SB.PC00.LPCB.H_EC.TSLT)) // Sensor Low Trip Point
//          \_SB.PC00.LPCB.H_EC.ECMD(0x4A)// Set Trip point.
//          Release(\_SB.PC00.LPCB.H_EC.PATM)
//        }
//      }
    }

    // PATx (Participant Programmable Auxiliary Trip) - Sets Aux Trip Point
    //
    // The PATx objects shall take a single integer parameter, in tenths of degree Kelvin, which
    // represents the temperature at which the device should notify the participant driver of
    // an auxiliary trip event. A PATx control method returns no value.
    //
    //  Arguments: (1)
    //    Arg0 - temperature in tenths of degree Kelvin
    //  Return Value:
    //    None
    //
    Method(PAT1, 1, Serialized)
    {
//      If (\_SB.PC00.LPCB.H_EC.ECAV)
//      {
//        Store (Acquire(\_SB.PC00.LPCB.H_EC.PATM, 100),Local0)  // Save Acquire result so we can check for Mutex acquired
//        If (LEqual(Local0, Zero))  // Check for Mutex acquired
//        {
//          Store(\_SB.IETM.K10C(Arg0),Local1)
//          \_SB.PC00.LPCB.H_EC.ECWT(0x2, RefOf(\_SB.PC00.LPCB.H_EC.TSI)) // Select Thermal Sensor
//          \_SB.PC00.LPCB.H_EC.ECWT(0x2, RefOf(\_SB.PC00.LPCB.H_EC.HYST)) // Thermal Sensor Hysteresis, 2 degrees
//          \_SB.PC00.LPCB.H_EC.ECWT(Local1, RefOf(\_SB.PC00.LPCB.H_EC.TSHT)) // Sensor High Trip Point
//          \_SB.PC00.LPCB.H_EC.ECMD(0x4A) // Set Trip point.
//          Release(\_SB.PC00.LPCB.H_EC.PATM)
//        }
//      }
    }

    // Thermal Sensor Hysteresis, 2 degrees
    Name(GTSH, 20)

//    Name(LSTM,0)  // Last temperature reported

    // _DTI (Device Temperature Indication)
    //
    // Conveys the temperature of a device's internal temperature sensor to the platform when a temperature trip point
    // is crossed or when a meaningful temperature change occurs.
    //
    // Arguments: (1)
    //   Arg0 - An Integer containing the current value of the temperature sensor (in tenths Kelvin)
    // Return Value:
    //   None
    //
//    Method(_DTI, 1)
//    {
//      Store(Arg0,LSTM)
//      Notify(\_SB.PC00.LPCB.H_EC.SEN3, 0x91) // notify the participant of a trip point change event
//    }

    // _NTT (Notification Temperature Threshold)
    //
    // Returns the temperature change threshold for devices containing native temperature sensors to cause
    // evaluation of the _DTI object
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   An Integer containing the temperature threshold in tenths of degrees Kelvin.
    //
//    Method(_NTT, 0)
//    {
//      Return(2782)  // 5 degree Celcius, this could be a platform policy with setup item
//    }

   // Default values for trip points for the validation purpose.
//    Name (S3AC,60)  // Active0 trip point in celsius
//    Name (S3A1,50)  // Active1 trip point in celsius
//    Name (S3A2,40)  // Active2 trip point in celsius
//    Name (S3PV,65)  // Passive trip point in celsius
//    Name (S3CC,80)  // Critical trip point in celsius
//    Name (S3C3,70)  // Critical s3 trip point in celsius
//    Name (S3HP,75)  // Hot trip point in celsius
//    Name (SSP3,0)   // Thermal Sampling Period

    // _TSP (Thermal Sampling Period)
    //
    // Sets the polling interval in 10ths of seconds. A value of 0 tells the driver to use interrupts.
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   An Integer containing the polling rate in tenths of seconds.
    //   A value of 0 will specify using interrupts through the ACPI notifications.
    //
    //   The granularity of the sampling period is 0.1 seconds. For example, if the sampling period is 30.0
    //   seconds, then _TSP needs to report 300; if the sampling period is 0.5 seconds, then it will report 5.
    //
    Method(_TSP,0,Serialized)
    {
      //Return(\SSP3)
      Return(0)
    }

    // _ACx (Active Cooling)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    An Integer containing the active cooling temperature threshold in tenths of degrees Kelvin
    //
//    Method(_AC3,0,Serialized)
//    {
//      Store(\_SB.IETM.CTOK(S3AC),Local1) // Active Cooling Policy
//      If(LGreaterEqual(LSTM,Local1))
//      {
//        Return(Subtract(Local1,20)) // subtract 2 degrees which is the Hysteresis
//      }
//      Else
//      {
//        Return(Local1)
//      }
//    }

    // _ACx (Active Cooling)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    An Integer containing the active cooling temperature threshold in tenths of degrees Kelvin
    //
//    Method(_AC4,0,Serialized)
//    {
//      Return(\_SB.IETM.CTOK(S3A1))
//    }

    // _ACx (Active Cooling)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    An Integer containing the active cooling temperature threshold in tenths of degrees Kelvin
    //
//    Method(_AC5,0,Serialized)
//    {
//      Return(\_SB.IETM.CTOK(S3A2))
//    }

    // _PSV (Passive)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    An Integer containing the passive cooling temperature threshold in tenths of degrees Kelvin
    //
//    Method(_PSV,0,Serialized)
//    {
//      Return(\_SB.IETM.CTOK(S3PV)) // Passive Cooling Policy
//    }

    // _CRT (Critical Temperature)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    An Integer containing the critical temperature threshold in tenths of degrees Kelvin
    //
//    Method(_CRT,0,Serialized)
//    {
//      Return(\_SB.IETM.CTOK(S3CC))
//    }

    // _CR3 (Critical Temperature for S3/CS)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    An Integer containing the critical temperature threshold in tenths of degrees Kelvin
    //
//    Method(_CR3,0,Serialized)
//    {
//      Return(\_SB.IETM.CTOK(S3C3))
//    }

    // _HOT (Hot Temperature)
    //
    // This ACPI method is optional.
    // OEM/ODM can directly set the value on Intel(R) Dynamic Tuning Technology UI for the participant and then export the value to BIOS GDDV for storage.
    // The value programmed here is the default value used on Intel RVP for the validation purpose.
    // OEM/ODM can set a different default value for a specific platform if necessary.
    //
    //  Arguments: (0)
    //    None
    //  Return Value:
    //    The return value is an integer that represents the critical sleep threshold tenths of degrees Kelvin.
//    //
//    Method(_HOT,0,Serialized)
//    {
//      Return(\_SB.IETM.CTOK(S3HP) )
//    }

  } // End SEN3 Device
}// end Scope(\_SB.PC00.LPCB.EC0)
