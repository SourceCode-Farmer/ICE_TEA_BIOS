/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  Intel ACPI Reference Code for Intel(R) Dynamic Tuning Technology
  Used for driver debugging and validation.
@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

External(\_SB.PC00.LPCB.H_EC.CHRG, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.DGPU, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.SEN2, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.SEN3, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.SEN4, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.SEN5, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.TFN1, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.TFN2, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.TFN3, DeviceObj)
External(\_SB.TPWR, DeviceObj)

Scope(\_SB.IETM)
{
  // TEVT (Trigger EvenT)
  // Used for Intel(R) Dynamic Tuning Technology driver debugging and validation
  // This function will generate an event of the event code as given in arg1 for the participant given in parameter arg0.
  // Arguments: (2)
  //   Arg0 - The device object name string.
  //   Arg1 - An Integer containing the notification code to be used in Notify.
  // Return Value:
  //   None
  //
  Method(TEVT,2,Serialized,,,{StrObj,IntObj})
  {
    Switch(ToInteger(Arg0))
    {
      case("IETM") {
        Notify(\_SB.IETM, Arg1) // Intel Extended Thermal Manager (Intel(R) Dynamic Tuning Technology Manager)
      }

      case("TCPU") {
        Notify(\_SB.PC00.TCPU, Arg1) // TCPU Participant (SA Thermal Device)
      }

      case("TPCH") {
        Notify(\_SB.TPCH, Arg1) // PCH FIVR Participant
      }
    }
#if FeaturePcdGet (PcdUseCrbEcFlag)
    If (\ECON) {
      Switch(ToInteger(Arg0))
      {
        case("CHRG") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.CHRG, Arg1) // Charger Participant
#endif
        }

        case("DGPU") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.DGPU, Arg1) // Sensor Participant
#endif
        }

        case("SEN2") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.SEN2, Arg1) // Sensor Participant
#endif
        }

        case("SEN3") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.SEN3, Arg1) // Sensor Participant
#endif
        }

        case("SEN4") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.SEN4, Arg1) // Sensor Participant
#endif
        }

        case("SEN5") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.SEN5, Arg1) // Sensor Participant
#endif
        }

        case("TFN1") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.TFN1, Arg1) // Fan Participant
#endif
        }

        case("TFN2") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.TFN2, Arg1) // Fan Participant
#endif
        }

        case("TFN3") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.PC00.LPCB.H_EC.TFN3, Arg1) // Fan Participant
#endif
        }

        case("TPWR") {
#if FeaturePcdGet (PcdUseCrbEcFlag)
          Notify(\_SB.TPWR, Arg1) // Power Participant
#endif
        }
      }
    }
#endif
  }

} // End Scope(\_SB.IETM)
