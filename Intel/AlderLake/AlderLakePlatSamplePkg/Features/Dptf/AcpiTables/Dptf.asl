/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  Intel ACPI Reference Code for Intel(R) Dynamic Tuning Technology

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "CpuPowerMgmt.h"
#include <Include/AcpiDebug.h>
#include "PlatformBoardId.h"

#include "CpuRegs.h"
#include "CpuGenInfo.h"
#include "Hid.h"

DefinitionBlock (
  "Dptf.aml",
  "SSDT",
  2,
  "DptfTb",
  "DptfTabl",
  0x1000
  )
{

External(\P8XH, MethodObj)
External(\TSOD, IntObj)
External(\PNHM, IntObj)
External(\TCNT, IntObj)
External(\PWRS, IntObj)
External(\CRTT, IntObj)
External(\ACTT, IntObj)
External(\PSVT, IntObj)
External(\ATPC, IntObj)
External(\PTPC, IntObj)

External(\PLID, IntObj) // PlatformId
External(\DPTF, IntObj) // EnableDptf
External(\DCFE, IntObj) // EnableDCFG

External(\SADE, IntObj) // EnableSaDevice

External(\FND1, IntObj) // EnableFan1Device
External(\FND2, IntObj) // EnableFan2Device
External(\FND3, IntObj) // EnableFan3Device

External(\S1DE, IntObj) // EnableSen1Participant
External(\SSP1, IntObj) // SensorSamplingPeriodSen1

External(\S2DE, IntObj) // EnableSen2Participant
External(\SSP2, IntObj) // SensorSamplingPeriodSen2

External(\S3DE, IntObj) // EnableSen3Participant
External(\SSP3, IntObj) // SensorSamplingPeriodSen3

External(\S4DE, IntObj) // EnableSen4Participant
External(\SSP4, IntObj) // SensorSamplingPeriodSen4

External(\S5DE, IntObj) // EnableSen5Participant
External(\SSP5, IntObj) // SensorSamplingPeriodSen5

External(\S6DE, IntObj) // EnableDgpuParticipant
External(\S6P2, IntObj) // Thermal Sampling Period

External(\CHGE, IntObj) // EnableChargerParticipant
External(\PWRE, IntObj) // EnablePowerParticipant
External(\PPPR, IntObj) // PowerParticipantPollingRate

External(\BATR, IntObj) // EnableBatteryParticipant
External(\IN34, IntObj) // EnableInt3400Device.

External(\PPSZ, IntObj) // PPCC Step Size
External(\PF00, IntObj) // PR00 _PDC Flags

External(\ODV0, IntObj) // OemDesignVariable0
External(\ODV1, IntObj) // OemDesignVariable1
External(\ODV2, IntObj) // OemDesignVariable2
External(\ODV3, IntObj) // OemDesignVariable3
External(\ODV4, IntObj) // OemDesignVariable4
External(\ODV5, IntObj) // OemDesignVariable5
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
External(\ODV6, IntObj) // OemDesignVariable6
External(\ODV7, IntObj) // OemDesignVariable7
External(\ODV8, IntObj) // OemDesignVariable8
External(\ODV9, IntObj) // OemDesignVariable9
External(\ODVA, IntObj) // OemDesignVariable10
External(\ODVB, IntObj) // OemDesignVariable11
External(\ODVC, IntObj) // OemDesignVariable12
External(\ODVD, IntObj) // OemDesignVariable13
External(\ODVE, IntObj) // OemDesignVariable14
External(\ODVF, IntObj) // OemDesignVariable15
External(\ODVG, IntObj) // OemDesignVariable16
External(\ODVH, IntObj) // OemDesignVariable17
External(\ODVI, IntObj) // OemDesignVariable18
External(\ODVJ, IntObj) // OemDesignVariable19
External(\ODVK, IntObj) // OemDesignVariable20
//[-start-211126-JEPLIUT121-add]//
//[-start-211214-JEPLIUT207-modify]//  
#if defined(S570_SUPPORT)
External(\ODVL, IntObj) // OemDesignVariable21
External(\ODVM, IntObj) // OemDesignVariable22
#endif
//[-end-211214-JEPLIUT207-modify]// 
//[-end-211126-JEPLIUT121-add]//
#endif
//[-end-210701-KEBIN00030-modify]//
External(\_TZ.ETMD, IntObj)
External(\_TZ.TZ00, ThermalZoneObj)

External(\_SB.PC00, DeviceObj)
External(\_SB.PC00.TCPU, DeviceObj)
External(\_SB.PC00.MC.MHBR, FieldUnitObj)

#if FeaturePcdGet (PcdUseCrbEcFlag)
External(\ECON, IntObj)
External(\_SB.PC00.LPCB.H_EC, DeviceObj)
External(\_SB.PC00.LPCB.H_EC.ECAV, IntObj)
External(\_SB.PC00.LPCB.H_EC.ECMD, MethodObj)    // EC Command Method
External(\_SB.PC00.LPCB.H_EC.ECRD, MethodObj)    // EC Read Method
External(\_SB.PC00.LPCB.H_EC.ECWT, MethodObj)    // EC Write Method
External(\_SB.PC00.LPCB.H_EC.ECF2, OpRegionObj)

//
// Sensors
//
External(\_SB.PC00.LPCB.H_EC.TSR1, FieldUnitObj) // Sensor Temperature Values
External(\_SB.PC00.LPCB.H_EC.TSR2, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSR3, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSR4, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSR5, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSI,  FieldUnitObj)  // Sensor Select
External(\_SB.PC00.LPCB.H_EC.HYST, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSHT, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSLT, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.TSSR, FieldUnitObj)

External(\_SB.PC00.LPCB.H_EC.PPSL, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PPSH, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PINV, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PENV, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PSTP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.CMDR, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.CFSP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.DFSP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.GFSP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.CPUP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PMAX, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PLMX, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PECH, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.CFAN, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.B1RC, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.B1FC, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.BAT1._BST, MethodObj)
External(\_SB.PC00.LPCB.H_EC.BAT1._BIX, MethodObj)
External(\_SB.PC00.LPCB.H_EC.BMAX, FieldUnitObj)
//
// Power Boss
//
External(\_SB.PC00.LPCB.H_EC.PWRT, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PBSS, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.VMIN, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PSOC, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.AVOL, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.ACUR, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.ARTG, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.CTYP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.BICC, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.PROP, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.AP01, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.AP02, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.AP10, FieldUnitObj)

External(\_SB.PC00.LPCB.H_EC.PPWR, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.CHGR, FieldUnitObj)
External(\_SB.PC00.LPCB.H_EC.FCHG, FieldUnitObj)
#endif

External(\_SB.CPPC, FieldUnitObj)
External(\_SB.PR00, ProcessorObj)
External(\_SB.PR00._PSS, MethodObj)
External(\_SB.PR00.TPSS, PkgObj)
External(\_SB.PR00.LPSS, PkgObj)
External(\_SB.PR00._PPC, MethodObj)
External(\_SB.PR00._TSS, MethodObj)
External(\_SB.CFGD, FieldUnitObj)
External(\_SB.PR00.TSMF, PkgObj)
External(\_SB.PR00.TSMC, PkgObj)
External(\_SB.PR00._PTC, MethodObj)
External(\_SB.PR00._TSD, MethodObj)
External(\_SB.PR00._TPC, IntObj)
External(\_SB.PR01, ProcessorObj)
External(\_SB.PR02, ProcessorObj)
External(\_SB.PR03, ProcessorObj)
External(\_SB.PR04, ProcessorObj)
External(\_SB.PR05, ProcessorObj)
External(\_SB.PR06, ProcessorObj)
External(\_SB.PR07, ProcessorObj)
External(\_SB.PR08, ProcessorObj)
External(\_SB.PR09, ProcessorObj)
External(\_SB.PR10, ProcessorObj)
External(\_SB.PR11, ProcessorObj)
External(\_SB.PR12, ProcessorObj)
External(\_SB.PR13, ProcessorObj)
External(\_SB.PR14, ProcessorObj)
External(\_SB.PR15, ProcessorObj)
External(\_SB.PR16, ProcessorObj)
External(\_SB.PR17, ProcessorObj)
External(\_SB.PR18, ProcessorObj)
External(\_SB.PR19, ProcessorObj)
External(\_SB.PR20, ProcessorObj)
External(\_SB.PR21, ProcessorObj)
External(\_SB.PR22, ProcessorObj)
External(\_SB.PR23, ProcessorObj)
External(\_SB.PR24, ProcessorObj)
External(\_SB.PR25, ProcessorObj)
External(\_SB.PR26, ProcessorObj)
External(\_SB.PR27, ProcessorObj)
External(\_SB.PR28, ProcessorObj)
External(\_SB.PR29, ProcessorObj)
External(\_SB.PR30, ProcessorObj)
External(\_SB.PR31, ProcessorObj)

External(\_SB.CLVL, FieldUnitObj)
External(\_SB.CBMI, FieldUnitObj)
External(\_SB.PL10, FieldUnitObj)
External(\_SB.PL20, FieldUnitObj)
External(\_SB.PLW0, FieldUnitObj)
External(\_SB.CTC0, FieldUnitObj)
External(\_SB.TAR0, FieldUnitObj)
External(\_SB.PL11, FieldUnitObj)
External(\_SB.PL21, FieldUnitObj)
External(\_SB.PLW1, FieldUnitObj)
External(\_SB.CTC1, FieldUnitObj)
External(\_SB.TAR1, FieldUnitObj)
External(\_SB.PL12, FieldUnitObj)
External(\_SB.PL22, FieldUnitObj)
External(\_SB.PLW2, FieldUnitObj)
External(\_SB.CTC2, FieldUnitObj)
External(\_SB.TAR2, FieldUnitObj)
External(\_SB.APSV, FieldUnitObj)
External(\_SB.AAC0, FieldUnitObj)
External(\_SB.ACRT, FieldUnitObj)
External(\_SB.PAGD, DeviceObj)
External(\_SB.PAGD._PUR, PkgObj)
External(\_SB.PAGD._STA, MethodObj)

// Platform-Wide OS Capable externals
External(\_SB.OSCP, IntObj)

// Intel Proprietary Wake up Event support externals.
#if FeaturePcdGet (PcdUseCrbEcFlag)  
External(\_SB.HIDD.HPEM, MethodObj)
#endif
External(\_SB.SLPB, DeviceObj)
External(HIDW, MethodObj)
External(HIWC, MethodObj)
External(\PCHE, FieldUnitObj) // EnablePchFivrParticipant
//[start-210903-STORM1111-modify]
External(\UMAB, IntObj)
//[-start-210915-YUNLEI0133-modify]//
#if defined(C770_SUPPORT)
External(\MCSZ, IntObj) // Machine size, 12 = 12'. 13 = 13'
External(\PTCI, IntObj) //For use PackageTdp to distinguish CPU U15/U28   U15:15 U28:28 
//[start-220307-STORM1127-modify]
External(\MFID, IntObj) // panel Manufacture ID
External(\PAID, IntObj) // panel PID
//[end-220307-STORM1127-modify]
#endif
//[-end-210915-YUNLEI0133-modify]//
//[end-210903-STORM1111-modify]
//[-start-210929-SHAONN0010-add]//
#if defined(S370_SUPPORT)
External(\OPTY, IntObj) //Project Type  0: S170; 1:S370; 2:V141517; 3:S1415
#endif
//[-end-210929-SHAONN0010-add]//
//[-start-211126-JEPLIUT121-add]//
#if defined(S570_SUPPORT)
 External(\CPTY, IntObj)  //Cpu Type  0x37: i7 ;  0x35: i5;   0x33: i3
 External(\CIPU, IntObj)  // 1: Support IPU , 0: No support
#endif
//[-end-211126-JEPLIUT121-add]//
//#[-start-211028-Dongxu0027-add]##  Thermal Tool support
#if defined(C970_SUPPORT) 
External(DDTS)
#endif  
//#[-end-211028-Dongxu0027-add]##  
//[-start-220222-QINGLIN0161-add]//
#if defined(S370_SUPPORT)
External(\MCSZ, IntObj)
#endif
//[-end-220222-QINGLIN0161-add]//

Scope(\_SB)
{
  //
  // DPTF Thermal Zone Device
  //
  //
  Device(IETM)
  {

    // GHID (Get HID)
    //
    // This method returns the HID value of the device
    //
    // Arguments: (1)
    //   Arg0: _UID Unique ID for the Device
    // Return Value:
    //   Returns appropriate HID for the respective participant
    //   "XXXX9999": return value if _UID doesn't match existing participants
    //
    Method(GHID, 1, Serialized,,StrObj)
    {

        If (LEqual(Arg0, "IETM")) {
          Return(IETM_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "SEN1")) {
          Return(SENX_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "SEN2")) {
          Return(SENX_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "SEN3")) {
          Return(SENX_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "SEN4")) {
          Return(SENX_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "SEN5")) {
          Return(SENX_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "TPCH")) {
          Return(PCHP_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "TFN1")) {
          Return(TFN1_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "TFN2")) {
          Return(TFN1_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "TFN3")) {
          Return(TFN1_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "TPWR")) {
          Return(TPWR_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "1")) {
          Return(BAT1_PARTICIPANT_HID_ADL)
        }
        If (LEqual(Arg0, "CHRG")) {
          Return(CHRG_PARTICIPANT_HID_ADL)
        }
        Return("XXXX9999")
    }

    //
    // Intel DPTF Thermal Framework Device
    //
    Name (_UID, "IETM")
    Method(_HID)
    {
      Return(\_SB.IETM.GHID(_UID))  // Intel (R) Dynamic Tuning Technology IETM device
    }

    //-------------------------------------------
    //  Intel Proprietary Wake up Event solution
    //-------------------------------------------
    Method(_DSM, 0x4, Serialized, 0, {IntObj, BuffObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      If(CondRefOf(HIWC)) {
        If(HIWC(Arg0)) {
          If(CondRefOf(HIDW)) {
            Return (HIDW(Arg0, Arg1, Arg2, Arg3))
          }
        }
      }
      Return(Buffer(One) { 0x00 }) // Guid mismatch
    }

    // _STA (Status)
    //
    // This object returns the current status of a device.
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   An Integer containing a device status bitmap:
    //    Bit 0 - Set if the device is present.
    //    Bit 1 - Set if the device is enabled and decoding its resources.
    //    Bit 2 - Set if the device should be shown in the UI.
    //    Bit 3 - Set if the device is functioning properly (cleared if device failed its diagnostics).
    //    Bit 4 - Set if the battery is present.
    //    Bits 5-31 - Reserved (must be cleared).
    //
//#[-start-211028-Dongxu0027-add]##  Thermal Tool support
#if defined(C970_SUPPORT)     
    Method(_STA)
    {
     If(LAnd(LAnd(LEqual(\DPTF,1),LEqual(\IN34,1)),LEqual(DDTS,0))){
        Return(0x0F)
      } Else {
        Return(0x00)
      }
    }
#else
    Method(_STA)
    {
     If(LAnd(LEqual(\DPTF,1),LEqual(\IN34,1))){
        Return(0x0F)
      } Else {
        Return(0x00)
      }
    }    
#endif  
//#[-end-211028-Dongxu0027-add]##  
    //
    // Save original trip points so _OSC method can enable/disable Legacy thermal policies by manipulating trip points.
    //
    Name (PTRP,0)  // Passive trip point
    Name (PSEM,0)  // Passive semaphore
    Name (ATRP,0)  // Active trip point
    Name (ASEM,0)  // Active semaphore
    Name (YTRP,0)  // Critical trip point
    Name (YSEM,0)  // Critical semaphore

    // _OSC (Operating System Capabilities)
    //
    // This object is evaluated by each DPTF policy implementation to communicate to the platform of the existence and/or control transfer.
    //
    // Arguments: (4)
    //   Arg0 - A Buffer containing a UUID
    //   Arg1 - An Integer containing a Revision ID of the buffer format
    //   Arg2 - An Integer containing a count of entries in Arg3
    //   Arg3 - A Buffer containing a list of DWORD capabilities
    // Return Value:
    //   A Buffer containing a list of capabilities
    //
    Method(_OSC, 4,Serialized,,BuffObj,{BuffObj,IntObj,IntObj,BuffObj})
    {

      // Point to Status DWORD in the Arg3 buffer (STATUS)
      CreateDWordField(Arg3, 0, STS1)

      // Point to Caps DWORDs of the Arg3 buffer (CAPABILITIES)
      CreateDWordField(Arg3, 4, CAP1)

      //
      // _OSC needs to validate the Revision.
      //
      // IF Unsupported Revision
      //  Return Unsupported Revision _OSC Failure
      //
      //    STS0[0] = Reserved
      //    STS0[1] = _OSC Failure
      //    STS0[3] = Unsupported Revision
      //    STS0[4] = Capabilities masked
      //

      If(LNot(LEqual(Arg1, 1)))
      {
        //
        // Return Unsupported Revision _OSC Failure
        //
        And(STS1,0xFFFFFF00,STS1)
        Or(STS1,0xA,STS1)
        Return(Arg3)
      }

      If(LNot(LEqual(Arg2, 2)))
      {
        //
        // Return Argument 3 Buffer Count not sufficient
        //
        And(STS1,0xFFFFFF00,STS1)
        Or(STS1,0x2,STS1)
        Return(Arg3)
      }

      //
      // Save Auto Passive Trip Point
      //
      If(CondRefOf(\_SB.APSV)){
        If(LEqual(PSEM,0)){
          Store(1,PSEM)
          Store(\_SB.APSV,PTRP)  // use semaphore so variable is only initialized once
        }
      }
      //
      // Save Auto Active Trip Point
      //
      If(CondRefOf(\_SB.AAC0)){
        If(LEqual(ASEM,0)){
          Store(1,ASEM)
          Store(\_SB.AAC0,ATRP)  // use semaphore so variable is only initialized once
        }
      }
      //
      // Save Auto Critical Trip Point
      //
      If(CondRefOf(\_SB.ACRT)){
        If(LEqual(YSEM,0)){
          Store(1,YSEM)
          Store(\_SB.ACRT,YTRP)  // use semaphore so variable is only initialized once
        }
      }

      // CAP1 contains 4 bits. 1st bit is to indicate that Intel(R) Dynamic Tuning is enabled and wants to enabled some policy.
      // Bios can ignore that 1st bit(indicated by "x" in following table) and
      // only look at bits which are dedicated to each policy as shown in following table.
      // When the bit0 is zero the all CAP bits will be zero.
      // 001x: Enable Active Policy
      // 010x: Enable Passive Policy
      // 100x: Enable Critical Policy
      // 011x: Enable Active/Passive Policy
      // 101x: Enable Active/Critical Policy
      // 110x: Enable Passive/Critical Policy
      // 111x: Enable Active/Passive/Critical Policy
      // xxx0: Disabled all of the Intel(R) Dynamic Tuning Policies.

      //
      // Verify the Intel(R) Dynamic Tuning UUID.
      //
      If(LEqual(Arg0, ToUUID ("B23BA85D-C8B7-3542-88DE-8DE2FFCFD698"))){  // Intel(R) Dynamic Tuning GUID
        If(Not(And(STS1, 0x01))) // Test Query Flag
        { // Not a query operation, so process the request
          If(And(CAP1, 0x1)){  // Validate Intel(R) Dynamic Tuning input, When the bit0 is zero then no Intel(R) Dynamic Tuning policy is enabled.
            If(And(CAP1, 0x2)){  // Enable Active Policy. Nullify the legacy thermal zone.
              Store(110,\_SB.AAC0)
              Store(0, \_TZ.ETMD)  // Legacy Active TM Management relies on this variable.
            } Else{  // re-enable legacy thermal zone with active trip point
              Store(ATRP,\_SB.AAC0)
              Store(1, \_TZ.ETMD)
            }
            If(And(CAP1,0x4)){  // Enable Passive Policy. Nullify the legacy thermal zone.
              Store(110,\_SB.APSV)
            }Else{  // re-enable legacy thermal zone with passive trip point
              Store(PTRP,\_SB.APSV)
            }
            If(And(CAP1, 0x8)){  // Enable Critical Policy. Nullify the legacy thermal zone.
              Store(210,\_SB.ACRT)
            }Else{  // re-enable legacy thermal zone with critical trip point
              Store(YTRP,\_SB.ACRT)
            }
            // Send notification to legacy thermal zone for legacy policy to be enabled/disabled
            If(CondRefOf(\_TZ.TZ00)){
              Notify(\_TZ.TZ00, 0x81)
            }
          } Else{
            Store(YTRP,\_SB.ACRT) // re-enable legacy thermal zone with critical trip point
            Store(PTRP,\_SB.APSV) // re-enable legacy thermal zone with passive trip point
            Store(ATRP,\_SB.AAC0) // re-enable legacy thermal zone with active trip point
            Store(1, \_TZ.ETMD)
          }
          // Send notification to legacy thermal zone for legacy policy to be enabled/disabled
          If(CondRefOf(\_TZ.TZ00)){
            Notify(\_TZ.TZ00, 0x81)
          }
        }
        Return(Arg3)
      }

      Return(Arg3)
    }

    // DCFG (DPTF Configuration)
    //
    // Returns a DWORD data representing the desired behavior of DPTF besides supported DSP and participants.
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   An Integer containing the DPTF Configuration bitmap:
    //    Bit 0 = Generic UI Access Control (0 - enable as default, 1 - disable access)
    //    Bit 1 = Restricted UI Access Control ( 0 - enable as default, 1 - disable access )
    //    Bit 2 = Shell Access Control ( 0 - enable as default, 1 - disable access)
    //    Bit 3 = Environment Monitoring Report Control ( 0 - report is allowed as default, 1 - No environmental monitoring report to Microsoft )
    //    Bit 4 = Thermal Mitigation Report Control ( 0 - No mitigation report to Microsoft as default, 1 - report is allowed)
    //    Bit 5 = Thermal Policy Report Control ( 0 - No policy report to Microsoft as default, 1 - report is allowed)
    //    Bits[31:6] - Reserved (must be cleared).
    //
    Method(DCFG)
    {
      Return(\DCFE)
    }

    // ODVP (Oem Design Variables Package)
    //
    // Variables for OEM's to customize DPTF behavior based on platform changes.
    //
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT   
//[-start-211126-JEPLIUT121-modify]// 
#if defined(S570_SUPPORT)
//[-start-211214-JEPLIUT207-modify]// 
//    Name(ODVX,Package(){0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0})
    Name(ODVX,Package(){0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0})
//[-start-211214-JEPLIUT207-modify]//     
#else
    Name(ODVX,Package(){0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0})
#endif
//[-end-211126-JEPLIUT121-modify]//	
#else
    Name(ODVX,Package(){0,0,0,0,0,0})
#endif
//[-end-210701-KEBIN00030-modify]//

    // ODVP (Oem Design Variables Package)
    //
    // Variables for OEM's to customize DPTF behavior based on platform changes.
    //
    // Arguments: (0)
    //   None
    // Return Value:
    //   Package of integers
    //
    Method(ODVP,0,Serialized,,PkgObj)
    {
      Store(\ODV0,Index(ODVX,0))
      Store(\ODV1,Index(ODVX,1))
      Store(\ODV2,Index(ODVX,2))
      Store(\ODV3,Index(ODVX,3))
      Store(\ODV4,Index(ODVX,4))
      Store(\ODV5,Index(ODVX,5))
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
      Store(\ODV6,Index(ODVX,6))
      Store(\ODV7,Index(ODVX,7))
      Store(\ODV8,Index(ODVX,8))
      Store(\ODV9,Index(ODVX,9))
      Store(\ODVA,Index(ODVX,10))
      Store(\ODVB,Index(ODVX,11))
      Store(\ODVC,Index(ODVX,12))
      Store(\ODVD,Index(ODVX,13))
      Store(\ODVE,Index(ODVX,14))
      Store(\ODVF,Index(ODVX,15))
      Store(\ODVG,Index(ODVX,16))
      Store(\ODVH,Index(ODVX,17))
//[-start-210902-FLINT00017-modify]//
      Store(\ODVI,Index(ODVX,18))
      Store(\ODVJ,Index(ODVX,19))
      Store(\ODVK,Index(ODVX,20))   
//[-end-210902-FLINT00017-modify]//
//[-start-211126-JEPLIUT121-add]//
//[-start-211214-JEPLIUT207-modify]// 
#if defined(S570_SUPPORT)
      Store(\ODVL,Index(ODVX,21))
      Store(\ODVM,Index(ODVX,22)) 
#endif	  
//[-end-211214-JEPLIUT207-modify]// 
//[-end-211126-JEPLIUT121-add]//
#endif
//[-start-210701-KEBIN00030-modify]//
      Return(ODVX)
    }

  } // End IETM Device
} // End \_SB Scope

#if FixedPcdGetBool(PcdEcEnable) == 1
#if FeaturePcdGet (PcdUseCrbEcFlag)
//
// EC support code
//
Scope(\_SB.PC00.LPCB.H_EC) // Open scope to Embedded Controller
{
  //
  // Create a Mutex for PATx methods to prevent Sx resume race condition problems asscociated with EC commands.
  //
  Mutex(PATM, 0)

  // _QF1 (Query - Embedded Controller Query F1)
  //
  // Handler for EC generated SCI number F1.
  //
  // Arguments: (0)
  //   None
  // Return Value:
  //   None
  //
  Method(_QF1)
  { // Thermal sensor threshold crossing event handler
    Store(\_SB.PC00.LPCB.H_EC.ECRD(RefOf(\_SB.PC00.LPCB.H_EC.TSSR)), Local0)
    While(Local0) // Ensure that events occuring during execution
    {             // of this handler are not dropped
      \_SB.PC00.LPCB.H_EC.ECWT(0, RefOf(\_SB.PC00.LPCB.H_EC.TSSR)) // clear all status bits
      If(And(Local0, 0x10))
      { // BIT4: Sensor 5 Threshold Crossed
        Notify(\_SB.PC00.LPCB.H_EC.SEN5, 0x90)
      }
      If(And(Local0, 0x8))
      { // BIT3: Sensor 4 Threshold Crossed
        Notify(\_SB.PC00.LPCB.H_EC.SEN4, 0x90)
      }
      If(And(Local0, 0x4))
      { // BIT2: Sensor 3 Threshold Crossed
        Notify(\_SB.PC00.LPCB.H_EC.SEN3, 0x90)
      }
      If(And(Local0, 0x2))
      { // BIT1: Sensor 2 Threshold Crossed
        Notify(\_SB.PC00.LPCB.H_EC.SEN2, 0x90)
      }
      If(And(Local0, 0x1))
      { // BIT0: Sensor 1 (Dgpu) Threshold Crossed
        Notify(\_SB.PC00.LPCB.H_EC.DGPU, 0x90)
      }
      Store(\_SB.PC00.LPCB.H_EC.ECRD(RefOf(\_SB.PC00.LPCB.H_EC.TSSR)), Local0)
    }
  }

} // End \_SB.PC00.LPCB.H_EC Scope

//
// Fan participant.
//
  Include("TFN1Participant.asl")
  Include("TFN2Participant.asl")
  Include("TFN3Participant.asl")
//
// Participants using device sensors.
//
  Include("ChrgParticipant.asl")
  Include("TPwrParticipant.asl")

//
// Participants using battery.
//
  Include("BatteryParticipant.asl")

//
// Participants using motherboard sensors.
//
  Include("Sen1Participant.asl")
  Include("Sen2Participant.asl")
  Include("Sen3Participant.asl")
  Include("Sen4Participant.asl")
  Include("Sen5Participant.asl")
  Include("dGpuSensor.asl")

//
// Policy support files
//
  Include("Trt.asl")
  Include("Psvt.asl")
  Include("Art.asl")
#endif
#endif // FixedPcdGetBool(PcdEcEnable) == 1

//
// DPTF Helper functions.
//
  Include("HelperFunctions.asl")


//
// Processor participant.
//
  Include("B0d4Participant.asl")
  Include("B0d4CtdpPolicy.asl")

//[-start-210706-SHUI0001-add]//
#ifdef C970_SUPPORT
Include("Psvt.asl")
Include("Dppm.asl")
Include("Sen1Participant_C970.asl")
Include("Sen2Participant_C970.asl")
Include("Sen3Participant_C970.asl")
Include("Sen4Participant_C970.asl")
#endif
//[-end-210706-SHUI0001-add]//

//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT
Include("Psvt.asl")
Include("Dppm.asl")
Include("Sen1Participant_C770.asl")
Include("Sen2Participant_C770.asl")
Include("Sen3Participant_C770.asl")
Include("Sen4Participant_C770.asl")
//[start-210903-STORM1111-modify]
If(LNotEqual(UMAB, Zero)){    ///- Check if not UMA
  Include("Sen5Participant_C770.asl")
//[start-220113-STORM1124-modify]
  Include("Sen6Participant_C770.asl")
  Include("Sen7Participant_C770.asl")
//[end-220113-STORM1124-modify]
}
//[end-210903-STORM1111-modify]
#endif
//[end-210720-STORM1100-modify]//

//[-start-210721-QINGLIN0001-modify]//
#ifdef S570_SUPPORT
  Include("Psvt.asl")
  Include("Dppm.asl")
  Include("Sen1Participant_S570.asl")
  Include("Sen2Participant_S570.asl")
  Include("Sen3Participant_S570.asl")
  Include("Sen4Participant_S570.asl")
//[-start-211126-JEPLIUT121-add]//
    Include("Sen5Participant_S570.asl")
//[-end-211126-JEPLIUT121-add]//
#endif
//[-end-210721-QINGLIN0001-modify]//

//[-start-210803-QINGLIN0008-add]//
#ifdef S370_SUPPORT
    Include("Psvt.asl")
    Include("Dppm.asl")
    Include("Sen1Participant_S370.asl")
    Include("Sen2Participant_S370.asl")
    Include("Sen3Participant_S370.asl")
    Include("Sen4Participant_S370.asl")
//[start-211117-SHAONN0018-modify]
//If(LNotEqual(UMAB, Zero)){    ///- Check if not UMA
  Include("Sen5Participant_S370.asl")
//}
//[end-211117-SHAONN0018-modify]
#endif
//[-end-210803-QINGLIN0008-add]//

//[-start-210924-GEORGE0008-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
Include("Psvt.asl")
Include("Dppm.asl")
Include("Sen1Participant_S77014.asl")
Include("Sen2Participant_S77014.asl")
Include("Sen3Participant_S77014.asl")
Include("Sen4Participant_S77014.asl")
#endif
//[-end-210924-GEORGE0008-add]//

//[-start-211026-GEORGE0017-add]//
#ifdef S77013_SUPPORT
Include("Psvt.asl")
Include("Dppm.asl")
Include("Sen1Participant_S77013.asl")
Include("Sen2Participant_S77013.asl")
Include("Sen3Participant_S77013.asl")
Include("Sen4Participant_S77013.asl")
#endif
//[-end-211026-GEORGE0017-add]//

  //
  // PCH FIVR Participant
  //
  Include("PchpParticipant.asl")

//[-start-210706-SHUI0001-remove]//
//[-start-210721-QINGLIN0001-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210924-GEORGE0008-modify]//
//[-start-211026-GEORGE0017-modify]//
//#if !defined(C970_SUPPORT) && !defined(C770_SUPPORT) && !defined(S570_SUPPORT) && !defined(S370_SUPPORT) && !defined(S77014_SUPPORT)
#ifndef LCFC_SUPPORT
  Include("Dppm.asl")
#endif
//[-end-211026-GEORGE0017-modify]//
//[-end-210924-GEORGE0008-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-modify]//
//[-end-210706-SHUI0001-remove]//

//
// DPTF Trigger Event function.
//
  Include("DptfTriggerEvent.asl")

Scope(\_SB.IETM)
{
  // GDDV (Get Dptf Data Vault)
  //
  // The data vault can contain APCT, APAT, and PSVT tables.
  //
  //  Arguments: (0)
  //    None
  //  Return Value:
  //    A package containing the data vault
  //
//[-start-210709-SHUI0001-modify]//
  Method(GDDV,0,Serialized,0,PkgObj)
  {
//[-start-210721-QINGLIN0001-modify]//
#if defined(C970_SUPPORT)
    Return(Package()
    {
      Buffer()
      {
        Include("BiosDataVault_C970.asl") 
      }
    })
#elif defined(C770_SUPPORT)
//[start-210720-STORM1100-modify]//
//[-start-210915-YUNLEI0133-modify]//
//[-start-211014-YUNLEI0142-modify]//
    If (LEqual(MCSZ, 0x16))  // C770 16'
    {
      If(LEqual(UMAB, Zero))  //16' UMA
      {
        If (LEqual(PTCI, 15)) // CPU U15
        {
          Return(Package()
          {
            Buffer()
            {
              Include("BiosDataVault_C770_16UMA_U15.asl") 
            }
          })
         }
         Else{//// CPU U28
          Return(Package()
          {
            Buffer()
            {
              Include("BiosDataVault_C770_16UMA_U28.asl") 
            }
          })
        }
      }
      Else  
      {//16' DIS
        Return(Package()
        {
          Buffer()
          {
            Include("BiosDataVault_C770_16DIS.asl") 
          }
        })
      }
    }
    Else  
    {// C770 14' UMA
      If (LEqual(PTCI, 15)) // CPU U15
      {
        Return(Package()
        {
          Buffer()
          {
            Include("BiosDataVault_C770_14_U15.asl") 
          }
        })
      }
      Else{// CPU U28
//[start-220307-STORM1127-modify]
        If(LOr(LAnd(LEqual (MFID, 0x834C),LEqual (PAID, 0x417A)),LAnd(LEqual (MFID, 0x834C),LEqual (PAID, 0x4152)))) {
        Return(Package()
          {
            Buffer()
              {
                Include("BiosDataVault_C770_14_U28_OLED.asl") 
              }
          })
        }
        Else
//[end-220307-STORM1127-modify]

        {
          Return(Package()
          {
          Buffer()
            {
              Include("BiosDataVault_C770_14_U28.asl") 
            }
        })
        }
      }
    }
//[-end-211014-YUNLEI0142-modify]//
//[-end-210915-YUNLEI0133-modify]//
//[end-210720-STORM1100-modify]//
//[-start-211126-JEPLIUT121-add]// 
//[-start-211214-JEPLIUT208-modify]//
//[-start-211223-JEPLIUT211-modify]//
//[-start-210614-JEPLIUT223-modify]//
#elif defined ( S570_SUPPORT )
   If (LEqual ( CPTY, 7 )) //i7
    { 
     Return(Package(){
       Buffer() 
       {
         Include("S570I7_T0223.ASL")
        }
     } ) 
   }
   elseif  (LEqual ( CPTY, 5 )) { //i5
      if (CIPU == 1) {
        Return (Package() {
           Buffer() 
           {  
             Include("S570I5_T0223.ASL")
           }
        }) 
      }
      else {
       Return (Package() {
        Buffer() 
           {  
             Include("S570I5NoIpu_T0613.asl")
           }
       }) 
      }
   }
//[-start-211204-JEPLIUT204-modify]//    
   elseif  (LEqual ( CPTY, 3)) { //i3
    if( CIPU == 1) {
      Return (Package() {
       Buffer() 
       {  
        Include("S570I3_T0223.asl") 
        }
      }) 
    }
    else{
      Return (Package() {
       Buffer() 
       {  
         Include("S570I3NoIpu_T0613.asl")
        }
      }) 
    }
   }
   else {  //Default i7
       Return (Package()
       {
         Buffer ()
          {
      Include("S570I7_T0223.ASL")
          }
       }) 
   }
//[-end-210614-JEPLIUT223-modify]//   
//[-end-211223-JEPLIUT211-modify]//   
//[-end-211214-JEPLIUT208-modify]//
//[-end-211204-JEPLIUT204-modify]//    
//[-end-211126-JEPLIUT121-add]//
//[-start-210803-QINGLIN0008-add]//
//[-start-210929-SHAONN0010-add]//
//[-start-220105-SHAONN0026-modify]//
//[-start-220120-SHAONN0028-modify]//
//[-start-220120-SHAONN0029-modify]//
//[-start-220222-QINGLIN0161-modify]//
//[-start-220225-SHAONN0030-modify]//
//[-start-220225-SHAONN0031-modify]//
#elif defined(S370_SUPPORT)
  If (LEqual(OPTY, 0x00)) //S170
  {
    Return(Package()
    {
      Buffer()
      {
        Include("BiosDataVault_S170.asl") 
      }
    })
  }
  ElseIf (LOr(LOr(LEqual(OPTY, 0x01), LEqual(OPTY, 0x03)), LAnd(LEqual(OPTY, 0x02), LEqual(MCSZ, 0x17)))) //S370 or S1415 or V17
  {
    If (LEqual (\ODVF, 0x07)) {//i7
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_S370_I7.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x05)) {//i5
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_S370_I5.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x03)) {//i3
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_S370_I3.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x02)) {//Pen
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_S370_Pen.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x01)) {//Cel
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_S370_Cel.asl") 
        }
      })
    }
    Else { //Default use I5
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_S370_I5.asl") 
        }
      })
    }
  }
  ElseIf (LEqual(OPTY, 0x02)) //V1415
  {
    If (LEqual (\ODVF, 0x07)) {//i7
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_V1415_I7.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x05)) {//i5
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_V1415_I5.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x03)) {//i3
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_V1415_I3.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x02)) {//Pen
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_V1415_Pen.asl") 
        }
      })
    }
    ElseIf (LEqual(\ODVF, 0x01)) {//Cel
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_V1415_Cel.asl") 
        }
      })
    }
    Else { //Default use I5
      Return(Package()
      {
        Buffer()
        {
          Include("BiosDataVault_V1415_I5.asl") 
        }
      })
    }
  }
  Else
  {
    Return(Package()
    {
      Buffer()
      {
        Include("BiosDataVault_S370_I5.asl") 
      }
    })
  }
//[-end-220225-SHAONN0031-modify]//
//[-end-220225-SHAONN0030-modify]//
//[-end-220222-QINGLIN0161-modify]//
//[-end-220120-SHAONN0029-modify]//
//[-end-220120-SHAONN0028-modify]//
//[-end-220105-SHAONN0026-modify]//
//[-end-210929-SHAONN0010-add]//
//[-end-210803-QINGLIN0008-add]//
//[-start-210924-GEORGE0010-add]//
//[-start-220311-GEORGE0054-modify]//
#elif defined(S77014_SUPPORT)
    Return(Package()
    {
      Buffer()
      {
        Include("BiosDataVault_S77014.asl") 
      }
    })
#elif defined(S77014IAH_SUPPORT)
    Return(Package()
    {
      Buffer()
      {
        Include("BiosDataVault_S77014IAH.asl") 
      }
    })
//[-end-220311-GEORGE0054-modify]//
//[-end-210924-GEORGE0010-add]//
//[-start-211027-GEORGE0017-add]//
#elif defined(S77013_SUPPORT)
    Return(Package()
    {
      Buffer()
      {
        Include("BiosDataVault_S77013.asl") 
      }
    })
//[-end-211027-GEORGE0017-add]//
#else
    Switch (ToInteger(PLID)) {
      Case (Package () {BoardIdAdlPLp5Gcs}) { // For ADL GCS Board Type
        Return(Package()
        {
          Buffer()
          {
            Include("BiosDataVaultGcs.asl") // empty data vault for documentation purposes
          }
        })
      }
      Default {
        Return(Package()
        {
          Buffer()
          {
            Include("BiosDataVault.asl") // empty data vault for documentation purposes
          }
        })
      }
    }
#endif
//[-end-210721-QINGLIN0001-modify]//
  }
//[-end-210709-SHUI0001-modify]//
  // IMOK
  //
  // IMOK to test if DPTF is OK and alive.
  //
  // Arguments: (1)
  //   Arg0 - integer
  //  Return Value: (0)
  //    An Integer containing the status of IMOK
  //
  Method(IMOK,1,,,IntObj)
  {
    //It is mainly a stub placeholder
    //OEMs can implement it in a way they choose
    Return (Arg0)
  }

} // End Scope(\_SB.IETM)


} // End SSDT
