/**@file
  Remote Platform Erase -  BIOS AMT Interface Support Header File.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _RPE_AMT_SUPPORT_H_
#define _RPE_AMT_SUPPORT_H_

#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <SetupVariable.h>
#include <Protocol/AmtWrapperProtocol.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseLib.h>
#include <AsfMsgs.h>
#include <Library/DxeAmtHeciLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DxeAsfHeciLib.h>
#include <RemotePlatformErase.h>
#include <Library/DxeAsfLib.h>

#define RPE_MAX_RETRIES_LIMIT                        3

//
// BIOS RPE Boot Option Parameter Stucture
//
typedef struct {
  UINT32           CsmeEraseActionBitMask;
  UINT32           EraseActionsToTrigger;
  BOOLEAN          ActionInProgress;
  UINT8            Reserved [3];
  UINT32           LastActionBeforeReboot;
  UINT32           MaxRetries;
  UINT32           CurrentRetry;
} BIOS_RPE_BOOT_PARAMETERS;

//
// RPE Boot Option Parameter Types
//
typedef enum {
  NoType,
  BitMaskParamType,
  MaxRetriesParamType,
  PsidParamType = 10,
  RpePassword = 20,
  OemDefinedParamType = 30,
} RPE_PARAM_TYPE;

//
// RPE Boot Option Parameter Type Structure
//
typedef struct {
  UINT16        VendorId;    ///< Vendor ID
  UINT16        ParamTypeId; ///< Parameter Type ID
} RPE_PARAMETER_TYPE;

//
// RPE Boot Option Parameter TLV Header Stucture
//
typedef struct {
  RPE_PARAMETER_TYPE  ParamType;  ///< RPE Parameter Type
  UINT32              Length;     ///< Length of the value array
} RPE_PARAMETER_TLV_HEADER;

//
// RPE Boot Option Parameter TLV Stucture
//
typedef struct {
  RPE_PARAMETER_TLV_HEADER  Header;     ///< RPE Parameter Type
  UINT8                     Value [0];  ///< RPE Paramter data
} RPE_PARAMETER_TLV;

/**
  Store BIOS RPE BootParameters structure to NVRAM.

  @param[in] BiosRpeBootParameter    UEFI Boot Parameters Structure

  @retval EFI_SUCCESS                The firmware has successfully stored the variable and its data as
                                     defined by the Attributes.
  @retval EFI_INVALID_PARAMETER      An invalid combination of attribute bits, name, and GUID was supplied, or the
                                     DataSize exceeds the maximum allowed.
  @retval EFI_INVALID_PARAMETER      VariableName is an empty string.
  @retval EFI_OUT_OF_RESOURCES       Not enough storage is available to hold the variable and its data.
  @retval EFI_DEVICE_ERROR           The variable could not be retrieved due to a hardware error.
  @retval EFI_WRITE_PROTECTED        The variable in question is read-only.
  @retval EFI_WRITE_PROTECTED        The variable in question cannot be deleted.
  @retval EFI_SECURITY_VIOLATION     The variable could not be written due to EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACESS being set,
                                     but the AuthInfo does NOT pass the validation check carried out by the firmware.
  @retval EFI_NOT_FOUND              The variable trying to be updated or deleted was not found.
**/
EFI_STATUS
StoreBiosRpeBootParameter (
  IN BIOS_RPE_BOOT_PARAMETERS  *BiosRpeBootParameters
  );

/**
  Decrement EraseAction Bit in the BiosRpeBootParameter Structure.

  @param[in] BiosRpeBootParameter    UEFI Boot Parameters Structure
  @param[in] EraseActionBit          EraseAction Bit to be cleared.
  @param[in] Status                  EraseAction exit status.

**/
VOID
DecrementBiosRpeBootParameter (
  IN BIOS_RPE_BOOT_PARAMETERS    *BiosRpeBootParameters,
  IN UINT32                      EraseActionBit,
  IN EFI_STATUS                  Status
  );

/**
  Initialize RPE BootParameters structure with the BootParameters from ASF.
  Send commands to ME to start the RPE Actions.

  @param[out] BiosRpeBootParameters     UEFI Boot Option with parameters

  @retval EFI_UNSUPPORTED               Current ME mode doesn't support this function
  @retval EFI_SUCCESS                   Command succeeded
  @retval EFI_INVALID_PARAMETER         Invalid Boot Options State
  @retval EFI_NOT_FOUND                 HECI Communication failed
  @retval EFI_DEVICE_ERROR              ASF Response is invalid
  @retval EFI_OUT_OF_RESOURCES          Return Out Of Resource
**/
EFI_STATUS
RpeInitialize (
  OUT BIOS_RPE_BOOT_PARAMETERS  *BiosRpeBootParameters
  );

/**
  Delete "BiosRpeBootParameters" NVRAM Data
**/
VOID
DeleteBiosRpeBootParameters (
  VOID
  );

/**
  Check RPE Retry condition

  @param[in] BiosRpeBootParameter      UEFI Boot Parameters Structure

  @retval TRUE                         Retry Condition Check PASS.
  @retval FALSE                        Do Not Retry.
**/
BOOLEAN
CheckAmtRpeRetryCondition (
  IN BIOS_RPE_BOOT_PARAMETERS  *BiosRpeBootParameters
  );

/**
  Initializes the library. Required to be used before calling any other function
  from this library.
**/
VOID
InitUiLib (
  VOID
  );
#endif
