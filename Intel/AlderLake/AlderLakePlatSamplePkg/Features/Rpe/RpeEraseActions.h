/**@file
  RPE Erase Action Header File.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _RPE_ERASE_ACTION_H_
#define _RPE_ERASE_ACTION_H_

#include <Library/PcdLib.h>

#define UNCONFIGURE_TIMEOUT       60

typedef struct {
  CHAR16                        *Name;
  EFI_GUID                      *Guid;
} USER_DATA_VARIABLE_ENTRY;

/**
  Register RPE SSD Erase Action Event

  @retval EFI_SUCCESS         SSD Erase procedure has been started
  @retval EFI_ABORTED         Failed to register Ssd Erase Event.
**/
EFI_STATUS
RegisterRpeSsdEraseEvent (
  VOID
  );

/**
  Function to trigger TPM Clear operation by invoking Tpm2ClearControl and Tpm2Clear.
  If TPM is present/enabled and TCG2 protocol is not installed while this driver is getting executed,
  then an End of DXE event will be registered and operation will be performed later in the boot flow.
  TPM Clear affects Endorsement keys, NV Indexes and objects stored in the TPM's Owner or
  Endorsement hierarchies (like bitlocker key / virtual smart card / fingerprint data etc...)
  TPM clear does not affect TPM measurements.

  @retval EFI_SUCCESS           Operation completed successfully.
  @retval EFI_UNSUPPORTED       If TPM is not present/enabled.
  @retval EFI_DEVICE_ERROR      If TPM is present/enabled and TCG2 protocol is not installed.
  @retval EFI_TIMEOUT           The register can't run into the expected status in time.
  @retval EFI_BUFFER_TOO_SMALL  Response data buffer is too small.
  @retval EFI_DEVICE_ERROR      Unexpected device behavior.
**/
EFI_STATUS
TriggerTpmClear (
  VOID
  );

/**
  Reload BIOS NVRAM Variables with its Default Configuration.

  @retval EFI_SUCCESS         BIOS NV Data Reload status.
  @retval EFI_ABORTED         Failed to Reload BIOS NV Data.
**/
EFI_STATUS
SetBiosNvDataDefaultConfig (
  VOID
  );

/**
  Perform Unconfigure CSME without password and try to wait for completion.

  @retval EFI_ABORTED  Unconfiguration error
  @retval EFI_TIMEOUT  Unconfiguration is still in progress after time elapsed
  @retval EFI_SUCCESS  Unconfiguration finished successfully
**/
EFI_STATUS
RpeCsmeUnconfigure (
  VOID
  );
#endif
