/**@file
  Remote Platform Erase - BIOS AMT Interface Support File.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "RpeAmtSupport.h"

/**
  Store BIOS RPE BootParameters structure to NVRAM.

  @param[in] BiosRpeBootParameter    UEFI Boot Parameters Structure

  @retval EFI_SUCCESS                The firmware has successfully stored the variable and its data as
                                     defined by the Attributes.
  @retval EFI_INVALID_PARAMETER      An invalid combination of attribute bits, name, and GUID was supplied, or the
                                     DataSize exceeds the maximum allowed.
  @retval EFI_INVALID_PARAMETER      VariableName is an empty string.
  @retval EFI_OUT_OF_RESOURCES       Not enough storage is available to hold the variable and its data.
  @retval EFI_DEVICE_ERROR           The variable could not be retrieved due to a hardware error.
  @retval EFI_WRITE_PROTECTED        The variable in question is read-only.
  @retval EFI_WRITE_PROTECTED        The variable in question cannot be deleted.
  @retval EFI_SECURITY_VIOLATION     The variable could not be written due to EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACESS being set,
                                     but the AuthInfo does NOT pass the validation check carried out by the firmware.
  @retval EFI_NOT_FOUND              The variable trying to be updated or deleted was not found.
**/
EFI_STATUS
StoreBiosRpeBootParameter (
  IN BIOS_RPE_BOOT_PARAMETERS  *BiosRpeBootParameters
  )
{
  EFI_STATUS                           Status;

  DEBUG ((DEBUG_INFO, "RPE: Inside StoreBiosRpeBootParameter()\n"));

  //
  // Create BiosRpeBootParameters NVRAM structure to track RPE actions.
  //
  Status = gRT->SetVariable (
                   L"BiosRpeBootParameters",
                   &gSetupVariableGuid,
                   EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                   sizeof (BIOS_RPE_BOOT_PARAMETERS),
                   BiosRpeBootParameters
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPE: SetVariable of BiosRpeBootParameters Failed.\n"));
  }
  return Status;
}

/**
  Decrement EraseAction Bit in the BiosRpeBootParameter Structure.

  @param[in] BiosRpeBootParameter    UEFI Boot Parameters Structure
  @param[in] EraseActionBit          EraseAction Bit to be cleared.
  @param[in] Status                  EraseAction exit status.

**/
VOID
DecrementBiosRpeBootParameter (
  IN BIOS_RPE_BOOT_PARAMETERS    *BiosRpeBootParameters,
  IN UINT32                      EraseActionBit,
  IN EFI_STATUS                  Status
  )
{
  // Clear the BitMask Bit Corresponding to mark the completion of the current Action iteration.
  BiosRpeBootParameters->EraseActionsToTrigger &= (UINT32) (~EraseActionBit);

  // Handle the Erase Action Status: Success/Not Supported Case:
  if ((Status == EFI_SUCCESS) || (Status == EFI_UNSUPPORTED)) {
    // Clear the Erase Action BitMask to mark this action as completed.
    BiosRpeBootParameters->CsmeEraseActionBitMask &= (UINT32) (~EraseActionBit);
  }

  // Update the BiosRpeBootParameters to NVRAM.
  StoreBiosRpeBootParameter (BiosRpeBootParameters);
}

/**
  Initialize RPE BootParameters structure with the BootParameters from ASF.
  Send commands to CSME to notify RPE beginning.

  @param[out] BiosRpeBootParameters     UEFI Boot Option with parameters

  @retval EFI_UNSUPPORTED               Current ME mode doesn't support this function
  @retval EFI_SUCCESS                   Command succeeded
  @retval EFI_INVALID_PARAMETER         Invalid Boot Options State
  @retval EFI_NOT_FOUND                 HECI Communication failed
  @retval EFI_DEVICE_ERROR              ASF Response is invalid
  @retval EFI_OUT_OF_RESOURCES          Return Out Of Resource
**/
EFI_STATUS
RpeInitialize (
  OUT BIOS_RPE_BOOT_PARAMETERS  *BiosRpeBootParameters
  )
{
  EFI_STATUS                           Status;
  UEFI_BOOT_OPTION_PARAMETER           *AmtUefiBootOption;
  RPE_PARAMETER_TLV                    *RpeParamTlv;
  UINT32                               Index;
  UINT32                               BiosRpeBootParametersVolAttr;
  UINTN                                Size;

  DEBUG ((DEBUG_INFO, "Platform Erase: Inside RpeInitialize to Get RPE Asf_GetUefiBootParameters()\n"));

  //
  // Check if BIOS_RPE_BOOT_PARAMETERS exists in NVRAM from previous RPE Erase Action Reboot.
  // If exists:
  //      Update the BiosRpeBootParameters structure from NVRAM Data.
  // If doesn't exists:
  //      Send PET Alert :  BEGIN_PLATFORM_ERASE
  //      Update the BiosRpeBootParameters structure with BootParameters from ASF.
  //      Store the  BiosRpeBootParameters structure to NVRAM.
  //

  Size = sizeof (BIOS_RPE_BOOT_PARAMETERS);
  Status = gRT->GetVariable (L"BiosRpeBootParameters", &gSetupVariableGuid, &BiosRpeBootParametersVolAttr, &Size, BiosRpeBootParameters);
  if (Status == EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters exists. Returning BiosRpeBootParameters.\n"));
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters->CsmeEraseActionBitMask %x \n", BiosRpeBootParameters->CsmeEraseActionBitMask ));
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters->EraseActionsToTrigger %x \n", BiosRpeBootParameters->EraseActionsToTrigger ));
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters->ActionInProgress %x \n", BiosRpeBootParameters->ActionInProgress ));
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters->LastActionBeforeReboot %x \n", BiosRpeBootParameters->LastActionBeforeReboot ));
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters->MaxRetries %x \n", BiosRpeBootParameters->MaxRetries ));
    DEBUG ((DEBUG_INFO, "Platform Erase: BiosRpeBootParameters->CurrentRetry %x \n", BiosRpeBootParameters->CurrentRetry ));
    return Status;
  }

  SendRpePetAlert (ASF_RPE_EVENT_DATA3_BEGINNING_PLATFORM_ERASE, 0, ASF_RPE_EVENT_OFFSET_PROGRESS);
  // Set BIOS_LastStatus = 1 (InProgress)
  ReportBiosStatus (AsfRbsInProgress);

  //
  // Get ASF BootParameters from CSME.
  //
  AmtUefiBootOption = AllocateZeroPool (sizeof (UEFI_BOOT_OPTION_PARAMETER));
  if (AmtUefiBootOption == NULL) {
    DEBUG ((DEBUG_ERROR, "Platform Erase: AmtUefiBootOption EFI_OUT_OF_RESOURCES.\n"));
    return EFI_OUT_OF_RESOURCES;
  }


      Status = AsfGetUefiBootParameters (AmtUefiBootOption);
  if (EFI_ERROR (Status)) {
    SendRpePetAlert (ASF_RPE_EVENT_DATA3_BOOT_PARAMETER_RECEIVE_FAILED, 0, ASF_RPE_EVENT_OFFSET_FAILURE);
    DEBUG ((DEBUG_ERROR, "Platform Erase: AsfGetUefiBootParameters Failed.\n"));
    FreePool (AmtUefiBootOption);
    return Status;
  }

  //
  // Extract BootParameters from the TLV Structure
  // Update LOCAL_RPE_BOOT_PARAMETERS structure with the value from ASF which is in PARAMETER_TLV format.
  //

  RpeParamTlv = (RPE_PARAMETER_TLV*) AmtUefiBootOption->ParametersArray;

  for (Index = 0; Index < AmtUefiBootOption->NumberOfParams; Index++) {
    DEBUG ((DEBUG_INFO, "Index:%d ParamTypeId: %d\n length:%d\n", Index, RpeParamTlv->Header.ParamType.ParamTypeId, RpeParamTlv->Header.Length));

    switch (RpeParamTlv->Header.ParamType.ParamTypeId) {
      case BitMaskParamType:
        CopyMem (&BiosRpeBootParameters->CsmeEraseActionBitMask, &RpeParamTlv->Value, RpeParamTlv->Header.Length);
        DEBUG ((DEBUG_INFO, "Platform Erase: BitMask = %x.\n", BiosRpeBootParameters->CsmeEraseActionBitMask));
        break;

      default:
        break;
    }
    // Increment to next RpeParamTlv
    RpeParamTlv = (RPE_PARAMETER_TLV*) (((UINT8 *) RpeParamTlv) + sizeof (RPE_PARAMETER_TLV_HEADER) + RpeParamTlv->Header.Length);
  }

  FreePool (AmtUefiBootOption);

  // Initialize BIOS_RPE_BOOT_PARAMETERS structure members to defaults.
  BiosRpeBootParameters->ActionInProgress = FALSE;
  BiosRpeBootParameters->LastActionBeforeReboot = 0;
  BiosRpeBootParameters->CurrentRetry = 0;
  BiosRpeBootParameters->MaxRetries = RPE_MAX_RETRIES_LIMIT;

  //
  // Clear reserved parameters. Enable only the Platform Supported RPE Erase Action Bit Masks.
  //
  BiosRpeBootParameters->EraseActionsToTrigger = BiosRpeBootParameters->CsmeEraseActionBitMask & RPE_CAPABILITY_SUPPORT;

  // If  _ConfigurationDataReset_ boot option bit is set and if any other Erase action bit is set then it is an Invalid scenario.
  // In this case send PET alert as invalid boot parameters and return.

  if ((AsfIsConfigurationDataResetEnabled ()) &&
      ((BiosRpeBootParameters->EraseActionsToTrigger & RPE_CAPABILITY_SUPPORT) & ~RPE_SUPPORTED)) {
    SendRpePetAlert (ASF_RPE_EVENT_DATA3_INVALID_BOOT_PARAMETERS, 0, ASF_RPE_EVENT_OFFSET_FAILURE);
    return EFI_UNSUPPORTED;
  }
  SendRpePetAlert (ASF_RPE_EVENT_DATA3_CLEAR_RESERVED_PARAMETERS, 0, ASF_RPE_EVENT_OFFSET_PROGRESS);

  // If _ConfigurationDataReset_ is not set then store BiosRpeBootParameter and return.
  // Else it is CSME Unconfigure case and hence no need to store BiosRpeBootParameter.
  if (!(AsfIsConfigurationDataResetEnabled ())) {
    // Create BiosRpeBootParameters NVRAM structure with value from ASF BootParameters.
    Status = StoreBiosRpeBootParameter (BiosRpeBootParameters);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Platform Erase: Failed to store BIOS RPE BootParameters structure to NVRAM.\n"));
      return Status;
    }
  }

  return EFI_SUCCESS;
}

/**
  Delete "BiosRpeBootParameters" NVRAM Data
**/
VOID
DeleteBiosRpeBootParameters (
  VOID
  )
{
  EFI_STATUS                 Status;

  DEBUG ((DEBUG_INFO, "Platform Erase: Deleting 'BiosRpeBootParameters' NVRAM Data\n"));
  //
  // Delete "BiosRpeBootParameters" NVRAM Structure
  //
  Status = gRT->SetVariable (
                   L"BiosRpeBootParameters",
                   &gSetupVariableGuid,
                   0,
                   0,
                   NULL
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to Delete BiosRpeBootParameters NVRAM Structure = %r\n", Status));
  }
}

/**
  Check RPE Retry condition

  @param[in] BiosRpeBootParameter      UEFI Boot Parameters Structure

  @retval TRUE                         Retry Condition Check PASS.
  @retval FALSE                        Do Not Retry.
**/
BOOLEAN
CheckAmtRpeRetryCondition (
  IN BIOS_RPE_BOOT_PARAMETERS  *BiosRpeBootParameters
  )
{
  DEBUG ((DEBUG_INFO, "Platform Erase: Inside CheckAmtRpeRetryCondition()\n"));

  //
  // Check if CurrentRetry Count < MaxRetry value from ASF BootOption:
  //
  if ((BiosRpeBootParameters->CurrentRetry) < (BiosRpeBootParameters->MaxRetries)) {
    ReportBiosStatus (AsfRbsInProgress);
    return TRUE;
  }

  //
  // If control comes here: CurrentRetry Count is > MaxRetry value.
  // 1. Send PET Event (Max Retries Reached)
  // 2. Set BIOS_LastStatus = 0 (Success)
  //

  // Send PET Event (Max Retries Reached)
  SendRpePetAlert (ASF_RPE_EVENT_DATA3_REACHED_MAX_RETRY_COUNT, 0, ASF_RPE_EVENT_OFFSET_FAILURE);

  //
  // Set BIOS_LastStatus = 0 (Success)
  //
  ReportBiosStatus (AsfRbsSuccess);

  DEBUG ((DEBUG_ERROR, "Platform Erase: Max Retries Reached. Exiting RetryLoop.\n"));
  return FALSE;
}
