/**@file
  This file is CastroCove Pmic Nvm update

@copyright
 Copyright (c) 2020 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains an 'Intel Peripheral Driver' and is uniquely
 identified as "Intel Reference Module" and is licensed for Intel
 CPUs and chipsets under the terms of your license agreement with
 Intel or your vendor. This file may be modified by the user, subject
 to additional terms of the license agreement.

@par Specification
**/

#include "CastroCovePmicNvm.h"
#include <CastroCovePmicRegisters.h>
#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TimerLib.h>
#include <PlatformInfo.h>
#include <Library/PcdLib.h>
#include <Library/PmcLib.h>
#include <Register/PmcRegs.h>
#include <Protocol/FirmwareVolume2.h>
#include <Library/CpuLib.h>
#include <Library/PostCodeLib.h>
#include <PlatformBoardId.h>


/**
  Reads an 8-bit PMIC register of CastroCove.

  Reads the 8-bit PMIC register specified by RegisterOffset input field.
  The operation status is returned.

  @param[in]  RegisterOffset  - The PMIC register to read
  @param[out] Value           - The value read from the PMIC register

  @retval EFI_SUCCESS         - Read bytes from PMIC device successfully
  @retval Others              - Status depends on IPC operation
**/
EFI_STATUS
EFIAPI
CastroCovePmicRead8 (
  IN UINT32    RegisterOffset,
  OUT UINT8    *Value,
  IN UINT8     PmicId
  )
{
  EFI_STATUS                Status;

  Status = PmicRead8 (RegisterOffset, Value, PmicId);
  return Status;
}

/**
  Writes an 8-bit PMIC register of CastroCove with a 8-bit value.

  Writes the 8-bit PMIC register specified by RegisterOffset input field with the value specified
  by Value and return the operation status.

  @param[in]  RegisterOffset  - The PMIC register to write.
  @param[in]  Value           - The value to write to the PMIC register

  @retval EFI_SUCCESS         - Write bytes to PMIC device successfully
  @retval Others              - Status depends on IPC operation
**/
EFI_STATUS
EFIAPI
CastroCovePmicWrite8 (
  IN UINT32    RegisterOffset,
  IN UINT8     Value,
  IN UINT8     PmicId
  )
{
  EFI_STATUS                Status;
  Status = PmicWrite8 (RegisterOffset, Value, PmicId);
  return Status;
}

/**
  Wait for NVM OTP Power Recovery

  @param[in]                  - None

  @retval EFI_SUCCESS         - OTP memory is powered up
  @retval Others              - OTP memory is in power down mode
**/
EFI_STATUS
EFIAPI
WaitsForOtpPowerUp (
  IN UINT8                PmicId
  )
{
  EFI_STATUS                Status;
  UINT8                     Data8;
  UINT8                     Count;
  UINT8                     MaxCount;

  Status = EFI_TIMEOUT;
  Count = 0;
  MaxCount = 10;

  DEBUG ((EFI_D_INFO, "WaitsForOtpPowerUp Begin\n"));

  while (Count < MaxCount) { // Max timeout = 10*100ms = 1 second
    Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMSTAT0_REG, &Data8, PmicId);
    if (Status == EFI_SUCCESS && (Data8 & B_CASTRO_COVE_NVMSTAT0_REG_OTP_PD_ACT) == 0) {
      Status = EFI_SUCCESS;
      break;
    }
    gBS->Stall (100*1000); //100ms
    Count++;
  }

  // If no successful response even after 1 sec, indicate failure
  if (Count == MaxCount) {
    Status = EFI_TIMEOUT;
  }

  DEBUG ((EFI_D_INFO, "WaitsForOtpPowerUp End, Status: %r\n", Status));

  return Status;
}

/**
  Wait for NVM Ready to accept write access

  @param[in]                  - None

  @retval EFI_SUCCESS         - Ready to accept a new data section
  @retval Others              - Programming OTP memory
**/
EFI_STATUS
EFIAPI
WaitsForNvmReadyToAcceptWrite (
  IN UINT8                PmicId
  )
{
  EFI_STATUS                Status;
  UINT8                     Data8;
  UINT8                     Count;
  UINT8                     MaxCount;

  Status = EFI_TIMEOUT;
  Count = 0;
  MaxCount = 100;

  DEBUG ((EFI_D_INFO, "WaitsForNvmReadyToAcceptWrite Begin\n"));

  while (Count < MaxCount) { // Max timeout = 100*10ms = 1 second
    Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMSTAT0_REG, &Data8, PmicId);
    if ((Data8 & B_CASTRO_COVE_NVMSTAT0_REG_OTP_WR_ONGOING) == 0) {
      Status = EFI_SUCCESS;
      break;
    }
    gBS->Stall (10*1000); //10ms
    Count++;
  }

  DEBUG ((EFI_D_INFO, "WaitsForNvmReadyToAcceptWrite End, Status:%r\n", Status));

  return Status;
}

/**
  Wait for NVM Controller Writing Status is Active

  @param[in]                  - None

  @retval EFI_SUCCESS         - Active
  @retval Others              - Inactive
**/
EFI_STATUS
EFIAPI
WaitsForProgrammingOtpMemoryBegin (
  IN UINT8                PmicId
  )
{
  EFI_STATUS                Status;
  UINT8                     Data8;
  UINT8                     Count;
  UINT8                     MaxCount;

  Status = EFI_TIMEOUT;
  Count = 0;
  MaxCount = 100;

  DEBUG ((EFI_D_INFO, "WaitsForProgrammingOtpMemoryBegin Begin\n"));

  while (Count < MaxCount) { // Max timeout = 100*10ms = 1 second
    Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMSTAT0_REG, &Data8, PmicId);
    if ((Data8 & B_CASTRO_COVE_NVMSTAT0_REG_OTP_WR_ONGOING) == 0) {
      Status = EFI_SUCCESS;
      break;
    }
    gBS->Stall (10*1000); //10ms
    Count++;
  }

  DEBUG ((EFI_D_INFO, "WaitsForProgrammingOtpMemoryBegin End, Status:%r\n", Status));

  return Status;
}

/**
  Wait for NVM finishes the section write access

  @param[in]                  - None

  @retval EFI_SUCCESS         - Section write successful
  @retval Others              - Section write un-successful
**/
EFI_STATUS
EFIAPI
WaitsForNvmSectionWriteCompletion (
  IN UINT8                PmicId
  )
{
  // Wait for completion interrupt [NVMVIRQ_REG.WRFIN]
  //     (or)
  // Predefined period of time
  //     Per bit write time taken = 100us
  //     MaxBytes to write = 35
  //     35*8=280 bits
  //     280*100=28000us=28ms
  //     For illustration purpose we are using 100ms as safe time period

  EFI_STATUS                Status;
  UINT8                     Data8;
  UINT8                     Count;
  UINT8                     MaxCount;

  Status = EFI_SUCCESS;
  Count = 0;
  MaxCount = 100;

  DEBUG ((EFI_D_INFO, "WaitsForNvmSectionWriteCompletion Begin\n"));

  while (Count < MaxCount) { // Max timeout = 100*1ms = 100 milli-second
    Status = CastroCovePmicRead8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, &Data8, PmicId);
    if (Status == EFI_SUCCESS) {
      if ((Data8 & B_CASTRO_COVE_LVL2_NVM_IRQ_REG_WRFIN_IRQ) == 1) {
        break;
      }
      gBS->Stall (1*1000); //1ms
      Count++;
    } else {
      break;
    }
  }

  if (Status == EFI_SUCCESS) {
    // AP Clears interrupt by writing 0x01 to NVMIRQ
    Status = CastroCovePmicRead8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, &Data8, PmicId);
    if (Status == EFI_SUCCESS) {
      Data8 |= B_CASTRO_COVE_LVL2_NVM_IRQ_REG_WRFIN_IRQ;
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, Data8, PmicId);
    }
  }

  DEBUG ((EFI_D_INFO, "WaitsForNvmSectionWriteCompletion End\n"));

  return Status;
}

/**
  Read the input Nvm file from Firmware Volume.

  @param[in]                  - Address of the buffer to store the Nvm content
  @param[in]                  - Size in bytes

  @retval Buffer              - Address of the buffer where the Nvm content is stored
  @retval Size                - Size in bytes
**/
EFI_STATUS
EFIAPI
CastroCovePmicNvmReadFromFv (
  IN OUT UINT8                **Buffer,
  IN OUT UINTN                *Size,
  IN EFI_GUID                 *PmicNvmFvFileGuid
  )
{
  EFI_STATUS                    Status;
  UINTN                         NumHandles;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         Index;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *Fv;
  UINT8                         *FileBuffer;
  UINTN                         FileBufferSize;
  UINT32                        AuthenticationStatus;

  HandleBuffer   = NULL;
  Fv             = NULL;
  FileBuffer     = NULL;
  FileBufferSize = 0;

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmReadFromFv Begin\n"));

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiFirmwareVolume2ProtocolGuid,
                  NULL,
                  &NumHandles,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicNvmReadFromFv LocateHandleBuffer Status - %r\n", Status));
    return Status;
  }

  for (Index = 0; Index < NumHandles; Index++) {
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiFirmwareVolume2ProtocolGuid,
                    (VOID **) &Fv
                    );
    if (EFI_ERROR (Status)) continue;

    Status = Fv->ReadSection (
                      Fv,
                      PmicNvmFvFileGuid,
                      EFI_SECTION_RAW,
                      0,
                      (VOID **) &FileBuffer,
                      &FileBufferSize,
                      &AuthenticationStatus
                      );
    if (Status == EFI_SUCCESS) {
      *Buffer = FileBuffer;
      *Size = FileBufferSize;
      break;
    }

  }

  (gBS->FreePool)(HandleBuffer);
  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmReadFromFv End, Status: %r\n", Status));
  return Status;
}

/**
  Check the conditions for allow the update or not.

  @param[in] Buffer           - Address of the buffer where the Nvm content is stored
  @param[in] Size             - Size in bytes

  @retval EFI_SUCCESS         - Allow the update
  @retval Others              - Don't allow the update
**/
EFI_STATUS
EFIAPI
CastroCovePmicNvmUpdateCriteriaCheck (
  IN UINT8               *Buffer,
  IN UINTN                Size,
  IN UINT8                PmicId
  )
{
  EFI_STATUS                Status;
  UINT8                     *BufferPointer;
  UINT8                     CurrentPmicRevision;
  CASTRO_COVE_PMIC_NVM_GLOBAL_HEADER *GlobalHeader;
  UINT16                    Index;

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdateCriteriaCheck Begin\n"));

  Status = EFI_ACCESS_DENIED;
  BufferPointer = Buffer;
  GlobalHeader = (CASTRO_COVE_PMIC_NVM_GLOBAL_HEADER *)BufferPointer;
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_ID2_REG, &CurrentPmicRevision, PmicId);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (Index = 0; Index < MAX_SILICON_STEPPINGS; Index++) {
    if (CurrentPmicRevision == GlobalHeader->Stepping[Index].ID2_REG) { // Allow to upadte if silicon stepping ID is matched
      Status = EFI_SUCCESS;
    }
  }
  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdateCriteriaCheck End, Status:%r\n", Status));

  return Status;
}

/**
  Write the Nvm contents from buffer to Castro Cove Pmic Nvm.

  @param[in] Buffer           - Address of the buffer where the Nvm content is stored
  @param[in] Size             - Size in bytes

  @retval EFI_SUCCESS         - Write bytes to PMIC device successfully
  @retval Others              - Status depends on IPC operation
**/
EFI_STATUS
EFIAPI
CastroCovePmicNvmUpdate (
  IN UINT8               *Buffer,
  IN UINTN                Size,
  IN UINT8                DesiredPayloadType,
  IN UINT8                PmicId,
  IN OUT UINT8           *PayloadFvBuffer,
  IN OUT UINT16          *PayloadFvSize
  )
{
  EFI_STATUS                          Status;
  UINT8                               *BufferPointer;
  UINT16                              PayloadFvIndex;
  UINT16                              Index;
  UINT16                              DataIndex;
  CASTRO_COVE_PMIC_NVM_SECTION_HEADER *SectionHeader;
  CASTRO_COVE_PMIC_NVM_PAYLOAD_HEADER *PayloadHeader;
  UINT8                               SectionLength;
  UINT8                               CurrentNvmVersion;
  UINT8                               CurrentCustomizationVersion;
  UINT8                               CurrentPmicRevision;
  UINT16                              Length_Of_Payload = 0;
  UINT16                              Offset;
  UINT8                               Data8;
  BOOLEAN                             UpdateFinished;

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate Begin\n"));

  PayloadFvIndex = *PayloadFvSize;
  UpdateFinished = FALSE;
  BufferPointer = Buffer;

  Index = 0 + CASTRO_COVE_PMIC_NVM_FILE_GLOBAL_HEADER_SIZE; // Index starts from 0 (but if any padding added for File Release information then Index starts after the padding data)
  Offset = CASTRO_COVE_PMIC_NVM_FILE_GLOBAL_HEADER_SIZE; // Offset now points to End of Global header (beginning of first payload header)
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMBUILD0_REG, &CurrentNvmVersion, PmicId); // Get latest NVM version from board
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_CUSTVER_REG, &CurrentCustomizationVersion, PmicId); // Get the Latest Customer version from board
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_ID2_REG, &CurrentPmicRevision, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  while (Index < Size) {
    //
    // Calculate starting Offset of Payload data
    //
    Offset = Offset + Length_Of_Payload; // Offset now points to beginning of first payload data header
    DEBUG ((EFI_D_INFO, "Index = %d, Offset - %d\n", Index, Offset));
    PayloadHeader = (CASTRO_COVE_PMIC_NVM_PAYLOAD_HEADER *)(BufferPointer + Offset);
    DEBUG ((EFI_D_INFO, "\n"));
    DEBUG ((EFI_D_INFO, "PayloadHeader->PAYLOAD_TYPE:          0x%02X\n", PayloadHeader->PAYLOAD_TYPE));
    DEBUG ((EFI_D_INFO, "PayloadHeader->ID1_REG:               0x%02X\n", PayloadHeader->ID1_REG));
    DEBUG ((EFI_D_INFO, "PayloadHeader->ID2_REG:               0x%02X\n", PayloadHeader->ID2_REG));
    DEBUG ((EFI_D_INFO, "PayloadHeader->NVMBUILD0_CUSTVER_REG: 0x%02X\n", PayloadHeader->NVMBUILD0_CUSTVER_REG));
    Length_Of_Payload = (UINT16)((PayloadHeader->PAYLOAD_LENGTH_MSB << 8) | (PayloadHeader->PAYLOAD_LENGTH_LSB & 0xff));
    DEBUG ((EFI_D_INFO, "PayloadHeader->PAYLOAD_LENGTH:        0x%04X\n", Length_Of_Payload));
    Offset = Offset + CASTRO_COVE_PAYLOAD_HEADER_SIZE; // Offset now points to beginning of first payload data

    if (PayloadHeader->PAYLOAD_TYPE == INVALID_PAYLOAD) { // If payload type is not valid stop the update
      if (UpdateFinished == TRUE) {
        break;
      } else {
        DEBUG ((EFI_D_INFO, "CastroCove Payload Type Not Valid\n"));
        Status = EFI_ALREADY_STARTED;
        return Status;
      }
    }
    if ((DesiredPayloadType != PayloadHeader->PAYLOAD_TYPE) || (PayloadHeader->ID1_REG != CASTRO_COVE_PMIC_ID)) { // Skip the update If the PMIC identification register is not CRC
      DEBUG ((EFI_D_INFO, "PMIC identification register is not CastroCove\n"));
      Status = EFI_INVALID_PARAMETER;
      continue;
    }
    if ((DesiredPayloadType == CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION) && (PayloadHeader->NVMBUILD0_CUSTVER_REG <= CurrentCustomizationVersion)) { // Skip the update If current Customization version is greater than and equal to payload version
      DEBUG ((EFI_D_INFO, "Current Customization version(%x) is greater than the payload version(%x)\n", CurrentCustomizationVersion, PayloadHeader->NVMBUILD0_CUSTVER_REG));
      Status = EFI_INVALID_PARAMETER;
      continue;
    }
    if ((DesiredPayloadType == CASTRO_COVE_PAYLOAD_TYPE_NVM) && (PayloadHeader->NVMBUILD0_CUSTVER_REG <= CurrentNvmVersion)) { // Skip the Update If current NVM version is greater than and equal to payload version
      DEBUG ((EFI_D_INFO, "Current NVM version(%x) is greater than the payload version(%x)\n", CurrentNvmVersion, PayloadHeader->NVMBUILD0_CUSTVER_REG));
      Status = EFI_INVALID_PARAMETER;
      continue;
    }
    if (CurrentPmicRevision != PayloadHeader->ID2_REG) { // Skip the update if slicon stepping ID is not equal
      DEBUG ((EFI_D_INFO, "CastroCove slicon stepping ID is not equal\n"));
      continue;
    }

    Index = Offset;
    while (Index < (Offset + Length_Of_Payload)) {
      // Apply recommended NVM controller parameters
      // variable programming pulse width support, OTP program recovery timeout support
      Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMCTRL0_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Data8 &= (~B_CASTRO_COVE_NVMCTRL0_REG_PRG_PULSE_WIDTH); // 0 : T050US
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVMCTRL0_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the NVM CTRL Config Register 0, Status: %r\n", Status));
        return Status;
      }

      Status = CastroCovePmicRead8 (R_CASTRO_COVE_OTPMRB1_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Data8 |= B_CASTRO_COVE_OTPMRB1_REG_OTP_RD2_MRB14; // 1 : disabled
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_OTPMRB1_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the OTP Mode Register B1, Status: %r\n", Status));
        return Status;
      }

      Status = CastroCovePmicRead8 (R_CASTRO_COVE_OTPMRB0_RD2_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Data8 &= (~(B_CASTRO_COVE_OTPMRB0_RD2_REG_OTP_RD2_MRB_H + B_CASTRO_COVE_OTPMRB0_RD2_REG_OTP_RD2_MRB_L));
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_OTPMRB0_RD2_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the OTP Mode Register B0 Read2, Status: %r\n", Status));
        return Status;
      }

      Status = CastroCovePmicRead8 (R_CASTRO_COVE_OTPMRB0_RD1_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Data8 &= (~(B_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_H + B_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_L));
      Data8 |= (8 << N_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_L);
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_OTPMRB0_RD1_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the OTP Mode Register B0 Read1, Status: %r\n", Status));
        return Status;
      }

      Status = CastroCovePmicRead8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the NVM Controller Interrupt Register, Status: %r\n", Status));
        return Status;
      }

      /*
      [Step #1] AP reads OTP_WR_ONGOING bit
      [    1.a] If it is 0 AP writes section sequentially to the I2C buffer and proceeds to step 2
      [    1.b] If it is 1 AP waits for predefined period before reading OTP_WR_ONGOING bit again
      */
      Status = WaitsForNvmReadyToAcceptWrite(PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate WaitsForNvmReadyToAcceptWrite Status:%r\n", Status));
        return Status;
      }
      //
      //[Step #2] AP writes complete section to the register buffer (NVMDBUF0_REG - NVMDBUF34_REG)
      //
      SectionHeader = (CASTRO_COVE_PMIC_NVM_SECTION_HEADER *)(BufferPointer + Index);
      SectionLength = (UINT8)(SectionHeader->SECTION_LENGTH) + 1;

      for (DataIndex = Index; DataIndex < (sizeof(CASTRO_COVE_PMIC_NVM_SECTION_HEADER)+SectionLength + Index); DataIndex++) {
        Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVMDBUF0_REG + (DataIndex - Index), BufferPointer[DataIndex], PmicId);
        //
        // For successful Section update, PMIC sets bit[7] = 1 of the Section header 1st byte.
        // Hence, while copying the payload buffer, bit[7] is set to 1 to aid in comparison of the NVM contents
        //
        if (DataIndex == Index) {
          PayloadFvBuffer[PayloadFvIndex++] = (BufferPointer[DataIndex] | (1 << 7));
        } else {
          PayloadFvBuffer[PayloadFvIndex++] = BufferPointer[DataIndex];
        }
        if (EFI_ERROR (Status)) {
          DEBUG ((EFI_D_INFO, "Could not access the Buffer Register 0x%x, Status: %r\n", R_CASTRO_COVE_NVMDBUF0_REG + (DataIndex - Index), Status));
          return Status;
        }
      }

      /*
      [Step #3] AP writes 1 to the OTP_WR_MODE_EN bit to enable the write process
      [    3.a] NVM controller acknowledges the OTP_WR_MODE_EN flag and starts OTP power recovery if required
      */
      Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMCTRL1_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Data8 |= B_CASTRO_COVE_NVMCTRL1_REG_OTP_WR_MODE_EN;
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVMCTRL1_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the NVM CTRL Config Register 1, Status: %r\n", Status));
        return Status;
      }

      /*
      [Step #4] AP waits for OTP power up (waits for OTP_PD_ACT = 0)
      */
      Status = WaitsForOtpPowerUp(PmicId);
      if (EFI_ERROR (Status)){
        DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate WaitsForOtpPowerUp Status:%r\n", Status));
        return Status;
      }

      /*
      [Step #5] AP writes 1 to the OTP_WR_TRIGGER flag
      */
      Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMCTRL1_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Data8 |= B_CASTRO_COVE_NVMCTRL1_REG_OTP_WR_TRIGGER;
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVMCTRL1_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }

      /*
      [Step #6] NVM controller recognize rising edge of OTP_WR_TRIGGER flag issued in step#5 and sets
      OTP_WR_ONGOING flag to one before section transfer begins from the buffer registers to the OTP
      (assuming OTP power recovery is finished)
      This step can not be verified in AP side due to the OTP_WR_ONGOING flag gets reset to 0 immediately which indicates the transfer is done.
      Recommendation is AP should poll this flag until it become 0 or timeout error occurs
      */
      Status = WaitsForProgrammingOtpMemoryBegin(PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate [Step #6] Status - %r\n", Status));
        return Status;
      }

      /*
      [Step #7] AP waits for a NVM interrupt indicating that the NVM controller has finished the section write procedure
      (AP waits for NVMVIRQ_REG.WRFIN interrupt). (Alternatively, the AP could wait for predefined period of
      time sufficient enough for NVM controller to write complete section in the case that NVM interrupt is
      masked.)
      */
      Status = WaitsForNvmSectionWriteCompletion(PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate [Step #7] Status - %r\n", Status));
        return Status;
      }

      /*
      [Step #8] AP starts NVM interrupt procedure in order to check if error condition is detected
      */
      // No error handling suppport added other than clear the IRQ register
      Status = CastroCovePmicRead8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, &Data8, PmicId);
      if (EFI_ERROR (Status)) {
        return Status;
      }
      Status = CastroCovePmicWrite8 (R_CASTRO_COVE_LVL2_NVM_IRQ_REG, Data8, PmicId);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "Could not access the NVM Controller Interrupt Register, Status: %r\n", Status));
        return Status;
      }

      /*
      [Step #9] If there is additional sections to write, the AP returns to step 4. Prerequisite for writing new section by AP
      is that OTP_WR_ONGOING and OTP_WR_TRIGGER flags are set to 0. OTP_WR_MODE_EN has to be set to 1.
      */
      Index = Index + sizeof(CASTRO_COVE_PMIC_NVM_SECTION_HEADER)+SectionLength; //Goto next section for processing
    }
    UpdateFinished = TRUE;
  }
  /*
  [Step #10] Otherwise, if there is no new section to write, AP clears the OTP_WR_MODE_EN flag (writes 0) which
             indicates to NVM controller to turn off the charge pump and finish the OTP write procedure
  */
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMCTRL1_REG, &Data8, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Data8 &= (~B_CASTRO_COVE_NVMCTRL1_REG_OTP_WR_MODE_EN);
  Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVMCTRL1_REG, Data8, PmicId);

  *PayloadFvSize = PayloadFvIndex;

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate End, Status:%r\n", Status));

  return Status;
}

/**
  Calculate the size of the payload to be flashed

  @param[in] Buffer             - Address of the buffer where the Nvm content is stored
  @param[in] Size               - Size in bytes
  @param[in] DesiredPayloadType - Payload type
  @param[out] PayloadSize       - Actual Payload Size in bytes to flash

  @retval EFI_SUCCESS         - Calculates the actual payload data successfully
  @retval Others              - Status depends on IPC operation
**/
EFI_STATUS
EFIAPI
CastroCovePmicCalculateNvmSize (
  IN UINT8               *Buffer,
  IN UINTN                Size,
  IN UINT8                DesiredPayloadType,
  IN OUT UINT16           *PayloadSize,
  IN UINT8                PmicId
  )
{
  EFI_STATUS                           Status;
  UINT8                                *BufferPointer;
  UINT16                               Index;
  CASTRO_COVE_PMIC_NVM_PAYLOAD_HEADER  *PayloadHeader;
  UINT8                                CurrentNvmVersion;
  UINT8                                CurrentCustomizationVersion;
  UINT8                                CurrentPmicRevision;
  UINT16                               Length_Of_Payload = 0;
  UINT16                               Offset;

  DEBUG ((EFI_D_INFO, "CastroCovePmicCalculateNvmSize Begin\n"));

  BufferPointer = Buffer;

  Index = 0 + CASTRO_COVE_PMIC_NVM_FILE_GLOBAL_HEADER_SIZE; // Index starts from 0 (but if any padding added for File Release information then Index starts after the padding data)
  Offset = CASTRO_COVE_PMIC_NVM_FILE_GLOBAL_HEADER_SIZE; // Offset now points to End of Global header (beginning of first payload header)

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMBUILD0_REG, &CurrentNvmVersion, PmicId); // Get latest NVM version from board
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_CUSTVER_REG, &CurrentCustomizationVersion, PmicId); // Get the Latest Customer version from board
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_ID2_REG, &CurrentPmicRevision, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  while (Index < Size) {
    //
    // Calculate starting Offset of Payload data
    //
    Offset = Offset + Length_Of_Payload; // Offset now points to beginning of first payload data header
    PayloadHeader = (CASTRO_COVE_PMIC_NVM_PAYLOAD_HEADER *)(BufferPointer + Offset);
    DEBUG ((EFI_D_INFO, "\n"));
    DEBUG ((EFI_D_INFO, "PayloadHeader->PAYLOAD_TYPE:          0x%02X\n", PayloadHeader->PAYLOAD_TYPE));
    DEBUG ((EFI_D_INFO, "PayloadHeader->ID1_REG:               0x%02X\n", PayloadHeader->ID1_REG));
    DEBUG ((EFI_D_INFO, "PayloadHeader->ID2_REG:               0x%02X\n", PayloadHeader->ID2_REG));
    DEBUG ((EFI_D_INFO, "PayloadHeader->NVMBUILD0_CUSTVER_REG: 0x%02X\n", PayloadHeader->NVMBUILD0_CUSTVER_REG));
    Length_Of_Payload = (UINT16)((PayloadHeader->PAYLOAD_LENGTH_MSB << 8) | (PayloadHeader->PAYLOAD_LENGTH_LSB & 0xff));
    DEBUG ((EFI_D_INFO, "PayloadHeader->PAYLOAD_LENGTH:        0x%04X\n", Length_Of_Payload));
    Offset = Offset + CASTRO_COVE_PAYLOAD_HEADER_SIZE; // Offset now points to beginning of first payload data

    if (PayloadHeader->PAYLOAD_TYPE == INVALID_PAYLOAD) { // If we have browsed through all the payloads, break
      break;
    }
    if ((DesiredPayloadType != PayloadHeader->PAYLOAD_TYPE) || (PayloadHeader->ID1_REG != CASTRO_COVE_PMIC_ID)) { // Skip the update If the PMIC identification register is not WRC
      DEBUG ((EFI_D_INFO, "PMIC identification register is not CastroCove\n"));
      continue;
    }
    if ((DesiredPayloadType == CASTRO_COVE_PAYLOAD_TYPE_NVM) && (PayloadHeader->NVMBUILD0_CUSTVER_REG <= CurrentNvmVersion)) { // Skip the update If current NVM version is lesser than and equal to payload version
      DEBUG ((EFI_D_INFO, "Current NVM version(%x) is greater than the payload version(%x)\n", CurrentNvmVersion, PayloadHeader->NVMBUILD0_CUSTVER_REG));
      continue;
    }
    if ((DesiredPayloadType == CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION) && (PayloadHeader->NVMBUILD0_CUSTVER_REG <= CurrentCustomizationVersion)) { // Skip the update If current Customization version is lesser than and equal to payload version
      DEBUG ((EFI_D_INFO, "Current Customization version(%x) is greater than the payload version(%x)\n", CurrentCustomizationVersion, PayloadHeader->NVMBUILD0_CUSTVER_REG));
      continue;
    }
    if (CurrentPmicRevision != PayloadHeader->ID2_REG) { // Skip the update if slicon stepping ID is not equal
      DEBUG ((EFI_D_INFO, "CastroCove slicon stepping ID is not equal\n"));
      continue;
    }

    // Payload valid for update
    *PayloadSize += Length_Of_Payload;
    Index += Length_Of_Payload;
  }

  return Status;
}


/**
  Read the programmed Nvm file.

  @param[in]                  - Address of the buffer to store the Nvm content
  @param[in]                  - Size in bytes

  @retval Buffer              - Address of the buffer where the Nvm content is stored
  @retval Size                - Size in bytes
**/
EFI_STATUS
EFIAPI
CastroCovePmicReadFromNvm (
  IN OUT UINT8                **Buffer,
  IN OUT UINT16                *Size,
  IN UINT16                    StartAddress,
  IN UINT8                     PmicId
  )
{
  EFI_STATUS                Status;
  UINT8                    *File1Buffer;
  UINT8                    *File2Buffer;
  UINT16                    Index;
  UINT8                     Data8;
  UINT16                    Data16;

  DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm Begin\n"));

  // Allocate memory to max size
  Status = (gBS->AllocatePool) (EfiBootServicesData, CASTRO_COVE_PMIC_NVM_FILE_MAX_SIZE, (VOID **) &File1Buffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm Allocate memory to max size Status - %r\n", Status));
    return Status;
  }
  (gBS->SetMem) (File1Buffer, CASTRO_COVE_PMIC_NVM_FILE_MAX_SIZE, 0);

  // Find out the address of the last used location in the OTP memory
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVM_USAGE_STATL_REG, &Data8, PmicId);
  DEBUG ((EFI_D_INFO, "R_CASTRO_COVE_NVM_USAGE_STATL_REG: 0x%02X\n", Data8));
  if (EFI_ERROR(Status)) {
    return Status;
  }
  Data16 = (UINT8) Data8;

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVM_USAGE_STATH_REG, &Data8, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  DEBUG ((EFI_D_INFO, "Data8: 0x%02X\n", Data8));
  Data8 &= CASTRO_COVE_NVM_USAGE_STATH_REG_MASK;
  DEBUG ((EFI_D_INFO, "R_CASTRO_COVE_NVM_USAGE_STATH_REG: 0x%02X\n", Data8));
  Data16 |= Data8 << 8;

  DEBUG ((EFI_D_INFO, "Last used location in OTP memory (Data16): 0x%04X\n", Data16));

  Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVM_MB_ADDRH_REG, (UINT8) (StartAddress >> 8), PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CastroCovePmicWrite8 (R_CASTRO_COVE_NVM_MB_ADDRL_REG, (UINT8) StartAddress, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = WaitsForOtpPowerUp(PmicId);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm WaitsForOtpPowerUp Status - %r\n", Status));
    return Status;
  }

  // Wait for 320us
  gBS->Stall (320);

  DEBUG ((EFI_D_INFO, "Starting address for Read the Programmed NVM - %x\n", StartAddress));
  DEBUG ((EFI_D_INFO, "Ending address for Read the Programmed NVM  - %x\n", Data16));

  Index = 0;
  while (StartAddress < Data16) {
    Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVM_MB_DATA_REG, &Data8, PmicId);
    if (EFI_ERROR (Status)) {
      return Status;
    }
    File1Buffer[Index++] = Data8;
    StartAddress++;
    // Consecutive reads to the NVM_MB_DATA_REG should have at minimum window of 300 ns in order to give enough time to the NVM controller to obtain the next byte from OTP memory.
    gBS->Stall (1); //1 us
  }

  // Allocate memory to right size
  Status = (gBS->AllocatePool) (EfiBootServicesData, Index, (VOID **) &File2Buffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm Allocate memory to Index:0x%x Status - %r\n", Index, Status));
    return Status;
  }
  (gBS->SetMem) (File2Buffer, Index, 0);

  (gBS->CopyMem) (File2Buffer, File1Buffer, Index);

  (gBS->FreePool)(File1Buffer); //Free max size buffer

  *Buffer = File2Buffer;
  *Size = Index;
  DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm End, Status :%r\n", Status));

  return Status;
}

/**
  Set OTP memory is write-locked

  @param[in]                  - None

  @retval EFI_SUCCESS         - Write bytes to PMIC device successfully
  @retval Others              - Status depends on IPC operation
**/
EFI_STATUS
EFIAPI
CastroCovePmicWriteLock (
  VOID
  )
{
  EFI_STATUS                Status;

  Status = PmicWriteLock ();

  return Status;
}

EFI_STATUS
EFIAPI
CastroCovePmicNvmUpdatefromApp(
  IN UINT8                *Buffer,
  IN UINTN                Size
  )
{
  EFI_STATUS             Status;
  EFI_STATUS             OemNvmStatus;
  UINT16                 NvmUsage;
  UINT8                  Stath;
  UINT8                  Statl;
  UINT16                 TotalPayloadSize;
  UINT8                  *VerifyFvBuffer;
  UINT8                  *VerifyNvmBuffer;
  UINT16                 ActualNvmSize;
  UINT16                 ActualFvSize;
//
// This is dummy(PmicId=0), When we port PmicNvm update EFI application driver to ADL from LKF,
// then we need get PmicId from EFI application driver.
//
  UINT8                  PmicId=0;
  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdatefromApp Begin\n"));
  ActualNvmSize = 0;
  ActualFvSize = 0;
  NvmUsage = 0;
  Stath = 0;
  Statl = 0;

  //
  // Calculate the Payload size to flash
  //
  TotalPayloadSize = 0;
  Status = CastroCovePmicCalculateNvmSize (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_NVM, &TotalPayloadSize, PmicId); // Update the NVM payload data
  if (Status == EFI_SUCCESS && PcdGetBool(PcdCrcPmicCustomizationNvmUpdateEnable)) {
    Status = CastroCovePmicCalculateNvmSize (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION, &TotalPayloadSize, PmicId); // Update the Customization Payload data
  }

  if (TotalPayloadSize == 0) {
    DEBUG((EFI_D_INFO, "CSC: NVM already up to date (or) updating nvm payload size is zero...\n"));
    return EFI_INVALID_PARAMETER;
  }
  //
  //Check the NVM buffer usage, NVMUsage = (high << 8) + low = (STATH_REG << 8) + (STATL_REG)
  //

  Status = CastroCovePmicRead8(R_CASTRO_COVE_NVM_USAGE_STATL_REG, &Statl, PmicId);
  if (EFI_ERROR(Status)) {
    return Status;
  }

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVM_USAGE_STATH_REG, &Stath, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  NvmUsage = (Stath << 8) + Statl;
  DEBUG ((EFI_D_INFO, "CastroCove: Size of NVM Memory Used : 0X%x\n", NvmUsage));
  DEBUG ((EFI_D_INFO, "CastroCove: Size of the Payload to be updated : 0X%x\n", TotalPayloadSize));
  // Check if payload can fit in the available NVM space
  if (CASTRO_COVE_NVM_MEMORY_SIZE <= (NvmUsage + TotalPayloadSize)) {
    DEBUG ((EFI_D_INFO, "CastroCove NVM Memory is Full, Can Not Update the NVM\n"));
    return RETURN_BAD_BUFFER_SIZE;
  }

  //
  // Check the update criteria is met
  //
  Status = CastroCovePmicNvmUpdateCriteriaCheck (Buffer, Size, PmicId);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCove: PmicNvmUpdateCriteriaCheck, Status: %r\n", Status));
    return Status;
  }

  // Allocate memory to Payload size to store the payload from the FV (Firmware volume)
  Status = (gBS->AllocatePool) (EfiBootServicesData, TotalPayloadSize, (VOID **)&VerifyFvBuffer);
  if (EFI_ERROR(Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdatefromApp Allocate memory to Payload size Status - %r\n", Status));
    return Status;
  }
  (gBS->SetMem) (VerifyFvBuffer, TotalPayloadSize, 0);

  //
  // Program the buffer into Nvm
  //
  Status = CastroCovePmicNvmUpdate (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_NVM, PmicId, VerifyFvBuffer, &ActualFvSize); // Update the NVM payload data
  OemNvmStatus = Status;
  if ((Status == EFI_SUCCESS || Status == EFI_ALREADY_STARTED) && PcdGetBool(PcdCrcPmicCustomizationNvmUpdateEnable)) {
    Status = CastroCovePmicNvmUpdate (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION, PmicId, VerifyFvBuffer, &ActualFvSize); // Update the Customization Payload data
  }

  if (EFI_ERROR(Status) && (OemNvmStatus != EFI_SUCCESS)) {
    DEBUG((EFI_D_INFO, "CastroCovePmicNvmUpdate Status - %r\n", Status));
    return Status;
  }

  // Allocate memory to Updated NVM Payload size to store the NVM buffer
  Status = (gBS->AllocatePool) (EfiBootServicesData, TotalPayloadSize, (VOID **)&VerifyNvmBuffer);
  if (EFI_ERROR(Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdatefromApp Allocate memory to Updated NVM Payload size Status - %r\n", Status));
    return Status;
  }
  (gBS->SetMem) (VerifyNvmBuffer, TotalPayloadSize, 0);

  // Read the Updated NVM data from the PMIC NVM buffer
  CastroCovePmicReadFromNvm (&VerifyNvmBuffer, &ActualNvmSize, NvmUsage, PmicId);

  //
  // Compare the Updated NVM data
  //
  if (!((ActualFvSize == ActualNvmSize) && (CompareMem(VerifyFvBuffer, VerifyNvmBuffer, ActualFvSize) == 0))) {
    return EFI_INVALID_PARAMETER;
  }

  (gBS->FreePool) (VerifyFvBuffer);
  (gBS->FreePool) (VerifyNvmBuffer);

  CastroCovePmicWriteLock();
  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdatefromApp End\n"));
  return Status;
}

//
// Castro Cove Pmic Nvm protocol
//
GLOBAL_REMOVE_IF_UNREFERENCED EFI_CASTRO_COVE_PMIC_NVM_PROTOCOL  mCastroCovePmicNvmProtocol = {
  CastroCovePmicRead8,
  CastroCovePmicWrite8,
  CastroCovePmicNvmReadFromFv,
  CastroCovePmicNvmUpdateCriteriaCheck,
  CastroCovePmicNvmUpdate,
  CastroCovePmicNvmUpdatefromApp,
  CastroCovePmicReadFromNvm,
  CastroCovePmicWriteLock
};

/**
CastroCove Pmic Nvm Update Process. This API does have below functionalities:

[1] Install Castro Cove Pmic Nvm protocol
[2] Get the input Nvm file to be programmed and save it in buffer
[3] Check the update criteria is met
[4] Calculate the Payload size to flash
[5] Check if payload can fit in the available NVM space
[6] Program the buffer into Nvm
[7] Compare the Updated NVM data

Note: Call the API if wanted to program the Nvm progrmming now or call it later which depends on program requirements

Returns:

EFI_SUCCESS             Thread can be successfully created
EFI_OUT_OF_RESOURCES    Cannot allocate protocol data structure
EFI_DEVICE_ERROR        Cannot startup the driver
EFI_ALREADY_STARTED     NVM already upto date
RETURN_BAD_BUFFER_SIZE  NVM memory size is full

**/
EFI_STATUS
EFIAPI
CastroCovePmicNvmUpdateProcess(
  UINT8     PmicId,
  UINT16    BoardId
  )
{
  EFI_STATUS             Status;
  EFI_STATUS             OemNvmStatus;
  EFI_HANDLE             Handle;
  UINT8                  *Buffer;
  UINTN                  Size;
  BOOLEAN                UpdateFinished;
  UINT8                  CurrentNvmVersion;
  UINT8                  CurrentCustomizationVersion;
  UINT8                  CurrentPmicRevision;
  UINT8                  Stath;
  UINT8                  Statl;
  UINT16                 NvmUsage;
  UINT16                 TotalPayloadSize;
  UINT8                  *VerifyFvBuffer;
  UINT8                  *VerifyNvmBuffer;
  UINT16                 ActualNvmSize;
  UINT16                 ActualFvSize;
  UINT16                 i = 0;

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdateProcess Start\n"));

  NvmUsage = 0;
  ActualNvmSize = 0;
  ActualFvSize = 0;
  UpdateFinished = FALSE;

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVMBUILD0_REG, &CurrentNvmVersion, PmicId); // Get latest NVM version from board
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_CUSTVER_REG, &CurrentCustomizationVersion, PmicId); // Get the Latest Customer version from board
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_ID2_REG, &CurrentPmicRevision, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  PcdSet8S (CastroCovePmicNvmRevision, CurrentNvmVersion);
  PcdSet8S (CastroCovePmicCustomizedRevision, CurrentCustomizationVersion);

  DEBUG ((EFI_D_INFO, "CurrentNvmVersion:            0x%04X = 0x%02X\n", R_CASTRO_COVE_NVMBUILD0_REG, CurrentNvmVersion));
  DEBUG ((EFI_D_INFO, "CurrentCustomizationVersion:  0x%04X = 0x%02X\n", R_CASTRO_COVE_CUSTVER_REG, CurrentCustomizationVersion));
  DEBUG ((EFI_D_INFO, "CurrentPmicRevision:          0x%04X = 0x%02X\n", R_CASTRO_COVE_ID2_REG, CurrentPmicRevision));


  //
  // Install Castro Cove Pmic Nvm protocol
  // The protocol support is intended for EFI SHELL application
  //
  Handle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
      &Handle,
      &gCastroCovePmicNvmProtocolGuid,
      &mCastroCovePmicNvmProtocol,
      NULL
      );

  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCove: Could not Install the Protocol, Status: %r\n", Status));
    return Status;
  }

  //
  // Get the input Nvm file to be programmed and save it in buffer
  //
  Buffer = NULL;
  Size = 0;
  //
  //BoardID=0x2 is the board,which have one PMIC,
  //BoardID=0x3 is the board,which have 2 PMIC
  //
  if (BoardId == BoardIdAdlMLp5PmicRvp && PmicId == 0x1) {
    Status = CastroCovePmicNvmReadFromFv (&Buffer, &Size, &gCastroCovePmic1NvmFileGuid);
  } else if(BoardId == BoardIdAdlMLp5PmicRvp && PmicId == 0x2) {
    Status = CastroCovePmicNvmReadFromFv (&Buffer, &Size, &gCastroCovePmic2NvmFileGuid);
  } else if (BoardId == BoardIdAdlMLp5Rvp && PmicId == 0x3) {
    Status = CastroCovePmicNvmReadFromFv(&Buffer, &Size, &gCastroCovePmic3NvmFileGuid);
  } else {
    DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdateProcess - CastroCovePmicNvmUpdate not enabled\n"));
    Status = EFI_UNSUPPORTED;
  }

  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCove: Could not Read from FV, Status: %r\n", Status));
    return Status;
  }

  //
  // Check the update criteria is met
  //
  Status = CastroCovePmicNvmUpdateCriteriaCheck (Buffer, Size, PmicId);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCove: PmicNvmUpdateCriteriaCheck, Status: %r\n", Status));
    return Status;
  }

  //
  // Calculate the Payload size to flash
  //
  TotalPayloadSize = 0;
  Status = CastroCovePmicCalculateNvmSize (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_NVM, &TotalPayloadSize, PmicId); // Update the NVM payload data
  if (Status == EFI_SUCCESS && PcdGetBool(PcdCrcPmicCustomizationNvmUpdateEnable)) {
    Status = CastroCovePmicCalculateNvmSize (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION, &TotalPayloadSize, PmicId); // Update the Customization Payload data
  }
  DEBUG ((EFI_D_INFO, "Total PayloadSize to flash = 0x%x\n", TotalPayloadSize));

  if (TotalPayloadSize == 0) {
    DEBUG((EFI_D_INFO, "CSC: NVM already up to date (or) updating nvm payload size is zero...\n"));
    return EFI_INVALID_PARAMETER;
  }
  //
  // Check the NVM buffer usage, NVMUsage = (high << 8) + low = (STATH_REG << 8) + (STATL_REG)
  //
  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVM_USAGE_STATL_REG, &Statl, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = CastroCovePmicRead8 (R_CASTRO_COVE_NVM_USAGE_STATH_REG, &Stath, PmicId);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  NvmUsage = (Stath << 8) + Statl;
  DEBUG ((EFI_D_INFO, "CastroCove: Size of NVM Memory Used : 0X%x\n", NvmUsage));
  DEBUG ((EFI_D_INFO, "CastroCove: Size of the Payload to be updated : 0X%x\n", TotalPayloadSize));

  // Check if payload can fit in the available NVM space
  if (CASTRO_COVE_NVM_MEMORY_SIZE <= (NvmUsage + TotalPayloadSize)) {
    DEBUG ((EFI_D_INFO, "CastroCove: NVM Memory is Full, cannot update the NVM\n"));
    return RETURN_BAD_BUFFER_SIZE;
  }

  // Allocate memory to Payload size to store the payload from the FV (Firmware volume)
  Status = (gBS->AllocatePool) (EfiBootServicesData, TotalPayloadSize, (VOID **)&VerifyFvBuffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm Allocate memory to Payload size Status - %r\n", Status));
    return Status;
  }
  (gBS->SetMem) (VerifyFvBuffer, TotalPayloadSize, 0);

  //
  // Program the buffer into Nvm
  //
  Status = CastroCovePmicNvmUpdate(Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_NVM, PmicId, VerifyFvBuffer, &ActualFvSize); // Update the NVM payload data
  OemNvmStatus = Status;
  if ((Status == EFI_SUCCESS || Status == EFI_ALREADY_STARTED) && PcdGetBool(PcdCrcPmicCustomizationNvmUpdateEnable)) {
    Status = CastroCovePmicNvmUpdate (Buffer, Size, CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION, PmicId, VerifyFvBuffer, &ActualFvSize); // Update the Customization Payload data
  }

  if (EFI_ERROR(Status) && (OemNvmStatus != EFI_SUCCESS)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdate Status - %r\n", Status));
    return Status;
  }

  // Allocate memory to Updated NVM Payload size to store the NVM buffer
  Status = (gBS->AllocatePool) (EfiBootServicesData, TotalPayloadSize, (VOID **)&VerifyNvmBuffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO, "CastroCovePmicReadFromNvm Allocate memory to Updated NVM Payload size Status - %r\n", Status));
    return Status;
  }
  (gBS->SetMem) (VerifyNvmBuffer, TotalPayloadSize, 0);

  // Read the Updated NVM data from the PMIC NVM buffer
  CastroCovePmicReadFromNvm (&VerifyNvmBuffer, &ActualNvmSize, NvmUsage, PmicId);

  DEBUG ((EFI_D_INFO, "Updated FV Payload Size : 0X%x\n", ActualFvSize));
  DEBUG ((EFI_D_INFO, "Updated NVM Paylaod Size : 0X%x\n", ActualNvmSize));

  DEBUG ((EFI_D_INFO, "FV Payload data dump start..\n"));
  for (i = 0; i < ActualFvSize; i++) {
    DEBUG ((EFI_D_INFO, "%02x ", VerifyFvBuffer[i]));
    if ((i % 16) == 0) {
      DEBUG ((EFI_D_INFO, "\n"));
    }
  }
  DEBUG ((EFI_D_INFO, "\nPayload data dump End..\n"));

  DEBUG ((EFI_D_INFO, "NVM Payload data dump start..\n"));
  for (i = 0; i < ActualNvmSize; i++) {
    DEBUG ((EFI_D_INFO, "%02x ", VerifyNvmBuffer[i]));
    if ((i % 16) == 0) {
      DEBUG ((EFI_D_INFO, "\n"));
    }
  }
  DEBUG ((EFI_D_INFO, "\nNVM Payload data dump End..\n"));

  //
  // Compare the Updated NVM data
  //
  if (!((ActualFvSize == ActualNvmSize) && (CompareMem (VerifyFvBuffer, VerifyNvmBuffer, ActualFvSize) == 0))) {
    DEBUG ((EFI_D_INFO, "Updated NVM data is not valid\n"));
    PostCode(0xFFE);
    ASSERT_EFI_ERROR (EFI_VOLUME_CORRUPTED);
  }

  (gBS->FreePool) (VerifyNvmBuffer);
  (gBS->FreePool) (VerifyFvBuffer);

  // Successful PMIC NVM update
  if (!EFI_ERROR (Status)) {
    UpdateFinished = TRUE;
  }

  //
  // Lock Nvm
  //
  if (PcdGetBool (PcdPmicNvmWriteLockEnable)) {
    Status = CastroCovePmicWriteLock ();
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_INFO, "CastroCove: PmicWritelock, Status: %r\n", Status));
      return Status;
    }
  }

  if (UpdateFinished) {
    PcdSetBoolS (CscPmicUpdateDoneCount, TRUE);
  }

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdateProcess End, Status:%r\n", Status));

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
CastroCovePmicNvmEntryPoint (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
/*++

Routine Description:

  Driver Entry point. It will call the CastroCovePmicNvmUpdateProcess API for Update the NVM with valid data.

Arguments:

  ImageHandle             Image handle of the loaded driver
  SystemTable             Pointer to the System Table

Returns:
  @retval EFI_SUCCESS         - NVM updated Successfully
  @retval Others              - Due to PMIC Device error, NVM memory Full , NVM already Up to date or Payload data not valid. 
--*/
{
  EFI_STATUS             Status;
  UINT8                  Dummy = 0;
  UINT8                  PmicId;
  UINT16                 BoardId;

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmEntryPoint Begin\n"));

  // Update CRC PMIC NVM only when Setup option is Enabled.
  if (PcdGetBool(PcdCrcPmicNvmUpdateEnable)) {
    // This API used for Update the CastroCove NVM with valid data.
    //
    BoardId = PcdGet16(PcdBoardId);
    DEBUG((EFI_D_INFO, "BoardId %x\n", BoardId));
    if (BoardId == BoardIdAdlMLp5PmicRvp) {
      for (PmicId = 1; PmicId <= 2; PmicId++) {
        Status = CastroCovePmicNvmUpdateProcess(PmicId, BoardId);
      }
    } else if (BoardId == BoardIdAdlMLp5Rvp) {
      PmicId = 3;
      Status = CastroCovePmicNvmUpdateProcess(PmicId, BoardId);
    }

    if (PcdGetBool (CscPmicUpdateDoneCount)) {
      Status = CastroCovePmicRead8(R_CHIPCTRL_REG, &Dummy, PmicId);
      DEBUG((EFI_D_INFO, "CSC Platform reset read :%r value = \n", Status, Dummy));
      Status = CastroCovePmicWrite8(R_CHIPCTRL_REG, (Dummy | B_CASTRO_COVE_PMIC_PLTRST), PmicId);
      DEBUG((EFI_D_INFO, "CSC Platform reset write :%r\n", Status));
    }
  } else {
    DEBUG ((EFI_D_INFO, "CastroCovePmicNvmUpdateProcess - CastroCovePmicNvmUpdate not enabled\n"));
  }

  DEBUG ((EFI_D_INFO, "CastroCovePmicNvmEntryPoint End, Status:%r\n", Status));

  return EFI_SUCCESS;
}
