/**@file
  This file is for CastroCove Pmic Registers definitions

@copyright
 Copyright (c) 2020 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains an 'Intel Peripheral Driver' and is uniquely
 identified as "Intel Reference Module" and is licensed for Intel
 CPUs and chipsets under the terms of your license agreement with
 Intel or your vendor. This file may be modified by the user, subject
 to additional terms of the license agreement.

@par Specification
**/

#ifndef _CASTRO_COVE_PMIC_REGISTERS_H_
#define _CASTRO_COVE_PMIC_REGISTERS_H_

#define MAX_SILICON_STEPPINGS                           10
#define INVALID_PAYLOAD                                 0
#define CASTRO_COVE_PMIC_NVM_FILE_MAX_SIZE              4*1024 // 4KB
#define CASTRO_COVE_PAYLOAD_TYPE_NVM                    1
#define CASTRO_COVE_PAYLOAD_TYPE_CUSTOMIZATION          2
#define CASTRO_COVE_LATEST_NVM_VERSION_OFFSET           2
#define CASTRO_COVE_LATEST_CUSTOMER_VERSION_OFFSET      3
#define CASTRO_COVE_PAYLOAD_HEADER_SIZE                 6      // Payload Header size
#define CASTRO_COVE_PMIC_NVM_FILE_GLOBAL_HEADER_SIZE    31     // This has the information of File Release information
#define CASTRO_COVE_PMIC_ID                             0xF1   // PMIC Identifiaction data for CRC
#define CASTRO_COVE_NVM_MEMORY_SIZE                     0x1000 // NVM Buffer Size 4KB

#pragma pack(1)

typedef struct _CASTRO_COVE_PMIC_NVM_SECTION_HEADER {
  UINT8 DEVADDR : 7;
  UINT8 MBX : 1;
  UINT16 SECTION_LENGTH : 5;
  UINT16 Type : 1;
  UINT16 RSTID : 4;
  UINT16 PLT_SEL : 3;
  UINT16 Rsvd : 1;
  UINT16 IV : 1;
  UINT16 V : 1;
} CASTRO_COVE_PMIC_NVM_SECTION_HEADER;

typedef struct _CASTRO_COVE_PMIC_NVM_GLOBAL_STEPPING_HEADER {
  UINT8 ID2_REG;        // Identification Register 2 (Vendor ID/Major Revision/Minor Revision)
  UINT8 NVMBUILD0_REG;  // NVM Build LSB Register
  UINT8 CUSTVER_REG;    // NVM Customization Version Register
} CASTRO_COVE_PMIC_NVM_GLOBAL_STEPPING_HEADER;

typedef struct _CASTRO_COVE_PMIC_NVM_GLOBAl_HEADER {
  UINT8 ID1_REG; // Identification Register 1 (Device ID)
  CASTRO_COVE_PMIC_NVM_GLOBAL_STEPPING_HEADER Stepping[MAX_SILICON_STEPPINGS];
} CASTRO_COVE_PMIC_NVM_GLOBAL_HEADER;

typedef struct _CASTRO_COVE_PMIC_NVM_PAYLOAD_HEADER {
  UINT8 PAYLOAD_TYPE;
  UINT8 ID1_REG;
  UINT8 ID2_REG;
  UINT8 NVMBUILD0_CUSTVER_REG;
  UINT8 PAYLOAD_LENGTH_MSB;
  UINT8 PAYLOAD_LENGTH_LSB;
} CASTRO_COVE_PMIC_NVM_PAYLOAD_HEADER;

typedef enum {
  RegOverride = 0,
  RegAnd = 1,
  RegOr = 2,
  RegClear = 3
} PMIC_REG_ACTION;

typedef struct _PMIC_REG_INIT {
  UINT32           Offset;
  PMIC_REG_ACTION  Action;
  UINT8            Mask;
  UINT8            Value;
  UINT32           Delay;
} PMIC_REG_INIT;

#pragma pack()

/**
  - Prefix:
    Definitions beginning with "R_" are registers
    Definitions beginning with "B_" are bits within registers
    Definitions beginning with "V_" are meaningful values within the bits
    Definitions beginning with "S_" are register size
    Definitions beginning with "N_" are the bit position
**/

#define R_CASTRO_COVE_SMPS1_DVS_PS_REG                        0x0000560B // Power State Control Register
#define R_CASTRO_COVE_SMPS3_DVS_PS_REG                        0x0000580B // Power State Control Register
#define R_CASTRO_COVE_SMPS5_DVS_PS_REG                        0x00005A0B // Power State Control Register
#define R_CASTRO_COVE_SMPS6_DVS_PS_REG                        0x00005B0B // Power State Control Register
#define R_CASTRO_COVE_SMPS7_DVS_PS_REG                        0x00005C0B // Power State Control Register
#define R_CASTRO_COVE_SMPS8_DVS_PS_REG                        0x00005D0B // Power State Control Register
#define R_CASTRO_COVE_ID2_REG                                 0x00007902 // Identification Register 2
#define R_CASTRO_COVE_NVMREL_REG                              0x00007904 // NVM Release Register
#define R_CASTRO_COVE_CUSTVER_REG                             0x00007905 // NVM Customization Version Register
#define R_CASTRO_COVE_HARD_LCK1_REG                           0x000079F4 // Hard Locking Register
#define B_CASTRO_COVE_HARD_LCK1_REG_NVM_HLCK                  BIT4       // Lock NVM Settings
                                                                         // 0 : OTP memory is writeable
                                                                         // 1 : OTP memory is write-locked
#define R_CASTRO_COVE_LVL2_NVM_IRQ_REG                        0x00007A32 // NVM Controller Interrupt Register
#define B_CASTRO_COVE_LVL2_NVM_IRQ_REG_WRFIN_IRQ              BIT0       // Write Finished Interrupt
                                                                         // 0 : IRQ not asserted
                                                                         // 1 : IRQ asserted
#define R_CASTRO_COVE_MLVL2_NVM_IRQ_REG                       0x00007A6C // NVM Controller Interrupt Mask Register
#define B_CASTRO_COVE_MLVL2_NVM_IRQ_REG_MWRFIN_IRQ            BIT0       // Write Finished Interrupt Mask
                                                                         // 0 : IRQ unmasked
                                                                         // 1 : IRQ masked
#define R_CASTRO_COVE_CM_TSPAN_CTRL_REG                       0x00007B90 // VR Monitor Time Span Control Register
#define R_CASTRO_COVE_CM_SMPS1_CTRL_REG                       0x00007B91 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS2_CTRL_REG                       0x00007B92 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS3_CTRL_REG                       0x00007B93 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS4_CTRL_REG                       0x00007B94 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS5_CTRL_REG                       0x00007B95 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS6_CTRL_REG                       0x00007B96 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS7_CTRL_REG                       0x00007B97 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS8_CTRL_REG                       0x00007B98 // VR Monitor Control Register
#define R_CASTRO_COVE_CM_SMPS9_CTRL_REG                       0x00007B99 // VR Monitor Control Register
#define R_CASTRO_COVE_NVMBUILD0_REG                           0x00007D03 // NVM Build LSB Register
#define R_CASTRO_COVE_NVM_MB_ADDRH_REG                        0x00007DC9 // OTP Read Mailbox Address Register (High)
#define R_CASTRO_COVE_NVM_MB_ADDRL_REG                        0x00007DCA // OTP Read Mailbox Address Register (Low)
#define R_CASTRO_COVE_NVM_MB_DATA_REG                         0x00007DCC // OTP Read Mailbox Data Register
#define R_CASTRO_COVE_NVMDBUF0_REG                            0x00007DCD // OTP Write Data Buffer Register 0
#define R_CASTRO_COVE_NVMCTRL0_REG                            0x00007DF0 // NVM CTRL Config Register 0
#define B_CASTRO_COVE_NVMCTRL0_REG_PRG_PULSE_WIDTH            (BIT2+BIT1+BIT0) // OTP programming Pulse Width
                                                                               // 0 : T050US
                                                                               // 1 : T100US
                                                                               // 2 : T150US
                                                                               // 3 : T200US
                                                                               // 4 : T250US
                                                                               // 5 : T300US
                                                                               // 6 : T400US
                                                                               // 7 : T500US
#define R_CASTRO_COVE_NVMCTRL1_REG                            0x00007DF1 // NVM CTRL Config Register 1
#define B_CASTRO_COVE_NVMCTRL1_REG_OTP_WR_TRIGGER             BIT1       // OTP programming trigger
                                                                         // 0 : no action
                                                                         // 1 : start OTP memory programming
#define B_CASTRO_COVE_NVMCTRL1_REG_OTP_WR_MODE_EN             BIT0       // OTP Write Mode
                                                                         // 0 : OTP write mode disabled
                                                                         // 1 : OTP write mode enabled
#define R_CASTRO_COVE_OTPMRB0_RD1_REG                         0x00007DF5 // OTP Mode Register B0 Read1
#define B_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_H           (BIT6+BIT5+BIT4)      // OTP Read1 Mode Register B upper nibble
#define N_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_H           4
#define B_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_L           (BIT3+BIT2+BIT1+BIT0) // OTP Read1 Mode Register B lower nibble
#define N_CASTRO_COVE_OTPMRB0_RD1_REG_OTP_RD1_MRB_L           0
#define R_CASTRO_COVE_OTPMRB0_RD2_REG                         0x00007DF6 // OTP Mode Register B0 Read2
#define B_CASTRO_COVE_OTPMRB0_RD2_REG_OTP_RD2_MRB_H           (BIT6+BIT5+BIT4)      // OTP Read2 Mode Register B upper nibble
#define B_CASTRO_COVE_OTPMRB0_RD2_REG_OTP_RD2_MRB_L           (BIT3+BIT2+BIT1+BIT0) // OTP Read2 Mode Register B lower nibble
#define R_CASTRO_COVE_OTPMRB1_REG                             0x00007DF7 // OTP Mode Register B1
#define B_CASTRO_COVE_OTPMRB1_REG_OTP_RD2_MRB14               BIT6       // OTP Read2 Mode Register B Bit 14
                                                                         // 0 : enabled
                                                                         // 1 : disabled
#define R_CASTRO_COVE_NVMSTAT0_REG                            0x00007DFB // NVM Control Status Register 0
#define B_CASTRO_COVE_NVMSTAT0_REG_OTP_PD_ACT                 BIT1       // OTP Memory Status
                                                                         // 0 : OTP memory is powered up
                                                                         // 1 : OTP memory is in power down mode
#define B_CASTRO_COVE_NVMSTAT0_REG_OTP_WR_ONGOING             BIT0       // NVM Controller Writing Status
                                                                         // 0 : Ready to accept a new data section
                                                                         // 1 : Programming OTP memory
#define R_CASTRO_COVE_NVM_USAGE_STATH_REG                     0x00007DFE // OTP Usage Status Register (High)
#define CASTRO_COVE_NVM_USAGE_STATH_REG_MASK                  (BIT4+BIT3+BIT2+BIT1+BIT0)
#define R_CASTRO_COVE_NVM_USAGE_STATL_REG                     0x00007DFF // OTP Usage Status Register (Low)

#define R_CHIPCTRL_REG                                        0x00004F78 // Chip Control Register
#define B_CASTRO_COVE_PMIC_PLTRST                             BIT5       // Initiate Platform Off
                                                                         // 0: Platform reset not triggered
                                                                         // 1: Platform reset triggered
#endif //_CASTRO_COVE_PMIC_REGISTERS_H_
