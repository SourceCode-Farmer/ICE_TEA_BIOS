/**@file
  TSN FV Installation

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/FirmwareBootMediaLib.h>
#include <Library/PeiTsnFvLib.h>
#include <Library/PeiSubRegionLib.h>

/**
  Get Tsn FV base address

  Base address will vary between SPI and UFS boot.

  @return FvBase associated with FvBinary FV.
**/
UINT32
GetFlashTsnFvBase (
  VOID
  )
{
  UINT32 FvBase;

  FvBase = FixedPcdGet32 (PcdFlashFvTsnMacAddressBase);

  DEBUG ((DEBUG_INFO, "GetFvTsnBase - 0x%x  ......\n", FvBase));
  return FvBase;
}

/**
  Install Base and Size Info Ppi for Siip Firmware Volume.

  @retval     EFI_SUCCESS  Always returns success.

**/
EFI_STATUS
EFIAPI
InstallTsnFv (
  VOID
)
{
  UINT32 FlashTsnFvBase;

  FlashTsnFvBase = GetFlashTsnFvBase ();
  // Report and install FV Info Ppi to Pei Core for TSN FV
  PeiServicesInstallFvInfoPpi (
    NULL,
    (VOID *) FlashTsnFvBase,
    PcdGet32 (PcdFlashFvTsnMacAddressSize),
    NULL,
    NULL
    );
  return EFI_SUCCESS;
}

/**
  Build FV Hob for TSN Sub Region so it can be found in DXE

  @retval     EFI_SUCCESS  Always returns success.

**/
EFI_STATUS
EFIAPI
LoadFlashTsnFv (
  VOID
)
{
  UINT32 FlashTsnFvBase;
  UINT32 TsnFvBaseOrig;

  FlashTsnFvBase = GetFlashTsnFvBase ();
  TsnFvBaseOrig = FixedPcdGet32 (PcdFlashFvTsnMacAddressBase);

  BuildFvHob (
    (UINTN) FlashTsnFvBase,
    (UINTN) FixedPcdGet32 (PcdFlashFvTsnMacAddressSize)
  );

  return EFI_SUCCESS;
}

/*
  Reads TSN Sub region.

  Reads TSN sub region data from FV. Returns pointer to data.

  @param[out]  SubRegion Pointer to sub region data.

  @retval     EFI_SUCCESS  Function has completed successfully.
  @retval     Others       TSN region was not found
*/
EFI_STATUS
GetTsnSubRegion (
  OUT TSN_SUB_REGION** SubRegion
  )
{
  EFI_STATUS Status;
  VOID  *TsnSubRegionStart;
  UINTN TsnSubRegionSize;
  UINT32 CRC;

  DEBUG ((DEBUG_INFO, "GetTsnSubRegion() Start\n"));

  // Read Spi Flash
  Status = ReadSectionInformationFromFv (&gTsnMacAddrSectionGuid, EFI_SECTION_RAW, &TsnSubRegionStart, &TsnSubRegionSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "Failed to get TSN Sub Region FV\n"));
    return Status;
  }

  *SubRegion = (TSN_SUB_REGION*) TsnSubRegionStart;

  CRC = CalculateCrc32(*SubRegion, sizeof(PCH_TSN_SUB_REGION_PORT));
  if (CompareMem (&(*SubRegion)->Signature, &CRC, sizeof(UINT32)) != 0) {
    DEBUG ((DEBUG_ERROR, "TSN sub region image loaded is corrupted.\n"));
    return EFI_NOT_FOUND;
  }

  DEBUG ((DEBUG_INFO, "GetTsnSubRegion() End\n"));
  return Status;
}