/** @file
  Tbt PD Retimer Device Library Functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
  - UEFI Specification 2.6, Section 22.3

**/

#include <Library/DebugLib.h>
#include <Protocol/RetimerCapsuleUpdate.h>

#pragma pack (push,1)
//
// Tcss Private Data structure for the TBT PD Retimer Device Mapping
//
typedef struct {
  UINT8                                PdControllerIndex;
  UINT16                               PdToTbtRetimerIndexBitMapping;
  UINT8                                Reserved[13];
}PD_TBT_RETIMER_MAPPING;
#pragma pack (pop)

static  PD_TBT_RETIMER_MAPPING  gPdIndexToTbtRetimerMappingBuffer [MAX_TBT_RETIMER_DEVICE];

extern BOOLEAN  mRetimerCapsuleUpdateNotSupported;
extern BOOLEAN  mRetimerCapsuleUpdateRunning;
extern UINT32   gTotalCountOfPdController;

/**
  Get TBT Pd Retimer Device Data from Tcss Retimer Platform Data PCD.

  @param[in]   RetimerPlatformDataTable  Pointer to Retimer Platform Specific Data table.
  @param[in]   RetimerDataTableSize      Size of Retimer Platform Specific Data table.
  @param[out]  TcssRetimerDeviceInfo     Pointer to buffer where output will be stored.

  @retval  The function return total PD controller count.
**/
UINT32
GetTbtPdRetimerDeviceData (
  OUT TCSS_RETIMER_PROTOCOL_CAPABILITY  *TcssRetimerDeviceInfo,
  IN  RETIMER_PLATFORM_DATA             *RetimerPlatformDataTable,
  IN  UINT32                            RetimerDataTableSize
  )
{
  UINT8    Index;
  UINT8    BufferIndex;
  UINT32   Count;
  BOOLEAN  PdControllerIndexMatchFound;

  DEBUG ((DEBUG_INFO, "\nGetTbtPdRetimerDeviceData: Start\n"));
  if ((gTotalCountOfPdController !=0) && (gTotalCountOfPdController < MAX_TBT_RETIMER_DEVICE)) {
    DEBUG ((DEBUG_INFO, "\n Already Initialized previously with Value of TotalTbtPdControllerCount = %d\n", gTotalCountOfPdController));
    return gTotalCountOfPdController;
  }

  // Initialize Data Default Value
  Count = 0;
  PdControllerIndexMatchFound = FALSE;

  if ((RetimerPlatformDataTable == NULL) || (RetimerDataTableSize == 0)) {
    DEBUG ((DEBUG_ERROR, "\nRetimer Platform Data Table Not Available.\n"));
    return 0;
  }

  // Initialize gPdIndexToTbtRetimerMappingBuffer
  for (BufferIndex = 0; BufferIndex < MAX_TBT_RETIMER_DEVICE; BufferIndex++) {
    gPdIndexToTbtRetimerMappingBuffer[BufferIndex].PdControllerIndex             = INVALID_PD_CONTROLLER_INDEX;
    gPdIndexToTbtRetimerMappingBuffer[BufferIndex].PdToTbtRetimerIndexBitMapping = 0;
  }

  for (Index = 0; Index < RetimerDataTableSize; Index++, RetimerPlatformDataTable++) {
    if (RetimerPlatformDataTable->TbtPdRetimerData.RetimerDevicePresent != TRUE) {
      continue;
    }
    if (RetimerPlatformDataTable->TbtPdRetimerData.PdControllerIndex >= MAX_TBT_RETIMER_DEVICE) {
      // Invalid PD Controller Index
      DEBUG ((DEBUG_ERROR, "\nINVALID PD Controller Index = %d found.\n", RetimerPlatformDataTable->TbtPdRetimerData.PdControllerIndex));
      continue;
    }
    if (RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex >= MAX_TBT_RETIMER_DEVICE) {
      // Invalid PD Controller Index
      DEBUG ((DEBUG_ERROR, "\nINVALID Retimer Device Index = %d found.\n", RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex));
      continue;
    }
    if (Count >= MAX_TBT_RETIMER_DEVICE) {
      // Array Boundry Condition Reached
      DEBUG ((DEBUG_ERROR, "\nBuffer Array Boundry Condition Reached with PD Index = %d.\n Retimer Index =%d\n Invalid Buffer Index = %d", \
             RetimerPlatformDataTable->TbtPdRetimerData.PdControllerIndex, RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex, Count));
      break;
    }
    // Count Tbt Pd Retimer Device Number
    if ((RetimerPlatformDataTable->RetimerType == TbtPdRetimerType) \
           && ((TcssRetimerDeviceInfo->RetimerDeviceIndexBitMap & ( BIT0 << RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex)) == 0) \
      ) {
      // Fill Tbt Pd Retimer Data only for TbtPdRetimerType
      TcssRetimerDeviceInfo->RetimerDeviceIndexBitMap |= ( BIT0 << RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex);
      TcssRetimerDeviceInfo->RetimerDeviceCount++;
    }
    if ((RetimerPlatformDataTable->RetimerType != TbtPdRetimerType) \
        && (RetimerPlatformDataTable->RetimerType != NonTbtI2cRetimerType) \
      ) {
      // Only Count PD Controller Number Data for TbtPdRetimerType and NonTbtI2cRetimerType
      continue;
    }
    // Travel through PD Controller Index Buffer to check if already added
    PdControllerIndexMatchFound = FALSE; // Initialize this variable before every loop start
    for (BufferIndex = 0; BufferIndex < Count; BufferIndex++) {
      if (gPdIndexToTbtRetimerMappingBuffer[BufferIndex].PdControllerIndex == RetimerPlatformDataTable->TbtPdRetimerData.PdControllerIndex) {  // Dulicate Entry Found
          PdControllerIndexMatchFound = TRUE;
          gPdIndexToTbtRetimerMappingBuffer[BufferIndex].PdToTbtRetimerIndexBitMapping |= ( BIT0 << RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex);
          break;
      }
    }
    if (PdControllerIndexMatchFound == FALSE) { // Reached to end and no PD Controller Index Entry matched
      gPdIndexToTbtRetimerMappingBuffer[Count].PdControllerIndex             = RetimerPlatformDataTable->TbtPdRetimerData.PdControllerIndex; // Add Entry
      DEBUG ((DEBUG_INFO, "\nNew PD Controller Index = %d found.\n", RetimerPlatformDataTable->TbtPdRetimerData.PdControllerIndex));
      gPdIndexToTbtRetimerMappingBuffer[Count].PdToTbtRetimerIndexBitMapping = ( BIT0 << RetimerPlatformDataTable->TbtPdRetimerData.RetimerDeviceIndex); // 1st Entry of BIT MAPPING
      Count++;
    }
  }
  DEBUG ((DEBUG_INFO, "\nTbt PD Controller Final Count = %d\n", Count));
  DEBUG ((DEBUG_INFO, "\nTbt PD Retimer Device Final Count = %d\n Retimer Device BIT Map = %X\n",\
                                TcssRetimerDeviceInfo->RetimerDeviceCount, TcssRetimerDeviceInfo->RetimerDeviceIndexBitMap));
  DEBUG ((DEBUG_INFO, "\nGetTbtPdRetimerDeviceData: End.\n"));
  return Count;
}

/**
  Change Tbt Retimer PD controller Fw Update Mode.

  @param[in]  PdControllerMode     Mode of PD Controller to SET.
  @param[in]  RetimerDeviceNumber  Number or Index of Retimer Device.

  @retval  EFI_SUCCESS            Successfully Mode is Changed to FW Update Mode.
  @retval  EFI_INVALID_PARAMETER  Invalid GUID from ESRT Table is Passed.
  @retval  EFI_UNSUPPORTED        This driver does not support.
  @retval  EFI_DEVICE_ERROR       This driver cannot be started due to device Error.
  @retval  EFI_ALREADY_STARTED    This driver has been started. This is applicable for PD Fw Update Mode Entry.
  @retval  EFI_NOT_STARTED        This driver has not been started. This is applicable for PD Fw Update Mode Exit.
  @retval  EFI_TIMEOUT            Mode Change Command timeout Happen.
**/
EFI_STATUS
TbtPdRetimerFwUpdateModeChange (
  IN  UINT8  PdControllerMode,
  IN  UINT8  RetimerDeviceNumber
  )
{
  EFI_STATUS  Status;
  UINT8       DeviceMode;
  CHAR16      *String = NULL;
  UINT8       PdControllerNumber;
  UINT8       Count;

  String = ((PdControllerMode == RetimerFirmWareUpdateEnableMode) ? L"Enable" : L"Disable" );
  PdControllerNumber  = INVALID_PD_CONTROLLER_INDEX;

  // Check if PD Controller available in board or not.
  if (gTotalCountOfPdController == 0) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "\nTbtPdRetimerFwUpdateMode.%S: Retimer Capsule Update is not supported as no PD Controller available. Returning with Status = %r\n", String, Status));
    return Status;
  }

  if (RetimerDeviceNumber == ALL_RETIMER_DEVICE) {
    PdControllerNumber = ALL_PD_CONTROLLER;
  } else {
    // Get PD Controller Number from gPdIndexToTbtRetimerMappingBuffer
    //
    // Travers through PdIndexToTbtRetimerMappingBuffer to find matching Pd Controller Number
    // PAD number for I2cRetimerSlaveDeviceIndex
    //
    for (Count = 0; Count < gTotalCountOfPdController; Count++) {
      // Send EC Command to Set Non Tbt I2c Retimer Device Mode
      if (((gPdIndexToTbtRetimerMappingBuffer[Count].PdToTbtRetimerIndexBitMapping & (BIT0 << RetimerDeviceNumber)) != 0)) {
        PdControllerNumber = gPdIndexToTbtRetimerMappingBuffer[Count].PdControllerIndex;
        break;
      }
    }
  }
  if (PdControllerNumber == INVALID_PD_CONTROLLER_INDEX) {
    return EFI_INVALID_PARAMETER;
  }

  // Check the PD Controller Mode
  DeviceMode = INVALID_DEVICE_MODE; // Initialize DeviceMode with Invalid Data before getting PD Controller Mode
  Status = GetPdControllerMode (&DeviceMode, PdControllerNumber, gTotalCountOfPdController);
  // Check if any ERROR status on Get Mode
  if (EFI_ERROR(Status)) {
    DEBUG ((DEBUG_ERROR, "\nTbtPdRetimerFwUpdateMode.%S: PD Controller GET MODE Not Responding. Returning with Status = %r\n", String, Status));
    return Status;
  }
  // Check if PD controller is in or not in FW Update mode already.
  if (DeviceMode == PdControllerMode) {
    if (PdControllerMode == RetimerFirmWareUpdateEnableMode) {
      DEBUG ((DEBUG_INFO, "\nTbtPdRetimerFwUpdateMode.Drive: PD Controller is already in FW Update Mode\n"));
      Status = EFI_ALREADY_STARTED;
      // Now PD Controller is In FW Update Mode.
      mRetimerCapsuleUpdateRunning = TRUE;
    }
    if (PdControllerMode == RetimerFirmWareUpdateDisableMode) {
      DEBUG ((DEBUG_INFO, "\nTbtPdRetimerFwUpdateMode.Restore: PD Controller is already exited FW Update Mode.\n"));
      Status = EFI_NOT_STARTED;
      // Now PD Controller already EXITED From PD Update Mode.
      mRetimerCapsuleUpdateRunning = FALSE;
    }
    return Status;
  }
  // Send command to EC instruct to PD Controller to either enter or exit FW Update Mode
//[-start-211109-BAIN000054-add]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT)||defined(S77014IAH_SUPPORT)
  PdControllerMode = 0x03;
#endif
//[-end-211109-BAIN000054-add]//
  Status = SetPdControllerMode (PdControllerMode, PdControllerNumber);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "\nTbtPdRetimerFwUpdateMode.%S: PD Controller SET MODE Not Responding. Returning with Status = %r\n", String, Status));
    return Status;
  }
  DeviceMode = INVALID_DEVICE_MODE; // Initialize DeviceMode with Invalid Data before getting PD Controller Mode
  Status = GetPdControllerMode (&DeviceMode, PdControllerNumber, gTotalCountOfPdController);
  // Check if any ERROR status on Get Mode
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "\nTbtPdRetimerFwUpdateMode.%S: PD Controller GET MODE Not Responding. Returning with Status = %r\n", String, Status));
    return Status;
  }
  // Check if any ERROR status on Set Mode
  if (DeviceMode != PdControllerMode) {
    Status = EFI_TIMEOUT;
    DEBUG ((DEBUG_ERROR, "\nTbtPdRetimerFwUpdateMode.%S: PD Controller SET MODE is not Completed. Returning with Status = %r\n", String, Status));
    return Status;
  }

  if (PdControllerMode == RetimerFirmWareUpdateEnableMode) {
    // Now PD Controller Succesfully Enter Into FW Update Mode.
    mRetimerCapsuleUpdateRunning = TRUE;
    DEBUG ((DEBUG_INFO, "\nTbtPdRetimerFwUpdateMode.Drive: "));
    DEBUG ((DEBUG_INFO, "PD Controller Enter Into FW Update Mode with Status = %r\n", Status));
  }
  if (PdControllerMode == RetimerFirmWareUpdateDisableMode) {
    // Now PD Controller Succesfully EXIT From FW Update Mode.
    mRetimerCapsuleUpdateRunning = FALSE;
    DEBUG ((DEBUG_INFO, "\nTbtPdRetimerFwUpdateMode.Restore: "));
    DEBUG ((DEBUG_INFO, "PD Controller Exit from FW Update Mode with Status = %r\n", Status));
  }

  return EFI_SUCCESS;
}
