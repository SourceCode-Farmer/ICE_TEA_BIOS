/** @file
  Publish TCSS_RETIMER_PROTOCOL.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
  - UEFI Specification 2.6, Section 22.3

**/

#include <Protocol/RetimerCapsuleUpdate.h>

// Global Variable Defination
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN    mRetimerCapsuleUpdateNotSupported     = FALSE;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN    mRetimerCapsuleUpdateRunning          = FALSE;
GLOBAL_REMOVE_IF_UNREFERENCED UINT32     gTotalCountOfPdController             = 0; // For storing Total number of PD controller available on Board.
//GLOBAL_REMOVE_IF_UNREFERENCED UINT32     gTotalCountOfI2cRetimerForcePowerGpio = 0; // For storing Total number of Force Power GPIO Available for I2C Retimer on Board.
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN    mInitDoneFlag                         = FALSE;
GLOBAL_REMOVE_IF_UNREFERENCED TCSS_RETIMER_PROTOCOL             mTcssRetimerProtocol;
GLOBAL_REMOVE_IF_UNREFERENCED TCSS_RETIMER_PROTOCOL_CAPABILITY  mTcssRetimerDeviceInfo[MaxRetimerDeviceType];

// Function Declaration
/**
  Get TBT Pd Retimer Device Data from Tcss Retimer Platform Data PCD.

  @param[in]   RetimerPlatformDataTable  Pointer to Retimer Platform Specific Data table.
  @param[in]   RetimerDataTableSize      Size of Retimer Platform Specific Data table.
  @param[out]  TcssRetimerDeviceInfo     Pointer to buffer where output will be stored.

  @retval  The function return total PD controlle count.
**/
UINT32
GetTbtPdRetimerDeviceData (
  OUT TCSS_RETIMER_PROTOCOL_CAPABILITY  *TcssRetimerDeviceInfo,
  IN  RETIMER_PLATFORM_DATA             *RetimerPlatformDataTable,
  IN  UINT32                            RetimerDataTableSize
  );

/**
  Get Non TBT I2c Retimer Device Data from Tcss Retimer Platform Data Pcd.

  @param[in]   RetimerPlatformDataTable  Pointer to Retimer Platform Specific Data table.
  @param[in]   RetimerDataTableSize      Size of Retimer Platform Specific Data table.
  @param[out]  TcssRetimerDeviceInfo     Pointer to buffer where output will be stored.

  @retval  The function returns total count of Non TBT I2C Retimer Force Power GPIO.
**/
/*
UINT32
GetNonTbtI2cRetimerDeviceData (
  OUT TCSS_RETIMER_PROTOCOL_CAPABILITY    *TcssRetimerDeviceInfo,
  IN  RETIMER_PLATFORM_DATA               *RetimerPlatformDataTable,
  IN  UINT32                              RetimerDataTableSize
  );
*/
/**
  Change Non TBT I2C Retimer FW update mode.

  @param[in]  I2cRetimerDeviceMode        Mode of I2c Retimer Device to SET.
  @param[in]  I2cRetimerSlaveDeviceIndex  Number or Index of I2c Retimer Peripheral Device.

  @retval  EFI_SUCCESS            Successfully Mode is Changed to Retimer FW Update Mode.
  @retval  EFI_INVALID_PARAMETER  Invalid GUID from ESRT Table is Passed.
  @retval  EFI_UNSUPPORTED        This driver does not support.
  @retval  EFI_DEVICE_ERROR       This driver cannot be started due to device Error.
  @retval  EFI_ALREADY_STARTED    This driver has been started. This is applicable for PD FwUpdate Mode Entry.
  @retval  EFI_NOT_STARTED        This driver has not been started. This is applicable for PD FwUpdate Mode Exit.
  @retval  EFI_TIMEOUT            Mode Change Command timeout Happen.
**/
/*
EFI_STATUS
NonTbtI2cRetimerFwUpdateModeChange (
  IN  UINT8  I2cRetimerDeviceMode,
  IN  UINT8  I2cRetimerSlaveDeviceIndex
  );
*/

/**
  Change Tbt Retimer PD controller Fw Update Mode.

  @param[in]  PdControllerMode    Mode of PD Controller to SET.
  @param[in]  PdControllerNumber  Number or Index of PD Controller.

  @retval  EFI_SUCCESS            Successfully Mode is Changed to ALT TBT Mode.
  @retval  EFI_INVALID_PARAMETER  Invalid GUID from ESRT Table is Passed.
  @retval  EFI_UNSUPPORTED        This driver does not support.
  @retval  EFI_DEVICE_ERROR       This driver cannot be started due to device Error.
  @retval  EFI_ALREADY_STARTED    This driver has been started. This is applicable for PD FwUpdate Mode Entry.
  @retval  EFI_NOT_STARTED        This driver has not been started. This is applicable for PD FwUpdate Mode Exit.
  @retval  EFI_TIMEOUT            Mode Change Command timeout Happen.
**/
EFI_STATUS
TbtPdRetimerFwUpdateModeChange (
  IN  UINT8  PdControllerMode,
  IN  UINT8  RetimerDeviceNumber
  );

/**
  Get PD controller count and Force Power GPIO count from Retimer Platform Specific Data table.

  @retval  EFI_SUCCESS    Successfully get data from Retimer data table.
  @retval  EFI_NOT_READY  No Retimer data table found.
**/
EFI_STATUS
GetValidateData (
  )
{
  UINT8                  Index;
  RETIMER_PLATFORM_DATA  *RetimerPlatformDataTable;
  UINT32                 RetimerDataTableSize;

  RetimerPlatformDataTable = (RETIMER_PLATFORM_DATA*) PcdGet64 (PcdBoardRetimerDataTablePtr);
  RetimerDataTableSize     = PcdGet32 (PcdBoardRetimerDataTableEntryCount);

  if ((RetimerPlatformDataTable == 0) || (RetimerDataTableSize == 0)) {
    mInitDoneFlag = FALSE;
    return EFI_NOT_READY;
  }

  // Initialize Global Variable
  for (Index = 0; Index < MaxRetimerDeviceType; Index++) {
    mTcssRetimerDeviceInfo[Index].RetimerType              = Index;
    mTcssRetimerDeviceInfo[Index].RetimerDeviceCount       = 0;
    mTcssRetimerDeviceInfo[Index].RetimerDeviceIndexBitMap = 0;
  }

  gTotalCountOfPdController  = GetTbtPdRetimerDeviceData (
                                 &mTcssRetimerDeviceInfo[TbtPdRetimerType],
                                 RetimerPlatformDataTable,
                                 RetimerDataTableSize
                                 );
/*  gTotalCountOfI2cRetimerForcePowerGpio = GetNonTbtI2cRetimerDeviceData (
                                            &mTcssRetimerDeviceInfo[NonTbtI2cRetimerType],
                                            RetimerPlatformDataTable,
                                            RetimerDataTableSize
                                            ); */
  mInitDoneFlag = TRUE;

  return EFI_SUCCESS;
}

/**
  Get TCSS Retimer Controller info and Capability.

  @param[in]   This         The TCSS RETIMER PROTOCOL Instance.
  @param[in]   RetimerGuid  GUID from ESRT ACPI Table.
  @param[out]  RetimerInfo  The Pointer to Output Buffer.

  @retval  EFI_SUCCESS            Successfully Get Info of Retimer Controller.
  @retval  EFI_INVALID_PARAMETER  Invalid GUID from ESRT Table is Passed.
  @retval  EFI_NOT_READY          Board Retimer PCD is not ready or Not Available.
  @retval  EFI_UNSUPPORTED        This driver does not support.
**/
EFI_STATUS
EFIAPI
GetRetimerInfo (
  IN TCSS_RETIMER_PROTOCOL              *This,
  IN EFI_GUID                           RetimerGuid, ///< GUID from ESRT ACPI Table
  OUT TCSS_RETIMER_PROTOCOL_CAPABILITY  *RetimerInfo OPTIONAL
  )
{
  EFI_STATUS    Status;
  BOOLEAN       ValidGuidFound;
  RETIMER_TYPE  RetimerType;
  UINT8         RetimerDeviceIndex;

  RetimerDeviceIndex = INVALID_RETIMER_INDEX;     // Initialize Retimer Device Index with Invalid Data
  RetimerType        = InvalidRetimerDeviceType;  // Initialize Retimer Type with Invalid Data
  ValidGuidFound     = NO_VALID_GUID_FOUND;
  Status             = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Info: Start\n"));

  if (mInitDoneFlag == FALSE) {
    Status = GetValidateData();
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Info: Return with Status = %r\n", Status));

    if (EFI_ERROR(Status)) {
      return EFI_NOT_STARTED;
    }
  }

  // Check if Valid Guid is passed or Not for TBT Retimer Device.
  if ((CompareGuid(&RetimerGuid, &gAllTbtRetimerDeviceGuid)) \
          && (mTcssRetimerDeviceInfo[TbtPdRetimerType].RetimerDeviceCount != 0) \
    ) {
    RetimerDeviceIndex = ALL_RETIMER_DEVICE;
    ValidGuidFound     = TBT_RETIMER_VALID_GUID_FOUND;
    RetimerType        = TbtPdRetimerType;
  }

  // Check if Valid Guid is passed or Not for Non-TBT I2C Retimer Device.
/*
  if ((CompareGuid(&RetimerGuid, &gAllNonTbtI2cRetimerDeviceGuid)) \
          && (mTcssRetimerDeviceInfo[NonTbtI2cRetimerType].RetimerDeviceCount != 0) \
    ) {
    RetimerDeviceIndex  = ALL_RETIMER_DEVICE;
    ValidGuidFound      = NON_TBT_I2C_RETIMER_VALID_GUID_FOUND | TBT_RETIMER_VALID_GUID_FOUND;
    RetimerType         = NonTbtI2cRetimerType;
  }
*/

  if ((ValidGuidFound == NO_VALID_GUID_FOUND) || (RetimerType == InvalidRetimerDeviceType)) {
    Status = EFI_INVALID_PARAMETER;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Info: Invalid Retimer Guid Passed for Capsule Update. Returning with Status = %r\n", Status));
    return Status;
  }

  // Check if Retimer Capsule Update is not supported
  if (mRetimerCapsuleUpdateNotSupported) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Info: Retimer Capsule Update is not supported. Returning with Status = %r\n", Status));
    return Status;
  }

  if (RetimerInfo != NULL) {
    // Save Data to RetimerInfo Return Buffer
    RetimerInfo->RetimerType = mTcssRetimerDeviceInfo[RetimerType].RetimerType;
    if (RetimerDeviceIndex  == ALL_I2C_RETIMER_SLAVE_DEVICE) {
      RetimerInfo->RetimerDeviceCount       = mTcssRetimerDeviceInfo[RetimerType].RetimerDeviceCount;
      RetimerInfo->RetimerDeviceIndexBitMap = mTcssRetimerDeviceInfo[RetimerType].RetimerDeviceIndexBitMap;
    } else {
      RetimerInfo->RetimerDeviceIndexBitMap = (mTcssRetimerDeviceInfo[RetimerType].RetimerDeviceIndexBitMap & (BIT0 << RetimerDeviceIndex));
      RetimerInfo->RetimerDeviceCount       = (RetimerInfo->RetimerDeviceIndexBitMap? 1:0);
    }
  }

  if (RetimerDeviceIndex  == ALL_I2C_RETIMER_SLAVE_DEVICE) {
    if(mTcssRetimerDeviceInfo[RetimerType].RetimerDeviceCount == 0) {
      Status = EFI_INVALID_PARAMETER;
    }
  } else {
    if((mTcssRetimerDeviceInfo[RetimerType].RetimerDeviceIndexBitMap & (BIT0 << RetimerDeviceIndex)) == 0) {
      Status = EFI_INVALID_PARAMETER;
    }
  }

  DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Info: End\n"));
  return Status;
}

/**
  Drive TCSS Retimer Controller into FW Update Mode.

  @param[in]  This         The TCSS RETIMER PROTOCOL Instance.
  @param[in]  RetimerGuid  GUID from ESRT ACPI Table.

  @retval  EFI_SUCCESS            Successfully Mode is Changed to FW Update Mode.
  @retval  EFI_INVALID_PARAMETER  Invalid GUID from ESRT Table is Passed.
  @retval  EFI_UNSUPPORTED        This driver does not support.
  @retval  EFI_DEVICE_ERROR       This driver cannot be started due to device Error.
  @retval  EFI_ALREADY_STARTED    This driver has been started.
  @retval  EFI_TIMEOUT            DriveToFwUpdateMode Command timeout Happen.
  @retval  EFI_NOT_READY          Board Retimer PCD is not ready or Not Available.
**/
EFI_STATUS
EFIAPI
DriveToFwUpdateMode (
  IN  TCSS_RETIMER_PROTOCOL  *This,
  IN  EFI_GUID               RetimerGuid ///< GUID from ESRT ACPI Table
  )
{
  EFI_STATUS  Status;
  BOOLEAN     ValidGuidFound;
  UINT8       RetimerDeviceIndex;

  RetimerDeviceIndex = INVALID_RETIMER_INDEX; // Initialize Retimer Device Index with Invalid Data
  ValidGuidFound     = NO_VALID_GUID_FOUND;
  Status             = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Drive: Start\n"));
  DEBUG ((DEBUG_ERROR, "  RetimerGuid = %g\n", RetimerGuid));

  if (mInitDoneFlag == FALSE) {
    Status = GetValidateData();
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Drive: Return with Status = %r\n", Status));

    if (EFI_ERROR(Status)) {
      return EFI_NOT_STARTED;
    }
  }

  // Check if Valid Guid is passed or Not for TBT Retimer Device.
  if ((CompareGuid(&RetimerGuid, &gAllTbtRetimerDeviceGuid)) \
          && (mTcssRetimerDeviceInfo[TbtPdRetimerType].RetimerDeviceCount != 0) \
    ) {
    RetimerDeviceIndex = ALL_RETIMER_DEVICE;
    if ((RetimerDeviceIndex == ALL_RETIMER_DEVICE) || ((mTcssRetimerDeviceInfo[TbtPdRetimerType].RetimerDeviceIndexBitMap & (BIT0 << RetimerDeviceIndex)) != 0)) {
      ValidGuidFound = TBT_RETIMER_VALID_GUID_FOUND;
    }
  }
/*
  // Check if Valid Guid is passed or Not for Non-TBT I2C Retimer Device.
  if ((CompareGuid(&RetimerGuid, &gAllNonTbtI2cRetimerDeviceGuid)) \
          && (mTcssRetimerDeviceInfo[NonTbtI2cRetimerType].RetimerDeviceCount != 0) \
    ) {
    RetimerDeviceIndex = ALL_RETIMER_DEVICE;
    if ((RetimerDeviceIndex == ALL_RETIMER_DEVICE) || ((mTcssRetimerDeviceInfo[NonTbtI2cRetimerType].RetimerDeviceIndexBitMap & (BIT0 << RetimerDeviceIndex)) != 0)) {
      ValidGuidFound = NON_TBT_I2C_RETIMER_VALID_GUID_FOUND | TBT_RETIMER_VALID_GUID_FOUND;
    }
  }
*/

  if (ValidGuidFound == NO_VALID_GUID_FOUND) {
    Status = EFI_INVALID_PARAMETER;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Drive: Invalid Retimer Guid Passed for Capsule Update. Returning with Status = %r\n", Status));
    return Status;
  }

  // Check if Retimer Capsule Update is not supported
  if (mRetimerCapsuleUpdateNotSupported) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Drive: Retimer Capsule Update is not supported. Returning with Status = %r\n", Status));
    return Status;
  }

  // Check if Retimer Capsule Update is already Running. Or Device Mode is already Saved.
  if (mRetimerCapsuleUpdateRunning) {
    Status = EFI_ALREADY_STARTED;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Drive: Retimer Capsule Update is already Running. Returning with Status = %r\n", Status));
    return Status;
  }
/*
  if ((ValidGuidFound & NON_TBT_I2C_RETIMER_VALID_GUID_FOUND) == NON_TBT_I2C_RETIMER_VALID_GUID_FOUND) {
    // Change the I2C Retimer Peripheral Device Mode
    Status = NonTbtI2cRetimerFwUpdateModeChange (RetimerFirmWareUpdateEnableMode, RetimerDeviceIndex);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Drive: Failed to Enable Retimer FW Update Mode with Status = %r\n", Status));
      return Status;
    }
  }
*/
  if ((ValidGuidFound & TBT_RETIMER_VALID_GUID_FOUND) == TBT_RETIMER_VALID_GUID_FOUND) {
    // Change the PD Controller Mode
    Status = TbtPdRetimerFwUpdateModeChange (RetimerFirmWareUpdateEnableMode, RetimerDeviceIndex);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Drive: Failed to Enable Retimer FW Update Mode with Status = %r\n", Status));
      return Status;
    }
  }

  DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Drive: End\n"));
  return Status;
}

/**
  Restore TBT PD Controller into original mode.

  @param[in]  This         The TCSS RETIMER PROTOCOL Instance.
  @param[in]  RetimerGuid  GUID from ESRT ACPI Table.

  @retval  EFI_SUCCESS            Successfully Mode is Restore.
  @retval  EFI_INVALID_PARAMETER  Invalid GUID from ESRT Table is Passed.
  @retval  EFI_UNSUPPORTED        This driver does not support.
  @retval  EFI_DEVICE_ERROR       This driver cannot be started due to device Error.
  @retval  EFI_NOT_STARTED        This driver has not been started.
  @retval  EFI_TIMEOUT            RestoreToOriginalMode Command timeout Happen.
  @retval  EFI_NOT_READY          Board Retimer PCD is not ready or Not Available.
**/
EFI_STATUS
EFIAPI
RestoreToOriginalMode (
  IN TCSS_RETIMER_PROTOCOL  *This,
  IN EFI_GUID               RetimerGuid ///< GUID from ESRT ACPI Table
  )
{
  EFI_STATUS  Status;
  UINT8       ValidGuidFound;
  UINT8       RetimerDeviceIndex;

  RetimerDeviceIndex = INVALID_RETIMER_INDEX; // Initialize Retimer Device Index with Invalid Data
  ValidGuidFound     = NO_VALID_GUID_FOUND;
  Status             = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Restore: Start\n"));

  if (mInitDoneFlag == FALSE) {
    Status = GetValidateData();
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Restore: Return with Status = %r\n", Status));
    if (EFI_ERROR(Status)) {
      return EFI_NOT_STARTED;
    }
  }

  // Check if Valid Guid is passed or Not.
  if ((CompareGuid(&RetimerGuid, &gAllTbtRetimerDeviceGuid)) \
          && (mTcssRetimerDeviceInfo[TbtPdRetimerType].RetimerDeviceCount != 0) \
    ) {
    RetimerDeviceIndex  = ALL_RETIMER_DEVICE;
    if ((RetimerDeviceIndex == ALL_RETIMER_DEVICE) || ((mTcssRetimerDeviceInfo[TbtPdRetimerType].RetimerDeviceIndexBitMap & (BIT0 << RetimerDeviceIndex)) != 0)) {
      ValidGuidFound      = TBT_RETIMER_VALID_GUID_FOUND;
    }
  }

  // Check if Valid Guid is passed or Not for Non-TBT I2C Retimer Device.
/*
  if ((CompareGuid(&RetimerGuid, &gAllNonTbtI2cRetimerDeviceGuid)) \
          && (mTcssRetimerDeviceInfo[NonTbtI2cRetimerType].RetimerDeviceCount != 0) \
    ) {
    RetimerDeviceIndex     = ALL_RETIMER_DEVICE;
    if ((RetimerDeviceIndex == ALL_RETIMER_DEVICE) || ((mTcssRetimerDeviceInfo[NonTbtI2cRetimerType].RetimerDeviceIndexBitMap & (BIT0 << RetimerDeviceIndex)) != 0)) {
      ValidGuidFound         = NON_TBT_I2C_RETIMER_VALID_GUID_FOUND | TBT_RETIMER_VALID_GUID_FOUND;
    }
  }
*/

  if (ValidGuidFound == NO_VALID_GUID_FOUND) {
    Status = EFI_INVALID_PARAMETER;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Restore: Invalid Retimer Guid Passed for Capsule Update. Returning with Status = %r\n", Status));
    return Status;
  }

  // Check if Retimer Capsule Update is not supported
  if (mRetimerCapsuleUpdateNotSupported) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Restore: Retimer Capsule Update is not supported. Returning with Status = %r\n", Status));
    return Status;
  }

  // Check if Retimer Capsule Update is already Running. Or Device Mode is already Saved.
  if (!mRetimerCapsuleUpdateRunning) {
    DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Restore: Drive FW Update Mode for PD controller was never called.\n"));
    Status = EFI_NOT_STARTED;
    return Status;
  }

  if ((ValidGuidFound & TBT_RETIMER_VALID_GUID_FOUND) == TBT_RETIMER_VALID_GUID_FOUND) {
    // Change the PD Controller Mode
    Status = TbtPdRetimerFwUpdateModeChange (RetimerFirmWareUpdateDisableMode, RetimerDeviceIndex);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Restore: Failed to Exit Retimer FW Update Mode For PD Controller with Status = %r\n", Status));
      return Status;
    }
  }
/*
  if ((ValidGuidFound & NON_TBT_I2C_RETIMER_VALID_GUID_FOUND) == NON_TBT_I2C_RETIMER_VALID_GUID_FOUND) {
    // Change the I2C Retimer Peripheral Device Mode
    Status = NonTbtI2cRetimerFwUpdateModeChange (RetimerFirmWareUpdateDisableMode, RetimerDeviceIndex);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "\nTCSS_RETIMER_PROTOCOL.Restore: Failed to Exit Retimer FW Update Mode For I2c Retimer Device with Status = %r\n", Status));
      return Status;
    }
  }
*/
  DEBUG ((DEBUG_INFO, "\nTCSS_RETIMER_PROTOCOL.Restore: End\n"));
  return Status;
}

/**
  Notify function for event group EFI_EVENT_GROUP_EXIT_BOOT_SERVICES. This is used to
  remove Protocol Service support to make sure This protocol will not be used by
  any UEFI Application running in EDK Shell.

  @param[in]  Event    The Event that is being processed.
  @param[in]  Context  The Event Context.
**/
VOID
EFIAPI
TcssRetimerSupportCloseEventNotify (
  IN  EFI_EVENT  Event,
  IN  VOID       *Context
  )
{
  mRetimerCapsuleUpdateNotSupported = TRUE;
  //
  // Only one successful install
  //
  gBS->CloseEvent(Event);
}

/**
  Retimer Capsule Update DXE module entry point.

  @param[in]  ImageHandle  Not used.
  @param[in]  SystemTable  Global system service table.

  @retval  EFI_SUCCESS           Initialization complete.
  @retval  EFI_UNSUPPORTED       The chipset is unsupported by this driver.
  @retval  EFI_OUT_OF_RESOURCES  Do not have enough resources to initialize the driver.
  @retval  EFI_DEVICE_ERROR      Device error, driver exits abnormally.
**/
EFI_STATUS
EFIAPI
RetimerCapsuleUpdateEntryPoint (
  IN  EFI_HANDLE        ImageHandle,
  IN  EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS  Status;
  EFI_EVENT   Event;

  DEBUG ((DEBUG_INFO, "RetimerCapsuleUpdateEntryPoint: Start\n"));

  GetValidateData();

  // Initializing TCSS_RETIMER_PROTOCOL
  mTcssRetimerProtocol.GetRetimerInfo = GetRetimerInfo;
  mTcssRetimerProtocol.Drive          = DriveToFwUpdateMode;
  mTcssRetimerProtocol.Restore        = RestoreToOriginalMode;

  //
  // Install Tbt Retimer Protocol
  //
  Status = gBS->InstallMultipleProtocolInterfaces (
                  &ImageHandle,
                  &gTcssRetimerProtocolGuid,
                  &mTcssRetimerProtocol,
                  NULL
                  );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RetimerCapsuleUpdate: Failed to Install Tcss Retimer Protocol with Status = %r\n", Status));
    return Status;
  }
  DEBUG ((DEBUG_INFO, "RetimerCapsuleUpdateEntryPoint: TbtRetimerProtocol is installed Successfully.\n"));

  //
  // Register notify function to Remove Protocol Service on Exit Boot Services Event.
  // This additional functionality to make sure This protocol will not be used by
  // any UEFI Application running in EDK Shell.
  //
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  TcssRetimerSupportCloseEventNotify,
                  NULL,
                  &gEfiEventExitBootServicesGuid,
                  &Event
                  );
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_INFO, "RetimerCapsuleUpdateEntryPoint: TcssRetimerSupportCloseEvent is Register with Status =%r\n", Status));

  DEBUG ((DEBUG_INFO, "RetimerCapsuleUpdateEntryPoint: End\n"));
  return Status;
}
