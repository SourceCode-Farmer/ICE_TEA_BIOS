/** @file
  Secure Erase Implementation.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/**@file
  Secure Erase Implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Protocol/AmtWrapperProtocol.h>
#include <Protocol/AmtReadyToBoot.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/DxeAsfHeciLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/SecureEraseDxeLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <SetupVariable.h>
#include <MeBiosPayloadHob.h>
//[-start-201125-IB14630454-add]//
#include <Protocol/HddPasswordService.h>
//[-end-201125-IB14630454-add]//

GLOBAL_REMOVE_IF_UNREFERENCED SETUP_DATA                  mSetupData     = {0};
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                      mSetupDataAttr = 0;

/**
  This function is executed on AMT Ready to boot and performs Secure Erase.
**/
VOID
EFIAPI
SecureEraseOnReadyToBoot (
  VOID
  );

GLOBAL_REMOVE_IF_UNREFERENCED AMT_READY_TO_BOOT_PROTOCOL  mSecureEraseReadyToBoot = {
  AMT_READY_TO_BOOT_PROTOCOL_REVISION,
  SecureEraseOnReadyToBoot
};

/**
  Get Setup Variable from NVM

  @retval EFI_SUCCESS      Data retrieved
  @retval Others           Data was not retrieved
**/
EFI_STATUS
GetSetupData (
  VOID
  )
{
  EFI_STATUS    Status;
  UINTN         VarSize;

  VarSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  &mSetupDataAttr,
                  &VarSize,
                  &mSetupData
                  );
  return Status;
}

/**
  Save Setup Variable to NVM

  @retval EFI_SUCCESS      Data saved successfully
  @retval Others           Data was not retrieved
**/
EFI_STATUS
SaveSetupData (
  VOID
  )
{
  EFI_STATUS    Status;
  UINTN         VarSize;

  VarSize = sizeof (SETUP_DATA);
  Status = gRT->SetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  mSetupDataAttr,
                  VarSize,
                  &mSetupData
                  );

  DEBUG ((DEBUG_INFO, "SecureErase::SaveSetupData : Status = %r\n", Status));
  return Status;
}
/**
  Checks if Secure Erase is requested by AMT

  @retval TRUE     Secure Erase was requested by AMT
  @retval FALSE    Secure Erase wasn't requested by AMT
**/
BOOLEAN
IsAmtSecureErase (
  VOID
  )
{
  EFI_STATUS                           Status;
  AMT_WRAPPER_PROTOCOL                 *AmtWrapper;

  Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);

  if (!EFI_ERROR (Status) && AmtWrapper->IsSecureEraseEnabled ()) {
    DEBUG ((DEBUG_INFO, "SecureErase :: IsSecureEraseRequested : Requested by AMT\n"));
    return TRUE;
  }

  return FALSE;
}

/**
  Checks if Secure Erase is requested by Setup

  @retval TRUE     Secure Erase was requested by Setup
  @retval FALSE    Secure Erase wasn't requested by Setup
**/
BOOLEAN
IsSetupSecureErase (
  VOID
  )
{
  EFI_STATUS    Status;

  Status = GetSetupData ();
  if (!EFI_ERROR (Status) && mSetupData.ForceSecureErase) {
    DEBUG ((DEBUG_INFO, "SecureErase :: IsSecureEraseRequested : Requested by SetupData\n"));
    return TRUE;
  }
  return FALSE;
}

/**
  Check if Corporate firmware is installed

  @retval TRUE     The firmware is Corporate
  @retval FALSE    The firmware is NOT Corporate
**/
EFI_STATUS
IsCorporateFwInstalled (
  VOID
  )
{
  ME_BIOS_PAYLOAD_HOB  *MbpHob;

  MbpHob = GetFirstGuidHob (&gMeBiosPayloadHobGuid);
  if (MbpHob == NULL) {
    return FALSE;
  }

  if ((MbpHob->MeBiosPayload.FwPlatType.Available) &&
      (MbpHob->MeBiosPayload.FwPlatType.RuleData.Fields.IntelMeFwImageType == IntelMeCorporateFw)) {
    DEBUG ((DEBUG_INFO, "SecureErase :: Installed CSME is a Corporate FW\n"));
    return TRUE;
  }

  return FALSE;
}

/**
  Checks if Secure Erase has been requested

  @retval TRUE     Secure Erase was requested
  @retval FALSE    Secure Erase wasn't requested
**/
BOOLEAN
IsSecureEraseRequested (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "SecureErase :: IsSecureEraseRequested\n"));

  if (IsAmtSecureErase () || IsSetupSecureErase ()) {
    return TRUE;
  }

  DEBUG ((DEBUG_INFO, "SecureErase :: IsSecureEraseRequested : Secure Erase not requested\n"));
  return FALSE;
}

/**
  This function is executed on AMT Ready to boot and performs Secure Erase.
**/
VOID
EFIAPI
SecureEraseOnReadyToBoot (
  VOID
  )
{
  EFI_STATUS    Status;

  DEBUG ((DEBUG_INFO, "SecureErase :: SecureEraseOnReadyToBoot\n"));
  ReportBiosStatus (AsfRbsInProgress);
  Status = PerformSsdErase (SECURE_ERASE_FEATURE, (BOOLEAN) mSetupData.SecureEraseModeRealMode);

  if (!EFI_ERROR (Status) && (mSetupData.ForceSecureErase == TRUE)) {
    mSetupData.ForceSecureErase = FALSE;
    SaveSetupData ();
  }

  if (mSetupData.SecureEraseModeRealMode == 0) {
    //
    // If demo mode is enabled, don't restart. Use dead loop to make sure screen keeps
    // displaying "missing operating system" message from SimulatedSecureErase.c forever.
    //
    CpuDeadLoop ();
  } else {
    gRT->ResetSystem (EfiResetShutdown, Status, 0, NULL);
  }
}

//[-start-201125-IB14630454-add]//
VOID
EFIAPI
HddUnLockNotificationEvent (
  IN  EFI_EVENT       Event,
  IN  VOID            *Context
  )
{
  EFI_STATUS  Status;
  EFI_HDD_PASSWORD_DIALOG_PROTOCOL *HddPasswordDialogProtocol;

  Status = gBS->LocateProtocol (&gEfiHddPasswordDialogProtocolGuid, NULL, (VOID **) &HddPasswordDialogProtocol);
  if (EFI_ERROR (Status)) {
    return;
  }
  gBS->CloseEvent (Event);
 
  HddPasswordDialogProtocol->HddPasswordUnlocked (HddPasswordDialogProtocol);
}
//[-end-201125-IB14630454-add]//

VOID
EFIAPI
SecureEraseNotificationEvent (
  IN  EFI_EVENT       Event,
  IN  VOID            *Context
  )
{
  EFI_STATUS  Status;
  VOID        *Protocol;

  Status = gBS->LocateProtocol (&gTrustedConsoleConnectedProtocolGuid, NULL, (VOID **) &Protocol);
  if (EFI_ERROR (Status)) {
    return;
  }
  gBS->CloseEvent (Event);

  DEBUG ((DEBUG_INFO, "SecureErase :: SecureEraseNotificationEvent\n"));
  SecureEraseOnReadyToBoot ();
}
/**
  Drivers entry point. Checks if secure erase has been requested via boot options,
  if so, starts the procedure.

  @param[in] ImageHandle      The firmware allocated handle for the EFI image.
  @param[in] SystemTable      A pointer to the EFI System Table.

  @retval EFI_SUCCESS         Secure erase procedure has been started
  @retval EFI_ABORTED         Failed to register ready to boot event
  @retval EFI_UNSUPPORTED     Feature is not supported by the FW
**/
EFI_STATUS
EFIAPI
SecureEraseEntryPoint (
  IN EFI_HANDLE                       ImageHandle,
  IN EFI_SYSTEM_TABLE                 *SystemTable
  )
{
  EFI_STATUS        Status;
  EFI_HANDLE        Handle;
  VOID              *Registration;
//[-start-201125-IB14630454-add]//
  VOID              *Registration2;
//[-end-201125-IB14630454-add]//

  DEBUG ((DEBUG_INFO, "SecureErase :: SecureEraseEntryPoint\n"));

  if (!IsCorporateFwInstalled ()) {
    return EFI_UNSUPPORTED;
  }

  if (!IsSecureEraseRequested ()) {
    return EFI_ABORTED;
  }
  //
  // Secure Erase is requested, so this module will take control over HDD's and OPAL's unlocking process
  //
  PcdSetBoolS (PcdSkipHddPasswordPrompt, TRUE);
  PcdSetBoolS (PcdSkipOpalPasswordPrompt, TRUE);

  //
  // Get setup data in case mSetupData is not initialized.
  //
  Status = GetSetupData ();
  ASSERT_EFI_ERROR (Status);

  //
  // Register notify function when trusted console connected.
  //
  EfiCreateProtocolNotifyEvent (
    &gTrustedConsoleConnectedProtocolGuid,
    TPL_CALLBACK,
    SecureEraseNotificationEvent,
    NULL,
    &Registration
    );

//[-start-201125-IB14630454-add]//
  EfiCreateProtocolNotifyEvent (
    &gEfiHddPasswordDialogProtocolGuid,
    TPL_NOTIFY,
    HddUnLockNotificationEvent,
    NULL,
    &Registration2
    );  
//[-end-201125-IB14630454-add]//

  //
  // Install an Amt ready to boot protocol.
  //
  Handle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gAmtReadyToBootProtocolGuid,
                  &mSecureEraseReadyToBoot,
                  NULL
                  );
  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;
}
