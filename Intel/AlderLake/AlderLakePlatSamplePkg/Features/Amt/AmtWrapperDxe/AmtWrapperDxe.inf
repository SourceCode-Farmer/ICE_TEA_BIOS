### @file
# Component description file for AmtWrapper Driver.
#
#******************************************************************************
#* Copyright 2020 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2010 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = AmtWrapperDxe
  FILE_GUID                      = D77C900D-A1C7-41C5-B989-0C3D37FCA432
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = DXE_DRIVER
  ENTRY_POINT                    = AmtWrapperDxeEntryPoint
#
# The following information is for reference only and not required by the build tools.
#
# VALID_ARCHITECTURES = IA32 X64 IPF EBC
#

[LibraryClasses]
  UefiDriverEntryPoint
  PrintLib
  DevicePathLib
  DxeServicesTableLib
  MemoryAllocationLib
  BaseMemoryLib
  UefiRuntimeServicesTableLib
  UefiBootServicesTableLib
  UefiBootManagerLib
#[-start-200831-IB17800089-add]#
#  PlatformBootManagerLib
  GenericBdsLib
#[-end-200831-IB17800089-add]#
  PchInfoLib
  ConfigBlockLib
  DxeAsfLib
  DxeAmtHeciLib
  PchPciBdfLib
  DxeMeLib
  PciSegmentLib
  IoLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-200420-IB17800056-add]#
  SecurityPkg\SecurityPkg.dec
#[-end-200420-IB17800056-add]#
#[-start-200831-IB17800089-add]#
  InsydeModulePkg/InsydeModulePkg.dec
  ShellPkg/ShellPkg.dec
#[-end-200831-IB17800089-add]#
#[-start-210421-IB09480139-add]#
  AlderLakeChipsetPkg/AlderLakeChipsetPkg.dec
#[-end-210421-IB09480139-add]#

[Sources]
  AmtWrapperDxe.c
  AsfSupport.c

[Protocols]
  gAlertStandardFormatProtocolGuid              ## CONSUMES
  gHeciProtocolGuid                             ## CONSUMES
  gEfiSimpleFileSystemProtocolGuid              ## CONSUMES
  gAmtWrapperProtocolGuid                       ## PRODUCES
#[-start-200831-IB17800089-add]#
  gDxeAmtPolicyGuid
#[-end-200831-IB17800089-add]#

[Guids]
  gEfiGlobalVariableGuid                        ## CONSUMES
  gMeBiosPayloadHobGuid                         ## CONSUMES
  gSetupEnterGuid                               ## CONSUMES
  gAmtDxeConfigGuid                             ## CONSUMES
#[-start-200831-IB17800089-add]#
  gEfiSecureBootEnableDisableGuid
  gUefiShellFileGuid
#[-end-200831-IB17800089-add]#

[Pcd]

[Depex]
  gAlertStandardFormatProtocolGuid    AND
  gDxeAmtPolicyGuid                   AND
  gEfiPciRootBridgeIoProtocolGuid
