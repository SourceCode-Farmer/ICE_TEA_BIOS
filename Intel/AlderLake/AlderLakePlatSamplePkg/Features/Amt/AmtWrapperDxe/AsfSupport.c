/** @file
  Support routines for ASF boot options in the BDS
 
;******************************************************************************
;* Copyright 2020 - 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************

@copyright
  INTEL CONFIDENTIAL
  Copyright 2005 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//[-start-210421-IB09480139-modify]//
#include <UefiBootManagerLib.h>
//[-end-210421-IB09480139-modify]//
#include "AmtWrapperDxe.h"
#include "AsfSupport.h"
#include <Library/DebugLib.h>
#include <PchBdfAssignment.h>
//[-start-200831-IB17800089-add]//
#include <Library/GenericBdsLib.h>
//[-end-200831-IB17800089-add]//

//
// Global variables
//
GLOBAL_REMOVE_IF_UNREFERENCED EFI_GUID                        mAsfRestoreBootSettingsGuid = RESTORE_SECURE_BOOT_GUID;
GLOBAL_REMOVE_IF_UNREFERENCED STORAGE_REDIRECTION_DEVICE_PATH mUsbrDevicePath = {
  gPciRootBridge,
  {
    {
      HARDWARE_DEVICE_PATH,
      HW_PCI_DP,
      {
        (UINT8)(sizeof (PCI_DEVICE_PATH)),
        (UINT8)((sizeof (PCI_DEVICE_PATH)) >> 8)
      }
    },
    PCI_FUNCTION_NUMBER_PCH_XHCI,
    PCI_DEVICE_NUMBER_PCH_XHCI
  },
  {
    {
      MESSAGING_DEVICE_PATH,
      MSG_USB_DP,
      {
        (UINT8)(sizeof (USB_DEVICE_PATH)),
        (UINT8)((sizeof (USB_DEVICE_PATH)) >> 8)
      }
    },
    0,
    0
  },
  gEndEntire
};

/**
  This routine makes necessary Secure Boot change for Storage Redirection boot

  @retval EFI_SUCCESS      Changes applied succesfully
**/
EFI_STATUS
ManageSecureBootState (
  VOID
  )
{
  EFI_STATUS Status;
  BOOLEAN    EnforceSecureBoot;
  UINT8      SecureBootState;
  UINT8      UsbrBoot;
  UINTN      VarSize;
  UINT32     VarAttributes;
  UINT8      RestoreBootSettings;

  VarSize = sizeof (UINT8);

  //
  // Get boot parameters (Storage Redirection boot?, EnforceSecureBoot flag set?, secure boot enabled?)
  //
  EnforceSecureBoot = AsfIsEnforceSecureBootEnabled ();
  UsbrBoot          = AsfIsStorageRedirectionEnabled ();

  Status = gRT->GetVariable (
                  L"SecureBootEnable",
                  &gEfiSecureBootEnableDisableGuid,
                  &VarAttributes,
                  &VarSize,
                  &SecureBootState
                  );
  if (EFI_ERROR(Status)) {
    return EFI_SUCCESS;
  }

  //
  // Check whether we need to restore SecureBootEnable value changed in previous Storage Redirection boot
  //
  Status = gRT->GetVariable(
                  L"RestoreBootSettings",
                  &mAsfRestoreBootSettingsGuid,
                  NULL,
                  &VarSize,
                  &RestoreBootSettings
                  );

  if (Status == EFI_SUCCESS && RestoreBootSettings != RESTORE_SECURE_BOOT_NONE) {
    if (RestoreBootSettings == RESTORE_SECURE_BOOT_ENABLED &&
        SecureBootState == SECURE_BOOT_DISABLED &&
        !(UsbrBoot && !EnforceSecureBoot)) {

      SecureBootState = SECURE_BOOT_ENABLED;
      Status = gRT->SetVariable (
                      L"SecureBootEnable",
                      &gEfiSecureBootEnableDisableGuid,
                      VarAttributes,
                      VarSize,
                      &SecureBootState
                      );
      ASSERT_EFI_ERROR (Status);

      //
      // Delete RestoreBootSettings variable
      //
      Status = gRT->SetVariable (
                      L"RestoreBootSettings",
                      &mAsfRestoreBootSettingsGuid,
                      0,
                      0,
                      NULL
                      );
      ASSERT_EFI_ERROR (Status);

      DEBUG ((DEBUG_INFO, "Secure Boot settings restored after Storage Redirection boot - Cold Reset!\n"));
      gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
    }
  }

  Status = EFI_SUCCESS;

  if (UsbrBoot) {
    if (SecureBootState == SECURE_BOOT_ENABLED && !EnforceSecureBoot) {
      //
      // Secure boot needs to be disabled if we're doing Storage Redirection and EnforceSecureBoot not set
      //
      SecureBootState     = SECURE_BOOT_DISABLED;
      RestoreBootSettings = RESTORE_SECURE_BOOT_ENABLED;

      Status = gRT->SetVariable (
                      L"SecureBootEnable",
                      &gEfiSecureBootEnableDisableGuid,
                      VarAttributes,
                      sizeof (UINT8),
                      &SecureBootState
                      );
      ASSERT_EFI_ERROR (Status);

      //
      // Set variable to restore previous secure boot state
      //
      Status = gRT->SetVariable (
                      L"RestoreBootSettings",
                      &mAsfRestoreBootSettingsGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                      sizeof (UINT8),
                      &RestoreBootSettings
                      );
      ASSERT_EFI_ERROR (Status);

      DEBUG ((DEBUG_INFO, "Secure Boot disabled for USBr boot - Cold Reset!\n"));
      gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
    }
  }

  return Status;
}

/**
  Compare two device paths up to a size of Boot Opion's Device Path

  @param[in] BootOptionDp     Device path acquired from BootXXXX EFI variable
  @param[in] FileSysDp        Device path acquired through EFI_SIMPLE_FILE_SYSTEM_PROTOCOL Handles buffer

  @retval TRUE                Both device paths point to the same device
  @retval FALSE               Device paths point to different devices
**/
BOOLEAN
CompareDevicePaths (
  IN  EFI_DEVICE_PATH_PROTOCOL *BootOptionDp,
  IN  EFI_DEVICE_PATH_PROTOCOL *FileSysDp
  )
{
  UINTN BootOptionDpSize;
  UINTN FileSysDpSize;

  if (BootOptionDp == NULL || FileSysDp == NULL) {
    return FALSE;
  }

  BootOptionDpSize = GetDevicePathSize (BootOptionDp) - END_DEVICE_PATH_LENGTH;
  FileSysDpSize    = GetDevicePathSize (FileSysDp) - END_DEVICE_PATH_LENGTH;

  if ((BootOptionDpSize <= FileSysDpSize) && (CompareMem (FileSysDp, BootOptionDp, BootOptionDpSize) == 0)) {
    return TRUE;
  }

  return FALSE;
}

/**
  Get EFI device path through EFI_SIMPLE_FILE_SYSTEM_PROTOCOL Handles buffer. Acquired path must
  point to the same device as argument DevicePath passed to the function.

  @param[in] DevicePath              Device path acquired from BootXXXX EFI variable
  @param[in] EfiDeviceType           EFI device path type

  @retval EFI_DEVICE_PATH_PROTOCOL   Device path for booting
**/
EFI_DEVICE_PATH_PROTOCOL *
GetFullBootDevicePath (
  IN EFI_DEVICE_PATH_PROTOCOL *DevicePath,
  IN UINTN                    EfiDeviceType
  )
{
  EFI_STATUS               Status;
  EFI_DEVICE_PATH_PROTOCOL *TempDevicePath;
  EFI_DEVICE_PATH_PROTOCOL *ReturnDevicePath;
  UINTN                    HandleNum;
  EFI_HANDLE               *HandleBuf;
  UINTN                    Index;

  ReturnDevicePath = NULL;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  ((EfiDeviceType == MSG_MAC_ADDR_DP) ? &gEfiLoadFileProtocolGuid : &gEfiSimpleFileSystemProtocolGuid),
                  NULL,
                  &HandleNum,
                  &HandleBuf
                  );
  if ((EFI_ERROR (Status)) || (HandleBuf == NULL)) {
    return NULL;
  }

  for (Index = 0; Index < HandleNum; Index++) {
    TempDevicePath = DevicePathFromHandle (HandleBuf[Index]);

    if (CompareDevicePaths (DevicePath, TempDevicePath)) {
      ReturnDevicePath = DuplicateDevicePath (TempDevicePath);
      break;
    }
  }

  return ReturnDevicePath;
}

/**
  Translate ASF request type to BBS or EFI device path type

  @param[in] DeviceType     ASF request type

  @retval UINTN             Translated device type
**/
UINTN
GetBootDeviceType (
  IN UINTN    DeviceType
  )
{
  UINTN Type = 0;

  switch (DeviceType) {
    case FORCE_PXE:
      Type = MSG_MAC_ADDR_DP;
      break;
    case FORCE_HARDDRIVE:
    case FORCE_SAFEMODE:
      Type = MEDIA_HARDDRIVE_DP;
      break;
    case FORCE_DIAGNOSTICS:
      Type = MEDIA_PIWG_FW_FILE_DP;
      break;
    case FORCE_CDDVD:
      Type = MEDIA_CDROM_DP;
      break;
    default:
      break;
  }

  return Type;
}

/**
  Update the BBS table with our required boot device

  @param[in]  BbsIndex    Index of BBS_TABLE structures
  @param[out] IsUsbr      Set to TRUE if this is USBR device path

  @retval EFI_SUCCESS     BBS table successfully updated
**/
EFI_STATUS
ProcessBbsTable (
  IN UINT16     BbsIndex,
  OUT BOOLEAN   *IsUsbr
  )
{
  return EFI_NOT_FOUND;
}

/**
  Build the Device Path for this boot selection

  @param[in] BootOptions   Boot Option data pointer
  @param[in] OptionCount   Boot Option count
  @param[in] DeviceType    Boot device whose device type
  @param[in] DeviceIndex   Boot device whose device index
  @param[in] UsbrBoot      If UsbrBoot is TRUE then check USBr device

  @retval BootDeviceIndex  Index of Boot Device
**/
UINTN
GetBootOptionIndex (
  IN EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions,
  IN UINTN                        OptionCount,
  IN UINT16                       DeviceType,
  IN UINT16                       DeviceIndex,
  IN BOOLEAN                      UsbrBoot
  )
{
  EFI_DEVICE_PATH_PROTOCOL     *FullDevicePath;
//[-start-201126-IB14630455-add]//
  EFI_DEVICE_PATH_PROTOCOL     *BootOptionDevicePath;
//[-end-201126-IB14630455-add]//
  UINTN                        Index;
  UINTN                        TempIndex;
  EFI_DEVICE_PATH_PROTOCOL     *DevPathNode;
  UINTN                        EfiDeviceType;
  BOOLEAN                      TypeMatched;
//[-start-210523-IB05660170-add]//
  BOOLEAN                      TypeMatchedHdd;
//[-end-210523-IB05660170-add]//
  INTN                         BootDeviceIndex;
  UINTN                        EfiNodeType;

  FullDevicePath   = NULL;
//[-start-201126-IB14630455-add]//
  BootOptionDevicePath = NULL;
//[-end-201126-IB14630455-add]//
  TempIndex        = 1;
  EfiDeviceType    = GetBootDeviceType (DeviceType);
  TypeMatched      = FALSE;
//[-start-210523-IB05660170-add]//
  TypeMatchedHdd   = FALSE;
//[-end-210523-IB05660170-add]//
  BootDeviceIndex  = -1;
  EfiNodeType      = (EfiDeviceType == MSG_MAC_ADDR_DP) ? MESSAGING_DEVICE_PATH : MEDIA_DEVICE_PATH;

  mUsbrDevicePath.Usbr.InterfaceNumber = AsfGetStorageRedirectionBootDevice ();
  mUsbrDevicePath.Usbr.ParentPortNumber = GetPchUsbrStoragePortNum ();

  for (Index = 0; Index < OptionCount; Index++) {
    //
    // If this is EFI boot option, we need to get full device path from EFI_SIMPLE_FILE_SYSTEM_PROTOCOL
    // to determine type of device
    //
//[-start-201126-IB14630455-add]//
    BootOptionDevicePath = (IS_USB_SHORT_FORM_DEVICE_PATH (BootOptions[Index].FilePath)) ?BdsLibExpandUsbShortFormDevPath (BootOptions[Index].FilePath) : BootOptions[Index].FilePath;
//[-end-201126-IB14630455-add]//

//[-start-201126-IB14630455-modify]//
    FullDevicePath = GetFullBootDevicePath (BootOptionDevicePath, EfiDeviceType);
//[-end-201126-IB14630455-modify]//
    if (FullDevicePath == NULL) {
//[-start-210523-IB05660170-modify]//
      //continue;
      FullDevicePath = DuplicateDevicePath(BootOptions[Index].FilePath);
//[-end-210523-IB05660170-modify]//
    }

    DevPathNode = FullDevicePath;

//[-start-210523-IB05660170-add]//
    if (DevPathNode == NULL) {
      continue;
    }
//[-end-210523-IB05660170-add]//

    //
    // Check if this is our requested boot device
    //
    while (!IsDevicePathEnd (DevPathNode)) {
      DEBUG ((DEBUG_INFO, "DevPathNode: %s\n", ConvertDevicePathToText (DevPathNode, FALSE, FALSE)));
      if (UsbrBoot) {
        if (CompareDevicePaths((EFI_DEVICE_PATH_PROTOCOL *)&mUsbrDevicePath, FullDevicePath)) {
          TypeMatched = TRUE;
        }
      } else {
        //
        // EFI boot option
        //
        if (DevicePathType (DevPathNode) == EfiNodeType && DevicePathSubType (DevPathNode) == EfiDeviceType) {
          if (DeviceType == FORCE_DIAGNOSTICS) {
            //
            // If boot to EFI shell, find shell file by GUID
            //
            if (CompareMem (&((MEDIA_FW_VOL_FILEPATH_DEVICE_PATH *)DevPathNode)->FvFileName, &gUefiShellFileGuid, sizeof(EFI_GUID))) {
              TypeMatched = TRUE;
            }
          } else {
            TypeMatched = TRUE;
          }
        } else if ((DeviceType == FORCE_HARDDRIVE) && (DevicePathSubType (DevPathNode) == MSG_USB_DP)) {
          DEBUG ((DEBUG_INFO, "Skip USB disk if system is forced to boot local hard drive\n"));
          break;
        }
//[-start-210523-IB05660170-add]//
        if (DevicePathType (DevPathNode) == MEDIA_DEVICE_PATH && DevicePathSubType (DevPathNode) == MEDIA_FILEPATH_DP) {
          TypeMatchedHdd = TRUE;
        }
//[-end-210523-IB05660170-add]//
      }

//[-start-210523-IB05660170-add]//
      DevPathNode = NextDevicePathNode(DevPathNode);
//[-end-210523-IB05660170-add]//

      if (TypeMatched) {
        //
        // Type matched, check for device index
        //
        if (!UsbrBoot && TempIndex < DeviceIndex) {
          TempIndex++;
          TypeMatched = FALSE;
          break;
        }
//[-start-210523-IB05660170-add]//
        if ((DeviceType == FORCE_HARDDRIVE) && (!TypeMatchedHdd)) {
          continue;
        }
//[-end-210523-IB05660170-add]//
        BootDeviceIndex = Index;
        break;
      }

//[-start-210523-IB05660170-remove]//
//    DevPathNode = NextDevicePathNode (DevPathNode);
//[-end-210523-IB05660170-remove]//
    }

    if (FullDevicePath != NULL) {
      FreePool (FullDevicePath);
      FullDevicePath = NULL;
    }

//[-start-201126-IB14630455-add]//
    if (BootOptionDevicePath != NULL) {
      FreePool (BootOptionDevicePath);
      BootOptionDevicePath = NULL;
    }
//[-end-201126-IB14630455-add]//

    if (BootDeviceIndex != -1) {
      break;
    }
//[-start-210523-IB05660170-add]//
    TypeMatched = FALSE;
//[-end-210523-IB05660170-add]//
  }

  return BootDeviceIndex;
}

/**
  Found out ASF boot options.

  @param[in] BootOptions               Boot Option data pointer
  @param[in] DeviceType                Boot device whose device type
  @param[in] OptionCount               Boot Option count

  @retval INTN                         Boot Option Index.
**/
INTN
GetAsfBootOptionIndex (
  IN EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions,
  IN UINT8                        DeviceType,
  IN UINTN                        OptionCount
  )
{
  INTN                BootOptionIndex = -1;
  UINT8               SpecialCmdParamHigh;
  UINT8               SpecialCmdParamLow;

  SpecialCmdParamHigh = AsfGetSpecialCmdParamHighByte ();
  SpecialCmdParamLow  = AsfGetSpecialCmdParamLowByte ();

  //
  // First we check ASF boot options Special Command
  //
  switch (DeviceType) {
  //
  // The Special Command Parameter can be used to specify a PXE
  // parameter. When the parameter value is 0, the system default PXE device is booted. All
  // other values for the PXE parameter are reserved for future definition by this specification.
  //
  case FORCE_PXE:
  case FORCE_DIAGNOSTICS:
    if ((SpecialCmdParamHigh != 0) || (SpecialCmdParamLow != 0)) {
      //
      // ASF spec says 0 currently is the only option
      //
      break;
    }

    //
    // We want everything connected up for PXE or EFI shell
    //
//[-start-200831-IB17800089-remove]//
//    EfiBootManagerConnectAll ();
//[-end-200831-IB17800089-remove]//
    BootOptionIndex = GetBootOptionIndex (BootOptions, OptionCount, DeviceType, SpecialCmdParamHigh, FALSE);
    break;

  //
  // The Special Command Parameter identifies the boot-media index for
  // the managed client. When the parameter value is 0, the default hard-drive/optical drive is booted, when the
  // parameter value is 1, the primary hard-drive/optical drive is booted; when the value is 2, the secondary
  // hard-drive/optical drive is booted and so on.
  //
  case FORCE_HARDDRIVE:
  case FORCE_SAFEMODE:
  case FORCE_CDDVD:
    BootOptionIndex = GetBootOptionIndex (BootOptions, OptionCount, DeviceType, SpecialCmdParamHigh, FALSE);
    break;

  //
  // No additional special command is included; the Special Command Parameter has no
  // meaning.
  //
  case NOP:
  default:
    break;
  }

  return BootOptionIndex;
}

/**
  Check Storage Redirection boot device and Asf boot device

  @param[in] BootOptions   Boot Option data pointer
  @param[in] OptionCount   Boot Option count

  @retval INTN             Boot Option Index.
**/
INTN
GetForcedBootOptionIndex (
  IN EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions,
  IN UINTN                        OptionCount
  )
{
  INTN                BootOptionIndex = -1;
  UINT8               SpecialCommand;

  SpecialCommand = AsfGetSpecialCommand ();

  //
  // OEM command values; the interpretation of the Special Command and associated Special
  // Command Parameters is defined by the entity associated with the Enterprise ID.
  //
  if (AsfIsStorageRedirectionEnabled ()) {
    BootOptionIndex = GetBootOptionIndex (
                        BootOptions,
                        OptionCount,
                        FORCE_CDDVD,
                        0,
                        TRUE
                        );
  } else if (!AsfIsIndustryIanaId ()) {
    BootOptionIndex = GetAsfBootOptionIndex (
                        BootOptions,
                        SpecialCommand,
                        OptionCount
                        );
  }

  return BootOptionIndex;
}

/**
  Process ASF boot options and if available, attempt the boot

  @retval EFI_SUCCESS    The command completed successfully
**/
EFI_STATUS
BdsBootViaAsf (
  VOID
  )
{
  EFI_STATUS                   Status;
  EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions;
  UINTN                        OptionCount;
  INTN                         BootOptionIndex;
  EFI_INPUT_KEY                Key = {0};
  UINTN                        EventIndex;
  EFI_TPL                      OldTpl;

  Status      = EFI_SUCCESS;
  BootOptions = NULL;

  //
  // Check if ASF Boot Options is present.
  //
  if (!AsfIsBootOptionsPresent ()) {
    return EFI_NOT_FOUND;
  }

  BootOptions = EfiBootManagerGetLoadOptions (&OptionCount, LoadOptionTypeBoot);
  if (BootOptions == NULL) {
    return EFI_UNSUPPORTED;
  }

  BootOptionIndex = GetForcedBootOptionIndex (BootOptions, OptionCount);
  //
  // If device path was set, the we have a boot option to use
  //
  if (BootOptionIndex == -1) {
    return EFI_UNSUPPORTED;
  }

  //
  // If this is RCO/Storage Redirection EFI Boot, keep trying unless user cancels
  //
  while (!(Key.ScanCode == SCAN_ESC && Key.UnicodeChar == 0)) {
    EfiBootManagerBoot (&BootOptions[BootOptionIndex]);
    //
    // Returning from EfiBootManagerBoot means the boot failed
    // Display message to user before attempting another RCO/Storage Redirection boot
    //
    gST->ConOut->ClearScreen (gST->ConOut);
    gST->ConOut->OutputString (
                  gST->ConOut,
                  L"RCO/USBR boot failed. Press ENTER to try again or ESC to return to regular boot\r\n"
                  );
    Key.ScanCode    = 0;
    Key.UnicodeChar = 0;
    OldTpl = gBS->RaiseTPL (TPL_HIGH_LEVEL);
    gBS->RestoreTPL (TPL_APPLICATION);
    while (!(Key.ScanCode == 0 && Key.UnicodeChar == L'\r')) {
      gBS->WaitForEvent (1, &(gST->ConIn->WaitForKey), &EventIndex);
      gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);

      if (Key.ScanCode == SCAN_ESC && Key.UnicodeChar == 0) {
        break;
      }
    }

    if (OldTpl > TPL_APPLICATION) {
      gBS->RaiseTPL (OldTpl);
    }
  }

  EfiBootManagerFreeLoadOptions (BootOptions, OptionCount);

  return Status;
}

