/** @file
  Header file for WiFi Profile Sync driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _WIFI_PROFILE_SYNC_H_
#define _WIFI_PROFILE_SYNC_H_

#include <Uefi.h>
#include <WifiProfileSyncAsfMsgs.h>
#include <WifiConnectionManagerDxe/WifiConnectionMgrConfig.h>

#define WIFI_PROFILE_SYNC_VERSION   0x01

// Profile security type
#define PSK_NONE          0
#define PSK_PASS_PHRASE   3
#define PSK_NETWORK       4

// Timeout to get profile data from AMT
#define ME_DELAY_COUNT    150u      // delay cycles to get profile from ME with fail status
#define ME_WLAN_DELAY     100000u   // 100msec delay allowing ME WLAN connection completion time

// EFI Network Setup Data Options
enum {
  EfiNetworkDisabled  = 0,
  EfiNetworkWifi      = 2
  };

/**
  Function to set the WiFi connection status recieved by the WiFiConnectionManager
  as EFI_80211_CONNECT_NETWORK_RESULT_CODE, this will get converted to EFI_STATUS
  type

  @param[in] EFI_80211_CONNECT_NETWORK_RESULT_CODE    WiFi connection attempt results
**/
VOID
EFIAPI
WifiProfileSyncSetConnectStatus (
  IN EFI_80211_CONNECT_NETWORK_RESULT_CODE  ConnectionStatus
  );

/**
  Function to retrieve the WiFi connection status when in OCR WLAN flow

  @return Status                    WiFi connection status stored
**/
EFI_STATUS
EFIAPI
WifiProfileSyncGetConnectStatus (
  VOID
  );

/**
  Callback to reset EFI newtork data setup data

  @param[in] Event                  A pointer to the Event that triggered the callback
  @param[in] Context                A pointer to private data registered with the callback function

**/
VOID
EFIAPI
WifiProfileSyncDisableNetwork (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  );

/**
  Callback set for exit boot services to clear the profile data from memory and unistall protocol

  @param[in] Event                  A pointer to the Event that triggered the callback
  @param[in] Context                A pointer to private data registered with the callback function

**/
VOID
EFIAPI
WifiProfileSyncCleanCallback (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  );

/**
  This API will be used by the WiFi connection manager to get the WiFi profile that ASF shared
  and was stored in WiFi profile protocol, aligning and passing the ASF data structure to WCM
  structure

  @param[in, out]  WcmProfile       WiFi Connection Manager profile structure

  @return EFI_SUCCESS               Profiles returned
  @return EFI_UNSUPPORTED           Profile protocol sharing not supported or enabled
  @return EFI_NOT_FOUND             No profiles returned
  @return Others                    Error Occurred
**/
EFI_STATUS
EFIAPI
WifiProfileSyncGetProfile (
  IN OUT WIFI_MGR_NETWORK_PROFILE *WcmProfile
  );

/**
  The entry point for the Wifi Profile Sync driver.

  @param[in] ImageHandle            The firmware allocated handle for the EFI image
  @param[in] SystemTable            A pointer to the EFI System Table

  @retval EFI_SUCCESS               The entry point is executed successfully
  @retval other                     Some error occurs when executing this entry point
**/
EFI_STATUS
EFIAPI
WifiProfileSyncEntryPoint (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  );

#endif
