/** @file
  Utility function header file support for WiFi Profile Sync driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _WIFI_PROFILE_SYNC_UTILS_H_
#define _WIFI_PROFILE_SYNC_UTILS_H_

#include <WifiConnectionManagerDxe/WifiConnectionMgrDxe.h>

enum {
// Encryption method from CSME
  EncryptionMethodUnknown                   = 0xff,
  EncryptionMethodNone                      = 0x01,
  EncryptionMethodWep                       = 0x02,  ///< (should not be supported any more)
  EncryptionMethodTkip                      = 0x08,  ///< (should not be supported any more)
  EncryptionMethodCcmp                      = 0x10,
  EncryptionMethodGcmp256                   = 0x100,

// AKM authentication method from CSME
  AuthenticationMethodOpen                  = 0x00, ///< open systems
  AuthenticationMethodSharedKey             = 0x01,
  AuthenticationMethodWpa                   = 0x02, ///< wpa (wpa ieee 802.1x)
  AuthenticationMethodWpaPsk                = 0x04,
  AuthenticationMethodRsn                   = 0x08, ///< rsn (wpa2 ieee 802.1x)
  AuthenticationMethodRsnPsk                = 0x10, ///< rsn psk (wpa2 psk)
  AuthenticationMethodWpa3Sae               = 0x20,
  AuthenticationMethodWpa3Owe               = 0x40,
  AuthenticationMethodUnknown               = 0xff,

// Authentication protocol EAP method from CSME
  AuthenticationProtocolEapMethodDisabled   = 0,
  AuthenticationProtocolEapMethodGtc        = 6,    ///< (credentials_8021x)
  AuthenticationProtocolEapMethodTls        = 13,   ///< (pki_8021x)
  AuthenticationProtocolEapMethodTtls       = 21,   ///< (pki_8021x)
  AuthenticationProtocolEapMethodPeap       = 25,   ///< (pki_8021x)
  AuthenticationProtocolEapMethodFast       = 43,   ///< (pac_8021x)
  AuthenticationProtocolEapMethodUnknown    = 0xff,

// Authentication protocol inner method from CSME
  AuthenticationProtocolInnerMethodNone     = 0,
  AuthenticationProtocolInnerMethodMschapv2 = 1,
  AuthenticationProtocolInnerMethodGtc      = 2,
  AuthenticationProtocolInnerMethodTls      = 3,
  AuthenticationProtocolInnerMethodUnknown  = 0xff
};

/**
  Converts CSME provided encryption method to WiFiConnectionManager value

  @param[in] AuthenticationMethod  - CSME provided encryption method

  @return The corresponding WiFiConnectionManager cipher value
**/
UINT8
MapEncryptionToCipher (
  IN UINT32   EncryptionMethod
  );

/**
  Converts CSME provided  AuthenticationProtocol_eapMethod to WiFiConnectionManager EapAuthMethod value

  @param[in] AuthenticationProtocolEapMethod  - CSME provided encryption method

  @return The corresponding WiFiConnectionManager EAP Method value
**/
UINT8
MapEapAuthMethod (
  IN UINT32   AuthenticationProtocolEapMethod
  );

/**
  Converts CSME provided authentication method to WiFiConnectionManager value

  @param[in] AuthenticationMethod  - CSME provided authenticaion method

  @return The corresponding WiFiConnectionManager AKM value
**/
UINT8
MapAuthenticationToAKM (
  IN UINT32   AuthenticationMethod
  );

/**
  Converts CSME provided Inner/Secondary EAP method to WiFiConnectionManager value

  @param[in] AuthInnerMethod  - CSME provided Inner/Secondary EAP method

  @return The corresponding WiFiConnectionManager secondary/inner EAP method value
**/
UINT8
MapAuthInnerMethodToEapSecondAuthMethod (
  IN UINT32   AuthInnerMethod
  );

/**
  Converts the low nibble of a byte  to hex unicode character.

  @param[in] Nibble - lower nibble of a byte.

  @return Hex unicode character.
**/
CHAR16
NibbleToHexChar (
  IN UINT8      Nibble
  );

/**
  Converts binary buffer to Unicode string.
  At a minimum, any blob of data could be represented as a hex string.

  @param[in, out] Str   - Pointer to the string.
  @param[in] StrLen     - Length in bytes of buffer to hold the hex string. Includes tailing '\0' character.
                            If routine return with EFI_SUCCESS, containing length of hex string buffer.
                            If routine return with EFI_BUFFER_TOO_SMALL, containg length of hex string buffer desired.
  @param[in] Buf        - Buffer to be converted from.
  @param[in] Len        - Length in bytes of the buffer to be converted.

  @return EFI_SUCCESS: Routine success.
  @return EFI_BUFFER_TOO_SMALL: The hex string buffer is too small.
**/
EFI_STATUS
BufferToHexArray (
  IN OUT CHAR16   *Str,
  IN     UINTN    StrLen,
  IN     UINT8    *Buf,
  IN     UINTN    Len
  );

#endif
