/** @file
  Utility function support for WiFi Profile Sync driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "WifiProfileSyncUtils.h"

/**
  Converts CSME provided encryption method to WiFiConnectionManager value

  @param[in] EncryptionMethod  - CSME provided encryption method

  @return The corresponding WiFiConnectionManager cipher value
**/
UINT8
MapEncryptionToCipher (
  IN UINT32   EncryptionMethod
  )
{
  switch (EncryptionMethod) {
    case EncryptionMethodNone:
      return EncryptionMethodUnknown;
    case EncryptionMethodWep:
      return Ieee80211PairwiseCipherSuiteWEP40;
    case EncryptionMethodTkip:
      return Ieee80211PairwiseCipherSuiteTKIP;
    case EncryptionMethodCcmp:
      return Ieee80211PairwiseCipherSuiteCCMP;
    case EncryptionMethodGcmp256:
      return Ieee80211PairwiseCipherSuiteUseGroupCipherSuite;
    default:
      return EncryptionMethodUnknown;
  }
}

/**
  Converts CSME provided  AuthenticationProtocol_eapMethod to WiFiConnectionManager EapAuthMethod value

  @param[in] AuthenticationProtocolEapMethod  - CSME provided encryption method

  @return The corresponding WiFiConnectionManager EAP Method value
**/
UINT8
MapEapAuthMethod (
  IN UINT32   AuthenticationProtocolEapMethod
  )
{
  switch (AuthenticationProtocolEapMethod) {
    case AuthenticationProtocolEapMethodDisabled:
      return AuthenticationProtocolEapMethodDisabled;
    case AuthenticationProtocolEapMethodTtls:
      return EAP_AUTH_METHOD_TTLS;
    case AuthenticationProtocolEapMethodPeap:
      return EAP_AUTH_METHOD_PEAP;
    case AuthenticationProtocolEapMethodTls:
      return EAP_AUTH_METHOD_TLS;
    default:
      return AuthenticationProtocolEapMethodUnknown;
  }
}

/**
  Converts CSME provided authentication method to WiFiConnectionManager value

  @param[in] AuthenticationMethod  - CSME provided authenticaion method

  @return The corresponding WiFiConnectionManager AKM value
**/
UINT8
MapAuthenticationToAKM (
  IN UINT32   AuthenticationMethod
  )
{
  switch (AuthenticationMethod) {
    case AuthenticationMethodOpen:
      return AuthenticationMethodUnknown;
    case AuthenticationMethodSharedKey:
      return Ieee80211AkmSuitePSK;
    case AuthenticationMethodWpa:
      return Ieee80211AkmSuite8021XOrPMKSA;
    case AuthenticationMethodWpaPsk:
      return Ieee80211AkmSuitePSK;
    case AuthenticationMethodRsn:
      return Ieee80211AkmSuite8021XOrPMKSA;
    case AuthenticationMethodRsnPsk:
      return Ieee80211AkmSuitePSK;
    case AuthenticationMethodWpa3Sae: // not currently supported
    case AuthenticationMethodWpa3Owe: // not currently supported
    default:
      return AuthenticationMethodUnknown;
  }
}

/**
  Converts CSME provided Inner/Secondary EAP method to WiFiConnectionManager value

  @param[in] AuthInnerMethod  - CSME provided Inner/Secondary EAP method

  @return The corresponding WiFiConnectionManager secondary/inner EAP method value
**/
UINT8
MapAuthInnerMethodToEapSecondAuthMethod (
  IN UINT32   AuthInnerMethod
  )
{
  switch (AuthInnerMethod) {
    case AuthenticationProtocolInnerMethodNone:
      return AuthenticationProtocolInnerMethodNone;
    case AuthenticationProtocolInnerMethodMschapv2:
      return EAP_SEAUTH_METHOD_MSCHAPV2;
    case AuthenticationProtocolInnerMethodGtc:
      return AuthenticationProtocolInnerMethodUnknown;
    case AuthenticationProtocolInnerMethodTls:
      return AuthenticationProtocolInnerMethodUnknown;
    default:
      return AuthenticationProtocolInnerMethodUnknown;
  }
}

/**
  Converts the low nibble of a byte  to hex unicode character.

  @param[in] Nibble - lower nibble of a byte.

  Hex unicode character.

**/
CHAR16
NibbleToHexChar (
  IN UINT8      Nibble
  )
{
  Nibble &= 0x0F;
  if (Nibble <= 0x9) {
    return (CHAR16)(Nibble + L'0');
  }
  return (CHAR16)(Nibble - 0xA + L'a');
}

/**
  Converts binary buffer to Unicode string.
  At a minimum, any blob of data could be represented as a hex string.

  @param[in, out] Str   - Pointer to the string.
  @param[in] DestLen    - Length in bytes of buffer to hold the hex string. Includes tailing '\0' character.
                            If routine return with EFI_SUCCESS, containing length of hex string buffer.
                            If routine return with EFI_BUFFER_TOO_SMALL, containg length of hex string buffer desired.
  @param[in] Buf        - Buffer to be converted from.
  @param[in] Len        - Length in bytes of the buffer to be converted.

  @return EFI_SUCCESS: Routine success.
  @return EFI_BUFFER_TOO_SMALL: The hex string buffer is too small.
**/
EFI_STATUS
BufferToHexArray (
  IN OUT CHAR16   *Str,
  IN     UINTN    DestLen,
  IN     UINT8    *Buf,
  IN     UINTN    Len
  )
{
  UINTN       Idx;
  UINTN       StrIdx;

  if (DestLen < ((Len * 2) + 1)) {
    return EFI_BUFFER_TOO_SMALL;
  }

  StrIdx = 0;
  for (Idx = 0; Idx < Len; Idx++) {
    Str[StrIdx] = NibbleToHexChar ((UINT8)(Buf[Idx] >> 4));
    StrIdx++;
    Str[StrIdx] = NibbleToHexChar ((UINT8)Buf[Idx]);
    StrIdx++;
  }
  Str[StrIdx] = L'\0';
  return EFI_SUCCESS;
}