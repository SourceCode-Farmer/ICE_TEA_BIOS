/**@file
  Secure Erase Library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _SECURE_ERASE_DXE_LIB_H_
#define _SECURE_ERASE_DXE_LIB_H_

#define SECURE_ERASE_FEATURE                                0
#define REMOTE_PLATFORM_ERASE_FEATURE                       1

/**
  Attempts to erase ATA and NVMe drives.
  This function should be called as a BDS Phase Event, once all the devices/protocols are available.

  @param[in]   Feature             Feature Type to decide code flow
  @param[in]   IsRealEraseMode     Perform erase in Real mode or Simulation mode

  @retval      EFI_SUCCESS         Erase device succeed
  @retval      EFI_UNSUPPORTED     The device is not supported
  @retval      EFI_ACCESS_DENIED   User has entered wrong password too many times
  @retval      EFI_ABORTED         The device is supported, but the system
                                   has failed to erase it
**/
EFI_STATUS
PerformSsdErase (
  IN UINT8             Feature,
  IN BOOLEAN           IsRealEraseMode
  );

#endif
