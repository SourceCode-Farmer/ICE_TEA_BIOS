/** @file
  Intel One Click Recovery WiFi Profile Sync Profile Protocol.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _WIFI_PROFILE_SYNC_PROTOCOL_H_
#define _WIFI_PROFILE_SYNC_PROTOCOL_H_

#include <WifiConnectionManagerDxe/WifiConnectionMgrConfig.h>

//
// Extern the GUID for protocol users.
//
extern EFI_GUID   gWiFiProfileSyncProtocolGuid;

typedef
EFI_STATUS
(EFIAPI *WIFI_PROFILE_GET) (
  IN OUT  WIFI_MGR_NETWORK_PROFILE  *Profile
  );

typedef
VOID
(EFIAPI *WIFI_SET_CONNECT_STATE) (
  IN EFI_80211_CONNECT_NETWORK_RESULT_CODE  ConnectionStatus
  );

typedef
EFI_STATUS
(EFIAPI *WIFI_GET_CONNECT_STATE) (
  VOID
  );

typedef struct {
  UINT32                      Revision;
  WIFI_SET_CONNECT_STATE      WifiProfileSyncSetConnectState;
  WIFI_GET_CONNECT_STATE      WifiProfileSyncGetConnectState;
  WIFI_PROFILE_GET            WifiProfileSyncGetProfile;
} WIFI_PROFILE_SYNC_PROTOCOL;

/**
  Intel WiFi Profile Protocol revision number

  Revision 1:   Initial version
**/
#define  WIFI_PROFILE_SYNC_PROTOCOL_REVISION     1

#endif
