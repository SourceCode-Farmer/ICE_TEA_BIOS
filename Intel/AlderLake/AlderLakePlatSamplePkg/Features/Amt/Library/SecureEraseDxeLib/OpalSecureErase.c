/**@file
  Opal Secure Erase File.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Protocol/StorageSecurityCommand.h>
#include <Library/TcgStorageOpalLib.h>
#include "OpalSecureErase.h"
#include "Ui.h"

#define OPAL_MSID_LENGTH               128
#define OPAL_MAX_PASSWORD_SIZE         32
#define MAX_PASSWORD_TRY_COUNT         5

/**
  Check if the device is Opal Locked and revert Opal feature.

  @param[in]  RsePassword            Password received from FW.
  @param[in]  DeviceString           Device string to display in the header.

  @retval EFI_SUCCESS                Successfully performed operation.
  @retval EFI_DEVICE_ERROR           A device error occurred while attempting to send the Opal Command.
  @retval EFI_ACCESS_DENIED          Password is not correct.
**/
EFI_STATUS
RevertOpalFeature (
  IN CHAR8                             *RsePassword,
  IN CHAR16                            *DeviceString
  )
{
  EFI_STATUS                             Status;
  UINTN                                  HandleNum;
  UINTN                                  Index;
  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL  *Sscp;
  EFI_HANDLE                             *SscpHandles;
  OPAL_SESSION                           Session;
  OPAL_DISK_SUPPORT_ATTRIBUTE            SupportedAttributes;
  TCG_LOCKING_FEATURE_DESCRIPTOR         LockingFeature;
  UINT16                                 OpalBaseComId;
  TCG_RESULT                             TcgResult;
  UINT32                                 PasswordLength;
  CHAR8                                  Password[OPAL_MAX_PASSWORD_SIZE + 1];
  BOOLEAN                                PasswordFailed;
  UINT32                                 MsidLength;             // Byte length of MSID Pin for device
  UINT8                                  Msid[OPAL_MSID_LENGTH]; // MSID Pin for device
  UINT8                                  Count;
  EFI_INPUT_KEY                          Key;
  CHAR16                                 OpalString[MEDIA_DEVICE_MODEL_NO_MAX_LENGTH];

  PasswordLength = 0;

  DEBUG ((DEBUG_INFO, "OpalSecureErase:: %a start\n", __FUNCTION__));

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiStorageSecurityCommandProtocolGuid,
                  NULL,
                  &HandleNum,
                  &SscpHandles
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "Locate StorageSecurityCommandProtocol: Status = %r\n", Status));
    if (Status == EFI_NOT_FOUND) {
      return EFI_SUCCESS;
    } else {
      return Status;
    }
  }

  TcgResult = TcgResultSuccess;
  DEBUG ((DEBUG_INFO, "Number of detected SSCP Handles : %d\n", HandleNum));
  for (Index = 0; Index < HandleNum; Index++) {
    //
    // Get the SSP interface.
    //
    Status = gBS->HandleProtocol (
                    SscpHandles[Index],
                    &gEfiStorageSecurityCommandProtocolGuid,
                    (VOID **) &Sscp
                    );
    if (EFI_ERROR (Status)) {
      continue;
    }

    ZeroMem (&Session, sizeof (OPAL_SESSION));
    ZeroMem (&SupportedAttributes, sizeof (OPAL_DISK_SUPPORT_ATTRIBUTE));
    ZeroMem (&LockingFeature, sizeof (TCG_LOCKING_FEATURE_DESCRIPTOR));
    OpalBaseComId   = 0;
    Session.Sscp    = Sscp;

    TcgResult = OpalGetSupportedAttributesInfo (&Session, &SupportedAttributes, &OpalBaseComId);
    if (TcgResult != TcgResultSuccess) {
      if ((SupportedAttributes.Sp1 == 0) || (SupportedAttributes.Sp2 == 0)) {
        DEBUG ((DEBUG_INFO, "Device does not support TCG_OPAL_SECURITY_PROTOCOL_1 or TCG_OPAL_SECURITY_PROTOCOL_2,\n"));
        DEBUG ((DEBUG_INFO, "Skip the following unlock process\n"));
      } else {
        DEBUG ((DEBUG_INFO, "GetSupportedAttributesInfo Result = 0x%x\n", TcgResult));
      }
      continue;
    }

    DEBUG ((DEBUG_INFO, "SupportedAttributes.DataRemoval = 0x%x\n", SupportedAttributes.DataRemoval));
    DEBUG ((DEBUG_INFO, "SupportedAttributes.MediaEncryption = 0x%x\n", SupportedAttributes.MediaEncryption));

    Session.OpalBaseComId  = OpalBaseComId;

    TcgResult = OpalGetLockingInfo (&Session, &LockingFeature);
    if (TcgResult != TcgResultSuccess) {
      DEBUG ((DEBUG_INFO, "GetLockingInfo Result = 0x%x\n", TcgResult));
      continue;
    }

    if (!OpalDeviceLocked (&SupportedAttributes, &LockingFeature)) {
      DEBUG ((DEBUG_INFO, "Opal Device is not Locked, return directly\n"));
      continue;
    }

    //
    // If there are multiple Sscp instances, the devices' name can't be retrieved.
    // Because the protocol is installed on different handle other than AtaPassThru/NvmExpressPassThru,
    // use generic string as header.
    //
    ZeroMem (OpalString, sizeof (OpalString));
    if (HandleNum > 1) {
      StrCatS (OpalString, MEDIA_DEVICE_MODEL_NO_MAX_LENGTH, L"OPAL Device # ");
      UnicodeValueToStringS (OpalString + StrLen (OpalString), MEDIA_DEVICE_MODEL_NO_MAX_LENGTH, 0, Index + 1, 0);
    } else {
      StrCpyS (OpalString, MEDIA_DEVICE_MODEL_NO_MAX_LENGTH, DeviceString);
    }

    PasswordFailed = FALSE;
    ZeroMem (Password, sizeof (CHAR8) * (OPAL_MAX_PASSWORD_SIZE + 1));
    if ((AsciiStrLen (RsePassword) == 0)) {
      //
      // Request password from user
      //
      Count = 0;
      while (Count < MAX_PASSWORD_TRY_COUNT) {
        Status = PopupHddPasswordInputWindows (Password, OpalString, TRUE);
        ClearScreen ();
        if (EFI_ERROR (Status)) {
          break;
        }
        PasswordLength = (UINT32) AsciiStrLen (Password);
        TcgResult = OpalUtilUpdateGlobalLockingRange (&Session, Password, PasswordLength, FALSE, FALSE);
        if (TcgResult == TcgResultSuccess) {
          break;
        }
        Count++;
        ZeroMem (Password, sizeof (CHAR8) * (OPAL_MAX_PASSWORD_SIZE + 1));
        CreatePopUp (
          EFI_LIGHTGRAY | EFI_BACKGROUND_BLUE,
          &Key,
          L"Failed to unlock the device, please try again.",
          L"Press [Enter] to continue",
          NULL
          );
        ClearScreen ();
      }
    } else {
      //
      // Use password from host
      //
      AsciiStrCpyS (Password, OPAL_MAX_PASSWORD_SIZE, RsePassword);
      PasswordLength = (UINT32) AsciiStrLen (Password);
      TcgResult = OpalUtilUpdateGlobalLockingRange (&Session, Password, PasswordLength, FALSE, FALSE);
    }

    if (TcgResult != TcgResultSuccess) {
      DEBUG ((DEBUG_INFO, "UpdateGlobalLockingRange Result = 0x%x\n", TcgResult));
      continue;
    }

    TcgResult = OpalUtilGetMsid (&Session, Msid, OPAL_MSID_LENGTH, &MsidLength);
    if (TcgResult != TcgResultSuccess) {
      DEBUG ((DEBUG_INFO, "GetMsid Result = 0x%x\n", TcgResult));
      continue;
    }

    TcgResult = OpalUtilRevert (
                  &Session,
                  FALSE,
                  Password,
                  PasswordLength,
                  &PasswordFailed,
                  Msid,
                  MsidLength
                  );
    if (TcgResult != TcgResultSuccess) {
      DEBUG ((DEBUG_INFO, "Revert Result = 0x%x, PasswordFailed = %x\n", TcgResult, PasswordFailed));
      continue;
    }
  }

  ZeroMem (Password, sizeof (CHAR8) * (OPAL_MAX_PASSWORD_SIZE + 1));

  if (TcgResult != TcgResultSuccess) {
    DEBUG ((DEBUG_ERROR, "OpalSecureErase:: %a exit with error TcgResult = 0x%x\n", __FUNCTION__, TcgResult));
    return EFI_ACCESS_DENIED;
  } else {
    return EFI_SUCCESS;
  }
}
