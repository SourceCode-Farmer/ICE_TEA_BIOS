/**@file
  Secure Erase Library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <IndustryStandard/Pci.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/SecureEraseDxeLib.h>
#include <Library/GpioLib.h>
#include <Library/DxeAsfHeciLib.h>
#include <Library/DxeAmtHeciLib.h>
#include <Protocol/AmtReadyToBoot.h>
#include <Protocol/PciIo.h>
#include <Protocol/PlatformSpecificResetHandler.h>
#include <Protocol/SecureEraseHook.h>
#include <MeBiosPayloadHob.h>
#include "Ui.h"
#include "AtaSecureErase.h"
#include "NvmeSecureErase.h"
#include "SimulatedSecureErase.h"

GLOBAL_REMOVE_IF_UNREFERENCED CHAR16  *mRseDetailMessage;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN mResetSystemHook;

/**
  Due to platform security concern, only trusted storages are connected.
  This function is to connect all storages so they can be found and erased.
**/
VOID
ConnectAllStorages (
  VOID
  )
{
  EFI_STATUS                    Status;
  UINTN                         Index;
  UINTN                         PciIoHandleCount;
  EFI_HANDLE                    *PciIoHandleBuffer;
  EFI_PCI_IO_PROTOCOL           *PciIo;
  UINT8                         ClassCode[3];

  DEBUG ((DEBUG_INFO, "SecureErase :: %a\n", __FUNCTION__));

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPciIoProtocolGuid,
                  NULL,
                  &PciIoHandleCount,
                  &PciIoHandleBuffer
                  );
  if (EFI_ERROR (Status) || (PciIoHandleBuffer == NULL) || (PciIoHandleCount == 0)) {
    return;
  }

  for (Index = 0; Index < PciIoHandleCount; Index++) {
    Status = gBS->HandleProtocol (
                    PciIoHandleBuffer[Index],
                    &gEfiPciIoProtocolGuid,
                    (VOID **) &PciIo
                    );
    ASSERT_EFI_ERROR (Status);

    Status = PciIo->Pci.Read (
                          PciIo,
                          EfiPciIoWidthUint8,
                          PCI_CLASSCODE_OFFSET,
                          sizeof (ClassCode),
                          ClassCode
                          );
    ASSERT_EFI_ERROR (Status);
    //
    // Examine Nvm Express controller PCI Configuration table fields
    //
    if ((ClassCode[0] == PCI_IF_NVMHCI) &&
        (ClassCode[1] == PCI_CLASS_MASS_STORAGE_NVM) &&
        (ClassCode[2] == PCI_CLASS_MASS_STORAGE)) {
      gBS->ConnectController (PciIoHandleBuffer[Index], NULL, NULL, TRUE);
    }

    //
    // Connect SATA controller
    //
    if (ClassCode[2] == PCI_CLASS_MASS_STORAGE &&
        (ClassCode[1] == PCI_CLASS_MASS_STORAGE_SATADPA || ClassCode[1] == PCI_CLASS_MASS_STORAGE_RAID)) {
      gBS->ConnectController (PciIoHandleBuffer[Index], NULL, NULL, TRUE);
    }

  }
  gBS->FreePool (PciIoHandleBuffer);
}


/**
  Hook system reset to display requesting G3 cycle message.

  When PcdSkipHddPasswordPrompt / PcdSkipOpalPasswordPrompt are set and SSD is in the unlocked status,
  system is forced to shutdown due to security concerns without any message.
  This function is to display message to request user to perform G3 cycle.

  @param[in]  ResetType         The type of reset to perform.
  @param[in]  ResetStatus       The status code for the reset.
  @param[in]  DataSize          The size, in bytes, of ResetData.
  @param[in]  ResetData         For a ResetType of EfiResetCold, EfiResetWarm, or
                                EfiResetShutdown the data buffer starts with a Null-terminated
                                string, optionally followed by additional binary data.
                                The string is a description that the caller may use to further
                                indicate the reason for the system reset.
                                For a ResetType of EfiResetPlatformSpecific the data buffer
                                also starts with a Null-terminated string that is followed
                                by an EFI_GUID that describes the specific type of reset to perform.
**/
VOID
EFIAPI
SecureEraseResetSystem (
  IN EFI_RESET_TYPE           ResetType,
  IN EFI_STATUS               ResetStatus,
  IN UINTN                    DataSize,
  IN VOID                     *ResetData OPTIONAL
  )
{
  EFI_STATUS                  Status;
  SECURE_ERASE_HOOK_PROTOCOL  *SecureErasHook;

  DEBUG ((DEBUG_INFO, "SecureErase :: %a()\n", __FUNCTION__));

  if (!mResetSystemHook) {
    return;
  }

  ClearScreen ();
  Status = gBS->LocateProtocol (&gSecureEraseHookProtocolGuid, NULL, (VOID **) &SecureErasHook);
  if (!EFI_ERROR (Status)) {
    SecureErasHook->ResetHook ();
  } else {
    DEBUG ((DEBUG_INFO, "SecureErase :: SecureErasHook protocol is not found, request user to perform G3 cycle \n"));
    OutputString (L"!!G3 cycle is required to restore storage device security state.", 0, 6, EFI_BACKGROUND_BLACK | EFI_WHITE);
    CpuDeadLoop ();
  }
  return;
}


/**
  Hook the system reset to properly handle SSD security state.

  @param[in]  Event     Event whose notification function is being invoked
  @param[in]  Context   Pointer to the notification function's context
**/
VOID
EFIAPI
SecureEraseOnResetHook (
  IN EFI_EVENT                      Event,
  IN VOID                           *Context
  )
{
  EFI_STATUS                                     Status;
  EDKII_PLATFORM_SPECIFIC_RESET_HANDLER_PROTOCOL *ResetHandler;

  Status = gBS->LocateProtocol (
                  &gEdkiiPlatformSpecificResetHandlerProtocolGuid,
                  NULL,
                  (VOID **) &ResetHandler
                  );

  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "SecureErase :: %a()\n", __FUNCTION__));
    ResetHandler->RegisterResetNotify (ResetHandler, SecureEraseResetSystem);
    gBS->CloseEvent (Event);
  }
}

/**
  Attempts to erase ATA and NVMe drives.
  This function should be called as a BDS Phase Event, once all the devices/protocols are available.

  @param[in]   Feature             Feature Type to decide code flow
  @param[in]   IsRealEraseMode     Perform erase in Real mode or Simulation mode

  @retval      EFI_SUCCESS         Erase device succeed
  @retval      EFI_UNSUPPORTED     The device is not supported
  @retval      EFI_ACCESS_DENIED   User has entered wrong password too many times
  @retval      EFI_ABORTED         The device is supported, but the system
                                   has failed to erase it
**/
EFI_STATUS
PerformSsdErase (
  IN UINT8             Feature,
  IN BOOLEAN           IsRealEraseMode
  )
{
  EFI_STATUS    Status;
  EFI_STATUS    AtaStatus;
  EFI_STATUS    NvmeStatus;
  EFI_TPL       OldTpl;
  CHAR8         RsePassword[RSE_PASSWORD_MAX_LENGTH + 1];
  UINT32        AsfBiosStatus;
  VOID          *Registration;

  DEBUG ((DEBUG_INFO, "SecureErase :: PerformSsdErase\n"));
  AsfBiosStatus = AsfRbsInProgress;

  OldTpl = EfiGetCurrentTpl ();
  gBS->RestoreTPL (TPL_APPLICATION);
  InitUiLib ();
  SetScreenTitle (L"Secure erase");
  ClearScreen ();

  if (IsRealEraseMode) {
    mRseDetailMessage = AllocateZeroPool (MAX_STRING_LEN * sizeof (CHAR16));
    ASSERT (mRseDetailMessage != NULL);

    //
    // Retrieve password from host.
    //
    ZeroMem (RsePassword, sizeof (CHAR8) * (RSE_PASSWORD_MAX_LENGTH + 1));
    GetRsePasswordV2 (RsePassword);

    //
    // Power off SSD is required to restore its security state.
    // There are two scenarios we need to power off SSD:
    // 1. When PcdSkipHddPasswordPrompt / PcdSkipOpalPasswordPrompt are set and SSD is in the unlocked status,
    //    system is forced shutdown for security concern.
    // 2. When ATA SSD's SecurityStatus.Frozen is set.
    //
    // Due to board design, SSD may not power off during system reset/shutdown,
    // So we register a reset hook to use GPIO power pins to power off SSD and perform warm reset.
    // If these power pins are not assigned, G3 cycle is still mandatory.
    //
    EfiCreateProtocolNotifyEvent (
      &gEdkiiPlatformSpecificResetHandlerProtocolGuid,
      TPL_CALLBACK,
      SecureEraseOnResetHook,
      NULL,
      &Registration
      );
    //
    // When connecting storages, system might be forced shutdown for above scenario #1 without any message.
    // Set mResetSystemHook TRUE as a indicator that we should perform the hook in SecureEraseResetSystem (),
    // and set it FALSE after storage connection done to prevent any unexpected message displayed when reset system.
    //
    mResetSystemHook = TRUE;
    ConnectAllStorages ();
    mResetSystemHook = FALSE;

    AtaStatus = EraseAtaDevice (RsePassword);
    DEBUG ((DEBUG_INFO, "SecureErase :: AtaStatus = %r\n", AtaStatus));
    NvmeStatus = EraseNvmeDevice (RsePassword);
    DEBUG ((DEBUG_INFO, "SecureErase :: NvmeStatus = %r\n", NvmeStatus));
    Status = AtaStatus | NvmeStatus;
    //
    // Remove password received from ASF.
    //
    ZeroMem (RsePassword, sizeof (CHAR8) * (RSE_PASSWORD_MAX_LENGTH + 1));
  } else {
    Status = SimulateSecureErase ();
  }

  switch (Status) {
    case EFI_SUCCESS:
      AsfBiosStatus = AsfRbsSuccess;
      break;

    case EFI_ACCESS_DENIED:
      AsfBiosStatus = AsfRbsAccessDenied;
      break;

    case EFI_UNSUPPORTED:
      AsfBiosStatus = AsfRbsUnsupported;
      break;

    default:
      AsfBiosStatus = AsfRbsGeneralFailure;
      break;
  }

  if (Feature == SECURE_ERASE_FEATURE) {
    DEBUG ((DEBUG_INFO, "SecureErase :: Report Secure Erase Operation Status: %r\nSecureErase :: EraseDevices end\n", Status));
    if (!EFI_ERROR (Status)) {
      ClearBootOptions ();
    }
    SendRsePetAlert (Status);
    ReportBiosStatus (AsfBiosStatus);
  }

  if (IsRealEraseMode) {
    (Feature == REMOTE_PLATFORM_ERASE_FEATURE) ? PrintRpeSummary (Status) : PrintSummary (Status);
  } else {
    SimulateSecureEraseDone ();
  }
  gBS->RaiseTPL (OldTpl);
  return Status;
}