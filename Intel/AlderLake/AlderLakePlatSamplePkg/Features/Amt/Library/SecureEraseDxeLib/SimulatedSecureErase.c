/**@file
  Simulated Secure Erase implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "SimulatedSecureErase.h"

/**
  Display "erase in progress" message to users.
**/
EFI_STATUS
SimulateSecureErase (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "SecureErase :: SimulateSecureErase\n"));
  ProgressBarDialog (L"Erasing SSD", 5);
  return EFI_SUCCESS;
}

/**
  Display "erase completed" message to users.
**/
VOID
SimulateSecureEraseDone (
  VOID
  )
{
  UINTN    Index = 0;
  CHAR16   AfterEraseMsgFormat[] = L"Disk erase completed. Exiting SSD Erase in %d seconds";
  CHAR16   AfterEraseMsg[(sizeof (AfterEraseMsgFormat) / sizeof (CHAR16)) + 5];

  DEBUG ((DEBUG_INFO, "SecureErase :: SimulateSecureEraseDone\n"));
  for (Index = 10; Index != 0 ; Index--) {
    UnicodeSPrint (AfterEraseMsg, sizeof (AfterEraseMsg), AfterEraseMsgFormat, Index);
    ClearScreen ();
    OutputString (AfterEraseMsg, 10, 10, EFI_BACKGROUND_BLACK | EFI_WHITE);
    gBS->Stall (1000000);
  }

  FillScreen (EFI_BACKGROUND_BLACK | EFI_WHITE);
  gBS->Stall (1000000);
  OutputString (L"Operating system not found...", 1, 1, EFI_BACKGROUND_BLACK | EFI_WHITE);

  return;
}
