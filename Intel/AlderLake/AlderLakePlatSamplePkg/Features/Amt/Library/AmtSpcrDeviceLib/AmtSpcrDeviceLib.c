/** @file
  Implementation for SpcrDeviceLib with AMT support.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Include/Uefi.h>
#include <Protocol/DevicePath.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/SpcrDeviceLib.h>

#if FixedPcdGetBool(PcdAmtEnable) == 1
#include <Protocol/AmtWrapperProtocol.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#endif

/**
  Get a Serial Port device used for SPCR.
  The caller should call FreePool() to free the memory if return value is not NULL.

  This function will try to read the EFI variable OutOfBand,
  if empty, then try to get the Serial Over Lan device of AMT,
  and save it to OutOfBand again if get it.

  @retval NULL          Can not get device.
  @retval Others        A serial port device path.

**/
EFI_DEVICE_PATH_PROTOCOL *
EFIAPI
GetSpcrDevice (
  VOID
  )
{
  EFI_DEVICE_PATH_PROTOCOL *DevicePath;
#if FixedPcdGetBool(PcdAmtEnable) == 1
  AMT_WRAPPER_PROTOCOL     *AmtWrapper;
  EFI_STATUS               Status;
#endif

  DevicePath = NULL;
  GetVariable2 (L"OutOfBand", &gOutOfBandGuid, (VOID **) &DevicePath, NULL);
  if (DevicePath != NULL) {
    return DevicePath;
  }

#if FixedPcdGetBool(PcdAmtEnable) == 1
  //
  // Get devcie path info from sol type console device.
  //
  Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);
  if (EFI_ERROR (Status)) {
    return NULL;
  }
  if (AmtWrapper->IsSolEnabled ()) {
    return NULL;
  }
  AmtWrapper->Get ((VOID **) &DevicePath);
  if (DevicePath == NULL) {
    return NULL;
  }

  gRT->SetVariable (
          L"OutOfBand",
          &gOutOfBandGuid,
          EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
          GetDevicePathSize (DevicePath),
          DevicePath
          );
  // The memory returned by AmtWrapper->Get() can't be FreePool(), so duplicate it.
  DevicePath = DuplicateDevicePath (DevicePath);

#endif // FixedPcdGetBool(PcdTxtEnable) == 1
  return DevicePath;
}
