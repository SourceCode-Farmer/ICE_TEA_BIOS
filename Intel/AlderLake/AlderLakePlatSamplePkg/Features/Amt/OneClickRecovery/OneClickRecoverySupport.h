/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/** @file
  Intel One Click Recovery Support.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _OCR_SUPPORT_H_
#define _OCR_SUPPORT_H_

#include <Uefi.h>
#include <AsfMsgs.h>
#include <Protocol/OneClickRecoveryProtocol.h>
//[-start-211111-IB09480171-add]//
#include <Guid/AdmiSecureBoot.h>
//[-end-211111-IB09480171-add]//
//[-start-211215-IB09480180-add]//
#include <Protocol/SimpleFileSystem.h>
//[-end-211215-IB09480180-add]//

//
// Guid for OCR Boot Settings
//
#define OCR_BOOT_SETTINGS_GUID \
  { \
    0x6f31fbcb, 0xc924, 0x496f, {0x83, 0xc6, 0x8f, 0xa4, 0xf5, 0xdc, 0x79, 0x9d} \
  }

//
// OCR Boot Setting Name
//
#define OCR_BOOT_SETTINGS_NAME      L"OcrBootSettings"

//
// OCR Boot Options Exists
//
#define BOOT_OPTION_EXISTS_NONE           0
#define BOOT_OPTION_EXISTS                1

//
// OCR Restore Secure Boot Settings
//
#define RESTORE_SECURE_BOOT_NONE          0
#define RESTORE_SECURE_BOOT_ENABLED       1

//
// OCR Capabilities
//
#define ONE_CLICK_RECOVERY_HTTPS_BOOT_CAPABLE         1
#define ONE_CLICK_RECOVERY_PBA_BOOT_CAPABLE           1
#define ONE_CLICK_RECOVERY_WIN_RE_BOOT_CAPABLE        1
#define ONE_CLICK_RECOVERY_DIS_SEC_BOOT_CAPABLE       1
#define ONE_CLICK_RECOVERY_WIFI_PROFILE_SYNC_CAPABLE  1

//
// OCR HTTPS Response and Request Timeout Values
//
#define HTTP_BOOT_TIMEOUT_MIN        500       // 0.5sec for minimum allowed latency with http boot source.
#define HTTP_BOOT_TIMEOUT_MAX        120000    // 120sec for maximum allowed latency with http boot source.
#define HTTP_BOOT_TIMEOUT_DEFAULT    5000      // 5sec for default value of allowed latency.

//
// OCR Boot Setting Structure
//
typedef struct {
  UINT8 BootOptionExists;           ///< Boot Option Exists
  UINT8 RestoreSecureBootSetting;   ///< Restore Secure Boot Setting
} OCR_BOOT_SETTINGS;

//
// OEM Boot Option Structure
//
typedef struct {
  CHAR16      *Description; ///< Description
  CHAR16      *DevicePath;  ///< Device Path
} OEM_BOOT_OPTION;

//
// OCR Boot Option Parameter Type Structure
//
typedef struct {
  UINT16        VendorId;    ///< Vendor ID
  UINT16        ParamTypeId; ///< Parameter Type ID
} OCR_PARAMETER_TYPE;

//
// OCR Boot Option Parameter TLV Header Stucture
//
typedef struct {
  OCR_PARAMETER_TYPE  ParamType;  ///< OCR Parameter Type
  UINT32              Length;     ///< Length of the value array
} OCR_PARAMETER_TLV_HEADER;

//
// OCR Boot Option Parameter TLV Stucture
//
typedef struct {
  OCR_PARAMETER_TLV_HEADER  Header;   ///< OCR Parameter Type
  UINT8                     Value[0]; ///< OCR Paramter data
} OCR_PARAMETER_TLV;

#define MAX_UEFI_OPTIONAL_DATA  50

//
// OCR Boot Option Parameter Types
//
typedef enum {
  NoType,
  EfiNetworkDevicePathParamType,
  EfiFileDeviceParamType,
  EfiDevicePathLenParamType,
  BootImageHashSha256ParamType,
  BootImageHashSha384ParamType,
  BootImageHashSha512ParamType,
  EfiBootOptionalDataParamType = 10,
  HttpsCertSyncRootCaParamType = 20,
  HttpsCertServerNameParamType,
  HttpsServerNameVerifyMethodParamType,
  HttpsServerCertHashSha256ParamType,
  HttpsServerCertHashSha384ParamType,
  HttpsServerCertHashSha512ParamType,
  HttpsRequestTimeOutParamType = 30,
  HttpsDigestUserNameParamType = 40,
  HttpsDigestPasswordParamType
} OCR_PARAM_TYPE;


//
// OCR Boot Option Structure
//
typedef struct {
  CHAR16                DevicePath[MAX_UEFI_BOOT_OPTION_DEV_PATH_LENGTH]; ///< Device Path
  CHAR16                Description[MAX_UEFI_BOOT_OPTION_DESC_LENGTH];    ///< Description
  UINT8                 *OptionalData;                                    ///< Optional Data
  UINT32                OptionalDataSize;                                 ///< Optional Data Size
} OCR_BOOT_OPTION;

/**
  Gets Intel One click Recovery BIOS Capabilities

  @retval ONE_CLICK_RECOVERY_CAPS       One Click Recovery BIOS Capabilites
**/
ONE_CLICK_RECOVERY_CAP
EFIAPI
OneClickRecoveryCapabilities (
  VOID
  );

/**
  The Intel One Click Recovery Setup main function.

  The function does the necessary work to Setup the One Click Recovery feature.

  @retval     EFI_SUCCESS       One Click Recovery tasks have been ran
  @retval     EFI_UNSUPPORTED   One Click Recovery is not supported
**/
EFI_STATUS
EFIAPI
OneClickRecoveryMain (
  VOID
  );

/**
  Saves OCR Uefi Boot Option from AMT.

  Needs to Be called before Clear Boot Options.

  @retval     EFI_SUCCESS       Saved Boot Option
**/
EFI_STATUS
EFIAPI
OneClickRecoverySaveUefiBootOption (
  IN ASF_BOOT_OPTIONS           AsfBootOptions
  );

/**
  Creates OCR Description Title

  @param[in]  Description            Title to Append to boot option
  @param[out] OcrDescription         OCR Description
  @param[in]  MaxOcrDescriptionSize  MAX OCR Description

  @retval  EFI_SUCCESS               Created OCR Description
  @retval  EFI_INVALID_PARAMETER     Pointers are null
  @retval  EFI_BUFFER_TOO_SMALL      Description will be larger max size
**/
EFI_STATUS
CreateOcrBootOptionDescription (
  IN CHAR16*       Description,
  OUT CHAR16*      OcrDescription,
  IN UINT32        MaxOcrDescriptionSize
  );

/**
  Checks for "https://" string in the given url

  @param[in] Url        Pointer to ascii url
  @param[in] UrlLength  Length of url

  @retval TRUE          URL is HTTPS
  @retval FALSE         URL is not HTTPS
**/
BOOLEAN
IsHttpsUrl (
  IN CHAR8*   Url,
  IN UINT32   UrlLength
  );

/**
  Function to Delete x509 certificate

  @param[in] OcrTlsGuid         Guid associated with certificate

  @retval EFI_SUCCESS           Deleted x509 Certificate
  @retval EFI_OUT_OF_RESOURCES  Out of resources
**/
EFI_STATUS
DeleteOcrX509Certificate (
  IN EFI_GUID  *OcrTlsGuid
  );

/**
  Function to enroll x509 certificate

  @param[in] X509Data            X509 Certificate Data
  @param[in] X509DataSize        X509 Certificate Data Size
  @param[in] OcrTlsGuid          Guid associated with certificate

  @retval EFI_SUCCESS            Successfully enrolled x509 Certificate
  @retval EFI_OUT_OF_RESOURCES   Out of resources
**/
EFI_STATUS
EnrollOcrX509Certificate (
  IN VOID      *X509Data,
  IN UINTN     X509DataSize,
  IN EFI_GUID  *OcrTlsGuid
  );

/**
  Function to find Http Device Path, it will favor ip4 over ip6

  @param[out] HttpDevicePath     Http Device Path, NULL if one could not be located

  @retval EFI_SUCCESS            Successful search for Http Device Path
  @retval EFI_NOT_FOUND          Couldn't to find any simple network devices
  @retval EFI_OUT_OF_RESOURCES   Out of resources
**/
EFI_STATUS
SearchHttpDevicePath (
  OUT EFI_DEVICE_PATH_PROTOCOL      **HttpDevicePath
  );

/**
  Add OEM PBA Boot Option to Boot Option List

  @param[in, out] UefiBootOptions       Boot Options
  @param[in, out] NumOfUefiBootOptions  Number of UEFI boot options

  @retval  EFI_SUCCESS                  Added OCR Boot Option
  @retval  EFI_BUFFER_TOO_SMALL         Not enough room for boot options
  @retval  EFI_INVALID_PARAMETER        parameters are null pointers
**/
EFI_STATUS
AddOemPbaBootOptions (
  IN OUT UEFI_BOOT_OPTION    *UefiBootOptions,
  IN OUT UINT16              *NumOfUefiBootOptions
  );

/**
  Finds All WinRe Boot Options and adds them as boot options

  @param[in, out] UefiBootOptions      Boot Options
  @param[in, out] NumUefiBootOptions   Number of UEFI boot options

  @retval EFI_SUCCESS                  Added winre Boot Option if exist
  @retval EFI_BUFFER_TOO_SMALL         Not enough room to add boot option
  @retval EFI_INVALID_PARAMETER        parameters are null pointers
**/
EFI_STATUS
AddWinReBootOptions (
  IN OUT UEFI_BOOT_OPTION     *UefiBootOptions,
  IN OUT UINT16               *NumUefiBootOptions
  );

/**
  Gets the timeout value for requesting and receiving the recovery OS image from
  remote source when in HTTP recovery boot flow from AMT flow and sets the value
  of PcdHttpIoTimeout to update in NetworkPkg HttpBoot.

  @param[in] OcrParamTlv      Boot Option for HTTP boot image request and
                              response timeout value

  @retval EFI_SUCCESS         Successfully recieved timeout value and set PCD
  @retval other               Failed to get and set timeout value
**/
EFI_STATUS
SetHttpTimeout (
  IN OCR_PARAMETER_TLV    *OcrParamTlv
  );

//[-start-211111-IB09480171-add]//
UINT8
AsfSecureBootChangedSmiCall (
    IN     UINT8                 Action,   // rcx
    IN     UINT16                SmiPort   // rdx
  );
//[-end-211111-IB09480171-add]//

//[-start-211215-IB09480180-add]//
/**
  Checks to see if file exist in file system

  @param[in]    RootFs       Root File System
  @param[in]    FileName     File Name
  @param[out]   FileExist    File Exist

  @retval  EFI_SUCCESS          Successfuly determine if file exist or not
  @retval  EFI_OUT_OF_RESOURCES Ran out of resources
**/
EFI_STATUS
DoesFileExist (
  IN  EFI_FILE                        *RootFs,
  IN  CHAR16                          *FileName,
  OUT BOOLEAN                         *FileExist
  );

EFI_STATUS
UpdatePbaDevPath (
  IN OUT OCR_BOOT_OPTION      *BootOption
  );
//[-end-211215-IB09480180-add]//

#endif //_OCR_SUPPORT_H_
