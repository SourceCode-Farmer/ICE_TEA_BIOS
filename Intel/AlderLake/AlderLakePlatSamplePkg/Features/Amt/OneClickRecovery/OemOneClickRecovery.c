/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/** @file
  Intel One Click Reocvery OEM Implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <OneClickRecoverySupport.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
//[-start-211215-IB09480180-add]//
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DevicePathLib.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/BlockIo.h>
#include <Guid/FileInfo.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/PrintLib.h>
//[-end-211215-IB09480180-add]//

//
// OEM PBA Boot Options List
//
//[-start-211215-IB09480180-modify]//
CONST OEM_BOOT_OPTION OemBootOptionsList[] = {
  // {Description,  DevicePath}
  // Example:
  //  {L"OEM PBA", L"netboot.xyz-snp.efi"},
  {NULL, NULL} // End of list
};
//[-end-211215-IB09480180-modify]//

/*
  Add OEM PBA Boot Option to Boot Option List

  @param[in, out] UefiBootOptions       Boot Options
  @param[in, out] NumOfUefiBootOptions  Number of UEFI boot options

  @retval EFI_SUCCESS                   Added OCR Boot Option
  @retval EFI_BUFFER_TOO_SMALL          Not enough room for boot options
  @retval EFI_INVALID_PARAMETER         parameters are null pointers
*/
EFI_STATUS
AddOemPbaBootOptions (
  IN OUT UEFI_BOOT_OPTION    *UefiBootOptions,
  IN OUT UINT16              *NumOfUefiBootOptions
  )
{
  UINTN       Index;

  if ((UefiBootOptions == NULL) || (NumOfUefiBootOptions == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  Index = 0;
  while ((OemBootOptionsList[Index].Description != NULL) && (OemBootOptionsList[Index].DevicePath != NULL)) {
    if (*NumOfUefiBootOptions < MAX_UEFI_BOOT_OPTIONS) {
      UnicodeStrToAsciiStrS (
        OemBootOptionsList[Index].Description,
        (CHAR8 *) UefiBootOptions[*NumOfUefiBootOptions].Description,
        MAX_UEFI_BOOT_OPTION_DESC_LENGTH * sizeof (CHAR8)
        );
      UefiBootOptions[*NumOfUefiBootOptions].DescriptionLength = (UINT16)StrLen (OemBootOptionsList[Index].Description);

      UnicodeStrToAsciiStrS (
        OemBootOptionsList[Index].DevicePath,
        (CHAR8 *) UefiBootOptions[*NumOfUefiBootOptions].EfiDevicePath,
        MAX_UEFI_BOOT_OPTION_DEV_PATH_LENGTH * sizeof (CHAR8)
        );
      UefiBootOptions[*NumOfUefiBootOptions].DevicePathLength = (UINT16)StrLen (OemBootOptionsList[Index].DevicePath);

      UefiBootOptions[*NumOfUefiBootOptions].UefiBootOptionType = Pba;
      (*NumOfUefiBootOptions)++;
    } else {
      DEBUG ((DEBUG_ERROR, "No free space to put Oem Pba entry in\n"));
      return EFI_BUFFER_TOO_SMALL;
    }

    Index++;
  }

  return EFI_SUCCESS;
}

/**
  Gets Intel One click Recovery BIOS Capabilities

  @retval ONE_CLICK_RECOVERY_CAPS       One Click Recovery BIOS Capabilites
**/
ONE_CLICK_RECOVERY_CAP
EFIAPI
OneClickRecoveryCapabilities (
  VOID
  )
{
  ONE_CLICK_RECOVERY_CAP  OcrCap;

  OcrCap.Data = 0;

  OcrCap.Bits.OcrBootHttps     = ONE_CLICK_RECOVERY_HTTPS_BOOT_CAPABLE;
  OcrCap.Bits.OcrBootPba       = ONE_CLICK_RECOVERY_PBA_BOOT_CAPABLE;
  OcrCap.Bits.OcrBootWinRe     = ONE_CLICK_RECOVERY_WIN_RE_BOOT_CAPABLE;
  OcrCap.Bits.OcrAmtDisSecBoot = ONE_CLICK_RECOVERY_DIS_SEC_BOOT_CAPABLE;
  #if FixedPcdGetBool (PcdWifiProfileSyncEnable) == 1
    OcrCap.Bits.OcrWifiProfile = ONE_CLICK_RECOVERY_WIFI_PROFILE_SYNC_CAPABLE;
  #else
    OcrCap.Bits.OcrWifiProfile = 0;
  #endif
  return OcrCap;
}

//[-start-211215-IB09480180-add]//
EFI_STATUS
UpdatePbaDevPath (
  IN OUT OCR_BOOT_OPTION      *BootOption
  )
{
  EFI_STATUS                      Status;
  UINTN                           Index;
  UINTN                           HandleCount;
  EFI_HANDLE                      *HandleBuffer;
  EFI_DEVICE_PATH_PROTOCOL        *HandleFilePath;
  EFI_FILE                        *RootFs;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *SimpleFileSystem;
  EFI_BLOCK_IO_PROTOCOL           *BlkIo;
  BOOLEAN                         FileExist;
  CHAR16                          *DevicePath;  
  CHAR16                          PbaDevicePath[MAX_UEFI_BOOT_OPTION_DEV_PATH_LENGTH];

  if (BootOption == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  UnicodeSPrint (
    PbaDevicePath,
    MAX_UEFI_BOOT_OPTION_DEV_PATH_LENGTH*sizeof(CHAR16),
    L"%s",
    BootOption->DevicePath
    );

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleFileSystemProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot Locate SimpleFileSystemProtocol\n"));
    return Status;
  }

  //
  // Find and open the file in removable media disk.
  //
  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiBlockIoProtocolGuid,
                    (VOID **) &BlkIo
                    );
    if (EFI_ERROR (Status)) {
      continue;
    }

    if (BlkIo->Media->MediaPresent) {
      Status = gBS->HandleProtocol (
                      HandleBuffer[Index],
                      &gEfiSimpleFileSystemProtocolGuid,
                      (VOID **) &SimpleFileSystem
                      );
      if (EFI_ERROR (Status)) {
        continue;
      }

      Status = SimpleFileSystem->OpenVolume (
                                   SimpleFileSystem,
                                   &RootFs
                                   );
      if (EFI_ERROR (Status)) {
        continue;
      }

      // Look for PBA File
      Status = DoesFileExist (RootFs, PbaDevicePath, &FileExist);
      if (EFI_ERROR (Status) || !FileExist) {
        continue;
      }

      Status =  gBS->HandleProtocol (
                       HandleBuffer[Index],
                       &gEfiDevicePathProtocolGuid,
                       (VOID **) &HandleFilePath
                       );
      if (EFI_ERROR (Status) || (HandleFilePath == NULL)) {
        continue;
      }

      DevicePath = ConvertDevicePathToText (HandleFilePath, TRUE, TRUE);

      if (DevicePath != NULL) {
        UnicodeSPrint (
          BootOption->DevicePath,
          MAX_UEFI_BOOT_OPTION_DEV_PATH_LENGTH*sizeof(CHAR16),
          L"%s/%s",
          DevicePath,
          PbaDevicePath
          );
      }
    }
  }

  return EFI_SUCCESS;
}
//[-end-211215-IB09480180-add]//
