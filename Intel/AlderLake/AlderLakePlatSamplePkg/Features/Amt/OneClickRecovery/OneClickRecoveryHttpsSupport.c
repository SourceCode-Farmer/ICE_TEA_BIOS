/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/** @file
  Intel One Click Recovery HTTPS Support.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <OneClickRecoverySupport.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PcdLib.h>
#include <Library/NetLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Guid/TlsAuthentication.h>
#include <Guid/ImageAuthentication.h>
#include <Protocol/PciIo.h>
#include <Protocol/WiFi2.h>
#include <Protocol/SimpleNetwork.h>
#include <Protocol/WifiProfileSyncProtocol.h>

/**
  Checks for "https://" string in the given url

  @param[in] Url        Pointer to ascii url
  @param[in] UrlLength  Length of url

  @retval TRUE          URL is HTTPS
  @retval FALSE         URL is not HTTPS
**/
BOOLEAN
IsHttpsUrl (
  IN CHAR8*   Url,
  IN UINT32   UrlLength
  )
{
  CHAR8*    TempString;
  UINT32    HttpsLength;
  UINT32    Index;
  BOOLEAN   IsHttpsString;
  CHAR8     *HttpsString;

  HttpsString = "https://";

  HttpsLength = (UINT32)AsciiStrLen (HttpsString);

  TempString = AllocateZeroPool (HttpsLength);

  if ((UrlLength < HttpsLength) || (Url == NULL) || (TempString == NULL)) {
    return FALSE;
  }

  // Copy beginning of URL that contains https string and lower case it
  for (Index = 0; Index < HttpsLength; Index++) {
    if (Url[Index] > 64 && Url[Index] < 91) {
      TempString[Index] = Url[Index] + 32;
    } else {
      TempString[Index] = Url[Index];
    }
  }

  IsHttpsString = (AsciiStrnCmp (HttpsString, TempString, HttpsLength) == 0);

  FreePool (TempString);

  return IsHttpsString;
}


/**
  Function to Delete x509 certificate

  @param[in] OcrTlsGuid         Guid associated with certificate

  @retval EFI_SUCCESS           Deleted x509 Certificate
  @retval EFI_OUT_OF_RESOURCES  Out of resources
**/
EFI_STATUS
DeleteOcrX509Certificate (
  IN EFI_GUID  *OcrTlsGuid
  )
{
  EFI_STATUS                  Status;
  UINTN                       DataSize;
  UINT32                      Attr;
  UINT32                      Index;
  UINT8                       *OldCertListData;
  UINT8                       *NewCertListData;
  EFI_SIGNATURE_LIST          *NewCertList;
  EFI_SIGNATURE_LIST          *TempCertList;
  EFI_SIGNATURE_DATA          *CertData;
  UINTN                       CertCount;
  UINT32                      TempCertOffset;
  UINT32                      Block1Size;
  UINT32                      Block2Size;
  UINT32                      OldBlock2Offset;
  BOOLEAN                     IsItemFound;
  UINT32                      ItemDataSize;

  NewCertList     = NULL;
  Attr            = 0;

  //
  // Get original signature list data.
  //
  DataSize = 0;
  Status = gRT->GetVariable (EFI_TLS_CA_CERTIFICATE_VARIABLE, &gEfiTlsCaCertificateGuid, NULL, &DataSize, NULL);
  if (EFI_ERROR (Status) && Status != EFI_BUFFER_TOO_SMALL) {
    return Status;
  }

  OldCertListData = (UINT8 *) AllocateZeroPool (DataSize);
  if (OldCertListData == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    return Status;
  }

  Status = gRT->GetVariable (EFI_TLS_CA_CERTIFICATE_VARIABLE, &gEfiTlsCaCertificateGuid, &Attr, &DataSize, OldCertListData);
  if (EFI_ERROR(Status)) {
    FreePool (OldCertListData);
    return Status;
  }

  //
  // Allocate space for new variable.
  //
  NewCertListData = (UINT8 *) AllocateZeroPool (DataSize);
  if (NewCertListData == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    FreePool (OldCertListData);
    return Status;
  }

  //
  // Enumerate all data and erase the target item.
  //
  IsItemFound = FALSE;
  ItemDataSize = (UINT32) DataSize;
  TempCertOffset = 0;
  TempCertList = (EFI_SIGNATURE_LIST*) OldCertListData;

  while ((ItemDataSize > 0) && (ItemDataSize >= TempCertList->SignatureListSize) && !IsItemFound) {
    // Move TempcertList to current
    TempCertList = (EFI_SIGNATURE_LIST*) (OldCertListData + TempCertOffset);

    if (CompareGuid(&TempCertList->SignatureType, &gEfiCertX509Guid)) {
      // Calculate the certificate count in List
      CertData = (EFI_SIGNATURE_DATA *) ((UINT8 *) TempCertList + sizeof (EFI_SIGNATURE_LIST) + TempCertList->SignatureHeaderSize);
      CertCount  = (TempCertList->SignatureListSize - sizeof (EFI_SIGNATURE_LIST) - TempCertList->SignatureHeaderSize) / TempCertList->SignatureSize;

      // Search for the OCR TLS Certificate
      for (Index = 0; Index < CertCount; Index++) {
        if (CompareGuid(&CertData->SignatureOwner, OcrTlsGuid)) {
          IsItemFound = TRUE;
          // Calculate Block 1 size, old certificate list block 2 offset, and Block 2 size
          if (CertCount == 1) {
            // if this is only certificate in list, then delete whole list including header
            Block1Size = TempCertOffset;
            OldBlock2Offset = Block1Size + TempCertList->SignatureListSize;
            Block2Size = (UINT32) DataSize - OldBlock2Offset;

            CopyMem (NewCertListData, OldCertListData, Block1Size);
            CopyMem (NewCertListData + Block1Size, OldCertListData + OldBlock2Offset, Block2Size);
          } else {
            // Only delete the certificate
            Block1Size = TempCertOffset + sizeof (EFI_SIGNATURE_LIST) + TempCertList->SignatureHeaderSize + (Index * TempCertList->SignatureSize);
            OldBlock2Offset = Block1Size + TempCertList->SignatureSize;
            Block2Size = (UINT32) DataSize - OldBlock2Offset;

            CopyMem(NewCertListData, OldCertListData, Block1Size);
            CopyMem(NewCertListData + Block1Size, OldCertListData + OldBlock2Offset, Block2Size);

            TempCertList = (EFI_SIGNATURE_LIST*) (NewCertListData + TempCertOffset);
            NewCertList = (EFI_SIGNATURE_LIST*) NewCertListData;
            NewCertList->SignatureListSize -= TempCertList->SignatureSize;
          }
          break;
        }
      }
    }
    TempCertOffset += TempCertList->SignatureListSize;
    ItemDataSize -= TempCertList->SignatureListSize;
  }

  if (IsItemFound) {
    DataSize  = Block1Size + Block2Size;

    Status = gRT->SetVariable(
                EFI_TLS_CA_CERTIFICATE_VARIABLE,
                &gEfiTlsCaCertificateGuid,
                Attr,
                DataSize,
                NewCertListData
                );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Failed to set variable, Status = %r\n", Status));
    }
  }

  if (OldCertListData != NULL) {
    FreePool (OldCertListData);
  }

  if (NewCertListData != NULL) {
    FreePool (NewCertListData);
  }

  return Status;

}

/**
  Function to enroll x509 certificate

  @param[in] X509Data            X509 Certificate Data
  @param[in] X509DataSize        X509 Certificate Data Size
  @param[in] OcrTlsGuid          Guid associated with certificate

  @retval EFI_SUCCESS            Successfully enrolled x509 Certificate
  @retval EFI_OUT_OF_RESOURCES   Out of resources
**/
EFI_STATUS
EnrollOcrX509Certificate (
  IN VOID      *X509Data,
  IN UINTN     X509DataSize,
  IN EFI_GUID  *OcrTlsGuid
  )
{
  EFI_STATUS                        Status;
  EFI_SIGNATURE_LIST                *CACert;
  EFI_SIGNATURE_DATA                *CACertData;
  UINTN                             DataSize;
  UINTN                             SigDataSize;
  UINT32                            Attr;

  if (X509Data == NULL || X509DataSize == 0) {
    return EFI_INVALID_PARAMETER;
  }

  SigDataSize = sizeof (EFI_SIGNATURE_LIST) + sizeof (EFI_SIGNATURE_DATA) - 1 + X509DataSize;

  CACert = (EFI_SIGNATURE_LIST*) AllocateZeroPool (SigDataSize);
  if (CACert == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CACert->SignatureListSize = (UINT32) SigDataSize;
  CACert->SignatureHeaderSize = 0;
  CACert->SignatureSize = (UINT32) (sizeof (EFI_SIGNATURE_DATA) - 1 + X509DataSize);
  CopyGuid (&CACert->SignatureType, &gEfiCertX509Guid);

  CACertData = (EFI_SIGNATURE_DATA*) ((UINT8* ) CACert + sizeof (EFI_SIGNATURE_LIST));
  CopyGuid (&CACertData->SignatureOwner, OcrTlsGuid);
  CopyMem ((UINT8* ) (CACertData->SignatureData), X509Data, X509DataSize);

  //
  // Check if signature database entry has been already existed.
  // If true, use EFI_VARIABLE_APPEND_WRITE attribute to append the
  // new signature data to original variable
  //
  Attr = (EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS);

  Status = gRT->GetVariable(
                  EFI_TLS_CA_CERTIFICATE_VARIABLE,
                  &gEfiTlsCaCertificateGuid,
                  NULL,
                  &DataSize,
                  NULL
                  );

  if (Status == EFI_BUFFER_TOO_SMALL) {
    Attr |= EFI_VARIABLE_APPEND_WRITE;
  } else if (Status != EFI_NOT_FOUND) {
    if (CACert != NULL) {
      FreePool (CACert);
    }
    return Status;
  }

  Status = gRT->SetVariable(
                  EFI_TLS_CA_CERTIFICATE_VARIABLE,
                  &gEfiTlsCaCertificateGuid,
                  Attr,
                  SigDataSize,
                  (VOID *) CACert
                  );


  if (CACert != NULL) {
    FreePool (CACert);
  }

  return Status;
}

/**
  Function to connect WiFi device driver, check if WiFi connection successful and return
  the device path to the WiFi device

  @param[out] WifiDevicePath     WiFi Device Path, NULL if one could not be located
**/
VOID
ConnectWifiController (
  OUT EFI_DEVICE_PATH_PROTOCOL      **WifiDevicePath
  )
{
  EFI_STATUS                        Status;
  UINTN                             Index;
  UINTN                             HandleCount;
  EFI_HANDLE                        *HandleBuffer;
  PCI_TYPE00                        PciConfig;
  EFI_PCI_IO_PROTOCOL               *PciIo;
  WIFI_PROFILE_SYNC_PROTOCOL        *WiFiProfileSyncProtocol;

  WiFiProfileSyncProtocol = NULL;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPciIoProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    return;
  }
  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiPciIoProtocolGuid,
                    (VOID *) &PciIo
                    );
    if (EFI_ERROR (Status)) {
      return;
    }
    PciIo->Pci.Read (
                PciIo,
                EfiPciIoWidthUint8,
                0,
                sizeof (PciConfig),
                &PciConfig
                );
    if (PciConfig.Hdr.ClassCode[1] == PCI_CLASS_NETWORK_OTHER && PciConfig.Hdr.ClassCode[2] == PCI_CLASS_NETWORK) {
      gBS->ConnectController (HandleBuffer[Index], NULL, NULL, TRUE);
    }
  }

  Status = gBS->LocateProtocol (
                  &gWiFiProfileSyncProtocolGuid,
                  NULL,
                  (VOID **) &WiFiProfileSyncProtocol
                  );
  if (!EFI_ERROR (Status) && (WiFiProfileSyncProtocol != NULL)) {
    if (WiFiProfileSyncProtocol->WifiProfileSyncGetConnectState () == (UINT32) EFI_SUCCESS) {
      Status = gBS->LocateHandleBuffer (
                      ByProtocol,
                      &gEfiWiFi2ProtocolGuid,
                      NULL,
                      &HandleCount,
                      &HandleBuffer
                      );
      if (EFI_ERROR (Status)) {
        return;
      }
      for (Index = 0; Index <= HandleCount; Index++) {
        *WifiDevicePath = DevicePathFromHandle (HandleBuffer[HandleCount]);
        if (*WifiDevicePath != NULL) {
          return;
        }
      }
    }
  }
  return;
}

/**
  This function will get the device path for a wifi device if there is one connected
  using the WiFi profile sync protocol.

  @param[out] WifiDevicePath    Valid IPv4 WiFi device path

  @retval EFI_SUCCESS           Successful search for Http Device Path
  @retval EFI_NOT_FOUND         Could not find any simple network devices
  @retval EFI_UNSUPPORTED       Device path not supported
 **/
EFI_STATUS
GetWifiDevicePath (
  OUT EFI_DEVICE_PATH_PROTOCOL      **WifiDevicePath
  )
{
  ConnectWifiController (WifiDevicePath);

  if (*WifiDevicePath != NULL) {
    while (!IsDevicePathEnd (*WifiDevicePath)) {
      if (DevicePathType (*WifiDevicePath) == MESSAGING_DEVICE_PATH) {
        if (DevicePathSubType (*WifiDevicePath) == MSG_IPv4_DP) {
          DEBUG ((DEBUG_INFO, "  Found IP4 Path\n"));
          return EFI_SUCCESS;
        } else if (DevicePathSubType (*WifiDevicePath) == MSG_IPv6_DP) {
          DEBUG ((DEBUG_INFO, "  Found IP6 Path\n"));
          // IPv6 not supported so break and return NULL when this type found
          return EFI_UNSUPPORTED;
        }
      }
      *WifiDevicePath = NextDevicePathNode (*WifiDevicePath);
    }
  }
  return EFI_NOT_FOUND;
}

/**
  This function will get the device path for an ethernet device if there is one connected.

  @param[out] LanDevicePath     Ethernet connected device path

  @retval EFI_SUCCESS           Successful search for Http Device Path
  @retval EFI_NOT_FOUND         Could not find any simple network devices
  @retval EFI_UNSUPPORTED       Device path not supported
 **/
EFI_STATUS
GetEthernetDevicePath (
  OUT EFI_DEVICE_PATH_PROTOCOL      **LanDevicePath
  )
{
  EFI_STATUS                        Status;
  UINTN                             Index;
  UINTN                             HandleCount;
  EFI_HANDLE                        *HandleBuffer;
//[-start-211215-IB09480180-remove]//
//  EFI_SIMPLE_NETWORK_PROTOCOL       *HandleSimpleNetwork;
//[-end-211215-IB09480180-remove]//
  EFI_DEVICE_PATH_PROTOCOL          *HandleDevicePath;
  EFI_DEVICE_PATH_PROTOCOL          *TempDevicePath;

//[-start-211215-IB09480180-modify]//
  //
  // Follow Insyde network behavior, use gEfiLoadFileProtocolGuid to find https handle
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiLoadFileProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Can not Locate EfiLoadFileProtocolGuid\n"));
    return Status;
  }

  // check for Ethernet network device
  for (Index = 0; Index < HandleCount; Index++) {
//    Status =  gBS->HandleProtocol(
//                     HandleBuffer[Index],
//                     &gEfiSimpleNetworkProtocolGuid,
//                     (VOID **) &HandleSimpleNetwork
//                     );

    // check for Ethernet network device
 //   if (HandleSimpleNetwork->Mode->IfType == NET_IFTYPE_ETHERNET) {

    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiDevicePathProtocolGuid,
                    (VOID **) &HandleDevicePath
                    );
    if (EFI_ERROR (Status) || (HandleDevicePath == NULL)) {
      DEBUG ((DEBUG_ERROR, "  No Device Path\n"));
      continue;
    }

    // Check if the device is ip4 or ip6
    TempDevicePath = HandleDevicePath;
    while (!IsDevicePathEnd (TempDevicePath)) {
      if (DevicePathType (TempDevicePath) == MESSAGING_DEVICE_PATH) {
        if (DevicePathSubType (TempDevicePath) == MSG_IPv4_DP) {
          DEBUG ((DEBUG_INFO, "  Found IP4 Path\n"));
          *LanDevicePath = HandleDevicePath;
          // Return success only for IPv4 as currently IPv6 is not supproted
          return EFI_SUCCESS;
        } else if (DevicePathSubType (TempDevicePath) == MSG_IPv6_DP) {
          DEBUG ((DEBUG_INFO, "  Found IP6 Path\n"));
          *LanDevicePath = HandleDevicePath;
          return EFI_UNSUPPORTED;
        }
      }
      TempDevicePath = NextDevicePathNode (TempDevicePath);
//      }
    }
  }
//[-end-211215-IB09480180-modify]//
  return EFI_NOT_FOUND;
}

/**
  Function to find Http Device Path, it will favor ip4 over ip6

  @param[out] HttpDevicePath     Http Device Path, NULL if one could not be located

  @retval EFI_SUCCESS            Successful search for Http Device Path
  @retval EFI_NOT_FOUND          Could not find any simple network devices
  @retval EFI_UNSUPPORTED        Device path not supported
  @retval EFI_OUT_OF_RESOURCES   Out of resources
**/
EFI_STATUS
SearchHttpDevicePath (
  OUT EFI_DEVICE_PATH_PROTOCOL      **HttpDevicePath
  )
{
  EFI_STATUS                        Status;

  Status = GetEthernetDevicePath (HttpDevicePath);
  if (EFI_ERROR (Status)) {
    #if FixedPcdGetBool (PcdWifiProfileSyncEnable) == 1
      return GetWifiDevicePath (HttpDevicePath);
    #endif
  }
  return Status;
}

/**
  Gets the timeout value for requesting and receiving the recovery OS image from
  remote source when in HTTP recovery boot flow from AMT flow and sets the value
  of PcdHttpIoTimeout to update in NetworkPkg HttpBoot.

  @param[in] OcrParamTlv      Boot Option for HTTP boot image request and
                              response timeout value

  @retval EFI_SUCCESS         Successfully recieved timeout value and set PCD
  @retval other               Failed to get and set timeout value
**/
EFI_STATUS
SetHttpTimeout (
  IN OCR_PARAMETER_TLV    *OcrParamTlv
  )
{
  UINT32      TimeoutValue;

  TimeoutValue = HTTP_BOOT_TIMEOUT_DEFAULT;

  //
  // Get AMT HTTP timeout value
  //
  if (OcrParamTlv->Header.ParamType.ParamTypeId == HttpsRequestTimeOutParamType) {
    ASSERT (OcrParamTlv->Header.Length == sizeof(UINT16));
    TimeoutValue = *((UINT16 *) OcrParamTlv->Value);
    DEBUG ((DEBUG_INFO, "Get AMT HTTP timeout value %d\n", TimeoutValue));
  }
  if ((TimeoutValue < HTTP_BOOT_TIMEOUT_MIN) || (TimeoutValue > HTTP_BOOT_TIMEOUT_MAX)) {
    TimeoutValue = HTTP_BOOT_TIMEOUT_DEFAULT;
  }

  DEBUG ((DEBUG_INFO, "Set PcdHttpIoTimeout %d\n", TimeoutValue));
  return PcdSet32S (PcdHttpIoTimeout, TimeoutValue);
}