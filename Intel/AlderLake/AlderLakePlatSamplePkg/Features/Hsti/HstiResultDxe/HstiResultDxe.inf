### @file
#  This is a DXE driver which updates the policy pointer to HSTI
#  cached results
#
#******************************************************************************
#* Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2015 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010006
  BASE_NAME                      = HstiResultDxe
  FILE_GUID                      = C642C14C-0E9C-4AEF-94A5-A213BAA35DE0
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = DriverEntryPoint

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64 IPF EBC
#

[Sources]
  HstiResultDxe.c
  HstiResultDxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
#[-start-180724-IB11790262-modify]#
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-180724-IB11790262-modify]#

[LibraryClasses]
  BaseLib
  UefiRuntimeServicesTableLib
  UefiBootServicesTableLib
  UefiLib
  UefiDriverEntryPoint
  BaseMemoryLib
  MemoryAllocationLib
  HobLib

[Guids]
  gPlatformConfigChangeGuid                     # CONSUMES
  gEfiEndOfDxeEventGroupGuid                    # CONSUMES
  gSiMemoryPlatformDataGuid                     # CONSUMES

[Protocols]
  gHstiPublishCompleteProtocolGuid              # CONSUMES
  gDxeSiPolicyProtocolGuid                      # CONSUMES
  gEdkiiVariableLockProtocolGuid                # SOMETIMES_CONSUMES
  gPlatformConfigChangeProtocolGuid             # SOMETIMES_CONSUMES
#[-start-180724-IB11790262-add]#
  gEfiSetupUtilityApplicationProtocolGuid       # SOMETIMES_CONSUMES
#[-end-180724-IB11790262-add]#

[Depex]
  gDxeSiPolicyProtocolGuid                      AND
  gEfiVariableArchProtocolGuid                  AND
  gEfiVariableWriteArchProtocolGuid
