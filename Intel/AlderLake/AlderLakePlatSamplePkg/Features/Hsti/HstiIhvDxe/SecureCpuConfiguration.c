/** @file
  This file contains the tests for the SecureCPUConfiguration bit

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include "HstiIhvDxe.h"
#include <Library/VmdInfoLib.h>

#define   B_SMRR_MASK_MASK                  (0xFFFFF000)
#define   B_SMRR_WORK_MASK                  (0xFFFFFFFF)
#define   B_PRMRR_WORK_MASK                 (0xFFFFFFFF)
#define   B_PRMRR_MASK_MASK_PROG            (0x7FFFFFFFF000)

/**
  Run tests for SecureCPUConfiguration bit
**/
VOID
CheckSecureCpuConfiguration (
  VOID
  )
{
  EFI_STATUS                        Status;
  BOOLEAN                           Result;
  UINTN                             CpuNumber;
  UINTN                             CpuIndex;
  CPUID_VERSION_INFO_ECX            Ecx;
  UINT32                            TsegMB;
  UINT32                            Bgsm;
  MSR_SMRR_BASE_REGISTER            SmrrPhysBase;
  MSR_SMRR_MASK_REGISTER            SmrrPhysMask;
  UINT64                            MasterSmrrBase;
  UINT64                            MasterSmrrMask;
  MSR_PRMRR_BASE_0_REGISTER         PrmrrPhysBase;
  MSR_PRMRR_MASK_REGISTER           PrmrrPhysMask;
  BOOLEAN                           PrmrrEnabled;
  BOOLEAN                           PrmrrsConsistent;
  UINT64                            MasterPrmrrBase;
  UINT64                            MasterPrmrrMask;
  MSR_LTCTRLSTS_REGISTER            MsrLtctrlsts;
  MEMORY_PLATFORM_DATA_HOB          *MemInfo;
  MSR_SMM_FEATURE_CONTROL_REGISTER  SmmFeatureControl;
  MSR_IA32_FEATURE_CONTROL_REGISTER Ia32FeatureControlMsr;
  MSR_MTRRCAP_REGISTER              MtrrCapMsr;
  MSR_IA32_BIOS_SIGN_ID_REGISTER    Ia32BiosSignIdMsr;
  MSR_BIOS_DONE_REGISTER            BiosDoneMsr;
  MSR_FEATURE_CONFIG_REGISTER       FeatureConfigMsr;
  ADL_MSR_PLATFORM_INFO_REGISTER    PlatformInfoMsr;
  UINT32                            CodeReportedBitmap = 0;

  if ((mFeatureImplemented[0] & HSTI_BYTE0_SECURE_CPU_CONFIGURATION) == 0) {
    return;
  }

  Result = TRUE;
  TsegMB = PciSegmentRead32 (
             PCI_SEGMENT_LIB_ADDRESS (
               SA_SEG_NUM,
               SA_MC_BUS,
               SA_MC_DEV,
               SA_MC_FUN,
               R_SA_TSEGMB)
             );

  DEBUG ((DEBUG_INFO, "  CPU Security Configuration per CPU Number Tests\n"));

  CpuNumber = GetCpuNumber ();
  for (CpuIndex = 0; CpuIndex < CpuNumber; CpuIndex++) {
    DEBUG ((DEBUG_INFO, "    [CPU - 0x%x]\n", CpuIndex));

    DEBUG ((DEBUG_INFO, "      Microcode Update Revision Test\n"));
    ProcessorWriteMsr64 (CpuIndex, MSR_IA32_BIOS_SIGN_ID, 0);
    ProcessorCpuid (CpuIndex, CPUID_VERSION_INFO, NULL, NULL, NULL, NULL);
    Ia32BiosSignIdMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_IA32_BIOS_SIGN_ID);
    if (Ia32BiosSignIdMsr.Bits.MicrocodeUpdateSignature == 0) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: Invalid Microcode Update Revision\n"));
      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_1,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_1,
        &CodeReportedBitmap,
        BIT1
        );
      Result = FALSE;
    }

    DEBUG ((DEBUG_INFO, "      Sample Part Test\n"));

    PlatformInfoMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, ADL_MSR_PLATFORM_INFO);
    if (PlatformInfoMsr.Bits.SamplePart != 0) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: This is a sample part\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_2,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_2,
        &CodeReportedBitmap,
        BIT2
        );
      Result = FALSE;
    }

    DEBUG ((DEBUG_INFO, "      IA32_FEATURE_CONTROL MSR Lock Test\n"));

    Ia32FeatureControlMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_IA32_FEATURE_CONTROL);
    if (Ia32FeatureControlMsr.Bits.Lock == 0) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: This is a sample part\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_3,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_3,
        &CodeReportedBitmap,
        BIT3
        );
      Result = FALSE;
    }

    SmmFeatureControl.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_SMM_FEATURE_CONTROL);

    DEBUG ((DEBUG_INFO, "      SMM_CODE_CHK_EN Test\n"));

    if (SmmFeatureControl.Bits.SmmCodeChkEn == 0) {

      DEBUG ((DEBUG_INFO, "        Unexpected Status: SMM Code Fetch outside SMRAM detect feature disabled\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_4,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_4,
        &CodeReportedBitmap,
        BIT4
        );
      Result = FALSE;
    }

    DEBUG ((DEBUG_INFO, "      SMM_FEATURE_CONTROL MSR Lock Test\n"));

    if (SmmFeatureControl.Bits.Lock == 0) {

      DEBUG ((DEBUG_INFO, "        Unexpected Status: SMM feature control MSR not locked\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_4,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_4,
        &CodeReportedBitmap,
        BIT4
        );
      Result = FALSE;
    }

    DEBUG ((DEBUG_INFO, "      FEATURE_CONFIG MSR  Lock Test\n"));

    ProcessorCpuid (CpuIndex, CPUID_VERSION_INFO, NULL, NULL, &Ecx.Uint32, NULL);
    if (Ecx.Bits.AESNI == 1) {
      FeatureConfigMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_FEATURE_CONFIG);
      if (FeatureConfigMsr.Bits.Lock == 0) {
        DEBUG ((DEBUG_INFO, "        Unexpected Status: Feature config MSR not locked\n"));

        BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_5,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_5,
        &CodeReportedBitmap,
        BIT5
        );
        Result = FALSE;
      }
    }


    DEBUG ((DEBUG_INFO, "      BIOS DONE MSR set Test\n"));

    BiosDoneMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_BIOS_DONE);

    if (BiosDoneMsr.Bits.EnableIaUntrusted == 0) {

      DEBUG ((DEBUG_INFO, "        Unexpected Status: BIOS Done MSR is not set\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_C,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_C,
        &CodeReportedBitmap,
        BIT12
        );
      Result = FALSE;
    }

    DEBUG((DEBUG_INFO, "      ConfigLock Test\n"));

    MsrLtctrlsts.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_LTCTRLSTS);
    if (MsrLtctrlsts.Bits.Configlock == 0) {
      DEBUG((DEBUG_INFO, "        Unexpected Status: ConfigLock not set\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_D,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_D,
        &CodeReportedBitmap,
        BIT13
        );
      Result = FALSE;
    }

  }

  DEBUG ((DEBUG_INFO, "      FIT Patch Support Test\n"));

  if (!IsValiduCodeEntry ()) {
    BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_6,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_6,
        &CodeReportedBitmap,
        BIT6
        );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "    SMM Security Configuration Tests\n"));

  DEBUG ((DEBUG_INFO, "      TSEGMB Alignment Test\n"));


  MemInfo = GetFirstGuidHob (&gSiMemoryPlatformDataGuid);
  ASSERT (MemInfo != NULL);
  if (MemInfo == NULL) {
    return;
  }

  if (((TsegMB & B_SA_TSEGMB_TSEGMB_MASK) & (LShiftU64 (MemInfo->Data.TsegSize,20) - 1)) != 0) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: TSEGMB not size aligned, TSEG size: 0x%x\n",(LShiftU64 (MemInfo->Data.TsegSize,20) - 1)));

    BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_9,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_9,
        &CodeReportedBitmap,
        BIT9
        );
    Result = FALSE;
  }

  MasterSmrrBase = 0;
  MasterSmrrMask = 0;
  for (CpuIndex = 0; CpuIndex < CpuNumber; CpuIndex++) {
      DEBUG((DEBUG_INFO, "      SMRR1 Configuration per CPU Number Test\n"));
      DEBUG ((DEBUG_INFO, "    [CPU - 0x%x]\n", CpuIndex));

    DEBUG ((DEBUG_INFO, "      SMRR1 are supported Test\n"));

    MtrrCapMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_MTRRCAP);
    if (MtrrCapMsr.Bits.Smrr == 0) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: SMRR1 not supported\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_A,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_A,
        &CodeReportedBitmap,
        BIT10
        );
      Result = FALSE;
    } else {
      DEBUG ((DEBUG_INFO, "      SMRR1 programmed consistently on all cores Test\n"));

      SmrrPhysBase.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_SMRR_BASE);
      SmrrPhysMask.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_SMRR_MASK);

      if (CpuIndex == 0) {
        MasterSmrrBase = SmrrPhysBase.Uint64;
        MasterSmrrMask = SmrrPhysMask.Uint64;
      } else {
        if ((SmrrPhysBase.Uint64 != MasterSmrrBase) || (SmrrPhysMask.Uint64 != MasterSmrrMask)) {
          DEBUG ((DEBUG_INFO, "        Unexpected Status: SMRR1 not programmed consistently across all cores\n"));

          BuildAndAppendHstiUniqueStatusString (
            HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_A,
            HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_A,
            &CodeReportedBitmap,
            BIT10
            );
          Result = FALSE;
        }
      }

      DEBUG ((DEBUG_INFO, "      SMRR1 enabled/correct Test\n"));

      if ((SmrrPhysMask.Bits.Vld == 0x0) ||
          (SmrrPhysMask.Bits.Assist != 0x0) ||
          (SmrrPhysBase.Bits.Memtype != 0x6)) {
        DEBUG ((DEBUG_INFO, "        Unexpected Status: SMRR1 not enabled/correct\n"));

        BuildAndAppendHstiUniqueStatusString (
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_A,
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_A,
          &CodeReportedBitmap,
          BIT10
          );
        Result = FALSE;
      }

      DEBUG ((DEBUG_INFO, "      SMRR1 and TSEGMB match Test\n"));

      DEBUG ((DEBUG_INFO, "        INFO: SMRR1:   0x%08x - 0x%08x\n", SmrrPhysBase.Uint64 & (SmrrPhysMask.Uint64 & B_SMRR_MASK_MASK), (UINT32) (~(SmrrPhysMask.Uint64 & B_SMRR_MASK_MASK) + 1)));
      DEBUG ((DEBUG_INFO, "        INFO: TSEGMB: 0x%08x\n", TsegMB & B_SA_TSEGMB_TSEGMB_MASK));

      if ((SmrrPhysBase.Uint64 & (SmrrPhysMask.Uint64 & B_SMRR_MASK_MASK)) != (TsegMB & B_SA_TSEGMB_TSEGMB_MASK)) {

        DEBUG ((DEBUG_INFO, "        Unexpected Status: SMRR1 != TSEGMB\n"));

        BuildAndAppendHstiUniqueStatusString (
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_A,
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_A,
          &CodeReportedBitmap,
          BIT10
          );
        Result = FALSE;
      }

      DEBUG ((DEBUG_INFO, "      SMRR1 size Test\n"));
      Bgsm = PciSegmentRead32 (
               PCI_SEGMENT_LIB_ADDRESS (
                 SA_SEG_NUM,
                 SA_MC_BUS,
                 SA_MC_DEV,
                 SA_MC_FUN,
                 R_SA_BGSM)
               );

      if ((UINT32) (~(SmrrPhysMask.Uint64 & B_SMRR_MASK_MASK) + 1) !=
          ((Bgsm & B_SA_BGSM_BGSM_MASK) - (TsegMB & B_SA_TSEGMB_TSEGMB_MASK))) {
        DEBUG ((DEBUG_INFO, "        Unexpected Status: SMRR1 size != BGSM-TSEGMB\n"));

        BuildAndAppendHstiUniqueStatusString (
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_A,
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_A,
          &CodeReportedBitmap,
          BIT10
          );
        Result = FALSE;
  }

      DEBUG ((DEBUG_INFO, "      SMRR1 Work Test\n"));

      if (MmioRead32 (SmrrPhysBase.Uint64 & SmrrPhysMask.Uint64) != B_SMRR_WORK_MASK) {
        DEBUG ((DEBUG_INFO, "        Unexpected Status: SMRR1 not working, read succeeded\n"));


        BuildAndAppendHstiUniqueStatusString (
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_A,
          HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_A,
          &CodeReportedBitmap,
          BIT10
          );
        Result = FALSE;
      }
    }
  }

  DEBUG ((DEBUG_INFO, "    Protected Range Microcode Range Register (PRMRR) Security Configuration per CPU Number Tests\n"));

  MasterPrmrrBase = 0;
  MasterPrmrrMask = 0;

  for (CpuIndex = 0; CpuIndex < CpuNumber; CpuIndex++) {
    DEBUG ((DEBUG_INFO, "    [CPU - 0x%x]\n", CpuIndex));

    DEBUG ((DEBUG_INFO, "      PRMRR supported Test\n"));

    MtrrCapMsr.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_MTRRCAP);
    if (MtrrCapMsr.Bits.Prmrr == 0) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: PRMRR not supported by CPU skipping PRMRR tests\n"));
    } else{
      PrmrrPhysBase.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_PRMRR_BASE_0);
      PrmrrPhysMask.Uint64 = ProcessorReadMsr64 (CpuIndex, MSR_PRMRR_MASK);
      PrmrrEnabled = FALSE;

      if (PrmrrPhysMask.Bits.Vld != 0) {
        PrmrrEnabled = TRUE;
      }
      DEBUG ((DEBUG_INFO, "        PRMRR Vld bit = 0x%x \n", PrmrrPhysMask.Bits.Vld));
      DEBUG ((DEBUG_INFO, "        PRMRR Mask Offset Value = 0x%016lx \n", (PrmrrPhysMask.Uint64 & B_PRMRR_MASK_MASK_PROG)));

      if (PrmrrEnabled && ((PrmrrPhysMask.Uint64 & B_PRMRR_MASK_MASK_PROG) != 0)) {
        DEBUG ((DEBUG_INFO, "      PRMRR programmed consistently on all cores Test\n"));

        PrmrrsConsistent = TRUE;
        if (CpuIndex == 0) {
          MasterPrmrrBase = PrmrrPhysBase.Uint64;
          MasterPrmrrMask = PrmrrPhysMask.Uint64;
        } else {
          if ((PrmrrPhysBase.Uint64 != MasterPrmrrBase) || (PrmrrPhysMask.Uint64 != MasterPrmrrMask)) {
            PrmrrsConsistent = FALSE;
            DEBUG ((DEBUG_INFO, "        Unexpected Status: PRMRR's not programmed consistently\n"));

            BuildAndAppendHstiUniqueStatusString (
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_7,
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_7,
              &CodeReportedBitmap,
              BIT7
              );
            Result = FALSE;
          }
        }

        DEBUG ((DEBUG_INFO, "      PRMRR correctly programmed & Memory Type initialized Tests\n"));
        if (PrmrrsConsistent) {
          if ((PrmrrPhysMask.Bits.Assist != 0x0) ||
              (PrmrrPhysMask.Bits.L == 0x0) ||
              (PrmrrPhysBase.Bits.Memtype != 0x6)) {
            DEBUG ((DEBUG_INFO, "        Unexpected Status: PRMRR's not programmed correctly or  Memory Type not initialized\n"));

            BuildAndAppendHstiUniqueStatusString (
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_7,
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_7,
              &CodeReportedBitmap,
              BIT7
              );
            Result = FALSE;
          }

          DEBUG ((DEBUG_INFO, "      PRMRR working Test\n"));

          if (MmioRead32 (PrmrrPhysBase.Uint64 & PrmrrPhysMask.Uint64) != B_PRMRR_WORK_MASK) {
            DEBUG ((DEBUG_INFO, "        Unexpected Status: PRMRR's not working, able to read from range\n"));

            BuildAndAppendHstiUniqueStatusString (
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_7,
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_7,
              &CodeReportedBitmap,
              BIT7
              );
            Result = FALSE;
          }
        }
      } else {
        DEBUG ((DEBUG_INFO, "        Unexpected Status: Processor Reserved Memory Range Register is Supported but Disabled\n"));

        BuildAndAppendHstiUniqueStatusString (
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_E,
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_E,
              &CodeReportedBitmap,
              BIT14
              );
        Result = FALSE;
      }
    }
  }

  // Check if VMD Device Present and Enabled then verify VDM Lock bit is set to 1
  DEBUG ((DEBUG_INFO, "    VMD Present and Enabled Test\n"));

  if (IsVmdEnabled ()) {
    DEBUG ((DEBUG_INFO, "    VMD Lock Bit Test\n"));

    if (IsVmdLocked() == FALSE) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: Volume Management Device (VMD) bit is unlocked\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_F,
        HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_F,
        &CodeReportedBitmap,
        BIT15
        );
      Result = FALSE;
    }
  } else {
    DEBUG ((DEBUG_INFO, "        INFO: Volume Management Device (VMD) is not Present\n"));
  }

  // Check if MK-TME is supported in the CPU SKU
  if (IsTmeSupported () == TRUE) {
    // Check if MK-TME Activate lock bit is set to 1 irrrespectively if this TME is enabled or not
    DEBUG ((DEBUG_INFO, "    MK-TME bit locked Test\n"));

    if ((IsTmeActivateLocked ()) == FALSE) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: MK-TME Active bit is unlocked\n"));

      BuildAndAppendHstiUniqueStatusString (
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_CODE_10,
              HSTI_BYTE0_SECURE_CPU_CONFIGURATION_UNEXP_STATUS_STRING_10,
              &CodeReportedBitmap,
              BIT16
              );
      Result = FALSE;
    }
  } else {
    DEBUG ((DEBUG_INFO, "        HSTI MSR INFO: MK-TME Technology is not supported in CPU SKU\n"));
  }

  //
  // ALL PASS
  //
  if (Result) {
    Status = HstiLibSetFeaturesVerified (
               PLATFORM_SECURITY_ROLE_PLATFORM_REFERENCE,
               NULL,
               0,
               HSTI_BYTE0_SECURE_CPU_CONFIGURATION
               );
    CheckStatusForHstiLibSet (Status);
  }

  return;
}
