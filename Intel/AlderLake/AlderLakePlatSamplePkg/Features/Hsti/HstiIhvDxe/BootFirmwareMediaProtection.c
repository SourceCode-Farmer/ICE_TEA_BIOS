/** @file
  This file contains the tests for the BootFirmwareMediaProtection BIT

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include "HstiIhvDxe.h"
#include <MeState.h>

#define V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE                        0x00A00000
#define V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE_PDR                    0x01A00000
#define V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE_EC                     0x10A00000
#define V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE_PDR_EC                 0x11A00000
#define V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE           0x00000F00
#define V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE_PDR       0x00001F00
#define V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE_EC        0x00010F00
#define V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE_PDR_EC    0x00011F00
#define V_SPI_GBE_REGION_WRITE_READ_ACCESS                                   0x00800900
#define V_SPI_ME_REGION_WRITE_READ_ACCESS                                    0x00400D00
#define V_SPI_EC_REGION_WRITE_READ_ACCESS1                                   0x10010300
#define V_SPI_EC_REGION_WRITE_READ_ACCESS2                                   0x10010100

/**
  Check Extended BIOS region Unexpected Status Tests.

  @param[in] Result - Secure Pch Configuration Tests Result

  @retval TRUE   Error Detected.
  @retval FALSE  Error Not Detected.
**/
BOOLEAN
SecureSpiExtendedBiosRegionConfiguration (
  VOID
  )
{
  BOOLEAN                       Result;

  Result = TRUE;

  DEBUG ((DEBUG_INFO, "        Extended BIOS Decode Region Limit Locked Test\n"));
  if (PchDmiIsExtendedBiosRegionLimitLocked () == FALSE) {
    DEBUG ((DEBUG_ERROR, "        Unexpected Status: Extended BIOS Decode Region limit is unlocked\n"));
    BuildAndAppendHstiStatusString(
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_11,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_11
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "        Extended BIOS Decode Region Base Locked Test\n"));
  if (SpiIsExtendedBiosRegionBaseLocked () == FALSE) {
    DEBUG ((DEBUG_ERROR, "        Unexpected Status: Extended BIOS Decode Region base is unlocked\n"));
    BuildAndAppendHstiStatusString(
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_12,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_12
      );
    Result = FALSE;
  }
  return Result;
}

/**
  Run tests for BootFirmwareMediaProtection bit
**/
VOID
CheckBootFirmwareMediaProtection (
  VOID
  )
{
  EFI_STATUS      Status;
  BOOLEAN         Result;
  UINT32          Cpptv;
  UINT32          Data32;
  BOOLEAN         PcrDmi;
  BOOLEAN         SpiBild;
  HECI_PROTOCOL   *Heci;
  UINT32          MeStatus;
  UINT32          CodeReportedBitmap = 0;

  if ((mFeatureImplemented[0] & HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION) == 0) {
    return;
  }

  Result = TRUE;

  DEBUG ((DEBUG_INFO, "  SPI Flash and BIOS Security Configuration\n"));
  DEBUG ((DEBUG_INFO, "      BIOS write-protection Test\n"));

  if (SpiIsWriteProtectionEnabled () == FALSE) {
    DEBUG((DEBUG_INFO, "        Unexpected Status: One or more bits misconfigured for BIOS Write-Protection\n"));
    DEBUG((DEBUG_INFO, "        Values expected: LE = 1; WPD = 0; EISS = 1\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_1,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_1
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      SPI flash descriptor security override pin-strap Test\n"));
  if (SpiIsFlashDescriptorOverrideEnabled ()) {
    BuildAndAppendHstiStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_2,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_2
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      SPI controller configuration is locked Test\n"));

  if (SpiIsFlashConfigurationLockDownEnabled () == FALSE) {
    DEBUG((DEBUG_INFO, "        Unexpected Status: SPI controller configuration Registers Unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_3,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_3
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      BIOS Interface Lock Test\n"));

  PcrDmi = PchDmiIsBiosInterfaceLock ();
  SpiBild = SpiIsBiosInterfaceLockdownEnabled ();
  if ((PcrDmi == FALSE) ||
      (SpiBild == FALSE)) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: DMI.BILD[7] = %x and/or SPI.BILD[7] = %x are not set\n", PcrDmi, SpiBild));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_4,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_4
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      BIOS Top Swap Mode Test\n"));

  if (SpiIsTopSwapEnabled () == TRUE) {
    DEBUG((DEBUG_INFO, "        Unexpected Status: BIOS Top Swap Mode Enabled\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_5,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_5
      );
    Result = FALSE;
  }

//[start-220126-STORM1121-modify]
  if (SpiIsSafModeActive() == FALSE) {
//[END-220126-STORM1121-modify]
    DEBUG ((DEBUG_INFO, "      Component Property Parameter Table Valid Test\n"));

    SpiGetComponentPropertyParameterTable (0, &Cpptv);
    if (SpiIsCppValidBitSet (Cpptv) == FALSE) {
      DEBUG((DEBUG_INFO, "        Unexpected Status: Component Property Parameter Table Invalid.\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_6,
        HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_6,
        &CodeReportedBitmap,
        BIT6
        );
      Result = FALSE;
    }

    DEBUG ((DEBUG_INFO, "      Component Property Parameter Table Valid, Secondary Flash Part Test\n"));

    //
    // If Second part is present the verify Cpp Valid bit is set, otherwise if not present verify that it's clear
    //
    if (SpiGetComponentPropertyParameterTable (1, &Cpptv) == TRUE) {
      if (SpiIsCppValidBitSet (Cpptv) == FALSE) {
        DEBUG((DEBUG_INFO, "        Unexpected Status: Second SPI Flash part present, Cpp Valid Bit is set to 0\n"));

        BuildAndAppendHstiUniqueStatusString (
          HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_6,
          HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_6,
          &CodeReportedBitmap,
          BIT6
          );
        Result = FALSE;
//[start-220126-STORM1121-modify]
      }
//[end-220126-STORM1121-modify]
    }
  }

  DEBUG ((DEBUG_INFO, "      ME Firmware Normal Working Status Test\n"));

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "        Unable to Locate HECI Protocol\n"));
  } else {
    Heci->GetMeStatus (&MeStatus);

    if (ME_STATUS_ME_STATE_ONLY (MeStatus) != ME_READY)  {
      DEBUG((DEBUG_INFO, "        Unexpected Status: ME Firmware not in Normal Working Status = %x\n", (ME_STATUS_ME_STATE_ONLY (MeStatus))));

      BuildAndAppendHstiStatusString (
        HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_8,
        HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_8
        );
      Result = FALSE;
    }
  }

  DEBUG ((DEBUG_INFO, "      BIOS Flash Descriptor Valid Test\n"));

  if (SpiIsFlashDescriptorValid () == FALSE) {
    DEBUG((DEBUG_INFO, "        Unexpected Status: BIOS Flash Descriptor Invalid\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_9,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_9
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      BIOS Region Flash Write Access Test\n"));
  ///
  /// Descriptor Main Access Read:  BIT 8
  /// Bios Main Access Read:        BIT 9
  /// CSE Main Access Read:         BIT 10
  /// GBE Main Access Read:         BIT 11
  /// PDR Main Access Read:         BIT 12
  /// EC  Main Access Read:         BIT 16
  /// Descriptor Main Access Write: BIT 20
  /// Bios Main Access Write:       BIT 21
  /// CSE Main Access Write:        BIT 22
  /// GBE Main Access Write:        BIT 23
  /// PDR Main Access Write:        BIT 24
  /// EC Main Access Write:         BIT 28
  /// Recommended Flash Region Read/Write access permissions as per SPI Programming Guide.
  ///
  Data32 = SpiGetBiosRegionFlashWriteAccess ();

  if ((Data32 != V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE) &&
      (Data32 != V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE_PDR) &&
      (Data32 != V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE_EC) &&
      (Data32 != V_SPI_BIOS_REGION_FLASH_WRITE_ACCESS_BIOS_GBE_PDR_EC)) {

    DEBUG ((DEBUG_INFO, "        Unexpected Status: Bios Region Main Write Access Invalid Production Permissions\n"));

    BuildAndAppendHstiUniqueStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
      &CodeReportedBitmap,
      BIT10
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      BIOS Region Flash Read Access Test\n"));
  Data32 = SpiGetBiosRegionFlashReadAccess ();

  if ((Data32 != V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE) &&
      (Data32 != V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE_PDR) &&
      (Data32 != V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE_EC) &&
      (Data32 != V_SPI_BIOS_REGION_FLASH_READ_ACCESS_DESCRIPTOR_BIOS_ME_GBE_PDR_EC)) {

    DEBUG ((DEBUG_INFO, "        Unexpected Status: Bios Region Main Read Access Invalid Production Permissions\n"));

    BuildAndAppendHstiUniqueStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
      &CodeReportedBitmap,
      BIT10
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      BIOS Main Read Access Test\n"));

  if (SpiGetMasterReadAccess () != 0) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: BMRAG Not Equal To 0x00\n"));

    BuildAndAppendHstiUniqueStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
      &CodeReportedBitmap,
      BIT10
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      BIOS Main Write Access Test\n"));

  if (SpiGetMasterWriteAccess () != 0) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: BMWAG Not Equal To 0x00\n"));

    BuildAndAppendHstiUniqueStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
      &CodeReportedBitmap,
      BIT10
      );
    Result = FALSE;
  }


  DEBUG ((DEBUG_INFO, "      GbE Region Access Test\n"));
  if (SpiIsGbeRegionValid () == TRUE) {
    Data32 = SpiGetGbeRegionAccess ();
    if (Data32 != V_SPI_GBE_REGION_WRITE_READ_ACCESS) {
      DEBUG ((DEBUG_INFO, "        Unexpected Status: Gbe Region Access Flash Descriptor\n"));

      BuildAndAppendHstiUniqueStatusString (
        HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
        HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
        &CodeReportedBitmap,
        BIT10
        );
      Result = FALSE;
    }
  }

  DEBUG ((DEBUG_INFO, "      ME Region Access Test\n"));

  Data32 = SpiGetCsmeRegionAccess ();
  if (Data32 != V_SPI_ME_REGION_WRITE_READ_ACCESS) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: ME Region Access Flash Descriptor\n"));

    BuildAndAppendHstiUniqueStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
      &CodeReportedBitmap,
      BIT10
      );
    Result = FALSE;
  }
  DEBUG ((DEBUG_INFO, "      EC Region Access Test\n"));

  Data32 = SpiGetEcRegionAccess ();

  if ((Data32 != V_SPI_EC_REGION_WRITE_READ_ACCESS1) &&
      (Data32 != V_SPI_EC_REGION_WRITE_READ_ACCESS2)) {
      DEBUG((DEBUG_INFO, "        Unexpected Status: EC Region Access Flash Descriptor\n"));

    BuildAndAppendHstiUniqueStatusString (
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_A,
      &CodeReportedBitmap,
      BIT10
      );
    Result = FALSE;
  }

  DEBUG((DEBUG_INFO, "      Disallow WriteStatus SPI OPCODE Test\n"));

  if (SpiIsWriteStatusDisable () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: WriteStatus SPI OPCODE is Allowed\n"));

    BuildAndAppendHstiStatusString(
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_CODE_F,
      HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION_UNEXP_STATUS_STRING_F
      );
    Result = FALSE;
  }

  DEBUG((DEBUG_INFO, "      Extended BIOS Decode Region Tests\n"));

  if (SpiIsExtendedBiosRegionEnabled () == TRUE) {
    DEBUG ((DEBUG_INFO, "        Extended BIOS Decode Region Enabled Test\n"));
    if (SecureSpiExtendedBiosRegionConfiguration () == FALSE) {
      Result = FALSE;
    }
  } else {
    DEBUG ((DEBUG_INFO, "        INFO: Extended BIOS Decode Region is not in use (disabled)\n"));
  }

  //
  // ALL PASS
  //
  if (Result) {
    Status = HstiLibSetFeaturesVerified (
               PLATFORM_SECURITY_ROLE_PLATFORM_REFERENCE,
               NULL,
               0,
               HSTI_BYTE0_BOOT_FIRMWARE_MEDIA_PROTECTION
               );
    CheckStatusForHstiLibSet (Status);
  }

  return;
}
