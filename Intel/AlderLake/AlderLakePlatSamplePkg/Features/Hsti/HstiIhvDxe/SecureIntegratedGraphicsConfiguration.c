/** @file
  This file contains the tests for the SecureIntegratedGraphics bit

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#include "HstiIhvDxe.h"

/**
  Get GttMmAddr Base Address

  @retval UINT64 - return the GttMmAddr Base Address
**/
UINT64
GetGttMmABaseddr (
  VOID
  )
{
  UINT64          GttMmAdr;
  UINT64          McD2BaseAddress;

 McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                     SA_SEG_NUM,
                     GetIgdBusNumber(),
                     GetIgdDevNumber(),
                     GetIgdFuncNumber(),
                     0
                     );

  PciSegmentReadBuffer (McD2BaseAddress + GetGttMmAdrOffset(), sizeof (UINT64), &GttMmAdr);
  GttMmAdr &= B_SA_GTTMMADR_BASE_ADDR_MASK_64B;

  return GttMmAdr;
}

/**
  Get Igd Ggms Size in Mb

  @retval UINT32 - return the Igd Ggms Size in Mb
**/
UINT32
GetIgdGgmsSizeInMb (
  VOID
  )
{
  UINT16          Ggc;
  UINT32          GgsmSize;

  Ggc = PciSegmentRead16 (
          PCI_SEGMENT_LIB_ADDRESS (
            SA_SEG_NUM,
            SA_MC_BUS,
            SA_MC_DEV,
            SA_MC_FUN,
            R_SA_GGC
          )
        );

  switch ((Ggc & B_SA_GGC_GGMS_MASK) >> N_SA_GGC_GGMS_OFFSET) {
    case V_SA_GGC_GGMS_2MB:
      GgsmSize = SIZE_2MB;
      break;
    case V_SA_GGC_GGMS_4MB:
      GgsmSize = SIZE_4MB;
      break;
    case V_SA_GGC_GGMS_8MB:
      GgsmSize = SIZE_8MB;
      break;
    default:
      GgsmSize = 0;
      break;
  }

  return GgsmSize;
}

/**
  Check for IGD stolen memory Consistency

  @retval BOOLEAN - TRUE  IGD stolen memory ranges are allocated correctly:
                          Contiguos, DSM below TOLUD, GSM below DSM
                    FALSE IGD stolen memory ranges are not allocated correctly
**/
BOOLEAN
IsIgdStolenMemoryConsistent (
  VOID
  )
{
  BOOLEAN         Result;
  UINT32          Bdsm;
  UINT32          Bgsm;
  UINT32          Tolud;
  UINT32          GsmSize;
  UINT32          DsmSize;

  Result  = TRUE;
  Bdsm    = PciSegmentRead32 (
              PCI_SEGMENT_LIB_ADDRESS (
                SA_SEG_NUM,
                SA_MC_BUS,
                SA_MC_DEV,
                SA_MC_FUN,
                R_SA_BDSM
              )
            );
  Bgsm    = PciSegmentRead32 (
              PCI_SEGMENT_LIB_ADDRESS (
                SA_SEG_NUM,
                SA_MC_BUS,
                SA_MC_DEV,
                SA_MC_FUN,
                R_SA_BGSM
              )
            );
  Tolud   = PciSegmentRead32 (
              PCI_SEGMENT_LIB_ADDRESS (
                SA_SEG_NUM,
                SA_MC_BUS,
                SA_MC_DEV,
                SA_MC_FUN,
                R_SA_TOLUD
              )
            );
  DsmSize = GetIgdGmsSizeInMb ();
  GsmSize = GetIgdGgmsSizeInMb ();

  DEBUG ((DEBUG_INFO, "        INFO: DSM:   0x%08x  Size: 0x%08x\n", Bdsm & B_SA_BDSM_BDSM_MASK, DsmSize));
  DEBUG ((DEBUG_INFO, "        INFO: GSM:   0x%08x  Size: 0x%08x\n", Bgsm & B_SA_BGSM_BGSM_MASK, GsmSize));
  DEBUG ((DEBUG_INFO, "        INFO: TOLUD: 0x%08x\n", Tolud & B_SA_TOLUD_TOLUD_MASK));

  //
  // If IGD stolen memory ranges: DSM and GSM are allocated correctly:
  // Contiguos, DSM below TOLUD and GSM below DSM
  //

  if ((Bgsm & B_SA_BGSM_BGSM_MASK) != (Bdsm & B_SA_BDSM_BDSM_MASK) - GsmSize) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found BGSM: 0x%x != BDSM: %x - GGMSize: %x\n",(Bgsm & B_SA_BGSM_BGSM_MASK),(Bdsm & B_SA_BDSM_BDSM_MASK),GsmSize));

    Result = FALSE;
  }
  if ((Bdsm & B_SA_BDSM_BDSM_MASK) != (Tolud & B_SA_TOLUD_TOLUD_MASK) - DsmSize) {
    DEBUG ((DEBUG_ERROR, "        Mistmatch found  BDSM: 0x%x != TOLUD: %x - DSMSize: %x\n",(Bdsm & B_SA_BDSM_BDSM_MASK),(Tolud & B_SA_TOLUD_TOLUD_MASK),DsmSize));

    Result = FALSE;
  }

  return Result;
}

/**
  Check Reserved Bit is Enabled

  @retval BOOLEAN - TRUE  Reserved bit is enabled
                    FALSE Reserved bit is not enabled
**/
BOOLEAN
IsReservedBitEnabled (
  VOID
  )
{
  BOOLEAN         Result;
  UINT32          Pavpc;
  UINT32          PcmBasePavpc;
  UINT32          Bdsm;
  UINT32          Tolud;
  UINT64          McD0BaseAddress;

  Result          = TRUE;
  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                      SA_SEG_NUM,
                      SA_MC_BUS,
                      SA_MC_DEV,
                      SA_MC_FUN,
                      0
                    );
  Bdsm            = PciSegmentRead32 (
                      PCI_SEGMENT_LIB_ADDRESS (
                        SA_SEG_NUM,
                        SA_MC_BUS,
                        SA_MC_DEV,
                        SA_MC_FUN,
                        R_SA_BDSM)
                      );
  Tolud           = PciSegmentRead32 (
                      PCI_SEGMENT_LIB_ADDRESS (
                        SA_SEG_NUM,
                        SA_MC_BUS,
                        SA_MC_DEV,
                        SA_MC_FUN,
                        R_SA_TOLUD
                      )
                    );
  Pavpc           = (PciSegmentRead32 (McD0BaseAddress + R_SA_PAVPC));
  PcmBasePavpc    = Pavpc & B_SA_PAVPC_PCMBASE_MASK;

  if ((Pavpc & B_SA_PAVPC_PCME_MASK) != 0) {
    if ((PcmBasePavpc < (Bdsm & B_SA_BDSM_BDSM_MASK)) ||
        (PcmBasePavpc > (Tolud & B_SA_TOLUD_TOLUD_MASK)) ||
        ((PcmBasePavpc + SIZE_1MB) < (Bdsm & B_SA_BDSM_BDSM_MASK)) ||
        ((PcmBasePavpc + SIZE_1MB) > (Tolud & B_SA_TOLUD_TOLUD_MASK))) {
      DEBUG ((DEBUG_ERROR, "        Graphics Security Reserved Check is Enabled but is not fully inside of DSM\n"));
      DEBUG ((DEBUG_INFO, "        PCMBASE PAVPC: 0x%08x or PCMBASE: 0x%08x + 1M Size out of BDSM 0x%x or TOLUD 0x%08x boundaries\n", PcmBasePavpc, PcmBasePavpc + SIZE_1MB, Bdsm & B_SA_BDSM_BDSM_MASK, Tolud & B_SA_TOLUD_TOLUD_MASK));
      Result = FALSE;
    }
  } else {
    DEBUG ((DEBUG_ERROR, "        Graphics Security Reserved Check is Disabled\n"));

    Result = FALSE;
  }

  return Result;
}

/**
  Check for DSM Registers Consistency

  @retval BOOLEAN - TRUE  DSM registers have the same address value programmed
                    FALSE DSM registers do not have same address or are equals to 0
**/
BOOLEAN
IsDsmRegistersConsistent (
  VOID
  )
{
  UINT32      BdsmAdr;
  UINTN       GttMmAdr;
  UINT32      BdsmGttMmAdr;

  BdsmAdr      = PciSegmentRead32 (
                   PCI_SEGMENT_LIB_ADDRESS (
                     SA_SEG_NUM,
                     SA_MC_BUS,
                     SA_MC_DEV,
                     SA_MC_FUN,
                     R_SA_BDSM
                   )
                 ) & B_SA_BDSM_BDSM_MASK;
  GttMmAdr     = GetGttMmABaseddr ();
  BdsmGttMmAdr = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_BDSM_OFFSET )) & B_SA_BDSM_BDSM_MASK;

  if (BdsmAdr != BdsmGttMmAdr) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found BdsmAdr: 0x%x != BdsmGttMmAdr: %x\n", BdsmAdr, BdsmGttMmAdr));
    return FALSE;
  }

  return TRUE;
}

/**
  Check for GSM Registers Consistency

  @retval BOOLEAN - TRUE  GSM registers have the same address value programmed
                    FALSE GSM registers do not have same address or are equals to 0
**/
BOOLEAN
IsGsmRegistersConsistent (
  VOID
  )
{
  UINT32          BgsmAdr;
  UINTN           GttMmAdr;
  UINT32          BgsmGttMmAdr;

  BgsmAdr      = PciSegmentRead32 (
                   PCI_SEGMENT_LIB_ADDRESS (
                     SA_SEG_NUM,
                     SA_MC_BUS,
                     SA_MC_DEV,
                     SA_MC_FUN,
                     R_SA_BGSM
                   )
                 ) & B_SA_BGSM_BGSM_MASK;
  GttMmAdr     = GetGttMmABaseddr ();
  BgsmGttMmAdr = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_BGSM_OFFSET )) & B_SA_BDSM_BDSM_MASK;

  if (BgsmAdr != BgsmGttMmAdr) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found BgsmAdr: 0x%x != BgsmGttMmAdr: %x\n", BgsmAdr, BgsmGttMmAdr));
    return FALSE;
  }

  return TRUE;
}

/**
  Check for GGC Registers Consistency

  @retval BOOLEAN - TRUE  GGC registers have the same address value programmed
                    FALSE GGC registers do not have same address or are equals to 0
**/
BOOLEAN
IsGgcRegistersConsistent (
  VOID
  )
{
  UINT32          GgcAdr;
  UINTN           GttMmAdr;
  UINT32          GgcGttMmAdr;

  GgcAdr      = PciSegmentRead32 (
                  PCI_SEGMENT_LIB_ADDRESS (
                    SA_SEG_NUM,
                    SA_MC_BUS,
                    SA_MC_DEV,
                    SA_MC_FUN,
                    R_SA_GGC
                  )
                ) & B_SA_GGC_GGC_MASK;
  GttMmAdr    = GetGttMmABaseddr ();
  GgcGttMmAdr = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_GGC_OFFSET )) & B_SA_GGC_GGC_MASK;

  if (GgcAdr != GgcGttMmAdr) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found GgcAdr: 0x%x != GgcGttMmAdr: %x\n", GgcAdr, GgcGttMmAdr));
    return FALSE;
  }

  return TRUE;
}

/**
  Check for TOLUD Registers Consistency

  @retval BOOLEAN - TRUE  TOLUD registers have the same address value programmed
                    FALSE TOLUD registers do not have same address or are equals to 0
**/
BOOLEAN
IsToludRegistersConsistent (
  VOID
  )
{
  UINT32          ToludAdr;
  UINTN           GttMmAdr;
  UINT32          ToludGttMmAdr;

  ToludAdr      = PciSegmentRead32 (
                    PCI_SEGMENT_LIB_ADDRESS (
                      SA_SEG_NUM,
                      SA_MC_BUS,
                      SA_MC_DEV,
                      SA_MC_FUN,
                      R_SA_TOLUD
                    )
                  ) & B_SA_TOLUD_TOLUD_MASK;
  GttMmAdr      = GetGttMmABaseddr ();
  ToludGttMmAdr = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_TOLUD_OFFSET )) & B_SA_TOLUD_TOLUD_MASK;

  if (ToludAdr != ToludGttMmAdr) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found ToludAdr: 0x%x != ToludGttMmAdr: %x\n", ToludAdr, ToludGttMmAdr));
    return FALSE;
  }

  return TRUE;
}

/**
  Check for TOUUD Registers Consistency

  @retval BOOLEAN - TRUE  TOUUD registers have the same address value programmed
                    FALSE TOLUD registers do not have same address or are equals to 0
**/
BOOLEAN
IsTouudRegistersConsistent (
  VOID
  )
{
  UINT32          TouudAdr;
  UINTN           GttMmAdr;
  UINT32          TouudGttMmAdr;

  TouudAdr      = PciSegmentRead32 (
                    PCI_SEGMENT_LIB_ADDRESS (
                      SA_SEG_NUM,
                      SA_MC_BUS,
                      SA_MC_DEV,
                      SA_MC_FUN,
                      R_SA_TOUUD
                    )
                  ) & B_SA_TOUUD_TOUUD_MASK;
  GttMmAdr      = GetGttMmABaseddr ();
  TouudGttMmAdr = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_TOUUD_OFFSET )) & B_SA_TOUUD_TOUUD_MASK;

  if (TouudAdr != TouudGttMmAdr) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found TouudAdr: 0x%x != TouudGttMmAdr: %x\n", TouudAdr, TouudGttMmAdr));
    return FALSE;
  }

  return TRUE;
}

/**
  Check for TSEG Registers Consistency

  @retval BOOLEAN - TRUE  TSEG registers have the same address value programmed
                    FALSE TSEG registers do not have same address or are equals to 0
**/
BOOLEAN
IsTsegRegistersConsistent (
  VOID
  )
{
  UINT32          TsegAdr;
  UINTN           GttMmAdr;
  UINT32          TsegGttMmAdr;

  TsegAdr      = PciSegmentRead32 (
                   PCI_SEGMENT_LIB_ADDRESS (
                     SA_SEG_NUM,
                     SA_MC_BUS,
                     SA_MC_DEV,
                     SA_MC_FUN,
                     R_SA_TSEGMB
                   )
                 ) & B_SA_TSEGMB_TSEGMB_MASK;
  GttMmAdr     = GetGttMmABaseddr ();
  TsegGttMmAdr = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_TSEGMB_OFFSET )) & B_SA_TSEGMB_TSEGMB_MASK;

  if (TsegAdr != TsegGttMmAdr) {
    DEBUG ((DEBUG_ERROR, "         Mistmatch found TsegAdr: 0x%x != TsegGttMmAdr: %x\n", TsegAdr, TsegGttMmAdr));
    return FALSE;
  }

  return TRUE;
}

/**
  Check DSM Bit Locked

  @retval BOOLEAN - TRUE  DSM bit is locked
                    FALSE DSM bit is not locked
**/
BOOLEAN
IsDsmBitLocked (
  VOID
  )
{
  UINT32   BdsmLock;

  BdsmLock = PciSegmentRead32 (
               PCI_SEGMENT_LIB_ADDRESS (
                 SA_SEG_NUM,
                 SA_MC_BUS,
                 SA_MC_DEV,
                 SA_MC_FUN,
                 R_SA_BDSM
               )
             ) & B_SA_BDSM_LOCK_MASK;

  if (BdsmLock == 0){
    DEBUG ((DEBUG_ERROR, "         BdsmLock bit is unlocked\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Check GSM Bit Locked

  @retval BOOLEAN - TRUE  GSM bit is locked
                    FALSE GSM bit is not locked
**/
BOOLEAN
IsGsmBitLocked (
  VOID
  )
{
  UINT32   BgsmLock;

  BgsmLock = PciSegmentRead32 (
               PCI_SEGMENT_LIB_ADDRESS (
                 SA_SEG_NUM,
                 SA_MC_BUS,
                 SA_MC_DEV,
                 SA_MC_FUN,
                 R_SA_BGSM
               )
             ) & B_SA_BGSM_LOCK_MASK;

  if (BgsmLock == 0){
    DEBUG ((DEBUG_ERROR, "         BgsmLock bit is unlocked\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Check GGC Bit Locked

  @retval BOOLEAN - TRUE  GGC bit is locked
                    FALSE GGC bit is not locked
**/
BOOLEAN
IsGgcBitLocked (
  VOID
  )
{
  UINT32    GgcLock;

  GgcLock = PciSegmentRead32 (
              PCI_SEGMENT_LIB_ADDRESS (
                SA_SEG_NUM,
                SA_MC_BUS,
                SA_MC_DEV,
                SA_MC_FUN,
                R_SA_GGC
              )
            ) & B_SA_GGC_GGCLCK_MASK;

  if (GgcLock == 0){
    DEBUG ((DEBUG_ERROR, "         GgcLock bit is unlocked\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Check TOLUD Bit Locked

  @retval BOOLEAN - TRUE  TOLUD bit is locked
                    FALSE TOLUD bit is not locked
**/
BOOLEAN
IsToludBitLocked (
  VOID
  )
{
  UINT32    ToludLock;

  ToludLock = PciSegmentRead32 (
                PCI_SEGMENT_LIB_ADDRESS (
                  SA_SEG_NUM,
                  SA_MC_BUS,
                  SA_MC_DEV,
                  SA_MC_FUN,
                  R_SA_TOLUD
                )
              ) & B_SA_TOLUD_LOCK_MASK;

  if (ToludLock == 0){
    DEBUG ((DEBUG_ERROR, "         ToludLock bit is unlocked\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Check TOUUD Bit Locked

  @retval BOOLEAN - TRUE  TOUUD bit is locked
                    FALSE TOUUD bit is not locked
**/
BOOLEAN
IsTouudBitLocked (
  VOID
  )
{
  UINT32    TouudLock;

  TouudLock = PciSegmentRead32 (
                PCI_SEGMENT_LIB_ADDRESS (
                  SA_SEG_NUM,
                  SA_MC_BUS,
                  SA_MC_DEV,
                  SA_MC_FUN,
                  R_SA_TOUUD
                )
               ) & B_SA_TOUUD_LOCK_MASK;

  if (TouudLock == 0){
    DEBUG ((DEBUG_ERROR, "         TouudLock bit is unlocked\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Check TSEG Bit Locked

  @retval BOOLEAN - TRUE  TSEG bit is locked
                    FALSE TSEG bit is not locked
**/
BOOLEAN
IsTsegBitLocked (
  VOID
  )
{
  UINT32   TsegLock;

  TsegLock = PciSegmentRead32 (
               PCI_SEGMENT_LIB_ADDRESS (
                 SA_SEG_NUM,
                 SA_MC_BUS,
                 SA_MC_DEV,
                 SA_MC_FUN,
                 R_SA_TSEGMB
               )
             ) & B_SA_TSEGMB_LOCK_MASK;

  if (TsegLock == 0){
    DEBUG ((DEBUG_ERROR, "         TsegLock bit is unlocked\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Check DSM range valid

  @retval BOOLEAN - TRUE  DSM range valid
                    FALSE DSM range valid
**/
BOOLEAN
IsDsmRangeValid (
  VOID
  )
{
  UINT32           BdsmAdrBase;
  UINT32           ToludAdrBase;
  UINT32           DsmSize;

  // BDSM Base (IOP) B0/D0/F0:R B0h, bits[31:20]
  BdsmAdrBase  =  (PciSegmentRead32 (
                     PCI_SEGMENT_LIB_ADDRESS (
                       SA_SEG_NUM,
                       SA_MC_BUS,
                       SA_MC_DEV,
                       SA_MC_FUN,
                       R_SA_BDSM
                     )
                   ) & B_SA_BDSM_BDSM_MASK);
  // TOLUD Base (IOP)B0/D0/F0:R BCh, bits[31:20]
  ToludAdrBase   = PciSegmentRead32 (
                     PCI_SEGMENT_LIB_ADDRESS (
                       SA_SEG_NUM,
                       SA_MC_BUS,
                       SA_MC_DEV,
                       SA_MC_FUN,
                       R_SA_TOLUD
                     )
                   ) & B_SA_TOLUD_TOLUD_MASK;
  DsmSize        = GetIgdGmsSizeInMb ();

  if ((BdsmAdrBase + DsmSize) != ToludAdrBase) {
    DEBUG ((DEBUG_ERROR, "         Invalid Range found BDSM: 0x%x + GGMSize: 0x%x != TOLUD: 0x%x\n", BdsmAdrBase, DsmSize, ToludAdrBase));

    return FALSE;
  }

  return TRUE;
}

/**
  Check GSM range valid

  @retval BOOLEAN - TRUE  GSM range valid
                    FALSE GSM range valid
**/
BOOLEAN
IsGsmRangeValid (
  VOID
  )
{
  UINT32          BgsmAdrBase;
  UINT32          BdsmAdrBase;
  UINT32          GsmSize;

  BgsmAdrBase = PciSegmentRead32 (
                  PCI_SEGMENT_LIB_ADDRESS (
                    SA_SEG_NUM,
                    SA_MC_BUS,
                    SA_MC_DEV,
                    SA_MC_FUN,
                    R_SA_BGSM
                  )
                ) & B_SA_BGSM_BGSM_MASK;
  BdsmAdrBase = PciSegmentRead32 (
                  PCI_SEGMENT_LIB_ADDRESS (
                    SA_SEG_NUM,
                    SA_MC_BUS,
                    SA_MC_DEV,
                    SA_MC_FUN,
                    R_SA_BDSM
                  )
                ) & B_SA_BDSM_BDSM_MASK;
  GsmSize     = GetIgdGgmsSizeInMb ();

  if ((BgsmAdrBase + GsmSize) != BdsmAdrBase){
    DEBUG ((DEBUG_ERROR, "         Invalid Range found BGSM: 0x%x + GGMSize: 0x%x != BDSM: 0x%x\n", BgsmAdrBase, GsmSize, BdsmAdrBase));

    return FALSE;
  }

  return TRUE;
}

/**
  Run tests for SecureIntegratedGraphicsConfiguration bit
**/
VOID
CheckSecureIntegratedGraphicsConfiguration (
  VOID
  )
{
  EFI_STATUS      Status;
  BOOLEAN         Result;
  UINT32          IgdVendorId;
  UINT16          IgdDid;
  UINT64          GttMmAdr;
  UINT64          McD2BaseAddress;

  //
  // Read Integrated Graphics Device
  //
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                      SA_SEG_NUM,
                      GetIgdBusNumber(),
                      GetIgdDevNumber(),
                      GetIgdFuncNumber(),
                      0
                    );
  IgdVendorId     = PciSegmentRead32 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) & B_PCI_VENDOR_ID_MASK;
  IgdDid          = PciSegmentRead16 (
                      PCI_SEGMENT_LIB_ADDRESS (
                        SA_SEG_NUM,
                        GetIgdBusNumber(),
                        GetIgdDevNumber(),
                        GetIgdFuncNumber(),
                        PCI_DEVICE_ID_OFFSET
                      )
                    );
  GttMmAdr        = GetGttMmABaseddr ();

  if (((mFeatureImplemented[1] & HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION) == 0) ||
      (IgdVendorId == B_PCI_VENDOR_ID_MASK) || (IgdDid == B_IGD_DID_MASK)) {
    return;
  }

  Result = TRUE;

  DEBUG ((DEBUG_INFO, "      IGD Stolen Memory Consistency Test\n"));
  if (IsIgdStolenMemoryConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: Mistmatch found among BGSM, BDSM- GSMSize or/and TOLUD - DSMSize\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_2,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_2
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      Reserved Check Test\n"));
  if (IsReservedBitEnabled () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: Graphics Security Reserved bit Check failed\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_3,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_3
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      WOPCM BASE consistency Test\n"));
  if (IsWoPcmBaseRegistersConsistent (GttMmAdr) == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: One or more of the WOPMC Base Register(s) is/are not equal among them or is/are equal zero \n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_4,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_4
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      DSM consistency Test\n"));
  if (IsDsmRegistersConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: DSM Registers are not equal or are equal zero \n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_5,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_5
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      GSM consistency Test\n"));
  if (IsGsmRegistersConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: GSM Registers are not equal or are equal zero \n"));
    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_6,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_6
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      GGC consistency Test\n"));
  if (IsGgcRegistersConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: GGC Registers are not equal or are equal zero \n"));
    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_7,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_7
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      TOLUD consistency Test\n"));
  if (IsToludRegistersConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: Tolud Registers are not equal or are equal zero \n"));
    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_8,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_8
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      TOUUD consistency Test\n"));
  if (IsTouudRegistersConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: TOUUD Registers are not equal or are equal zero \n"));
    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_9,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_9
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      TSEG consistency Test\n"));
  if (IsTsegRegistersConsistent () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: TSEG Registers are not equal or are equal zero \n"));
    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_A,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_A
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      PAVP bit locked Test\n"));
  if (IsPavpBitLocked (GttMmAdr) == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: PAVP bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_B,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_B
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      DSM bit locked Test\n"));
  if (IsDsmBitLocked () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: DSM bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_C,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_C
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      GSM bit locked Test\n"));
  if (IsGsmBitLocked () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: GSM bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_D,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_D
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      GGC bit locked Test\n"));
  if (IsGgcBitLocked () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: GGC bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_E,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_E
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      TOLUD bit locked Test\n"));
  if (IsToludBitLocked () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: TOLUD bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_F,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_F
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      TOUUD bit locked Test\n"));
  if (IsTouudBitLocked () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: TOUUD bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_10,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_10
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      TSEG bit locked Test\n"));
  if (IsTsegBitLocked () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: TSEG bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_11,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_11
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      RC6 bit locked Test\n"));
  if (IsRc6Locked (GttMmAdr) == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: RC6 bit is unlocked\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_12,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_12
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      DSM range valid Test\n"));
  if (IsDsmRangeValid () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: DSM range is invalid\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_13,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_13
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      GSM range valid Test\n"));
  if (IsGsmRangeValid () == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: GSM range is invalid\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_14,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_14
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      WOPCM range valid Test\n"));
  if (IsWoPcmRangeValid (GttMmAdr) == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: WOPCM range is invalid\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_15,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_15
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      RC6 range valid Test\n"));
  if (IsRc6RangeValid (GttMmAdr) == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: RC6 range is invalid\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_16,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_16
      );
    Result = FALSE;
  }

  DEBUG ((DEBUG_INFO, "      Doorbell range valid Test\n"));
  if (IsDoorbellRangeValid (GttMmAdr) == FALSE) {
    DEBUG ((DEBUG_INFO, "        Unexpected Status: Doorbell range is invalid\n"));

    BuildAndAppendHstiStatusString (
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_CODE_17,
      HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION_UNEXP_STATUS_STRING_17
      );
    Result = FALSE;
  }

  //
  // ALL PASS
  //
  if (Result) {
    Status = HstiLibSetFeaturesVerified (
               PLATFORM_SECURITY_ROLE_PLATFORM_REFERENCE,
               NULL,
               1,
               HSTI_BYTE1_SECURE_INTEGRATED_GRAPHICS_CONFIGURATION
               );
    CheckStatusForHstiLibSet (Status);
  }

  return;
}
