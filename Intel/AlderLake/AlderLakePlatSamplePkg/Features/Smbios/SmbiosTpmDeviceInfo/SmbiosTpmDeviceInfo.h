/**@file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SMBIOS_TPM_DEVICE_INFO_H_
#define _SMBIOS_TPM_DEVICE_INFO_H_

#include <IndustryStandard/SmBios.h>
#include <Guid/TpmInstance.h>

#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PcdLib.h>
#include <Library/PrintLib.h>
#include <Library/Tpm2CommandLib.h>
///
/// Driver Consumed Protocol Prototypes
///
#include <Protocol/Smbios.h>

#define EFI_SMBIOS_TYPE_TPM_DEVICE       SMBIOS_TYPE_TPM_DEVICE

#define SMBIOS_TYPE43_NUMBER_OF_STRINGS  1
#define NO_STRING_AVAILABLE              0
#define STRING_1                         1

///
/// Non-static SMBIOS table data to be filled later with a dynamically generated value
///
#define TO_BE_FILLED                     0
#define TO_BE_FILLED_STRING              " "        ///< Initial value should not be NULL

#define ASCII_NULL_CHAR                  0x00
#define ASCII_SPACE_CHAR                 0x20

#pragma pack(1)
typedef struct {
  CHAR8 *Description;
} SMBIOS_TYPE43_STRING_ARRAY;
#pragma pack()

typedef enum {
  TpmDeviceNone  = 0,
  TpmDevice12TPM = 1,
  TpmDevice20TPM = 2,
} SUPPORTED_TPM_DEVICE_TYPES;

#endif // _SMBIOS_TPM_DEVICE_INFO_H_
