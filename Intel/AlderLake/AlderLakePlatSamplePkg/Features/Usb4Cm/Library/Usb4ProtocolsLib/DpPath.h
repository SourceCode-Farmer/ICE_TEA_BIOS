/** @file
  Header file for DisplayPort path configuration.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _DP_PATH_H_
#define _DP_PATH_H_

#include <Uefi.h>
#include <Usb4RouterDefs.h>

/**
  Enable/Disable Video/Aux path of DP adapter.

  @param[in] Router  - Pointer to Router.
  @param[in] AdpNum  - DP adapter number.
  @param[in] Enable  - Indicate the path should be enabled or disabled.

  @retval EFI_SUCCESS           - Enable Video/Aux path of DP adapter success.
  @retval EFI_UNSUPPORTED       - Enable Video/Aux path of DP adapter failure.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
DpAdpEnablePath (
  IN PUSB4_ROUTER    Router,
  IN UINT8           AdpNum,
  IN BOOLEAN         Enable
  );

/**
  Traverse and configure the path between DP source adapter and DP sink adapter.

  @param[in] DpSrcRt      - Pointer to DP Source Router.
  @param[in] DpSrcAdpNum  - DP-IN adapter number of DP source.
  @param[in] DpSinkRt     - Pointer to DP Sink Router.
  @param[in] DpSinkAdpNum - DP_OUT adapter number of DP sink.
  @param[in] StreamId     - DP stream ID for identifying DP path.

  @retval EFI_SUCCESS           - Setup DP path successfully.
  @retval EFI_UNSUPPORTED       - Fail to setup DP path.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
DpPathSetup (
  IN PUSB4_ROUTER    DpSrcRt,
  IN UINT8           DpSrcAdpNum,
  IN PUSB4_ROUTER    DpSinkRt,
  IN UINT8           DpSinkAdpNum,
  IN UINT32          StreamId
  );

/**
  Tear down the DP OUT path between the DP source and DP sink.
  If RemovedRt is specified, tear down DP path from DP source to RemovedRt's upper level router.

  @param[in] DpSrcRt      - Pointer to DP Source Router.
  @param[in] DpSrcAdpNum  - DP-IN adapter number of DP source.
  @param[in] DpSinkRt     - Pointer to DP Sink Router.
  @param[in] DpSinkAdpNum - DP_OUT adapter number of DP sink.
  @param[in] StreamId     - DP stream ID for identifying DP path.
  @param[in] RemovedRt    - Removed router in the DP path (optional)

  @retval EFI_SUCCESS           - DP path tear down success.
  @retval EFI_UNSUPPORTED       - Fail to tear down DP path.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
DpPathTearDown (
  IN PUSB4_ROUTER    DpSrcRt,
  IN UINT8           DpSrcAdpNum,
  IN PUSB4_ROUTER    DpSinkRt,
  IN UINT8           DpSinkAdpNum,
  IN UINT32          StreamId,
  IN PUSB4_ROUTER    RemovedRt
  );
#endif
