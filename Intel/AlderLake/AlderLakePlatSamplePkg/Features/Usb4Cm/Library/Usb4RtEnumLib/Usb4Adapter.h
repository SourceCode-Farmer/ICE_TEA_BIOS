/** @file
  Header file for USB4 adapter specific implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_ADAPTER_H_
#define _USB4_ADAPTER_H_

#include <Uefi.h>
#include <Usb4RouterDefs.h>

/**
  Enumerate USB4 Adapter and its Capabilities.

  @param[in,out] Router - Pointer to USB4 Router instance.
  @param[in]     AdpNum - Adapter Number.
  @param[in]     Lane0  - Lane0 indicator. Enumerated adapter should be Lane0 in USB4 Port if it's Lane adapter.

  @retval EFI_SUCCESS           - Enumerate USB4 Adapter success.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access.
**/
EFI_STATUS
Usb4AdpEnum (
  IN OUT PUSB4_ROUTER    Router,
  IN  UINT8              AdpNum,
  IN  BOOLEAN            Lane0
  );

/**
  Query Lane Adapter State.

  @param[in]  Router       - Pointer to Router instance.
  @param[in]  AdpNum       - Downstream Adapter number.
  @param[out] AdapterState - Lane adpater state.

  @retval EFI_SUCCESS           - Query Lane adapter state successfully.
  @retval EFI_UNSUPPORTED       - Not a Lane adapter or Lane adapter capability is not present.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
QueryLaneAdapterState (
  IN  PUSB4_ROUTER          Router,
  IN  UINT8                 AdpNum,
  OUT LANE_ADAPTER_STATE    *AdapterState
  );

/**
  Unlock Router Downstream Adapter.

  @param[in] Router - Pointer to Router instance.
  @param[in] AdpNum - Adapter number to be unlocked.

  @retval EFI_SUCCESS           - Unlock Router Downstream Adapter success.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval EFI_UNSUPPORTED       - Unsupported adapter type.
  @retval Errors in config space access.
**/
EFI_STATUS
LaneAdapterUnlock (
  IN PUSB4_ROUTER    Router,
  IN UINT8           AdpNum
  );

/**
  Bonding dual lanes link in USB4 Port.

  @param[in,out] Router - Pointer to Router instance.
  @param[in]     AdpNum - Adapter Number.

  @retval EFI_SUCCESS           - Bonding dual lanes link success.
  @retval EFI_UNSUPPORTED       - Lane bonding is not supported.
  @retval EFI_TIMEOUT           - Lane bonding timeout
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
AdapterLaneBonding (
  IN OUT PUSB4_ROUTER    Router,
  IN     UINT8           AdpNum
  );
#endif
