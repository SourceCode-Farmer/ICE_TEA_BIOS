/** @file
  Header file for dump functions of USB4 capabilities and path entries.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_DUMP_H_
#define _USB4_DUMP_H_

#include <Uefi.h>
#include <Usb4ConfigSpace.h>
#include <Usb4Capabilities.h>

/**
  Dump adapter TMU capabilities.

  @param[in] TmuCap - Pointer to TMU capabilities.
**/
VOID
DumpLaneAdapterTmuCap (
  IN USB4_ADP_CAP_TMU    *TmuCap
  );

/**
  Dump Lane adapter capabilities.

  @param[in] AdpCap - Pointer to Lane adapter capabilities.
**/
VOID
DumpLaneCap (
  IN USB4_ADP_CAP_LANE    *AdpCap
  );

/**
  Dump DP-IN adapter capability.

  @param[in] DpInCap - Pointer to DP-IN adapter capability.
**/
VOID
DumpAdpDpInCap (
  IN USB4_ADP_CAP_DP_IN    *DpInCap
  );

/**
  Dump DP-OUT adapter capability.

  @param[in] DpOutCap - Pointer to DP-OUT adapter capability.
**/
VOID
DumpAdpDpOutCap (
  IN USB4_ADP_CAP_DP_OUT    *DpOutCap
  );

/**
  Dump USB3 adapter capabilities

  @param[in] AdpCap - Pointer to USB3 adapter capabilities.
**/
VOID
DumpAdpUsb3Cap (
  IN USB4_ADP_CAP_USB3    *AdpCap
  );

/**
  Dump PCIe adapter capabilities

  @param[in] AdpCap - Pointer to PCIe adapter capabilities.
**/
VOID
DumpAdpPcieCap (
  IN USB4_ADP_CAP_PCIE    *AdpCap
  );

/**
  Dump Lane adapter capabilities CS DWORD1

  @param[in] AdpCap - Pointer to Lane adapter capabilities CS DWORD1.
**/
VOID
DumpLaneCapCs1 (
  IN USB4_ADP_CAP_LANE_CS_1    *AdpCapCs1
  );

/**
  Dump path entry

  @param[in] PathEntry - Pointer to the path entry
**/
VOID
DumpPathEntry (
  IN USB4_PATH_CS_ENTRY    *PathEntry
  );
#endif
