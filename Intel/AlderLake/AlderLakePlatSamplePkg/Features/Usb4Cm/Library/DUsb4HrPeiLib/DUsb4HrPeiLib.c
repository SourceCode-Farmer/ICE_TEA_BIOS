/** @file
  Discrete USB4 host controller initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Ppi/PeiDTbtPolicy.h>
#include <IndustryStandard/Pci22.h>
#include <DTbtDefinition.h>
#include <Library/DTbtCommonLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/CmosAccessLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PcdLib.h>
#include <Library/TimerLib.h>
#include <Library/DebugLib.h>
#include <Usb4PlatformInfo.h>
#include <Usb4CmMode.h>

/**
  Query the discrete USB4 host router information in the platform.

  @param[out] Usb4PlatformInfo - Pointer to USB4 host routers information in the platform.

  @retval EFI_SUCCESS           - Query the discrete USB4 host router information successfully.
  @retval EFI_NOT_FOUND         - No discrete USB4 host router information found in platform.
  @retval EFI_UNSUPPORTED       - Fail to query the discrete USB4 host router information.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Dusb4PlatformInfoQuery (
  OUT USB4_PLATFORM_INFO    *Usb4PlatformInfo
  )
{
  EFI_STATUS         Status;
  PEI_DTBT_POLICY    *PeiDTbtConfig;
  PUSB4_HR_INFO      Usb4HrInfo;
  UINT8              HrIndex;
  UINT8              Index;
  UINT8              RpType;
  UINT8              RpNum;
  UINTN              RpDev;
  UINTN              RpFunc;
  UINT8              TbtTempBus;
  UINT16             MailboxCmdReg;
  UINT16             MailboxStatusReg;

  DEBUG ((DEBUG_INFO, "Dusb4PlatformInfoQuery entry\n"));

  if (Usb4PlatformInfo == NULL) {
    DEBUG ((DEBUG_ERROR, "Dusb4PlatformInfoQuery: Null Usb4PlatformInfo!\n", Usb4PlatformInfo));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  PeiDTbtConfig = NULL;
  Status = PeiServicesLocatePpi (
             &gPeiDTbtPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &PeiDTbtConfig
             );
  if (PeiDTbtConfig == NULL) {
    DEBUG ((DEBUG_INFO, "No discrete USB4 host router found in platform!\n"));
    Status = EFI_NOT_FOUND;
    goto Exit;
  }

  if (PeiDTbtConfig->DTbtGenericConfig.Usb4CmMode & USB4_CM_MODE_SWITCH_UNSUPPORTED) {
    DEBUG ((DEBUG_INFO, "Dusb4PlatformInfoQuery: USB4 CM mode switch is not supported in platform!\n"));
    Usb4PlatformInfo->CmSwitchSupport = FALSE;
    Usb4PlatformInfo->CmMode = USB4_CM_MODE_FW_CM;
  } else {
    Usb4PlatformInfo->CmSwitchSupport = TRUE;
    Usb4PlatformInfo->CmMode = PeiDTbtConfig->DTbtGenericConfig.Usb4CmMode;
  }

  //
  // Retrieve dTBT control information from PCD
  //
  TbtTempBus       = FixedPcdGet8 (PcdTbtTempBusNumber);
  MailboxCmdReg    = FixedPcdGet16 (PcdPcieToDTbtRegister);
  MailboxStatusReg = FixedPcdGet16 (PcdDTbtToPcieRegister);

  //
  // Search discrete USB4 host router information in platform
  //
  HrIndex = Usb4PlatformInfo->Usb4HrCount;
  for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++)
  {
    if (PeiDTbtConfig->DTbtControllerConfig[Index].DTbtControllerEn != 0) {
      //
      // PcieRpNumber is 1-base and RpNum is 0-base
      //
      RpType = PeiDTbtConfig->DTbtControllerConfig[Index].Type;
      RpNum  = PeiDTbtConfig->DTbtControllerConfig[Index].PcieRpNumber - 1;
      DEBUG ((DEBUG_INFO, "Discrete USB4 host router %d : RpType = %d, PCIe RP number = %d\n", Index, RpType, RpNum + 1));
      Status = GetDTbtRpDevFun (RpType, RpNum, &RpDev, &RpFunc);
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "DUsb4PlatformInfoQuery: Fail to get dTBT RP Dev/Func\n"));
        Status = EFI_UNSUPPORTED;
        goto Exit;
      }
      DEBUG ((DEBUG_INFO, "Discrete USB4 host router %d : Dev = %d, Func = %d\n", Index, RpDev, RpFunc));

      if (HrIndex >= USB4_HR_SUPPORT_MAX) {
        DEBUG ((DEBUG_ERROR, "Dusb4PlatformInfoQuery: No more USB4 host router can be supported!\n"));
        Status = EFI_UNSUPPORTED;
        goto Exit;
      }

      //
      // Fill in discrete USB4 host router information.
      // Discrete USB4 host router information consists of RP, upstream port, downstream port and NHI device.
      // Upstream port is the target for mailbox command communication.
      // NHI device is the device providing USB4 host interface for USB4 config space access.
      //
      Usb4HrInfo = &(Usb4PlatformInfo->Usb4Hr[HrIndex]);

      Usb4HrInfo->IntegratedHr = FALSE;
      //
      // Supported USB4 control
      // bit 0 - USB Tunneling
      // bit 1 - DisplayPort Tunneling
      // bit 2 - PCIe Tunneling
      // bit 3 - Inter-Domain USB4 Internet Protocol
      // bit 31:4 - Reserved
      //
      Usb4HrInfo->Usb4Control.Value = DEFAULT_USB4_CONTROLS;
      if (Usb4PlatformInfo->PcieOverUsb4En == 0) {
        Usb4HrInfo->Usb4Control.Fields.Pcie = 0;
      }
      //
      // Target for mailbox command communication
      //
      Usb4HrInfo->MbStatusReg     = MailboxStatusReg;
      Usb4HrInfo->MbCmdReg        = MailboxCmdReg;
      Usb4HrInfo->MailboxBdf.Bus  = TbtTempBus;
      Usb4HrInfo->MailboxBdf.Dev  = 0;
      Usb4HrInfo->MailboxBdf.Func = 0;
      //
      // Root port information
      //
      Usb4HrInfo->Rp.Bdf.Bus  = 0;
      Usb4HrInfo->Rp.Bdf.Dev  = (UINT8) RpDev;
      Usb4HrInfo->Rp.Bdf.Func = (UINT8) RpFunc;
      Usb4HrInfo->Rp.PriBus   = 0;
      Usb4HrInfo->Rp.SecBus   = TbtTempBus;
      Usb4HrInfo->Rp.SubBus   = TbtTempBus + 2;
      //
      // Upstream port information
      //
      Usb4HrInfo->UsPort.Bdf.Bus  = TbtTempBus;
      Usb4HrInfo->UsPort.Bdf.Dev  = 0;
      Usb4HrInfo->UsPort.Bdf.Func = 0;
      Usb4HrInfo->UsPort.PriBus   = TbtTempBus;
      Usb4HrInfo->UsPort.SecBus   = TbtTempBus + 1;
      Usb4HrInfo->UsPort.SubBus   = TbtTempBus + 2;
      //
      // Downstream port information
      //
      Usb4HrInfo->DsPort.Bdf.Bus  = TbtTempBus + 1;
      Usb4HrInfo->DsPort.Bdf.Dev  = 0;
      Usb4HrInfo->DsPort.Bdf.Func = 0;
      Usb4HrInfo->DsPort.PriBus   = TbtTempBus + 1;
      Usb4HrInfo->DsPort.SecBus   = TbtTempBus + 2;
      Usb4HrInfo->DsPort.SubBus   = TbtTempBus + 2;
      //
      // Native Host Interface device (DMA controller) information
      //
      Usb4HrInfo->NhiBdf.Bus  = TbtTempBus + 2;
      Usb4HrInfo->NhiBdf.Dev  = 0;
      Usb4HrInfo->NhiBdf.Func = 0;

      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] BDF = (%d,%d,%d)\n",
              Index,
              Usb4HrInfo->NhiBdf.Bus,
              Usb4HrInfo->NhiBdf.Dev,
              Usb4HrInfo->NhiBdf.Func
            ));
      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] Root port BDF = (%d,%d,%d)\n",
              Index,
              Usb4HrInfo->Rp.Bdf.Bus,
              Usb4HrInfo->Rp.Bdf.Dev,
              Usb4HrInfo->Rp.Bdf.Func
            ));
      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] Upstream port BDF = (%d,%d,%d)\n",
              Index,
              Usb4HrInfo->UsPort.Bdf.Bus,
              Usb4HrInfo->UsPort.Bdf.Dev,
              Usb4HrInfo->UsPort.Bdf.Func
            ));
      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] Downstream port BDF = (%d,%d,%d)\n",
              Index,
              Usb4HrInfo->DsPort.Bdf.Bus,
              Usb4HrInfo->DsPort.Bdf.Dev,
              Usb4HrInfo->DsPort.Bdf.Func
            ));
      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] mailbox command register offset = 0x%X\n", Index, Usb4HrInfo->MbCmdReg));
      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] mailbox status register offset = 0x%X\n", Index, Usb4HrInfo->MbStatusReg));
      DEBUG ((DEBUG_INFO, "Discrete USB4 HR[%d] USB4 control = 0x%0X\n", Index, Usb4HrInfo->Usb4Control));

      //
      // Enabled USB4 host router mask
      // bit 0 - 3 : Integrated USB4 host router 0 - 3
      // bit 4 - 7 : Discrete USB4 host router 0 - 3
      //
      Usb4PlatformInfo->Usb4HrMask |= (1 << (Index + 4));
      Usb4PlatformInfo->Usb4HrCount++;
      HrIndex++;
      TbtTempBus += 3;
    }
  } // for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++)

  Status = EFI_SUCCESS;

Exit:
  DEBUG ((DEBUG_INFO, "Dusb4PlatformInfoQuery exit\n"));
  return Status;
}