/** @file
  USB4 event handler implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/Usb4HiCoreLib.h>
#include <Usb4HrInst.h>
#include "CmEvtHotPlug.h"

/**
  CM event handler

  @param[in] Usb4Hr   - Pointer to USB4 host router instance.
  @param[in] RxEvent  - Pointer to RX event.

  @retval EFI_SUCCESS           - Rx event is processed successfully.
  @retval EFI_UNSUPPORTED       - Unexpected process Rx events failure.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
CmEventHandler (
  IN USB4_HR_INSTANCE    *Usb4Hr,
  IN PRX_EVENT           RxEvent
  )
{
  EFI_STATUS    Status;

  if ((Usb4Hr == NULL) || (RxEvent == NULL)) {
    DEBUG ((DEBUG_ERROR, "CmEventHandler: Invalid parameter (%p, %p)\n", Usb4Hr, RxEvent));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  if (RxEvent->Pdf == PDF_HOT_PLUG) {
    Status = CmHotPlugHandler (Usb4Hr, RxEvent);
  } else {
    Status = EFI_UNSUPPORTED;
  }

Exit:
  return Status;
}

/**
  Process Rx events if any pending Rx events are available in the Rx event queue.

  @param[in] Usb4Hr - Pointer to USB4 host router instance.

  @retval EFI_SUCCESS           - Process Rx events successfully.
  @retval EFI_UNSUPPORTED       - Unexpected process Rx events failure.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
CmProcessRxEvents (
  IN USB4_HR_INSTANCE    *Usb4Hr
  )
{
  EFI_STATUS    Status;
  RX_EVENT      RxEvent;
  UINT32        EvtCount;

  if (Usb4Hr == NULL) {
    DEBUG ((DEBUG_ERROR, "CmProcessRxEvents: Null Usb4Hr\n"));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  Status = EFI_SUCCESS;
  EvtCount = 0;

  //
  // Query Rx events until no pending Rx event available.
  //
  while (Usb4HiQueryRxEvent (&(Usb4Hr->HiCore), &RxEvent) == EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "RxEvent received - Rt 0x%016llx adp %d Pdf %d Unplug %d\n",
            RxEvent.TopologyId, RxEvent.AdpNum, RxEvent.Pdf, RxEvent.Unplug));
    Status = CmEventHandler (Usb4Hr, &RxEvent);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "CmProcessRxEvents: CmEventHandler failure, %r\n", Status));
    }
    EvtCount++;
    //
    // Stop processing events if the processed event count exceeds the maximum range.
    // Prevent the while loop being stuck by event storm.
    //
    if (EvtCount >= MAX_PROCESSED_RX_EVENTS) {
      break;
    }
  }

Exit:
  return Status;
}
