/** @file
  Header file for event report and processing of USB4 host interface.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _HI_CORE_EVT_H_
#define _HI_CORE_EVT_H_

#include <Uefi.h>
#include <Usb4HiCoreDefs.h>
#include <Library/Usb4HiCoreLib.h>

/**
  Initialize the event queues of USB4 host interface core instance.

  @param[in] HiCore - Pointer to USB4 Host Interface Core instance.

  @retval EFI_SUCCESS           - Initialize the event queue successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
HiCoreEvtQueueInit (
  IN PUSB4_HI_CORE    HiCore
  );

/**
  Uninitialize the event queues of USB4 host interface core instance.

  @param[in] HiCore - Pointer to USB4 Host Interface Core instance.
**/
VOID
HiCoreEvtQueueUninit (
  IN PUSB4_HI_CORE    HiCore
  );

/**
  Get an empty RxEvent slot for RxEvent report

  @param[in]  HiCore   - Pointer to USB4 Host Interface Core instance.
  @param[out] EmptyEvt - Pointer of pointer to the returned RxEvent.

  @retval EFI_SUCCESS           - Get an empty RxEvent success.
  @retval EFI_OUT_OF_RESOURCES  - No empty RxEvent available.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
GetEmptyRxEvent (
  IN  PUSB4_HI_CORE    HiCore,
  OUT PRX_EVENT        *EmptyEvt
  );

/**
  Free a RxEvent

  @param[in] HiCore  - Pointer to USB4 host interface core instance.
  @param[in] RxEvent - Pointer to RxEvent to be freed.
**/
VOID
FreeRxEvent (
  IN PUSB4_HI_CORE    HiCore,
  IN PRX_EVENT        RxEvent
  );

/**
  Check if the two events are duplicated events.

  @param[in] RxEvt1 - Pointer to the first Rx Event for comparison.
  @param[in] RxEvt2 - Pointer to the second Rx Event for comparison.

  @retval TRUE  - The two Rx events are duplicated events.
  @retval FALSE - The two Rx events are different events.
**/
BOOLEAN
IsDuplicatedEvent (
  IN PRX_EVENT    RxEvt1,
  IN PRX_EVENT    RxEvt2
 );

/**
  Report hot plug event to the internal event queue of USB4 Host Interface Core for processing later.

  @param[in] HiCore     - Pointer to USB4 host interface core instance.
  @param[in] HotPlugPkt - Pointer to the hot plug packet.

  @retval EFI_SUCCESS           - Report hot plug event to the internal event queue success.
  @retval EFI_OUT_OF_RESOURCES  - The internal event queue is full for inserting event.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
ReportHotplugEvent (
  IN PUSB4_HI_CORE                  HiCore,
  IN PUSB4_HOT_PLUG_EVENT_PACKET    HotPlugPkt
  );

#endif
