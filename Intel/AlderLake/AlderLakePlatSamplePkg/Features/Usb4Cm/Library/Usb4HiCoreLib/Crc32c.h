/** @file
  Header of the CRC-32C standard implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _CRC_32C_H_
#define _CRC_32C_H_

#define CRC32C_POLY_REVERSED      0x82F63B78
#define CRC32C_POLY_NORMAL        0x1EDC6F41

#define CRC32C_INITIAL_REMAINDER  0xFFFFFFFF

/**
  Generate a CRC-32C table that can speed up CRC calculation by table look up
**/
VOID
Crc32cInit (
  VOID
  );

/**
  Calculate CRC value of the input data based on CRC-32C standard

  @param[in]  Data     - Pointer to source data for calculating CRC codes
  @param[in]  DataSize - Data length in bytes of source data
  @param[out] CrcOut   - Calculated CRC codes for source data

  @retval EFI_SUCCESS           - CRC calculation success
  @retval EFI_INVALID_PARAMETER - Invalid input parameter is specified
**/
EFI_STATUS
CalCrc32c (
  IN  UINT8     *Data,
  IN  UINT32    DataSize,
  OUT UINT32    *CrcOut
  );

#endif
