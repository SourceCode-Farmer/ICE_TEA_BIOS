/** @file
  Header file for USB4 control packets transmit and receive.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_CTRL_PKT_H_
#define _USB4_CTRL_PKT_H_

#include <Uefi.h>
#include <Usb4HiCoreDefs.h>

/**
  Send the USB4 control packet to the host router Tx ring.

  @param[in] HiCore    - Pointer to the USB4 Host Interface Core instance.
  @param[in] PktBuf    - Pointer to packet buffer.
  @param[in] Pdf       - PDF value.
  @param[in] PktLen    - Packet length in bytes.

  @retval EFI_SUCCESS           - Send control packet successfully.
  @retval EFI_OUT_OF_RESOURCES  - No Tx buffer to send the control packet.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CtrlPktSend (
  IN PUSB4_HI_CORE    HiCore,
  IN UINT8            *PktBuf,
  IN USB4_CTRL_PDF    Pdf,
  IN UINT16           PktLen
  );

/**
  Receive the USB4 control response packet from the host router Rx ring.

  @param[in]  HiCore         - Pointer to the USB4 Host Interface Core instance.
  @param[out] PktBuf         - Pointer to packet buffer.
  @param[in]  Pdf            - PDF value.
  @param[in]  ExpectedPktLen - Expected response packet length in bytes.

  @retval EFI_SUCCESS           - Send control packet successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval EFI_DEVICE_ERROR      - Error notification is received or the received data length is not expected.
  @retval EFI_NOT_FOUND         - ERR_ADDR notification event is received from Rx ring.
  @retval EFI_TIMEOUT           - The expected PDF packet is not received after timeout period.
**/
EFI_STATUS
Usb4CtrlPktRcv (
  IN PUSB4_HI_CORE    HiCore,
  IN UINT8            *PktBuf,
  IN USB4_CTRL_PDF    Pdf,
  IN UINT16           ExpectedPktLen
  );

/**
  Send Notification Packet to ack the events from USB4 Routers.

  @param[in] HiCore       - Pointer to the USB4 Host Interface Core instance.
  @param[in] TopologyId   - Pointer to the Topology ID of the target router.
  @param[in] AdpNum       - Adapter number.
  @param[in] Event        - Notification event code
  @param[in] Plug         - Differentiate Hot Plug and Hot Unplug Events in a Hot Plug Acknowledgment Packet
                            0 = Not a Hot Plug Acknowledgment Packet
                            1 = Reserved
                            2 = Hot Plug Acknowledgment
                            3 = Hot Unplug Acknowledgment

  @retval EFI_SUCCESS           - Notification packet sent success
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval EFI_OUT_OF_RESOURCES  - No Tx buffer to send the control packet.
**/
EFI_STATUS
SendNotification (
  IN PUSB4_HI_CORE              HiCore,
  IN PTOPOLOGY_ID               TopologyId,
  IN UINT8                      AdpNum,
  IN USB4_NOTIFICATION_EVENT    Event,
  IN USB4_NOTIFICATION_PG       Plug
  );

/**
  Free Rx buffer descriptor for later use

  @param[in] HiCore    - Pointer to the USB4 Host Interface Core instance.
  @param[in] RxDesc    - Pointer to Rx descriptor ring.
  @param[in] ConsIndex - Index of Rx buffer descriptor that should be released.
**/
VOID
FreeRxDesc (
  IN PUSB4_HI_CORE    HiCore,
  IN USB4_RING_DESC   *RxDesc,
  IN UINT32           ConsIndex
  );
#endif
