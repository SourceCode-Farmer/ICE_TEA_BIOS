/** @file
  Implementation of USB4 control packets transmit and receive.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Usb4HiCoreDefs.h>
#include <Usb4HiRing.h>
#include <Library/Usb4HrSrvLib.h>
#include <Library/CmUtilsLib.h>
#include "HiCoreEvt.h"
#include "Crc32c.h"

/**
  Dump Ring Debug Information

  @param[in] RingDbgInfo - Pointer to Ring debug information.
  @param[in] DbgIndex    - The entry index of next debug information
  @param[in] Entries     - Debug information entry count.
**/
VOID
DumpRingDbgInfo (
  IN PRING_DBG_INFO     RingDbgInfo,
  IN UINT8              DbgIndex,
  IN UINT8              Entries
  )
{
  UINT8             Index;
  UINT8             Count;
  PRING_DBG_INFO    Info;

  if ((RingDbgInfo == NULL) || (DbgIndex >= Entries)) {
    DEBUG ((DEBUG_ERROR, "DumpRingDbgInfo: Invalid parameter\n"));
    return;
  }

  DEBUG ((DEBUG_ERROR, "Ring History:\n"));
  Index = (DbgIndex + 1) % Entries;
  for (Count = 0; Count < Entries; Count++) {
    Info = &(RingDbgInfo[Index]);
    DEBUG ((DEBUG_ERROR, "    [%d] Pdf = %d, prod = %d, cons = %d\n", Index, Info->Pdf, Info->Prod, Info->Cons));
    Index = (Index + 1) % Entries;
  }
}

/**
  Free Rx buffer descriptor for later use

  @param[in] HiCore    - Pointer to the USB4 Host Interface Core instance.
  @param[in] RxDesc    - Pointer to Rx descriptor ring.
  @param[in] ConsIndex - Index of Rx buffer descriptor that should be released.
**/
VOID
FreeRxDesc (
  IN PUSB4_HI_CORE    HiCore,
  IN USB4_RING_DESC   *RxDesc,
  IN UINT32           ConsIndex
  )
{
  if ((HiCore == NULL) || (RxDesc == NULL) || (ConsIndex >= USB4_RX_RING_BUFS)) {
    DEBUG ((DEBUG_ERROR, "FreeRxDesc: Invalid parameter\n"));
  } else {
    //
    // Clear Rx buffer descriptor attributes and update the consumer index register
    //
    DEBUG ((CM_VERBOSE, "      Free Rx Desc %d and update Rx Cons register to %d\n", ConsIndex, ConsIndex));
    RxDesc[ConsIndex].Attribs = DESC_ATTR_REQ_STS;
    HrSrvMmioWrite (HiCore->HrContext, REG_RX_RING_BASE + REG_RING_CONS_PROD_OFFSET, 1, &ConsIndex);
  }
}

/**
  Get a free TX data buffer, which is pointed by the producer index.

  @param[in]  HiCore      - Pointer to the USB4 host interface core instance.
  @param[out] ProdIndex   - Current producer index of TX ring.
  @param[out] TxBuf       - Pointer of pointer to Tx buffer which is pointed by the producer index

  @retval EFI_SUCCESS      - Get Tx buffer successfully.
  @retval EFI_TIMEOUT      - Timeout failure for waiting free Tx buffer.
  @retval EFI_UNSUPPORTED  - Fail to get free Tx buffer.
  @retval EFI_DEVICE_ERROR - Invalid register value read from host interface.
**/
EFI_STATUS
GetTxBuf (
  IN  PUSB4_HI_CORE    HiCore,
  OUT UINT16           *ProdIndex,
  OUT VOID             **TxBuf
  )
{
  EFI_STATUS    Status;
  UINT32        ProdConsReg;
  UINT32        Prod;
  UINT32        Cons;
  UINT8         DbgIndex;
  UINT8         Retry;

  if ((HiCore == NULL) || (ProdIndex == NULL) || (TxBuf == NULL)) {
    DEBUG ((DEBUG_ERROR, "GetTxBuf: Invalid parameter\n"));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  if (HiCore->Ring0.RingBuf == NULL) {
    DEBUG ((DEBUG_ERROR, "GetTxBuf: Null DMA buffer\n"));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  for (Retry = 0; Retry < USB4_TX_RING_TOTAL_WAITS; Retry++)
  {
    //
    // Read Tx Ring Producer/Consumder index to find the next available Tx buffer
    //
    Status = HrSrvMmioRead (HiCore->HrContext, REG_TX_RING_BASE + REG_RING_CONS_PROD_OFFSET, 1, &ProdConsReg);
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "GetTxBuf: MMIO read failure, %r\n", Status));
      goto Exit;
    }
    Prod = (ProdConsReg & REG_RING_PROD_MASK) >> REG_RING_PROD_SHIFT;
    Cons = (ProdConsReg & REG_RING_CONS_MASK) >> REG_RING_CONS_SHIFT;
    if (Prod >= USB4_TX_RING_BUFS) {
      DEBUG ((DEBUG_ERROR, "GetTxBuf: Prod %u out of range\n", Prod));
      Status = EFI_DEVICE_ERROR;
      goto Exit;
    }

    if (!USB4_TX_RING_FULL (Prod, Cons, USB4_TX_RING_BUFS)) {
      if (Retry != 0) {
        DEBUG ((DEBUG_INFO, "GetTxBuf: Tx Retry = %d\n", Retry));
      }
      break;
    } else {
      if (Retry == 0) {
        DEBUG ((DEBUG_ERROR, "GetTxBuf: TX ring is full (Prod = %d, Cons = %d), wait for available Tx Buffer....\n", Prod, Cons));
        //
        // Update Tx Ring dbginfo before dump
        //
        DbgIndex = HiCore->TxDbgIndex;
        HiCore->TxDbgInfo[DbgIndex].Prod = (UINT16)Prod;
        HiCore->TxDbgInfo[DbgIndex].Cons = (UINT16)Cons;
        DumpRingDbgInfo (&(HiCore->TxDbgInfo[0]), HiCore->TxDbgIndex, TX_DBG_ENTRIES);
      }
    }
    //
    // Wait for available Tx buffer
    //
    gBS->Stall (USB4_TX_RING_POLL_US);
  }

  if (Retry == USB4_TX_RING_TOTAL_WAITS) {
    DEBUG ((DEBUG_ERROR, "GetTxBuf: TX ring is still full after wait loop timeout ....\n"));
    Status = EFI_TIMEOUT;
    goto Exit;
  }

  //
  // Update Tx Ring dbginfo entry
  //
  DbgIndex = HiCore->TxDbgIndex;
  HiCore->TxDbgInfo[DbgIndex].Prod = (UINT16)Prod;
  HiCore->TxDbgInfo[DbgIndex].Cons = (UINT16)Cons;

  *ProdIndex = (UINT16)Prod;
  *TxBuf = (VOID *)(HiCore->Ring0.RingBuf->TxBuf[Prod]);

Exit:
  return Status;
}

/**
  Initiate the frame transfer operation of Tx Ring
  Operation:
    Increment producer index, which will trigger the DMA to fetch first the
    descriptor and then the data.

  @param[in] HiCore   - Pointer to the USB4 Host Interface Core instance.
  @param[in] Pdf       - PDF value that should be put in EOF_PDF field of packet.
  @param[in] MsgLen    - packet length.
  @param[in] ProdIndex - Current producer index.

  @retval EFI_SUCCESS           - Initiate Tx frame transfer successfully.
  @retval EFI_UNSUPPORTED       - The operation can't be supported.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
InitiateTx (
  IN PUSB4_HI_CORE    HiCore,
  IN USB4_CTRL_PDF    Pdf,
  IN UINT16           MsgLen,
  IN UINT16           ProdIndex
  )
{
  EFI_STATUS        Status;
  UINT32            ProdCons;
  UINT32            NewAttributes;

  if ((HiCore == NULL) || (ProdIndex >= USB4_TX_RING_BUFS)) {
    DEBUG ((DEBUG_ERROR, "InitiateTx: Invalid parameter (%p, %d)\n", HiCore, ProdIndex));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  if (HiCore->Ring0.RingBuf == NULL) {
    DEBUG ((DEBUG_ERROR, "InitiateTx: Null DMA buffer\n"));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  DEBUG ((TX_VERBOSE, "      InitiateTx: pdf=0x%x, length=0x%x, prod=0x%x\n", Pdf, MsgLen, ProdIndex));

  //
  // Update Tx descriptor attributes
  //
  NewAttributes = (MsgLen << DESC_ATTR_LEN_SHIFT) | (Pdf << DESC_ATTR_EOF_SHIFT) | DESC_ATTR_REQ_STS;
  HiCore->Ring0.RingBuf->TxBufDesc[ProdIndex].Attribs = NewAttributes;

  //
  // Increment Producer index to initiate Tx operation
  //
  ProdCons = ((ProdIndex + 1) % USB4_TX_RING_BUFS) << REG_RING_PROD_SHIFT;
  HrSrvMmioWrite (HiCore->HrContext, REG_TX_RING_BASE + REG_RING_CONS_PROD_OFFSET, 1, &ProdCons);

  //
  // Increment Tx dbginfo entry index
  //
  HiCore->TxDbgInfo[HiCore->TxDbgIndex].Pdf = (UINT8)Pdf;
  HiCore->TxDbgIndex = (HiCore->TxDbgIndex + 1) % TX_DBG_ENTRIES;
  DEBUG ((TX_VERBOSE, "      InitiateTx: Update Producer register to 0x%x\n", ProdIndex + 1));

  Status = EFI_SUCCESS;

Exit:
  return Status;
}

/**
  Send the USB4 control packet to the host router Tx ring.

  @param[in] HiCore    - Pointer to the USB4 Host Interface Core instance.
  @param[in] PktBuf    - Pointer to packet buffer.
  @param[in] Pdf       - PDF value.
  @param[in] PktLen    - Packet length in bytes.

  @retval EFI_SUCCESS           - Send control packet successfully.
  @retval EFI_OUT_OF_RESOURCES  - No Tx buffer to send the control packet.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CtrlPktSend (
  IN PUSB4_HI_CORE    HiCore,
  IN UINT8            *PktBuf,
  IN USB4_CTRL_PDF    Pdf,
  IN UINT16           PktLen
  )
{
  EFI_STATUS    Status;
  UINT16        Prod;
  VOID          *TxBuf;

  if ((HiCore == NULL) || (PktBuf == NULL) || (PktLen > USB4_FRAME_SIZE_MAX)) {
    DEBUG ((DEBUG_ERROR, "Usb4CtrlPktSend: Invalid parameter (%p, %p, %d)\n", HiCore, PktBuf, PktLen));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  //
  // Get the next Tx buffer for data copy
  //
  Status = GetTxBuf (HiCore, &Prod, &TxBuf);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Usb4CtrlPktSend: Get Tx buffer failure, %r\n", Status));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  CmCopyMem (TxBuf, PktBuf, PktLen);

  //
  // Initiate Tx transfer
  //
  Status = InitiateTx (HiCore, Pdf, PktLen, Prod);

Exit:
  return Status;
}

/**
  Send Notification Packet to ack the events from USB4 Routers.

  @param[in] HiCore       - Pointer to the USB4 Host Interface Core instance.
  @param[in] TopologyId   - Pointer to the Topology ID of the target router.
  @param[in] AdpNum       - Adapter number.
  @param[in] Event        - Notification event code
  @param[in] Plug         - Differentiate Hot Plug and Hot Unplug Events in a Hot Plug Acknowledgment Packet
                            0 = Not a Hot Plug Acknowledgment Packet
                            1 = Reserved
                            2 = Hot Plug Acknowledgment
                            3 = Hot Unplug Acknowledgment

  @retval EFI_SUCCESS           - Notification packet sent success
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval EFI_OUT_OF_RESOURCES  - No Tx buffer to send the control packet.
**/
EFI_STATUS
SendNotification (
  IN USB4_HI_CORE               *HiCore,
  IN TOPOLOGY_ID                *TopologyId,
  IN UINT8                      AdpNum,
  IN USB4_NOTIFICATION_EVENT    Event,
  IN USB4_NOTIFICATION_PG       Plug
  )
{
  EFI_STATUS                   Status;
  UINT32                       Len;
  UINT32                       Crc32c;
  USB4_NOTIFICATION_PACKET     *CtrlNotification;

  if ((HiCore == NULL) || (TopologyId == NULL)) {
    DEBUG ((DEBUG_ERROR, "SendNotification: Invalid Parameter!\n"));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  DEBUG ((HOTPLUG_VERBOSE, "SendNotification: Topology ID = 0x%016llX, AdpNum = %d, Event = %d, Plug = %d\n",
          TopologyId->Value,
          AdpNum,
          Event,
          Plug
          ));

  CtrlNotification = (USB4_NOTIFICATION_PACKET *) &HiCore->CtrlNotification;
  CtrlNotification->RouteString.Str.High = TopologyId->Id.High;
  CtrlNotification->RouteString.Str.Low  = TopologyId->Id.Low;
  CtrlNotification->AdpNum               = AdpNum;
  CtrlNotification->EventCode            = Event;
  CtrlNotification->Reserved             = 0;
  CtrlNotification->Plug                 = Plug;

  //
  // Swap byte endianess of each DWORD of the notification packet excluding CRC before transmit
  //
  Len = sizeof (USB4_NOTIFICATION_PACKET) - 4;
  CmSwapEndianness ((UINT32 *) CtrlNotification, Len / 4, (UINT32 *) CtrlNotification);
  CalCrc32c ((UINT8 *) CtrlNotification, Len, &Crc32c);
  CtrlNotification->Crc = SwapBytes32 (Crc32c);

  // Transmit the notification packet
  Status = Usb4CtrlPktSend (HiCore, (UINT8 *)CtrlNotification, PDF_NOTIFICATION_PACKET, sizeof(USB4_NOTIFICATION_PACKET));
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Notification sent failure, %r - RouteString = 0x%016llX, AdpNum = %d, Event = %d, Plug = %d\n",
            Status,
            TopologyId->Value,
            AdpNum,
            Event,
            Plug
            ));
    goto Exit;
  }

Exit:
  return Status;
}

/**
  Waits for a response with given pdf.

  @param[in]  HiCore    - Pointer to the USB4 Host Interface Core instance.
  @param[in]  Pdf       - PDF of a frame to wait for.
  @param[out] ConsIndex - Consumer index to return.
  @param[out] RxBuf     - Pointer of pointer to Rx buffer containing the received packet data
  @param[out] Length    - Length of the received frame.

  @retval EFI_SUCCESS      - Receive response with give PDF successfully.
  @retval EFI_DEVICE_ERROR - Error event is received from Rx ring.
  @retval EFI_NOT_FOUND    - ERR_ADDR notification event is received from Rx ring.
  @retval EFI_TIMEOUT      - The expected PDF packet is not received after timeout period.
  @retval EFI_NOT_READY    - The adapter is disconnected or disabled.
**/
EFI_STATUS
WaitForResponse (
  IN PUSB4_HI_CORE    HiCore,
  IN USB4_CTRL_PDF    Pdf,
  OUT UINT32          *ConsIndex,
  OUT VOID            **RxBuf,
  OUT UINT16          *Length
  )
{
  EFI_STATUS                     Status;
  USB4_HI_RING_BUF               *Ring;
  USB4_RING_DESC                 *RxDesc;
  UINT32                         ProdConsReg;
  UINT32                         Cons;
  UINT32                         Prod;
  UINT8                          ReceivedPdf;
  UINT32                         Index;
  UINT32                         TotalRxPkts;
  UINT32                         *PktDws;
  TOPOLOGY_ID                    TopologyId;
  PUSB4_NOTIFICATION_PACKET      NotificationPkt;
  PUSB4_HOT_PLUG_EVENT_PACKET    HotPlugPkt;

  DEBUG ((CM_VERBOSE, "      WaitForResponse entry - Expected pdf = 0x%x\n", Pdf));

  if ((HiCore == NULL) || (ConsIndex == NULL) || (RxBuf == NULL) || (Length == NULL)) {
    DEBUG ((DEBUG_ERROR, "WaitForResponse: Invalid parameter\n"));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  Ring = HiCore->Ring0.RingBuf;
  if (Ring == NULL) {
    DEBUG ((DEBUG_ERROR, "WaitForResponse: Null DMA buffer\n"));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  Status = HrSrvMmioRead (HiCore->HrContext, REG_RX_RING_BASE + REG_RING_CONS_PROD_OFFSET, 1, &ProdConsReg);
  if (EFI_ERROR(Status)) {
    DEBUG ((DEBUG_ERROR, "WaitForResponse: MMIO read failure, %r\n", Status));
    goto Exit;
  }

  Cons = (ProdConsReg & REG_RING_CONS_MASK) >> REG_RING_CONS_SHIFT;
  Prod = (ProdConsReg & REG_RING_PROD_MASK) >> REG_RING_PROD_SHIFT;
  TotalRxPkts = 0;
  DEBUG ((CM_VERBOSE, "      WaitForResponse: Cons = %d, Prod = %d\n", Cons, Prod));

  //
  // Wait for DD(Descriptor Done) bit of Rx buffer descriptor is set
  // Release the current Rx buffer descriptor and process the next one if the received PDF is not excepted
  // Rx ring empty condition is not checked since DD bit of next Rx buffer won't be set if Rx ring is empty
  //
  do {
    Cons = (Cons + 1) % USB4_RX_RING_BUFS;
    RxDesc = &(Ring->RxBufDesc[Cons]);
    DEBUG ((CM_VERBOSE, "      Wait RxDesc[%d], attributes - 0x%x\n", Cons, RxDesc->Attribs));

    //
    // Wait for DD(Descriptor Done) bit of Rx buffer descriptor is set
    // Rx ring empty condition is not checked since DD bit of next Rx buffer won't be set if Rx ring is empty
    //
    for (Index = 0; Index < USB4_RX_RING_TOTAL_WAITS; Index++) {
      if (RxDesc->Attribs & DESC_ATTR_DESC_DONE) {
        break;
      }
      gBS->Stall (USB4_RX_RING_POLL_US);
    }

    if (Index >= USB4_RX_RING_TOTAL_WAITS) {
      DEBUG ((DEBUG_ERROR, "WaitForResponse: Response packet timeout!\n"));
      DEBUG ((DEBUG_ERROR, "The descriptor address - 0x%x, attributes - 0x%x\n", RxDesc, RxDesc->Attribs));
      Status = EFI_TIMEOUT;
      goto Exit;
    }

    ReceivedPdf = (UINT8)((RxDesc->Attribs & DESC_ATTR_EOF_MASK) >> DESC_ATTR_EOF_SHIFT);
    //
    // Process hot plug or error notification event if the received PDF is not expected
    //
    if (ReceivedPdf != Pdf) {
      PktDws = (UINT32 *)(Ring->RxBuf[Cons]);
      CmSwapEndianness (PktDws, (RxDesc->Attribs & DESC_ATTR_LEN_MASK)/4, PktDws);

      if (ReceivedPdf == PDF_NOTIFICATION_PACKET) {
        //
        // Error notification event is received from Rx ring.
        //
        NotificationPkt = (PUSB4_NOTIFICATION_PACKET)PktDws;
        DEBUG ((DEBUG_ERROR, "USB4 Notification Packet: RouteString = 0x%08X%08X, AdpNum = %d, event = %d\n",
                NotificationPkt->RouteString.Str.High,
                NotificationPkt->RouteString.Str.Low,
                NotificationPkt->AdpNum,
                NotificationPkt->EventCode));
        if (NotificationPkt->EventCode == USB4_ERR_CONN) {
          Status = EFI_NOT_READY;
        } else if (NotificationPkt->EventCode == USB4_ERR_ADDR) {
          Status = EFI_NOT_FOUND;
        } else {
          Status = EFI_DEVICE_ERROR;
        }
        FreeRxDesc (HiCore, Ring->RxBufDesc, Cons);
        goto Exit;
      } else if (ReceivedPdf == PDF_HOT_PLUG) {
        //
        // Hot plug event is received from Rx ring.
        //
        HotPlugPkt = (PUSB4_HOT_PLUG_EVENT_PACKET)PktDws;
        DEBUG ((HOTPLUG_VERBOSE, "Hot Plug Packet: RouteString = 0x%08X%08X, AdpNum = %d, Unplug = %d\n",
                HotPlugPkt->RouteString.Str.High,
                HotPlugPkt->RouteString.Str.Low,
                HotPlugPkt->AdpNum,
                HotPlugPkt->Unplug
                ));
        //
        // Report hot plug event to the internal Rx event queue for processing later.
        // Send the ack notification to Router if the hot plug event is reported successfully.
        // Router re-sends hot plug event if it doesn't receive ACK from CM within notification timeout period.
        //
        Status = ReportHotplugEvent (HiCore, HotPlugPkt);
        if (Status == EFI_SUCCESS) {
          TopologyId.Id.High = HotPlugPkt->RouteString.Str.High & ROUTE_STRING_HIGH_MASK;
          TopologyId.Id.Low  = HotPlugPkt->RouteString.Str.Low & ROUTE_STRING_LOW_MASK;
          Status = SendNotification (
                    HiCore,
                    &TopologyId,
                    (UINT8)HotPlugPkt->AdpNum,
                    USB4_HOTPLUG_ACK,
                    (HotPlugPkt->Unplug) ? PG_HOT_UNPLUG_EVENT : PG_HOT_PLUG_EVENT);
          if (EFI_ERROR(Status)) {
            DEBUG ((DEBUG_ERROR, "Send Hotplug Ack failure, %r\n", Status));
            //
            // Keep processing Rx events
            //
            Status = EFI_SUCCESS;
          }
        } else {
          DEBUG ((DEBUG_ERROR, "Report Hot Plug event to the internal Rx queue failure, %r\n", Status));
          //
          // Keep processing Rx events
          //
          Status = EFI_SUCCESS;
        }
      } else {
        DEBUG ((DEBUG_ERROR, "Packet with unexpected PDF %d is received!\n", ReceivedPdf));
      }

      FreeRxDesc (HiCore, Ring->RxBufDesc, Cons);
    }
    TotalRxPkts++;
  } while ((ReceivedPdf != Pdf) && (TotalRxPkts < MAX_PACKETS_TO_RX_WHILE_WAIT_FOR_RESP));

  if (TotalRxPkts >= MAX_PACKETS_TO_RX_WHILE_WAIT_FOR_RESP) {
    DEBUG ((DEBUG_ERROR, "Timeout waiting for the response with given Pdf 0x%x. Exiting...\n", Pdf));
    Status = EFI_TIMEOUT;
    goto Exit;
  }

  *ConsIndex = Cons;
  *RxBuf     = (VOID *)(Ring->RxBuf[Cons]);
  *Length    = (UINT16)(RxDesc->Attribs & DESC_ATTR_LEN_MASK);

Exit:
  DEBUG ((CM_VERBOSE, "      WaitForResponse exit\n"));
  return Status;
}

/**
  Receive the USB4 control response packet from the host router Rx ring.

  @param[in]  HiCore         - Pointer to the USB4 Host Interface Core instance.
  @param[out] PktBuf         - Pointer to packet buffer.
  @param[in]  Pdf            - PDF value.
  @param[in]  ExpectedPktLen - Expected response packet length in bytes.

  @retval EFI_SUCCESS           - Send control packet successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval EFI_DEVICE_ERROR      - Error notification is received or the received data length is not expected.
  @retval EFI_NOT_FOUND         - ERR_ADDR notification event is received from Rx ring.
  @retval EFI_TIMEOUT           - The expected PDF packet is not received after timeout period.
**/
EFI_STATUS
Usb4CtrlPktRcv (
  IN PUSB4_HI_CORE    HiCore,
  IN UINT8            *PktBuf,
  IN USB4_CTRL_PDF    Pdf,
  IN UINT16           ExpectedPktLen
  )
{
  EFI_STATUS    Status;
  UINT32        Cons;
  UINT16        RcvdLen;
  VOID          *RxBuf;

  if ((HiCore == NULL) || (PktBuf == NULL)) {
    DEBUG ((DEBUG_ERROR, "Usb4CtrlPktRcv: Invalid parameter (%p, %p)\n", HiCore, PktBuf));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  if (HiCore->Ring0.RingBuf == NULL) {
    DEBUG ((DEBUG_ERROR, "Usb4CtrlPktRcv: Null DMA buffer\n"));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }
  //
  // Wait for response packet with same PDF value received
  // Consume Rx buffer until the received packet with same PDF is found
  //
  Status = WaitForResponse (HiCore, Pdf, &Cons, &RxBuf, &RcvdLen);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Usb4CtrlPktRcv: Wait for response packet failure, %r\n", Status));
    goto Exit;
  }

  //
  // Report failure if the received data length is not expected.
  //
  if (RcvdLen != ExpectedPktLen) {
    DEBUG ((DEBUG_ERROR, "Usb4CtrlPktRcv: RcvdPktLen mismatches, exp = 0x%x, rcvd = 0x%x\n", ExpectedPktLen, RcvdLen));
    //
    // Release the returned Rx buffer
    //
    FreeRxDesc (HiCore, HiCore->Ring0.RingBuf->RxBufDesc, Cons);
    Status = EFI_DEVICE_ERROR;
    goto Exit;
  }

  CmCopyMem (PktBuf, RxBuf, RcvdLen);

  //
  // Release the returned Rx buffer
  //
  FreeRxDesc (HiCore, HiCore->Ring0.RingBuf->RxBufDesc, Cons);

Exit:
  return Status;
}