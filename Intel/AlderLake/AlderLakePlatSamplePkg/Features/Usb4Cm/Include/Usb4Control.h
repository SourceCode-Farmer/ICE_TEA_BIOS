/** @file
  Header file for USB4 control fields

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_CONTROL_H_
#define _USB4_CONTROL_H_

#include <Uefi.h>

//
// USB4 control fields
//
// bit 0 - USB Tunneling
// bit 1 - DisplayPort Tunneling
// bit 2 - PCIe Tunneling
// bit 3 - Inter-Domain USB4 Internet Protocol
// bit 31:4 - Reserved
//
#define USB4_CONTROL_USB3            0x00000001
#define USB4_CONTROL_DP              0x00000002
#define USB4_CONTROL_PCIE            0x00000004
#define USB4_CONTROL_INTER_DOMAIN    0x00000008

#define DEFAULT_USB4_CONTROLS        0x0000000F

typedef union _USB4_CONTROL {
  struct {
    UINT32 Usb3        : 1;
    UINT32 Dp          : 1;
    UINT32 Pcie        : 1;
    UINT32 InterDomain : 1;
  } Fields;
  UINT32 Value;
} USB4_CONTROL, *PUSB4_CONTROL;

#endif
