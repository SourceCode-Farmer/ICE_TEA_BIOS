/** @file
  Header file for USB4 host interface core implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_HI_CORE_DEFS_H_
#define _USB4_HI_CORE_DEFS_H_

#include <Uefi.h>
#include <Usb4Common.h>
#include <Usb4ConfigLayer.h>
#include <Usb4ConfigSpace.h>
#include <Usb4CsIo.h>

#pragma pack(push, 1)

//
// Tx/Rx ring descriptor
//
typedef struct {
  UINT64    PhysAddr;
  UINT32    Attribs;
  UINT32    Reserved;
} USB4_RING_DESC;

#pragma pack(pop)

//
// Tx/Rx ring buffer size definition
//
#define USB4_RING_BUF_SIZE    EFI_PAGE_SIZE
#define USB4_FRAME_SIZE_MAX   256
#define USB4_BUF_ENTRY_SIZE   (sizeof (USB4_RING_DESC) + USB4_FRAME_SIZE_MAX)
#define USB4_TX_RING_BUFS     4
#define USB4_RX_RING_BUFS     ((USB4_RING_BUF_SIZE - (USB4_TX_RING_BUFS * USB4_BUF_ENTRY_SIZE)) / USB4_BUF_ENTRY_SIZE)

#define TX_DBG_ENTRIES    (USB4_TX_RING_BUFS * 2)
#define RX_DBG_ENTRIES    (USB4_RX_RING_BUFS * 2)

//
// Tx/Rx ring definition for USB4 host interface implementation
//
typedef struct {
  // Tx ring buffers
  UINT8             TxBuf[USB4_TX_RING_BUFS][USB4_FRAME_SIZE_MAX];
  // Rx ring buffers
  UINT8             RxBuf[USB4_RX_RING_BUFS][USB4_FRAME_SIZE_MAX];
  // Tx ring descriptors
  USB4_RING_DESC    TxBufDesc[USB4_TX_RING_BUFS];
  // Rx ring descriptors
  USB4_RING_DESC    RxBufDesc[USB4_RX_RING_BUFS];
} USB4_HI_RING_BUF, *PUSB4_HI_RING_BUF;

//
// Tx/Rx ring debug information entry
//
typedef struct _RING_DBG_INFO {
  UINT8   Pdf;
  UINT16  Prod;
  UINT16  Cons;
} RING_DBG_INFO, *PRING_DBG_INFO;

//
// DMA buffer information of ring
//
typedef struct _USB4_HI_RING {
  USB4_HI_RING_BUF    *RingBuf;
  UINT32              BufPages;
  PHYSICAL_ADDRESS    MappedAddr;
  VOID *              Mapping;
} USB4_HI_RING, *PUSB4_HI_RING;

#define RX_EVT_MAX    64

//
// Event received from Rx ring
//
typedef struct _RX_EVENT {
  LIST_ENTRY      Entry;
  TOPOLOGY_ID     TopologyId;
  UINT8           AdpNum;
  UINT8           Pdf;
  UINT8           EventCode;
  UINT8           Unplug;
} RX_EVENT, *PRX_EVENT;

#define GET_RXEVT(Addr, Field)  ((PRX_EVENT)((UINT8 *)Addr - (UINTN)(&((PRX_EVENT)0)->Field)))

#define USB4_HI_CORE_SIGNATURE          SIGNATURE_32 ('u', '4', 'h', 'i')
#define USB4_HI_CORE_FROM_CSIO(a)       CR (a, USB4_HI_CORE, Usb4CsIo, USB4_HI_CORE_SIGNATURE)

#define HI_CORE_RING0_INIT    0x00000001
#define HI_CORE_START         0x00000002

//
// Instance for USB4 host interface implementation
//
typedef struct _USB4_HI_CORE{
  UINT32                         Signature;
  // Function table for USB4 config space access
  USB4_CS_IO                     Usb4CsIo;
  // Reference count to USB4_CS_IO function table
  UINT32                         Usb4CsIoRef;
  // Host router context of this host interface instance
  USB4_HR_CONTEXT                HrContext;
  // USB4 Ring0 DMA Buffer
  USB4_HI_RING                   Ring0;
  // Rx event poll
  RX_EVENT                       RxEvts[RX_EVT_MAX];
  // Rx Ring Event queue
  LIST_ENTRY                     RxEvtQueue;
  // Free Event queue
  LIST_ENTRY                     FreeEvtQueue;
  // Packet buffer for Control request / response
  USB4_CONTROL_READ_REQUEST      CtrlReadReq;
  USB4_CONTROL_READ_RESPONSE     CtrlReadRsp;
  USB4_CONTROL_WRITE_REQUEST     CtrlWriteReq;
  USB4_CONTROL_WRITE_RESPONSE    CtrlWriteRsp;
  USB4_NOTIFICATION_PACKET       CtrlNotification;
  // Tx ring debug information - consumer/producer index history
  RING_DBG_INFO                  TxDbgInfo[TX_DBG_ENTRIES];
  UINT8                          TxDbgIndex;
  // Rx Ring debug information - consumer/producer index history
  RING_DBG_INFO                  RxDbgInfo[RX_DBG_ENTRIES];
  UINT8                          RxDbgIndex;
  // Host interface state - init/start
  UINT32                         State;
} USB4_HI_CORE, *PUSB4_HI_CORE;

#endif
