/** @file
  Header file for USB4 platform information.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_PLATFORM_INFO_H_
#define _USB4_PLATFORM_INFO_H_

#include <Uefi.h>
#include <Usb4Common.h>
#include <Usb4Control.h>
#include <Usb4HrPci.h>

#define HR_MASK_IUSB4_BITS     0x0F
#define HR_MASK_DUSB4_BITS     0xF0

#define HR_MASK_IUSB4_0        0x01
#define HR_MASK_IUSB4_1        0x02
#define HR_MASK_IUSB4_2        0x04
#define HR_MASK_IUSB4_3        0x08
#define HR_MASK_DUSB4_0        0x10
#define HR_MASK_DUSB4_1        0x20
#define HR_MASK_DUSB4_2        0x40
#define HR_MASK_DUSB4_3        0x80

//
// USB4 Host Router Information
//
typedef struct _USB4_HR_INFO {
  BOOLEAN               IntegratedHr;
  // USB4 control supported : USB3/DP/PCIe/InterDomain
  USB4_CONTROL          Usb4Control;
  // Mailbox status register offset
  UINT32                MbStatusReg;
  // Mailbox command register offset
  UINT32                MbCmdReg;
  // BDF for Mailbox command
  PCI_DEV_BDF           MailboxBdf;
  // BDF for USB4 Host Interface
  PCI_DEV_BDF           NhiBdf;
  // Root port of USB4 host router
  PCI_BRIDGE_INFO       Rp;
  // Upstream port of USB4 host router
  PCI_BRIDGE_INFO       UsPort;
  // Downstream port of USB4 host router
  PCI_BRIDGE_INFO       DsPort;
  // PEI reserved DMA buffer address for USB4 host router
  PHYSICAL_ADDRESS      PeiDmaBufBase;
  // PEI reserved DMA buffer size for USB4 host router
  UINT32                PeiDmaBufSize;
} USB4_HR_INFO, *PUSB4_HR_INFO;

//
// Overall USB4 Host Router information in the platform.
//
typedef struct _USB4_PLATFORM_INFO {
  UINT8           Usb4HrCount;
  UINT8           Usb4HrMask;
  UINT8           CmMode;
  UINT8           CmModeOption;
  BOOLEAN         CmSwitchSupport;
  BOOLEAN         PcieOverUsb4En;
  USB4_HR_INFO    Usb4Hr[USB4_HR_SUPPORT_MAX];
} USB4_PLATFORM_INFO, *PUSB4_PLATFORM_INFO;

#endif
