/** @file
  Header file for Tx/Rx ring configuration of USB4 host interface.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_HI_RING_H_
#define _USB4_HI_RING_H_

#define USB4_TX_RING_FULL(prod, cons, size)  ((((prod) + 1) % (size)) == (cons))
#define USB4_TX_RING_EMPTY(prod, cons)       ((prod) == (cons))
#define USB4_RX_RING_FULL(prod, cons)        ((prod) == (cons))
#define USB4_RX_RING_EMPTY(prod, cons, size) ((((cons) + 1) % (size)) == (prod))

#define USB4_TX_RING_POLL_US                     10
#define USB4_TX_RING_TOTAL_WAITS                 200
#define USB4_RX_RING_POLL_US                     50         // 50 us
#define USB4_RX_RING_TOTAL_WAIT_US               10000000   // 10s
#define USB4_RX_RING_TOTAL_WAITS                 (USB4_RX_RING_TOTAL_WAIT_US / USB4_RX_RING_POLL_US)
#define MAX_PACKETS_TO_RX_WHILE_WAIT_FOR_RESP    1000

//
// Register definition of Host Interface Programming interface
//
#define REG_TX_RING_BASE                      0x00000
#define REG_RX_RING_BASE                      0x08000
#define REG_RING_PHYS_LO_OFFSET               0
#define REG_RING_PHYS_HI_OFFSET               4
#define REG_RING_CONS_PROD_OFFSET             8
#define REG_RING_CONS_SHIFT                   0
#define REG_RING_CONS_MASK                    0x0000FFFF     // Bit[15:0]
#define REG_RING_PROD_SHIFT                   16
#define REG_RING_PROD_MASK                    0xFFFF0000     // Bit[31:16]
#define REG_RING_SIZE_OFFSET                  12
#define REG_RING_SIZE_SHIFT                   0
#define REG_RING_SIZE_MASK                    0x0000FFFF     // Bit[15:0]
#define REG_RING_BUF_SIZE_SHIFT               16
#define REG_RING_BUF_SIZE_MASK                0x0FFF0000     // Bit[27:16]

#define REG_TX_OPTIONS_BASE                   0x19800
#define REG_RX_OPTIONS_BASE                   0x29800
#define REG_RX_OPTS_MASK_OFFSET               4
#define REG_RX_OPTS_MASK_EOF_SHIFT            0
#define REG_RX_OPTS_MASK_EOF_MASK             0x0000FFFF     // Bit[15:0]
#define REG_RX_OPTS_MASK_SOF_SHIFT            16
#define REG_RX_OPTS_MASK_SOF_MASK             0xFFFF0000     // Bit[31:16]
#define REG_OPTS_RAW                          BIT30
#define REG_OPTS_VALID                        BIT31

#define DESC_ATTR_LEN_SHIFT                   0
#define DESC_ATTR_LEN_MASK                    0x00000FFF     // Bit[11:0]
#define DESC_ATTR_EOF_SHIFT                   12
#define DESC_ATTR_EOF_MASK                    0x0000F000     // Bit[15:12]
#define DESC_ATTR_SOF_SHIFT                   16
#define DESC_ATTR_SOF_MASK                    0x000F0000     // Bit[19:16]
#define DESC_ATTR_TX_ISOCH_DMA_EN             BIT20  // TX
#define DESC_ATTR_RX_CRC_ERR                  BIT20  // RX after use
#define DESC_ATTR_DESC_DONE                   BIT21
#define DESC_ATTR_REQ_STS                     BIT22  // TX and RX before use
#define DESC_ATTR_RX_BUF_OVRN_ERR             BIT22  // RX after use
#define DESC_ATTR_INT_EN                      BIT23
#define DESC_ATTR_OFFSET_SHIFT                24
#define DESC_ATTR_OFFSET_MASK                 0xFF000000     // Bit[31:24]

#endif
