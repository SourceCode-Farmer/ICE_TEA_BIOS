/** @file
  USB4 protocol tunneling library header.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_PROTOCOLS_LIB_H_
#define _USB4_PROTOCOLS_LIB_H_

#include <Uefi.h>
#include <Usb4RouterDefs.h>

/**
  Enable Protocol Tunneling for the downstream Router.

  @param[in] Router - Pointer to Router instance.
  @param[in] AdpNum - Downstream Lane adapter number.

  @retval EFI_SUCCESS           - Enable Protocol Tunneling successfully.
  @retval EFI_UNSUPPORTED       - Fail to enable protocol tunneling.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4RtEnableDsProtocols (
  PUSB4_ROUTER    Router,
  UINT8           AdpNum
  );

/**
  Remove Protocol Tunneling settings that are confirured for the connection between Router and its downstream Routers.

  @param[in] Router - Pointer to Router instance.
  @param[in] AdpNum - Downstream Adapter number.

  @retval EFI_SUCCESS           - Remove Protocol Tunneling settings success.
  @retval EFI_UNSUPPORTED       - Unexpected protocol tunneling removal failure.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4RtRemoveDsProtocols (
  IN PUSB4_ROUTER    Router,
  IN UINT8           AdpNum
  );

/**
  Execute DP Tunneling Router Operation

  @param[in]  Router    - Pointer to Router instance.
  @param[in]  OpCode    - Router operation OpCode.
  @param[in]  DpNum     - DisplayPort number.
  @param[out] OpStatus  - Pointer to Router operation returned status.

  @retval EFI_SUCCESS           - Router operation execution success.
  @retval EFI_TIMEOUT           - Timeout failure of router operation execution.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4DpRtOpExec (
  IN PUSB4_ROUTER   Router,
  IN UINT32         OpCode,
  IN UINT32         DpNum,
  OUT UINT32        *OpStatus
  );

/**
  Enable DP Tunnel between DP Source and DP Sink.

  @param[in] DpSrcRt      - Pointer to router instance which contains DP-IN adapter connecting DP source.
  @param[in] DpSrcAdpNum  - Adapter number of DP-IN adapter connecting DP source.
  @param[in] DpSinkRt     - Pointer to router instance which contains DP-OUT adapter connecting DP sink.
  @param[in] DpSinkAdpNum - Adapter number of DP-OUT adapter connecting DP sink.
  @param[in] StreamId     - DP stream ID for identifying DP path.

  @retval EFI_SUCCESS           - Enable DP Tunnel success.
  @retval EFI_UNSUPPORTED       - Fail to enable DP Tunnel.
  @retval EFI_OUT_OF_RESOURCES  - Fail to get DP resources to enable DP tunnel.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4EnableDpTunnel (
  IN PUSB4_ROUTER    DpSrcRt,
  IN UINT8           DpSrcAdpNum,
  IN PUSB4_ROUTER    DpSinkRt,
  IN UINT8           DpSinkAdpNum,
  IN UINT32          StreamId
  );

/**
  Disable DP Tunnel between DP Source and DP Sink for DP adapter removal or router removal.
  - Disable DP path of DP-IN adapter and DP-OUT adapter.
  - Tear down DP path between DP-IN and DP-OUT (until upper level router of the removed router if any).
  - De-allocate DP resources

  @param[in] DpSrcRt      - Pointer to router instance which contains DP-IN adapter connecting DP source.
  @param[in] DpSrcAdpNum  - Adapter number of DP-IN adapter connecting DP source.
  @param[in] DpSinkRt     - Pointer to router instance which contains DP-OUT adapter connecting DP sink.
  @param[in] DpSinkAdpNum - Adapter number of DP-OUT adapter connecting DP sink.
  @param[in] StreamId     - DP stream ID for identifying DP path.
  @param[in] RemovedRt    - Removed router on DP path.
                            This is specified if DP sink router is under the removed router tree.
                            USB4 config space access is not available from the removed router.

  @retval EFI_SUCCESS           - Disable DP Tunnel success.
  @retval EFI_UNSUPPORTED       - Fail to disable DP Tunnel.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4DisableDpTunnel (
  IN PUSB4_ROUTER    DpSrcRt,
  IN UINT8           DpSrcAdpNum,
  IN PUSB4_ROUTER    DpSinkRt,
  IN UINT8           DpSinkAdpNum,
  IN UINT32          StreamId,
  IN PUSB4_ROUTER    RemovedRt
  );
#endif
