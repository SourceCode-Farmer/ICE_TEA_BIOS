/** @file
  USB4 configuration space access library header.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_CS_LIB_H_
#define _USB4_CS_LIB_H_

#include <Uefi.h>
#include <Usb4RouterDefs.h>

/**
  Read USB4 router config space data.

  @param[in]  Router  - Pointer to Router instance.
  @param[in]  Offset  - DWORD offset in Router Configuration Space.
  @param[in]  Count   - Data count in DWORD.
  @param[out] Data    - Data buffer for the returned data.

  @retval EFI_SUCCESS           - Read USB4 router config space data successfully.
  @retval EFI_UNSUPPORTED       - Fail to read USB4 router config space data.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsRtRead (
  IN  PUSB4_ROUTER    Router,
  IN  UINT16          Offset,
  IN  UINT8           Count,
  OUT UINT32          *Data
  );

/**
  Write USB4 router config space data.

  @param[in]  Router  - Pointer to Router instance.
  @param[in]  Offset  - DWORD offset in Router Configuration Space.
  @param[in]  Count   - Data count in DWORD.
  @param[out] Data    - Data buffer for the written data.

  @retval EFI_SUCCESS           - Write USB4 router config space data successfully.
  @retval EFI_UNSUPPORTED       - Fail to write USB4 router config space data.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsRtWrite (
  IN  PUSB4_ROUTER    Router,
  IN  UINT16          Offset,
  IN  UINT8           Count,
  OUT UINT32          *Data
  );

/**
  Read USB4 adapter config space data.

  @param[in]  Router  - Pointer to Router instance.
  @param[in]  AdpNum  - Adapter number.
  @param[in]  Offset  - DWORD offset in Adapter Configuration Space.
  @param[in]  Count   - Data count in DWORD.
  @param[out] Data    - Data buffer for the returned data.

  @retval EFI_SUCCESS           - Read USB4 adapter config space data successfully.
  @retval EFI_UNSUPPORTED       - Fail to read USB4 adapter config space data.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsAdpRead (
  IN  PUSB4_ROUTER    Router,
  IN  UINT8           AdpNum,
  IN  UINT16          Offset,
  IN  UINT8           Count,
  OUT UINT32          *Data
  );

/**
  Write USB4 adapter config space data.

  @param[in]  Router  - Pointer to Router instance.
  @param[in]  AdpNum  - Adapter number.
  @param[in]  Offset  - DWORD offset in Adapter Configuration Space.
  @param[in]  Count   - Data count in DWORD.
  @param[out] Data    - Data buffer for the written data.

  @retval EFI_SUCCESS           - Write USB4 adapter config space data successfully.
  @retval EFI_UNSUPPORTED       - Fail to write USB4 adapter config space data.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsAdpWrite (
  IN  PUSB4_ROUTER    Router,
  IN  UINT8           AdpNum,
  IN  UINT16          Offset,
  IN  UINT8           Count,
  OUT UINT32          *Data
  );

/**
  Read Path entry in Path Configuration Space.

  @param[in]  Router    - Pointer to Router instance.
  @param[in]  AdpNum    - Adapter number.
  @param[in]  HopId     - Path HopId.
  @param[out] PathEntry - Data buffer for the returned data.

  @retval EFI_SUCCESS           - Read USB4 path entry successfully.
  @retval EFI_UNSUPPORTED       - Fail to read USB4 path entry.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsPathRead (
  IN  PUSB4_ROUTER           Router,
  IN  UINT8                  AdpNum,
  IN  UINT16                 HopId,
  OUT PUSB4_PATH_CS_ENTRY    PathEntry
  );

/**
  Write Path entry in Path Configuration Space.

  @param[in]  Router    - Pointer to Router instance.
  @param[in]  AdpNum    - Adapter number.
  @param[in]  HopId     - Path HopId.
  @param[out] PathEntry - Path entry data to be written.

  @retval EFI_SUCCESS           - Write USB4 path entry successfully.
  @retval EFI_UNSUPPORTED       - Fail to write USB4 path entry.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsPathWrite (
  IN  PUSB4_ROUTER           Router,
  IN  UINT8                  AdpNum,
  IN  UINT16                 HopId,
  OUT PUSB4_PATH_CS_ENTRY    PathEntry
  );

/**
  Enable/Disable Path entry in Path Configuration Space.

  @param[in] Router   - Pointer to Router instance.
  @param[in] AdpNum   - Adapter number.
  @param[in] HopId    - Path HopId.
  @param[in] Enable   - 1: Enable path, 0: Disable path.

  @retval EFI_SUCCESS           - Enable/Disable Path entry success.
  @retval EFI_UNSUPPORTED       - Fail to enable/disable path entry.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsPathEnable (
  IN PUSB4_ROUTER    Router,
  IN UINT8           AdpNum,
  IN UINT16          HopId,
  IN BOOLEAN         Enable
  );

/**
  Query Upstream Adapter number of Router.
  Used for TBT3 compatible operations.

  @param[in]  Router       - Pointer to Router instance.
  @param[out] UpAdpNum     - Pointer to Upstream Adapter number.

  @retval EFI_SUCCESS           - Query upstream adapter number successfully.
  @retval EFI_UNSUPPORTED       - Fail to query upstream adapter number.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CsRtQueryUpAdapter (
  IN PUSB4_ROUTER    Router,
  OUT UINT8          *UpAdpNum
  );
#endif
