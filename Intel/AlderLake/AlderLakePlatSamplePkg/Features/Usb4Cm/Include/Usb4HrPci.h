/** @file
  Header file for PCI information of USB4 host router.
@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _USB4_HR_PCI_H_
#define _USB4_HR_PCI_H_
#include <Uefi.h>

//
// PCI device bus/device/function
//
typedef struct _PCI_DEV_BDF {
  UINT8  Bus : 8;
  UINT8  Dev : 5;
  UINT8  Func: 3;
} PCI_DEV_BDF, *PPCI_DEV_BDF;

//
// PCI bridge information
//
typedef struct _PCI_BRIDGE_INFO {
  PCI_DEV_BDF    Bdf;
  UINT8          PriBus;
  UINT8          SecBus;
  UINT8          SubBus;
  UINT16         MemBase;
} PCI_BRIDGE_INFO, *PPCI_BRIDGE_INFO;

#endif
