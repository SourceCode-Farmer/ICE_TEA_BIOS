/** @file
  Header file for USB4 host router instance.
@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _USB4_HR_INST_H_
#define _USB4_HR_INST_H_

#include <Protocol/PciIo.h>
#include <Usb4PlatformInfo.h>
#include <Usb4HiCoreDefs.h>
#include <Usb4HcMem.h>
#include <Usb4DomainDefs.h>

#define USB4_HR_INSTANCE_SIGNATURE      SIGNATURE_32 ('u', '4', 'h', 'r')
typedef struct _USB4_HR_INSTANCE {
  UINT32                 Signature;
  USBHC_MEM_POOL         *MemPool;
  // USB4 host router information
  USB4_HR_INFO           HrInfo;
  // PCI I/O protocol should be used for MMIO and DMA buffer allocation if it's present.
  EFI_PCI_IO_PROTOCOL    *PciIo;
  // MMIO base of USB4 host router for Tx/Rx Ring
  PHYSICAL_ADDRESS       MmioBase;
  // MMIO length of USB4 host router for Tx/Rx Ring, not used in post pci
  UINT32                 MmioLen;
  // Indicate whether PEI reserved DMA buffer is in use, not used in post pci
  BOOLEAN                PeiDmaBufInUse;
  // USB4 Host Interface core
  USB4_HI_CORE           HiCore;
  // USB4 Domain
  USB4_DOMAIN            *Usb4Domain;
  BOOLEAN                PciInitDone;
  BOOLEAN                CmStart;
  EFI_EVENT              PollTimer;
} USB4_HR_INSTANCE, *PUSB4_HR_INSTANCE;

#define USB4_HR_FROM_HC_PROTOCOL(a) \
  CR (a, USB4_HR_INSTANCE, Usb4Hr, USB4_HR_INSTANCE_SIGNATURE)

#endif
