/** @file
  Header file for Connection Manager mode information

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_CM_MODE_H_
#define _USB4_CM_MODE_H_

#include <Uefi.h>

//
// CM mode in Setup menu - 3 bits
//
#define USB4_SETUP_CM_MODE_FW       0
#define USB4_SETUP_CM_MODE_SW       1
#define USB4_SETUP_CM_MODE_OS       2
#define USB4_SETUP_CM_MODE_DEBUG    3

//
// CM mode in Pre-boot - 3 bits
//
#define USB4_CM_MODE_FW_CM                 0x00
#define USB4_CM_MODE_SW_CM                 0x01

//
// CM mode switch unsupport bit
//
#define USB4_CM_MODE_SWITCH_UNSUPPORTED    0x08

//
// CM Mode information for the communication with ASL codes
//
// Bit 7   - Valid bit
// Bit 6:4 - Setup CM value (0 = FW CM, 1 = SW CM, 2 = OS, 3 = Pass Through)
// Bit 3   - Reserved
// Bit 2:0 - Pre-boot CM mode (0 = FW CM, 1 = SW CM)
//
// Note: 8 bits value in NVS
//       NVS variable length needs to be incremented if new bits are added.
//
typedef union _USB4_CM_MODE_INFO {
  struct {
    UINT8   CmMode  : 3;
    UINT8   Rsvd    : 1;
    UINT8   CmSetup : 3;
    UINT8   Valid   : 1;
  } Fields;
  UINT8 Value;
} USB4_CM_MODE_INFO, *PUSB4_CM_MODE_INFO;

#endif
