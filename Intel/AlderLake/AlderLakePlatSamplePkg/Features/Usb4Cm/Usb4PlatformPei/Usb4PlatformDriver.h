/** @file
  Header file for USB4 host router initialization routines in PEI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_PLATFORM_DRIVER_H_
#define _USB4_PLATFORM_DRIVER_H_

#include <Usb4PlatformInfo.h>

//
// USB4 PEI reserved DMA buffer for each host router
//
#define MAX_USB4_RING_SUPPORT      16
#define USB4_PEI_DMA_BUFFER_PAGES   (1 * MAX_USB4_RING_SUPPORT)

//
// Firmware CM delay in us for PCIe tunneling (= 600ms)
//
#define USB4_FW_CM_PCIE_DELAY_IN_US                 600000

/**
  Query the USB4 host router information in the platform.

  @param[out] Usb4PlatformInfo - Pointer to USB4 host routers information in the platform.

  @retval EFI_SUCCESS           - Query the integrated USB4 host router information successfully.
  @retval EFI_UNSUPPORTED       - Fail to query the integrated USB4 host router information.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4PlatformInfoQuery (
  OUT USB4_PLATFORM_INFO    *Usb4PlatformInfo
  );

/**
  Initialize all USB4 host routers to the platform preferred CM mode.

  @param[in] Usb4PlatformInfo - Pointer to USB4 host routers information in the platform.

  @retval EFI_SUCCESS           - Initialize all USB4 host routers successfully.
  @retval EFI_UNSUPPORTED       - Unable to initialize all USB4 host routers to the preferred CM mode.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4PlatformInit (
  IN USB4_PLATFORM_INFO    *Usb4PlatformInfo
  );

#endif
