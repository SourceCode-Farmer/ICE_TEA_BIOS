/** @file
  USB4 Connection Manager Platform PEI driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <SetupVariable.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Usb4PlatformHob.h>
#include "Usb4PlatformPei.h"
#include "Usb4PlatformDriver.h"

EFI_PEI_NOTIFY_DESCRIPTOR mEndOfPeiNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) Usb4PlatformEndOfPeiPpiNotifyCallback
};


/**
  The callback function for EndOfPeiPpi.
  - Query USB4 platform information
  - Initialize USB4 host router to the platform preferred CM mode.
  - Allocate IOMMU DMA buffer for use in Dxe
  - Create USB4 platform hob to pass USB4 platform information to Dxe

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval EFI_SUCCESS          - The function completed successfully.
  @retval EFI_UNSUPPORTED      - The function failed.
**/
EFI_STATUS
EFIAPI
Usb4PlatformEndOfPeiPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS            Status;
  USB4_PLATFORM_HOB     *Usb4PlatformHob;
  USB4_PLATFORM_INFO    *Usb4PlatformInfo;

  DEBUG ((DEBUG_INFO, "Usb4PlatformEndOfPeiPpiNotifyCallback entry\n"));

  Status = EFI_SUCCESS;
  Usb4PlatformInfo = NULL;

  //
  // Allocate memory for receiving the host router information in the platform.
  //
  Usb4PlatformInfo = AllocateZeroPool (sizeof (USB4_PLATFORM_INFO));
  if (Usb4PlatformInfo == NULL) {
    DEBUG ((DEBUG_ERROR, "Out of resources for querying USB4 platform information\n"));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  //
  // Query USB4 platform information for CM support
  //
  Status = Usb4PlatformInfoQuery (Usb4PlatformInfo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Fail to query USB4 platform information for CM support, %r\n", Status));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  //
  // USB4 platform initialization for CM support
  //
  Status = Usb4PlatformInit (Usb4PlatformInfo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "USB4 platform initialization failure for CM support, %r\n", Status));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  //
  // Create USB4 platform hob only if platform is ready for USB4 support.
  //
  Usb4PlatformHob = BuildGuidHob (&gUsb4PlatformHobGuid, sizeof (USB4_PLATFORM_HOB));
  if (Usb4PlatformHob == NULL) {
    DEBUG ((DEBUG_ERROR, "Out of resources for USB4 platform hob creation\n"));
    Status = EFI_UNSUPPORTED;
    goto Exit;
  }

  //
  // Save USB4 platform information to USB4 platform hob
  //
  CopyMem (&(Usb4PlatformHob->Usb4PlatformInfo), Usb4PlatformInfo, sizeof (USB4_PLATFORM_INFO));

Exit:
  if (Usb4PlatformInfo != NULL) {
    FreePool (Usb4PlatformInfo);
  }
  DEBUG ((DEBUG_INFO, "Usb4PlatformEndOfPeiPpiNotifyCallback exit\n"));
  return Status;
}

EFI_STATUS
EFIAPI
Usb4PlatformPeiEntry (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS    Status;

  DEBUG ((DEBUG_INFO, "Usb4PlatformPeiEntry entry\n"));

  //
  // Register a notification for End of PEI to allocate IOMMU DMA buffer.
  //
  Status = PeiServicesNotifyPpi (&mEndOfPeiNotifyList);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR , "Usb4PlatformPeiEntry: Failed to register a notification - %r\n", Status));
  }

  DEBUG ((DEBUG_INFO, "Usb4PlatformPeiEntry exit\n"));
  return Status;
}
