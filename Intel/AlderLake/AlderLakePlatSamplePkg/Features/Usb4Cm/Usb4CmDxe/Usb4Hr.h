/** @file
  Header file for USB4 host router routines used before PCI bus driver enumeration.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_HR_H_
#define _USB4_HR_H_

#include <Usb4Common.h>
#include <Usb4PlatformInfo.h>

//
// Allocate 1 MB MMIO size for USB4 Tx/Rx Ring programming interface.
// MMIO alignment 20 means 2^20 = 1MB
//
#define USB4_RING_MMIO_LENGTH        (1024 * 1024)
#define MMIO_ALIGNMENT_1MB           20

/**
  Create and initialize host router instance.
  - Allocate and assign MMIO space for Tx/Rx ring access
  - Initialize PCI config space.

  @param[in]  ImageHandle  - Usb4CmDxe image handle.
  @param[in]  HrInfo       - Host router information.
  @param[out] Usb4HrInst   - Pointer of pointer to the created USB4 host router instance.

  @retval EFI_SUCCESS           - Create host router instance successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval EFI_NOT_FOUND         - Device is not present.
  @retval EFI_OUT_OF_RESOURCES  - Insufficient to initialize host router instance.
**/
EFI_STATUS
Usb4HrCreate (
  IN  EFI_HANDLE         ImageHandle,
  IN  PUSB4_HR_INFO      HrInfo,
  OUT USB4_HR_INSTANCE   **Usb4HrInst
  );

/**
  Stop USB4 host router.
  - Un-initialize PCI config space.
  - Free the allocated MMIO space.

  @param[in] Usb4HrInst - Pointer to USB4 host router instance.
**/
VOID
Usb4HrStop (
  IN USB4_HR_INSTANCE    *Usb4HrInst
  );

/**
  Destroy USB4 host router instance.
  - Un-initialize PCI config space.
  - Free the allocated MMIO space.
  - Free the host router instance.

  @param[in] Usb4HrInst - Pointer to USB4 host router instance.
**/
VOID
Usb4HrDestroy (
  IN USB4_HR_INSTANCE    *Usb4HrInst
  );

/**
  Build USB4 domain topology of host router.

  @param[in] Usb4Hr - Pointer to USB4 host router instance.

  @retval EFI_SUCCESS           - Build USB4 topology successfully.
  @retval EFI_UNSUPPORTED       - Fail to build USB4 topology.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4HrBuildTopology (
  IN  USB4_HR_INSTANCE    *Usb4Hr
  );

/**
  Unpower USB4 host router

  @param[in] Usb4Hr - Pointer to USB4 host router instance.
**/
VOID
Usb4HrUnPower (
  IN USB4_HR_INSTANCE    *Usb4Hr
  );
#endif
