/** @file
  Header file for USB4 host router control routines used after PCI bus driver enumeration.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _POST_PCI_USB4_HR_H_
#define _POST_PCI_USB4_HR_H_

#include <Usb4Common.h>
#include <Usb4PlatformInfo.h>
#include <Usb4HrInst.h>

/**
  Start USB4 host router.
  - Query USB4 host router PCI resources assigned by PCI bus driver.
  - Initialize USB4 host interface.

  @param[in] Usb4Hr - Pointer to USB4 host router instance.

  @retval EFI_SUCCESS           - Start USB4 host router successfully.
  @retval EFI_UNSUPPORTED       - Fail to start USB4 host router.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
PostPciUsb4HrStart (
  IN USB4_HR_INSTANCE    *Usb4Hr
  );

/**
  Destroy USB4 host interface and host router instance.

  @param[in] Usb4Hr - Pointer to USB4 host router instance.
**/
VOID
PostPciUsb4HrDestroy (
  IN USB4_HR_INSTANCE    *Usb4Hr
  );
#endif
