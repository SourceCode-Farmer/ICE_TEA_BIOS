/** @file
  Header file for USB4 connection manager driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_CM_DXE_H_
#define _USB4_CM_DXE_H_
#include <Uefi.h>
#include <Usb4HrInst.h>
#include <Usb4PlatformInfo.h>

extern PUSB4_HR_INSTANCE   mUsb4HrInst[USB4_HR_SUPPORT_MAX];
extern UINT32              mUsb4HrCount;

//
// USB4 CM monitor timer interval.
// The unit is 100us, takes 100ms as interval.
//
#define USB4_CM_MONITOR_TIMER_INTERVAL     EFI_TIMER_PERIOD_MILLISECONDS(100)

/**
  USB4 CM execution for USB4 host routers in the platform.

  @param[in] ImageHandle      - ImageHandle of the loaded driver.
  @param[in] Usb4PlatformInfo - Pointer to the platform USB4 host router information.

  @retval EFI_SUCCESS           - Software CM execution success.
  @retval EFI_UNSUPPORTED       - General USB4 CM execution failure.
  @retval EFI_OUT_OUT_RESOURCES - Insufficient resources to execute CM.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4CmExecute (
  IN EFI_HANDLE            ImageHandle,
  IN USB4_PLATFORM_INFO    *Usb4PlatformInfo
  );

/**
  Stop CM and release resources.
  The resources will be re-assigned in PCI enumeration phase.
**/
VOID
Usb4CmStop (
  VOID
  );

/**
  Destroy all CM resources and instances.
**/
VOID
Usb4CmDestroy (
  VOID
  );

/**
  Unpower USB4 host router in platform.
**/
VOID Usb4CmUnpowerHr (
  VOID
  );
#endif
