## @file
# 
#******************************************************************************
#* Copyright 2021 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
### @file
#  Component information file for USB4 Software Connection Manager Dxe.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x0001001B
  BASE_NAME                      = Usb4CmDxe
  FILE_GUID                      = 5CD9649D-2874-4834-802B-C559C94703AE
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = DXE_DRIVER
  ENTRY_POINT                    = Usb4CmDxeEntryPoint

[LibraryClasses]
  BaseLib
  BaseMemoryLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  ReportStatusCodeLib
  HobLib
  IoLib
  MemoryAllocationLib
  UefiLib
  DebugLib
  ConfigBlockLib
  PcdLib
  DxeServicesTableLib
  TbtCommonLib
  PostCodeLib
  CmUtilsLib
  Usb4HrSrvLib
  Usb4HiCoreLib
  Usb4DomainLib
  Usb4RtEnumLib
  Usb4CsLib
  CmEvtLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
#[-start-210506-IB16740139-add]# for build error in RC2162
# insyde add for OemPlatformNvsArea.h
  $(PROJECT_PKG)/Project.dec
#[-end-210506-IB16740139-add]#

[Sources]
  Usb4CmDxe.c
  Usb4Cm.c
  Usb4Hr.c
  PostPciUsb4Cm.c
  PostPciUsb4Hr.c
  Usb4IhrPci.c
  Usb4DhrPci.c

[Pcd]
  gSiPkgTokenSpaceGuid.PcdITbtToPcieRegister         ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdPcieToITbtRegister         ## CONSUMES

[Protocols]
  gEfiFirmwareVolume2ProtocolGuid                ## CONSUMES
  gEdkiiIoMmuProtocolGuid                        ## CONSUMES
  gEfiPciIoProtocolGuid                          ## CONSUMES
  gEfiResetNotificationProtocolGuid              ## CONSUMES

[Guids]
  gEfiEventExitBootServicesGuid                 ## CONSUMES
  gEfiEndOfDxeEventGroupGuid                    ## CONSUMES
  gUsb4PlatformHobGuid                          ## CONSUMES
  gSetupVariableGuid                            ## CONSUMES

[Depex]
  gEfiVariableWriteArchProtocolGuid AND
  gEfiVariableArchProtocolGuid