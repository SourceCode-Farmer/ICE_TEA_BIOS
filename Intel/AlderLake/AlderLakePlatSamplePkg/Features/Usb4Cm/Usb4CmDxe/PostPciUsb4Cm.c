/** @file
  USB4 Connection Manager routines used after PCI bus driver enumeration.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Usb4PlatformHob.h>
#include <Library/CmUtilsLib.h>
#include <Library/Usb4DomainLib.h>
#include <Library/Usb4RouterLib.h>
#include <Library/CmEvtLib.h>
#include "PostPciUsb4Hr.h"
#include "Usb4CmDxe.h"

/**
  Start connection manager on the USB4 host router.

  @param[in] Usb4Hr - Pointer to USB4 host router instance.

  @retval EFI_SUCCESS           - Start connection manager successfully.
  @retval EFI_UNSUPPORTED       - Fail to start connection manager.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
PostPciCmStart (
  IN USB4_HR_INSTANCE    *Usb4Hr
  )
{
  EFI_STATUS   Status;

  DEBUG ((DEBUG_INFO, "PostPciCmStart entry - Usb4Hr = %p\n", Usb4Hr));

  if (Usb4Hr == NULL) {
    DEBUG ((DEBUG_ERROR, "PostPciCmStart: Null Usb4Hr\n"));
    Status = EFI_INVALID_PARAMETER;
    goto Exit;
  }

  //
  // Start USB4 host router to enable USB4 host interface.
  //
  Status = PostPciUsb4HrStart (Usb4Hr);

  //
  // Bind USB4 host router to the USB4 domain.
  // Domain pointer should come from pre-installed protocol for driver model driver case.
  //
  if (Usb4Hr->Usb4Domain != NULL) {
    Status = Usb4DomainBindHr (Usb4Hr, Usb4Hr->Usb4Domain);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "PostPciCmStart: Fail to bind USB4 domain to USB4 host router, %r\n", Status));
      goto Exit;
    }
  }

  DEBUG ((DEBUG_INFO, "PostPciCmStart exit\n"));

Exit:
  return Status;
}

/**
  CM monitor handler that process all events received in Rx Ring.

  @param  Event      - Timer event.
  @param  Context    - Pointer to USB4 host router context
  .
**/
VOID
EFIAPI
Usb4CmMonitor (
  IN EFI_EVENT            Event,
  IN VOID                 *Context
  )
{
  USB4_HR_INSTANCE    *Usb4Hr;
  EFI_TPL             OldTpl;

  OldTpl = gBS->RaiseTPL (TPL_NOTIFY);

  Usb4Hr = (USB4_HR_INSTANCE *) Context;
  if (Usb4Hr == NULL) {
    DEBUG ((DEBUG_ERROR, "Usb4CmMonitor: Null Usb4Hr\n"));
    goto Exit;
  }

  if (Usb4Hr->Signature != USB4_HR_INSTANCE_SIGNATURE) {
    DEBUG ((DEBUG_ERROR, "Usb4CmMonitor: Invalid USB4 HR signature 0x%0x\n", Usb4Hr->Signature));
    goto Exit;
  }
  //
  // Process Rx events
  //
  CmProcessRxEvents (Usb4Hr);

Exit:
  gBS->RestoreTPL (OldTpl);
  return;
}

/**
  Connection Manager destroy.

  @param[in] CmInst - Pointer to CM instance.
**/
VOID
PostPciCmDestroy (
  IN USB4_HR_INSTANCE    *Usb4Hr
  )
{
  DEBUG ((DEBUG_INFO, "PostPciCmDestroy entry - Usb4Hr = %p\n", Usb4Hr));

  if (Usb4Hr != NULL) {

    if (Usb4Hr->Usb4Domain != NULL) {
      //
      // Remove and destroy all Routers from Domain.
      //
      Usb4DomainDestroy (Usb4Hr->Usb4Domain);
      Usb4Hr->Usb4Domain = NULL;
    }

    PostPciUsb4HrDestroy (Usb4Hr);
  }

  DEBUG ((DEBUG_INFO, "PostPciCmDestroy exit\n"));
}