/**@file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#ifndef _TBT_SECURITY_LIB_H_
#define _TBT_SECURITY_LIB_H_

#include <Protocol/AcpiTable.h>
#include <IndustryStandard/Pci.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/UefiLib.h>
#include <Uefi.h>
#include <SetupVariable.h>
#include <DTbtInfoHob.h>
#include <PcieRegs.h>
#include <Library/DTbtCommonLib.h>

#define TBT_SECURITY_EVENT_STRING                 "DMA Protection Disabled"
#define TBT_SECURITY_EVENT_STRING_LEN             (sizeof (TBT_SECURITY_EVENT_STRING) - 1)

#define TBT_SECURITY_LEVEL_DOWNGRADED_STRING      "Security Level is Downgraded to 0"
#define TBT_SECURITY_LEVEL_DOWNGRADED_STRING_LEN  (sizeof (TBT_SECURITY_LEVEL_DOWNGRADED_STRING) - 1)

#define GET_TBT_SECURITY_MODE    0
#define SET_TBT_SECURITY_MODE    1

/**
  Get or set Thunderbolt(TM) security mode

  @param[in]  SecurityMode        - TBT Security Level
  @param[in]  DTbtController      - Enable/Disable DTbtController
  @param[in]  MaxControllerNumber - Number of contorller
  @param[in]  Action              - 0 = get, 1 = set

  @retval                         - Return security level
**/
UINT8
EFIAPI
GetSetSecurityMode (
  IN UINT8                       SecurityMode,
  IN UINT8                       *DTbtController,
  IN UINT8                       MaxControllerNumber,
  IN UINT8                       Action
);
#endif
