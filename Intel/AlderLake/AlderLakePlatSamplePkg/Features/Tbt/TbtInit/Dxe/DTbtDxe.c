/** @file
  DTBT init Dxe driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <Uefi/UefiSpec.h>
#include <DTbtInfoHob.h>
#include <SetupVariable.h>
#include <Protocol/TbtDisBmeProtocol.h>
#include <Protocol/DxeDTbtPolicy.h>
#include <Protocol/DTbtNvsArea.h>
#include <Protocol/FirmwareVolume2.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/TbtCommonLib.h>
#include <Library/PcdLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/AslUpdateLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DxeTbtDisBmeLib.h>
#include <Library/DTbtCommonLib.h>
#include <Library/DevicePathLib.h>
#include <Library/PrintLib.h>

GLOBAL_REMOVE_IF_UNREFERENCED DTBT_NVS_AREA_PROTOCOL                    mDTbtNvsAreaProtocol;
GLOBAL_REMOVE_IF_UNREFERENCED DTBT_INFO_HOB                             *gDTbtInfoHob = NULL;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN                                   mTbtRtd3PepEnable = FALSE;

/**
  Initialize System Agent SSDT ACPI tables.

  @retval EFI_SUCCESS    ACPI tables are initialized successfully
  @retval EFI_NOT_FOUND  ACPI tables not found
**/
EFI_STATUS
InitializeDTbtSsdtAcpiTables (
  VOID
  )
{
  EFI_STATUS                    Status;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         NumberOfHandles;
  EFI_FV_FILETYPE               FileType;
  UINT32                        FvStatus;
  EFI_FV_FILE_ATTRIBUTES        Attributes;
  UINTN                         Size;
  UINTN                         i;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
  INTN                          Instance;
  EFI_ACPI_COMMON_HEADER        *CurrentTable;
  UINTN                         AcpiTableKey;
  UINT8                         *CurrPtr;
  UINT8                         *EndPtr;
  UINT32                        *Signature;
  EFI_ACPI_DESCRIPTION_HEADER   *TbtAcpiTable;
  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;

  FwVol       = NULL;
  TbtAcpiTable = NULL;
  ///
  /// Locate ACPI Table protocol
  ///
  DEBUG ((DEBUG_INFO, "Init DTBT SSDT table\n"));
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID **) &AcpiTable);
  if (Status != EFI_SUCCESS) {
    DEBUG ((DEBUG_WARN, "Fail to locate EfiAcpiTableProtocol.\n"));
    return EFI_NOT_FOUND;
  }
  ///
  /// Locate protocol.
  /// There is little chance we can't find an FV protocol
  ///
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiFirmwareVolume2ProtocolGuid,
                  NULL,
                  &NumberOfHandles,
                  &HandleBuffer
                  );
  ASSERT_EFI_ERROR (Status);
  ///
  /// Looking for FV with ACPI storage file
  ///
  for (i = 0; i < NumberOfHandles; i++) {
    ///
    /// Get the protocol on this handle
    /// This should not fail because of LocateHandleBuffer
    ///
    Status = gBS->HandleProtocol (
                    HandleBuffer[i],
                    &gEfiFirmwareVolume2ProtocolGuid,
                    (VOID **) &FwVol
                    );
    ASSERT_EFI_ERROR (Status);

    ///
    /// See if it has the ACPI storage file
    ///
    Size      = 0;
    FvStatus  = 0;
    Status = FwVol->ReadFile (
                      FwVol,
                      &gEfiCallerIdGuid,
                      NULL,
                      &Size,
                      &FileType,
                      &Attributes,
                      &FvStatus
                      );
    ///
    /// If we found it, then we are done
    ///
    if (Status == EFI_SUCCESS) {
      break;
    }
  }
  ///
  /// Free any allocated buffers
  ///
  FreePool (HandleBuffer);

  ///
  /// Sanity check that we found our data file
  ///
  ASSERT (FwVol != NULL);
  if (FwVol == NULL) {
    DEBUG ((DEBUG_INFO, "TBT Global NVS table not found\n"));
    return EFI_NOT_FOUND;
  }
  ///
  /// Our exit status is determined by the success of the previous operations
  /// If the protocol was found, Instance already points to it.
  /// Read tables from the storage file.
  ///
  Instance      = 0;
  CurrentTable  = NULL;
  while (Status == EFI_SUCCESS) {
    Status = FwVol->ReadSection (
                      FwVol,
                      &gEfiCallerIdGuid,
                      EFI_SECTION_RAW,
                      Instance,
                      (VOID **) &CurrentTable,
                      &Size,
                      &FvStatus
                      );
    if (!EFI_ERROR (Status)) {
      ///
      /// Check the table ID to modify the table
      ///
      if (((EFI_ACPI_DESCRIPTION_HEADER *) CurrentTable)->OemTableId == SIGNATURE_64 ('D','T', 'b', 't', 'S', 's', 'd', 't')) {
        TbtAcpiTable = (EFI_ACPI_DESCRIPTION_HEADER *) CurrentTable;
        ///
        /// Locate the SSDT package
        ///
        CurrPtr = (UINT8 *) TbtAcpiTable;
        EndPtr  = CurrPtr + TbtAcpiTable->Length;

        for (; CurrPtr <= EndPtr; CurrPtr++) {
          Signature = (UINT32 *) (CurrPtr + 3);
          if (*Signature == SIGNATURE_32 ('D', 'T', 'N', 'V')) {
            ASSERT (*(UINT32 *) (CurrPtr + 3 + sizeof (*Signature) + 2) == 0xFFFF0000);
            ASSERT (*(UINT16 *) (CurrPtr + 3 + sizeof (*Signature) + 2 + sizeof (UINT32) + 1) == 0xAA55);
            ///
            /// TBT Global NVS Area address
            ///
            *(UINT32 *) (CurrPtr + 3 + sizeof (*Signature) + 2) = (UINT32) (UINTN) mDTbtNvsAreaProtocol.Area;
            ///
            /// TBT Global NVS Area size
            ///
            *(UINT16 *) (CurrPtr + 3 + sizeof (*Signature) + 2 + sizeof (UINT32) + 1) = sizeof (DTBT_NVS_AREA);

            AcpiTableKey = 0;
            Status = AcpiTable->InstallAcpiTable (
                                  AcpiTable,
                                  TbtAcpiTable,
                                  TbtAcpiTable->Length,
                                  &AcpiTableKey
                                  );
            ASSERT_EFI_ERROR (Status);
            return EFI_SUCCESS;
          }
        }
      }
      ///
      /// Increment the instance
      ///
      Instance++;
      CurrentTable = NULL;
    }
  }
  return Status;
}
/**
  DisableDTbtBmeCallBackFunction

  Disable BME on DTbt tree at ExitBootServices to hand off security of TBT hierarchies to the OS.

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
DisableDTbtBmeCallBackFunction (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS                Status;
  UINT8                     Index;
  SBDF                      Sbdf = {0,0,0,0,0};
  UINTN                     RpDev;
  UINTN                     RpFunc;
  DTBT_INFO_HOB             *DTbtInfoHob = NULL;

  DEBUG((DEBUG_INFO, "[TBT] DisableDTbtBmeCallBackFunction START\n"));

  Status      = EFI_SUCCESS;
  Index       = 0;
  RpDev       = 0;
  RpFunc      = 0;

  //
  // Get DTbt INFO HOB
  //
  DTbtInfoHob = (DTBT_INFO_HOB *) GetFirstGuidHob (&gDTbtInfoHobGuid);
  if (DTbtInfoHob == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to get gDTbtInfoHob\n"));
    goto Exit;
  }

  for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
    DEBUG ((DEBUG_INFO, "DTbtInfoHob->DTbtControllerConfig[%d].PcieRpNumber: %d\n", Index, DTbtInfoHob->DTbtControllerConfig[Index].PcieRpNumber));

    if (DTbtInfoHob->DTbtControllerConfig[Index].DTbtControllerEn == 1) {
      Status = GetDTbtRpDevFun (DTbtInfoHob->DTbtControllerConfig[Index].Type, DTbtInfoHob->DTbtControllerConfig[Index].PcieRpNumber - 1, &RpDev, &RpFunc);
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "Failed to get GetDTbtRpDevFun, Status: %d\n", Status));
        continue;
      }
      Sbdf.Dev = (UINT32) RpDev;
      Sbdf.Func = (UINT32) RpFunc;
      RecursiveTbtHierarchyConfiguration (Sbdf);
    } else {
      DEBUG ((DEBUG_INFO, "DTbtInfoHob->DTbtControllerConfig[%d].DTbtControllerEn: %d\n", Index, DTbtInfoHob->DTbtControllerConfig[Index].DTbtControllerEn));
    }
  }

Exit:
  gBS->CloseEvent (Event);
  DEBUG((DEBUG_INFO, "[TBT] DisableDTbtBmeCallBackFunction END\n"));
}

EFI_DISABLE_TBT_BME_PROTOCOL mDisableDTbtBmeProtocol = {
    DisableDTbtBmeCallBackFunction,
};

/**
  Install DisableBme Protocol for DTBT UEFI-Shell Validation
**/
VOID
InstallDTbtDisableBmeProtocol (
  VOID
  )
{
  EFI_STATUS        Status;
  EFI_HANDLE        Handle;

  Handle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gDxeDisableDTbtBmeProtocolGuid,
                  &mDisableDTbtBmeProtocol,
                  NULL
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "DTbtDisableBmeProtocol Failed. Status: %d\n", Status));
  } else {
    DEBUG ((DEBUG_INFO, "DTbtDisableBmeProtocol Installed\n"));
  }
}

/**
  DTBT NVS Area Initialize
**/
VOID
DTbtNvsAreaInit (
  IN  VOID              **mDTbtNvsAreaPtr
  )
{
  UINTN                         Pages;
  EFI_PHYSICAL_ADDRESS          Address;
  EFI_STATUS                    Status;

  DEBUG ((DEBUG_INFO, "DTbtNvsAreaInit Start\n"));

  Pages = EFI_SIZE_TO_PAGES (sizeof (DTBT_NVS_AREA));
  Address = 0xffffffff; // allocate address below 4G.

  Status  = gBS->AllocatePages (
                   AllocateMaxAddress,
                   EfiACPIMemoryNVS,
                   Pages,
                   &Address
                   );
  ASSERT_EFI_ERROR (Status);

  *mDTbtNvsAreaPtr = (VOID *) (UINTN) Address;
  SetMem (*mDTbtNvsAreaPtr, sizeof (DTBT_NVS_AREA), 0);

  DEBUG ((DEBUG_INFO, "DTbtNvsAreaInit End\n"));
}

/**
  DTBT NVS Area Update
**/
VOID
DTbtNvsAreaUpdate (
)
{
  DTBT_NVS_AREA_PROTOCOL        *DTbtNvsAreaProtocol;
  DXE_DTBT_POLICY_PROTOCOL      *DxeDTbtConfig;
  EFI_STATUS                    Status;

  DEBUG ((DEBUG_INFO, "DTbtNvsAreaUpdate Start\n"));

  Status = gBS->LocateProtocol(
                  &gDxeDTbtPolicyProtocolGuid,
                  NULL,
                  (VOID **) &DxeDTbtConfig
                  );
  ASSERT_EFI_ERROR (Status);

  //
  // DTBTNvsAreaProtocol default value init here
  //
  DTbtNvsAreaProtocol = (DTBT_NVS_AREA_PROTOCOL *) &mDTbtNvsAreaProtocol;

  //
  // Initialize default values for DTBT NVS
  //
  DTbtNvsAreaProtocol->Area->TbtAcpiRemovalSupport   = 0;
  DTbtNvsAreaProtocol->Area->DiscreteTbtSupport      = ((gDTbtInfoHob->DTbtControllerConfig[0].DTbtControllerEn == 1 || gDTbtInfoHob->DTbtControllerConfig[1].DTbtControllerEn == 1) ? TRUE : FALSE);
  DTbtNvsAreaProtocol->Area->TbtGpioFilter           = (UINT8) DxeDTbtConfig->DTbtGenericConfig.Gpio5Filter;
  DTbtNvsAreaProtocol->Area->TbtWakeupSupport        = (UINT8) DxeDTbtConfig->DTbtGenericConfig.TbtWakeupSupport;
  DTbtNvsAreaProtocol->Area->DTbtRtd3                = (UINT8) DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3;             // DTBT RTD3 Enable.
  DTbtNvsAreaProtocol->Area->DTbtRtd3OffDelayOptEn   = (UINT8) DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3OffDelayOptEn;// DTBT RTD3 Off Optimization Enable.
  DTbtNvsAreaProtocol->Area->DTbtRtd3OffDelay        = (UINT16) DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3OffDelay;    // DTBT RTD3 Off delay in ms.
  DTbtNvsAreaProtocol->Area->DTbtRtd3ClkReq          = (UINT8) DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3ClkReq;       // DTBT RTD3 ClkReq Mask Enable.
  DTbtNvsAreaProtocol->Area->DTbtRtd3ClkReqDelay     = (UINT16) DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3ClkReqDelay; // DTBT RTD3 ClkReq mask delay in ms.
  DTbtNvsAreaProtocol->Area->DTbtGo2SxCommand        = (UINT8) DxeDTbtConfig->DTbtGenericConfig.DTbtGo2SxCommand;

  //
  // DTBT Controller 1
  //
  DTbtNvsAreaProtocol->Area->RootportSelected0       = gDTbtInfoHob->DTbtControllerConfig[0].PcieRpNumber;
  DTbtNvsAreaProtocol->Area->RootportSelected0Type   = gDTbtInfoHob->DTbtControllerConfig[0].Type;
  DTbtNvsAreaProtocol->Area->RootportEnabled0        = gDTbtInfoHob->DTbtControllerConfig[0].DTbtControllerEn;
  DTbtNvsAreaProtocol->Area->TbtCioPlugEventGpioNo0  = gDTbtInfoHob->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad;
  DTbtNvsAreaProtocol->Area->TbtPcieRstGpioNo0       = gDTbtInfoHob->DTbtControllerConfig[0].PcieRstGpio.GpioPad;
  DTbtNvsAreaProtocol->Area->TbtPcieRstGpioLevel0    = gDTbtInfoHob->DTbtControllerConfig[0].PcieRstGpio.GpioLevel;

  //
  // DTBT Controller 2
  //
  DTbtNvsAreaProtocol->Area->RootportSelected1       = gDTbtInfoHob->DTbtControllerConfig[1].PcieRpNumber;
  DTbtNvsAreaProtocol->Area->RootportSelected1Type   = gDTbtInfoHob->DTbtControllerConfig[1].Type;
  DTbtNvsAreaProtocol->Area->RootportEnabled1        = gDTbtInfoHob->DTbtControllerConfig[1].DTbtControllerEn;
  DTbtNvsAreaProtocol->Area->TbtCioPlugEventGpioNo1  = gDTbtInfoHob->DTbtControllerConfig[1].CioPlugEventGpio.GpioPad;
  DTbtNvsAreaProtocol->Area->TbtPcieRstGpioNo1       = gDTbtInfoHob->DTbtControllerConfig[1].PcieRstGpio.GpioPad;
  DTbtNvsAreaProtocol->Area->TbtPcieRstGpioLevel1    = gDTbtInfoHob->DTbtControllerConfig[1].PcieRstGpio.GpioLevel;

  DTbtNvsAreaProtocol->Area->TBtCommonGpioSupport    = gDTbtInfoHob->DTbtGenericConfig.DTbtSharedGpioConfiguration;
  DTbtNvsAreaProtocol->Area->TbtBootDeviceConnected  = 0;
  DTbtNvsAreaProtocol->Area->DTbtControllerPresentNo = PcdGet8 (PcdDTbtControllerNumber);

  DEBUG ((DEBUG_INFO, "DTbtNvsAreaUpdate End\n"));
}

/**
  This function gets registered as a callback to patch TBT ASL code

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
DTbtAcpiEndOfDxeCallback (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS                            Status;
  UINTN                                 RpDev;
  UINTN                                 RpFunc;
  UINT16                                HrDeviceId;
  BOOLEAN                               EnableResetBridge;
  UINT8                                 HostRouterBus;
  UINT8                                 DTbtControllerBus;
  UINT8                                 RootPortSubordinateBus;
  UINT8                                 Index;

  EnableResetBridge = FALSE;

  mDTbtNvsAreaProtocol.Area->DTbtRevision       = DTBT_NVS_AREA_REVISION;
  if (gDTbtInfoHob != NULL) {

    // Check and Update the Validity and Availability of TBT Host Router
    if (mTbtRtd3PepEnable) {
      for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
        if (gDTbtInfoHob->DTbtControllerConfig[Index].DTbtControllerEn == 1) {
          Status = GetDTbtRpDevFun (gDTbtInfoHob->DTbtControllerConfig[Index].Type, gDTbtInfoHob->DTbtControllerConfig[Index].PcieRpNumber - 1, &RpDev, &RpFunc);
          ASSERT_EFI_ERROR(Status);

          HostRouterBus = PciSegmentRead8 (PCI_SEGMENT_LIB_ADDRESS (0, 0x00, (UINT32)RpDev, (UINT32)RpFunc, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET));
          RootPortSubordinateBus = PciSegmentRead8 (PCI_SEGMENT_LIB_ADDRESS (0, 0x00, (UINT32)RpDev, (UINT32)RpFunc, PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET));
          DEBUG ((DEBUG_INFO, "Validating TBT 1 Controller HR for TBT PEP Constraint(), TBT_HR_BUS = 0x%x \n", HostRouterBus));
          DTbtControllerBus = HostRouterBus;

          if (HostRouterBus == 0 || HostRouterBus == 0xFF) {
            //
            // Set Sec/Sub buses to a temporary value
            //
            PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET), FixedPcdGet8 (PcdTbtTempBusNumber));
            PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET), FixedPcdGet8 (PcdTbtTempBusNumber));
            DTbtControllerBus = FixedPcdGet8 (PcdTbtTempBusNumber);
            DEBUG ((DEBUG_INFO, "HostRouterBus is set to a temporary bus number: 0x%x\n", HostRouterBus));
            EnableResetBridge = TRUE;
          }

          //
          // AS TBT PEP should be enable only if TBT is in RTD3 mode. So Tbt
          // Hostrouter Device ID should be visible now. If Device Id = 0xFFFF or
          // Device ID is not a Tbt Hostrouter Device ID then dTBT card is not
          // connected. So disable Corresponding TBT RP Enable NVS Variable and exit.
          //
          HrDeviceId = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (0, DTbtControllerBus, 0, 0, PCI_DEVICE_ID_OFFSET));

          switch (Index) {
          case 0:
            mDTbtNvsAreaProtocol.Area->RootportEnabled0 = IsTbtHostRouter (HrDeviceId);
            break;
          case 1:
            mDTbtNvsAreaProtocol.Area->RootportEnabled1 = IsTbtHostRouter (HrDeviceId);
            break;
          default:
            DEBUG ((DEBUG_INFO, "TBT RTD3 PEP Invalid Controller Number.\n"));
            break;
          }
          if (EnableResetBridge == TRUE) {
            //
            // Reset Sec/Sub buses to original value
            //
            PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET), HostRouterBus);
            PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET), RootPortSubordinateBus);
            EnableResetBridge = FALSE;
          }
        }
      }
    }
  }

  return;
}

/**
TbtScanDownStreamBootDeviceConnected

In this function, check the system boot list includes TBT downstream port device.
If TBT storage is list as the current boot device, updates TbtBootDeviceConnected to 1,
then will return NULL with TBT RP _DEP method to avoid sticking on dependency
while booting OS from storage.

@param[in] Event     - A pointer to the Event that triggered the callback.
@param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
TbtScanDownStreamBootDeviceConnected (
  IN EFI_EVENT    Event,
  IN VOID         *Context
)
{
  UINTN                        Index;
  BOOLEAN                      TbtBootDeviceEnable;
  EFI_DEVICE_PATH_PROTOCOL     *CurrentPath;
  PCI_DEVICE_PATH              *PciNode;
  PCI_DEVICE_PATH              TbtRootPort[MAX_DTBT_CONTROLLER_NUMBER];
  UINTN                        TbtControllerIndex;
  UINTN                        TbtEnableCount;
  UINTN                        RpDev;
  UINTN                        RpFunc;
  EFI_STATUS                   Status;
  UINT16                       BootCurrent;
  CHAR16                       BootOptionName[16];
  UINT8                        *BootOption;
  UINT8                        *Ptr;
  UINTN                        VarSize;

  DEBUG((DEBUG_INFO, "[TBT] TbtScanDownStreamBootDeviceConnected::Entry\n"));

  TbtBootDeviceEnable = FALSE;
  Index = 0;

  if (gDTbtInfoHob == NULL) {
    DEBUG ((DEBUG_INFO, "gDTbtInfoHob::NULL return\n"));
    return;
  }

  DEBUG ((DEBUG_INFO, "Default BootCurrent is '0'\n"));

  //
  // Collect System Enable DTBT root port PSF address if DTBT is enable.
  //
  TbtEnableCount = 0;
  for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
    if (gDTbtInfoHob->DTbtControllerConfig[Index].DTbtControllerEn == 1) {
      Status = GetDTbtRpDevFun (gDTbtInfoHob->DTbtControllerConfig[Index].Type, \
        gDTbtInfoHob->DTbtControllerConfig[Index].PcieRpNumber - 1, \
        &RpDev, &RpFunc);
      if (EFI_ERROR (Status)) {
        continue;
      }

      //
      // Save Enable TBT PCIe Root Port number.
      //
      TbtRootPort[TbtEnableCount].Device = (UINT8)RpDev;
      TbtRootPort[TbtEnableCount].Function = (UINT8)RpFunc;
      TbtEnableCount++;
      DEBUG ((DEBUG_INFO, "Dtbt Root Port:Index-%x:Dev-%x Fun-%x\n", Index, RpDev, RpFunc));
    }
  }

  //
  // Get BootCurrent variable
  //
  VarSize = sizeof (UINT16);
  Status = gRT->GetVariable (
                  L"BootCurrent",
                  &gEfiGlobalVariableGuid,
                  NULL,
                  &VarSize,
                  &BootCurrent
                  );
  if (EFI_ERROR (Status)) {
    return;
  }

  //
  // Create boot option Bootxxxx from BootCurrent
  //
  UnicodeSPrint (BootOptionName, sizeof(BootOptionName), L"Boot%04X", BootCurrent);
  GetEfiGlobalVariable2 (BootOptionName, (VOID **)&BootOption, &VarSize);
  if (BootOption == NULL || VarSize == 0) {
    return;
  }

  Ptr = BootOption;
  Ptr += sizeof (UINT32);
  Ptr += sizeof (UINT16);
  Ptr += StrSize ((CHAR16 *) Ptr);
  CurrentPath = (EFI_DEVICE_PATH_PROTOCOL *) Ptr;

  DEBUG ((DEBUG_INFO, "TbtEnableCount::%x\n", TbtEnableCount));

  //
  // Checking boot device currently is booting from TBT hierarchy
  //
  if (TbtEnableCount > 0) {
    while (!IsDevicePathEnd (CurrentPath) && (TbtBootDeviceEnable == FALSE)) {
      if ((DevicePathType (CurrentPath) == HARDWARE_DEVICE_PATH) && \
        (DevicePathSubType (CurrentPath) == HW_PCI_DP)) {

        PciNode = (PCI_DEVICE_PATH *) CurrentPath;
        DEBUG ((DEBUG_INFO, "Search device node:Dev-%x Fun-%x\n", PciNode->Device, PciNode->Function));

        for (TbtControllerIndex = 0; TbtControllerIndex < TbtEnableCount; TbtControllerIndex++) {

          DEBUG ((DEBUG_INFO, "TbtControllerIndex %x :Dev-%x Fun-%x\n", TbtControllerIndex, TbtRootPort[TbtControllerIndex].Device, TbtRootPort[TbtControllerIndex].Function));
          if ((TbtRootPort[TbtControllerIndex].Device == PciNode->Device) && \
            (TbtRootPort[TbtControllerIndex].Function == PciNode->Function)) {
            //
            // Found TBT Boot device.
            //
            DEBUG ((DEBUG_INFO, "Found DTBT Bootable device:--:\n"));
            TbtBootDeviceEnable = TRUE;
          }
        }
      }

      CurrentPath = NextDevicePathNode (CurrentPath);
    }
  } // End of if (BootOptionCount > 0) {

  DEBUG ((DEBUG_INFO, "TbtBootDeviceEnable::%x\n", TbtBootDeviceEnable));

  //
  // If found TBT boot device, update TBTNVS.TbtBootDeviceConnected "TBDC" to '1'.
  //
  if (TbtBootDeviceEnable == TRUE) {
    mDTbtNvsAreaProtocol.Area->TbtBootDeviceConnected = 1;
  }

  DEBUG ((DEBUG_INFO, "[TBT] TbtScanDownStreamBootDeviceConnected::End\n"));
}

/**
  Initialize Thunderbolt(TM) SSDT ACPI tables

  @retval EFI_SUCCESS    ACPI tables are initialized successfully
  @retval EFI_NOT_FOUND  ACPI tables not found
**/

EFI_STATUS
EFIAPI
DTbtDxeEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS                Status;
  EFI_HANDLE                Handle;
  EFI_EVENT                 EndOfDxeEvent;
  EFI_EVENT                 ExitBootServiceEvent;
  UINTN                     DataSize;
  SETUP_DATA                *SetupData;
  DXE_DTBT_POLICY_PROTOCOL  *DxeTbtConfig;
  UINT32                    SetupDataAttributes;
  EFI_EVENT                 ReadyToBootEvent;

  Status                    = EFI_SUCCESS;
  Handle                    = NULL;
  EndOfDxeEvent             = NULL;
  ExitBootServiceEvent      = NULL;
  DxeTbtConfig              = NULL;

  DEBUG ((DEBUG_INFO, "[TBT] DTbtDxeEntryPoint START\n"));

  //
  // Get DTBT INFO HOB
  //
  gDTbtInfoHob = (DTBT_INFO_HOB *) GetFirstGuidHob (&gDTbtInfoHobGuid);
  if (gDTbtInfoHob == NULL) {
    DEBUG ((DEBUG_INFO, "gDTbtInfoHobGuid not found\n"));
    return EFI_NOT_FOUND;
  }

  //
  // Initialize DTbt Nvs Area
  //
  DTbtNvsAreaInit ((VOID **) &mDTbtNvsAreaProtocol.Area);

  //
  // Install DTbt Global NVS ACPI table
  //
  Status = InitializeDTbtSsdtAcpiTables ();

  //
  // Update DTbt Nvs Area
  //
  DTbtNvsAreaUpdate ();

  //
  // [ACPI] DTBT ACPI table
  //

  Handle = NULL;

  Status = gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gDTbtNvsAreaProtocolGuid,
                  &mDTbtNvsAreaProtocol,
                  NULL
                  );
  ASSERT_EFI_ERROR (Status);

  //
  // Register an end of DXE event for DTBT ACPI to do some patch
  //
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  DTbtAcpiEndOfDxeCallback,
                  NULL,
                  &gEfiEndOfDxeEventGroupGuid,
                  &EndOfDxeEvent
                  );
  ASSERT_EFI_ERROR (Status);

  //
  // TBT Security requirment
  //
  DataSize = sizeof (SETUP_DATA);
  SetupData = AllocateZeroPool (DataSize);
  if (SetupData == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to allocate setup data size\n"));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  &SetupDataAttributes,
                  &DataSize,
                  SetupData
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Setup Data Status: %d\n", Status));
    goto Exit;
  }

  // Check TBT PEP support and update mTbtRtd3PepEnable
  if (SetupData->Rtd3Support && SetupData->DiscreteTbtSupport && SetupData->DTbtRtd3 && SetupData->PepTcss && SetupData->LowPowerS0Idle) {
    mTbtRtd3PepEnable = TRUE;
  }

  if (SetupData->TbtBootOn != 0) {
    //
    // Register a Ready to Boot event for check TBT Boot device is exsit
    //
    Status = gBS->CreateEventEx(
                    EVT_NOTIFY_SIGNAL,
                    TPL_NOTIFY,
                    TbtScanDownStreamBootDeviceConnected,
                    NULL,
                    &gEfiEventReadyToBootGuid,
                    &ReadyToBootEvent
                    );
  }

  Status = gBS->LocateProtocol (
              &gDxeDTbtPolicyProtocolGuid,
              NULL,
              (VOID **) &DxeTbtConfig
              );
  if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Failed to DxeTbtConfig, Status: %d\n", Status));
      goto Exit;
  }

  if (DxeTbtConfig->TbtCommonConfig.TbtVtdBaseSecurity == TRUE) {

    //
    // Register a Exit Boot Service for disable DTbt BME
    //
    DEBUG ((DEBUG_INFO, "Register a Exit Boot Service for disable DTbt BME\n"));
    Status = gBS->CreateEventEx (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    DisableDTbtBmeCallBackFunction,
                    NULL,
                    &gEfiEventExitBootServicesGuid,
                    &ExitBootServiceEvent
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Failed to Register a Exit Boot Service for disable DTbt BME, Status: %d\n", Status));
      gBS->CloseEvent (ExitBootServiceEvent);
      goto Exit;
    }

    //
    // Install DTbt DisableBme for UEFI-Shell testing
    //
    InstallDTbtDisableBmeProtocol ();
  }

Exit:
  DEBUG ((DEBUG_INFO, "[TBT] DTbtDxeEntryPoint END\n"));
  return Status;
}
