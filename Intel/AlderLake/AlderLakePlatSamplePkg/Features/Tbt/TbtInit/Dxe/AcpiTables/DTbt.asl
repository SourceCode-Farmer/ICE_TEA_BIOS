/** @file
 Thunderbolt ACPI methods

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <TbtMailBoxCmdDefinition.h>

#define PCIE_RP_TYPE_PCH                     0x01
#define PCIE_RP_TYPE_CPU                     0x02

#ifndef  BIT29
#define  BIT29    0x20000000
#endif
#define MAX_PEG_RP                        3

External (\_SB.PC00, DeviceObj)
External (\_SB.PC00.GPCB, MethodObj)
External (\_SB.SPC0, MethodObj)
External (\ADBG, MethodObj)
External (\RPA1, MethodObj)
External (\RPA2, MethodObj)
External (\RPA3, MethodObj)
External (\RPA4, MethodObj)
External (\RPA5, MethodObj)
External (\RPA6, MethodObj)
External (\RPA7, MethodObj)
External (\RPA8, MethodObj)
External (\RPA9, MethodObj)
External (\RPAA, MethodObj)
External (\RPAB, MethodObj)
External (\RPAC, MethodObj)
External (\RPAD, MethodObj)
External (\RPAE, MethodObj)
External (\RPAF, MethodObj)
External (\RPAG, MethodObj)
External (\RPAH, MethodObj)
External (\RPAI, MethodObj)
External (\RPAJ, MethodObj)
External (\RPAK, MethodObj)
External (\RPAL, MethodObj)
External (\RPAM, MethodObj)
External (\RPAN, MethodObj)
External (\RPAO, MethodObj)
External (\RPAP, MethodObj)
External (\RPAQ, MethodObj)
External (\RPAR, MethodObj)
External (\RPAS, MethodObj)
External (WFEV)
External (\_SB.PC00.PEG0, DeviceObj)
External (\_SB.PC00.PEG1, DeviceObj)
External (\_SB.PC00.PEG2, DeviceObj)
External (\_SB.PC00.PEG3, DeviceObj)
External (\_SB.PC00.RP01, DeviceObj)
External (\_SB.PC00.RP02, DeviceObj)
External (\_SB.PC00.RP03, DeviceObj)
External (\_SB.PC00.RP04, DeviceObj)
External (\_SB.PC00.RP05, DeviceObj)
External (\_SB.PC00.RP06, DeviceObj)
External (\_SB.PC00.RP07, DeviceObj)
External (\_SB.PC00.RP08, DeviceObj)
External (\_SB.PC00.RP09, DeviceObj)
External (\_SB.PC00.RP10, DeviceObj)
External (\_SB.PC00.RP11, DeviceObj)
External (\_SB.PC00.RP12, DeviceObj)
External (\_SB.PC00.RP13, DeviceObj)
External (\_SB.PC00.RP14, DeviceObj)
External (\_SB.PC00.RP15, DeviceObj)
External (\_SB.PC00.RP16, DeviceObj)
External (\_SB.PC00.RP17, DeviceObj)
External (\_SB.PC00.RP18, DeviceObj)
External (\_SB.PC00.RP19, DeviceObj)
External (\_SB.PC00.RP20, DeviceObj)
External (\_SB.PC00.RP21, DeviceObj)
External (\_SB.PC00.RP22, DeviceObj)
External (\_SB.PC00.RP23, DeviceObj)
External (\_SB.PC00.RP24, DeviceObj)
External (\_SB.PC00.RP25, DeviceObj)
External (\_SB.PC00.RP26, DeviceObj)
External (\_SB.PC00.RP27, DeviceObj)
External (\_SB.PC00.RP28, DeviceObj)
External (P8XH, MethodObj)
External (OSUM)
External (\_SB.CAGS, MethodObj)
External (\_SB.SGOV, MethodObj)

External (\_SB.PEPD.DEVY, PkgObj)

External (\_SB.PC00.RTEN, MethodObj)
External (\_SB.PC00.RTDS, MethodObj)
External (\_SB.PC00.RP01.PXP._ON, MethodObj)
External (\_SB.PC00.RP05.PXP._ON, MethodObj)
External (\_SB.PC00.RP09.PXP._ON, MethodObj)
External (\_SB.PC00.RP13.PXP._ON, MethodObj)
External (\_SB.PC00.RP17.PXP._ON, MethodObj)
External (\_SB.PC00.RP21.PXP._ON, MethodObj)
External (\_SB.PC00.RP25.PXP._ON, MethodObj)
External (\_SB.PC00.RP01.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP01.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP05.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP05.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP09.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP09.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP13.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP13.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP17.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP17.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP21.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP21.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP25.RSTG, IntObj) // PERST for DTBT GPIO access
External (\_SB.PC00.RP25.PWRG, IntObj) // CIO RTD3_PWR_EN for DTBT GPIO access
External (\_SB.PC00.RP01.SXWK, MethodObj)
External (\_SB.PC00.RP05.SXWK, MethodObj)
External (\_SB.PC00.RP09.SXWK, MethodObj)
External (\_SB.PC00.RP13.SXWK, MethodObj)
External (\_SB.PC00.RP17.SXWK, MethodObj)
External (\_SB.PC00.RP21.SXWK, MethodObj)
External (\_SB.PC00.RP25.SXWK, MethodObj)

External (\CMSK, IntObj)

//
// AcpiPinDriverLib imports(from DSDT in platform)
//
External (\PIN.STA, MethodObj)
External (\PIN.ON, MethodObj)
External (\PIN.OFF, MethodObj)

Name (TRDO, 0) // 1 during TBT RTD3 _ON
Name (TRD3, 0) // 1 during TBT RTD3 _OFF

Name (TBPE, 0) // Reflects RTD3_PWR_EN value
Name (TOFF, 0) // param to TBT _OFF method
Name (TEDC, 0) // 0 means Assuming no TBT Endpoint Device is connected

Name (DSCE, 0) // 0 = SW CM Disabled (FW CM), 1 = SW CM Enabled

  //
  // Name: TBON
  // Description: Turn On the DTBT PCH PCIE RP Resource
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> PCIE RP Type
  // Return:  0: On Success, 1: On Failure
  //
  Method (TBON, 2, Serialized) {

    If (LEqual (Arg1, PCIE_RP_TYPE_CPU)) {
      Return (1);
    }
    Switch (ToInteger (Arg0)) { // PCH DTBT Root port Selector
      Case (1) {
        If (CondRefOf (\_SB.PC00.RP01.PXP._ON)) {
          \_SB.PC00.RP01.PXP._ON ()
        }
      }

      Case (5) {
        If (CondRefOf (\_SB.PC00.RP05.PXP._ON)) {
          \_SB.PC00.RP05.PXP._ON ()
        }
      }

      Case (9) {
        If (CondRefOf (\_SB.PC00.RP09.PXP._ON)) {
          \_SB.PC00.RP09.PXP._ON ()
        }
      }

      Case (13) {
        If (CondRefOf (\_SB.PC00.RP13.PXP._ON)) {
          \_SB.PC00.RP13.PXP._ON ()
        }
      }

      Case (17) {
        If (CondRefOf (\_SB.PC00.RP17.PXP._ON)) {
          \_SB.PC00.RP17.PXP._ON ()
        }
      }

      Case (21) {
        If (CondRefOf (\_SB.PC00.RP21.PXP._ON)) {
          \_SB.PC00.RP21.PXP._ON ()
        }
      }

      Case (25) {
        If (CondRefOf (\_SB.PC00.RP25.PXP._ON)) {
          \_SB.PC00.RP25.PXP._ON ()
        }
      }

    }//Switch (ToInteger (Arg0)) // TBT Selector
    Return (0)
  } // End of TBON

  //
  // Name: TBOF
  // Description: Turn off the DTBT PCIE RP Resource
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> PCIE RP Type
  // Return: 0: On Success, 1: On Failure
  //
  Method (TBOF, 2, Serialized) {

    If (LEqual (Arg1, PCIE_RP_TYPE_CPU)) {
      Return (1);
    }
    Switch (ToInteger (Arg0)) { // PCH DTBT Root port Selector
      Case (1) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP01.RSTG)) {
          \PIN.ON (\_SB.PC00.RP01.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP01.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP01.PWRG)
        }
      }
      Case (5) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP05.RSTG)) {
          \PIN.ON (\_SB.PC00.RP05.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP05.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP05.PWRG)
        }
      }
      Case (9) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP09.RSTG)) {
          \PIN.ON (\_SB.PC00.RP09.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP09.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP09.PWRG)
        }
      }
      Case (13) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP13.RSTG)) {
          \PIN.ON (\_SB.PC00.RP13.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP13.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP13.PWRG)
        }
      }
      Case (17) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP17.RSTG)) {
          \PIN.ON (\_SB.PC00.RP17.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP17.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP17.PWRG)
        }
      }
      Case (21) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP21.RSTG)) {
          \PIN.ON (\_SB.PC00.RP21.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP21.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP21.PWRG)
        }
      }
      Case (25) {
        //
        // Assert Reset Pin (PERST)
        //
        If (CondRefOf (\_SB.PC00.RP25.RSTG)) {
          \PIN.ON (\_SB.PC00.RP25.RSTG)
        }

        //
        // On S3/S4/S5/RTD3 entry platform should drive PERST# low first wait at least 10ms,
        // and then drive RTD3_POWER low
        //
        Sleep (10)

        //
        // USB4 doesn't support legacy and native mode
        // TBT FW dosen't know if system is in Sx or not
        // BIOS w/a: Power OFF RTD3_PWR_EN for TBT
        //
        If (CondRefOf (\_SB.PC00.RP25.PWRG)) {
          \PIN.OFF (\_SB.PC00.RP25.PWRG)
        }
      }
    }//Switch (ToInteger (Arg0)) // TBT Selector
    Return (0)
  } // End of TBOF

  //
  // Name: DTWE
  // Description: Turn On the DTBT PCH PCIE RP Wake GPIO to wake from Sx
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> PCIE RP Type
  // Return: 0: On Success, 1: On Failure
  //
  Method (DTWE, 2, Serialized) {

    If (LEqual (Arg1, PCIE_RP_TYPE_CPU)) {
      Return (1)
    }
    Switch (ToInteger (Arg0)) { // PCH DTBT Root port Selector
      Case (1) {
        If (CondRefOf (\_SB.PC00.RP01.SXWK)) {
          \_SB.PC00.RP01.SXWK ()
        }
      }

      Case (5) {
        If (CondRefOf (\_SB.PC00.RP05.SXWK)) {
          \_SB.PC00.RP05.SXWK ()
        }
      }

      Case (9) {
        If (CondRefOf (\_SB.PC00.RP09.SXWK)) {
          \_SB.PC00.RP09.SXWK ()
        }
      }

      Case (13) {
        If (CondRefOf (\_SB.PC00.RP13.SXWK)) {
          \_SB.PC00.RP13.SXWK ()
        }
      }

      Case (17) {
        If (CondRefOf (\_SB.PC00.RP17.SXWK)) {
          \_SB.PC00.RP17.SXWK ()
        }
      }

      Case (21) {
        If (CondRefOf(\_SB.PC00.RP21.SXWK)) {
          \_SB.PC00.RP21.SXWK ()
        }
      }

      Case (25) {
        If (CondRefOf(\_SB.PC00.RP25.SXWK)) {
          \_SB.PC00.RP25.SXWK ()
        }
      }

    }//Switch (ToInteger (Arg0)) // TBT Selector
    Return (0)
  } // End of DTWE

  //
  // Name: TBTD
  // Description: Function to return the TBT RP# device no
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  // Return: TBT RP# device no
  //
  Method (TBTD,2,Serialized)
  {
    If (LEqual (Arg1, PCIE_RP_TYPE_PCH)) {
      Switch (ToInteger (Arg0))
      {
        Case (Package () {1, 2, 3, 4, 5, 6, 7, 8})
        {
          Store (0x1C, Local0) //Device28-Function0...Function7 = 11100.000...111
        }
        Case (Package () {9, 10, 11, 12, 13, 14, 15, 16})
        {
          Store (0x1D, Local0) //Device29-Function0...Function7 = 11101.000...111
        }
        Case (Package () {17, 18, 19, 20, 21, 22, 23, 24})
        {
          Store (0x1B, Local0) //Device27-Function0...Function3 = 11011.000...011
        }
        Case (Package () {25, 26, 27, 28})
        {
          Store (0x1A, Local0) //Device26-Function0...Function3 = 11010.000...011
        }
      }
    } ElseIf (LEqual (Arg1, PCIE_RP_TYPE_CPU)) {
      Switch (ToInteger (Arg0))
      {
        Case (Package () {1, 2, 3})
        {
          Store (0x1, Local0) //Device1-Function0...Function2 = 00001.000...010
        }
        Case (Package () {4})
        {
          Store (0x6, Local0) //Device6-Function0 = 00110.000
        }
      }
    } Else {
      Store (0xFF, Local0)
    }


    Return (Local0)
  } // End of Method (TBTD, 1)

  //
  // Name: TBTF
  // Description: Function to return the TBT RP# function no
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  // Return: TBT RP# function no
  //
  Method (TBTF,2,Serialized)
  {
    If (LEqual (Arg1, PCIE_RP_TYPE_PCH)) {
      Switch (ToInteger (Arg0))
      {
        Case (1)
        {
          Store (And (\RPA1, 0xF), Local0) //Device28-Function0 = 11100.000
        }
        Case (2)
        {
          Store (And (\RPA2, 0xF), Local0) //Device28-Function1 = 11100.001
        }
        Case (3)
        {
          Store (And (\RPA3, 0xF), Local0) //Device28-Function2 = 11100.010
        }
        Case (4)
        {
          Store (And (\RPA4, 0xF), Local0) //Device28-Function3 = 11100.011
        }
        Case (5)
        {
          Store (And (\RPA5, 0xF), Local0) //Device28-Function4 = 11100.100
        }
        Case (6)
        {
          Store (And (\RPA6, 0xF), Local0) //Device28-Function5 = 11100.101
        }
        Case (7)
        {
          Store (And (\RPA7, 0xF), Local0) //Device28-Function6 = 11100.110
        }
        Case (8)
        {
          Store (And (\RPA8, 0xF), Local0) //Device28-Function7 = 11100.111
        }
        Case (9)
        {
          Store (And (\RPA9, 0xF), Local0) //Device29-Function0 = 11101.000
        }
        Case (10)
        {
          Store (And (\RPAA, 0xF), Local0) //Device29-Function1 = 11101.001
        }
        Case (11)
        {
          Store (And (\RPAB, 0xF), Local0) //Device29-Function2 = 11101.010
        }
        Case (12)
        {
          Store (And (\RPAC, 0xF), Local0) //Device29-Function3 = 11101.011
        }
        Case (13)
        {
          Store (And (\RPAD, 0xF), Local0) //Device29-Function4 = 11101.100
        }
        Case (14)
        {
          Store (And (\RPAE, 0xF), Local0) //Device29-Function5 = 11101.101
        }
        Case (15)
        {
          Store (And (\RPAF, 0xF), Local0) //Device29-Function6 = 11101.110
        }
        Case (16)
        {
          Store (And (\RPAG, 0xF), Local0) //Device29-Function7 = 11101.111
        }
        Case (17)
        {
          Store (And (\RPAH, 0xF), Local0) //Device27-Function0 = 11011.000
        }
        Case (18)
        {
          Store (And (\RPAI, 0xF), Local0) //Device27-Function1 = 11011.001
        }
        Case (19)
        {
          Store (And (\RPAJ, 0xF), Local0) //Device27-Function2 = 11011.010
        }
        Case (20)
        {
          Store (And (\RPAK, 0xF), Local0) //Device27-Function3 = 11011.011
        }
        Case (21)
        {
          Store (And (\RPAL, 0xF), Local0) //Device27-Function4 = 11011.100
        }
        Case (22)
        {
          Store (And (\RPAM ,0xF), Local0) //Device27-Function5 = 11011.101
        }
        Case (23)
        {
          Store (And (\RPAN, 0xF), Local0) //Device27-Function6 = 11011.110
        }
        Case (24)
        {
          Store (And (\RPAO, 0xF), Local0) //Device27-Function7 = 11011.111
        }
        Case (25)
        {
          Store (And (\RPAP,0xF), Local0) //Device26-Function0 = 11010.001
        }
        Case (26)
        {
          Store (And (\RPAQ,0xF), Local0) //Device26-Function1 = 11010.001
        }
        Case (27)
        {
          Store (And (\RPAR,0xF), Local0) //Device26-Function2 = 11010.010
        }
        Case (28)
        {
          Store (And (\RPAS,0xF), Local0) //Device26-Function3 = 11010.011
        }
      }
    } ElseIf (LEqual (Arg1, PCIE_RP_TYPE_CPU)) {
      Switch (ToInteger (Arg0))
      {
        Case (1)
        {
          Store (0x0, Local0) //Device1-Function0 = 00001.000
        }
        Case (2)
        {
          Store (0x1, Local0) //Device1-Function1 = 00001.001
        }
        Case (3)
        {
          Store (0x2, Local0) //Device1-Function2 = 00001.010
        }
        Case (4)
        {
          Store (0x0, Local0) //Device6-Function0 = 00110.000
        }
      }
    } Else {
      Store (0xFF, Local0)
    }


    Return (Local0)
  } // End of Method(TBTF,1)

  //
  // Name: MMRP
  // Description: Function to return the Pci base address of TBT rootport
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  //

  Method(MMRP, 2, Serialized)
  {
    Store (\_SB.PC00.GPCB (), Local0) // MMIO Base address
    Add (Local0, ShiftLeft (TBTD (Arg0, Arg1), 15), Local0) // Device no
    Add (Local0, ShiftLeft (TBTF (Arg0, Arg1), 12), Local0) // Function no

    Return (Local0)
  } // End of Method (MMRP)

  //
  // Name: MMTB
  // Description: Function to return the Pci base address of TBT Up stream port
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  //
  Method(MMTB, 2, Serialized)
  {

    Store(\_SB.PC00.GPCB(), Local0) // MMIO Base address

    Add(Local0, ShiftLeft(TBTD(Arg0, Arg1), 15), Local0) // Device no
    Add(Local0, ShiftLeft(TBTF(Arg0, Arg1), 12), Local0) // Function no

    OperationRegion (MMMM, SystemMemory, Local0, 0x1A)
    Field (MMMM, AnyAcc, NoLock, Preserve)
    {
      Offset(0x19),
      SBUS, 8
    }
    Store(SBUS, Local2)
    Store(\_SB.PC00.GPCB(), Local0)
    Multiply(Local2, 0x100000, Local2)
    Add(Local2, Local0, Local0) // TBT HR US port


    Return(Local0)
  } // End of Method(MMTB, 1, Serialized)
  //
  // Name: FFTB
  // Description: Function to  Check for FFFF in TBT PCIe
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  // Return: 1 if TBT PCIe space has value FFFF, 0 if not
  //
  Method(FFTB, 2, Serialized)
  {

    Add(MMTB(Arg0, Arg1), 0x548, Local0)
    OperationRegion(PXVD,SystemMemory,Local0,0x08)
    Field(PXVD,DWordAcc, NoLock, Preserve)
    {
      TB2P, 32,
      P2TB, 32
    }

    Store(TB2P, Local1)

    If(LEqual(Local1, 0xFFFFFFFF))
    {
      Return (1)
    }
    Else
    {
      Return (0)
    }
  } // End of Method(FFTB)

  //
  // Name: DTSX
  // Description: DTBT Prepare to Sleep.Sends Go2Sx cmd to TBT RPs.
  // Input: Arg0 -> DTbt Sx Command
  // Input: Arg1 -> PCIE RP Index
  // Input: Arg2 -> PCIE RP Type
  // Return: 1 if Success, 0 if not
  //
  Method (DTSX, 3, Serialized)
  {
    Store (\MMTB (Arg1, Arg2), Local5)
    OperationRegion (TBD1, SystemMemory, Local5, 0x550)// TBT HR PCICFG MMIO
    Field (TBD1, DWordAcc, NoLock, Preserve) {
      Offset (0x548),
      T2P1, 32,
      P2T1, 32
    }
    Store (\MMRP (Arg1, Arg2), Local6)
    OperationRegion (RP0P, SystemMemory, Local6, 0xB0)
    Field (RP0P, WordAcc, NoLock, Preserve)
    {
      Offset (0xA4),// PMCSR
      PS0V, 2, // PowerState value
    }
    Store (PS0V, Local3)
    if (LEqual (Local3, 0x3))
    {
      Store (0, PS0V)
    }

    //
    // Write Go2Sx on RP selected 1
    //
    Store (50, Local1)
    Store (Arg0, P2T1) // Write Go2Sx and PCIE2TBT_VLD_B Command to PCIe2TBT
    While (LGreater (Local1, 0))
    {
      Decrement (Local1)
      Store (T2P1, Local2)
      If ( LAnd (LEqual (Local2, 0xFFFFFFFF), LEqual (Local1, 0x1)))
      {
        // Sleep (1000)
        Return (0)
      }
      If (LAnd (LNotEqual (Local2, 0xFFFFFFFF), And (Local2, 1)))
      {
        Break
      }
      If (LEqual (Local1, 0x01))
      {
        // Sleep (1000)
        ADBG (Concatenate ("Error Code:", ToHexString (Local2))) // Check Go2Sx Error code
        Sleep (1000)
        Return (0)
      }
      Sleep (100)
    }
    Store (0x00, P2T1) // Write 0 to PCIe2TBT

    // Restore previous power state
    Store (Local3, PS0V)
    Return (1)
  }

  //
  // Name: TPTS
  // Description: TBT Prepare to Sleep.Sends Go2Sx cmd to TBT RPs.
  // Input: Arg0 -> System Sx state
  //
  Method (TPTS, 1, Serialized)
  {
    //
    // Prepare Command and store in local variable
    //
    If (LEqual (DTWA, 1)) {
      // Wake Enabled, GO2SX Command
      Store (0x05, Local4)  //Go2Sx and PCIE2TBT_VLD_B command
    } Else {
      // Wake Disabled, GO2SX_NO_WAKE Command
      Store (0x07, Local4)  //Go2Sx with no wake and PCIE2TBT_VLD_B command
    }

    If( LAnd (LNotEqual (RPS0, 0), LNotEqual (RPN0, 0))) { // Return if no root port is selected

      //
      // Bring DTBT 0 to D0 and send Go2Sx command
      //
      If (LEqual (DG2S, 1)) {
        //
        // Call DTBT 0 On process before Sending Go2Sx Command
        //
        TBON (\RPS0, \RPT0)

        DTSX (Local4, \RPS0, \RPT0)
      }
      //
      // Call DTBT 0 Off process After Sending Go2Sx Command
      //
      TBOF (\RPS0, \RPT0)

      //
      // Enable Wake GPIO
      //
      If (LEqual (DTWA, 1)) {
        DTWE (\RPS0, \RPT0)
      }
    }
    If ( LAnd (LNotEqual (RPS1, 0), LNotEqual (RPN1, 0))) { // Return if no root port is selected

      //
      // Bring DTBT 1 to D0 and send Go2Sx command
      //
      If (LEqual (DG2S, 1)) {
        //
        // Call DTBT 1 On process before Sending Go2Sx Command
        //
        TBON (\RPS1, \RPT1)

        DTSX (Local4, \RPS1, \RPT1)
      }
      //
      // Call DTBT 1 Off process After Sending Go2Sx Command
      //
      TBOF (\RPS1, \RPT1)

      //
      // Enable Wake GPIO
      //
      If (LEqual (DTWA, 1)) {
        DTWE (\RPS1, \RPT1)
      }
    }

  }

Scope(\_GPE)
{
  //
  //
  //OS up Mail Box command execution to host router upstream port each time
  //exiting from Sx State .Avoids intermediate
  //PCIe Scan by OS during resorce allocation
  // Arg0 : PCIe Base address
  //Developer notes: Called twice
  // 1. During OS INIT (booting to OS from S3-S5/Reboot)
  // 2. Up on Hot plug
  //
  Method(DTOU, 1, Serialized)
  {

    Add(Arg0, 0x540, Local0)
    OperationRegion(PXVD,SystemMemory,Local0,0x10)
    Field(PXVD,DWordAcc, NoLock, Preserve)
    {
      Offset(0x08),
      DT2P, 32,
      DP2T, 32
    }

    Store(100, Local1)
    Store(0x0D, DP2T) // Write OS_Up to PCIe2TBT
    While(LGreater(Local1, 0))
    {
      Store(Subtract(Local1, 1), Local1)
      Store(DT2P, Local2)
      //Store(TB2P, Local2)
      If(LAnd(LEqual(Local2, 0xFFFFFFFF), LEqual(Local1, 0x1))) // Device gone
      {
        Return(2)
      }
      If(LAnd(LNotEqual(Local2, 0xFFFFFFFF), And(Local2, 1))) // Done
      {
        break
      }
      Sleep(50)
    }
    //Store(0x00, P2TB) // Write 0 to PCIe2TBT


    Return(1)
  } // End of Method(DTOU, 1, Serialized)

  //
  // Check for FFFF in TBT
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  //

  Method(TBFF, 2, Serialized)
  {

    Store(MMTB(Arg0, Arg1), Local0)
    OperationRegion(PXVD,SystemMemory,Local0,0x4)
    Field(PXVD,DWordAcc, NoLock, Preserve)
    {
      VEDI, 32 // Vendor/Device ID
    }
    Store(VEDI, Local1)

    If(LEqual(Local1, 0xFFFFFFFF))
    {
      Return (DTOU(Local0))
    }
    Else
    {
      Return (0)
    }
  } // End of Method(TBFF, 1, Serialized)

  //
  // Pmem of TBT RP
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  //

  Method(TSUP, 2, Serialized)
  {

    Store(\_SB.PC00.GPCB(), Local0) // MMIO Base address

    Add(Local0, ShiftLeft(TBTD(Arg0, Arg1), 15), Local0) // Device no
    Add(Local0, ShiftLeft(TBTF(Arg0, Arg1), 12), Local0) // Function no


    OperationRegion (MMMM, SystemMemory, Local0, 0x30)
    Field (MMMM, AnyAcc, NoLock, Preserve)
    {
      CMDS, 32,
      Offset(0x19),
      SBUS, 8,
      SBU5, 8,
      Offset(0x1C),
      SEIO, 32,
      MMBL, 32,
      PMBL, 32,

    }


    Return(PMBL)
  } // End of Method(TSUP, 0, Serialized)

  //
  // Wait for secondary bus in TBT RP
  // Input: Arg0 -> Tbt Root Port value from Tbt NVS
  // Input: Arg1 -> Tbt port type value from Tbt NVS
  //

  Method(WSUB, 2, Serialized)
  {

    Store(0, Local0)
    Store(0, Local1)
    While(1)
    {
      Store(TSUP(Arg0, Arg1), Local1)
      If(LGreater(Local1, 0x1FFF1))
      {
        Break
      }
      Else
      {
        Add(Local0, 1, Local0)
        If(LGreater(Local0, 1000))
        {
          Sleep(1000)
        }
        Else
        {
          Sleep(16)
        }
      }
    }
  } // End of Method(WSUB)

  // Wait for _WAK finished
  Method(WWAK)
  {

    Wait(WFEV, 0xFFFF)
    Signal(WFEV) // Set it, to enter on next HP
  } // End of Method(WWAK)

  Method(NTFY, 2, Serialized)
  {

    If(LEqual(DTHN,1))
    {
      If (LEqual (Arg1, PCIE_RP_TYPE_PCH)) {
        Switch (ToInteger (Arg0)) // TBT Selector
        {
          Case (1)
          {
            Notify (\_SB.PC00.RP01,0)
          }
          Case (2)
          {
            Notify (\_SB.PC00.RP02,0)
          }
          Case (3)
          {
            Notify (\_SB.PC00.RP03,0)
          }
          Case (4)
          {
            Notify (\_SB.PC00.RP04,0)
          }
          Case (5)
          {
            Notify (\_SB.PC00.RP05,0)
          }
          Case (6)
          {
            Notify (\_SB.PC00.RP06,0)
          }
          Case (7)
          {
            Notify (\_SB.PC00.RP07,0)
          }
          Case (8)
          {
            Notify (\_SB.PC00.RP08,0)
          }
          Case (9)
          {
            Notify(\_SB.PC00.RP09,0)
          }
          Case (10)
          {
            Notify (\_SB.PC00.RP10,0)
          }
          Case (11)
          {
            Notify (\_SB.PC00.RP11,0)
          }
          Case (12)
          {
            Notify (\_SB.PC00.RP12,0)
          }
          Case (13)
          {
            Notify (\_SB.PC00.RP13,0)
          }
          Case (14)
          {
            Notify (\_SB.PC00.RP14,0)
          }
          Case (15)
          {
            Notify (\_SB.PC00.RP15,0)
          }
          Case (16)
          {
            Notify (\_SB.PC00.RP16,0)
          }
          Case (17)
          {
            Notify (\_SB.PC00.RP17,0)
          }
          Case (18)
          {
            Notify (\_SB.PC00.RP18,0)
          }
          Case (19)
          {
            Notify (\_SB.PC00.RP19,0)
          }
          Case (20)
          {
            Notify (\_SB.PC00.RP20,0)
          }
          Case (21)
          {
            Notify (\_SB.PC00.RP21,0)
          }
          Case (22)
          {
            Notify (\_SB.PC00.RP22,0)
          }
          Case (23)
          {
            Notify (\_SB.PC00.RP23,0)
          }
          Case (24)
          {
            Notify (\_SB.PC00.RP24,0)
          }
          Case (25)
          {
            Notify (\_SB.PC00.RP25,0)
          }
          Case (26)
          {
            Notify (\_SB.PC00.RP26,0)
          }
          Case (27)
          {
            Notify (\_SB.PC00.RP27,0)
          }
          Case (28)
          {
            Notify (\_SB.PC00.RP28,0)
          }

        }//Switch(ToInteger(TBSS)) // TBT Selector
      } ElseIf (LEqual(Arg1, PCIE_RP_TYPE_CPU)) {
        Switch (ToInteger (Arg0))
        {
          Case (1)
          {
            Notify (\_SB.PC00.PEG0,0)
          }
          Case (2)
          {
            Notify (\_SB.PC00.PEG1,0)
          }
          Case (3)
          {
            Notify (\_SB.PC00.PEG2,0)
          }
          Case (4)
          {
            If (CondRefOf(\_SB.PC00.PEG3)) {
              Notify (\_SB.PC00.PEG3,0)
            }
          }
        }
      }//Switch(ToInteger(TBSS)) // TBT Selector
    }//If(NOHP())
    P8XH(0,0xC2)
    P8XH(1,0xC2)
  }// End of Method(NTFY)

//
//  TBT BIOS, GPIO 5 filtering,
//  Hot plug of 12V USB devices, into TBT host router, cause electrical noise on PCH GPIOs,
//  This noise cause false hot-plug events, and negatively influence BIOS assisted hot-plug.
//  PCH GPIO does not implement Glitch Filter logic (refer to GPIO HAS) on any GPIO pad. Native functions have to implement their own digital glitch-filter logic
//  if needed. As HW filter was not implemented on PCH, because of that SW workaround should be implemented in BIOS.
//  Register 0x544(Bios mailbox) bit 0 definition:
//  if BIOS reads bit as 1, BIOS will clear the bit and continue normal flow, if bit is 0 BIOS will exit from method
//
  Method(GNIS,2, Serialized)
  {

    If(LEqual(GP5F, 0))
    {
      Return(0)
    }
    //
    // BIOS mailbox command for GPIO filter
    //
    Add(MMTB(Arg0, Arg1), 0x544, Local0)
    OperationRegion(PXVD,SystemMemory,Local0,0x08)

    Field(PXVD,DWordAcc, NoLock, Preserve)
    {
      HPFI, 1,
      Offset(0x4),
      TB2P, 32
    }
    Store(TB2P, Local1)
    ADBG (Concatenate ("TB2P=", ToHexString (Local1)))
    If(LEqual(Local1, 0xFFFFFFFF)) // Disconnect?
    {
      Return(0)
    }
    Store(HPFI, Local2)
    If(LEqual(Local2, 0x01))
    {
      Store(0x00, HPFI)
      Return(0)
    }
    // Any other values treated as a GPIO noise
    Return(1)
  }

  OperationRegion(SPRT,SystemIO, 0xB2,2)
  Field (SPRT, ByteAcc, Lock, Preserve)
  {
    SSMP, 8
  }
  //
  // Method to notify OS about hot plug events
  //
  Method(XTBT,2, Serialized)
  {
    Store(Arg0, DTCP) // Root port to enumerate
    Store(Arg1, DTPT)   // Root port Type
    If(LNotEqual(Arg0, RPS0) && LNotEqual(Arg0, RPS1)) {
      Return ()
    }

    WWAK()
    WSUB(Arg0, Arg1)
    If(GNIS(Arg0, Arg1))
    {
      Return()
    }


    Acquire(OSUM, 0xFFFF)
    Store(TBFF(Arg0, Arg1), Local1)
    If(LEqual(Local1, 1))// Only HR
    {
      Sleep(16)
      Release(OSUM)
      Return ()
    }
    If(LEqual(Local1, 2)) // Disconnect
    {
      NTFY(Arg0, Arg1)
      Sleep(16)
      Release(OSUM)
      Return ()
    }

    NTFY(Arg0, Arg1)
    Sleep(16)
    Release(OSUM)

  } // End of Method(XTBT)

  //
  // Method to Init dTBT PCIE RP
  //
  Method (DTIN, 0, Serialized)
  {
    If (LEqual (RPN0, 0x01)) {
      DINI (RPS0, RPT0)
    }
    If (LEqual (RPN1, 0x01)) {
      DINI (RPS1, RPT1)
    }
  }
  //
  // Method to call OSPU Mail box command
  // Arg0 : TBT RP Selector / DMA
  // Arg1 : TBT Type (PCH or PEG)
  //
  Method(DINI, 2, Serialized)
  {
    Store (MMRP (Arg0, Arg1), Local0) //Function to return the Pci base address of TBT rootport
    OperationRegion (RP_X,SystemMemory,Local0,0x20)
    Field (RP_X, DWordAcc, NoLock, Preserve)
    {
      Offset(0x18),
      PBCR, 32         // P2P Bridge BUS Configuration Register
    }
    Field (RP_X, AnyAcc, NoLock, Preserve)
    {
      Offset(0x19),
      SBUS, 8
    }
    Store (PBCR, Local1)
    If (LOr (LEqual (SBUS, 0), LEqual (SBUS, 0xFF))) {
      Store (0x00282800, PBCR)
    } Else {
    }
    Store (MMTB (Arg0, Arg1), Local2)
    DTOU (Local2)
    Store (Local1, PBCR)
  }

} // End of Scope (\_GPE)


If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 1),LEqual(RPS1, 1))))
{
  Scope(\_SB.PC00.RP01)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP01)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 2),LEqual(RPS1, 2))))
{
  Scope(\_SB.PC00.RP02)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP02)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 3),LEqual(RPS1, 3))))
{
  Scope(\_SB.PC00.RP03)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP03)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 4),LEqual(RPS1, 4))))
{
  Scope(\_SB.PC00.RP04)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP04)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 5),LEqual(RPS1, 5))))
{
  Scope(\_SB.PC00.RP05)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP05)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 6),LEqual(RPS1, 6))))
{
  Scope(\_SB.PC00.RP06)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP06)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 7),LEqual(RPS1, 7))))
{
  Scope(\_SB.PC00.RP07)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP07)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 8),LEqual(RPS1, 8))))
{
  Scope(\_SB.PC00.RP08)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP08)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 9),LEqual(RPS1, 9))))
{
  Scope(\_SB.PC00.RP09)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP09)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 10),LEqual(RPS1, 10))))
{
  Scope(\_SB.PC00.RP10)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP10)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 11),LEqual(RPS1, 11))))
{
  Scope(\_SB.PC00.RP11)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP11)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 12),LEqual(RPS1, 12))))
{
  Scope(\_SB.PC00.RP12)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP12)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 13),LEqual(RPS1, 13))))
{
  Scope(\_SB.PC00.RP13)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP13)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 14),LEqual(RPS1, 14))))
{
  Scope(\_SB.PC00.RP14)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP14)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 15),LEqual(RPS1, 15))))
{
  Scope(\_SB.PC00.RP15)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP15)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 16),LEqual(RPS1, 16))))
{
  Scope(\_SB.PC00.RP16)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP16)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 17),LEqual(RPS1, 17))))
{
  Scope(\_SB.PC00.RP17)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP17)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 18),LEqual(RPS1, 18))))
{
  Scope(\_SB.PC00.RP18)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP18)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 19),LEqual(RPS1, 19))))
{
  Scope(\_SB.PC00.RP19)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP19)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 20),LEqual(RPS1, 20))))
{
  Scope(\_SB.PC00.RP20)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.RP20)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 21),LEqual(RPS1, 21))))
{
  Scope(\_SB.PC00.PEG0)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.PEG0)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 22),LEqual(RPS1, 22))))
{
  Scope(\_SB.PC00.PEG1)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.PEG1)
}

If(LAnd(LEqual(\DTFS, 1),LOr(LEqual(RPS0, 23),LEqual(RPS1, 23))))
{
  Scope(\_SB.PC00.PEG2)
  {
    Device(HRUS)// Host router Upstream port
    {
      Name(_ADR, 0x00000000)

      Method(_RMV)
      {
        Return(DTAR)
      } // end _RMV
    }
  }//End of Scope(\_SB.PC00.PEG2)
}

//
// Name: STPC
// Description: Function to Set TBT PEP Constraint of TBT rootport in DEVY PEP Package
// Input: Arg0 -> Starting Index from where TBT PEP Entry can be added in DEVY PEP Package
// Input: Arg1 -> Max Possible entry or No of element in DEVY PEP Package
//
Method (STPC, 2, Serialized) {
  Name (DEVI, 0)  // Device Index
  Name (MENT, 2)  // Max Entry Needed for TBT
  Store (Arg0, DEVI)

  // Check if DEVY boundary is not crossing
  if (LGreater (Add (DEVI, MENT), Arg1)) {
      // DEVY Boundary crossing. So not sufficient Space for TBT. Return FAIL
      Return (0)
  }
  Store (0, Local0) // Intialise Valid PCH/PEG RP to 0
  If (LAnd (LGreater (RPS0, 0), RPN0)) // TBT1 RP is enable and TBT1 RP Must be Greater Than 0
  {
    If (LEqual (RPT0, PCIE_RP_TYPE_PCH)) {
      If (LLess (RPS0, 10))
      {
        Store (Concatenate("RP0", ToDecimalString(RPS0)), Local1)
        Store (1, Local0) // Initialise Valid PCH RP to 1
      } Else {
        If (LLess (RPS0, 29))
        {
          Store (Concatenate("RP", ToDecimalString(RPS0)), Local1)
          Store (1, Local0) // Initialise Valid PCH RP to 1
        }
      }
    }
    If (LEqual (RPT0, PCIE_RP_TYPE_CPU)) {
      If (LLess (RPS0, MAX_PEG_RP))
      {
        Store (Concatenate("PEG", ToDecimalString(RPS0)), Local1)
        Store (1, Local0) // Initialise Valid PEG RP to 1
      }
    }
  }
  If (LEqual (Local0, 1)) // If Valid PEH/PEG RP is passed
  {
    Store (Concatenate("\\_SB.PC00.", Local1), Index (DeRefOf(Index (\_SB.PEPD.DEVY, DEVI)), 0)) // Arg0 - TBT RPxx
    Store (0x01, Index (DeRefOf(Index (\_SB.PEPD.DEVY, DEVI)), 1)) // Arg0 - TBT RPxx
    Increment (DEVI)
  }

  // For TBT2 Controller Support
  Store (0, Local0) // Intialise Valid PCH/PEG RP to 0
  If (LAnd (LGreater (RPS1, 0), RPN1)) // TBT2 RP is enable and TBT2 RP Must be Greater Than 0
  {
    If (LEqual (RPT1, PCIE_RP_TYPE_PCH)) {
      If (LLess (RPS1, 10))
      {
        Store (Concatenate("RP0", ToDecimalString(RPS1)), Local1)
        Store (1, Local0) // Initialise Valid PCH RP to 1
      } Else {
        If (LLess (RPS1, 29))
        {
          Store (Concatenate("RP", ToDecimalString(RPS1)), Local1)
          Store (1, Local0) // Initialise Valid PCH RP to 1
        }
      }
    }
    If (LEqual (RPT1, PCIE_RP_TYPE_CPU)) {
      If (LLess (RPS1, MAX_PEG_RP))
      {
        Store (Concatenate("PEG", ToDecimalString(RPS1)), Local1)
        Store (1, Local0) // Initialise Valid PEG RP to 1
      }
    }
  }
  If (LEqual (Local0, 1)) // If Valid PCH/PEG RP is passed
  {
    Store (Concatenate("\\_SB.PC00.", Local1), Index (DeRefOf(Index (\_SB.PEPD.DEVY, DEVI)), 0)) // (Arg0 + 2) - TBT2 RPxx
    Store (0x01, Index (DeRefOf(Index (\_SB.PEPD.DEVY, DEVI)), 1)) // (Arg0 + 2) - TBT2 RPxx
    Increment (DEVI)
  }
  Return (1)
} // End of Method (STPC, 2, Serialized)

//
// Name: TVCF
// Description: Function to return the TBT Variable Value to be Stored After Calculation for TBT Controller
// Input: Arg0 -> Tbt Controller UID number
// Input: Arg1 -> Value to be Operated on TBT Variable
// Input: Arg2 -> TBT Controller Data Width in BITS for the Current Variable
// Input: Arg3 -> TBT Variable Value, On which operation will be conducted.
// Return: TBT Variable Value to be Stored After Calculation
//
Method(TVCF,4)
{
  // Error Handling if Invalid TUID is passed
  If (LGreaterEqual (Arg0, TCPN)) {
    Return (Arg3)
  }

  // Error Handling if Invalid TBT BITS size is Passed
  Multiply (TCPN, Arg2, Local3) // Get TBT Variable Maximum Size in BITS
  If (LGreater (Local3, 32)) {
    Return (Arg3)
  }

  // Get the Starting/Base BIT address of TBT Controller Data in Current Variable
  Multiply (Arg0, Arg2, Local0)

  // Get the Mask Value for TBT
  ShiftLeft (0x1, Arg2, Local1)
  Decrement (Local1)

  // Mask The Value to be operated to Avoid Boundary Crossing
  And (Arg1, Local1, Local2)
  ShiftLeft (Local2, Local0, Local2)
  ShiftLeft (Local1, Local0, Local1)

  // Invert To get Mask Value to Clear TBT Controller Specific BITS in Current Variable
  Not (Local1, Local1)
  // Mask or Clear the Existing TBT Controller Specific BITS value in Current Variable
  And (Arg3, Local1, Local1)
  // Assign new TBT Controller Specific BITS value in Current Variable.
  Or (Local1, Local2, Local2)

  Return (Local2)
} // End of Method(TVCF,4)

//
// Name: TVRF
// Description: Function to return the TBT Controller DATA Value in Passed Variable
// Input: Arg0 -> Tbt Controller UID number
// Input: Arg1 -> TBT Controller Data Width in BITS for the Current Variable
// Input: Arg2 -> TBT Variable Value, On which operation will be conducted.
// Return: TBT Controller Data Value After Calculation
//
Method(TVRF,3)
{
  // Error Handling if Invalid TUID is passed
  If (LGreaterEqual (Arg0, TCPN)) {
    Return (0XFFFF)
  }

  // Error Handling if Invalid TBT BITS size is Passed
  Multiply (TCPN, Arg1, Local3) // Get TBT Variable Maximum Size in BITS
  If (LGreater (Local3, 32)) {
    Return (0XFFFF)
  }

  // Get the Starting/Base BIT address of TBT Controller Data in Current Variable
  Multiply (Arg0, Arg1, Local0)

  // Get the Mask Value for TBT
  ShiftLeft (0x1, Arg1, Local1)
  Decrement (Local1)

  // Read TBT Controller Specific DATA from Passed Variable
  ShiftRight (Arg2, Local0, Local2)
  And (Local2, Local1, Local2)

  Return (Local2)
} // End of Method(TVRF,3)

//
// Name: DTMB
// Description: Send mailbox command to discrete USB4 host router
// Arguments:
// Arg0 - Mailbox command to be sent
// Arg1 - Mailbox argument
// Arg2 - Root port number of discrete USB4 host router
// Arg3 - Root port type of discrete USB4 host router
// Return: 0 = Success. 0xFF = Failure
// Local0 - The PCI base address of upstream port for sending mailbox
// Local1 - Command value set to P2TB register
// Local2 - Command done timeout counter
// Local3 - Command clear timeout counter
//
Method (DTMB, 4, Serialized)
{
  ADBG (Concatenate ("DTMB - MB command  = ", ToHexString (Arg0)))
  ADBG (Concatenate ("DTMB - MB argument = ", ToHexString (Arg1)))
  Store (MMTB (Arg2, Arg3), Local0)
  ADBG (Concatenate ("DTMB - TB2P offset = ", ToHexString (Local0)))
  OperationRegion (DTOP, SystemMemory, Local0, 0x550)
  Field (DTOP, DWordAcc, NoLock, Preserve) {
    DIVI, 32,
    CMDR, 32,
    Offset (0x84),
    TBPS, 2, // PowerState of TBT
    Offset (0x548),
    TB2P, 32,
    P2TB, 32
  }

  If (LNotEqual (DIVI, 0xFFFFFFFF)) {
  }

  //
  // Send mailbox command via P2TB
  // Bit 0    - Valid
  // Bit 7:1  - Mailbox command
  // Bit 31:8 - Command argument
  //
  Or (Arg0, PCIE2TBT_VLD_B, Local1)
  Or (Local1, ShiftLeft (Arg1, 8), Local1)
  Store (Local1, P2TB)
  //
  // Wait for command done bit set
  //
  Store (SET_CM_TIMEOUT_IN_MS, Local2)
  ADBG ("Wait for TB2P command done bit set")
  While (LGreater (Local2, 0)) {
    If (And (TB2P, TBT2PCIE_DON_R)) {
      Break
    }
    Decrement (Local2)
    Sleep (1)
  }
  If (LEqual (Local2, 0)) {
    ADBG (Concatenate ("Command Timeout, timeout value = ", SET_CM_TIMEOUT_IN_MS))
  }

  //
  // Clear command
  //
  Store (0, P2TB)
  //
  // Wait for command done bit clear
  //
  Store (SET_CM_TIMEOUT_IN_MS, Local3)
  ADBG ("Wait for TB2P command done bit cleared")
  While (LGreater (Local3, 0)) {
    If (LEqual (And (TB2P, TBT2PCIE_DON_R), 0)) {
      Break
    }
    Decrement (Local3)
    Sleep (1)
  }
  If (LEqual (Local3, 0)) {
    ADBG (Concatenate ("Command clear timeout, timeout value = ", SET_CM_TIMEOUT_IN_MS))
  }

  //
  // Return cmd sent success if command done bit is set for mailbox command sent
  //
  If (LGreater (Local2, 0)) {
    Return (0)
  } Else {
    Return (0xFF)
  }
}

//
// Name: DCCM
// Description: Send mailbox command to switch CM mode of discrete USB4 host router
// Arguments:
// Arg0 - CM mode value (0: FW CM, 1: SW CM)
// Arg1 - Root port number of discrete USB4 host router
// Arg2 - Root port type of discrete USB4 host router
// Return: 0 = Success. 0xFF = Failure
// Local0 - Single mailbox command execution status
//
Method (DCCM, 3, Serialized)
{
  If (LEqual (Arg0, 1)) {
    ADBG ("DCCM - Switch discrete USB4 host router to SW CM mode")
    Store (DTMB (PCIE2TBT_PASS_THROUGH_MODE, 0, Arg1, Arg2), Local0)
  } Else {
    ADBG ("DCCM - Switch discrete USB4 host router to FW CM mode")
    Store (DTMB (PCIE2TBT_FIRMWARE_CM_MODE, 0, Arg1, Arg2), Local0)
    If (LNotEqual (Local0, 0)) {
      Return (0xFF)
    }
    Store (DTMB (PCIE2TBT_BOOT_ON, 0, Arg1, Arg2), Local0)
  }
  Return (Local0)
}

//
// Name: DSCM
// Description: Set CM mode of discrete USB4 host router to OS preferred CM mode
// Arguments:
// Arg0 - OS USB4 native support state, the CM mode to be applied (0 = FW CM, 1 = SW CM)
// Arg1 - CM mode information in BIOS phase (Bit 7 - Valid, Bit 6:4 - CM mode option, Bit 3:0 - CM mode in BIOS phase)
// Arg2 - DTbt host router index
// Return : CM mode applied for OS phase (0 = FW CM, 1 = SW CM, 0xFF = Failure)
//
// Local0 - Pre-boot CM mode value (0 = FW CM, 1 = SW CM)
// Local1 - Indicate CM mode switch is required or not
//
Method (DSCM, 3, Serialized)
{
  ADBG (Concatenate ("DSCM - DTBT Index = ", ToDecimalString (Arg2)))

  //
  // Lower 3 bits of Arg1 indicate pre-boot CM mode value
  //
  And (Arg1, 0x07, Local0)

  //
  // Switch CM mode by default
  // It is not required only if pre-boot CM mode aligns with OS preferred CM mode
  //
  Store (1, Local1)

  //
  // U4CM Bit Definition
  // Bit 7   - Valid bit
  // Bit 6:4 - Setup CM value (0 = FW CM, 1 = SW CM, 2 = OS, 3 = Pass Through)
  // Bit 3   - Reserved
  // Bit 2:0 - Pre-boot CM mode (0 = FW CM, 1 = SW CM)
  //
  If (LNotEqual (And (Arg1, 0x80), 0x80))
  {
    ADBG ("USB4 CM mode info valid bit is not set!")
    Return (0xFF)
  }
  Else
  {
    //
    // CM mode switch is not required if pre-boot CM mode aligns with OS preferred CM mode
    //
    Store (Local0, DSCE)  // Save pre-boot CM setting to DSCE
    If (LEqual (Arg0, Local0))
    {
      ADBG ("Pre-boot CM mode aligns with OS preferred CM mode")
      Store (0, Local1)
    }
  }

  //
  // Send mailbox command to switch CM mode if pre-boot CM mode doesn't align with OS preferred CM mode
  //
  If (LEqual (Local1, 1))
  {
    //
    // Switch CM mode to OS preferred CM mode
    //
    If (LEqual (Arg2, 0)) {
      If (LAnd (LNotEqual (RPS0, 0), LNotEqual (RPN0, 0))) {
        ADBG (Concatenate ("DTbt 0 switch CM mode with requested mode = ", Arg0))
        //
        // Send switch CM command to dTBT0
        //
        If (LEqual (DCCM (Arg0, RPS0, RPT0), 0))
        {
          ADBG (Concatenate ("DTbt 0 switch CM mode success, requested mode = ", Arg0))
          Store (Arg0, DSCE)  // Update current CM setting to DSCE
        }
        Else
        {
          ADBG (Concatenate ("DTbt 0 switch CM mode failure, requested mode = ", Arg0))
          Return (0xFF)
        }
      }
    } ElseIf (LEqual (Arg2, 1)) {
      If (LAnd (LNotEqual (RPS1, 0), LNotEqual (RPN1, 0))) {
        ADBG (Concatenate ("DTbt 1 switch CM mode with requested mode ", Arg0))
        //
        // Send switch CM command to dTBT1
        //
        If (LEqual (DCCM (Arg1, RPS1, RPT1), 0))
        {
          ADBG (Concatenate ("DTbt 1 switch CM mode success, requested mode = ", Arg0))
          Store (Arg0, DSCE)  // Update current CM setting to DSCE
        }
        Else
        {
          ADBG (Concatenate ("DTbt 1 switch CM mode failure, requested mode = ", Arg0))
          Return (0xFF)
        }
      }
    }
  }
  ADBG (Concatenate ("DSCM - DSCE = ", ToHexString (DSCE)))
  Return (DSCE)
}

#define CM_MASK_DTBT_0              0x10
#define CM_MASK_DTBT_1              0x20

//
// Name: DTCM
// Description: Set CM mode of all discrete USB4 host routers to the specified CM mode
// Arguments:
// Arg0 - CM mode to be applied (0 = FW CM, 1 = SW CM)
// Arg1 - CM mode information in BIOS phase (Bit 7 - Valid, Bit 6:4 - CM mode option, Bit 3:0 - CM mode in BIOS phase)
// Return : CM mode applied for OS phase (0 = FW CM, 1 = SW CM, 0xFF = Failure)
//
// Local0 - CM mode that has been applied to USB4 host router
//          (0 = FW CM, 1 = SW CM, 0xFF = CM mode is not applied to any host router)
// Local1 - Save CM mode returned by DSCM function for later use
//
Method (DTCM, 2, Serialized)
{
  ADBG (Concatenate ("DTCM - Requested CM mode = ", ToHexString (Arg0)))
  ADBG (Concatenate ("DTCM - Pre-boot CM Info  = ", ToHexString (Arg1)))

  If (LNot (CondRefOf (CMSK))) {
    ADBG ("DTCM - CMSK is not present!")
    Return (0xFF)
  }
  Store (0xFF, Local0)
  //
  // Send switch CM command if dTBT 0 bit is set in HR mask
  //
  If (And (CMSK, CM_MASK_DTBT_0)) {
    Store (DSCM (Arg0, Arg1, 0), Local1)
    If (LNotEqual (Local1, 0xFF)) {
      ADBG (Concatenate ("DTCM - Apply CM mode to dTBT 0 successfully, CM mode = ", ToHexString (Local1)))
      Store (Local1, Local0)
    } Else {
      ADBG (Concatenate ("DTCM - Fail to apply CM mode to dTBT 0, CM mode = ", ToHexString (Arg0)))
      Return (0xFF)
    }
  }
  //
  // Send switch CM command if dTBT 1 bit is set in HR mask
  //
  If (And (CMSK, CM_MASK_DTBT_1)) {
    Store (DSCM (Arg0, Arg1, 1), Local1)
    If (LNotEqual (Local1, 0xFF)) {
      ADBG (Concatenate ("DTCM - Apply CM mode to dTBT 1 successfully, CM mode = ", ToHexString (Local1)))
      Store (Local1, Local0)
    } Else {
      ADBG (Concatenate ("DTCM - Fail to apply CM mode to dTBT 1, CM mode = ", ToHexString (Arg0)))
      Return (0xFF)
    }
  }
  Return (Local0)
}
