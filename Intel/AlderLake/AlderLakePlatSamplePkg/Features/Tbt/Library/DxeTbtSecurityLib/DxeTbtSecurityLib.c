/** @file
  TBT Security Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <Library/DxeTbtSecurityLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Pi/PiHob.h>
#include <Library/HobLib.h>
#include <Library/PostCodeLib.h>
#include <PlatformPostCode.h>
#include <Register/SaPcieDmiRegs.h>
#include <TbtMailBoxCmdDefinition.h>

/**
  Get or set Thunderbolt(TM) security mode

  @param[in]  SecurityMode        - TBT Security Level
  @param[in]  DTbtController      - Enable/Disable DTbtController
  @param[in]  MaxControllerNumber - Number of contorller
  @param[in]  Action              - 0 = get, 1 = set

  @retval     0 or 5              - Return security level
  @retval     0xFF                - if error, return 0xFF
**/
UINT8
EFIAPI
GetSetSecurityMode (
  IN UINT8                       SecurityMode,
  IN UINT8                       *DTbtController,
  IN UINT8                       MaxControllerNumber,
  IN UINT8                       Action
)
{
  EFI_STATUS                  Status;
  UINT8                       HostRouterBus;
  UINTN                       RpDev;
  UINTN                       RpFunc;
  UINTN                       PegBaseAddress;
  UINT8                       Vcn;
  UINT32                      MaxLoopCount;
  UINT8                       Index;
  UINT8                       ReturnFlag;
  DTBT_INFO_HOB               *DTbtInfoHob;
  BOOLEAN                     EnableResetBridge;
  UINT32                      Timeout;
  UINT16                      DeviceId;

  DEBUG ((DEBUG_INFO, "GetSetSecurityMode() Start\n"));
  ReturnFlag = 0xFF;
  DeviceId = 0xFFFF;
  EnableResetBridge = FALSE;

  if (MaxControllerNumber > MAX_DTBT_CONTROLLER_NUMBER) {
    DEBUG ((DEBUG_ERROR, "Over Max DTBT controller number\n"));
    return ReturnFlag;
  }

  DTbtInfoHob = (DTBT_INFO_HOB *) GetFirstGuidHob (&gDTbtInfoHobGuid);
  if (DTbtInfoHob == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to get DTbtInfoHob\n"));
    return ReturnFlag;
  }

  for (Index = 0; Index < MaxControllerNumber; Index++) {
    if (DTbtController[Index] == 0x01) {
      Status = GetDTbtRpDevFun (DTbtInfoHob->DTbtControllerConfig[Index].Type,
                                DTbtInfoHob->DTbtControllerConfig[Index].PcieRpNumber - 1,
                                &RpDev,
                                &RpFunc
                                );
      ASSERT_EFI_ERROR (Status);

      HostRouterBus = PciSegmentRead8 (PCI_SEGMENT_LIB_ADDRESS (0, 0x00, (UINT32)RpDev, (UINT32)RpFunc, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET));
      DEBUG ((DEBUG_INFO, "GetSetSecurityMode(), TBT_US_BUS = 0x%x \n", HostRouterBus));

      if (HostRouterBus == 0) {
        //
        // Set Sec/Sub buses to a temporary value
        //
        PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET), FixedPcdGet8 (PcdTbtTempBusNumber));
        PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET), FixedPcdGet8 (PcdTbtTempBusNumber));
        HostRouterBus = FixedPcdGet8 (PcdTbtTempBusNumber);
        DEBUG ((DEBUG_INFO, "HostRouterBus is set to a temporary bus number: 0x%x", HostRouterBus));
        EnableResetBridge = TRUE;
      }

      if (DTbtInfoHob->DTbtControllerConfig[Index].Type == PCIE_RP_TYPE_CPU) {
        PegBaseAddress = PCI_SEGMENT_LIB_ADDRESS (0, SA_PEG_BUS_NUM, (UINT32)RpDev, (UINT32)RpFunc, 0);
        if (((PciSegmentRead8 (PegBaseAddress + R_SA_PEG_LCTL_OFFSET)) & BIT4) == BIT4) {
          PciSegmentAnd8 (PegBaseAddress + R_SA_PEG_AFEOVR_OFFSET, (UINT8)~BIT4);
          PciSegmentAnd8 (PegBaseAddress + R_SA_PEG_AFEOVR_OFFSET, (UINT8)~BIT5);
          PciSegmentAnd8 (PegBaseAddress + R_SA_PEG_CMNSPARE_OFFSET, (UINT8)~BIT3);
          PciSegmentAnd8 (PegBaseAddress + R_SA_PEG_LCTL_OFFSET, (UINT8)~BIT4);
          PciSegmentOr8 (PegBaseAddress + 0x508, BIT1);
          Vcn = (PciSegmentRead8 (PegBaseAddress + R_SA_PEG_VC0RSTS_OFFSET) & BIT1);
          MaxLoopCount = 16;
          DEBUG ((DEBUG_INFO, "GetSetSecurityMode(), TBT SETUP:polling for Vcn before %x \n", Vcn));
          while (MaxLoopCount-- > 0) {
            if (Vcn == 0) {
              break;
            }
            gBS->Stall (2000);
          }
        }
      }

      //
      // Check device id
      //
      for (Timeout = 0; Timeout < 500; Timeout++) {
        DeviceId = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (0, HostRouterBus, 0, 0, PCI_DEVICE_ID_OFFSET));
        if (DeviceId != 0xFFFF) {
          break;
        }
        gBS->Stall (1000);
      }

      if (IsTbtHostRouter (DeviceId) == TRUE) {
        PostCode (GET_SET_TBT_SEC_LEVEL_ENTRY);

        if (Action == GET_TBT_SECURITY_MODE) {
          ReturnFlag = GetSecLevel (FixedPcdGet16 (PcdDTbtToPcieRegister), FixedPcdGet16 (PcdPcieToDTbtRegister), HostRouterBus, 0, 0, PCIE2TBT_GET_SECURITY_LEVEL, TBT_5S_TIMEOUT);
        } else {
          if (SetSecLevel (SecurityMode, FixedPcdGet16(PcdDTbtToPcieRegister), FixedPcdGet16 (PcdPcieToDTbtRegister), HostRouterBus, 0, 0, PCIE2TBT_SET_SECURITY_LEVEL, TBT_5S_TIMEOUT)) {
            ReturnFlag = SecurityMode;
          }
        }

        PostCode (GET_SET_TBT_SEC_LEVEL_EXIT);
      }

      if (EnableResetBridge == TRUE) {
        //
        // Restore Sec/Sub buses to original value
        //
        PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET), 0x00);
        PciSegmentWrite8 (PCI_SEGMENT_LIB_ADDRESS (0, 0, RpDev, RpFunc, PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET), 0x00);
        EnableResetBridge = FALSE;
        DEBUG ((DEBUG_INFO, "Restore Sec/Sub buses to original value\n"));
      }

    }
  }

  DEBUG ((DEBUG_INFO, "ReturnFlag is %x\n", ReturnFlag));
  DEBUG ((DEBUG_INFO, "GetSetSecurityMode() End\n"));

  return ReturnFlag;
}
