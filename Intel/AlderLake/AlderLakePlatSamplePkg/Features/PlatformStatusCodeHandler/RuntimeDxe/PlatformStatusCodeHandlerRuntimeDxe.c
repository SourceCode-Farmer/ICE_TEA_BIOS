//
// This file contains 'Framework Code' and is licensed as such
// under the terms of your license agreement with Intel or your
// vendor.  This file may not be modified, except as allowed by
// additional terms of your license agreement.
//
/** @file
  Platform status code implementation.

Copyright (c) 2010 - 2020, Intel Corporation. All rights reserved.<BR>
This software and associated documentation (if any) is furnished
under a license and may only be used or copied in accordance
with the terms of the license. Except as permitted by such
license, no part of this software or documentation may be
reproduced, stored in a retrieval system, or transmitted in any
form or by any means without the express written consent of
Intel Corporation.

**/

#include "PlatformStatusCodeHandlerRuntimeDxe.h"
#include <Library/FspErrorInfoLib.h>

EFI_RSC_HANDLER_PROTOCOL  *mRscHandlerProtocol       = NULL;

/**
  ReportStatusCode worker for FSP Error Information.

  @param  CodeType         Always (EFI_ERROR_CODE | EFI_ERROR_UNRECOVERED)
  @param  Value            Always 0
  @param  Instance         Always 0
  @param  CallerId         This optional parameter may be used to identify the caller.
                           It may be used to identify which internal component of the FSP
                           was executing at the time of the error.
  @param  Data             This data contains FSP error type and status code.

  @retval EFI_SUCCESS      Show error status sent by FSP successfully.
  @retval RETURN_ABORTED   Function skipped as unrelated.

**/
EFI_STATUS
EFIAPI
FspErrorStatusCodeReportWorkerDxe (
  IN EFI_STATUS_CODE_TYPE       CodeType,
  IN EFI_STATUS_CODE_VALUE      Value,
  IN UINT32                     Instance,
  IN EFI_GUID                   *CallerId,
  IN EFI_STATUS_CODE_DATA       *Data OPTIONAL
  )
{
  EFI_STATUS Ret;

  Ret = FspErrorStatusCodeReportWorker (CodeType, Value, Instance, (CONST EFI_GUID *) CallerId, (CONST EFI_STATUS_CODE_DATA *) Data);

  //
  // If register an unregister function of gEfiEventExitBootServicesGuid,
  // then some log called in ExitBootServices() will be lost,
  // so unregister the handler after receive the value of exit boot service.
  //
  if ((CodeType & EFI_STATUS_CODE_TYPE_MASK) == EFI_PROGRESS_CODE &&
      Value == (EFI_SOFTWARE_EFI_BOOT_SERVICE | EFI_SW_BS_PC_EXIT_BOOT_SERVICES)) {
    if (PcdGet8 (PcdFspModeSelection) == 0) {
      mRscHandlerProtocol->Unregister (FspErrorStatusCodeReportWorkerDxe);
    }
  }

  return Ret;
}

/**
  Unregister status code callback functions only available at boot time from
  report status code router when exiting boot services.

**/
VOID
EFIAPI
UnregisterUsb3BootTimeHandler (
  VOID
  )
{
  if (PcdGetBool (PcdStatusCodeUseUsb3)) {
    mRscHandlerProtocol->Unregister (Usb3StatusCodeReportWorker);
  }
}

/**
  Unregister status code callback functions only available at boot time from
  report status code router when exiting boot services.

**/
VOID
EFIAPI
UnregisterSerialIoUartBootTimeHandler (
  VOID
  )
{
  if (PcdGetBool (PcdStatusCodeUseSerialIoUart)) {
    mRscHandlerProtocol->Unregister (SerialIoUartStatusCodeReportWorker);
  }
}

/**
  Register status code callback function only when Report Status Code protocol
  is installed.

  @param  Event         Event whose notification function is being invoked.
  @param  Context       Pointer to the notification function's context, which is
                        always zero in current implementation.

**/
VOID
EFIAPI
RegisterBootTimeHandlers (
  IN EFI_EVENT        Event,
  IN VOID             *Context
)
{
  EFI_STATUS                 Status;
  UINT8                      StatusCodeFlags;

  Status = gBS->LocateProtocol (
                  &gEfiRscHandlerProtocolGuid,
                  NULL,
                  (VOID **) &mRscHandlerProtocol
                  );
  ASSERT_EFI_ERROR (Status);

  StatusCodeFlags = GetDebugInterface ();

  if (PcdGetBool (PcdStatusCodeUseUsb3)) {
    if (StatusCodeFlags & STATUS_CODE_USE_USB3) {
      Status = Usb3DebugPortInitialize ();
      ASSERT_EFI_ERROR (Status);
    }
    mRscHandlerProtocol->Register (Usb3StatusCodeReportWorker, TPL_HIGH_LEVEL);
  }

  if (PcdGetBool (PcdStatusCodeUseSerialIoUart)) {
    if (StatusCodeFlags & STATUS_CODE_USE_SERIALIO) {
      SerialIoUartDebugInit ();
    }
    mRscHandlerProtocol->Register (SerialIoUartStatusCodeReportWorker, TPL_HIGH_LEVEL);
  }

  if (PcdGet8 (PcdFspModeSelection) == 0) {
    mRscHandlerProtocol->Register (FspErrorStatusCodeReportWorkerDxe, TPL_HIGH_LEVEL);
  }
}

/**
  Entry point of DXE Status Code Driver.

  This function is the entry point of this DXE Status Code Driver.
  It initializes registers status code handlers, and registers event for EVT_SIGNAL_VIRTUAL_ADDRESS_CHANGE.

  @param  ImageHandle       The firmware allocated handle for the EFI image.
  @param  SystemTable       A pointer to the EFI System Table.

  @retval EFI_SUCCESS       The entry point is executed successfully.

**/
EFI_STATUS
EFIAPI
StatusCodeHandlerRuntimeDxeEntry (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                Status;
  EFI_EVENT                 RegisterStatusCodeHandlerEvent;
  VOID                      *Registration;

  Status = gBS->LocateProtocol (
                  &gEfiRscHandlerProtocolGuid,
                  NULL,
                  (VOID **) &mRscHandlerProtocol
                  );

  if (!EFI_ERROR (Status)) {
    RegisterBootTimeHandlers (NULL, NULL);
  } else {
    Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_NOTIFY,
                    RegisterBootTimeHandlers,
                    NULL,
                    &RegisterStatusCodeHandlerEvent
                    );
    ASSERT_EFI_ERROR (Status);

    //
    // Register for protocol notifications on this event
    //
    Status = gBS->RegisterProtocolNotify (
                    &gEfiRscHandlerProtocolGuid,
                    RegisterStatusCodeHandlerEvent,
                    &Registration
                    );
    ASSERT_EFI_ERROR (Status);
  }

  return EFI_SUCCESS;
}
