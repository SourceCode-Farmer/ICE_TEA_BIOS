/** @file
  Contains list of IO Ports and MSR registers to allow access for Read & Write 
  in SMM Handler
  
;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This header file contains list of IO Ports and MSR registers which are required
  to allow access for Read & Write in SMM Handler execution code after End Of DXE.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef __SMM_IOMSR_ACCESS_H__
#define __SMM_IOMSR_ACCESS_H__

#include <CpuRegs.h>
#include <Register/Msr.h>
#include <Register/PchRegsLpc.h>
#include <Register/RtcRegs.h>
#include <Register/CommonMsr.h>
#include <Register/AdlMsr.h>

//
// TODO: Below definitions for COM Ports, SIO, EC & KBC should be moved to appropriate common header files.
// Sighting Created: 1607461349
//

// Com ports
#define COM1_BASE               0x3F8
#define COM2_BASE               0x2F8
#define COM3_BASE               0x3E8
#define COM4_BASE               0x2E8
// Super IO
#define SIO_PORT0               0x2E
#define SIO_PORT1               0x4E
// EC Communication
#define EC_CMD                  0x62
#define EC_DATA                 0x66
// KBC
#define KBC_C_PORT              0x64
#define KBC_D_PORT              0x60

//Todo: To be updated to get from pcd
#define ACPI_BASE               0x1800      // FixedPcdGet16 (PcdAcpiBaseAddress)
#define SMBUS_BASE              0xEFA0      // FixedPcdGet16 (PcdSmbusBaseAddress)
#define TCO_BASE                0x0400      // FixedPcdGet16 (PcdTcoBaseAddress)

typedef struct {
  UINT16      PortBase;
  UINT16      PortLength;
} DGR_IO_TABLE;

typedef struct {
  UINT32       Msr;
  BOOLEAN      DenyReadAccess;
  BOOLEAN      DenyWriteAccess;
} DGR_MSR_TABLE;

const DGR_IO_TABLE mAllowedListIo[] = {
  { COM1_BASE,            8 },
  { COM2_BASE,            8 },
  { COM3_BASE,            8 },
  { COM4_BASE,            8 },
  { SIO_PORT0,            2 },
  { SIO_PORT1,            2 },
  { EC_CMD,               1 },
  { EC_DATA,              1 },
  { KBC_D_PORT,           1 },
  { KBC_C_PORT,           1 },
  { R_PCH_IO_RST_CNT,     1 },        // PCI Reset
  { R_PCH_IO_APM_CNT,     2 },        // SW SMI Port B2, B3
  { R_RTC_IO_INDEX,       8 },        // RTC
  { B_RTC_IO_REGB_SET,    4 },        // Port 80
  { ACPI_BASE,            0x80 },
  { SMBUS_BASE,           0x18 },
  { TCO_BASE,             0x14 }
};

const DGR_MSR_TABLE mAllowedListMsr[] = {
  { MSR_SMM_FEATURE_CONTROL,                  FALSE, FALSE },
  { MSR_IA32_MCG_CAP,                         FALSE, FALSE },
  { MSR_IA32_FEATURE_CONTROL,                 FALSE, FALSE },
  { MSR_IA32_MCG_EXT_CTL,                     FALSE, FALSE },
  { MSR_IA32_MCG_STATUS,                      FALSE, FALSE },
  { MSR_IA32_DEBUGCTL,                        FALSE, FALSE },
  { MSR_IA32_MISC_ENABLE,                     FALSE, FALSE },
  { MSR_TEMPERATURE_TARGET,                   FALSE, FALSE },
  { ADL_MSR_PLATFORM_INFO,                    FALSE, FALSE },
  { MSR_PACKAGE_RAPL_LIMIT,                   FALSE, FALSE },
  { MSR_PACKAGE_POWER_SKU_UNIT,               FALSE, FALSE },
  { MSR_CORE_THREAD_COUNT,                    FALSE, FALSE },
  { MSR_PACKAGE_POWER_SKU,                    FALSE, FALSE },
  { MSR_FLEX_RATIO,                           FALSE, FALSE },
  { MSR_TURBO_RATIO_LIMIT,                    FALSE, FALSE },
  { MSR_SMM_MCA_CAP,                          FALSE, FALSE },
  { MSR_PLAT_FRMW_PROT_CTRL,                  FALSE, FALSE },
  { MSR_PLAT_FRMW_PROT_TRIG_PARAM,            FALSE, FALSE },
  { MSR_PLAT_FRMW_PROT_TRIGGER,               FALSE, FALSE },
  { MSR_IA32_MTRRCAP,                         FALSE, FALSE },
  { MSR_IA32_SMRR_PHYSBASE,                   FALSE, FALSE },
  { MSR_IA32_SMRR_PHYSMASK,                   FALSE, FALSE },
  { MSR_SMM_PROT_MODE_BASE,                   FALSE, FALSE },
  { MSR_IA32_APIC_BASE,                       FALSE, FALSE },
  { MSR_IA32_THERM_INTERRUPT,                 FALSE, FALSE },
  { MSR_IA32_THERM_STATUS,                    FALSE, FALSE },
  { MSR_MISC_PWR_MGMT,                        FALSE, FALSE },
  { MSR_IA32_X2APIC_LVT_THERMAL,              FALSE, FALSE },
  { MSR_IA32_PACKAGE_THERM_STATUS,            FALSE, FALSE },
  { MSR_IA32_PACKAGE_THERM_INTERRUPT,         FALSE, FALSE },
  { MSR_CORE_THREAD_COUNT,                    FALSE, FALSE },
  { MSR_IA32_PM_ENABLE,                       FALSE, FALSE },
  { MSR_IA32_HWP_STATUS,                      FALSE, FALSE },
  { MSR_IA32_HWP_INTERRUPT,                   FALSE, FALSE },
  { MSR_IA32_SMBASE,                          FALSE, TRUE  },
  { MSR_IA32_X2APIC_APICID,                   FALSE, TRUE  },
  { MSR_IA32_X2APIC_ICR,                      FALSE, FALSE },
  { MSR_SPCL_CHIPSET_USAGE,                   FALSE, FALSE },
  { ADL_MSR_SMM_BLOCKED_CNT,                  FALSE, FALSE },
//[-start-200902-IB17040146-add]//
  { MSR_ANC_SACM_INFO,                        FALSE, FALSE }
//[-end-200902-IB17040146-add]//
};

#endif
