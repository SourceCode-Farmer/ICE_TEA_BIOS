/** @file
  Source code file for High Speed Phy Firmware Loading PEI module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiCpuPcieHsPhyInitLib.h>
#include <Library/PcdLib.h>

EFI_STATUS
EFIAPI
HsPhyInitPostMem (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR  mMemoryDiscoveredNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMemoryDiscoveredPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) HsPhyInitPostMem
};

static EFI_PEI_NOTIFY_DESCRIPTOR  mEdkiiIoMmuNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEdkiiIoMmuPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) HsPhyInitPostMem
};

/**
  This function handles Init of Hs Phy after memory initialization

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
**/
EFI_STATUS
EFIAPI
HsPhyInitPostMem (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  DEBUG ((DEBUG_INFO, "%a () Enter\n", __FUNCTION__));

  //
  // HsPhyInit
  //
  HsPhyInit ();
  DEBUG((DEBUG_INFO, "%a () Exit\n", __FUNCTION__));

  return EFI_SUCCESS;
}

/**
   Pcie Phy Firmware Loading PEI module entry point

  @param[in]  FileHandle           Not used.
  @param[in]  PeiServices          General purpose services available to every PEIM.

  @retval     EFI_SUCCESS          The function completes successfully
  @retval     EFI_OUT_OF_RESOURCES Insufficient resources to create database
**/
EFI_STATUS
EFIAPI
PciePhyFirmwareLoadingEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS  Status;
  UINT8       IsIommuSupported;
  DEBUG((DEBUG_INFO, "%a () Enter\n", __FUNCTION__));

  IsIommuSupported = ((PcdGetBool (PcdVTdDisablePeiPmrProtection) == FALSE) && (PcdGet8 (PcdVTdPolicyPropertyMask) & BIT0));

  if (IsIommuSupported) {
    DEBUG ((DEBUG_INFO, "IoMmu supported!\n"));
    //
    // Performing Pcie Phy Fw Loading after DMA Production PPI is produced
    //
    Status = PeiServicesNotifyPpi (&mEdkiiIoMmuNotifyList);
  } else {
    //
    // Performing Pcie Phy Fw Loading after Memory Discovered PPI is produced
    //
    Status = PeiServicesNotifyPpi (&mMemoryDiscoveredNotifyList);
  }
  DEBUG((DEBUG_INFO, "%a () Exit\n", __FUNCTION__));

  return Status;
}
