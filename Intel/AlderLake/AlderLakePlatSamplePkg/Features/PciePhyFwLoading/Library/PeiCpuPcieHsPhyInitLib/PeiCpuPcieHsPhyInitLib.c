/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This Library contains functions that initializes Hight Speed Phy

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Library/PeiMeLib.h>
//[-start-201229-IB06462251-modify]//
//#include <Library/BaseCryptLib.h>
//[-end-201229-IB06462251-modify]//
#include <Library/HsPhyDashboardLib.h>
#include <Library/PeiCpuPcieHsPhyInitLib.h>
#include <Library/PeiCpuPcieHashMiscLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/HobLib.h>
#include <Library/PeiServicesLib.h>
#include <Register/HsPhyDashboardRegs.h>
#include <Ppi/IoMmu.h>
#include <CpuPcieHob.h>
//[-start-201229-IB06462251-add]//
#define SHA512_DIGEST_SIZE  64
//[-start-201229-IB06462251-add]//

/**
  Push firmware contents into IP.

  @param[in] MemoryBuffer          Memory buffer of firmware data
  @param[in] MemorySize            Size of firmware data

  @retval EFI_SUCCESS              IP write success
  @retval Others                   Other failure of IP write
**/
EFI_STATUS
PushHsPhyFirmware (
  IN UINTN                MemoryBuffer,
  IN UINT32               MemorySize
  )
{
  IP_PUSH_MODEL           *IpPushModel;
  UINT16                  Index;
  UINT32                  MemoryIndex;

  MemoryIndex = 0;
  IpPushModel = (IP_PUSH_MODEL *)(UINTN *) MemoryBuffer;

  while (MemoryIndex < MemorySize) {
    MemoryIndex += sizeof (IP_PUSH_MODEL);
    if ((IpPushModel->Mode.Type == 0) && (IpPushModel->Count == 0)) {
      break; // Check for EOF(End of File)
    }
    for (Index = 0; Index < IpPushModel->Count; Index++) {
      CpuRegbarWrite32 (CPU_SB_PID_PCIE_PHYX16_BROADCAST, IpPushModel->Mode.Address, IpPushModel->DataBlob[Index]);
      MemoryIndex += sizeof (UINT32);
    }
    IpPushModel = (IP_PUSH_MODEL *)((UINT8 *) MemoryBuffer + MemoryIndex);
  }

  ZeroMem ((UINTN *) MemoryBuffer, EFI_SIZE_TO_PAGES (MemorySize)); // Clear content.
  FreePool ((UINTN *) MemoryBuffer); // Free buffer

  return EFI_SUCCESS;
}

/**
  Check if BIOS acts as Hsphy firmware loader

  @retval TRUE                  BIOS acts as LOADER
  @retval FALSE                 BIOS does not act as LOADER

**/
BOOLEAN
IsBiosHsPhyFwLoader (
  VOID
  )
{
  EFI_STATUS    Status;
  UINT64        CapBuffer;

  Status = HsPhyGetCapability0 (&CapBuffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Register Capability0 error, Status = %r\n", Status));
    return FALSE;
  }

  if (RShiftU64 (CapBuffer, N_HSPHY_MEM_IP_CAP_0_BIOS_LOADER) & B_HSPHY_MEM_IP_CAP_0_MASK) {
    return TRUE;
  }

  return FALSE;
}

/**
  Locate IOMMU PPI.

  @return Pointer to IOMMU PPI.

**/
EDKII_IOMMU_PPI *
LocateIoMmuPpi (
  VOID
  )
{
  EFI_STATUS         Status;
  EDKII_IOMMU_PPI    *IoMmuPpi;

  IoMmuPpi  = NULL;
  Status = PeiServicesLocatePpi (
             &gEdkiiIoMmuPpiGuid,
             0,
             NULL,
             (VOID **) &IoMmuPpi
             );

  if (!EFI_ERROR (Status) && (IoMmuPpi != NULL)) {
    DEBUG ((DEBUG_INFO, "gEdkiiIoMmuPpiGuid installed\n"));
    return IoMmuPpi;
  }

  return NULL;
}

/**
  Allocates pages that are suitable for an OperationBusMasterCommonBuffer or
  OperationBusMasterCommonBuffer64 mapping.

  @param  IoMmuPpi              Pointer to IOMMU PPI.
  @param  Pages                 The number of pages to allocate.
  @param  HostAddress           A pointer to store the base system memory address of the
                                allocated range.
  @param  DeviceAddress         The resulting map address for the bus master PCI controller to use to
                                access the hosts HostAddress.
  @param  Mapping               A resulting value to pass to Unmap().

  @retval EFI_SUCCESS           The requested memory pages were allocated.
  @retval EFI_UNSUPPORTED       Attributes is unsupported. The only legal attribute bits are
                                MEMORY_WRITE_COMBINE and MEMORY_CACHED.
  @retval EFI_INVALID_PARAMETER One or more parameters are invalid.
  @retval EFI_OUT_OF_RESOURCES  The memory pages could not be allocated.

**/
EFI_STATUS
IoMmuAllocateBuffer (
  IN  EDKII_IOMMU_PPI        *IoMmuPpi,
  IN  UINTN                   Pages,
  OUT VOID                  **HostAddress,
  OUT EFI_PHYSICAL_ADDRESS   *DeviceAddress,
  OUT VOID                  **Mapping
  )
{
  EFI_STATUS            Status;
  UINTN                 NumberOfBytes;

  DEBUG ((DEBUG_INFO, "%a () enter\n", __FUNCTION__));
  Status = EFI_INVALID_PARAMETER;

  if (IoMmuPpi != NULL) {
    *HostAddress   = NULL;
    *Mapping       = NULL;
    *DeviceAddress = 0;

    Status = IoMmuPpi->AllocateBuffer (
                         IoMmuPpi,
                         EfiBootServicesData,
                         Pages,
                         HostAddress,
                         0
                         );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "HsPhyInit: IoMmuPpi AllocateBuffer failure, %r\n", Status));
      goto Exit;
    }

    NumberOfBytes = EFI_PAGES_TO_SIZE (Pages);
    Status = IoMmuPpi->Map (
                         IoMmuPpi,
                         EdkiiIoMmuOperationBusMasterCommonBuffer,
                         *HostAddress,
                         &NumberOfBytes,
                         DeviceAddress,
                         Mapping
                         );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "HsPhyInit: IoMmuPpi Map failure, %r\n", Status));
      goto Exit;
    }

    Status = IoMmuPpi->SetAttribute (
                         IoMmuPpi,
                         *Mapping,
                         EDKII_IOMMU_ACCESS_READ | EDKII_IOMMU_ACCESS_WRITE
                         );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "HsPhyInit: IoMmuPpi SetAttribute failure, %r\n", Status));
      goto Exit;
    }

Exit:
    if (EFI_ERROR (Status)) {
      if (*Mapping != NULL) {
        IoMmuPpi->SetAttribute (IoMmuPpi, *Mapping, 0);
        IoMmuPpi->Unmap (IoMmuPpi, *Mapping);
      }
      if (*HostAddress != NULL) {
        IoMmuPpi->FreeBuffer (IoMmuPpi, Pages, *HostAddress);
      }
    }
  }
  DEBUG ((DEBUG_INFO, "%a () exit\n", __FUNCTION__));
  return Status;
}

/**
  This function creates a buffer and passes buffer information to CSME to load HsPhy Firmware
  Authenticates the buffer by comparing the HASH
  Pushes HsPhy Firmware to Phy Controller

  @retval EFI_SUCCESS                On successfully pushing IP Firmware to Hsphy controller
  @retval EFI_SECURITY_VIOLATION     Verification failed
  @retval EFI_OUT_OF_RESOURCES       Buffer creation failed
  @retval EFI_UNSUPPORTED            Bios doen't have Fw loading capabilities
**/
EFI_STATUS
HsPhyInit (
  VOID
  )
{
  EFI_STATUS       Status;
  VOID             *Buffer;
  UINT32           BufferSize;
  UINT32           IpLoaderStatus;
  UINT8            HashAlg;
  UINT8            *Hash;
//[-start-201229-IB06462251-remove]//
//  UINTN         PayloadHashSize;
//  UINT8         *PayloadHash;
//[-start-201229-IB06462251-remove]//
  UINT32           *BufferAddressHigh;
  UINT16           Data16;
  UINT32           Pages;
  UINT64           DeviceAddress;
  VOID             *BufferMapping;
  EDKII_IOMMU_PPI  *IoMmuPpi;
  UINT32            HsPhyMap;
  CPU_PCIE_HOB      *CpuPcieHob;

  DEBUG ((DEBUG_INFO, "%a () enter\n", __FUNCTION__));

  Buffer   = NULL;
  HsPhyMap = 0;
  //
  // Get CpuPcieHob HOB
  //
  CpuPcieHob = NULL;
  CpuPcieHob = (CPU_PCIE_HOB *) GetFirstGuidHob (&gCpuPcieHobGuid);

  if (CpuPcieHob != NULL) {
    HsPhyMap = CpuPcieHob->HsPhyMap;
  }

  //
  // Check Bios is loader of HsPhy firmware
  //
  if (!IsSimicsEnvironment ()) {
    if (!IsBiosHsPhyFwLoader () || (HsPhyMap == 0)) {
      DEBUG ((DEBUG_INFO, "Skip HsPhy firmware download\n"));
      return EFI_UNSUPPORTED;
    }
  }

  Hash = AllocateZeroPool (SHA512_DIGEST_SIZE);
  if (Hash == NULL) {
    DEBUG ((DEBUG_ERROR, "%a(): Could not allocate Memory for Hash Buffer\n", __FUNCTION__));
    return EFI_OUT_OF_RESOURCES;
  }

  BufferSize = HSPHY_PAYLOAD_SIZE;
  Pages      = EFI_SIZE_TO_PAGES (HSPHY_PAYLOAD_SIZE);
  IoMmuPpi   = LocateIoMmuPpi ();
  if (IoMmuPpi != NULL) {
    //
    // Memory allocation for transfer-related data
    //
    Status = IoMmuAllocateBuffer (
               IoMmuPpi,
               Pages,
               &Buffer,
               &DeviceAddress,
               &BufferMapping
               );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, " Fail to allocate IoMmu buffer\n"));
      return Status;
    }
  } else {
    Buffer = AllocatePages (Pages);
  }

  if (Buffer == NULL) {
    DEBUG ((DEBUG_INFO, "Buffer allocation for HsPhy firmware failed\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  ZeroMem (Buffer, BufferSize);

  //
  // Set BME and MSE to allow HECI1 copy massive data to Buffer
  //
  Data16 = 0;
  if (PciSegmentRead16 (PchHeci1PciCfgBase () + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
    DEBUG ((DEBUG_INFO, "Set BME, MSE on Heci1 config space\n"));
    Data16 = PciSegmentRead16 (PchHeci1PciCfgBase () + PCI_COMMAND_OFFSET);
    Data16 |= (EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER);
    PciSegmentWrite16 (PchHeci1PciCfgBase () + PCI_COMMAND_OFFSET, Data16);
  }

  //
  // Send Heci for Load PCIe GEN5 request
  //
  BufferAddressHigh = 0;
  DEBUG ((DEBUG_INFO, "BufferAddress %x\n", Buffer));
  Status = PeiHeciGetFwPayload (HsPhyFwLoad, (UINT32) Buffer, (UINT32) BufferAddressHigh, &BufferSize, &IpLoaderStatus, &HashAlg, Hash);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Terminate Heci communication if CSME is in Debug Mode.
  //
  Status = PeiHeciTerminateHeci ();

  //
  // Clear BME and MSE of Heci1
  //
  Data16 = 0;
  if (PciSegmentRead16 (PchHeci1PciCfgBase() + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
    DEBUG ((DEBUG_INFO, "Clear BME, MSE on Heci1 config space\n"));
    Data16 = PciSegmentRead16 (PchHeci1PciCfgBase() + PCI_COMMAND_OFFSET);
    Data16 &= ~(EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER);
    PciSegmentWrite16 (PchHeci1PciCfgBase () + PCI_COMMAND_OFFSET, Data16);
  }

  if (IpLoaderStatus != 0) {
    DEBUG ((DEBUG_INFO, "Me failed to copy HsPhy firmware, return status - %x\n", IpLoaderStatus));
    if (!IsSimicsEnvironment ()) {
      HsPhySetVerifierErrorCode (VerifierIdx1, IpLoaderStatus);
      return EFI_SECURITY_VIOLATION;
    }
  }

//[-start-201229-IB06462251-modify]//
//
// temporary remove it because we don't use OpendSSL.
//
//  //
//  // Check actual algorithm used by ME
//  //
//  PayloadHashSize = GetHashSize (HashAlg);
//  PayloadHash = AllocateZeroPool (PayloadHashSize);
//  if (PayloadHash == NULL) {
//    DEBUG ((DEBUG_ERROR, "%a(): Could not allocate Memory for Hash calc of Payload Buffer\n", __FUNCTION__));
//    return EFI_OUT_OF_RESOURCES;
//  }
//
//  //
//  // Calculate the Hash for the payload based on Hash Algorithm passed by csme
//  //
//  Status = CreateHash (HashAlg, Buffer, BufferSize, PayloadHash);
//  if (!Status) {
//    DEBUG ((DEBUG_INFO, "Creation of Hash for the HsPhy firmware failed\n"));
//    return EFI_SECURITY_VIOLATION;
//  }
//
//  //
//  // Compare the hash
//  //
//  if (CompareMem (PayloadHash, Hash, PayloadHashSize) != 0) {
//    DEBUG ((DEBUG_INFO, "Hash mismatch for HsPhy firmware payload\n"));
//    return EFI_SECURITY_VIOLATION;
//  }
//[-start-201229-IB06462251-modify]//

  //
  // Push the firmware
  //
  DEBUG ((DEBUG_INFO, "Push HsPhy firmware\n"));
  PushHsPhyFirmware ((UINTN)Buffer, BufferSize);

  if (IoMmuPpi != NULL) {
    if (BufferMapping != NULL) {
      IoMmuPpi->SetAttribute (IoMmuPpi, BufferMapping, 0);
      IoMmuPpi->Unmap (IoMmuPpi, BufferMapping);
    }
    if (DeviceAddress != 0) {
      IoMmuPpi->FreeBuffer (IoMmuPpi, Pages, &DeviceAddress);
    }
  } else if (Buffer != NULL) {
    FreePages (Buffer, Pages);
  }

  DEBUG ((DEBUG_INFO, "%a () exit\n", __FUNCTION__));
  return EFI_SUCCESS;
}