/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This Library contains Wrapper functions for Hash Calculations

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseCryptLib.h>
#include <Library/PeiCpuPcieHashMiscLib.h>

/**
Calculates the size of the hash based on algorithm.

@param[in] HashAlg     Algorithm used for hash calculation.

@retval  HashSize      The total size of hash in bytes.
@retval  0             Hash algorithm not supported.

**/
UINTN
GetHashSize (
  IN  UINT32              HashAlg
)
{
  switch (HashAlg) {
    case HASHALG_SHA1:
        //
        // SHA1 Hash
        //
        return SHA1_DIGEST_SIZE;
    case HASHALG_SHA256:
        //
        // SHA256 Hash
        //
        return SHA256_DIGEST_SIZE;
    case HASHALG_SHA384:
        //
        // SHA384 Hash
        //
        return SHA384_DIGEST_SIZE;
    case HASHALG_SHA512:
        //
        // SHA512 Hash
        //
        return SHA512_DIGEST_SIZE;
   default:
        return 0;
  }
}

/**
Calculates the hash of the given data based on the input hash algorithm.

@param[in]  HashAlg        Algorithm used for hash calculation
@param[in]  Data           Pointer to the data buffer to be hashed
@param[in]  DataSize       The size of data buffer in bytes
@param[out] HashValue      Pointer to a buffer that receives the hash result
@param[in]  HashSize       The total size of hash in bytes

@retval TRUE               Data hash calculation succeeded
@retval FALSE              Data hash calculation failed

**/
BOOLEAN
CreateHash (
  IN  UINT32              HashAlg,
  IN  VOID                *Data,
  IN  UINTN               DataSize,
  OUT UINT8               *HashValue
)
{
  BOOLEAN  Status;

  Status = FALSE;

//[-start-201229-IB06462251-modify]//
//
// temporary remove it because we don't use OpendSSL.
//
//  switch (HashAlg) {
//    case HASHALG_SHA1:
//        //
//        // SHA1 Hash
//        //
//        Status = Sha1HashAll(Data, DataSize, HashValue);
//        break;
//   case HASHALG_SHA256:
//        //
//        // SHA256 Hash
//        //
//        Status = Sha256HashAll(Data, DataSize, HashValue);
//        break;
//    case HASHALG_SHA384:
//        //
//        // SHA384 Hash
//        //
//        Status = Sha384HashAll(Data, DataSize, HashValue);
//        break;
//    case HASHALG_SHA512:
//        //
//        // SHA512 Hash
//        //
//        Status = Sha512HashAll(Data, DataSize, HashValue);
//        break;
//    default:
//        break;
//  }
//[-end-201229-IB06462251-modify]//

  return Status;
}
