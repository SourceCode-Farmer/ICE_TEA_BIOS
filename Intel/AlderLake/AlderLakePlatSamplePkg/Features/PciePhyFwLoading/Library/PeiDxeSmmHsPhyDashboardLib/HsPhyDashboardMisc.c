/** @file
  HSPHY - Miscellaneous Dashboard Library Modules Implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/IoLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/PchRegs.h>
#include <Library/PchPciBdfLib.h>
#include <HsPhyDashboard.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Library/HsPhyDashboardLib.h>
#include <Register/HsPhyDashboardRegs.h>

typedef union {
  UINT16  Data16;
  UINT32  Data32;
  struct {
    UINT32 Low32;
    UINT32 High32;
  } Uint64;
  UINT64  Data64;
} DATA_BUF;

/**
  Get IP specific dashboard base offset

  @param[in] RegisterType         Loader/Verifier type (0: Loader, 1: Verifier)
  @param[in] RegisterOffset       Dashboard register offset to be accessed

  @retval UINT32                  Dashboard register verifier/loader base.
                                  0 means not supported.

**/
UINT32
GetDashboardRegBase (
  IN HSPHY_REG_TYPE            RegisterType
  )
{
  if (RegisterType == RegVerifier) {
    return HSPHY_VERIFIER_BASE;
  }
  return HSPHY_LOADER_BASE;
}


/**
  Send Hsphy specific dashboard Read/Write command

  @param[in]       OperationType       Read/Write command (0: Read operation, 1: Write operation)
  @param[in]       RegisterType        Loader/Verifier type (0: Loader, 1: Verifier)
  @param[in]       RegisterOffset      Dashboard register offset to be accessed
  @param[in]       NumberOfBytes       Number of byes to be read or written
  @param[in out]   DataBuffer          Data buffer which holds data.
                                       For read operation it is of output type
                                       For write operation it is of input type

  @retval EFI_SUCCESS                 Command was executed successfully
  @retval Others                      Command was failed

**/
EFI_STATUS
HsphySendCommand (
  IN HSPHY_OP_TYPE                OperationType,
  IN HSPHY_REG_TYPE               RegisterType,
  IN UINT16                       RegisterOffset,
  IN HSPHY_NUM_BYTES              NumberOfBytes,
  IN OUT VOID                     *DataBuffer
  )
{
  DATA_BUF  DataBuf;
  UINT32    BaseOffset;

  DataBuf.Data64 = 0;
  BaseOffset = GetDashboardRegBase (RegisterType);

  if (OperationType == OpWrite) {
    switch (NumberOfBytes) {
      case TwoBytes:
        DataBuf.Data16 = *(UINT16 *)DataBuffer;
        CpuRegbarWrite16 (CPU_SB_PID_PCIE_BUTTRESSx16_0 , (UINT16)BaseOffset + RegisterOffset, DataBuf.Data16);
        break;

      case FourBytes:
        DataBuf.Data32 = *(UINT32 *)DataBuffer;
        CpuRegbarWrite32 (CPU_SB_PID_PCIE_BUTTRESSx16_0 , (UINT16)BaseOffset + RegisterOffset, DataBuf.Data32);
        break;

      case EightBytes:
        DataBuf.Data64 = *(UINT64 *)DataBuffer;
        CpuRegbarWrite64 (CPU_SB_PID_PCIE_BUTTRESSx16_0 , (UINT16)BaseOffset + RegisterOffset, DataBuf.Data64);
        break;

    default:
      break;
    }
  } else {
    switch (NumberOfBytes) {
      case TwoBytes:
        DataBuf.Data16 = CpuRegbarRead16 (CPU_SB_PID_PCIE_BUTTRESSx16_0 , (UINT16)BaseOffset + RegisterOffset);
        *(UINT16 *)DataBuffer = DataBuf.Data16;
        break;

      case FourBytes:
        DataBuf.Data32 = CpuRegbarRead32 (CPU_SB_PID_PCIE_BUTTRESSx16_0 , (UINT16)BaseOffset + RegisterOffset);
        *(UINT32 *)DataBuffer = DataBuf.Data32;
        break;

      case EightBytes:
        DataBuf.Data64  = CpuRegbarRead64 (CPU_SB_PID_PCIE_BUTTRESSx16_0 , (UINT16)BaseOffset + RegisterOffset);
        *(UINT64 *)DataBuffer = DataBuf.Data64;
        break;

      default:
        break;
    }
  }

  return EFI_SUCCESS;
}

/**
  Send IP specific dashboard Read/Write command

  @param[in]     OperationType    Read/Write command (0: Read operation, 1: Write operation)
  @param[in]     RegisterType     Loader/Verifier type (0: Loader, 1: Verifier)
  @param[in]     RegisterOffset   Dashboard register offset to be accessed
  @param[in]     NumberOfBytes    Number of byes to be read or written
  @param[in out] DataBuffer       Data buffer which holds data.
                                  For read operation it is of output type
                                  For write operation it is of input type

  @retval EFI_SUCCESS             Command was executed successfully
  @retval Others                  Command was failed

**/
EFI_STATUS
DashboardSendCommand (
  IN     HSPHY_OP_TYPE            OperationType,
  IN     HSPHY_REG_TYPE           RegisterType,
  IN     UINT16                   RegisterOffset,
  IN     HSPHY_NUM_BYTES          NumberOfBytes,
  IN OUT VOID                     *DataBuffer
  )
{
  return HsphySendCommand (OperationType, RegisterType, RegisterOffset, NumberOfBytes, DataBuffer);

}
