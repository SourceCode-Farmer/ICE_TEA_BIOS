/** @file
  HSPHY - Dashboard Library Modules Implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Register/PchRegs.h>
#include <Library/HsPhyDashboardLib.h>
#include <Register/HsPhyDashboardRegs.h>


/**
  Get the IP loader and verifier capabilities register 0

  @param[out] CapBuffer             Data which describes IP capabilities

  @retval EFI_SUCCESS               Successful operation
  @retval Others                    Failure

**/
EFI_STATUS
HsPhyGetCapability0 (
  OUT UINT64                   *CapBuffer
  )
{
  DEBUG ((DEBUG_INFO, "HSPHY: %a () enter\n", __FUNCTION__));

  *CapBuffer = 0;
  return DashboardSendCommand (OpRead, RegVerifier, R_HSPHY_MEM_IP_CAP_0, EightBytes, CapBuffer);
}

/**
  Set the error code in dashboard for verifier failed scenario

  @param[in] ImageNumber            Failed image number
  @param[in] ErrorCode              Failed reason

  @retval EFI_SUCCESS               Successful operation
  @retval Others                    Failure

**/
EFI_STATUS
HsPhySetVerifierErrorCode (
  IN     HSPHY_VERIFIER_INDEX      ImageNumber,
  IN     UINT32                   ErrorCode
  )
{
  EFI_STATUS             Status;
  UINT32                 Data32;

  DEBUG ((DEBUG_INFO, "HSPHY: %a () enter\n", __FUNCTION__));

  Data32 = 0;
  Status = DashboardSendCommand (OpRead, RegVerifier, R_HSPHY_MEM_VERIFIER_ERROR_STATUS, FourBytes, &Data32);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Set the verifier error code with its image number
  //
  Data32 |= (ErrorCode << N_HSPHY_MEM_VERIFIER_ERROR_STATUS_ERROR_CODE);
  Data32 |= (1 << ImageNumber);
  Status = DashboardSendCommand (OpWrite, RegVerifier, R_HSPHY_MEM_VERIFIER_ERROR_STATUS, FourBytes, &Data32);

  return Status;
}
