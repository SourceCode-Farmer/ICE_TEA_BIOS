/** @file
  Header file for HsPhy Dashboard definitions

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _HSPHY_DASHBOARD_H_
#define _HSPHY_DASHBOARD_H_

typedef enum {
  OpRead  = 0,
  OpWrite = 1
} HSPHY_OP_TYPE;

typedef enum {
  RegLoader   = 0,
  RegVerifier = 1
} HSPHY_REG_TYPE;

typedef enum {
  TwoBytes     = 2,
  FourBytes    = 4,
  EightBytes   = 8
} HSPHY_NUM_BYTES;

typedef enum {
  LoaderIdx1 = 1,
  LoaderIdx2 = 2,
  LoaderIdx3 = 3,
  LoaderIdx4 = 4,
  LoaderNull = 0
} HSPHY_LOADER_INDEX;

typedef enum {
  VerifierIdx1 = 1,
  VerifierIdx2 = 2,
  VerifierIdx3 = 3,
  VerifierIdx4 = 4,
  VerifierNull = 0,
  VerifierNotApplicable = 0xFF
} HSPHY_VERIFIER_INDEX;

typedef enum {
  MaterialIdx1 = 1,
  MaterialIdx2 = 2,
  MaterialIdx3 = 3,
  MaterialIdx4 = 4,
  MaterialNull = 0,
  MaterialNotApplicable = 0xFF
} HSPHY_VERIFICATION_MATERIAL_INDEX;

#endif // _HSPHY_DASHBOARD_H_
