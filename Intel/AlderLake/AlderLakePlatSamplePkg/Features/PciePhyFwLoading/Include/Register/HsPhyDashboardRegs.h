/** @file
    Register names for HsPhy Dashboard Registers

  - Register definition format:
    Prefix_[GenerationName]_[ComponentName]_SubsystemName_RegisterSpace_RegisterName
  - Prefix:
    Definitions beginning with "R_" are registers
    Definitions beginning with "B_" are bits within registers
    Definitions beginning with "V_" are meaningful values within the bits
    Definitions beginning with "S_" are register size
    Definitions beginning with "N_" are the bit position
  - [GenerationName]:
    Three letter acronym of the generation is used (e.g. SKL,KBL,CNL etc.).
    Register name without GenerationName applies to all generations.
  - [ComponentName]:
    This field indicates the component name that the register belongs to (e.g. PCH, SA etc.)
    Register name without ComponentName applies to all components.
    Register that is specific to -H denoted by "_PCH_H_" in component name.
    Register that is specific to -LP denoted by "_PCH_LP_" in component name.
  - SubsystemName:
    This field indicates the subsystem name of the component that the register belongs to
    (e.g. PCIE, USB, SATA, GPIO, PMC etc.).
  - RegisterSpace:
    MEM - MMIO space register of subsystem.
    IO  - IO space register of subsystem.
    PCR - Private configuration register of subsystem.
    CFG - PCI configuration space register of subsystem.
  - RegisterName:
    Full register name.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _HSPHY_DASHBOARD_REGS_H_
#define _HSPHY_DASHBOARD_REGS_H_

#define HSPHY_VERIFIER_BASE                                       0x3000   ///< Verifier Base - Offset from REGBAR base address and the port ID : CPU_SB_PID_PCIE_BUTTRESSx16_0
#define HSPHY_LOADER_BASE                                         0        ///< Loader Base (Not supported)
#define HSPHY_CR_OFFSET                                           0        ///< CR Offset - Offset from REGBAR base address and the port ID : CPU_SB_PID_DEKEL_IO4

///
/// Below registers are Verifier Base
///
#define R_HSPHY_MEM_IP_CAP_0                                      0x18      // IP Loader and Verifier Cap0 Register
#define N_HSPHY_MEM_IP_CAP_0_BIOS_LOADER                          12        // Loader Capabilities is BIOS
#define B_HSPHY_MEM_IP_CAP_0_MASK                                 0xF       // IP Loader and Verifier Capability Mask
#define R_HSPHY_MEM_VERIFIER_ERROR_STATUS                         0x30      // Verifier Error Status
#define N_HSPHY_MEM_VERIFIER_ERROR_STATUS_ERROR_CODE              4

//
// CPU SB Info
//
#define CPU_SB_PID_PCIE_BUTTRESSx16_0                             0x54

#endif // _HSPHY_DASHBOARD_REGS_H
