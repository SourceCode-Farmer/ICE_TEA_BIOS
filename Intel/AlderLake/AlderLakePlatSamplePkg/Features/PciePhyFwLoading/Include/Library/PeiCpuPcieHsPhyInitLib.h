/** @file
  This file contains functions that initializes High Speed Phy

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_FW_LOAD_PLAT_H_
#define _PEI_FW_LOAD_PLAT_H_

#define HSPHY_PAYLOAD_SIZE                          32 * 1024 // For ADL allocate 32KB of memory for Hsphy
#define CPU_SB_PID_PCIE_PHYX16_BROADCAST            0x55

typedef struct {
  UINT16               Count;
  union {
    UINT16             Address;
    UINT16             Type; // Valid only for Extended mode push model operations
  } Mode;
  UINT32               DataBlob[];
} IP_PUSH_MODEL;

typedef enum {
  HsPhyFwLoad = 1
} IpLoadRequest;

/**
  This function creates a buffer and passes buffer information to CSME to load HsPhy Firmware
  Authenticates the buffer by comparing the HASH
  Pushes HsPhy Firmware to Phy Controller

  @retval EFI_SUCCESS                On successfully pushing IP Firmware to Hsphy controller
  @retval EFI_SECURITY_VIOLATION     Verification failed
  @retval EFI_OUT_OF_RESOURCES       Buffer creation failed
  @retval EFI_UNSUPPORTED            Bios doen't have Fw loading capabilities
**/
EFI_STATUS
HsPhyInit  (
  VOID
  );

#endif
