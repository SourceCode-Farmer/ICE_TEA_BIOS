/** @file
  Header file for HSPHY Dashboard Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _HSPHY_DASHBOARD_LIB_H_
#define _HSPHY_DASHBOARD_LIB_H_

#include <HsPhyDashboard.h>

/**
  Send IP specific dashboard Read/Write command

  @param[in]     OperationType    Read/Write command (0: Read operation, 1: Write operation)
  @param[in]     RegisterType     Loader/Verifier type (0: Loader, 1: Verifier)
  @param[in]     RegisterOffset   Dashboard register offset to be accessed
  @param[in]     NumberOfBytes    Number of byes to be read or written
  @param[in out] DataBuffer       Data buffer which holds data.
                                  For read operation it is of output type
                                  For write operation it is of input type

  @retval EFI_SUCCESS             Command was executed successfully
  @retval Others                  Command was failed

**/
EFI_STATUS
DashboardSendCommand (
  IN     HSPHY_OP_TYPE            OperationType,
  IN     HSPHY_REG_TYPE           RegisterType,
  IN     UINT16                   RegisterOffset,
  IN     HSPHY_NUM_BYTES          NumberOfBytes,
  IN OUT VOID                     *DataBuffer
  );

/**
  Get the IP loader and verifier capabilities register 0

  @param[out] CapBuffer             Data which describes IP capabilities

  @retval EFI_SUCCESS               Successful operation
  @retval Others                    Failure

**/
EFI_STATUS
HsPhyGetCapability0 (
  OUT UINT64                   *CapBuffer
  );

/**
  Set the error code in dashboard for verifier failed scenario

  @param[in] ImageNumber            Failed image number
  @param[in] ErrorCode              Failed reason

  @retval EFI_SUCCESS               Successful operation
  @retval Others                    Failure

**/
EFI_STATUS
HsPhySetVerifierErrorCode (
  IN     HSPHY_VERIFIER_INDEX     ImageNumber,
  IN     UINT32                   ErrorCode
  );

#endif //_HSPHY_DASHBOARD_LIB_H_
