/** @file
  This header file contains Wrapper functions for Hash Calculations

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_HASH_MISC_PLAT_H_
#define _PEI_HASH_MISC_PLAT_H_
//
// Supported Hash Types
//
#define HASHALG_SHA1                      0x00000001
#define HASHALG_SHA256                    0x00000002
#define HASHALG_SHA384                    0x00000003
#define HASHALG_SHA512                    0x00000004

/**
Calculates the size of the hash based on algorithm.

@param[in] HashAlg     Algorithm used for hash calculation.

@retval  HashSize      The total size of hash in bytes.
@retval  0             Hash algorithm not supported.

**/
UINTN
GetHashSize (
  IN  UINT32              HashAlg
);

/**
Calculates the hash of the given data based on the input hash algorithm.

@param[in]  HashAlg        Algorithm used for hash calculation
@param[in]  Data           Pointer to the data buffer to be hashed
@param[in]  DataSize       The size of data buffer in bytes
@param[out] HashValue      Pointer to a buffer that receives the hash result
@param[in]  HashSize       The total size of hash in bytes

@retval TRUE               Data hash calculation succeeded
@retval FALSE              Data hash calculation failed

**/
BOOLEAN
CreateHash (
  IN  UINT32              HashAlg,
  IN  VOID                *Data,
  IN  UINTN               DataSize,
  OUT UINT8               *HashValue
);
#endif
