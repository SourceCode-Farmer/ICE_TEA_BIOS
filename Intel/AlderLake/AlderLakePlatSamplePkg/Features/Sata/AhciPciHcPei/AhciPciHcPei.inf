## @file
#  Component description file for the ATA AHCI host controller PCI
#  policy PEIM.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2019 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
#@par Specification
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = AhciPciHcPei
  FILE_GUID                      = 3D417445-1F37-46D8-BC1F-D07014344974
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = AhciPciHcPeimEntry

[Sources]
  AhciPciHcPei.h
  AhciPciHcPei.c

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  IntelFsp2WrapperPkg/IntelFsp2WrapperPkg.dec

[LibraryClasses]
  PciLib
  DebugLib
  PeiServicesLib
  MemoryAllocationLib
  PcdLib
  LockBoxLib
  PeimEntryPoint
  PciSegmentLib

[Pcd]
  gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioBase     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioLimit    ## CONSUMES

[Ppis]
  gEdkiiPeiAtaAhciHostControllerPpiGuid              ## SOMETIMES_PRODUCES
  gRstRaidControllerPpiGuid                          ## SOMETIMES_PRODUCES
  gEfiEndOfPeiSignalPpiGuid                          ## SOMETIMES_CONSUMES

[Depex]
  gFspSiliconInitDonePpiGuid OR gEndOfSiInitPpiGuid
