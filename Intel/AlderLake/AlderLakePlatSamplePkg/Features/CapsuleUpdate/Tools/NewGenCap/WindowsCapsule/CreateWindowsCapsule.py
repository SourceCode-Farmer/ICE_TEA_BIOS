#!python3
##
# Copyright (c) 2019, Microsoft Corporation

# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##

##
# This is a sample tool and should not be used in production environments.
#
# This tool creates a signed Windows Capsule.
##

import sys
import argparse
import re
import time
import os
import glob

#
# Globals for help information
#
__prog__        = sys.argv[0]
__copyright__   = 'Copyright (c) 2019, Intel Corporation. All rights reserved.'
__description__ = 'Generate Windows Microcode capsules.\n'

#
# Pay attention to
# DriverVer=04/24/2019,Version 1.2.3.4 => DriverVer=04/24/2019,0.0.0.16
# HKR,,FirmwareVersion,%REG_DWORD%,0x1234 => HKR,,FirmwareVersion,%REG_DWORD%,0x00000010
# HKR,,FirmwareId,,{96D4FDCD-1502-424D-9D4C-9B12-D2DCAE5C} => HKR,,FirmwareId,,{96D4FDCD-1502-424D-9D4C-9B12D2DCAE5C}
#
def GenerateInf (InputArgs):
    TemplateInf = '''
        ;
        ; {fwName}.inf
        ;
        ; Copyright (C) 2016 Microsoft Corporation.  All Rights Reserved.
        ;
        [Version]
        Signature="$WINDOWS NT$"
        Class=Firmware
        ClassGuid={{f2e7dd72-6468-4e36-b6f1-6488f42c1b52}}
        Provider=%Provider%
        DriverVer={date},{driverVer}
        PnpLockdown=1
        CatalogFile={fwName}.cat

        [Manufacturer]
        %MfgName% = Firmware,NTamd64

        [Firmware.NTamd64]
        %FirmwareDesc% = Firmware_Install,UEFI\RES_{{{EsrtGuid}}}

        [Firmware_Install.NT]
        CopyFiles = Firmware_CopyFiles

        [Firmware_CopyFiles]
        {OutputEfiName}

        [Firmware_Install.NT.Hw]
        AddReg = Firmware_AddReg

        [Firmware_AddReg]
        HKR,,FirmwareId,,{{{EsrtGuid}}}
        HKR,,FirmwareVersion,%REG_DWORD%,{VersionIntHexString}
        HKR,,FirmwareFilename,,{OutputEfiName}

        [SourceDisksNames]
        1 = %DiskName%

        [SourceDisksFiles]
        {OutputEfiName} = 1

        [DestinationDirs]
        DefaultDestDir = %DIRID_WINDOWS%,Firmware ; %SystemRoot%\Firmware

        [Strings]
        ; localizable
        Provider     = "{fwProvider}"
        MfgName      = "{fwMfgName}"
        FirmwareDesc = "{fwDesc}"
        DiskName     = "Firmware Update"

        ; non-localizable
        DIRID_WINDOWS = 10
        REG_DWORD     = 0x00010001
        '''
    Date = time.strftime("%m/%d/%Y")
    VersionString = InputArgs.CapsuleVersionDotString
    while VersionString.count(".") < 3:
        VersionString += ".0"
    DataContent = TemplateInf.format (
                    fwName = InputArgs.ProductFwName,
                    date = Date,
                    driverVer = VersionString,
                    EsrtGuid = InputArgs.ProductFmpGuid,
                    OutputEfiName = os.path.basename(InputArgs.CapsuleFileName),
                    VersionIntHexString = InputArgs.CapsuleVersionHexString,
                    fwProvider = InputArgs.ProductFwProvider,
                    fwMfgName = InputArgs.ProductFwMfgName,
                    fwDesc = InputArgs.ProductFwDesc
                    )
    with open(InputArgs.ProductFwName + ".inf", "w") as File:
        for line in DataContent.splitlines():
            File.write(line.strip() + "\n")

def GenWindowsCapsule (InputArgs):
    SignToolPath = os.path.join(os.getenv("ProgramFiles(x86)"), "Windows Kits", "10", "bin", "x64", "signtool.exe")
    if not os.path.exists(SignToolPath):
        SignToolPath = SignToolPath.replace('10', '8.1')
    if not os.path.exists(SignToolPath):
        SignToolPathPattern = os.path.join (os.getenv("ProgramFiles(x86)"), "Windows Kits", "10", "bin", "*", "x64", "signtool.exe")
        SignToolPathList = glob.glob (SignToolPathPattern)
        if len (SignToolPathList) != 0:
            SignToolPath = (SignToolPathList)[0]
    if not os.path.exists(SignToolPath):
        raise Exception("Can't find signtool on this machine.")

    Inf2CatToolPath = os.path.join(os.getenv("ProgramFiles(x86)"), "Windows Kits", "10", "bin", "x86", "Inf2Cat.exe")
    if not os.path.exists(Inf2CatToolPath):
        Inf2CatToolPathPattern = os.path.join (os.getenv("ProgramFiles(x86)"), "Windows Kits", "10", "bin", "*", "x86", "Inf2Cat.exe")
        Inf2CatToolPathList = glob.glob (Inf2CatToolPathPattern)
        if len (Inf2CatToolPathList) != 0:
            Inf2CatToolPath = (Inf2CatToolPathList)[0]
    if not os.path.exists(Inf2CatToolPath):
        raise Exception("Can't find Inf2Cat on this machine.  Please install the Windows 10 WDK - https://developer.microsoft.com/en-us/windows/hardware/windows-driver-kit")

    cmd = '"' + Inf2CatToolPath + '"' + " /driver:. /os:10_X64 /verbose /uselocaltime"
    ret = os.system(cmd)
    if ret != 0:
        raise Exception("Creating Cat file Failed with errorcode %d" % ret)

    if InputArgs.PfxFileName != None:
        cmd = '"' + SignToolPath + '"' + " sign /a /v /fd SHA256 /f " + InputArgs.PfxFileName + " " + InputArgs.ProductFwName + ".cat"
        ret = os.system(cmd)
        if ret != 0:
            raise Exception("Signing Cat file Failed with errorcode %d" % ret)

if __name__ == "__main__":
    def ValidateProductFwName (Argument):
        if re.search( r'\s', Argument, re.I):
            Message = '{Argument} can not contain any spaces.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Argument

    def ValidateGuidFormat (Argument):
        if not re.search(r'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$', Argument):
            Message = '{Argument} is not a valid GUID format.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Argument

    def ValidateHexString (Argument):
        if not re.search( r'^0[Xx][0-9a-fA-F]{1,8}$', sys.argv[5], re.I):
            Message = '{Argument} is not a hex string format.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Argument

    def ValidatePfxFile (Argument):
        if not os.path.isfile(Argument):
            Message = '{Argument} is not a valid file.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Argument

    #
    # Create command line argument parser object
    #
    parser = argparse.ArgumentParser (
                        prog = __prog__,
                        description = __description__ + __copyright__,
                        )

    parser.add_argument("ProductFwName", type = ValidateProductFwName)
    parser.add_argument("CapsuleVersionDotString", type = str)
    parser.add_argument("ProductFmpGuid", type = ValidateGuidFormat)
    parser.add_argument("CapsuleFileName", type = str)
    parser.add_argument("CapsuleVersionHexString", type = ValidateHexString)
    parser.add_argument("ProductFwProvider", type = str)
    parser.add_argument("ProductFwMfgName", type = str)
    parser.add_argument("ProductFwDesc", type = str)
    parser.add_argument("PfxFileName", type = str)

    args, remaining = parser.parse_known_args ()

    GenerateInf (args)
    GenWindowsCapsule (args)