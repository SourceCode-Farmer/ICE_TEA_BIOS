Only generate MicrocodeVersion.data:


Parameters:
  a) -o  $(WORKSPACE)\MicrocodeVersion.data
  a) --fw-version <UINT32>
  b) --lsv <UINT32>
  c) --fw-version-string <CHAR16 string>


Example:
GenMicrocodeVersion.exe -o %WORKSPACE%\Intel\RocketLake\RocketLakePlatSamplePkg\Features\CapsuleUpdate\Tools\NewGenCap\PyScript\MicrocodeVersion.data --fw-version 0x0001 --lsv 0x0001 --fw-version-string "Version 0.0.0.1"

