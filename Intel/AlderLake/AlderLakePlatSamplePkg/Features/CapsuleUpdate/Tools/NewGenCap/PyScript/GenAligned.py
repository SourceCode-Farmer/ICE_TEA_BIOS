#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Convert multiple input files into an output file with contents of each input
# file at a specified alignment.
#
# Copyright (c) 2019, Intel Corporation. All rights reserved.<BR>
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution.  The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#

'''
GenAligned
'''
from __future__ import print_function

import sys
import argparse

#
# Globals for help information
#
__prog__        = 'GenAligned'
__copyright__   = 'Copyright (c) 2019, Intel Corporation. All rights reserved.'
__description__ = 'Convert multiple input files into an output file with contents of each input file at a specified alignment. \n'


AlignmentChoices = {
  '1'    : 1,
  '2'    : 2,
  '4'    : 4,
  '8'    : 8,
  '16'   : 16,
  '128'  : 128,
  '512'  : 512,
  '1K'   : 1024,
  '4K'   : 4096,
  '32K'  : 32768,
  '64K'  : 65536,
  '128K' : 0x20000,
  '256K' : 0x40000,
  '512K' : 0x80000,
  '1M'   : 0x100000,
  '2M'   : 0x200000,
  '4M'   : 0x400000,
  '8M'   : 0x800000,
  '16M'  : 0x01000000
}

if __name__ == '__main__':
    def convert_arg_line_to_args(arg_line):
        for arg in arg_line.split():
            if not arg.strip():
                continue
            yield arg

    def Validate32BitInteger (Argument):
        try:
            Value = int (Argument, 0)
        except:
            Message = '{Argument} is not a valid integer value.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        if Value < 0:
            Message = '{Argument} is a negative value.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        if Value > 0xffffffff:
            Message = '{Argument} is larger than 32-bits.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Value

    #
    # Create command line argument parser object
    #
    parser = argparse.ArgumentParser (prog = __prog__,
                                      description = __description__ + __copyright__,
                                      conflict_handler = 'resolve',
                                      fromfile_prefix_chars = '@'
                                      )
    parser.convert_arg_line_to_args = convert_arg_line_to_args

    #
    # Add required arguments for this command
    #
    #[-start-201230-IB16810143-modify]#
    parser.add_argument ("-i", "--input", dest = 'InputFile', type = argparse.FileType ('rb'), action='append',
                         help = "Input binary filename.  Multiple input files are combined into a single PCD.")
    parser.add_argument ("-o", "--output", dest = 'OutputFile', type = argparse.FileType ('wb'),
                         help = "Output filename for PCD value or PCD statement")
    parser.add_argument ("-a", "--alignment", dest = 'Alignment', choices = AlignmentChoices.keys(),
                         help = 'Alignment applied to each input file .')
    #[-end-201230-IB16810143-modify]#
    #
    # Add optional arguments common to all operations
    #
    parser.add_argument ("-v", "--verbose", dest = 'Verbose', action = "store_true",
                         help = "Increase output messages")
    parser.add_argument ("-q", "--quiet", dest = 'Quiet', action = "store_true",
                         help = "Reduce output messages")
    parser.add_argument ("--debug", dest = 'Debug', type = int, metavar = '[0-9]', choices = range (0, 10), default = 0,
                         help = "Set debug level")

    #
    # Parse command line arguments
    #
    args = parser.parse_args ()
    args.Alignment = AlignmentChoices[args.Alignment]

    #
    # Read all binary input files and pad to specified alignment with 0xFF
    #
    Buffer = b''
    for File in args.InputFile:
        try:
            Buffer = Buffer + File.read ()
            File.close ()
#[-start-210607-IB11790419-modify]#
            Remainder = len(Buffer) % args.Alignment
            if Remainder != 0:
                Buffer = Buffer + b'\xff' * (args.Alignment - Remainder)
#[-end-210607-IB11790419-modify]#
        except:
            print ('GenAligned: error: can not read binary input file {File}'.format (File = File))
            sys.exit (1)

    #
    # Write output file
    #
    args.OutputFile.write (Buffer)
    args.OutputFile.close ()
