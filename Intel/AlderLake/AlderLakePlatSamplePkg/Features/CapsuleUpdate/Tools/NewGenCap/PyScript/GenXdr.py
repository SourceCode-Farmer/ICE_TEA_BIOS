## @file
# Convert multiple input files into an output file encoded using the
# Variable-Length Opaque Data format of RFC 4506 External Data Representation
# Standard (XDR).
#
# Copyright (c) 2019, Intel Corporation. All rights reserved.<BR>
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution.  The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#

'''
GenXdr
'''
from __future__ import print_function

import sys
import argparse
import re
import xdrlib

#
# Globals for help information
#
__prog__        = 'GenXdr'
__copyright__   = 'Copyright (c) 2019, Intel Corporation. All rights reserved.'
__description__ = 'Convert multiple input files into an output file encoded using the Variable-Length Opaque Data format of RFC 4506 External Data Representation Standard (XDR)\n'

if __name__ == '__main__':
    #
    # Create command line argument parser object
    #
    parser = argparse.ArgumentParser (prog = __prog__,
                                      description = __description__ + __copyright__,
                                      conflict_handler = 'resolve')
    parser.add_argument ("-i", "--input", dest = 'InputFile', type = argparse.FileType ('rb'), action='append', required = True,
                         help = "Input binary filename.  Multiple input files are combined into a single PCD.")
    parser.add_argument ("-o", "--output", dest = 'OutputFile', type = argparse.FileType ('wb'), required = True,
                         help = "Output filename for PCD value or PCD statement")
    parser.add_argument ("-v", "--verbose", dest = 'Verbose', action = "store_true",
                         help = "Increase output messages")
    parser.add_argument ("-q", "--quiet", dest = 'Quiet', action = "store_true",
                         help = "Reduce output messages")
    parser.add_argument ("--debug", dest = 'Debug', type = int, metavar = '[0-9]', choices = range (0, 10), default = 0,
                         help = "Set debug level")

    #
    # Parse command line arguments
    #
    args = parser.parse_args ()

    #
    # Read all binary input files and encode in XDR format
    #
    XdrEncoder = xdrlib.Packer ()
    InputFileName = []
    for File in args.InputFile:
        try:
            InputFileName.append(File.name)
            print ('GenXdr: Read binary input file {File}'.format (File = InputFileName))
            XdrEncoder.pack_bytes (File.read ())
            File.close ()
        except:
            print ('GenXdr: error: can not read binary input file {File}'.format (File = InputFileName))
            sys.exit (1)

    #
    # Write output file in XDR format
    #
    args.OutputFile.write (XdrEncoder.get_buffer ())
    args.OutputFile.close ()
