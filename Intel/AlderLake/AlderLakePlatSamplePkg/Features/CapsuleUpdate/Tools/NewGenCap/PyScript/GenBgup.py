import os
import sys
import argparse
import subprocess
import glob
import shutil
import struct
import xdrlib

#
# Globals for help information
#
__prog__        = sys.argv[0]
__copyright__   = 'Copyright (c) 2019, Intel Corporation. All rights reserved.'
__description__ = 'Generate BGUP content capsules.\n'

def ProcessCommand(Command):
    Process = subprocess.Popen(Command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    ProcessOutput = Process.communicate()
    if Process.returncode != 0:
        print (ProcessOutput)
        print (Process.returncode)
        print (Command)
        assert (Process.returncode == 0)

if __name__ == "__main__":
    def Validate32BitInteger (Argument):
        try:
            Value = int (Argument, 0)
        except:
            Message = '{Argument} is not a valid integer value.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        if Value < 0:
            Message = '{Argument} is a negative value.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        if Value > 0xffffffff:
            Message = '{Argument} is larger than 32-bits.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Value

    parser = argparse.ArgumentParser (
                        prog = __prog__,
                        description = __description__ + __copyright__,
                        )

    parser.add_argument ("-o", "--output", dest = 'Output', required = True,
                         help = 'The size of each slot.')
    parser.add_argument ("--bios-svn", dest = 'BiosSvn', type = Validate32BitInteger, required = True,
                         help = 'Bios Svn')
    parser.add_argument ("--microcode-fv", dest = 'MicrocodeFv', required = True,
                         help = 'MicrocodeFv.fv')

    args, remaining = parser.parse_known_args ()

    BgupTool = os.path.join (
                    os.environ["WORKSPACE_PLATFORM"],
                    os.environ["PLATFORM_FULL_PACKAGE"],
                    "InternalOnly/ToolScripts/BiosGuard/UpdatePackage"
                    )

    #
    # Path for Input
    #
    Input_Bgsl2Bin          = os.path.join(BgupTool, "bgsl2bin.exe")
    Input_Bgsl_Template     = os.path.join(BgupTool, "script_BuildBGUP_template_TopSwapUpdate.bgsl")
    Input_Bgpb              = os.path.join(BgupTool, "BGPB.exe")
    Input_SignBiosGuard     = os.path.join(BgupTool, "SignBiosGuard", "SignBiosGuard.exe")
    Input_PrivateKey        = os.path.join(BgupTool, "SignBiosGuard", "biosguard.pvk")
    Input_PublicKey         = os.path.join(BgupTool, "SignBiosGuard", "biosguard.pbk")
    Input_BiosGuardCapsule  = os.path.join(BgupTool, "BiosGuardCapsule.py")
    Input_CertHeader        = os.path.join(BgupTool, "CertHeader")

    #
    # Path for Output
    #
    Output_Script_Bin = os.path.join(args.Output, "script.data")
    Output_BgupHeader_Script_MicrocodeFv = os.path.join(args.Output, "BgupHeader_Script_MicrocodeFv.data")
    Output_SignedData = os.path.join(args.Output, "SignedData.data")
    Output_BgupHeader_Script = os.path.join(args.Output, "BgupHeader_Script.data")
    Output_BgupContent = os.path.join(args.Output, "BgupContent.data")

    #
    # Convert Bgsl from script to binary
    #
    bgsl2bin = '''
        {bgsl2bin}
        {script}
        {OUTPUT}
        '''
    Command = bgsl2bin.format (
                bgsl2bin = Input_Bgsl2Bin,
                script = Input_Bgsl_Template,
                OUTPUT = Output_Script_Bin
                )
    ProcessCommand(' '.join(Command.splitlines()).strip())

    #
    # Generate Bgup header and combine the generated bgup header, script.bin and MicrocodeFv.fv
    #
    bgpb = '''
        {bgpb}
        -bios_svn {SVN}
        -use_sfam true
        -use_ftu true
        -platform_id {PlatformId}
        -script {Script}
        -data {Data}
        '''
    assert (os.environ.get("TARGET_PLATFORM") != None)
    PlatformString = '"' + os.environ["TARGET_PLATFORM"].upper() + '"'
    Command = bgpb.format (
                bgpb = Input_Bgpb,
                SVN = args.BiosSvn,
                PlatformId = PlatformString,
                Script = Output_Script_Bin,
                Data = args.MicrocodeFv
                )
    ProcessCommand(' '.join(Command.splitlines()).strip())
    shutil.move("update_package.BIOS_Guard", Output_BgupHeader_Script_MicrocodeFv)

    #
    # Generate signed data
    #
    SignBiosGuard = '''
        {SignBiosGuard}
        {BgupHeader_Script_MicrocodeFv}
        {Output}
        -K {PrivateKey} {PublicKey}
        '''
    Command = SignBiosGuard.format (
                SignBiosGuard = Input_SignBiosGuard,
                BgupHeader_Script_MicrocodeFv = Output_BgupHeader_Script_MicrocodeFv,
                Output = Output_SignedData,
                PrivateKey = Input_PrivateKey,
                PublicKey = Input_PublicKey
                )
    ProcessCommand(' '.join(Command.splitlines()).strip())


    #
    # Extract BgupHeader and Script.
    #
    BgupHeader_Script = '''
        py -3 {BiosGuardCapsule}
        -i {BgupHeader_Script_MicrocodeFv}
        -o {Output}
        '''
    Command = BgupHeader_Script.format (
                BiosGuardCapsule = Input_BiosGuardCapsule,
                BgupHeader_Script_MicrocodeFv = Output_BgupHeader_Script_MicrocodeFv,
                Output = Output_BgupHeader_Script
                )
    ProcessCommand(' '.join(Command.splitlines()).strip())

    #
    # Combine BgupHeader + Script + CertHeader + SignedData
    #
    Buffer = b''
    FileList = [
        Output_BgupHeader_Script,
        Input_CertHeader,
        Output_SignedData
    ]

    for File in FileList:
        with open(File, "rb") as file:
            Buffer += file.read ()

    with open(Output_BgupContent, "wb") as file:
        file.write(Buffer)