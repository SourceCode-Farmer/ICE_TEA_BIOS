#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Generate RAW data for Version FFS file with 32-bit version, 32-bit lowest
# supported version, and null-terminated Unicode version string.
#
# Copyright (c) 2019, Intel Corporation. All rights reserved.<BR>
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution.  The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#

'''
GenerateVersionFfsRawData
'''

import argparse
import struct
#[-start-201130-IB16810140-add]#
import os
import re
import sys
#[-end-201130-IB16810140-add]#
#
# Globals for help information
#
__prog__        = 'GenMicrocodeVersion'
__copyright__   = 'Copyright (c) 2019, Intel Corporation. All rights reserved.'
__description__ = 'Generate RAW data contents with verion, LVS, and Version Stting.\n'

#[-start-201130-IB16810140-add]#
def DoAddPaddingData(File, SlotSize):
  with open(File, "rb") as fd:
    Binary = fd.read()
    if len(Binary) > SlotSize:
      print (("Microcode patch %s is larger than slot size(%d)\n") % (File, SlotSize))
      assert (False)
    elif len(Binary) < SlotSize:
      FileBackUp = File.replace(os.path.splitext(File)[1], os.path.splitext(File)[1] + "_backup")
      with open(FileBackUp, "wb") as backup:
        backup.write(Binary)
      Binary = Binary + b'\xff' * (SlotSize - len(Binary))

  with open(File, "wb") as fd:
    fd.write(Binary)

def GetRealFilePath(Path):
  if len(Path.split()) > 1:
    FilePath = Path.split()[-1]
  else:
    FilePath = Path
  index = 0
  while True:
    index1 = FilePath.find("$(", index)
    if index1 == -1:
      break
    index2 = FilePath.find(")",index1)
    FilePath = FilePath.replace(FilePath[index1:index2+1], os.environ[FilePath[index1+2:index2]])
    index = index2 + 1
  return FilePath
#[-end-201130-IB16810140-add]#

if __name__ == '__main__':
    def convert_arg_line_to_args(arg_line):
        for arg in arg_line.split():
            if not arg.strip():
                continue
            yield arg

    def Validate32BitInteger (Argument):
        try:
            Value = int (Argument, 0)
        except:
            Message = '{Argument} is not a valid integer value.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        if Value < 0:
            Message = '{Argument} is a negative value.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        if Value > 0xffffffff:
            Message = '{Argument} is larger than 32-bits.'.format (Argument = Argument)
            raise argparse.ArgumentTypeError (Message)
        return Value

    #
    # Create command line argument parser object
    #
    parser = argparse.ArgumentParser (
                        prog = __prog__,
                        description = __description__ + __copyright__,
                        conflict_handler = 'resolve',
                        fromfile_prefix_chars = '@'
                        )
    parser.convert_arg_line_to_args = convert_arg_line_to_args

    #
    # Add required arguments for this command
    #
    #[-start-201130-IB16810140-modify]#
    parser.add_argument("-o", "--output", dest = 'OutputFile', type = argparse.FileType('wb'),
                        help = "Output filename.")
    parser.add_argument ("--fw-version", dest = 'FwVersion', type = Validate32BitInteger,
                         help = 'The 32-bit version of the binary payload (e.g. 0x11223344 or 5678).')
    parser.add_argument ("--lsv", dest = 'LowestSupportedVersion', type = Validate32BitInteger,
                         help = 'The 32-bit lowest supported version of the binary payload (e.g. 0x11223344 or 5678).')
    parser.add_argument ("--fw-version-string", dest = 'VersionString',
                         help = 'The version string of the binary payload (e.g. "Version 0.1.2.3").')
    #[-end-201130-IB16810140-modify]#
    #
    # Add optional arguments common to all operations
    #
    parser.add_argument ("-v", "--verbose", dest = 'Verbose', action = "store_true",
                         help = "Turn on verbose output with informational messages printed, including capsule headers and warning messages.")
    parser.add_argument ("-q", "--quiet", dest = 'Quiet', action = "store_true",
                         help = "Disable all messages except fatal errors.")
    parser.add_argument ("--debug", dest = 'Debug', type = int, metavar = '[0-9]', choices = range (0, 10), default = 0,
                         help = "Set debug level")
    #[-start-201130-IB16810140-add]#
    parser.add_argument ("-p", dest = 'padding', action = "store_true",
                         help = "Padding microcode")
    parser.add_argument ("-gu", dest = 'GenUcode', action = "store_true",
                         help = "Generate microcode")
    #[-end-201130-IB16810140-add]#
    #
    # Parse command line arguments
    #
    args, remaining = parser.parse_known_args()

    #[-start-201130-IB16810140-add]#
    if args.GenUcode:
        PROJECT_BOARD_PACKAGE  = os.path.join(os.environ["WORKSPACE"], os.environ["PROJECT_BOARD_PACKAGE"])
        MICROCODE_FV_FDF       = os.path.join(PROJECT_BOARD_PACKAGE, "CapsuleUpdate", "MicrocodeFv.fdf")
        SlotSize               = int(os.environ["SLOT_SIZE"], 16)


        with open(MICROCODE_FV_FDF) as fd:
            data = fd.read()

        result = re.findall(r"FILE *RAW *= *197DB236-F856-4924-90F8-CDF12FB875F3", data)
        #
        # assumption: just one fv for microcode
        #
        assert(len(result) == 1)

        #
        # Find FILE RAW section according 197DB236-F856-4924-90F8-CDF12FB875F3 GUID
        #
        index1 = data.find(result[0])
        index2 = data.find("}", index1)
        count = data[index1:index2].count("{") - 1
        while count > 0:
            index2 = data.find("}", index2 + 1)
        index2 = index2 + 1

        for line in data[index1:index2].split("\n"):
            line = line.strip("\n").strip()

            if line.find("#") != -1:
                index1 = line.find("#")
                index2 = len(line)
                line = line.replace(line[index1:index2],"")

            if len(line) == 0:
                continue

            line = line.strip("\n").strip()
            if line[-4:] not in [".pdb", ".mcb", ".bin"]:
                continue

            if SlotSize != 0 and args.padding:
                DoAddPaddingData(os.path.join(os.environ["WORKSPACE_PLATFORM"], GetRealFilePath(line)), SlotSize)


    if args.FwVersion:
    #[-end-201130-IB16810140-add]#
        #[-start-201130-IB16810140-modify]#
        #
        # Generate buffer with FFS Version file RAW Data
        #
        RawData = b''
        RawData = RawData + struct.pack('<I', args.FwVersion)
        RawData = RawData + struct.pack('<I', args.LowestSupportedVersion)
        RawData = RawData + args.VersionString.encode('utf-16-le')
        RawData = RawData + b'\0\0'

        #
        # Write output file
        #
        args.OutputFile.write(RawData)
        args.OutputFile.close()
        #[-end-201130-IB16810140-modify]#

