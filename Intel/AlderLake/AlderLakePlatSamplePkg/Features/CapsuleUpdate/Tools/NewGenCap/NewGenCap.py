#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
# This script auto gens uCode Capsule
# uCode Capsule is a FMP capsule with a XDR format payload
# XDR format payload carries
#       1. uCode FV
#       2. Full range BgupScript
#       3. uCode Version FFS
#       4. uCode Arrary
#
import os
import sys
import argparse
import subprocess
import glob
import shutil
import struct
import xdrlib
import re

gIntelMicrocodeFirmwareVolumeGuid   = '9BC06401-13CE-4A6A-B2CD-31D874793B70'
gIntelMicrocodeVersionFfsFileGuid   = 'E0562501-B41B-4566-AC0F-7EA8EE817F20'
gIntelMicrocodeArrayFfsFileGuid     = '197DB236-F856-4924-90F8-CDF12FB875F3'
gFmpDevicePlatformuCodeGuid         = '69585D92-B50A-4AD7-B265-2EB1AE066574'
# gIntelFmpCapsuleGuid                = '96D4FDCD-1502-424D-9D4C-9B12D2DCAE5C'

#
# Globals for help information
#
__prog__        = sys.argv[0]
__copyright__   = 'Copyright (c) 2019, Intel Corporation. All rights reserved.'
__description__ = 'Generate Microcode capsules.\n'

#
# We expect this script be called by capsulebuild.bat in XXPlatSamplePkg folder.
# If you call this script by directly, you should know what SVN you are using.
# As general, this SVN should be equal with BIOS_SVN in capsulebuild.bat script.
#
SVN = 0x23570000
def RemoveTempDirect(Target):
    if os.path.exists(Target):
        shutil.rmtree(Target)

def CreateTempDirect(Target):
    RemoveTempDirect(Target)
    os.makedirs(Target)

#[-start-201130-IB16810140-remove]#
#def GenMicrocodeArrayRawData(uCodeFileList, Output, Trim = False):
#[-end-201130-IB16810140-remove]#
#[-start-201130-IB16810140-add]#
def GenMicrocodeArrayRawData(uCodeFileList, Output):
#[-end-201130-IB16810140-add]#
    Buffer = b''
    for File in uCodeFileList:
        with open(File, "rb") as fd:
          #[-start-201130-IB16810140-remove]#
          #  MicrocodeBinary = fd.read()
          #  if Trim:
          #      MicrocodeTotalSize = struct.unpack("I", MicrocodeBinary[32:36])[0]
          #      MicrocodeBinary = MicrocodeBinary[:MicrocodeTotalSize]
          #  Buffer = Buffer + MicrocodeBinary
          #[-end-201130-IB16810140-remove]#
          #[-start-201130-IB16810140-add]#
          Buffer = Buffer + fd.read()
          #[-end-201130-IB16810140-add]#
    with open(Output, "wb") as fd:
        fd.write(Buffer)

def ProcessCommand(Command):
    Process = subprocess.Popen(Command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    ProcessOutput = Process.communicate()
    if Process.returncode != 0:
        print (Command)
        print (ProcessOutput[1].decode())
        assert (False)

#
#   Split FlashMap content with target
#
#   group(1) => !if $(TARGET) ==
#   group(2) => FlashMap content
#   group(3) => !else
#   group(4) => FlashMap content
#   group(5) => !endif
#
#   group(2) is either debug or release
#   This is depends on the flag before $(TARGET)
#   group(4) same as group(2)
#
#   Target must be value as "debug" or "release" or
#   None if we expect the FlashMap has only one section
#
def SplitFlashMap(FlashMap, Target = None):
    with open(FlashMap) as fd:
      FlashMapString = fd.read()

    SearchGroup = re.search(r"(!if \$\(TARGET\) *== *[a-zA-Z]*)(.*)(!else)(.*)(!endif)", FlashMapString, re.I | re.DOTALL)

    if SearchGroup != None:
        assert (len(SearchGroup.groups()) == 5)
        assert (Target != None)
        assert (Target in ["debug", "release"])
        Type = SearchGroup.group(1).split("==")[1].strip().lower()
        if Type == Target:
            return SearchGroup.group(2)
        else:
            return SearchGroup.group(4)
    else:
        return FlashMapString

#
# Try to update the size of MicrocodeFv in capsule
# accoring to the size of MicrocodeFv from flash map
#
def UpdateMicrocodeFv(FlashMap, Target):
    if not os.path.isfile(FlashMap):
        print ("#################################################")
        print ("#                                               #")
        print ("#                                               #")
        print ("# WARNING: Can't find FlashMapIncludeRes.fdf!!! #")
        print ("#                                               #")
        print ("#                                               #")
        print ("#################################################")
        assert (False)

    FlashMapContent = SplitFlashMap(FlashMap, Target)
    #
    # Get FvMicrocode size from FlashMap and update this value to MicrocodeFv.fdf
    # The MicrocodeFv.fdf is used to create Microcode capsule
    #
    FvBlocks = None
    for line in FlashMapContent.split("\n"):
        if "PcdFlashFvMicrocodeSize" in line:
            FvSize = line.split("=")[1].strip().split()[0]
            FvSize = int(FvSize, 16)
            assert (FvSize % 0x10000 == 0)
            FvBlocks = str(hex(FvSize // 0x10000))
            break
    assert (FvBlocks != None)

    with open(MICROCODE_FV_FDF) as fd:
        string = fd.read().replace("{FvBlocks}", FvBlocks)

    with open(MICROCODE_FV_FDF, "w") as fd:
        fd.write(string)

def UpdateBgslScript(FlashMap, Bgsl, Target):
    assert (os.path.isfile(FlashMap))
    assert (os.path.isfile(Bgsl))

    string = ""
    with open(Bgsl) as fd:
        string = fd.read()

    FlashMapContent = SplitFlashMap(FlashMap, Target)
    MicrocodeFvOffset = None
    MicrocodeFvSize = None
    IbbOffset = None
    IbbROffset = None
    for line in FlashMapContent.split("\n"):
        if "gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeOffset" in line:
            MicrocodeFvOffset = int(line.split("=")[1].strip().split()[0],16)
        if "gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeSize" in line:
            MicrocodeFvSize = int(line.split("=")[1].strip().split()[0],16)
        if "gBoardModuleTokenSpaceGuid.PcdFlashIbbOffset" in line:
            IbbOffset = int(line.split("=")[1].strip().split()[0],16)
        if "gBoardModuleTokenSpaceGuid.PcdFlashIbbROffset" in line:
            IbbROffset = int(line.split("=")[1].strip().split()[0],16)

    string = string.replace("{IBB_MicrocodeFv_Base}", hex(MicrocodeFvOffset))
    string = string.replace("{IBB_MicrocodeFv_Size}", hex(MicrocodeFvSize))
    string = string.replace("{IBBR_MicrocodeFv_Base}", hex(IbbROffset + (MicrocodeFvOffset - IbbOffset)))

    with open(Bgsl, "w") as fd:
        fd.write(string)

def GenMicrocodeFv(Fdf, Dsc, Output):
    GenFvCommand = '''
        call {GenFds}
        -f {fdf}
        -p {dsc}
        -i {Section}
        -o {OutputDir}
        '''
    #[-start-201130-IB16810140-modify]#
    Command = GenFvCommand.format (
                GenFds = "genfds.exe",
                fdf = Fdf,
                dsc = Dsc,
                Section = "CapsulePayloaduCode",
                OutputDir = Output
                )
    #[-end-201130-IB16810140-modify]#
    ProcessCommand(' '.join(Command.splitlines()).strip())

def GenBgupSignedData(BgupDirectory, MicrocodeFv):
    GenSignedDataCommand = '''
        call {BuildBGUP_SPI}
        -d {MicrocodeFv}
        -p {Bgsl}
        -v {BIOS_SVN}
        '''
    Command = GenSignedDataCommand.format (
                BuildBGUP_SPI = os.path.join(BgupDirectory, "BuildBGUP_SPI.bat"),
                MicrocodeFv = MicrocodeFv,
                Bgsl = os.path.join(BgupDirectory, "script_BuildBGUP_template_TopSwapUpdate.bgsl"),
                BIOS_SVN = SVN
                )
    ProcessCommand(' '.join(Command.splitlines()).strip())

def GetRealFilePath(Path):
    if len(Path.split()) > 1:
        FilePath = Path.split()[-1]
    else:
        FilePath = Path
    index = 0
    while True:
        index1 = FilePath.find("$(", index)
        if index1 == -1:
            break
        index2 = FilePath.find(")",index1)
        FilePath = FilePath.replace(FilePath[index1:index2+1], os.environ[FilePath[index1+2:index2]])
        index = index2 + 1
    return FilePath

def GetMicrocodeListFromFdf(File):
    with open(File, "r") as fd:
        data = fd.read()

    result = re.findall(r"FILE *RAW *= *197DB236-F856-4924-90F8-CDF12FB875F3", data)
    #
    # assumption: just one fv for microcode
    #
    assert(len(result) == 1)

    #
    # Find FILE RAW section according 197DB236-F856-4924-90F8-CDF12FB875F3 GUID
    #
    index1 = data.find(result[0])
    index2 = data.find("}", index1)
    count = data[index1:index2].count("{") - 1
    while count > 0:
        index2 = data.find("}", index2 + 1)
    index2 = index2 + 1

    PatchList = []
    for line in data[index1:index2].split("\n"):
        line = line.strip("\n").strip()

        if line.find("#") != -1:
            index1 = line.find("#")
            index2 = len(line)
            line = line.replace(line[index1:index2],"")

        if len(line) == 0:
            continue

        line = line.strip("\n").strip()
        if line[-4:] not in [".pdb", ".mcb", ".bin"]:
            continue

        PatchList.append(os.path.join(os.environ["WORKSPACE_PLATFORM"], GetRealFilePath(line)))
    return PatchList

if __name__ == "__main__":
    #[-start-201130-IB16810140-remove]#
    #def Validate32BitInteger (Argument):
    #    try:
    #        Value = int (Argument, 0)
    #    except:
    #        Message = '{Argument} is not a valid integer value.'.format (Argument = Argument)
    #        raise argparse.ArgumentTypeError (Message)
    #    if Value < 0:
    #        Message = '{Argument} is a negative value.'.format (Argument = Argument)
    #        raise argparse.ArgumentTypeError (Message)
    #    if Value > 0xffffffff:
    #        Message = '{Argument} is larger than 32-bits.'.format (Argument = Argument)
    #        raise argparse.ArgumentTypeError (Message)
    #    return Value

    #def ValidateSlotSize (Argument):
    #    try:
    #        Value = int (Argument, 0)
    #    except:
    #        Message = '{Argument} is not a valid integer value.'.format (Argument = Argument)
    #        raise argparse.ArgumentTypeError (Message)
    #    if Value % (4096) != 0:
    #        Message = '{Argument} must be a multiple of 4KB'.format (Argument = Argument)
    #        raise argparse.ArgumentTypeError (Message)
    #    return Value
    #[-end-201130-IB16810140-remove]#

    #
    # Create command line argument parser object
    #
    parser = argparse.ArgumentParser (
                        prog = __prog__,
                        description = __description__ + __copyright__,
                        )

    #
    # For MicrocodeVersion.ffs
    #
    #[-start-201130-IB16810140-remove]#
    #parser.add_argument ("--fw-version", dest = 'FwVersion', type = Validate32BitInteger,
    #                     help = 'The 32-bit version of the binary payload (e.g. 0x11223344 or 5678).')
    #parser.add_argument ("--lsv", dest = 'LowestSupportedVersion', type = Validate32BitInteger,
    #                     help = 'The 32-bit lowest supported version of the binary payload (e.g. 0x11223344 or 5678).')
    #parser.add_argument ("--fw-version-string", dest = 'VersionString',
    #                     help = 'The version string of the binary payload (e.g. "Version 0.1.2.3").')
    #[-end-201130-IB16810140-remove]#
    #
    # For debug or release.
    # This input is not mandatory, to makes this is a compatibility change.
    # If the platform use new FlashMap, should change input for this tool.
    #
    parser.add_argument ("-b", "--build", dest = "Build", choices = ["debug", "release"],
                         help = "indicate capsule type, debug or release")

    #
    # Support Slot,Bgup,Full mode
    #
    #[-start-201130-IB16810140-remove]#
    #parser.add_argument ("-m", "--mode", dest = 'Mode', choices = ["ucodebgup", "ucodefull", "ucodeslot"], required = True,
    #                     help = 'Capsule mode')
    #[-end-201130-IB16810140-remove]#
    args, remaining = parser.parse_known_args ()

    FwVersion = os.environ["FW_VERSION"]
    Lsv = os.environ["LSV"]
    FwVersionString = os.environ["FW_VERSION_STRING"]
    SlotSize = int(os.environ["SLOT_SIZE"], 16)
    #[-start-201130-IB16810140-add]#
    UcodeMode = os.environ["UCODE_MODE"]  
    #[-end-201130-IB16810140-add]#
    #[-start-201130-IB16810140-remove]#
    #if args.FwVersion != None:
    #    FwVersion = str(hex(args.FwVersion))
    #if args.LowestSupportedVersion != None:
    #    Lsv = str(hex(args.LowestSupportedVersion))
    #if args.VersionString != None:
    #    FwVersionString = str(args.VersionString)
    #
    #BuildTarget = None
    #if args.Build != None:
    #    BuildTarget = args.Build
    #
    #if os.environ.get("BIOS_SVN") != None:
    #  SVN = int("0x" + os.environ.get("BIOS_SVN") + "0000",16)
    #elif args.Mode.lower() == "ucodebgup" or args.Mode.lower() == "ucodefull":
    #    print ("NewGenCap.py: error: Please assign BIOS_SVN by adding -biossvn <bios svn>.")
    #    print ("It's mandatory when building bgup and full mode ucode capsule.")
    #    exit (-1)
    #
    # Init folder and path
    #[-start-201130-IB16810140-modify]#
    #
    CWD                         = os.getcwd()    # %WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\NewGenCap
    PROJECT_BOARD_PACKAGE       = os.path.join(os.environ["WORKSPACE"], os.environ["PROJECT_BOARD_PACKAGE"]) #PROJECT_BOARD_PACKAGE=Board\Intel\%PROJECT_PKG%\
    TEMP_FOLDER                 = os.path.join(PROJECT_BOARD_PACKAGE, "CapsuleUpdate", "Temp")
    SCRIPT_FOLDER               = os.path.join(CWD, "PyScript")
    OUTOUT_FOLDER               = os.path.join(PROJECT_BOARD_PACKAGE, "CapsuleUpdate", "Output")
    #WINDOWS_CAP_FOLDER          = os.path.join(PROJECT_BOARD_PACKAGE, "WindowsCapsule")
    #WINDOWS_CAP_OUTPUT_FOLDER   = os.path.join(OUTOUT_FOLDER, "WindowsCapsule")

    MICROCODE_VERSION_FFS       = os.path.join(PROJECT_BOARD_PACKAGE, "CapsuleUpdate", "Binary", "MicrocodeVersion.data")
    MICROCODE_ARRAY_FFS         = os.path.join(TEMP_FOLDER, "MicrocodeArray.data")

    MICROCODE_FV_FDF            = os.path.join(PROJECT_BOARD_PACKAGE, "CapsuleUpdate", "MicrocodeFv.fdf")
    MICROCODE_FV_DSC            = os.path.join(PROJECT_BOARD_PACKAGE, "CapsuleUpdate", "MicrocodeFv.dsc")
    #FLASH_MAP_FOR_RESILIENCY    = os.path.join(PLATFORM_BOARD_PACKAGE, "Include", "Fdf", "FlashMapIncludeRes.fdf")

    SCRIPT_PY_GENXDR            = os.path.join(SCRIPT_FOLDER, "GenXdr.exe")
    SCRIPT_PY_GENBGUP           = os.path.join(SCRIPT_FOLDER, "GenBgup.py")

    VERSION_TOOL                = os.path.join(SCRIPT_FOLDER, "GenMicrocodeVersion.exe")

    TOOLS_PATH                  = os.environ["TOOLS_PATH"]  # %WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\GenerateCapsule
    #[-end-201130-IB16810140-modify]#
    #BgupTool = os.path.join (
    #                os.environ["WORKSPACE_PLATFORM"],
    #                os.environ["PLATFORM_FULL_PACKAGE"],
    #                "InternalOnly/ToolScripts/BiosGuard/UpdatePackage"
    #                )
    #BgslTemplate = os.path.join(BgupTool, "script_BuildBGUP_template_TopSwapUpdate.bgsl")
    #BgslTemp_bak = os.path.join(TEMP_FOLDER, "script_BuildBGUP_template_TopSwapUpdate.bgsl")

    CreateTempDirect (TEMP_FOLDER)

    #shutil.copy(os.path.join("MicrocodeFv", "MicrocodeFv.dsc"), TEMP_FOLDER)
    #shutil.copy(os.path.join("MicrocodeFv", "MicrocodeFv.fdf"), TEMP_FOLDER)

    #
    # Update MicrocodeFv.fdf file
    #
    #UpdateMicrocodeFv (FLASH_MAP_FOR_RESILIENCY, BuildTarget)
    #[-start-201130-IB16810140-modify]#
    #
    # Call microcode_padding.py
    #
    #PaddingCommand = '''
    #            py -3 microcode_padding.py
    #            --opt padding
    #            --fw-version {FW_VERSION}
    #            --lsv {LSV}
    #            --fw-version-string {FW_VERSION_STRING}
    #            --slotsize {SLOT_SIZE}
    #            --fdf {FDF}
    #            '''
    #Command = PaddingCommand.format (
    #            FW_VERSION = FwVersion,
    #            LSV = Lsv,
    #            FW_VERSION_STRING = FwVersionString,
    #            SLOT_SIZE = str(hex(SlotSize)),
    #            FDF = MICROCODE_FV_FDF
    #            )
    #os.chdir(PLATFORM_BOARD_PACKAGE)
    #os.system(' '.join(Command.splitlines()).strip())
    #os.chdir(CWD)
    if UcodeMode == "ucodefull":
        os.chdir(SCRIPT_FOLDER)
        os.system(VERSION_TOOL + " -o " + MICROCODE_VERSION_FFS + " --fw-version " + FwVersion + " --lsv " + Lsv +  " --fw-version-string " + FwVersionString + " -p" + " -gu")
        os.chdir(CWD)
    elif UcodeMode == "ucodeslot":
        os.chdir(SCRIPT_FOLDER)
        os.system(VERSION_TOOL + " -o " + MICROCODE_VERSION_FFS + " --fw-version " + FwVersion + " --lsv " + Lsv +  " --fw-version-string " + FwVersionString + " -gu")
        os.chdir(CWD)
    #[-end-201130-IB16810140-modify]#
    #
    # Genearte MicrocodeFv
    #
    GenMicrocodeFv (MICROCODE_FV_FDF, MICROCODE_FV_DSC, TEMP_FOLDER)

    #
    # Insert XDR structure
    #
    #[-start-201130-IB16810140-modify]#
    if UcodeMode == "ucodefull":
        GenXdr = '''
            call {GenXdr}
            -i {MicrocodeFv}
            -o {Output}
            '''
    #[-end-201130-IB16810140-modify]#
        Command = GenXdr.format (
                    GenXdr = SCRIPT_PY_GENXDR,
                    MicrocodeFv = os.path.join(TEMP_FOLDER, "FV\CAPSULEPAYLOADUCODE.Fv"),
                    Output = os.path.join(TEMP_FOLDER, "XdrMicrocodeFv.data")
                    )
        ProcessCommand(' '.join(Command.splitlines()).strip())

        CapInput  = os.path.join(TEMP_FOLDER, "XdrMicrocodeFv.data")
        CapOutput = os.path.join(OUTOUT_FOLDER, "uCodeFull.cap")
    #[-start-201130-IB16810140-modify]#
    elif UcodeMode == "ucodebgup":
        #shutil.copy (BgslTemplate, BgslTemp_bak)
        #UpdateBgslScript (FLASH_MAP_FOR_RESILIENCY, BgslTemplate, BuildTarget)
        GenerateBgup = '''
                call {GenBgup}
                -o {Output}
                --bios-svn {BiosSvn}
                --microcode-fv {MicrocodeFv}
                '''
    #[-end-201130-IB16810140-modify]#
        Command = GenerateBgup.format (
                    GenBgup = SCRIPT_PY_GENBGUP,
                    Output = TEMP_FOLDER,
                    BiosSvn = SVN,
                    MicrocodeFv = os.path.join(TEMP_FOLDER, "FV\CAPSULEPAYLOADUCODE.Fv")
                    )
        ProcessCommand(' '.join(Command.splitlines()).strip())
        #shutil.copy (BgslTemp_bak, BgslTemplate)
        #[-start-201130-IB16810140-modify]#
        GenXdr = '''
            call {GenXdr}
            -i {MicrocodeFv}
            -i {BgupContent}
            -o {Output}
            '''
        #[-end-201130-IB16810140-modify]#
        Command = GenXdr.format (
                    GenXdr = SCRIPT_PY_GENXDR,
                    MicrocodeFv = os.path.join(TEMP_FOLDER, "FV\CAPSULEPAYLOADUCODE.Fv"),
                    BgupContent = os.path.join(TEMP_FOLDER, "BgupContent.data"),
                    Output = os.path.join(TEMP_FOLDER, "XdrMicrocodeFvXdrBgup.data")
                    )
        ProcessCommand(' '.join(Command.splitlines()).strip())

        CapInput  = os.path.join(TEMP_FOLDER, "XdrMicrocodeFvXdrBgup.data")
        CapOutput = os.path.join(OUTOUT_FOLDER, "uCodeBgup.cap")
    #[-start-201130-IB16810140-modify]#
    elif UcodeMode == "ucodeslot":
    #[-end-201130-IB16810140-modify]#
        MicrocodePathList = GetMicrocodeListFromFdf (MICROCODE_FV_FDF)
        if len(MicrocodePathList) != 1:
            print ("NewGenCap.py: error: Please keep one Microcode patch in FvMicrocode.fdf file! Remove redundant Microcode patch.")
            exit (-1)
    #[-start-201130-IB16810140-modify]#
        GenMicrocodeArrayRawData (MicrocodePathList, MICROCODE_ARRAY_FFS)
        GenXdr = '''
            call {GenXdr}
            -i {SlotVersionFfs}
            -i {SlotuCodeArray}
            -o {Output}
            '''
    #[-end-201130-IB16810140-modify]#
        Command = GenXdr.format (
                    GenXdr = SCRIPT_PY_GENXDR,
                    SlotVersionFfs = MICROCODE_VERSION_FFS,
                    SlotuCodeArray = MICROCODE_ARRAY_FFS,
                    Output = os.path.join(TEMP_FOLDER, "XdrSlotVersionXdrSlotuCodeArray.data")
                    )
        ProcessCommand(' '.join(Command.splitlines()).strip())

        buffer = struct.pack('II', 0, 0)
        with open(os.path.join(TEMP_FOLDER, "XdrSlotVersionXdrSlotuCodeArray.data"), "rb") as File:
            buffer += File.read()

        with open(os.path.join(TEMP_FOLDER, "XdrSlotVersionXdrSlotuCodeArray.data"), "wb") as File:
            File.write(buffer)

        CapInput  = os.path.join(TEMP_FOLDER, "XdrSlotVersionXdrSlotuCodeArray.data")
        CapOutput = os.path.join(OUTOUT_FOLDER, "uCodeSlot.cap")

    #
    # Create output folder
    #
    if not os.path.isdir(OUTOUT_FOLDER):
        CreateTempDirect(OUTOUT_FOLDER)

    #
    # Generate capsule file
    #
    GenerateCapsule = '''
        call {GenerateCapsule}
        --encode
        -v
        --guid {FMP_CAPSULE_GUID}
        --fw-version {FMP_CAPSULE_VERSION}
        --lsv {FMP_CAPSULE_LSV}
        --capflag PersistAcrossReset
        --capflag InitiateReset
        --capoemflag {FMP_CAPSULE_OEM_FLAG}
        --signing-tool-path {OpenSSL}
        --signer-private-cert {PrivateCert}
        --other-public-cert {PublicCert}
        --trusted-public-cert {TrustedPublicCert}
        -o {Output}
        {Input}
        '''
    #[-start-201130-IB16810140-modify]#
    Command = GenerateCapsule.format (
                GenerateCapsule = os.path.join(os.environ["TOOLS_PATH"], "GenerateCapsule.exe"),
                FMP_CAPSULE_GUID = gFmpDevicePlatformuCodeGuid,
                FMP_CAPSULE_VERSION = FwVersion,
                FMP_CAPSULE_LSV = Lsv,
                FMP_CAPSULE_OEM_FLAG = 0x0001,
                OpenSSL = os.path.join(os.environ["TOOLS_PATH"], "OpenSSL"),
                PrivateCert = os.path.join(os.environ["TOOLS_PATH"], "Pkcs7Sign\TestCert.pem"),
                PublicCert = os.path.join(os.environ["TOOLS_PATH"], "Pkcs7Sign\TestSub.pub.pem"),
                TrustedPublicCert = os.path.join(os.environ["TOOLS_PATH"], "Pkcs7Sign\TestRoot.pub.pem"),
                Output = CapOutput,
                Input = CapInput
                )
    #[-end-201130-IB16810140-modify]#
    ProcessCommand(' '.join(Command.splitlines()).strip())
    #[-start-201130-IB16810140-remove]#
    #
    # Gen Windows Capsule file
    #
    #shutil.copy (CapOutput, WINDOWS_CAP_FOLDER)
    #os.chdir(WINDOWS_CAP_FOLDER)
    #GenerateWindowsCapsule = '''
    #    py -3 {CreateWindowsCapsule}
    #    windowscapsule
    #    "{FMP_CAPSULE_STRING}"
    #    "{FMP_CAPSULE_GUID}"
    #    "{FMP_CAPSULE_FILE}"
    #    "{FMP_CAPSULE_VERSION}"
    #    "Intel"
    #    "Intel"
    #    "uCode"
    #    "SAMPLE_DEVELOPMENT.pfx"
    #    '''
    #Command = GenerateWindowsCapsule.format (
    #            CreateWindowsCapsule = os.path.join(WINDOWS_CAP_FOLDER, "CreateWindowsCapsule.py"),
    #            FMP_CAPSULE_STRING = FwVersionString.strip('"'),
    #            FMP_CAPSULE_GUID = gFmpDevicePlatformuCodeGuid,
    #            FMP_CAPSULE_FILE = CapOutput,
    #            FMP_CAPSULE_VERSION = FwVersion,
    #            )
    #ProcessCommand(' '.join(Command.splitlines()).strip())
    #os.chdir(CWD)

    #
    # Copy windows capsule file to output folder
    #
    #CreateTempDirect(WINDOWS_CAP_OUTPUT_FOLDER)
    #for file in os.listdir(WINDOWS_CAP_FOLDER):
    #    if os.path.splitext(file)[1] in [".cap", ".cat", ".inf"]:
    #        shutil.move(os.path.join(WINDOWS_CAP_FOLDER, file), WINDOWS_CAP_OUTPUT_FOLDER)
    #[-end-201130-IB16810140-remove]#
    #
    # Call microcode_padding.py
    #
    #RevertCommand = '''
    #            py -3 microcode_padding.py
    #            --opt revert
    #            --fdf {FDF}
    #            '''
    #Command = RevertCommand.format (
    #            FDF = MICROCODE_FV_FDF
    #            )
    #os.chdir(PLATFORM_BOARD_PACKAGE)
    #os.system(' '.join(Command.splitlines()).strip())
    #os.chdir(CWD)