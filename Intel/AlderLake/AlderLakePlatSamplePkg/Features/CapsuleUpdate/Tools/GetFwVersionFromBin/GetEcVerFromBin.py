import sys
import os
import glob
import fileinput
import array
import struct

#
# Part of EC image header structure:
#
#
# typedef struct {
#   UINT32  Signature;
#   UINT8   PlatId;
#   UINT8   MajorVer;
#   UINT8   MinorVer;
#   UINT8   BuildVer;
# } EC_IMAGE_HEADER_PIECE;
#
EcImgSignature    = "TKSC"
EcImgHdrPiece     = '<IBBBB'

def get_ec_ver_from_bin(binfile, ecverfile):

  # open binfile
  fin = open(binfile, "rb")

  # Read file to buffer
  buffer = fin.read()

  # get the offset of EcImgSignature "TKSC"
  ec_img_sig_begin = buffer.find(EcImgSignature.encode('utf-8'))
  if ec_img_sig_begin == -1:
    print ("Can not find EC image signature in %s" % binfile)
    fin.close()
    return -1

  # Decode part of EC image header to get version info
  (Signature, PlatId, MajorVer, MinorVer, BuildVer) = struct.unpack (
               EcImgHdrPiece,
               buffer[ec_img_sig_begin : ec_img_sig_begin + struct.calcsize (EcImgHdrPiece)]
               )

  # Write to output file
  fout = open(ecverfile, "wb")
  if sys.version_info.major == 2:
    fout.write("VERSION_MAJOR = %s\r\n" % MajorVer)
    fout.write("VERSION_MINOR = %s\r\n" % MinorVer)
  else:
    fout.write(("VERSION_MAJOR = %s\r\n" % MajorVer).encode('utf-8'))
    fout.write(("VERSION_MINOR = %s\r\n" % MinorVer).encode('utf-8'))

  fin.close()
  fout.close()
  return 0

###################################################################################################
# Main
###################################################################################################

def main():
  if len(sys.argv) != 3:
    print ("incorrent number of arguments")
    return 1

  ret = get_ec_ver_from_bin(sys.argv[1], sys.argv[2])

  if ret != 0:
    return 1
  return 0

if __name__ == '__main__':
    sys.exit(main())
