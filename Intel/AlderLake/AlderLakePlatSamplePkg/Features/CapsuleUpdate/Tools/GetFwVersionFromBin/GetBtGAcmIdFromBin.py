import sys
import os
import glob
import fileinput
import array
from collections import namedtuple

# BTGACM ID string format:
#
# $(BOARD_ID)$(BOARD_REV).$(BOARD_EXT).$(VERSION_MAJOR).$(BUILD_TYPE)$(VERSION_MINOR).YYMMDDHHMM
#     BoardId_Rev           BoardExt     VersionMajor          BuildType_VerMinor     TimeStamp
#
# Example: "TRFTCRB1.000.0008.D03.1501301017"
#
IdStringList = namedtuple('IdStringList', ['BoardId_Rev', 'BoardExt', 'VersionMajor', 'BuildType_VerMinor', 'TimeStamp'])

IdSignature = "$BTGACM$"

#
#  Get BootGuard ACM Header
#
def GetSearchIndex(BtGAcmHeaderfile):
    #Read 64 to 84 bytes for Search Index String
    fin = open(BtGAcmHeaderfile, "rb")
    buffer = bytearray(fin.read())
    fin.close()

    return buffer

#
#  Converter array to string
#
def ConverterArrToStr(ReferenceBytes):
    Outstr = ''
    for i in range(len(ReferenceBytes)):
        Outstr += (chr(ReferenceBytes[i]))

    return Outstr

#
#  Based on BtGAcmHeader, find out and update version value in BtGAcmidfile
#
def UpdateVersion(binfile, BtGAcmidfile, BtGAcmHeaderfile):
    with open(binfile, "rb") as BtGAcmBinFile:
        BinFileRead = bytearray(BtGAcmBinFile.read())

    HeaderArray = GetSearchIndex(BtGAcmHeaderfile)

    if (BinFileRead.find(HeaderArray) != -1):
        offset = BinFileRead.find(HeaderArray)
        print ("BtGAcmHeader signature in BtGAcm.bin Offset 0x%08X" % offset)
    else:
        print ("Cannot find out BootGuard ACM header string")

    if BtGAcmBinFile.closed:
        print ("BtGAcmBinFile is closed, re-open it")
        BtGAcmBinFile = open(binfile, "rb")

    #
    #  Get BootGuard ACM Version at Header offset 0x[25:27]
    #
    BtGAcmBinFile.seek((offset + 37), 1)
    buffer = array.array('B')
    buffer.fromfile(BtGAcmBinFile, 3)

    #
    #  Update BootGuard ACM VERSION_MAJOR and VERSION_MINOR
    #
    print ("Open file %s" % BtGAcmidfile)
    InAcmIdFile = open(BtGAcmidfile, "r")
    lines = InAcmIdFile.readlines()
    print ("lines %s" % lines)
    InAcmIdFile.close()

    newlines = []
    for index in lines:
        if index.find(r'VERSION_NUMBER = ') != -1:
            (Left, Right) = index.split(r" = ")
            HandleString = Left
            HandleString = HandleString + " = " + str(buffer[0]) + "\n"
            newlines.append(HandleString)
        if index.find(r'VERSION_MAJOR  = ') != -1:
            (Left, Right) = index.split(r" = ")
            HandleString = Left
            HandleString = HandleString + " = " + str(buffer[1]) + "\n"
            newlines.append(HandleString)
        if index.find(r'VERSION_MINOR  = ') != -1:
            (Left, Right) = index.split(r" = ")
            HandleString = Left
            HandleString = HandleString + " = " + str(buffer[2]) + "\n"
            newlines.append(HandleString)
        if ((index.find(r'VERSION_NUMBER = ') == -1) and (index.find(r'VERSION_MAJOR  = ') == -1) and (index.find(r'VERSION_MINOR  = ') == -1)):
            newlines.append(index)

    print ("newlines %s" % newlines)
    OutAcmIdFile = open(BtGAcmidfile, "w")
    OutAcmIdFile.write("".join(newlines))
    OutAcmIdFile.close()
    return

def get_BtGAcm_id_from_bin(binfile, BtGAcmidfile, BtGAcmHeaderfile):

  # open binfile
  fin = open(binfile, "rb")

  # Read file to buffer
  buffer = fin.read()

  # get the offset of IdSignature "$BTGACM$"
  id_sig_begin = buffer.find(IdSignature.encode('utf-8'))
  if id_sig_begin == -1:
    print ("Can not find BiosId signature in %s" % binfile)
    fin.close()
    fout = open(BtGAcmidfile, "w")
    fout.write("BOARD_REV      = %s\r\n" % "1")
    fout.write("BUILD_TYPE     = %s\r\n" % "1")
    fout.write("VERSION_NUMBER = %s\r\n" % "1")
    fout.write("VERSION_MAJOR  = %s\r\n" % "1")
    fout.write("VERSION_MINOR  = %s\r\n" % "1")
    fout.write("BOARD_ID       = %s\r\n" % "1")
    fout.write("BOARD_EXT      = %s\r\n" % "1")
    fout.write("BUILD_TARGET   = %s\r\n" % "DEBUG")
    fout.close()
    UpdateVersion(binfile, BtGAcmidfile, BtGAcmHeaderfile)
    return 0

  # get the offset of IdString
  id_str_begin = id_sig_begin + len(IdSignature)

  # get IdString buffer
  id_str_bfr = buffer[id_str_begin:id_str_begin+64] # 64 = sizeof (BTGACM_ID_STRING) - sizeof (NullTerminator)

  # create an unicode type arrary to access IdString buffer...dont know how this wroks
  unicode_array = array.array('u', id_str_bfr).tounicode()

  # seperate IdString by '.' to id_string_list
  id_string_list = IdStringList._make(unicode_array.split('.'))

  # Write to output file
  fout = open(BtGAcmidfile, "wb")
  if sys.version_info.major == 2:
    fout.write("BOARD_REV      = %s\r\n" % id_string_list.BoardId_Rev[7])
    fout.write("BUILD_TYPE     = %s\r\n" % id_string_list.BuildType_VerMinor[0])
    fout.write("VERSION_NUMBER = %s\r\n" % id_string_list.VersionNumber)
    fout.write("VERSION_MAJOR  = %s\r\n" % id_string_list.VersionMajor)
    fout.write("VERSION_MINOR  = %s\r\n" % id_string_list.BuildType_VerMinor[1:3])
    fout.write("BOARD_ID       = %s\r\n" % id_string_list.BoardId_Rev[0:7])
    fout.write("BOARD_EXT      = %s\r\n" % id_string_list.BoardExt)
  else:
    fout.write(("BOARD_REV      = %s\r\n" % id_string_list.BoardId_Rev[7]).encode('utf-8'))
    fout.write(("BUILD_TYPE     = %s\r\n" % id_string_list.BuildType_VerMinor[0]).encode('utf-8'))
    fout.write(("VERSION_NUMBER = %s\r\n" % id_string_list.VersionNumber).encode('utf-8'))
    fout.write(("VERSION_MAJOR  = %s\r\n" % id_string_list.VersionMajor).encode('utf-8'))
    fout.write(("VERSION_MINOR  = %s\r\n" % id_string_list.BuildType_VerMinor[1:3]).encode('utf-8'))
    fout.write(("BOARD_ID       = %s\r\n" % id_string_list.BoardId_Rev[0:7]).encode('utf-8'))
    fout.write(("BOARD_EXT      = %s\r\n" % id_string_list.BoardExt).encode('utf-8'))
  if id_string_list.BoardExt[0:1] == "D":
    if sys.version_info.major == 2:
      fout.write("BUILD_TARGET  = %s\r\n" % "DEBUG")
    else:
      fout.write(("BUILD_TARGET  = %s\r\n" % "DEBUG").encode('utf-8'))
  else:
    if sys.version_info.major == 2:
      fout.write("BUILD_TARGET  = %s\r\n" % "RELEASE")
    else:
      fout.write(("BUILD_TARGET  = %s\r\n" % "RELEASE").encode('utf-8'))

  fin.close()
  fout.close()
  return 0

###################################################################################################
# Main
###################################################################################################

def main():
  if len(sys.argv) != 4:
    print ("incorrent number of arguments")
    return 1

  ret = get_BtGAcm_id_from_bin(sys.argv[1], sys.argv[2], sys.argv[3])

  if ret != 0:
    return 1
  return 0

if __name__ == '__main__':
    sys.exit(main())

