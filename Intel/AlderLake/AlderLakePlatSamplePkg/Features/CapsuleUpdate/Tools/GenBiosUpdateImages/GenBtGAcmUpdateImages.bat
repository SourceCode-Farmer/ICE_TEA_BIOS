@REM @file
@REM Script to generate BIOS update relative images with Fault Tolerance support.
@REM
@REM This script
@REM
@REM 1. Generate BtGAcm.Bin - a fault tolerance supported BtGAcm update image
@REM    a.
@REM    b.
@REM    c.
@REM
@REM   +==============================================================
@REM   | BtGAcm.bin | <= StartupAcm                                  <== Original BtGAcm image
@REM   +------------+=================================================
@REM   |    KEYM    | <= Key Manifest                                |
@REM   +------------+                                                <== Newly Appended Binaries
@REM   |    ACBP    | <= Hash Generation according to new STartupAcm |
@REM   +==============================================================
@REM 2. Generate BtGAcmUpdateConfig.ini
@REM    Patch correct BaseAddress/Length/ImageOffset value in BtGAcmUpdateConfig.ini file.
@REM 3. Generate BtGAcmBgup.bin
@REM    Generate BGUP for each update region (e.g., BtGAcm.bin, KEYM, and ACBP.) and merge
@REM    them to a single BtGAcmBgup binary.
@REM    Besides, update HelperOffset/HelperLength accordingly in BtGAcmUpdateConfig.ini to
@REM    indicate the offset/length of each BGUP within BtGAcmBgup binary.
@REM
@REM @copyright
@REM  INTEL CONFIDENTIAL
@REM  Copyright 2019 - 2020 Intel Corporation.
@REM
@REM  The source code contained or described herein and all documents related to the
@REM  source code ("Material") are owned by Intel Corporation or its suppliers or
@REM  licensors. Title to the Material remains with Intel Corporation or its suppliers
@REM  and licensors. The Material may contain trade secrets and proprietary and
@REM  confidential information of Intel Corporation and its suppliers and licensors,
@REM  and is protected by worldwide copyright and trade secret laws and treaty
@REM  provisions. No part of the Material may be used, copied, reproduced, modified,
@REM  published, uploaded, posted, transmitted, distributed, or disclosed in any way
@REM  without Intel's prior express written permission.
@REM
@REM  No license under any patent, copyright, trade secret or other intellectual
@REM  property right is granted to or conferred upon you by disclosure or delivery
@REM  of the Materials, either expressly, by implication, inducement, estoppel or
@REM  otherwise. Any license under such intellectual property rights must be
@REM  express and approved by Intel in writing.
@REM
@REM  Unless otherwise agreed by Intel in writing, you may not remove or alter
@REM  this notice or any other notice embedded in Materials by Intel or
@REM  Intel's suppliers or licensors in any way.
@REM
@REM  This file contains a 'Sample Driver' and is licensed as such under the terms
@REM  of your license agreement with Intel or your vendor. This file may be modified
@REM  by the user, subject to the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM

@REM Usage:
@REM GenBtGAcmUpdateImages.bat [BtGAcm BIN] [Build Type] [Output Path]
@REM   BtGAcm BIN    : The file path to BtGAcm image.
@REM   Build Type    : Indicates BtGAcm BIN is Prod or non-Prod build.
@REM   Output Path   : The path where the output files will be generated at.
@REM
@set BTGACM_BIN_FILE=%1
@set BTGACM_BIN_BUILD_TYPE=%2
@set BTGACM_UPDATE_IMAGE_OUTPUT=%3
@set BTGACM_UPDATE_TARGET_IMAGE=%4

@if not defined WORKSPACE               goto EnvironmentNotSet
@if not defined WORKSPACE_PLATFORM      goto EnvironmentNotSet
@if not defined PLATFORM_BOARD_PACKAGE  goto EnvironmentNotSet
@if not defined PLATFORM_FULL_PACKAGE   goto EnvironmentNotSet

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
)
%PYTHON_COMMAND% --version
if not %ERRORLEVEL% == 0 (
  @echo !!! %PYTHON_COMMAND% is invalid
  goto EnvironmentNotSet
)
@if not exist %BTGACM_BIN_FILE% (
  @echo !!! Assigned file %BTGACM_BIN_FILE% does not exist!!!
  @goto BiosImagesNotBuilt
)

@REM
@REM Define environment variables
@REM
@set CAPSULE_UPDATE_FEATURE_PATH=%WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Features\CapsuleUpdate
@REM Flash map used to extract BIOS BtGAcm region
@if %RESILIENCY_BUILD% EQU TRUE (
  @echo Setting Flash map file as FlashMapIncludeRes.fdf
  @set FLASH_MAP_NAME=%WORKSPACE_PLATFORM%\AlderLakeBoardPkg\Include\Fdf\FlashMapIncludeRes.fdf

) else (
  @echo Setting Flash map file as FlashMapInclude.fdf
  @set FLASH_MAP_NAME=%WORKSPACE_PLATFORM%\AlderLakeBoardPkg\Include\Fdf\FlashMapInclude.fdf
)

  @REM The ini source file
  set INI_TEMPLATE_FILE=%CAPSULE_UPDATE_FEATURE_PATH%\BiosUpdateConfig\BtGAcmUpdateConfig.ini

@REM Tool path to re-generate BootGuard sign data.
@set RECOVERY_GEN_TOOL_PATH=%CAPSULE_UPDATE_FEATURE_PATH%\Tools\GenBiosUpdateImages\CapsuleRecoveryGen

@REM
@REM Clean directories and files from the previous output
@REM
if exist %BTGACM_WORKING_PATH% rmdir /q /s %BTGACM_WORKING_PATH%
@del %BTGACM_UPDATE_IMAGE_OUTPUT%\BtGAcm_pad.bin 1>NUL 2>&1
@del %BTGACM_UPDATE_IMAGE_OUTPUT%\BtGAcmUpdateConfig.ini 1>NUL 2>&1
@del %BTGACM_UPDATE_IMAGE_OUTPUT%\BtGAcmBgup.bin 1>NUL 2>&1

@set SCRIPT_ERROR=0

@REM
@REM ============ 1. Generate 4K aligned BtGACM file ============
@REM
@REM Make BtGAcm binary 4K aligned for SPI flash update.
@REM Leverage python script in NewGenCap (uCode Capsule support)
%PYTHON_COMMAND% %CAPSULE_UPDATE_FEATURE_PATH%\Tools\NewGenCap\PyScript\GenAligned.py -a 4K -i %BTGACM_BIN_FILE% -o %BTGACM_UPDATE_IMAGE_OUTPUT%\BtGAcm_pad.bin

@REM
@REM ============ 2. Generate BtGAcmUpdateConfig.ini ============
@REM
@REM Update address/length information in configini file
%PYTHON_COMMAND% %RECOVERY_GEN_TOOL_PATH%\SplitFv.py updateconfig -i %INI_TEMPLATE_FILE% -t %BTGACM_BIN_BUILD_TYPE% -m %FLASH_MAP_NAME% -o %BTGACM_UPDATE_IMAGE_OUTPUT%


@echo.
@echo  GenBtGAcmUpdateImages completed.
@echo.
@goto GenBtGAcmUpdateImagesDone


:BtGAcmUpdateNotBuilt
@echo.
@echo !! GenBtGAcmUpdateImages failed.
@echo.
@exit /b 1

:EnvironmentNotSet
@echo.
@echo !! GenBtGAcmUpdateImages failed. Needed environment variables are not defined.
@echo.
@exit /b 1

:GenBtGAcmUpdateImagesDone
