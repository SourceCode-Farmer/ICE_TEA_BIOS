@REM @file
@REM 
@REM*****************************************************************************
@REM Copyright 2021 Insyde Software Corp. All Rights Reserved.
@REM
@REM You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM transmit, broadcast, present, recite, release, license or otherwise exploit
@REM any part of this publication in any form, by any means, without the prior
@REM written permission of Insyde Software Corp.
@REM
@REM*****************************************************************************
@REM @file
@REM Script to generate ME update capsule.
@REM
@REM Usage:
@REM   GenCapsuleSysFwMe.bat [FMP Payload] [ME Build Version] [Output Path]
@REM   FMP Payload      : The FMP payload used to create the Capsule image.
@REM   ME Build Version : ME Build version (in DEC format)
@REM   Output Path      : The path for output Capsule image.
@REM
@REM @copyright
@REM  INTEL CONFIDENTIAL
@REM  Copyright 2018 - 2021 Intel Corporation.
@REM
@REM  The source code contained or described herein and all documents related to the
@REM  source code ("Material") are owned by Intel Corporation or its suppliers or
@REM  licensors. Title to the Material remains with Intel Corporation or its suppliers
@REM  and licensors. The Material may contain trade secrets and proprietary and
@REM  confidential information of Intel Corporation and its suppliers and licensors,
@REM  and is protected by worldwide copyright and trade secret laws and treaty
@REM  provisions. No part of the Material may be used, copied, reproduced, modified,
@REM  published, uploaded, posted, transmitted, distributed, or disclosed in any way
@REM  without Intel's prior express written permission.
@REM
@REM  No license under any patent, copyright, trade secret or other intellectual
@REM  property right is granted to or conferred upon you by disclosure or delivery
@REM  of the Materials, either expressly, by implication, inducement, estoppel or
@REM  otherwise. Any license under such intellectual property rights must be
@REM  express and approved by Intel in writing.
@REM
@REM  Unless otherwise agreed by Intel in writing, you may not remove or alter
@REM  this notice or any other notice embedded in Materials by Intel or
@REM  Intel's suppliers or licensors in any way.
@REM
@REM  This file contains a 'Sample Driver' and is licensed as such under the terms
@REM  of your license agreement with Intel or your vendor. This file may be modified
@REM  by the user, subject to the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM
@echo off
setlocal
echo start

@REM [-start-210426-IB16810151-modify]
set FMP_CAPSULE_PAYLOAD=%1
set ME_VERSION_HEX=%2
set FMP_CAPSULE_GUID=%3
set FMP_CAPSULE_LSV=%4
set TOOL_PATH=%5
REM set ME_VERSION_STR=%2
REM set ME_FW_IMAGE_SKU=%3
REM set FMP_CAPSULE_OUTPUT_DIR=%4
REM set ME_LSV_VERSION_STR=%5

REM set FMP_CAPSULE_GUID=
REM set ME_VERSION_MAJ=
REM set ME_VERSION_MIN=
REM set ME_VERSION_HF=
REM set ME_VERSION_BUILD=
@REM [-end-210426-IB16810151-modify]

@REM [-start-211209-IB19270002-remove]
@REM
@REM Access ME version string and convert it to FMP version
@REM
REM for /F "tokens=1 delims=." %%a in ("%ME_VERSION_STR%") do set ME_VERSION_MAJ=%%a
REM for /F "tokens=2 delims=." %%a in ("%ME_VERSION_STR%") do set ME_VERSION_MIN=%%a
REM for /F "tokens=3 delims=." %%a in ("%ME_VERSION_STR%") do set ME_VERSION_HF=%%a
REM for /F "tokens=4 delims=." %%a in ("%ME_VERSION_STR%") do set ME_VERSION_BUILD=%%a

REM @if not defined ME_VERSION_MAJ   goto Invalid_MeVersion
REM @if not defined ME_VERSION_MIN   goto Invalid_MeVersion
REM @if not defined ME_VERSION_HF    goto Invalid_MeVersion
REM @if not defined ME_VERSION_BUILD goto Invalid_MeVersion
@REM [-end-211209-IB19270002-remove]

@REM
@REM Set FMP Type ID according to ME SKU
@REM Different generation and SKU need different GUID.
@REM GUIDs need to align with the definition in PlatformPkg.dec file.
@REM
@REM [-start-210426-IB16810151-remove]
REM if /I "%ME_FW_IMAGE_SKU%" == "Lp_Cons" (
REM   set FMP_CAPSULE_GUID=23192307-d667-4bdf-af1a-6059db171246
REM   set ME_FW_IMAGE_SKU=Lp_Cons
REM )
REM if /I "%ME_FW_IMAGE_SKU%" == "H_Cons" (
REM   set FMP_CAPSULE_GUID=7aa69739-8f78-41cb-bf44-854e2cb516bd
REM   set ME_FW_IMAGE_SKU=H_Cons
REM )

REM if /I "%ME_FW_IMAGE_SKU%" == "Lp_Corp" (
REM   set FMP_CAPSULE_GUID=4e78ce68-5389-4a95-bf10-e3568c30caf8
REM   set ME_FW_IMAGE_SKU=Lp_Corp
REM )

REM if /I "%ME_FW_IMAGE_SKU%" == "H_Corp" (
REM   set FMP_CAPSULE_GUID=347efe23-9f9a-4b26-b4db-e2414872dd14
REM   set ME_FW_IMAGE_SKU=H_Corp
REM )


REM if /I "%ME_FW_IMAGE_SKU%" == "default" (
REM   set FMP_CAPSULE_GUID=0EAB05C1-766A-4805-A039-3081DE0210C7
REM   set ME_FW_IMAGE_SKU=default
REM )

REM @if "%FMP_CAPSULE_GUID%" == "" (
REM   @echo.
REM   @echo !! Input ME SKU %ME_FW_IMAGE_SKU% is not recognized
REM   @echo    It should be one of the following: Lp_Cons/H_Cons/Lp_Corp/H_Corp/default
REM   exit /b 1
REM )


@REM
@REM A tricky way to covert FW Version from DEC to HEX
@REM
REM call cmd /c exit /b %ME_VERSION_MIN%
REM set ME_VERSION_MIN_HEX=00%=exitcode%

REM call cmd /c exit /b %ME_VERSION_HF%
REM set ME_VERSION_HF_HEX=00%=exitcode%

REM call cmd /c exit /b %ME_VERSION_BUILD%
REM set ME_VERSION_BUILD_HEX=0000%=exitcode%
@REM [-end-210426-IB16810151-remove]
@REM [-start-210830-IB16810163-remove]
REM set ME_VERSION_HEX=0x%ME_VERSION_MIN_HEX:~-2%%ME_VERSION_HF_HEX:~-2%%ME_VERSION_BUILD_HEX:~-4%
@REM [-end-210830-IB16810163-remove]
@REM [-start-210426-IB16810151-remove]
@REM
@REM Access ME LSV version string and convert it to FMP version
@REM
REM set ME_LSV_VERSION_MAJ=
REM set ME_LSV_VERSION_MIN=
REM set ME_LSV_VERSION_HF=
REM set ME_LSV_VERSION_BUILD=

REM for /F "tokens=1 delims=." %%a in ("%ME_LSV_VERSION_STR%") do set ME_LSV_VERSION_MAJ=%%a
REM for /F "tokens=2 delims=." %%a in ("%ME_LSV_VERSION_STR%") do set ME_LSV_VERSION_MIN=%%a
REM for /F "tokens=3 delims=." %%a in ("%ME_LSV_VERSION_STR%") do set ME_LSV_VERSION_HF=%%a
REM for /F "tokens=4 delims=." %%a in ("%ME_LSV_VERSION_STR%") do set ME_LSV_VERSION_BUILD=%%a

REM @if not defined ME_LSV_VERSION_MAJ   goto Invalid_LsvVersion
REM @if not defined ME_LSV_VERSION_MIN   goto Invalid_LsvVersion
REM @if not defined ME_LSV_VERSION_HF    goto Invalid_LsvVersion
REM @if not defined ME_LSV_VERSION_BUILD goto Invalid_LsvVersion

@REM
@REM A tricky way to covert FW Version from DEC to HEX
@REM
REM call cmd /c exit /b %ME_LSV_VERSION_MIN%
REM set ME_LSV_VERSION_MIN_HEX=00%=exitcode%

REM call cmd /c exit /b %ME_LSV_VERSION_HF%
REM set ME_LSV_VERSION_HF_HEX=00%=exitcode%

REM call cmd /c exit /b %ME_LSV_VERSION_BUILD%
REM set ME_LSV_VERSION_BUILD_HEX=0000%=exitcode%

REM set FMP_CAPSULE_LSV=0x%ME_LSV_VERSION_MIN_HEX:~-2%%ME_LSV_VERSION_HF_HEX:~-2%%ME_LSV_VERSION_BUILD_HEX:~-4%
@REM [-end-210426-IB16810151-remove]
@REM
@REM Setup enviroment variables
@REM
@REM [-start-210426-IB16810151-modify]
REM set FMP_CAPSULE_VENDOR=Intel
REM set FMP_CAPSULE_STRING=%ME_VERSION_MAJ%.%ME_VERSION_MIN%.%ME_VERSION_HF%.%ME_VERSION_BUILD%
set FMP_CAPSULE_FILE=SystemMeFw.Cap
set FMP_CAPSULE_VERSION=%ME_VERSION_HEX%
REM set FMP_CAPSULE_NAME="Intel CSME %FMP_CAPSULE_STRING% %ME_FW_IMAGE_SKU%"
@REM Use FMP_CAPSULE_LSV set above
@REM set FMP_CAPSULE_LSV=0x00000000
REM set WINDOWS_CAPSULE_KEY=SAMPLE_DEVELOPMENT.pfx
REM set WINDOWS_CAPSULE_OUTPUT_DIR=%FMP_CAPSULE_OUTPUT_DIR%\WindowsCapsule\Me

REM for %%i in ("%FMP_CAPSULE_FILE%") do SET WINDOWS_CAPSULE_NAME=%%~ni
@REM [-end-210426-IB16810151-modify]

if not exist "%FMP_CAPSULE_PAYLOAD%" exit /b 1

REM
REM Delete files if exist
REM
@REM [-start-201112-IB16810138-remove]
REM @del %FMP_CAPSULE_OUTPUT_DIR%\%FMP_CAPSULE_FILE% 1>NUL 2>&1
REM @del %WINDOWS_CAPSULE_OUTPUT_DIR%\%WINDOWS_CAPSULE_NAME%.* 1>NUL 2>&1
@REM [-end-201112-IB16810138-remove]

REM
REM Sign capsule using OpenSSL with EDK II Test Certificate
REM
@REM [-start-201112-IB16810138-modify]
call GenerateCapsule.exe ^
  --encode ^
  -v ^
  --guid %FMP_CAPSULE_GUID% ^
  --fw-version %FMP_CAPSULE_VERSION% ^
  --lsv %FMP_CAPSULE_LSV% ^
  --capflag PersistAcrossReset ^
  --capflag InitiateReset ^
  --signing-tool-path=%TOOL_PATH%\OpenSSL ^
  --signer-private-cert=%TOOL_PATH%\Pkcs7Sign\TestCert.pem ^
  --other-public-cert=%TOOL_PATH%\Pkcs7Sign\TestSub.pub.pem ^
  --trusted-public-cert=%TOOL_PATH%\Pkcs7Sign\TestRoot.pub.pem ^
  -o %FMP_CAPSULE_FILE% ^
  %FMP_CAPSULE_PAYLOAD%
@REM [-end-201112-IB16810138-modify]

@REM [-start-210426-IB16810151-remove]
REM copy %FMP_CAPSULE_FILE% %FMP_CAPSULE_OUTPUT_DIR%

REM
REM Generate Windows Capsule package with test key
REM
REM if exist "%WINDOWS_CAPSULE_KEY%" (
REM  @%PYTHON_COMMAND% CreateWindowsCapsule.py ^
REM    %WINDOWS_CAPSULE_NAME% ^
REM    %FMP_CAPSULE_STRING% ^
REM    %FMP_CAPSULE_GUID% ^
REM    %FMP_CAPSULE_FILE% ^
REM    %FMP_CAPSULE_VERSION% ^
REM    %FMP_CAPSULE_VENDOR% ^
REM    %FMP_CAPSULE_VENDOR% ^
REM    %FMP_CAPSULE_NAME% %WINDOWS_CAPSULE_KEY%
REM
REM  xcopy /s/e/v/i/y WindowsCapsule %WINDOWS_CAPSULE_OUTPUT_DIR%
REM  rmdir /s /q WindowsCapsule
REM )

REM erase %FMP_CAPSULE_FILE%
REM exit /b

REM :Invalid_MeVersion

REM @echo.
REM @echo Invalid ME version %ME_VERSION_STR%
REM @echo.
REM exit /b 1

REM :Invalid_LsvVersion
REM @echo.
REM @echo Invalid ME LSV version %ME_LSV_VERSION_STR%
REM @echo.
REM exit /b 1
@REM [-end-210426-IB16810151-remove]
