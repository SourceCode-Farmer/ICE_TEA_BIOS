@REM @file
@REM 
@REM*****************************************************************************
@REM Copyright 2021 Insyde Software Corp. All Rights Reserved.
@REM
@REM You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM transmit, broadcast, present, recite, release, license or otherwise exploit
@REM any part of this publication in any form, by any means, without the prior
@REM written permission of Insyde Software Corp.
@REM
@REM*****************************************************************************
@REM
@REM Usage:
@REM   GenCapsuleSysFwBios.bat [FMP Payload] [BIOS Version] [Output Path]
@REM   FMP Payload  : The FMP payload used to create the Capsule image.
@REM   BIOS Version : BIOS version in DEC format
@REM   Output Path  : The path for output Capsule image.
@REM
@REM   The format of BIOS Version (in DEC): MMMMmm
@REM   M: major version, m: minor version. e.g., 2413.01 -> 241301
@REM
@echo off
setlocal
echo start

set FMP_CAPSULE_PAYLOAD=%1
@REM[-start-201030-IB16810136-modify]
set TOOL_PATH=%2

REM set BIOS_VERSION_DEC=%2
REM set FMP_CAPSULE_OUTPUT_DIR=%3
@REM [-end-201030-IB16810136-modify]

@REM
@REM A tricky way to covert FW Version from DEC to HEX
@REM
@REM[-start-201030-IB16810136-modify]
REM call cmd /c exit /b %BIOS_VERSION_DEC%
set BIOS_VERSION_HEX=0x70104012
@REM [-end-201030-IB16810136-modify]

@REM
@REM Setup enviroment variables
@REM
@REM [-start-201030-IB16810136-modify]
REM set FMP_CAPSULE_VENDOR=Intel
set FMP_CAPSULE_GUID=6C8E136F-D3E6-4131-AC32-4687CB4ABD27
set FMP_CAPSULE_FILE=SystemFwBios.Cap
set FMP_CAPSULE_VERSION=%BIOS_VERSION_HEX%
REM set FMP_CAPSULE_STRING=0.0.%BIOS_VERSION_DEC:~-6,-2%.%BIOS_VERSION_DEC:~-2%
REM set FMP_CAPSULE_NAME="Intel %CAP_PLATFORM_FULLNAME% BIOS %FMP_CAPSULE_STRING%"
set FMP_CAPSULE_LSV=0x52430000
REM set WINDOWS_CAPSULE_KEY=SAMPLE_DEVELOPMENT.pfx
REM set WINDOWS_CAPSULE_OUTPUT_DIR=%FMP_CAPSULE_OUTPUT_DIR%\WindowsCapsule\Bios

REM for %%i in ("%FMP_CAPSULE_FILE%") do SET WINDOWS_CAPSULE_NAME=%%~ni
@REM [-end-201030-IB16810136-modify]

if not exist "%FMP_CAPSULE_PAYLOAD%" exit /b 1

REM
REM Delete files if exist
REM
@REM [-start-201030-IB16810136-remove]
REM @del %FMP_CAPSULE_OUTPUT_DIR%\%FMP_CAPSULE_FILE% 1>NUL 2>&1
REM @del %WINDOWS_CAPSULE_OUTPUT_DIR%\%WINDOWS_CAPSULE_NAME%.* 1>NUL 2>&1
@REM [-end-201030-IB16810136-remove]

REM
REM Sign capsule using OpenSSL with EDK II Test Certificate
REM
@REM [-start-201030-IB16810136-modify]
call GenerateCapsule.exe ^
  --encode ^
  -v ^
  --guid %FMP_CAPSULE_GUID% ^
  --fw-version %FMP_CAPSULE_VERSION% ^
  --lsv %FMP_CAPSULE_LSV% ^
  --capflag PersistAcrossReset ^
  --capflag InitiateReset ^
  --signing-tool-path=%TOOL_PATH%\OpenSSL ^
  --signer-private-cert=%TOOL_PATH%\Pkcs7Sign\TestCert.pem ^
  --other-public-cert=%TOOL_PATH%\Pkcs7Sign\TestSub.pub.pem ^
  --trusted-public-cert=%TOOL_PATH%\Pkcs7Sign\TestRoot.pub.pem ^
  -o %FMP_CAPSULE_FILE% ^
  %FMP_CAPSULE_PAYLOAD%
@REM [-end-201030-IB16810136-modify]

@REM [-start-201030-IB16810136-remove]
REM copy %FMP_CAPSULE_FILE% %FMP_CAPSULE_OUTPUT_DIR%

REM
REM Generate Windows Capsule package with test key
REM
REM if exist "%WINDOWS_CAPSULE_KEY%" (
REM  @%PYTHON_COMMAND% CreateWindowsCapsule.py ^
REM    %WINDOWS_CAPSULE_NAME% ^
REM    %FMP_CAPSULE_STRING% ^
REM    %FMP_CAPSULE_GUID% ^
REM    %FMP_CAPSULE_FILE% ^
REM    %FMP_CAPSULE_VERSION% ^
REM    %FMP_CAPSULE_VENDOR% ^
REM    %FMP_CAPSULE_VENDOR% ^
REM    %FMP_CAPSULE_NAME% %WINDOWS_CAPSULE_KEY%
REM
REM  xcopy /s/e/v/i/y WindowsCapsule %WINDOWS_CAPSULE_OUTPUT_DIR%
REM  rmdir /s /q WindowsCapsule
REM )

REM erase %FMP_CAPSULE_FILE%
@REM [-end-201030-IB16810136-remove]
