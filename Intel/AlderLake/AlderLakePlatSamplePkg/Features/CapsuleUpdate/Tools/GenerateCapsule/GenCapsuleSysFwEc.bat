@REM @file
@REM 
@REM*****************************************************************************
@REM Copyright 2021 Insyde Software Corp. All Rights Reserved.
@REM
@REM You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM transmit, broadcast, present, recite, release, license or otherwise exploit
@REM any part of this publication in any form, by any means, without the prior
@REM written permission of Insyde Software Corp.
@REM
@REM*****************************************************************************
@REM
@REM Usage:
@REM   GenCapsuleSysFwEc.bat [FMP Payload] [EC Version] [Output Path]
@REM   FMP Payload  : The FMP payload used to create the Capsule image.
@REM   EC Version   : EC version in DEC format
@REM   Output Path  : The path for output Capsule image.
@REM
@REM   The format of EC Version (in DEC): MMMmmm
@REM   M: major version, m: minor version. e.g., 1.18 -> 1018
@REM
@echo off
setlocal
echo start

set FMP_CAPSULE_PAYLOAD=%1
set EC_VERSION_DEC=%2
set FMP_CAPSULE_OUTPUT_DIR=%3

@REM
@REM A tricky way to covert FW Version from DEC to HEX
@REM
call cmd /c exit /b %EC_VERSION_DEC%
set EC_VERSION_HEX=0x%=exitcode%

@REM
@REM Setup enviroment variables
@REM
set FMP_CAPSULE_VENDOR=Intel
set FMP_CAPSULE_GUID=3DD84775-EC79-4ECB-8404-74DE030C3F77
set FMP_CAPSULE_FILE=%CAP_PLATFORM_PREFIX%SystemFwEc_%EC_VERSION_DEC%.cap
set FMP_CAPSULE_VERSION=%EC_VERSION_HEX%
set FMP_CAPSULE_STRING=0.0.%EC_VERSION_DEC:~-6,-3%.%EC_VERSION_DEC:~-3%
set FMP_CAPSULE_NAME="Intel %CAP_PLATFORM_FULLNAME% EC %FMP_CAPSULE_STRING%"
set FMP_CAPSULE_LSV=0x00000000
set WINDOWS_CAPSULE_KEY=SAMPLE_DEVELOPMENT.pfx
set WINDOWS_CAPSULE_OUTPUT_DIR=%FMP_CAPSULE_OUTPUT_DIR%\WindowsCapsule\Ec

for %%i in ("%FMP_CAPSULE_FILE%") do SET WINDOWS_CAPSULE_NAME=%%~ni

if not exist "%FMP_CAPSULE_PAYLOAD%" exit /b 1

REM
REM Delete files if exist
REM
@del %FMP_CAPSULE_OUTPUT_DIR%\%FMP_CAPSULE_FILE% 1>NUL 2>&1
@del %WINDOWS_CAPSULE_OUTPUT_DIR%\%WINDOWS_CAPSULE_NAME%.* 1>NUL 2>&1

REM
REM Sign capsule using OpenSSL with EDK II Test Certificate
REM
call GenerateCapsule ^
  --encode ^
  -v ^
  --guid %FMP_CAPSULE_GUID% ^
  --fw-version %FMP_CAPSULE_VERSION% ^
  --lsv %FMP_CAPSULE_LSV% ^
  --capflag PersistAcrossReset ^
  --capflag InitiateReset ^
  --signing-tool-path=%WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\OpenSSL ^
  --signer-private-cert=%EDK_TOOLS_PATH%\Source\Python\Pkcs7Sign\TestCert.pem ^
  --other-public-cert=%EDK_TOOLS_PATH%\Source\Python\Pkcs7Sign\TestSub.pub.pem ^
  --trusted-public-cert=%EDK_TOOLS_PATH%\Source\Python\Pkcs7Sign\TestRoot.pub.pem ^
  -o %FMP_CAPSULE_FILE% ^
  %FMP_CAPSULE_PAYLOAD%

copy %FMP_CAPSULE_FILE% %FMP_CAPSULE_OUTPUT_DIR%

REM
REM Generate Windows Capsule package with test key
REM
if exist "%WINDOWS_CAPSULE_KEY%" (
  @%PYTHON_COMMAND% CreateWindowsCapsule.py ^
    %WINDOWS_CAPSULE_NAME% ^
    %FMP_CAPSULE_STRING% ^
    %FMP_CAPSULE_GUID% ^
    %FMP_CAPSULE_FILE% ^
    %FMP_CAPSULE_VERSION% ^
    %FMP_CAPSULE_VENDOR% ^
    %FMP_CAPSULE_VENDOR% ^
    %FMP_CAPSULE_NAME% %WINDOWS_CAPSULE_KEY%

  xcopy /s/e/v/i/y WindowsCapsule %WINDOWS_CAPSULE_OUTPUT_DIR%
  rmdir /s /q WindowsCapsule
)

erase %FMP_CAPSULE_FILE%
