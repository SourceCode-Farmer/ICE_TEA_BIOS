/** @file
  Implementation of PlatformInitRecovery in PEI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Guid/SysFwUpdateProgress.h>
#include <Ppi/RecoveryModule.h>
#include <Library/PeiPlatformRecoveryLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/HobLib.h>
#include <Library/SpiAccessLib.h>
#if FixedPcdGetBool(PcdResiliencyEnable) == 1
#include <Library/EcMiscLib.h>

#include <Register/CommonMsr.h>
#include <CpuRegs.h>

#include <IndustryStandard/FirmwareInterfaceTable.h>
#include <Library/PeiBootGuardEventLogLib.h>
#include <Library/CmosAccessLib.h>
#include <CmosMap.h>

//
// Struct for policy
//
typedef struct {
  UINT16     IndexPort;
  UINT16     DataPort;
  UINT8      Width;
  UINT8      Bit;
  UINT16     Index;
  UINT8      Size[3];
  UINT8      Rsvd;
  UINT16     Version;
  UINT8      Type:7;
  UINT8      C_V:1;
  UINT8      Checksum;
} FIRMWARE_INTERFACE_TABLE_ENTRY_PORT;
#endif

GLOBAL_REMOVE_IF_UNREFERENCED EFI_PEI_PPI_DESCRIPTOR mPpiListRecoveryBootMode = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiBootInRecoveryModePpiGuid,
  NULL
};

/**
  Callback on BootMode availbale
**/
EFI_STATUS
EFIAPI
PlatformInitRecoveryCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

/**
  Notify on gEfiPeiMasterBootModePpiGuid
**/
static EFI_PEI_NOTIFY_DESCRIPTOR mPlatformInitRecoveryNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMasterBootModePpiGuid,
  PlatformInitRecoveryCallback
};

/**
  Callback on memory availbale
**/
EFI_STATUS
EFIAPI
MemoryDiscoveredPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

/**
  Notify on gEfiPeiMemoryDiscoveredPpiGuid
**/
static EFI_PEI_NOTIFY_DESCRIPTOR mMemDiscoveredNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMemoryDiscoveredPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) MemoryDiscoveredPpiNotifyCallback
};

/**
  Load recovery module from external storage (such as SATA or NVMe).

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  The typedef structure of the notification
                                descriptor. Not used in this function.
  @param[in]  Ppi               The typedef structure of the PPI descriptor.
                                Not used in this function.
  @retval EFI_SUCCESS           The function completed successfully
**/
EFI_STATUS
EFIAPI
LoadRecoveryModule (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

/**
  Notify list on VirtualBlockIoPpi
**/
static EFI_PEI_NOTIFY_DESCRIPTOR  mBlockIoNotifyList[] = {
  //
  // Use EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH flag rather than EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK
  // to ensure all VirtualBlockIoPpi notify callbacks (especially the one in FatLite) are executed
  // before invoking LoadRecoveryModule().
  //
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH,
    &gEfiPeiVirtualBlockIoPpiGuid,  //gEfiPeiDeviceRecoveryModulePpiGuid
    LoadRecoveryModule
  },
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gEfiPeiVirtualBlockIo2PpiGuid,  //gEfiPeiDeviceRecoveryModulePpiGuid
    LoadRecoveryModule
  }
};

/**
  Load recovery module from external storage (such as SATA or NVMe).

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  The typedef structure of the notification
                                descriptor. Not used in this function.
  @param[in]  Ppi               The typedef structure of the PPI descriptor.
                                Not used in this function.

  @retval EFI_SUCCESS           The function completed successfully
**/
EFI_STATUS
EFIAPI
LoadRecoveryModule (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                                  Status;
  EFI_PEI_RECOVERY_MODULE_PPI                *PeiRecovery;

  DEBUG ((DEBUG_INFO, "LoadRecoveryModule Entry\n"));

  //
  // load recovery file and install FV (original done in DXEIPL, but we copy it here)
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiRecoveryModulePpiGuid,
             0,
             NULL,
             (VOID **) &PeiRecovery
             );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Locate Pei Recovery Module Ppi Failed.(Status = %r)\n", Status));
    return Status;
  }

  Status = PeiRecovery->LoadRecoveryCapsule ((EFI_PEI_SERVICES **)PeiServices, PeiRecovery);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Load Recovery Capsule Failed.(Status = %r)\n", Status));
  }

  return Status;
}

/**
  Detect if there was an incompleted system firmware update.

  @retval  TRUE   Incompleted system firmware is detected.
  @retavl  FALSE  Not detected.

**/
BOOLEAN
IsSystemFirmwareUpdateInProgress (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *PeiReadOnlyVarPpi;
  UINTN                             VarSize;
  SYSTEM_FIRMWARE_UPDATE_PROGRESS   UpdateProgress;
  VOID                              *HobPtr;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &PeiReadOnlyVarPpi
             );

  ASSERT_EFI_ERROR (Status);

  if (Status == EFI_SUCCESS) {
    VarSize = sizeof (SYSTEM_FIRMWARE_UPDATE_PROGRESS);
    Status = PeiReadOnlyVarPpi->GetVariable (
                                  PeiReadOnlyVarPpi,
                                  SYSFW_UPDATE_PROGRESS_VARIABLE_NAME,
                                  &gSysFwUpdateProgressGuid,
                                  NULL,
                                  &VarSize,
                                  &UpdateProgress
                                  );
    if (Status == EFI_SUCCESS) {
      DEBUG ((
        DEBUG_INFO,
        "Detected system firmware update is in progress: Component 0x%x, Progress 0x%x\n",
        UpdateProgress.Component,
        UpdateProgress.Progress
        ));
#if (FixedPcdGetBool(PcdResiliencyEnable) == 0)
      //
      // Add check for handle res bios update to bkc bios case.
      //
      if (UpdateProgress.Component == UpdatingResiliency) {
        return FALSE;
      }
#endif
      //
      // Add check for NOT_STARTED case
      //
      if (UpdateProgress.Component == NotStarted) {
        return FALSE;
      }
      HobPtr = BuildGuidDataHob (&gSysFwUpdateProgressGuid, &UpdateProgress, sizeof (UpdateProgress));
      ASSERT (HobPtr != 0);
      return TRUE;
    }
  }

  return FALSE;
}

/**
  Callback once there is main memory

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
EFIAPI
MemoryDiscoveredPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                        Status;
  EFI_BOOT_MODE                     BootMode;
  EFI_HOB_GUID_TYPE                 *GuidHob;
  SYSTEM_FIRMWARE_UPDATE_PROGRESS   *UpdateProgress;

  GuidHob = NULL;
  UpdateProgress = NULL;

  GuidHob = GetFirstGuidHob (&gSysFwUpdateProgressGuid);
  if (GuidHob != NULL) {
    UpdateProgress = (SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob);
  }

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  ///
  /// If S3 resume or legacy recovery, then we are done
  ///
  if ((BootMode == BOOT_ON_S3_RESUME) || (BootMode == BOOT_IN_RECOVERY_MODE)) {
    return EFI_SUCCESS;
  }

  //
  // Check if flash update mode and recovery servie are needed.
  //
  if ((UpdateProgress != NULL) || SpiIsTopSwapEnabled ()) {
    //
    // Indicate an interrupted update or post-bios update (UpdatingResiliency) or
    // somehow BIOS ROM is corrupted. Therefore flash update would be required.
    // Set flash update mode again in case MRC modified the boot mode when MrcBootMode is bmCold or bmCold.
    //
    PeiServicesSetBootMode (BOOT_ON_FLASH_UPDATE);

    if (!SpiIsTopSwapEnabled () && (UpdateProgress != NULL) && (UpdateProgress->Component == UpdatingResiliency)) {
      //
      // This is a post-bios update phase. System is supposed to boot from ROM.
      // No recovery serivice is required.
      //
      DEBUG ((DEBUG_INFO, "UpdatingResiliency phase, recovery service is not needed\n"));
    } else {
      //
      // Initialize recovery servie to load recovery image or capsules on disk.
      //
      DEBUG ((DEBUG_INFO, "Initialize recovery servie\n"));

      Status = InitializeRecovery ();
      ASSERT_EFI_ERROR (Status);
      Status = PeiServicesNotifyPpi (mBlockIoNotifyList);
      ASSERT_EFI_ERROR (Status);
    }
  }

  return Status;
}


/**
  Callback on gEfiPeiMasterBootModePpiGuidiGuid so install gEfiPeiBootInRecoveryModePpiGuid.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The MasterBootMode PPI.  Not used.

  @retval EFI_SUCCESS          - The function completed successfully.
**/
EFI_STATUS
EFIAPI
PlatformInitRecoveryCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_BOOT_MODE        BootMode;

  PeiServicesGetBootMode (&BootMode);

  if ((BootMode != BOOT_IN_RECOVERY_MODE) && (BootMode != BOOT_ON_S3_RESUME)) {
    //
    // Check if flash update might be needed even if there is no Capsule detected.
    //
    if (IsSystemFirmwareUpdateInProgress () || SpiIsTopSwapEnabled ()) {
      PeiServicesSetBootMode (BOOT_ON_FLASH_UPDATE);
      //
      //  Install recovery ppi to make RstVmdPeim module install blockio
      //
      PeiServicesInstallPpi (&mPpiListRecoveryBootMode);
    }
  }

  return EFI_SUCCESS;
}

#if FixedPcdGetBool(PcdResiliencyEnable) == 1
/**
  Check if ACM policy (e.g., BPM, KM) is corrupted

  @retval TRUE  - ACM policy is corrupted
  @retval FALSE - ACM policy is valid

**/
BOOLEAN
IsAcmPolicyCorrupted (
  VOID
  )
{
  UINT8                       TxtPolicyDataRecord;
  UINT64                      BootGuardBootStatus;

  TxtPolicyDataRecord  = 0;
  BootGuardBootStatus  = 0;

  //
  // Check if TXT is enabled in Type A TXT policy data record first.
  // In 0T profile, ACM policy bit is reliable only when TXT is enabled in Type A TXT policy
  //
  TxtPolicyDataRecord = CmosRead8 (FIT_REC_TXT_POLICY_TYPE_A);
  DEBUG ((DEBUG_INFO, "TxtPolicyDataRecord=0x%x\n", TxtPolicyDataRecord));

  if (TxtPolicyDataRecord & BIT4) {
    //
    // TXT is enabled. Check ACM policy valid bit now.
    //
    BootGuardBootStatus = *(UINT64 *) (UINTN) (TXT_PUBLIC_BASE + R_CPU_BOOT_GUARD_BOOTSTATUS);
    DEBUG ((DEBUG_INFO, "R_CPU_BOOT_GUARD_BOOTSTATUS = 0x%016lx\n", BootGuardBootStatus));
    if ((BootGuardBootStatus & BIT54) == 0) {
      //
      // ACM policy valid bit is NOT set
      //
      DEBUG ((DEBUG_ERROR, "ACM policy is corrupted\n"));
      return TRUE;
    }
  }

  return FALSE;
}

/**
  Check if FIT table content is invalid according to FIT BIOS specification.
  Also do some basic check on Type A/B/C

  @retval TRUE  - Invalid FIT table or type entry is detected
  @retval FALSE - Invalid FIT table or type entry is not detected

**/
BOOLEAN
IsFitOrTypeEntryCorrupted (
  IN  UINT64               FitPointer
  )
{
  UINT64                                 FitEnding;
  FIRMWARE_INTERFACE_TABLE_ENTRY         *FitEntry;
  FIRMWARE_INTERFACE_TABLE_ENTRY_PORT    *FitEntryPort;
  UINT32                                 EntryNum;
  UINT32                                 Index;
  BOOLEAN                                BpmSigFound;
  BOOLEAN                                KeymSigFound;
  BOOLEAN                                TxtPolicyFound;

  DEBUG ((DEBUG_INFO, "Check FIT table.\n"));

  BpmSigFound    = FALSE;
  KeymSigFound   = FALSE;
  TxtPolicyFound = FALSE;

  //
  // The entire FIT table must reside with in the firmware address range
  // of (4GB-16MB) to (4GB-40h).
  //
  if ((FitPointer < (SIZE_4GB - SIZE_16MB)) || (FitPointer >= (SIZE_4GB - 0x40))) {
    //
    // Invalid FIT address, treat it as no FIT table.
    //
    DEBUG ((DEBUG_ERROR, "Invalid pointer 0x%p.\n", FitPointer));
    return TRUE;
  }

  //
  // Check FIT header.
  //
  FitEntry = (FIRMWARE_INTERFACE_TABLE_ENTRY *) (UINTN) FitPointer;
  if ((FitEntry[0].Type != FIT_TYPE_00_HEADER) ||
      (FitEntry[0].Address != FIT_TYPE_00_SIGNATURE)) {
    DEBUG ((DEBUG_ERROR, "Error: Invalid FIT header.\n"));
    return TRUE;
  }

  //
  // Check FIT version.
  //
  if (FitEntry[0].Version != FIT_TYPE_VERSION) {
    DEBUG ((DEBUG_ERROR, "Unsupported version.\n"));
    return TRUE;
  }

  //
  // Check FIT ending address in valid range.
  //
  EntryNum = *(UINT32 *)(&FitEntry[0].Size[0]) & 0xFFFFFF;
  FitEnding = FitPointer + sizeof (FIRMWARE_INTERFACE_TABLE_ENTRY) * EntryNum;
  if (FitEnding  > (SIZE_4GB - 0x40)) {
    DEBUG ((DEBUG_ERROR, "Table exceeds valid range.\n"));
    return TRUE;
  }

  //
  // Calculate FIT checksum if Checksum Valid bit is set.
  //
  if (FitEntry[0].C_V &&
      CalculateSum8 ((UINT8*) FitEntry, sizeof (FIRMWARE_INTERFACE_TABLE_ENTRY) * EntryNum) != 0) {
    DEBUG ((DEBUG_ERROR, "Invalid checksum.\n"));
    return TRUE;
  }

  //
  // Check required entry (Type A/B/C).
  //
  for (Index = 0; Index < EntryNum; Index++) {
    if ((FitEntry[Index].Type == FIT_TYPE_0A_TXT_POLICY) && (FitEntry[Index].Version == 0)) {
      FitEntryPort = (FIRMWARE_INTERFACE_TABLE_ENTRY_PORT *)&FitEntry[Index];
      //
      // W/A: Hard coded check on TXT policy
      //
      if ((FitEntryPort->IndexPort == 0x70) && \
          (FitEntryPort->DataPort  == 0x71) && \
          (FitEntryPort->Width     == 1) && \
          (FitEntryPort->Bit       == 4) && \
          (FitEntryPort->Index     == FIT_REC_TXT_POLICY_TYPE_A)) {
        DEBUG ((DEBUG_INFO, "TxtPolicyFound\n"));
        TxtPolicyFound = TRUE;
      }
    }

    if (FitEntry[Index].Type == FIT_TYPE_0B_KEY_MANIFEST) {
      //
      // Check KEYM signature
      //
      if (*(UINT64 *) (UINTN) FitEntry[Index].Address == KEY_MANIFEST_STRUCTURE_ID) {
        DEBUG ((DEBUG_INFO, "KeymSigFound\n"));
        KeymSigFound = TRUE;
      }
    }

    if (FitEntry[Index].Type == FIT_TYPE_0C_BOOT_POLICY_MANIFEST) {
      //
      // Check ACBP signature
      //
      if (*(UINT64 *) (UINTN) FitEntry[Index].Address == BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID) {
        DEBUG ((DEBUG_INFO, "BpmSigFound\n"));
        BpmSigFound = TRUE;
      }
    }
  }

  if ((!TxtPolicyFound) || (!KeymSigFound) || (!BpmSigFound)) {
    return TRUE;
  }

  return FALSE;
}

/**
  This function checks whether there is a FIT BIOS error or ACM policy error.

  @retval TRUE  - FIT or policy error is detected
  @retval FALSE - FIT or policy error is not detected
**/
BOOLEAN
IsFitOrPolicyError (
  VOID
  )
{
  UINT8                       FitEntryType;
  UINT8                       FitErrorCode;
  MSR_FIT_BIOS_ERROR_REGISTER FitData;
  UINT64                      FitPointer;
  ACM_BIOS_POLICY             AcmPolicyStatus;

  DEBUG ((DEBUG_INFO, "Check MSR FIT or ACM policy error\n"));

  FitPointer           = *(UINT64 *) (UINTN) FIT_POINTER_ADDRESS;
  AcmPolicyStatus.Data = 0;

  //
  // Check FIT BIOS Error in MSR first
  //
  FitData.Uint64 = AsmReadMsr64 (MSR_FIT_BIOS_ERROR);
  FitEntryType = (UINT8) FitData.Bits.EntryType;
  FitErrorCode = (UINT8) FitData.Bits.ErrorCode;

  DEBUG ((DEBUG_INFO, "FitEntryType:0x%x\n", FitEntryType));
  DEBUG ((DEBUG_INFO, "FitErrorCode:0x%x\n", FitErrorCode));

  if ((FitEntryType == FIT_STARTUP_ACM_ENTRY) && \
      (FitErrorCode == FIT_STARTUP_ACM_NOT_SUPPORTED)) {
    //
    // ACM is not supported (Profile 0). Skip the following check.
    //
    DEBUG ((DEBUG_INFO, "Skip FIT ACM Entry error when BtG is unsupported.\n"));
    return FALSE;
  }

  if (FitErrorCode != FIT_SUCCESSFUL) {
    //
    // FIT error is detected. (e.g., Microcode or ACM loading failure)
    //
    DEBUG ((DEBUG_ERROR, "Fit Error in MSR is detected.\n"));
    return TRUE;
  }

  //
  // FIT error in MSR is not detected.
  // Further check FIT table and ACM policy valid bit when TXT is supported
  // Below check is targeted on 0T profile specifically.
  // In 4/4T/5/5T profile, ACM policy corruption is supposed to be handled by ACM/CSME.
  //
  AcmPolicyStatus.Data = *(UINT64 *) (UINTN) (TXT_PUBLIC_BASE + R_CPU_ACM_POLICY_STATUS);
  DEBUG ((DEBUG_INFO, "R_CPU_ACM_POLICY_STATUS = 0x%016lx\n", AcmPolicyStatus.Data));

  if (AcmPolicyStatus.Bits.TxtSupported) {
    //
    // TXT is supported. Do a basic check on FIT table and Type A/B/C first
    //
    if (IsFitOrTypeEntryCorrupted (FitPointer)) {
      DEBUG ((DEBUG_ERROR, "Invalid FIT or type entry detected.\n"));
      return TRUE;
    }

    //
    // Further check ACM policy
    //
    if (IsAcmPolicyCorrupted ()) {
      DEBUG ((DEBUG_ERROR, "Invalid ACM policy detected.\n"));
      return TRUE;
    }
  }

  DEBUG ((DEBUG_INFO, "Fit or policy error is not detected.\n"));
  return FALSE;
}
#endif

/**
  Entry point of this module.

  @param[in] FileHandle   Handle of the file being invoked.
  @param[in] PeiServices  Describes the list of possible PEI Services.

  @retval EFI_SUCCESS     The function completed successfully.

**/
EFI_STATUS
EFIAPI
PlatformInitRecoveryEntryPoint (
  IN       EFI_PEI_FILE_HANDLE      FileHandle,
  IN CONST EFI_PEI_SERVICES         **PeiServices
  )
{
#if FixedPcdGetBool(PcdResiliencyEnable) == 1
  if (IsFitOrPolicyError ()) {
    DEBUG ((DEBUG_INFO, "Trigger EC WDT timeout for BIOS recovery.\n"));
    ArmEcWdtWithTimeOut (3);
    CpuDeadLoop ();
  }
#endif

  PeiServicesNotifyPpi (&mPlatformInitRecoveryNotifyList);
  PeiServicesNotifyPpi (&mMemDiscoveredNotifyList);

  return EFI_SUCCESS;
}
