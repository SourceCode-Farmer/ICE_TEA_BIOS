/** @file
  Library for FmpDeviceLib to support Fmp Capsule Dependency.

  To support getting and saving dependency, parameter "Image" of FmpDeviceSetImage
  and FmpDeviceGetImage is extended to include dependency data inside.
  The Layout of image:
  +--------------------------+
  |   Dependency Op-codes    |
  +--------------------------+
  |    Fmp Payload Image     |
  +--------------------------+

  This library provides function to save dependency into a protected storage and
  get dependency from the storage.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#include <PiDxe.h>

#include <Library/FmpDependencySupportLib.h>

#include <Library/UefiLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

#include <Protocol/VariableLock.h>


/**
  Calculate the size of dependency op-codes associated with the image.

  The param "Image" of FmpDeviceSetImage consists of dependency op-codes
  and Fmp Payload Image.

  Assumption: The dependency passes validation in Fmp->CheckImage().

  @param[in]   Image       Pointer to Image.
  @param[in]   ImageSize   Size of Image.

  @retval  Size of dependency op-codes.

**/
UINTN
CalculateFmpDependencySizeInImage (
  IN  UINT8                             *Image,
  IN  CONST UINTN                       ImageSize
  )
{
  UINT8  *Depex;

  Depex = ((EFI_FIRMWARE_IMAGE_DEP *)Image)->Dependencies;
  while (Depex < Image + ImageSize) {
    switch (*Depex)
    {
    case EFI_FMP_DEP_PUSH_GUID:
      Depex += sizeof (EFI_GUID) + 1;
      break;
    case EFI_FMP_DEP_PUSH_VERSION:
      Depex += sizeof (UINT32) + 1;
      break;
    case EFI_FMP_DEP_VERSION_STR:
      Depex += AsciiStrnLenS ((CHAR8 *) Depex, Image + ImageSize - Depex) + 1;
      break;
    case EFI_FMP_DEP_AND:
    case EFI_FMP_DEP_OR:
    case EFI_FMP_DEP_NOT:
    case EFI_FMP_DEP_TRUE:
    case EFI_FMP_DEP_FALSE:
    case EFI_FMP_DEP_EQ:
    case EFI_FMP_DEP_GT:
    case EFI_FMP_DEP_GTE:
    case EFI_FMP_DEP_LT:
    case EFI_FMP_DEP_LTE:
      Depex += 1;
      break;
    case EFI_FMP_DEP_END:
      Depex += 1;
      return Depex - Image;
    default:
      return 0;
    }
  }

  return 0;
}

/**
  Save dependency op-codes.

  @param[in]   Depex       Dependency op-codes.
  @param[in]   DepexSize   Size of dependency op-codes.

  @retval  EFI_SUCCESS  Succeed to save the dependency op-codes.
  @retval  Others       Fail to save the dependency op-codes.

**/
EFI_STATUS
SaveFmpDependencyToStorage (
  IN  EFI_FIRMWARE_IMAGE_DEP             *Depex,
  IN  UINTN                              DepexSize
  )
{
  EFI_STATUS Status;

  //
  // DepexSize = 0 means to clear exsiting dependency op-codes.
  //
  if (DepexSize > 0 && Depex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Save dependency op-codes to "FmpDepex" variable.
  //
  Status = gRT->SetVariable (
                  L"FmpDepex",
                  &gEfiCallerIdGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  DepexSize,
                  (VOID *)Depex
                  );

  return Status;
}

/**
  Get dependency op-codes.

  @param[out]   DepexSize  Size of dependency op-codes.

  @retval   Pointer to the dependency op-codes.

**/
EFI_FIRMWARE_IMAGE_DEP *
GetFmpDependencyFromStorage (
  OUT UINTN                              *DepexSize
  )
{
  EFI_STATUS             Status;
  UINTN                  DepexVarSize;
  EFI_FIRMWARE_IMAGE_DEP *Depex;

  Depex = NULL;
  *DepexSize = 0;
  DepexVarSize = 0;

  //
  // Get dependency from variable.
  //
  Status = GetVariable2 (
             L"FmpDepex",
             &gEfiCallerIdGuid,
             (VOID **) &Depex,
             &DepexVarSize
             );
  if (!EFI_ERROR (Status)) {
    *DepexSize = DepexVarSize;
  } else if (Status != EFI_NOT_FOUND) {
    //
    // Treat EFI_NOT_FOUND as no dependency.
    // Other errors are treated as dependency evalutates to FLASE.
    //
    *DepexSize = 2;
    Depex = AllocatePool (*DepexSize);
    if (Depex != NULL) {
      Depex->Dependencies[0] = EFI_FMP_DEP_FALSE;
      Depex->Dependencies[1] = EFI_FMP_DEP_END;
    }
  }

  return Depex;
}

/**
  Lock the FmpDepex variable.

  @param[in]   Event     The Event that is being processed, not used.
  @param[in]   Context   Event Context, not used.

**/
VOID
EFIAPI
DepexVariableLock (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  )
{
  EFI_STATUS                    Status;
  EDKII_VARIABLE_LOCK_PROTOCOL  *VariableLock;

  VariableLock = NULL;
  Status = gBS->LocateProtocol (
                  &gEdkiiVariableLockProtocolGuid,
                  NULL,
                  (VOID **)&VariableLock
                  );
  if (!EFI_ERROR (Status) && VariableLock != NULL) {
    Status = VariableLock->RequestToLock (
                             VariableLock,
                             L"FmpDepex",
                             &gEfiCallerIdGuid
                             );
  }
}

/**
  The constructor function to lock FmpDepex variable before EndOfDxe.

  @param[in]   ImageHandle    The firmware allocated handle for the EFI image.
  @param[in]   SystemTable    A pointer to the EFI System Table.

  @retval EFI_SUCCESS   The constructor succeeds.

**/
EFI_STATUS
EFIAPI
FmpDependencySupportLibConstructor (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                    Status;
  EFI_EVENT                     Event;

  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  DepexVariableLock,
                  NULL,
                  &gEfiEndOfDxeEventGroupGuid,
                  &Event
                  );

  return Status;
}
