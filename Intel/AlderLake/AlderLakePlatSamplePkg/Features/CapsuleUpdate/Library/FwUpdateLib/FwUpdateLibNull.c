/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/FWUpdateLib.h>

/**
  Starting a Full FW Update from a buffer.

  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.
  @param[in]  OemId         OEM ID to compare with OEM ID in FW (if exist). Can be NULL.
  @param[in]  Func          A callback function that reports the progress of sending the buffer
                            to FW (not the progress of the update itself). Can be NULL.
                            void* Func(UINT32 BytesSentToFw, UINT32 TotalBytesToSendToFw);

  @retval SUCCESS  If Update started successfully. Error code otherwise.
**/
UINT32
FwuFullUpdateFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength,
  IN  _UUID *OemId,
  IN  void(*Func)(UINT32, UINT32))
{
  return 0;
}

/**
  Starting a Partial FW Update from a buffer.

  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.
  @param[in]  PartitionId   ID of partition to update. Only partitions that are allowed to be partially updated.
  @param[in]  Func          A callback function that reports the progress of sending the buffer
                            to FW (not the progress of the update itself). Can be NULL.
                            void* Func(UINT32 BytesSentToFw, UINT32 TotalBytesToSendToFw);

  @retval SUCCESS  If Update started successfully. Error code otherwise.
**/
UINT32
FwuPartialUpdateFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength,
  IN  UINT32 PartitionId,
  IN  void(*Func)(UINT32, UINT32))
{
  return 0;
}

/**
  Check for Update progress.
  If in progress, return the current percent.
  If finished, return the status of the update, and the needed reset type after the update.
  This function should be called only after starting the update by calling FwuUpdateFull/Partial...

  @param[out] InProgress       True if Update is still in progress. False if Update finished. Caller allocated.
  @param[out] CurrentPercent   Current percent of the update, if Update is in progress. Caller allocated.
  @param[out] FwUpdateStatus   FW error code status of the update, if it finished (success or error code). Caller allocated.
  @param[out] NeededResetType  Needed reset type after the update, if it finished. Caller allocated.
                               MFT_PART_INFO_EXT_UPDATE_ACTION_NONE         0
                               MFT_PART_INFO_EXT_UPDATE_ACTION_HOST_RESET   1
                               MFT_PART_INFO_EXT_UPDATE_ACTION_CSE_RESET    2
                               MFT_PART_INFO_EXT_UPDATE_ACTION_GLOBAL_RESET 3

  @retval SUCCESS  If Update is still in progress, or finished successfully. Error code otherwise.
**/
UINT32
FwuCheckUpdateProgress (
  OUT BOOLEAN *InProgress,
  OUT UINT32 *CurrentPercent,
  OUT UINT32 *FwUpdateStatus,
  OUT UINT32 *NeededResetType
  )
{
  return 0;
}

/**
  Get FW Update enabling state.

  @param[out] EnabledState  FW Update enabling state. Caller allocated.
                            FW_UPDATE_DISABLED = 0. Full Disabled. Partial Enabled.
                            FW_UPDATE_ENABLED = 1. Full Enabled. Partial Enabled.
                            FW_UPDATE_FULL_AND_PARTIAL_DISABLED = 3. Full Disabled. Partial Disabled.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuEnabledState (
  OUT UINT16 *EnabledState
  )
{
  return 0;
}

/**
  Set FW Update enabling state.
  Supported only before EOP.

  @param[in]  EnabledState  FW Update enabling state.
                            FW_UPDATE_DISABLED = 0. Full Disabled. Partial Enabled.
                            FW_UPDATE_ENABLED = 1. Full Enabled. Partial Enabled.
                            FW_UPDATE_FULL_AND_PARTIAL_DISABLED = 3. Full Disabled. Partial Disabled.

  @return SUCCESS  If succeeded. Error code otherwise.
*/
UINT32
FwuSetEnabledState (
  IN  UINT32 EnabledState
  )
{
  return 0;
}

/**
  Get OEM ID from flash.

  @param[out] OemId  OEM ID from flash. Caller allocated.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuOemId (
  OUT _UUID *OemId
  )
{
  return 0;
}

/**
  Get FW Type, from the flash image.

  @param[out] FwType  FW Type. Caller allocated.
                      FWU_FW_TYPE_INVALID 0
                      FWU_FW_TYPE_RESERVED 1
                      FWU_FW_TYPE_SLIM 2
                      FWU_FW_TYPE_CONSUMER 3
                      FWU_FW_TYPE_CORPORATE 4

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuFwType (
  OUT UINT32 *FwType
  )
{
  return 0;
}

/**
  @brief Get FW Type, from the Update Image buffer.

  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.
  @param[out] FwType        FW Type. Caller allocated.
                            FWU_FW_TYPE_INVALID 0
                            FWU_FW_TYPE_RESERVED 1
                            FWU_FW_TYPE_SLIM 2
                            FWU_FW_TYPE_CONSUMER 3
                            FWU_FW_TYPE_CORPORATE 4

  @return SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuFwTypeFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength,
  OUT UINT32 *FwType
  )
{
  return 0;
}

/**
  Get PCH SKU, from the flash image.

  @param[out] PchSku  PCH SKU. Caller allocated.
                      FWU_PCH_SKU_INVALID 0
                      FWU_PCH_SKU_H 1
                      FWU_PCH_SKU_LP 2

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuPchSku (
  OUT UINT32 *PchSku
  )
{
  return 0;
}

/**
  @brief Get PCH SKU, from the Update Image buffer.

  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.
  @param[out] PchSku        PCH SKU. Caller allocated.
                            FWU_PCH_SKU_INVALID 0
                            FWU_PCH_SKU_H 1
                            FWU_PCH_SKU_LP 2

  @return SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuPchSkuFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength,
  OUT UINT32 *PchSku
  )
{
  return 0;
}

/**
  @brief Get Product Segment, from the flash image.

  @param[out] ProductSegment  Product Segment. Caller allocated.
                              FWU_PRODUCT_SEGMENT_CLIENT 1
                              FWU_PRODUCT_SEGMENT_HEDT 2
                              FWU_PRODUCT_SEGMENT_WS 3
                              FWU_PRODUCT_SEGMENT_CONVERGED_MOBILITY 4
                              FWU_PRODUCT_SEGMENT_IOT 5
                              FWU_PRODUCT_SEGMENT_SMALL_CORE_ENTRY_LEVEL 6

  @return SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuProductSegmentFromFlash (
  OUT UINT32 *ProductSegment
  )
{
  return 0;
}

/**
  @brief Get Product Segment, from the Update Image buffer.

  @param[in]  Buffer          Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength    Length of the buffer in bytes.
  @param[out] ProductSegment  Product Segment. Caller allocated.
                              FWU_PRODUCT_SEGMENT_CLIENT 1
                              FWU_PRODUCT_SEGMENT_HEDT 2
                              FWU_PRODUCT_SEGMENT_WS 3
                              FWU_PRODUCT_SEGMENT_CONVERGED_MOBILITY 4
                              FWU_PRODUCT_SEGMENT_IOT 5
                              FWU_PRODUCT_SEGMENT_SMALL_CORE_ENTRY_LEVEL 6

  @return SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuProductSegmentFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength,
  OUT UINT32 *ProductSegment
  )
{
  return 0;
}

/**
  @brief Check if the Update Image, from buffer, is compatible to the Flash
         Image, by comparing TCB SVN, ARB SVN, VCN of all the updatable
         partitions in the Flash Image and in the Update Image.
         It is not possible to update to a lower value.
         Update may not be possible for other reasons, that are not checked
         by this function.
  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.

  @return SUCCESS  If compatible. Error code otherwise.
**/
UINT32
FwuCheckCompatibilityFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength
  )
{
  return 0;
}

/**
  Get version of a specific partition, from the flash image.

  @param[in]  PartitionId  ID of partition. If the FW version of CSE is needed,
                           use FTPR partition ID: FPT_PARTITION_NAME_FTPR.
  @param[out] Major        Major number of version. Caller allocated.
  @param[out] Minor        Minor number of version. Caller allocated.
  @param[out] HotFix       Hotfix number of version. Caller allocated.
  @param[out] Build        Build number of version. Caller allocated.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuPartitionVersionFromFlash (
  IN  UINT32 PartitionId,
  OUT UINT16 *Major,
  OUT UINT16 *Minor,
  OUT UINT16 *Hotfix,
  OUT UINT16 *Build
  )
{
  return 0;
}

/**
  Get version of a specific partition, from the Update Image buffer.

  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.
  @param[in]  PartitionId   ID of partition. If the FW version of CSE is needed,
                            use FTPR partition ID: FPT_PARTITION_NAME_FTPR.
  @param[out] Major         Major number of version. Caller allocated.
  @param[out] Minor         Minor number of version. Caller allocated.
  @param[out] HotFix        Hotfix number of version. Caller allocated.
  @param[out] Build         Build number of version. Caller allocated.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuPartitionVersionFromBuffer (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength,
  IN  UINT32 PartitionId,
  OUT UINT16 *Major,
  OUT UINT16 *Minor,
  OUT UINT16 *Hotfix,
  OUT UINT16 *Build
  )
{
  return 0;
}

/**
  Get vendor ID of a specific partition, from the flash image.

  @param[in]  PartitionId  ID of partition.
  @param[out] VendorId     Vendor ID of partition. Caller allocated.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuPartitionVendorIdFromFlash (
  IN  UINT32 PartitionId,
  OUT UINT32 *VendorId
  )
{
  return 0;
}

/**
  Get the the current image from the flash - Restore Point Image, and save it to buffer.

  @param[out] buffer        Buffer of the saved Restore Point Image.
                            Allocated by the function, only in case of SUCCESS. NULL otherwise.
                            Caller should free the buffer
                            using FreePool() in EFI.
  @param[out] bufferLength  Length of the buffer in bytes.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuSaveRestorePointToBuffer (
  OUT UINT8 **buffer,
  OUT UINT32 *bufferLength
  )
{
  return 0;
}

/**
  Set ISH configuration file.
  Receive PDT file payload, create bios2ish file as a composition of
  bios2ish header (with PDT Update data type) and PDT file payload
  and send it to FW to set file.
  Supported only before EOP.

  @param[in] Buffer        Buffer of PDT file payload.
  @param[in] BufferLength  Length of the buffer in bytes.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuSetIshConfig (
  IN  UINT8 *Buffer,
  IN  UINT32 BufferLength
  )
{
  return 0;
}

/**
  Get PDT version and VDV version from ISH file INTC_pdt.

  @param[out] PdtVersion  PDT version. Caller allocated.
  @param[out] VdvVersion  VDV version. Caller allocated.

  @retval SUCCESS  If succeeded. Error code otherwise.
**/
UINT32
FwuGetIshPdtVersion (
  OUT UINT8 *PdtVersion,
  OUT UINT8 *VdvVersion
  )
{
  return 0;
}
