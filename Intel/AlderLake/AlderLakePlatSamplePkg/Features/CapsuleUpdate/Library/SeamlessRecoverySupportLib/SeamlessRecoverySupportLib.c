/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Library instance to support Seamless Recovery based system firmware update.
  The major functionalities are
  1. Save backup files to external storage used in recovery path.
  2. Get/Set/Clear update progress.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#include <Library/SeamlessRecoverySupportLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiLib.h>
#include <Library/HobLib.h>
#include <Library/BaseCryptLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/SpiAccessLib.h>
#include <Library/PlatformFlashAccessLib.h>
#include <Library/CpuPlatformLib.h>
#include <Protocol/FirmwareVolume2.h>
#include <BiosGuard.h>

#include <Protocol/SimpleFileSystem.h>
#include <Protocol/DevicePath.h>
#include <Protocol/PciIo.h>
#include <Protocol/Spi.h>

#include <Guid/FileSystemInfo.h>
#include <Guid/FileInfo.h>
#include <Guid/FmpCapsule.h>

#include <IndustryStandard/Pci.h>
#include <Library/VmdInfoLib.h>
//[-start-201112-IB16810138-add]//
#include <Library/VariableLib.h>
//[-end-201112-IB16810138-add]//

EFI_HANDLE                               mBackUpFileSystemHandle = NULL;
SYSTEM_FIRMWARE_UPDATE_PROGRESS          mPreviousUpdateProgress = {0};
BOOLEAN                                  mBiosGuardEnabled       = FALSE;
//[-start-201030-IB16810136-add]//
#define SHA256_DIGEST_HASH_SIZE          0x20
#define IBB_SIZE                         0x400000  //4m
//[-end-201030-IB16810136-add]//

/**
  Returns the FMP Payload Header size in bytes.

  @param[in]  Header          FMP Payload Header to evaluate
  @param[in]  FmpPayloadSize  Size of FMP payload
  @param[out] Size            The size, in bytes, of the FMP Payload Header.

  @retval EFI_SUCCESS            The firmware version was returned.
  @retval EFI_INVALID_PARAMETER  Header is NULL.
  @retval EFI_INVALID_PARAMETER  Size is NULL.
  @retval EFI_INVALID_PARAMETER  Header is not a valid FMP Payload Header.

**/
EFI_STATUS
EFIAPI
GetFmpPayloadHeaderSize (
  IN  CONST VOID   *Header,
  IN  CONST UINTN  FmpPayloadSize,
  OUT UINT32       *Size
  );

/**
  Dump raw data.

  @param[in]  Data  raw data
  @param[in]  Size  raw data size

**/
VOID
InternalDumpData (
  IN UINT8   *Data8,
  IN UINTN   DataSize
  )
{
  DEBUG_CODE_BEGIN();

  UINTN      Index;

  for (Index = 0; Index < DataSize; Index++) {
    if (Index % 0x10 == 0) {
      DEBUG ((DEBUG_INFO, "\n%08X:", Index));
    }
    DEBUG ((DEBUG_INFO, " %02X", *Data8++));
  }
  DEBUG ((DEBUG_INFO, "\n"));

  DEBUG_CODE_END();
}

/**
  Calculate SHA256 Hash

  @param[in]  Data   data
  @param[in]  Size   data size
  @param[out] Digest SHA256 digest

**/
VOID
CreateSha256Hash (
  IN  UINT8     *Data,
  IN  UINTN     Size,
  OUT UINT8     *Digest
  )
{
  UINTN       CtxSize;
  VOID        *HashCtx;

  CtxSize = Sha256GetContextSize ();
  HashCtx = AllocatePool (CtxSize);
  ASSERT (HashCtx != NULL);
  Sha256Init (HashCtx);
  Sha256Update (HashCtx, Data, Size);
  Sha256Final (HashCtx, Digest);
  InternalDumpData (Digest, 32);

  FreePool (HashCtx);
}
//[-start-201030-IB16810136-add]//
EFI_STATUS
CreateImageHash (
  IN VOID                         *FvImage,
  IN UINTN                        FvImageSize
){
  EFI_STATUS                            Status;
  UINT8                                 Sha256[SHA256_DIGEST_HASH_SIZE];

  ZeroMem (Sha256, SHA256_DIGEST_HASH_SIZE);
  DEBUG ((DEBUG_INFO, "Create Capsule digest:\n"));
  CreateSha256Hash ((UINT8 *)FvImage, FvImageSize, Sha256);
  Status = gRT->SetVariable (
                  SYSBIOS_NEW_CAPSULE_DIGEST_VARIABLE_NAMEN,
                  &gSysFwUpdateDigiestGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  SHA256_DIGEST_HASH_SIZE,
                  Sha256
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to keep message digest of the saved Capsule image.\n"));
    return Status;
  }

  return EFI_SUCCESS;
}
//[-end-201030-IB16810136-add]//

/**
  Connect storage controllers to back up relevant files for Fault Tolerance/Seamless Recovery support.
  Currently only NVME and onboard SATA controller are supported.

**/
VOID
ConnectPlatformController (
  VOID
  )
{
  EFI_STATUS           Status;
  UINTN                Index;
  UINTN                HandleCount;
  EFI_HANDLE           *HandleBuffer;
  PCI_TYPE00           PciData;
  EFI_PCI_IO_PROTOCOL  *PciIo;
  UINTN                Segment;
  UINTN                Bus;
  UINTN                Device;
  UINTN                Function;

  HandleCount  = 0;
  HandleBuffer = NULL;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPciIoProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiPciIoProtocolGuid,
                    (VOID *) &PciIo
                    );
    ASSERT_EFI_ERROR (Status);
    PciIo->GetLocation (PciIo, &Segment, &Bus, &Device, &Function);
    Status = PciIo->Pci.Read (
                          PciIo,
                          EfiPciIoWidthUint8,
                          0,
                          sizeof (PciData),
                          &PciData
                          );
    if (Bus != 0) {
      //
      //  Locate all NVME controllers and connect them.
      //
      if ((PciData.Hdr.ClassCode[2] == PCI_CLASS_MASS_STORAGE) &&
          (PciData.Hdr.ClassCode[1] == PCI_CLASS_MASS_STORAGE_SOLID_STATE) &&
          (PciData.Hdr.ClassCode[0] == PCI_IF_MASS_STORAGE_SOLID_STATE_ENTERPRISE_NVMHCI)) {
        Status = gBS->ConnectController (HandleBuffer[Index], NULL, NULL, TRUE);
        if (EFI_ERROR (Status)) {
          DEBUG ((DEBUG_ERROR, "Connect NVME Controller on PCI %d/%d/%d - %r\n", Bus, Device, Function, Status));
        }
      }
    } else {
      //
      //  Locate Sata controllers and connect them.
      //
      if ((PciData.Hdr.ClassCode[2] == PCI_CLASS_MASS_STORAGE) &&
          (PciData.Hdr.ClassCode[1] == PCI_CLASS_MASS_STORAGE_SATADPA)) {
        Status = gBS->ConnectController (HandleBuffer[Index], NULL, NULL, TRUE);
        if (EFI_ERROR (Status)) {
          DEBUG ((DEBUG_ERROR, "Connect Sata Controller on PCI %d/%d/%d - %r\n", Bus, Device, Function, Status));
        }
      }
      if ((PciData.Hdr.ClassCode[2] == PCI_CLASS_MASS_STORAGE) &&
          (PciData.Hdr.ClassCode[1] == PCI_CLASS_MASS_STORAGE_RAID)) {

        //
        // if detected as RAID, need check this PCI device is vmd or not to decide connect or disconnect
        //
        if ((GetVmdBusNumber () == Bus) && (GetVmdDevNumber () == Device) && (GetVmdFuncNumber () == Function)) {
          Status = gBS->ConnectController (HandleBuffer[Index], NULL, NULL, TRUE);
          if (EFI_ERROR (Status)) {
            DEBUG ((DEBUG_ERROR, "Connect vmd Controller on PCI %d/%d/%d - %r\n", Bus, Device, Function, Status));
          }
        } else {
          //
          // In case that RAID controller has been connected before update function being invoked.
          //
          Status = gBS->DisconnectController (
                          HandleBuffer[Index],
                          NULL,
                          NULL
                          );
          if (EFI_ERROR (Status)) {
            DEBUG ((DEBUG_ERROR, "Disconnect RAID Controller on PCI %d/%d/%d - %r\n", Bus, Device, Function, Status));
          }
        }
      }
    }
  }
  if (HandleBuffer) {
    FreePool (HandleBuffer);
  }
  return;
}

/**
  Find out the file system of EFI System partition to save backup files into storeage.

  @param[out] FileSystemHandle    Pointer to the file system handle would be used to keep
                                  Seamless Recovery backup files.

  @retval EFI_SUCCESS             The file system is found.
  @retval Others                  Cannot find an available file system.

**/
EFI_STATUS
SearchBackupFileSystem (
  OUT   EFI_HANDLE       *FileSystemHandle
  )
{
  EFI_STATUS                            Status;
  EFI_HANDLE                            *HandleArray;
  UINTN                                 HandleArrayCount;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  UINTN                                 Index;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *Fs;
  EFI_FILE                              *Root;
  EFI_FILE_SYSTEM_INFO                  *SysInfo;
  UINTN                                 SysInfoSize;
  SYSTEM_FIRMWARE_UPDATE_PROGRESS       PreviousProgress;

  DEBUG ((DEBUG_INFO, "SearchBackupFileSystem - entry\n"));

  *FileSystemHandle = NULL;
  HandleArray       = NULL;
  //
  // Search all EFI system partitions
  //
  Status = gBS->LocateHandleBuffer (ByProtocol, &gEfiPartTypeSystemPartGuid, NULL, &HandleArrayCount, &HandleArray);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot find ESP partition. Status = %r\n", Status));
    return Status;
  }

  DEBUG ((DEBUG_INFO, "ESP handle count is: %d\n", HandleArrayCount));

  for (Index = 0; (Index < HandleArrayCount) && (*FileSystemHandle == NULL); Index++) {
    Status = gBS->HandleProtocol (HandleArray[Index], &gEfiDevicePathProtocolGuid, (VOID **)&DevicePath);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Cannot locate DevicePath protocol. Status = %r\n", Status));
      continue;
    }

    //
    // Get the SFS protocol from the handle
    //
    Status = gBS->HandleProtocol (HandleArray[Index], &gEfiSimpleFileSystemProtocolGuid, (VOID **)&Fs);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Cannot locate SFS protocol. Status = %r\n", Status));
      continue;
    }

    //
    // Previous update is not finished, skip capsule backup
    //
    DEBUG ((DEBUG_INFO, "Check Update Process ...\n"));
    if (IsPreviousUpdateUnfinished (&PreviousProgress)) {
      DEBUG ((DEBUG_INFO, "Available ESP found! Previous update is not finished, skip checking free space.\n"));
      *FileSystemHandle = HandleArray[Index];
      return EFI_SUCCESS;
    }

    //
    // Open the root directory, get EFI_FILE_PROTOCOL
    //
    Status = Fs->OpenVolume (Fs, &Root);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Cannot open volume. Status = %r\n", Status));
      continue;
    }

    SysInfo     = NULL;
    SysInfoSize = 0;
    Status = Root->GetInfo (Root, &gEfiFileSystemInfoGuid, &SysInfoSize, SysInfo);
    if (Status == EFI_BUFFER_TOO_SMALL) {
      SysInfo = AllocateZeroPool (SysInfoSize);
      if (SysInfo == NULL) {
        DEBUG ((DEBUG_ERROR, "System memory is out of resource to allocate file system info buffer.\n"));
        Root->Close (Root);
        break;
      }
      Status = Root->GetInfo (Root, &gEfiFileSystemInfoGuid, &SysInfoSize, SysInfo);
      if (Status == EFI_SUCCESS) {
        DEBUG ((DEBUG_INFO, "File system info:\n"));
        DEBUG ((DEBUG_INFO, "FreeSpace:0x%x bytes\n", SysInfo->FreeSpace));
        DEBUG ((DEBUG_INFO, "BlockSize:0x%x bytes\n", SysInfo->BlockSize));
        DEBUG ((DEBUG_INFO, "ReadOnly:%x\n", SysInfo->ReadOnly));
        if ((SysInfo->FreeSpace >= SIZE_64MB) && (!SysInfo->ReadOnly)) {
          DEBUG ((DEBUG_INFO, "Available ESP found\n"));
          *FileSystemHandle = HandleArray[Index];
          Status = EFI_SUCCESS;
        }
      }
      FreePool (SysInfo);
    }

    Root->Close (Root);
  }

  if (*FileSystemHandle == NULL) {
    Status = EFI_NOT_FOUND;
  }

  FreePool (HandleArray);
  return Status;
}

/**
  Initialize mBackUpFileSystemHandle module variable

  @retval EFI_SUCCESS             Backup file system is found and assign to mBackUpFileSystemHandle
  @retval Others                  Cannot find an available file system to initialize mBackUpFileSystemHandle.

**/
EFI_STATUS
InitializeBackupFileSystem (
  VOID
  )
{
  EFI_STATUS             Status;

  if (mBackUpFileSystemHandle != NULL) {
    //
    // BackupFilesystem has been initialized.
    //
    return EFI_SUCCESS;
  }

  //
  // Connect storage and check free space.
  //
  ConnectPlatformController ();
  Status = SearchBackupFileSystem (&mBackUpFileSystemHandle);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot find storage file system to support seamless recovery. Status = %r\n", Status));
  }

  return Status;
}

/**
  Delete a file from an assigned file system.

  @param[in] FileSystemHandle    Handle of the file system that file would be deleted from.
  @param[in] FileName            Pointer to file name.

  @retval EFI_SUCCESS            File does not exist or deleted the file successfully.
  @retval Others                 Failed to delete the file.

**/
EFI_STATUS
DeleteFile (
  IN   EFI_HANDLE       FileSystemHandle,
  IN   CHAR16           *FileName
  )
{
  EFI_STATUS                            Status;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *FileSystem;
  EFI_FILE                              *Root;
  EFI_FILE                              *FileHandle;

  DEBUG ((DEBUG_INFO, "DeleteBackupFile - entry\n"));

  Status = gBS->HandleProtocol (FileSystemHandle, &gEfiSimpleFileSystemProtocolGuid, (VOID **)&FileSystem);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot locate SFS protocol. Status = %r\n", Status));
    return Status;
  }

  //
  // Open the root directory, get EFI_FILE_PROTOCOL
  //
  Status = FileSystem->OpenVolume (FileSystem, &Root);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot open volume. Status = %r\n", Status));
    return Status;
  }

  Status = Root->Open (Root, &FileHandle, FileName, EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE, 0);
  if (Status == EFI_NOT_FOUND) {
    DEBUG ((DEBUG_INFO, "File %s does not exist. No need to delete\n", FileName));
    return EFI_SUCCESS;
  } else if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot open file: %s. Status = %r\n", FileName, Status));
    return Status;
  }

  if (FileHandle == NULL) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "Failed to open root dir on partition for writing. Stautus = %r\n", Status));
    return Status;
  }

  Status = FileHandle->Delete (FileHandle);
  DEBUG ((DEBUG_INFO, "Delete %s %r\n", FileName, Status));

  Root->Close (Root);

  return Status;
}

/**
  Delete backup Obb files from the external storage.

**/
//[-start-201030-IB16810136-modify]//
VOID
DeleteBackupFvObb (
  VOID
  )
{
  EFI_STATUS               Status;

  Status = InitializeBackupFileSystem ();

  if (Status == EFI_SUCCESS) {
#if FixedPcdGetBool(PcdResiliencyEnable) == 0
    //
    // Not to delete the curent Obb for Resiliency support (PcdResiliencyEnable = TRUE).
    // Delete current Obb file only when we are about to update it.
    //
    DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_CURRENT_FVOBB_BACKUP_FILE_NAME);
#endif
    DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_NEW_FVOBB_BACKUP_FILE_NAME);
  }
}
//[-end-201030-IB16810136-modify]//

/**
  Delete backup Capsule files from the external storage plus the associated NV variable.

**/
VOID
DeleteBackupCapsules (
  VOID
  )
{
  EFI_STATUS               Status;

  Status = InitializeBackupFileSystem ();

  if (Status == EFI_SUCCESS) {
    DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_CAPSULE_BACKUP_FILE_NAME);
    DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_WINDOWS_UX_CAPSULE_FILE_NAME);
  }

  gRT->SetVariable (
         SYSFW_UPDATE_CAPSULE_DIGEST_VARIABLE_NAME,
         &gSysFwUpdateProgressGuid,
         0,
         0,
         NULL
         );

}

/**
  Delete all backup files from the external storage plus the associated NV variable.

**/
VOID
DeleteBackupFiles (
  VOID
  )
{
//[-start-201030-IB16810136-modify]//
    DeleteBackupFvObb ();
//[-end-201030-IB16810136-modify]//
  DeleteBackupCapsules ();
}

/**
  Write a file to an assigned file system.

  @param[in] FileSystemHandle    Handle of the file system that file would be write into.
  @param[in] FileName            Pointer to file name.
  @param[in] FileBuffer          The buffer to be written into file system.
  @param[in] FileSize            The size of FileBuffer.

  @retval EFI_SUCCESS            Wrote the file successfully.
  @retval Others                 Failed to write the file.

**/
//[-start-201112-IB16810138-modify]//
EFI_STATUS
WriteBackupFile (
  IN   EFI_HANDLE       FileSystemHandle,
  IN   CHAR16           *FileName,
  IN   UINT8            *FileBuffer,
  IN   UINTN            FileSize,
  IN   BOOLEAN          IsWriteToDirectory
  )
{
  EFI_STATUS                            Status;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *FileSystem;
  EFI_FILE                              *Root;
  EFI_FILE                              *FileHandle;
  EFI_FILE                              *DirHandle;
  UINTN                                 WriteSize;

  DEBUG ((DEBUG_INFO, "WriteBackupFile (%s) - entry\n", FileName));

  Status = gBS->HandleProtocol (FileSystemHandle, &gEfiSimpleFileSystemProtocolGuid, (VOID **)&FileSystem);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot locate SFS protocol. Status = %r\n", Status));
    return Status;
  }

  //
  // Open the root directory, get EFI_FILE_PROTOCOL
  //
  Status = FileSystem->OpenVolume (FileSystem, &Root);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot open volume. Status = %r\n", Status));
    return Status;
  }
//[-start-201112-IB16810138-add]//
  if (IsWriteToDirectory){
    //
    // Ensure that efi and updatecapsule directories exist
    //
    Status = Root->Open (Root, &DirHandle, L"\\EFI", EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE, 0);
    if (EFI_ERROR (Status)) {
      Status = Root->Open (Root, &DirHandle, L"\\EFI", EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE, EFI_FILE_DIRECTORY);
      if (EFI_ERROR (Status)) {
        Print(L"Unable to create %s directory\n", L"\\EFI");
        return EFI_NOT_FOUND;
      }
    }
    Status = Root->Open (Root, &DirHandle, EFI_CAPSULE_FILE_DIRECTORY, EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE , 0);
    if (EFI_ERROR (Status)) {
      Status = Root->Open (Root, &DirHandle, EFI_CAPSULE_FILE_DIRECTORY, EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE, EFI_FILE_DIRECTORY);
      if (EFI_ERROR (Status)) {
        Print(L"Unable to create %s directory\n", EFI_CAPSULE_FILE_DIRECTORY);
        return EFI_NOT_FOUND;
      }
    }
  } else {  
//[-end-201112-IB16810138-add]//
    Status = Root->Open (Root, &FileHandle, FileName, EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE, 0);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "Cannot open file: %s. Status = %r\n", FileName, Status));
      return Status;
    }
//[-start-201112-IB16810138-add]//
  }

  if (IsWriteToDirectory) {
    //
    // Open UpdateCapsule file
    //
    Status = DirHandle->Open (DirHandle, &FileHandle, FileName, EFI_FILE_MODE_CREATE | EFI_FILE_MODE_WRITE | EFI_FILE_MODE_READ, 0);
    if (EFI_ERROR (Status)) {
      Print (L"Unable to create %s file\n", FileName);
      return EFI_NOT_FOUND;
    }
  }
//[-end-201112-IB16810138-add]//
//[-end-201112-IB16810138-modify]//

  if (FileHandle == NULL) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "Failed to open root dir on partition for writing. Stautus = %r\n", Status));
    return Status;
  }

  do {
    WriteSize = (FileSize > SIZE_4KB) ? SIZE_4KB : FileSize;
    Status = FileHandle->Write (FileHandle, &WriteSize, FileBuffer);
    if (EFI_ERROR (Status)) {
      break;
    }
    FileSize = FileSize - WriteSize;
    FileBuffer = FileBuffer + WriteSize;
  } while (FileSize > 0);

  DEBUG ((DEBUG_INFO, "Write %s %r\n", FileName, Status));
  FileHandle->Close (FileHandle);
  Root->Close (Root);

  return Status;
}

/**
  Read a file from an assigned file system.

  @param[in] FileSystemHandle    Handle of the file system that file would be read.
  @param[in] FileName            Pointer to file name.
  @param[out] Buffer             Address of the buffer to which file is read.
  @param[out] BufferSize         The size of Buffer.

  @retval EFI_SUCCESS            Read the file successfully.
  @retval Others                 Failed to read the file.

**/
EFI_STATUS
ReadBackupFile (
  IN  EFI_HANDLE *FileSystemHandle,
  IN  CHAR16     *FileName,
  OUT VOID       **Buffer,
  OUT UINTN      *BufferSize
  )
{
  EFI_STATUS                            Status;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *FileSystem;
  EFI_FILE                              *Root;
  EFI_FILE                              *FileHandle;
  UINTN                                 FileInfoSize;
  EFI_FILE_INFO                         *FileInfo;
  EFI_GUID                              FileInfoGuid = EFI_FILE_INFO_ID;

  DEBUG ((DEBUG_INFO, "ReadBackupFile (%s) - entry\n", FileName));

  Status = gBS->HandleProtocol (FileSystemHandle, &gEfiSimpleFileSystemProtocolGuid, (VOID **)&FileSystem);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot locate SFS protocol. Status = %r\n", Status));
    return Status;
  }

  Root = NULL;
  //
  // Open the root directory, get EFI_FILE_PROTOCOL
  //
  Status = FileSystem->OpenVolume (FileSystem, &Root);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot open volume. Status = %r\n", Status));
    return Status;
  }

  if (Root == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = Root->Open (Root, &FileHandle, FileName, EFI_FILE_MODE_READ, 0);

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot open file: %s. Status = %r\n", FileName, Status));
    return Status;
  }

  if (FileHandle == NULL) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "Failed to open root dir on partition for reading. Stautus = %r\n", Status));
    return Status;
  }

  DEBUG ((DEBUG_INFO, "FileInfoRead\n"));

  FileInfoSize = 0;
  FileInfo = NULL;

  Status = FileHandle->GetInfo (
                         FileHandle,
                         &FileInfoGuid,
                         &FileInfoSize,
                         NULL
                         );

  if (EFI_ERROR (Status)) {
    if (Status != EFI_BUFFER_TOO_SMALL) {
      DEBUG ((DEBUG_ERROR, "FileRead fail, GetInfo error, Status: %r\n", Status));
      return Status;
    }
  }

  if (Buffer == NULL || BufferSize == NULL) {
    DEBUG ((DEBUG_INFO, "FileInfoRead only, FileInfo Status: %r\n", Status));
    return EFI_SUCCESS;
  }

  DEBUG ((DEBUG_INFO, "FileRead\n"));
  FileInfo = AllocatePool (FileInfoSize);
  if (FileInfo == NULL) {
    DEBUG ((DEBUG_ERROR, "FileRead fail, AllocatePool(FileInfoSize: %x) error\n", FileInfoSize));
    return EFI_OUT_OF_RESOURCES;
  }

  Status = FileHandle->GetInfo (
                         FileHandle,
                         &FileInfoGuid,
                         &FileInfoSize,
                         FileInfo
                         );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "FileRead fail, GetInfo error, Status: %r\n", Status));
    goto done;
  }

  *BufferSize = (UINT32) FileInfo->FileSize;
  if (*BufferSize != 0) {
    *Buffer = AllocateZeroPool (*BufferSize);
    if (*Buffer == NULL) {
      DEBUG ((DEBUG_ERROR, "FileRead fail, AllocatePool(FileSize: %x) error\n", *BufferSize));
      Status = EFI_OUT_OF_RESOURCES;
      goto done;
    }

    Status = FileHandle->Read (
                           FileHandle,
                           BufferSize,
                           *Buffer
                           );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "FileRead fail, Read error, Status: %r\n", Status));
      goto done;
    }

  } else {
    DEBUG ((DEBUG_INFO, "File size is 0, set return Buffer to NULL.\n"));
    *Buffer = NULL;
  }

  Status = EFI_SUCCESS;

done:
  if (FileInfo != NULL) {
    FreePool (FileInfo);
  }

  if (FileHandle != NULL) {
    FileHandle->Close (FileHandle);
  }

  if (Root != NULL) {
    Root->Close (Root);
  }

  DEBUG ((DEBUG_INFO, "FileRead done, Status: %r, BufferSize: %x\n", Status, *BufferSize));
  return Status;
}

/**
  Save Obb image from both current/new BIOS to external storages.
  If NewObbImage is NULL or NewObbImageSize is 0, means to bacup current Obb image only.

  @param[in] NewObbImage       Pointers to Obb image in new BIOS.
  @param[in] NewObbImageSize   The size of NewObbImageImage.

  @retval  EFI_SUCCESS         Successfully backed up necessary files on external storage.
  @retval  Others              Failed to back up necessary files.

**/
//[-start-201030-IB16810136-modify]//
EFI_STATUS
SaveObbToStorage (
  IN VOID                         *FvImage,
  IN UINTN                        FvImageSize
  )
{
  EFI_STATUS                            Status;
  UINT32                                ObbAddress;      
  UINT32                                ObbSize;  
  UINT32                                VariableRegionSize;
  UINT8                                 *FvObbImage;
    
  ObbAddress = 0;
  ObbSize = 0;
  VariableRegionSize = 0;
  FvObbImage = NULL;

  DEBUG ((DEBUG_INFO, "SaveObbToStorage - entry\n"));

  Status = InitializeBackupFileSystem ();

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // 1. Delete previous backup files before creating new one
  //
  DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_CURRENT_FVOBB_BACKUP_FILE_NAME);

  VariableRegionSize = PcdGet32(PcdFlashNvStorageVariableSize) + PcdGet32(PcdFlashNvStorageFtwWorkingSize) + PcdGet32(PcdFlashNvStorageFtwSpareSize);
  ObbAddress = PcdGet32 (PcdFlashAreaBaseAddress) + IBB_SIZE;
  ObbSize = PcdGet32 (PcdFlashAreaSize) - IBB_SIZE - VariableRegionSize;
  
  //
  // 2. Save Obb from current flash to external storage
  //
//[-start-201112-IB16810138-modify]//
  Status = WriteBackupFile (
             mBackUpFileSystemHandle,
             SYSFW_UPDATE_CURRENT_FVOBB_BACKUP_FILE_NAME,
             (UINT8 *) (UINTN) ObbAddress,
             (UINTN) ObbSize,
             FALSE
             );

//[-end-201112-IB16810138-modify]//
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to write Obb.bin(%r)\n", Status));
    return Status;
  }

  //
  // 3. Save Obb from new image to external storage if required.
  //
  VariableRegionSize = PcdGet32(PcdFlashNvStorageVariableSize) + PcdGet32(PcdFlashNvStorageFtwWorkingSize) + PcdGet32(PcdFlashNvStorageFtwSpareSize);
  FvObbImage = (UINT8*)FvImage + VariableRegionSize;

  if ((FvImage != NULL) && (FvImageSize != 0)) {
    DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_NEW_FVOBB_BACKUP_FILE_NAME);
//[-start-201112-IB16810138-modify]//
    Status = WriteBackupFile (
               mBackUpFileSystemHandle,
               SYSFW_UPDATE_NEW_FVOBB_BACKUP_FILE_NAME,
               (UINT8 *) (UINTN) FvObbImage,
               ObbSize,
               FALSE
               );
//[-end-201112-IB16810138-modify]//
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Failed to write ObbN.bin (%r)\n", Status));
      DeleteBackupFvObb ();
    }
  }

//[-end-201030-IB16810136-modify]//
  return Status;
}

/**
  Calculate the size of dependency op-codes associated with the image.

  The param "Image" of FmpDeviceSetImage consists of dependency op-codes
  and Fmp Payload Image.

  Assumption: The dependency passes validation in Fmp->CheckImage().

  @param[in]   Image       Pointer to Image.
  @param[in]   ImageSize   Size of Image.

  @retval  Size of dependency op-codes.

**/
UINTN
CalculateFmpDependencySizeInNewImage (
  IN  UINT8                             *Image,
  IN  CONST UINTN                       ImageSize
  )
{
  UINT8  *Depex;

  Depex = ((EFI_FIRMWARE_IMAGE_DEP *)Image)->Dependencies;
  while (Depex < Image + ImageSize) {
    switch (*Depex)
    {
    case EFI_FMP_DEP_PUSH_GUID:
      Depex += sizeof (EFI_GUID) + 1;
      break;
    case EFI_FMP_DEP_PUSH_VERSION:
      Depex += sizeof (UINT32) + 1;
      break;
    case EFI_FMP_DEP_VERSION_STR:
      Depex += AsciiStrnLenS ((CHAR8 *) Depex, Image + ImageSize - Depex) + 1;
      break;
    case EFI_FMP_DEP_AND:
    case EFI_FMP_DEP_OR:
    case EFI_FMP_DEP_NOT:
    case EFI_FMP_DEP_TRUE:
    case EFI_FMP_DEP_FALSE:
    case EFI_FMP_DEP_EQ:
    case EFI_FMP_DEP_GT:
    case EFI_FMP_DEP_GTE:
    case EFI_FMP_DEP_LT:
    case EFI_FMP_DEP_LTE:
      Depex += 1;
      break;
    case EFI_FMP_DEP_END:
      Depex += 1;
      return Depex - Image;
    default:
      return 0;
    }
  }

  return 0;
}

/**
  Search for a FMP capsule corredspnding to current FMP driver.

  @param[in] PayloadImage       Pointer to FMP payload image (FMP image header is stripped off).
  @param[in] PayloadImageSize   The size of PayloadImage.

  @retval Pointer to the capsule hob found.
          NULL means no such capsules there.

**/
EFI_HOB_UEFI_CAPSULE *
SearchThisFmpCapsule (
  IN VOID                         *PayloadImage,
  IN UINTN                        PayloadImageSize
  )
{
  EFI_PEI_HOB_POINTERS                          HobPointer;
  EFI_CAPSULE_HEADER                            *CapsuleHeader;
  EFI_FIRMWARE_MANAGEMENT_CAPSULE_HEADER        *FmpCapsuleHeader;
  EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER  *ImageHeader;
  UINTN                                         Index;
  UINT64                                        *ItemOffsetList;
  EFI_FIRMWARE_IMAGE_AUTHENTICATION             *FmpImageAuth;
  UINT8                                         *FmpDependency;
  UINTN                                         FmpDependencySize;
  VOID                                          *FmpPayloadHeader;
  UINT32                                        FmpPayloadHeaderSize;
  VOID                                          *PayloadImageInCapsule;
  UINTN                                         PayloadImageSizeInCapsule;
  EFI_STATUS                                    Status;

  //
  // Search all capsule images from hob
  //
  HobPointer.Raw = GetHobList ();
  DEBUG ((DEBUG_INFO, "SearchThisFmpCapsule Start.\n"));

  while ((HobPointer.Raw = GetNextHob (EFI_HOB_TYPE_UEFI_CAPSULE, HobPointer.Raw)) != NULL) {
    CapsuleHeader = (EFI_CAPSULE_HEADER *) (UINTN) HobPointer.Capsule->BaseAddress;
    DEBUG ((DEBUG_INFO, "Check Capsule ...\n"));

    //
    // Must be a nested FMP capsule or FMP capsule
    //
    if (!CompareGuid (&gEfiFmpCapsuleGuid, &CapsuleHeader->CapsuleGuid)) {
      //
      // Might be a nested FMP, check further
      //
      CapsuleHeader = (EFI_CAPSULE_HEADER *)((UINT8 *)CapsuleHeader + CapsuleHeader->HeaderSize);
    }

    if (!CompareGuid (&gEfiFmpCapsuleGuid, &CapsuleHeader->CapsuleGuid)) {
      HobPointer.Raw = GET_NEXT_HOB (HobPointer);
      continue;
    }

    FmpCapsuleHeader = (EFI_FIRMWARE_MANAGEMENT_CAPSULE_HEADER *)((UINT8 *)CapsuleHeader + CapsuleHeader->HeaderSize);
    ItemOffsetList = (UINT64 *)(FmpCapsuleHeader + 1);
    for (Index = FmpCapsuleHeader->EmbeddedDriverCount; Index < (UINT32)FmpCapsuleHeader->EmbeddedDriverCount + FmpCapsuleHeader->PayloadItemCount; Index++) {
      ImageHeader = (EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER *)((UINT8 *)FmpCapsuleHeader + ItemOffsetList[Index]);
      //
      // Strip off all headers to get Fmp payload image in the capsule file,
      // then compare it with the 'PayloadImage' in FmpDeviceSetImage().
      FmpImageAuth = (EFI_FIRMWARE_IMAGE_AUTHENTICATION *) (ImageHeader + 1);
      if (ImageHeader->Version < EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER_INIT_VERSION) {
        if (ImageHeader->Version == 2) {
          FmpImageAuth = (EFI_FIRMWARE_IMAGE_AUTHENTICATION *)((UINT8*) ImageHeader + OFFSET_OF (EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER, ImageCapsuleSupport));
        } else if (ImageHeader->Version == 1) {
          FmpImageAuth = (EFI_FIRMWARE_IMAGE_AUTHENTICATION *)((UINT8*) ImageHeader + OFFSET_OF (EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER, UpdateHardwareInstance));
        } else {
          ASSERT (FALSE);
        }
      }

      if (((UINTN)FmpImageAuth + sizeof (FmpImageAuth->MonotonicCount) + (UINTN) FmpImageAuth->AuthInfo.Hdr.dwLength) < (UINTN)FmpImageAuth || \
          ((UINTN)FmpImageAuth + sizeof (FmpImageAuth->MonotonicCount) + (UINTN) FmpImageAuth->AuthInfo.Hdr.dwLength) >= (UINTN)FmpImageAuth + ImageHeader->UpdateImageSize) {
        //
        // Pointer overflow. Invalid image.
        //

        UINTN    TempData = 0;
        TempData = FmpImageAuth->AuthInfo.Hdr.dwLength;

        DEBUG ((DEBUG_INFO, "AuthInfo.Hdr.dwLength: %d\n", FmpImageAuth->AuthInfo.Hdr.dwLength));
        DEBUG ((DEBUG_INFO, "TempData: %d\n", TempData));
        DEBUG ((DEBUG_INFO, "ImageHeader->UpdateImageSize: %d\n", ImageHeader->UpdateImageSize));
        continue;
      }

      //
      //
      // Strip off Fmp Image Authenticaion to get Fmp Dependency
      //
      FmpDependency = (UINT8 *) FmpImageAuth + sizeof (FmpImageAuth->MonotonicCount) + FmpImageAuth->AuthInfo.Hdr.dwLength;

      //
      // Strip off Fmp Dependency to get Fmp Payload Header.
      //
      FmpDependencySize = CalculateFmpDependencySizeInNewImage (FmpDependency, (UINT8 *) FmpImageAuth + ImageHeader->UpdateImageSize - FmpDependency);
      FmpPayloadHeader = FmpDependency + FmpDependencySize;

      //
      // Strip off Fmp Payload Header to get Fmp Payload Image
      //
      Status = GetFmpPayloadHeaderSize (FmpPayloadHeader, (UINT8 *) FmpImageAuth + ImageHeader->UpdateImageSize - (UINT8 *)FmpPayloadHeader, &FmpPayloadHeaderSize);
      if (EFI_ERROR (Status)) {
        //
        // Invalid Fmp payload image.
        //
        DEBUG ((DEBUG_INFO, "Invalid Fmp payload image.\n"));
        continue;
      }
      PayloadImageInCapsule = (UINT8 *) FmpPayloadHeader + FmpPayloadHeaderSize;
      PayloadImageSizeInCapsule = ImageHeader->UpdateImageSize - sizeof (FmpImageAuth->MonotonicCount) - FmpImageAuth->AuthInfo.Hdr.dwLength - FmpDependencySize - FmpPayloadHeaderSize;

      //
      // Check size first.
      //
      if (PayloadImageSizeInCapsule == PayloadImageSize) {
        //
        // Compare two payloads to check if they are indentical.
        //
        if (CompareMem (PayloadImageInCapsule, PayloadImage, PayloadImageSize) == 0) {
          DEBUG ((DEBUG_INFO, "Target FMP capsule is detected(%g).\n", &ImageHeader->UpdateImageTypeId));
          return HobPointer.Capsule;
        }
      }
    }

    HobPointer.Raw = GET_NEXT_HOB (HobPointer);
  }

  return NULL;
}

/**
  Search for Windows UX Capsule

  @retval Pointer to the capsule hob found.
          NULL means no such capsules there.

**/
EFI_HOB_UEFI_CAPSULE *
SearchWindowsUxCapsule (
  VOID
  )
{
  EFI_PEI_HOB_POINTERS                          HobPointer;
  EFI_CAPSULE_HEADER                            *CapsuleHeader;

  //
  // Search all capsule images from hob
  //
  HobPointer.Raw = GetHobList ();
  while ((HobPointer.Raw = GetNextHob (EFI_HOB_TYPE_UEFI_CAPSULE, HobPointer.Raw)) != NULL) {
    CapsuleHeader = (EFI_CAPSULE_HEADER *) (UINTN) HobPointer.Capsule->BaseAddress;

    if (CompareGuid (&CapsuleHeader->CapsuleGuid, &gWindowsUxCapsuleGuid)) {
      DEBUG ((DEBUG_INFO, "Windows UX capsule is detected\n"));
      return HobPointer.Capsule;
    }

    HobPointer.Raw = GET_NEXT_HOB (HobPointer);
  }

  return NULL;
}

/**
  Save current FMP Capsule and UX Capsule to external storages.

  @param[in] PayloadImage       Pointer to FMP payload image (FMP image header is stripped off).
  @param[in] PayloadImageSize   The size of PayloadImage.

  @retval  EFI_SUCCESS    Successfully backed up necessary files on external storage.
  @retval  Others         Failed to back up necessary files.

**/
EFI_STATUS
SaveCurrentCapsuleToStorage (
  IN VOID                         *PayloadImage,
  IN UINTN                        PayloadImageSize
  )
{
  EFI_STATUS                            Status;
  EFI_HOB_UEFI_CAPSULE                  *CurrentFmpCapsule;
  EFI_HOB_UEFI_CAPSULE                  *WindowsUxCapsule;
//[-start-201030-IB16810136-modify]//
  UINT8                                 Sha256[SHA256_DIGEST_HASH_SIZE];
//[-end-201030-IB16810136-modify]//

  DEBUG ((DEBUG_INFO, "SaveCurrentCapsuleToStorage - entry\n"));

  if ((PayloadImage == NULL) || (PayloadImageSize == 0)) {
    return EFI_INVALID_PARAMETER;
  }

  Status = InitializeBackupFileSystem ();

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // 1. Delete previous backup files before creating new one
  //
  DeleteBackupCapsules ();

  //
  // 2. Save current Capsule (including Capsule/FMP header) to external storage
  //
  CurrentFmpCapsule = SearchThisFmpCapsule (PayloadImage, PayloadImageSize);
  if (CurrentFmpCapsule == NULL) {
    DEBUG ((DEBUG_ERROR, "Cannot find Capsule image(%g)\n", &gEfiCallerIdGuid));
    return EFI_NOT_FOUND;
  }
//[-start-201112-IB16810138-modify]//
  Status = WriteBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_CAPSULE_BACKUP_FILE_NAME, (UINT8 *) (UINTN) CurrentFmpCapsule->BaseAddress, CurrentFmpCapsule->Length, FALSE);
//[-end-201112-IB16810138-modify]//
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // 2-1. Also keep the messag digest of the saved capsule image in NV variable for integrity check.
  //
//[-start-201030-IB16810136-modify]//
  ZeroMem (Sha256, SHA256_DIGEST_HASH_SIZE);
//[-end-201030-IB16810136-modify]//
  DEBUG ((DEBUG_INFO, "Create Capsule digest:\n"));
  CreateSha256Hash ((UINT8 *) (UINTN) CurrentFmpCapsule->BaseAddress, CurrentFmpCapsule->Length, Sha256);
//[-start-201030-IB16810136-modify]//
  Status = gRT->SetVariable (
                  SYSFW_UPDATE_CAPSULE_DIGEST_VARIABLE_NAME,
                  &gSysFwUpdateProgressGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  SHA256_DIGEST_HASH_SIZE,
                  Sha256
                  );
//[-end-201030-IB16810136-modify]//
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to keep message digest of the saved Capsule image.\n"));
    goto ClearBackup;
  }

  //
  // 3. Save Windows UX Capsule to external storage if there is one.
  //
  WindowsUxCapsule = SearchWindowsUxCapsule ();
  if (WindowsUxCapsule != NULL) {
    DEBUG ((DEBUG_INFO, "Save Windows UX Capsule to external storage.\n"));
//[-start-201112-IB16810138-modify]//
    WriteBackupFile (
      mBackUpFileSystemHandle,
      SYSFW_UPDATE_WINDOWS_UX_CAPSULE_FILE_NAME,
      (UINT8 *) (UINTN) WindowsUxCapsule->BaseAddress,
      WindowsUxCapsule->Length,
      FALSE
      );
//[-end-201112-IB16810138-modify]//
  }

ClearBackup:
  if (EFI_ERROR (Status)) {
    DeleteBackupCapsules ();
  }

  return Status;
}

/**
  Search for ME recovery capsule on storage and return TRUE if present.

  @retval TRUE               ME recovery image is present.
  @retval FALSE              ME recovery image is not present.

**/
BOOLEAN
IsMeRecoveryCapsuleExistOnStorage (
  VOID
  )
{
  EFI_STATUS               Status;

  Status = InitializeBackupFileSystem ();
  if (!EFI_ERROR (Status)) {
    Status = ReadBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME, NULL, NULL);
  }
  return (Status == EFI_SUCCESS);
}

/**
  Save current ME/Monolithic FMP Capsule as ME recovery capsule.

  @param[in] PayloadImage       Pointer to ME/Monolithic FMP payload image (FMP image header is stripped off).
  @param[in] PayloadImageSize   The size of PayloadImage.

  @retval  EFI_SUCCESS    Successfully backed up necessary files on external storage.
  @retval  Others         Failed to back up necessary files.

**/
EFI_STATUS
SaveMeRecoveryCapsule (
  IN VOID                         *PayloadImage,
  IN UINTN                        PayloadImageSize
  )
{
  EFI_STATUS                            Status;
  EFI_HOB_UEFI_CAPSULE                  *CurrentFmpCapsule;

  DEBUG ((DEBUG_INFO, "SaveMeRecoveryCapsule - entry\n"));

  if ((PayloadImage == NULL) || (PayloadImageSize == 0)) {
    return EFI_INVALID_PARAMETER;
  }

  Status = InitializeBackupFileSystem ();
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Find the capsule which has PayloadImage content.
  //
  CurrentFmpCapsule = SearchThisFmpCapsule (PayloadImage, PayloadImageSize);
  if (CurrentFmpCapsule == NULL) {
    DEBUG ((DEBUG_ERROR, "Cannot find ME Capsule image(%g)\n", &gEfiCallerIdGuid));
    return EFI_NOT_FOUND;
  }

  //
  // Replace the any existing old recovery capsule with new capsule.
  //
  Status = DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW);
  if (!EFI_ERROR (Status) || (Status == EFI_NOT_FOUND)) {
//[-start-201112-IB16810138-modify]//
    Status = WriteBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW, (UINT8 *) (UINTN) CurrentFmpCapsule->BaseAddress, CurrentFmpCapsule->Length, FALSE);
//[-end-201112-IB16810138-modify]//
  }

  return Status;
}
//[-start-201112-IB16810138-add]//
/**
  Save current ME/Monolithic Capsule as ME rollback capsule.

  @param[in] PayloadImage       Pointer to ME/Monolithic FMP payload image (FMP image header is stripped off).
  @param[in] PayloadImageSize   The size of PayloadImage.

  @retval  EFI_SUCCESS    Successfully backed up necessary files on external storage.
  @retval  Others         Failed to back up necessary files.

**/
EFI_STATUS
SaveMeCapsuleForRollback (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *Buffer;
  UINTN                                 Length;
  UINT8                                 Sha256[SHA256_DIGEST_HASH_SIZE];


  Buffer           = NULL;
  Length           = 0;


  DEBUG ((DEBUG_INFO, "SaveMeCapsuleForRollback - entry\n"));

  Status = InitializeBackupFileSystem ();
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get capsule as backup ME.
  //
  Status = ReadBackupFile (mBackUpFileSystemHandle, CAPSULE_IMAGE_FULL_PATH, (VOID **) &Buffer, &Length);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot find ME Capsule image.\n"));
    return EFI_NOT_FOUND;
  }

  //
  // Replace the any existing old recovery capsule with new capsule.
  //
  Status = DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW);
  if (!EFI_ERROR (Status) || (Status == EFI_NOT_FOUND)) {
    Status = WriteBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW, (UINT8 *)(UINTN) Buffer, Length, FALSE);
  }

  //
  // Caculate temp digest
  //
  ZeroMem (Sha256, SHA256_DIGEST_HASH_SIZE);
  CreateSha256Hash (Buffer, Length, Sha256);
  Status = SetVariableToSensitiveVariable (
               ME_CAPSULE_DIGEST_TEMP_VARIABLE_NAME,
               &gSysFwUpdateDigiestGuid,
               EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
               SHA256_DIGEST_HASH_SIZE,
               Sha256
               );
 
  //
  // Save windows ux capsule.
  //
  Status = ReadBackupFile (mBackUpFileSystemHandle, UX_IMAGE_FULL_PATH, (VOID **) &Buffer, &Length);
  if (!EFI_ERROR (Status)) {
    Status = WriteBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_WINDOWS_UX_CAPSULE_FILE_NAME, (UINT8 *)(UINTN) Buffer, Length, FALSE);
  }
  
  return Status;
}

EFI_STATUS
GetBackupMeAsCapsule(
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *Buffer;
  UINTN                                 Length;
  UINT8                                 CapsuleSha256[SHA256_DIGEST_HASH_SIZE];
  UINT8                                 BackupSha256[SHA256_DIGEST_HASH_SIZE];
  UINTN                                 VariableSize;

  DEBUG ((DEBUG_INFO, "GetBackupMeAsCapsule - entry\n"));

  Status = InitializeBackupFileSystem ();
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = ReadBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW, (VOID **) &Buffer, &Length);
  if (!EFI_ERROR (Status)) {
    //
    // Delete broken capsule.
    //
    DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW);
  }
  //
  // Get backup ME.
  //
  Status = ReadBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME, (VOID **) &Buffer, &Length);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot find Backup ME Capsule image.\n"));
    return EFI_NOT_FOUND;
  }
  
  //
  // Verify capsule
  // 
  ZeroMem (CapsuleSha256, SHA256_DIGEST_HASH_SIZE);
  CreateSha256Hash (Buffer, Length, CapsuleSha256);

  VariableSize = SHA256_DIGEST_HASH_SIZE;
  ZeroMem (BackupSha256, SHA256_DIGEST_SIZE);
  Status = gRT->GetVariable (
                  ME_CAPSULE_DIGEST_VARIABLE_NAME,
                  &gSysFwUpdateDigiestGuid,
                  NULL,
                  &VariableSize,
                  &BackupSha256
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Can't find Me backup digest.\n"));
    return Status;
  }

  if (CompareMem (CapsuleSha256, BackupSha256, SHA256_DIGEST_HASH_SIZE) == 0) {
    Status = WriteBackupFile (mBackUpFileSystemHandle, CAPSULE_IMAGE_NAME, (UINT8 *)(UINTN) Buffer, Length, TRUE);
  } else {
    DEBUG ((DEBUG_ERROR, "Me backup image loaded from ESP is corrupted.\n"));
    return EFI_DEVICE_ERROR;
  }

  //
  // Get backup ux 
  //
  Status = ReadBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_WINDOWS_UX_CAPSULE_FILE_NAME, (VOID **) &Buffer, &Length);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot find Backup ME Capsule image.\n"));
    return EFI_NOT_FOUND;
  }
  Status = WriteBackupFile (mBackUpFileSystemHandle, UX_IMAGE_NAME, (UINT8 *)(UINTN) Buffer, Length, TRUE);


  return Status;
}
//[-end-201112-IB16810138-add]//


/**
  Replace MeRecovCap with MeRecovCapNew upon successful capsule update.


  @retval  EFI_SUCCESS    Successfully old recovery capsule with new recovery capsule.
  @retval  Others         Failed to sync old/new recovery capsules.

**/
EFI_STATUS
SyncMeRecoveryCapsules (
  VOID
  )
{
  EFI_STATUS                     Status;
  UINT8                          *Buffer;
  UINTN                          Length;
//[-start-201112-IB16810138-add]//
  UINT8                          Sha256[SHA256_DIGEST_HASH_SIZE];
  UINTN                          VariableSize;
//[-end-201112-IB16810138-add]//
  Status           = EFI_SUCCESS;
  Buffer           = NULL;
  Length           = 0;

  DEBUG ((DEBUG_INFO, "SyncMeRecoveryCapsules - entry\n"));

  Status = InitializeBackupFileSystem ();
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = ReadBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW, (VOID **) &Buffer, &Length);
  if (!EFI_ERROR (Status)) {
    //
    // A new ME Recovery capsule exists on storage. Replace old recovery capsule with new capsule.
    //
    Status = DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME);
    if (!EFI_ERROR (Status) || (Status == EFI_NOT_FOUND)) {
//[-start-201112-IB16810138-modify]//
      Status = WriteBackupFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME, (UINT8 *) (UINTN) Buffer, Length, FALSE);
      if(EFI_ERROR (Status)){
        DEBUG ((DEBUG_INFO, "Write ME backup fail, Status %r\n", Status));
        goto Exit;
      }
      
      VariableSize = SHA256_DIGEST_HASH_SIZE;
      ZeroMem (Sha256, SHA256_DIGEST_SIZE);
      Status = gRT->GetVariable (
                  ME_CAPSULE_DIGEST_TEMP_VARIABLE_NAME,
                  &gSysFwUpdateDigiestGuid,
                  NULL,
                  &VariableSize,
                  &Sha256
                  );

      if(EFI_ERROR (Status)){
        DEBUG ((DEBUG_INFO, "Get me digest fail, Status %r\n", Status));
        goto Exit;
      }

      Status = SetVariableToSensitiveVariable (
                  ME_CAPSULE_DIGEST_VARIABLE_NAME,
                  &gSysFwUpdateDigiestGuid,
                  EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                  SHA256_DIGEST_HASH_SIZE,
                  Sha256
                  );

      if(EFI_ERROR (Status)){
        DEBUG ((DEBUG_INFO, "Write me digest fail, Status %r\n", Status));
        goto Exit;
      }

      DeleteFile (mBackUpFileSystemHandle, SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW);
      SetVariableToSensitiveVariable (
               ME_CAPSULE_DIGEST_TEMP_VARIABLE_NAME,
               &gSysFwUpdateProgressGuid,
               0,
               0,
               NULL
               );

    }
    FreePool (Buffer);
  }

Exit:
//[-end-201112-IB16810138-modify]//
  return Status;
}

/**
  Check platform capability to support Fault tolerance based system firmware update.

  @retval TRUE  Current platform is capable of supporting Fault tolerance based system firmware update.
  @retval FALSE Current platform is incapable of supporting Fault tolerance based system firmware update.

**/
BOOLEAN
IsBiosFaultTolerantUpdateSupported (
  VOID
  )
{
//  EFI_STATUS                Status;
//  PCH_SPI_PROTOCOL          *SpiProtocol = NULL;
//  UINT8                     PchStrap;

  DEBUG ((DEBUG_INFO, "Check PlatformCheckFtSupported.\n"));

/*  if ((FixedPcdGet32 (PcdFlashFvReservedSize) < FixedPcdGet32 (PcdFlashFvRecoverySize)) ||
      (FixedPcdGet32 (PcdFlashFvRecoverySize) != SIZE_128KB)) {
    DEBUG ((DEBUG_ERROR, "PcdFlashFvReservedSize or PcdFlashFvRecoverySize does not meet fault tolerant update requirement.\n"));
    return FALSE;
  }
*/
//
// @TODO: Determine if we need an API from SI or not.
//
//  Status = gBS->LocateProtocol (&gPchSpiProtocolGuid, NULL, (VOID **) &SpiProtocol);
//  ASSERT_EFI_ERROR (Status);
//  if (EFI_ERROR (Status)) {
//    DEBUG ((DEBUG_ERROR, "Failed to locate PCH SPI protocol\n"));
//    return FALSE;
//  }
//  Status = SpiProtocol->ReadPchSoftStrap (SpiProtocol, R_PCH_SPI_STRP_DSCR_63, sizeof (PchStrap), &PchStrap);
//  if (EFI_ERROR (Status)) {
//    DEBUG ((DEBUG_ERROR, "Failed to read PCH soft strap\n"));
//    return FALSE;
//  }
//
//  DEBUG ((DEBUG_INFO, "PCH Rescriptor Record 63: 0x%x\n", PchStrap));
//  if (((PchStrap & B_PCH_SPI_STRP_DSCR_63_TSBS) >> N_PCH_SPI_STRP_DSCR_63_TSBS) != V_PCH_SPI_STRP_DSCR_63_TSBS_128K) {
//    DEBUG ((DEBUG_ERROR, "Top Swap block size does not meet platform requirement\n"));
//    return FALSE;
//  }

  return TRUE;
}

/**
  Check if system firmware update got interrupted last time.

  @param[in,out] PreviousUpdateProgress      Pointers to the progress where updating process got
                                             interrupted last time.

  @retval TRUE   Previous update process got interrupted.
  @retval FALSE  There is no indication that update was in progress.

**/
BOOLEAN
IsPreviousUpdateUnfinished (
  IN OUT SYSTEM_FIRMWARE_UPDATE_PROGRESS       *PreviousUpdateProgress
  )
{
  if (PreviousUpdateProgress != NULL) {
    PreviousUpdateProgress->Component = mPreviousUpdateProgress.Component;
    PreviousUpdateProgress->Progress  = mPreviousUpdateProgress.Progress;
  }
#if (FixedPcdGetBool(PcdResiliencyEnable) == 0)
  //
  // Add check for handle res bios update to bkc bios case.
  //
  if (mPreviousUpdateProgress.Component == UpdatingResiliency) {
    ClearUpdateProgress ();
    return FALSE;
  }
#endif

  return (mPreviousUpdateProgress.Component != NotStarted) ? TRUE : FALSE;
}

/**
  Get the current update progress from variable

  @param[out] CurrentUpdateProgress       Pointers to the current progress from NV storage

  @retval EFI_SUCCESS             Update progress is get from to NV storage successfully or it is not set.
  @retval EFI_INVALID_PARAMETER   CurrentUpdateProgress is NULL.

**/
EFI_STATUS
GetCurrentUpdateProgress (
  IN OUT SYSTEM_FIRMWARE_UPDATE_PROGRESS     *CurrentUpdateProgress
  )
{
  EFI_STATUS                            Status;
  UINTN                                 VariableSize;

  if (CurrentUpdateProgress == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Check if system is continuing from an interrupted update.
  //
  ZeroMem (CurrentUpdateProgress, sizeof (SYSTEM_FIRMWARE_UPDATE_PROGRESS));
  VariableSize = sizeof (SYSTEM_FIRMWARE_UPDATE_PROGRESS);

  Status = gRT->GetVariable (
                  SYSFW_UPDATE_PROGRESS_VARIABLE_NAME,
                  &gSysFwUpdateProgressGuid,
                  NULL,
                  &VariableSize,
                  CurrentUpdateProgress
                  );

  if (Status == EFI_NOT_FOUND) {
    Status = EFI_SUCCESS;
  }

  DEBUG ((DEBUG_INFO, "Current UpdateProgress component = 0x%x.\n", CurrentUpdateProgress->Component));
  DEBUG ((DEBUG_INFO, "Current UpdateProgress progress  = 0x%x.\n", CurrentUpdateProgress->Progress));

  return Status;
}

/**
  Record the current update progress

  @param[in] UpdatingComponent      The FW component being updated now.
  @param[in] UpdatingProgress       The updating stage associated to UpdatingComponent

  @retval EFI_SUCCESS   Update progress is recorded to NV storage successfully.
  @retval Others        Update progress is not recorded.

**/
EFI_STATUS
SetUpdateProgress (
  IN SYSTEM_FIRMWARE_COMPONENT  UpdatingComponent,
  IN UINT32                     UpdatingProgress
  )
{
  EFI_STATUS                            Status;
  SYSTEM_FIRMWARE_UPDATE_PROGRESS       UpdateProgress;

  UpdateProgress.Component = UpdatingComponent;
  UpdateProgress.Progress  = UpdatingProgress;


  Status = gRT->SetVariable (
                  SYSFW_UPDATE_PROGRESS_VARIABLE_NAME,
                  &gSysFwUpdateProgressGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  sizeof (SYSTEM_FIRMWARE_UPDATE_PROGRESS),
                  &UpdateProgress
                  );

  return Status;
}

/**
  Clear update progress in NV storage. This indicates that no FW update process is happening.

**/
VOID
ClearUpdateProgress (
  VOID
  )
{
  EFI_STATUS   Status;
//[-start-201112-IB16810138-modify]//
  SetVariableToSensitiveVariable (
    FW_UPDATEINFO_VARIABLE_NAME,
    &gSysFwUpdateProgressGuid,
    0,
    0,
    NULL
    );
 
//[-start-201030-IB16810136-add]//
  gRT->SetVariable (
         L"FmpCapsuleInfo",
         &gFmpCapsuleInfoGuid,
         0,
         0,
         NULL
         );
  gRT->SetVariable (
         SYSBIOS_NEW_CAPSULE_DIGEST_VARIABLE_NAMEN,
         &gSysFwUpdateDigiestGuid,
         0,
         0,
         NULL
         );
//[-end-201030-IB16810136-add]//
//[-end-201112-IB16810138-modify]//
  //
  // Delete progress variable
  //
  Status = gRT->SetVariable (
                  SYSFW_UPDATE_PROGRESS_VARIABLE_NAME,
                  &gSysFwUpdateProgressGuid,
                  0,
                  0,
                  NULL
                  );
  if (EFI_ERROR (Status) && (Status != EFI_NOT_FOUND)) {
    ASSERT_EFI_ERROR (Status);
  }

}

/**
  The constructor function of SeamlessRecoverySupportLib.

  @param[in]  ImageHandle   The firmware allocated handle for the EFI image.
  @param[in]  SystemTable   A pointer to the EFI System Table.

  @retval EFI_SUCCESS   The constructor successfully .
**/
EFI_STATUS
EFIAPI
SeamlessRecoverySupportLibConstructor (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS     Status;
  UINTN          VariableSize;

  //
  // Check if system is continuing from an interrupted update.
  //
  ZeroMem (&mPreviousUpdateProgress, sizeof (SYSTEM_FIRMWARE_UPDATE_PROGRESS));
  VariableSize = sizeof (SYSTEM_FIRMWARE_UPDATE_PROGRESS);

  Status = gRT->GetVariable (
                  SYSFW_UPDATE_PROGRESS_VARIABLE_NAME,
                  &gSysFwUpdateProgressGuid,
                  NULL,
                  &VariableSize,
                  &mPreviousUpdateProgress
                  );

  if ((Status != EFI_NOT_FOUND) && (Status != EFI_SUCCESS)) {
    DEBUG ((DEBUG_INFO, "Get UpdateProgress variable = %r.\n", Status));
    ASSERT_EFI_ERROR (Status);
  }

  DEBUG ((DEBUG_INFO, "Last boot UpdateProgress component = 0x%x.\n", mPreviousUpdateProgress.Component));
  DEBUG ((DEBUG_INFO, "Last boot UpdateProgress progress  = 0x%x.\n", mPreviousUpdateProgress.Progress));

  return EFI_SUCCESS;
}
