/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Library instance to lock Seamless Recovery relative variables

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#include <Library/SeamlessRecoverySupportLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Protocol/VariableLock.h>
#include <Guid/BootStateCapsule.h>
#include <BootStateLib.h>
//[-start-201112-IB16810138-add]//
#include <Library/PcdLib.h>
//[-end-201112-IB16810138-add]//


/**
  The constructor function of SeamlessRecoveryVarLockLib.

  @param[in]  ImageHandle   The firmware allocated handle for the EFI image.
  @param[in]  SystemTable   A pointer to the EFI System Table.

  @retval EFI_SUCCESS   The constructor successfully .
**/
EFI_STATUS
EFIAPI
SeamlessRecoveryVarLockLibConstructor (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                                      Status;
  EDKII_VARIABLE_LOCK_PROTOCOL                    *VariableLock;
//[-start-201112-IB16810138-remove]//
//  SetBootStateAfterCapsule(FALSE);
//[-end-201112-IB16810138-remove]//
  //
  // Lock variables related to update progress and backup file.
  //
  Status = gBS->LocateProtocol (&gEdkiiVariableLockProtocolGuid, NULL, (VOID **)&VariableLock);
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status)) {
//[-start-201112-IB16810138-remove]//
//    Status = VariableLock->RequestToLock (VariableLock, BOOT_STATE_AFTER_CAPSULE_VARIABLE_NAME, &gBootStateAfterCapsuleGuid);
//
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_UPDATE_PROGRESS_VARIABLE_NAME, &gSysFwUpdateProgressGuid);
//    ASSERT_EFI_ERROR (Status);
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_UPDATE_CAPSULE_DIGEST_VARIABLE_NAME, &gSysFwUpdateProgressGuid);
//    ASSERT_EFI_ERROR (Status);
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_BIOS_DEFERRED_SVN_VARIABLE_NAME, &gSysFwBiosDeferredSVNGuid);
//    ASSERT_EFI_ERROR (Status);
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_BIOS_LSV_SVN_VARIABLE_NAME, &gSysFwBiosLsvSvnGuid);
//    ASSERT_EFI_ERROR (Status);
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_EC_LSV_SVN_VARIABLE_NAME, &gSysFwEcLsvSvnGuid);
//    ASSERT_EFI_ERROR (Status);
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_ME_LSV_SVN_VARIABLE_NAME, &gSysFwMeLsvSvnGuid);
//    ASSERT_EFI_ERROR (Status);
//    Status = VariableLock->RequestToLock (VariableLock, SYSFW_ISHPDT_LSV_SVN_VARIABLE_NAME , &gSysFwIshPdtLsvSvnGuid);
//    ASSERT_EFI_ERROR (Status);
//[-end-201112-IB16810138-remove]//
//[-start-201112-IB16810138-add]//
    if (PcdGetBool(PcdMeResiliencyEnable)){
      Status = VariableLock->RequestToLock (VariableLock, SYSFW_ME_LSV_SVN_VARIABLE_NAME, &gSysFwMeLsvSvnGuid);
      ASSERT_EFI_ERROR (Status);
      Status = VariableLock->RequestToLock (VariableLock, FW_UPDATEINFO_VARIABLE_NAME, &gSysFwUpdateProgressGuid);
      ASSERT_EFI_ERROR (Status);
      Status = VariableLock->RequestToLock (VariableLock, ME_CAPSULE_DIGEST_VARIABLE_NAME, &gSysFwUpdateDigiestGuid);
      ASSERT_EFI_ERROR (Status);
      Status = VariableLock->RequestToLock (VariableLock, ME_CAPSULE_DIGEST_TEMP_VARIABLE_NAME, &gSysFwUpdateProgressGuid);
      ASSERT_EFI_ERROR (Status);
    }
//[-end-201112-IB16810138-add]//
  }

  return EFI_SUCCESS;
}
