/** @file
  Component Update Library
  Called by each FmpDeviceLib instance to do capsule update.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <Library/ComponentUpdateLib.h>

#include <Library/DxeMeLib.h>
#include <Library/TimerLib.h>

#include <Guid/ZeroGuid.h>

//
// Used in GbE update function to update checksum word:
//   The Checksum word (3Fh) should be calculated such that after adding all the words (00h-3Fh),
//   including the Checksum word itself, the sum should be BABAh.
//
#define GBE_SW_OWNED_EEPROM_CHECKSUM_WORD_OFFSET     0x3F
#define GBE_SW_OWNED_EEPROM_WORDS_SUM                0xBABA

//
// Used in ME update function
//
#define FW_UPDATE_DISABLED                     0
#define FW_UPDATE_ENABLED                      1
#define ME_FWSTS2_TIMEOUT_COUNTER              150

//
// Use to display progress of sending ME FW
//
EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  mMeDisplayProgress = NULL;
UINTN                                          mMeSendProgressStartPercentage = 0;
UINTN                                          mMeSendProgressEndPercentage = 100;


/**
  Display the ME update progress.

  @param BytesSent               Bytes already sent to ME
  @param BytestobeSent           Bytes to be sent to ME

**/
VOID
DisplaySendMeFwStatus (
  UINT32                                BytesSent,
  UINT32                                BytestobeSent
  )
{
  UINTN  Percentage;

  Percentage = mMeSendProgressStartPercentage + (BytesSent * (mMeSendProgressEndPercentage - mMeSendProgressStartPercentage)) / BytestobeSent;

  if (mMeDisplayProgress != NULL) {
    mMeDisplayProgress (Percentage);
  }
}

/**
  Update ME Firmware by HECI interface.
  Use the ME FW Update API (FWUpdateLib.lib) provided by ME team to perform the update.

  @param[in] WriteReq                Request information for update flash.
  @param[in] Progress                A function used report the progress of the
                                     firmware update.  This is an optional parameter
                                     that may be NULL.
  @param[in] StartPercentage         The start completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.
  @param[in] EndPercentage           The end completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.

  @retval EFI_SUCCESS                ME FW is updated successfully.
  @retval Others                     The update operation fails.

**/
EFI_STATUS
UpdateMeByHeci (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  )
{
  EFI_STATUS                           Status;
  UINT32                               FwUpdateApiStatus;
  UINT16                               EnabledState;
  BOOLEAN                              InProgress;
  UINT32                               CurrentPercent;
  UINT32                               FwUpdateStatus;
  UINT32                               NeededResetType;
  UINT32                               UpdateTimer;
  UINT32                               PreviousPercent;
  _UUID                                Uuid;

  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: ME Image Length %d(%xh)\n", WriteReq->DataSize, WriteReq->DataSize));

  EnabledState    = 0;
  InProgress      = FALSE;
  CurrentPercent  = 0;
  FwUpdateStatus  = 0;
  NeededResetType = 0;
  UpdateTimer     = 0;
  PreviousPercent = 0;

  //
  // Check FWU enabled state
  //
  FwUpdateApiStatus = FwuEnabledState (&EnabledState);
  if (FwUpdateApiStatus != EFI_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "UpdateMeByHeci: FwuEnabledState failed: %r.\n", FwUpdateApiStatus));
    return EFI_DEVICE_ERROR;
  }

  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: FwuEnabledState is 0x%x.\n", EnabledState));

  switch (EnabledState) {
    case FW_UPDATE_DISABLED:
      DEBUG ((DEBUG_ERROR, "UpdateMeByHeci: FWUpdate is disabled.\n"));
      return EFI_DEVICE_ERROR;

    case FW_UPDATE_ENABLED:
      break;

    default:
      break;
  }

  if (Progress != NULL) {
    mMeDisplayProgress = Progress;
    mMeSendProgressStartPercentage = StartPercentage;
    //
    // Assign 1/5 of progress bar to SendProgress
    //
    mMeSendProgressEndPercentage = StartPercentage + ((EndPercentage - StartPercentage) / 5);
  }

  //
  // Starting FWU full update. Send image to FW Update Client
  //
  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: Executing Full FW Update.\n"));

  Status = FwuOemId (&Uuid);
  DEBUG ((DEBUG_INFO, "Firmware OEM ID: %g [%r]\r\n", &Uuid, Status));
  if (!EFI_ERROR (Status) && !CompareGuid ((EFI_GUID *) &Uuid, &gZeroGuid)) {
    FwUpdateApiStatus = FwuFullUpdateFromBuffer (WriteReq->Data, (UINT32) WriteReq->DataSize, &Uuid, &DisplaySendMeFwStatus);
  } else {
    FwUpdateApiStatus = FwuFullUpdateFromBuffer (WriteReq->Data, (UINT32) WriteReq->DataSize, NULL, &DisplaySendMeFwStatus);
  }

  if (FwUpdateApiStatus != EFI_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "UpdateMeByHeci: FwuFullUpdateFromBuffer failed: %r.\n", FwUpdateApiStatus));
    return EFI_DEVICE_ERROR;
  }

  //
  // Image was sent to FW Update client
  // Poll the FW Update progress until finished
  //
  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: Waiting for FW Update progress to be finished.\n"));
  do {
    FwUpdateApiStatus = FwuCheckUpdateProgress (
                          &InProgress,
                          &CurrentPercent,
                          &FwUpdateStatus,
                          &NeededResetType
                          );
    if (FwUpdateApiStatus != EFI_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "UpdateMeByHeci: FwuCheckUpdateProgress failed: %r.\n", FwUpdateApiStatus));
      break;
    }

    if (!InProgress) {
      DEBUG ((DEBUG_INFO, "UpdateMeByHeci: FWU Update finished successfully: %r.\n", FwUpdateApiStatus));
      if (Progress != NULL) {
        Progress (EndPercentage);
      }
      break;
    }

    // Update is in progress
    DEBUG ((DEBUG_INFO, "UpdateMeByHeci: Current percent: %d\n", CurrentPercent));
    gBS->Stall (250000); // wait 250 milliseconds before polling again

    // If 30 seconds passed
    if (UpdateTimer >= 30000) {
      // If percent didn't change in 30 seconds
      if (CurrentPercent == PreviousPercent) {
        DEBUG ((DEBUG_ERROR, "UpdateMeByHeci: FwuCheckUpdateProgress timeout.\n"));
        Status = EFI_TIMEOUT;
        break;
      }
      // Percent changed
      PreviousPercent = CurrentPercent;
      UpdateTimer = 0;
    } else {
      // Increase timer
      UpdateTimer += 250;
    }

    if (Progress != NULL) {
      Progress (mMeSendProgressEndPercentage + (CurrentPercent * (EndPercentage - mMeSendProgressEndPercentage)) / 100);
    }
  } while (TRUE);


  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: FwUpdateApiStatus: 0x%x (%d).\n", FwUpdateApiStatus, FwUpdateApiStatus));
  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: FwUpdateStatus   : 0x%x (%d).\n", FwUpdateStatus, FwUpdateStatus));
  DEBUG ((DEBUG_INFO, "UpdateMeByHeci: NeededResetType  : 0x%x.\n", NeededResetType));
  if ((FwUpdateApiStatus != EFI_SUCCESS) || (FwUpdateStatus != EFI_SUCCESS)) {
    Status = EFI_DEVICE_ERROR;
  } else {
    Status = EFI_SUCCESS;
  }

  return Status;
}

/**
  Perform ME firmware update.

  @param[in] WriteReq                Request information for update flash.
  @param[in] Progress                A function used report the progress of the
                                     firmware update.  This is an optional parameter
                                     that may be NULL.
  @param[in] StartPercentage         The start completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.
  @param[in] EndPercentage           The end completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.

  @retval EFI_SUCCESS                ME FW is updated successfully.
  @retval Others                     The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateMeFirmware (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  )
{
  EFI_STATUS                      Status;

  DEBUG ((DEBUG_INFO, "UpdateMeFirmware: MeBinSize = %x\n", WriteReq->DataSize));

  DEBUG ((DEBUG_INFO, "UpdateMeFirmware: Update ME firmware with HECI interface\n"));
  //
  // Use the HECI method to perform Me update.
  //
  Status = UpdateMeByHeci (
             WriteReq,
             Progress,
             StartPercentage,
             EndPercentage
             );

  return Status;
}

/**
  Update BIOS region.

  @param[in] WriteReq                    Request information for update flash.
  @param[in] Progress                    A function used report the progress of the
                                         firmware update.  This is an optional parameter
                                         that may be NULL.
  @param[in] StartPercentage             The start completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.
  @param[in] EndPercentage               The end completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.

  @retval EFI_SUCCESS                    The flash region is updated successfully.
  @retval EFI_INVALID_PARAMETER          The input buffer is invalid.
  @retval Others                         The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateBiosFirmware (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  )
{
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO, "UpdateBiosFirmware - 0x%x - 0x%x\n", (UINTN)WriteReq->FlashAddress, WriteReq->DataSize));
  if (WriteReq->BgupImage != NULL && WriteReq->BgupImageSize != 0) {
    if (Progress != NULL) {
      Progress (StartPercentage);
    }
  }

  Status = FlashUpdate(WriteReq, Progress, StartPercentage, EndPercentage);

  if (WriteReq->BgupImage != NULL && WriteReq->BgupImageSize != 0) {
    if (Progress != NULL) {
      Progress (EndPercentage);
    }
  }

  return Status;

}

/**
  Update EC region.

  @param[in] WriteReq                    Request information for update flash.
  @param[in] Progress                    A function used report the progress of the
                                         firmware update.  This is an optional parameter
                                         that may be NULL.
  @param[in] StartPercentage             The start completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.
  @param[in] EndPercentage               The end completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.

  @retval EFI_SUCCESS                    The flash region is updated successfully.
  @retval EFI_INVALID_PARAMETER          The input buffer is invalid.
  @retval Others                         The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateEcFirmware (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  )
{
  DEBUG ((DEBUG_INFO, "UpdateEcFirmware: Direct SPI.\n"));
  return FlashUpdate (WriteReq, Progress, StartPercentage, EndPercentage);
}


/**
  Perform ISH PDT configuration update

  @param[in] WriteReq                Request information for update flash.
  @param[in] Progress                A function used report the progress of the
                                     firmware update.  This is an optional parameter
                                     that may be NULL.
  @param[in] StartPercentage         The start completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.
  @param[in] EndPercentage           The end completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.

  @retval EFI_SUCCESS                PDT is updated successfully.
  @retval Others                     The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateIshPdt (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  )
{
  EFI_STATUS              Status;
  UINT8                   *PaddedPayload;
  UINTN                   PaddedPayloadSize;
  UINT32                  FwUpdateApiStatus;

  DEBUG ((DEBUG_INFO, "UpdateIshPdt: PdtBinSize = 0x%x\n", WriteReq->DataSize));
  ASSERT (WriteReq->DataSize <= SIZE_4KB);

  PaddedPayload = NULL;

  //
  // Payload size must be word aligned and padded with zero byte if size is not even.
  //
  PaddedPayloadSize = ALIGN_VALUE (WriteReq->DataSize, 2);
  DEBUG ((DEBUG_INFO, "UpdateIshPdt: Padded PdtBinSize = 0x%x\n", PaddedPayloadSize));

  PaddedPayload = AllocateZeroPool (PaddedPayloadSize);
  if (PaddedPayload == NULL) {
    DEBUG ((DEBUG_ERROR, "UpdateIshPdt: Ran out of memory resource.\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (PaddedPayload, WriteReq->Data, WriteReq->DataSize);

  if (Progress != NULL) {
    Progress (StartPercentage);
  }

  FwUpdateApiStatus = FwuSetIshConfig (PaddedPayload, (UINT32) PaddedPayloadSize);
  DEBUG ((DEBUG_INFO, "UpdateIshPdt FwuStatus = 0x%x\n", FwUpdateApiStatus));

  if (FwUpdateApiStatus != EFI_SUCCESS) {
    Status = EFI_DEVICE_ERROR;
  } else {
    Status = EFI_SUCCESS;
  }

  if (Progress != NULL) {
    Progress (EndPercentage);
  }

  FreePool (PaddedPayload);

  return Status;
}

/**
  The constructor function.

  @param[in]  ImageHandle   The firmware allocated handle for the EFI image.
  @param[in]  SystemTable   A pointer to the EFI System Table.

  @retval EFI_SUCCESS   The constructor successfully .
**/
EFI_STATUS
EFIAPI
ComponentUpdateLibConstructor (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  return EFI_SUCCESS;
}
