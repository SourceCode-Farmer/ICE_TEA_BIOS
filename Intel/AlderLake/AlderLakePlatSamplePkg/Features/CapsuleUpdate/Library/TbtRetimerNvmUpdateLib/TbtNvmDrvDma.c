/** @file
  Implements the TBT DMA

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <Protocol/PciIo.h>
#include <Library/UefiBootServicesTableLib.h>
#include "TbtNvmDrvDma.h"


#define REG_TX_RING_BASE            0x00000
#define REG_RX_RING_BASE            0x08000
#define REG_RING_STEP               16
#define REG_RING_PHYS_LO_OFFSET     0
#define REG_RING_PHYS_HI_OFFSET     4
#define REG_RING_CONS_PROD_OFFSET   8  /* cons - RO, prod - RW */
#define REG_RING_CONS_SHIFT         0
#define REG_RING_CONS_MASK          MASK(15, REG_RING_CONS_SHIFT)
#define REG_RING_PROD_SHIFT         16
#define REG_RING_PROD_MASK          MASK(31, REG_RING_PROD_SHIFT)
#define REG_RING_SIZE_OFFSET        12
#define REG_RING_SIZE_SHIFT         0
#define REG_RING_SIZE_MASK          MASK(15, REG_RING_SIZE_SHIFT)
#define REG_RING_BUF_SIZE_SHIFT     16
#define REG_RING_BUF_SIZE_MASK      MASK(27, REG_RING_BUF_SIZE_SHIFT)

#define REG_TX_OPTIONS_BASE             0x19800
#define REG_RX_OPTIONS_BASE             0x29800
#define REG_RX_OPTS_TX_E2E_HOP_ID_SHIFT 12
#define REG_RX_OPTS_TX_E2E_HOP_ID_MASK  MASK(22, REG_RX_OPTS_TX_E2E_HOP_ID_SHIFT)
#define REG_RX_OPTS_MASK_OFFSET         4
#define REG_RX_OPTS_MASK_EOF_SHIFT      0
#define REG_RX_OPTS_MASK_EOF_MASK       MASK(15, REG_RX_OPTS_MASK_EOF_SHIFT)
#define REG_RX_OPTS_MASK_SOF_SHIFT      16
#define REG_RX_OPTS_MASK_SOF_MASK       MASK(31, REG_RX_OPTS_MASK_SOF_SHIFT)
#define REG_OPTS_STEP                   32
#define REG_OPTS_E2E_EN                 BIT(28)
#define REG_OPTS_RAW                    BIT(30)
#define REG_OPTS_VALID                  BIT(31)

#define DESC_ATTR_LEN_SHIFT         0
#define DESC_ATTR_LEN_MASK          MASK(11, DESC_ATTR_LEN_SHIFT)
#define DESC_ATTR_EOF_SHIFT         12
#define DESC_ATTR_EOF_MASK          MASK(15, DESC_ATTR_EOF_SHIFT)
#define DESC_ATTR_SOF_SHIFT         16
#define DESC_ATTR_SOF_MASK          MASK(19, DESC_ATTR_SOF_SHIFT)
#define DESC_ATTR_TX_ISOCH_DMA_EN   BIT(20)  /* TX */
#define DESC_ATTR_RX_CRC_ERR        BIT(20)  /* RX after use */
#define DESC_ATTR_DESC_DONE         BIT(21)
#define DESC_ATTR_REQ_STS           BIT(22)  /* TX and RX before use */
#define DESC_ATTR_RX_BUF_OVRN_ERR   BIT(22)  /* RX after use */
#define DESC_ATTR_INT_EN            BIT(23)
#define DESC_ATTR_OFFSET_SHIFT      24
#define DESC_ATTR_OFFSET_MASK       MASK(31, DESC_ATTR_OFFSET_SHIFT)

#define TBT_TX_RING_FULL(prod, cons, size) ((((prod) + 1) % (size)) == (cons))
#define TBT_TX_RING_EMPTY(prod, cons) ((prod) == (cons))
#define TBT_RX_RING_FULL(prod, cons) ((prod) == (cons))
#define TBT_RX_RING_EMPTY(prod, cons, size) ((((cons) + 1) % (size)) == (prod))


#define TBT_TIME_BETWEEN_RX_RING_POLL_US 50         // 50us
#define TBT_TOTAL_TIME_TO_WAIT_FOR_RX_US 10000000   // 10s
#define TBT_TOTAL_ACCESSES_WHEN_WAIT_TO_RX  TBT_TOTAL_TIME_TO_WAIT_FOR_RX_US / TBT_TIME_BETWEEN_RX_RING_POLL_US
#define MAX_PACKETS_TO_RX_WHILE_WAIT_FOR_RESP 1000

/**
  Get TX data buffer, which is pointed by the producer index.

  @param[in]  This    This Pointer to the TBT_DMA instance on which the function is activated
  @param[out] ProdPtr A Pointer to producer index in TX ring

  @retval Pointer to the data buffer

**/
STATIC
VOID*
TbtNvmDrvGetTxBuf (
  IN TBT_DMA *This,
  OUT UINT16 *ProdPtr
  )
{
  UINT32 ProdConsReg;
  UINT32 Prod;
  UINT32 Cons;

  ProdConsReg = This->ReadMmio(This, REG_TX_RING_BASE + REG_RING_CONS_PROD_OFFSET);
  Prod = (ProdConsReg & REG_RING_PROD_MASK) >> REG_RING_PROD_SHIFT;
  Cons = (ProdConsReg & REG_RING_CONS_MASK) >> REG_RING_CONS_SHIFT;
  if (Prod >= FW_RING_NUM_TX_BUFS) {
    DEBUG ((DEBUG_ERROR, "TbtDrvGetTxBuf: DMA is not functional, producer %u out of range\n", Prod));
    return NULL;
  }
  if (TBT_TX_RING_FULL(Prod, Cons, FW_RING_NUM_TX_BUFS)) {
    DEBUG ((DEBUG_ERROR, "TbtDrvGetTxBuf: DMA is not functional, TX ring is full\n"));
    return NULL;
  }
  *ProdPtr = (UINT16)Prod;
  return This->Impl->pSharedMem->txBuf[Prod];
}

/**
  Send TX frame
  Operation:
    Increment producer index, which will trigger the DMA to fetch first the
    descriptor and then the data.

**/
STATIC
TBT_STATUS
TbtNvmDrvInitiateTx (
  IN TBT_DMA        *This,
  IN PDF_VALUE      Pdf,
  IN UINT16         MsgLen,
  IN UINT16         Producer
  )
{
  UINT32 ProdCons;
  UINT32 NewAttributes;

  if (Producer < FW_RING_NUM_TX_BUFS) {
    DEBUG ((DEBUG_VERBOSE, "TbtNvmDrvInitiateTx: Sending packet with pdf=0x%x, length=0x%x, prod=0x%x\n", Pdf, MsgLen, Producer));
    NewAttributes = MsgLen << DESC_ATTR_LEN_SHIFT;
    NewAttributes |= Pdf << DESC_ATTR_EOF_SHIFT;
    NewAttributes |= DESC_ATTR_REQ_STS;
    This->Impl->pSharedMem->txBufDesc[Producer].attributes = NewAttributes;
    ProdCons = ((Producer + 1) % FW_RING_NUM_TX_BUFS) << REG_RING_PROD_SHIFT;
    This->WriteMmio (This, REG_TX_RING_BASE + REG_RING_CONS_PROD_OFFSET, ProdCons);
  } else {
    DEBUG ((DEBUG_ERROR, "The producer - consumer is bigger than ring size!\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  return TBT_STATUS_SUCCESS;
}

STATIC
VOID
TbtNvmDrvClearDescriptor (
  IN TBT_BUF_DESC *DescMem,
  IN UINT32   DescPointer
  )
{
  DescMem[DescPointer].attributes = DESC_ATTR_REQ_STS;    // Request status is set
}

/**
  Prepares the descriptor for later use, increments the consumer

  @param[in] This     A Pointer to the DMA data structure
  @param[in] Cons     Current consumer index

**/
STATIC
VOID
TbtNvmDrvFreeRxDesc (
  IN TBT_DMA  *This,
  IN UINT32   Cons
  )
{
  TbtNvmDrvClearDescriptor (This->Impl->pSharedMem->rxBufDesc, Cons);
  This->WriteMmio (This, REG_RX_RING_BASE + REG_RING_CONS_PROD_OFFSET, Cons);
}

/**
  Waits for a response with given pdf
  Operation:
    Poll on buffer descriptor, where consumer index points to.
    If it is done:
      read pdf
      if pdf != Pdf:
        free descriptor, increment consumer
      else:
        return consumer index
  @param[in]  This        Pointer to the DMA instance on which the function is activated
  @param[in]  Pdf         PDF of a frame to wait for
  @param[out] ConsPtr     Consumer index to return
  @param[out] Length      Length of the received frame

  @retval EFI_SUCCESS in case of success. EFI_NO_RESPONSE indicating no response from DMA, stuck.

**/
STATIC
TBT_STATUS
TbtNvmDrvWaitForResponse (
  IN  TBT_DMA    *This,
  IN  PDF_VALUE  Pdf,
  OUT UINT32     *ConsPtr,
  OUT UINT16     *Length
  )
{
  EFI_STATUS             Status;
  UINT32                 ProdConsReg;
  UINT32                 ConsIndex;
  volatile UINT8         ReceivedPdf;
  volatile TBT_BUF_DESC  *RxDescPtr;
  UINT32                 Index;
  UINT32                 TotalRxPkts;

  ProdConsReg = This->ReadMmio (This, REG_RX_RING_BASE + REG_RING_CONS_PROD_OFFSET);
  ConsIndex = (ProdConsReg & REG_RING_CONS_MASK) >> REG_RING_CONS_SHIFT;
  TotalRxPkts = 0;
  // TODO improve this using timer
  do {
    ConsIndex = (ConsIndex + 1) % FW_RING_NUM_RX_BUFS;  // Get consumer pointer from HW
    RxDescPtr = &(This->Impl->pSharedMem->rxBufDesc[ConsIndex]);
    DEBUG ((DEBUG_VERBOSE, "Before: RxDescPtr - 0x%x, attributes - 0x%x, data addr - 0x%x\n", RxDescPtr, RxDescPtr->attributes, RxDescPtr->Phys));
    for (Index = 0; ((RxDescPtr->attributes & DESC_ATTR_DESC_DONE) == 0) && (Index < TBT_TOTAL_ACCESSES_WHEN_WAIT_TO_RX); ++Index) {
      Status = gBS->Stall (TBT_TIME_BETWEEN_RX_RING_POLL_US);
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "Stall had failed, Status %d\n", Status));
      }
    }

    if (Index >= TBT_TOTAL_ACCESSES_WHEN_WAIT_TO_RX) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForResponse: ERROR! Timeout waiting for any response packet from DMA\n"));
      DEBUG ((DEBUG_ERROR, "Waited for %d us. Exiting...\n", TBT_TOTAL_TIME_TO_WAIT_FOR_RX_US));
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForResponse: The descriptor address - 0x%x, attributes - 0x%x\n", RxDescPtr, RxDescPtr->attributes));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    ReceivedPdf = (UINT8)((RxDescPtr->attributes & DESC_ATTR_EOF_MASK) >> DESC_ATTR_EOF_SHIFT);
    DEBUG ((
      DEBUG_VERBOSE,
      "DMA RX frame: pdf 0x%x cmd 0x%x msg_len 0x%x. Consumer index 0x%x\n",
      ReceivedPdf,
      *(((UINT32*)This->Impl->pSharedMem->rxBuf[ConsIndex] + 2)),
      RxDescPtr->attributes & DESC_ATTR_LEN_MASK,
      ConsIndex
      ));

    DEBUG ((DEBUG_VERBOSE, "After: RxDescPtr - 0x%x, attributes - 0x%x, data addr - 0x%x\n", RxDescPtr, RxDescPtr->attributes, RxDescPtr->Phys));

    /* free the descriptors for more receive */
    if (ReceivedPdf != Pdf) {
      TbtNvmDrvFreeRxDesc (This, ConsIndex);
    }
    TotalRxPkts++;
  } while ((ReceivedPdf != Pdf) && (TotalRxPkts < MAX_PACKETS_TO_RX_WHILE_WAIT_FOR_RESP));

  if (TotalRxPkts >= MAX_PACKETS_TO_RX_WHILE_WAIT_FOR_RESP) {
    DEBUG ((DEBUG_ERROR, "Timeout waiting for the response with given Pdf 0x%x. Exiting...\n", Pdf));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  *ConsPtr = ConsIndex;
  *Length = (UINT16)(RxDescPtr->attributes & DESC_ATTR_LEN_MASK);
  return TBT_STATUS_SUCCESS;
}


/**
  Send DMA config packet with the given parameters.
  Config packet, meaning packet on DMA ring #0.
  Operation:
    Copy the given data to the TX ring data buffer
    Prepare TX descriptor
    Increment producer

  @param[in] This   Pointer to the DMA instance on which the function is activated
  @param[in] Eof    End of frame identifier
  @param[in] Length The length of the frame
  @param[in] Data   Pointer to the byte data

**/
STATIC
TBT_STATUS
TbtNvmDrvTxCfgPkt (
  IN TBT_DMA    *This,
  IN PDF_VALUE  Eof,
  IN UINT16     Length,
  IN UINT8      *Data
  )
{
  UINT16 Prod;
  VOID *TxBufPtr = TbtNvmDrvGetTxBuf (This, &Prod);
  if (TxBufPtr == NULL) {
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  gBS->CopyMem(TxBufPtr, Data, Length);   // Copy the TX frame to the DMA TX buffer
  return TbtNvmDrvInitiateTx (This, Eof, Length, Prod);
}

/**
  DMA will poll it's receive queue until the frame with the given parameters
  is received.

  In case the received frame has length other than expected, the error will be returned.

  @param[in]  This    Pointer to the DMA instance on which the function is activated
  @param[in]  Eof     End of frame identifier
  @param[in]  Length  The length of the expected frame in byte
  @param[out] Data    Pointer to the byte data where to store the received data

**/
STATIC
TBT_STATUS
TbtNvmDrvRxCfgPkt (
  IN  TBT_DMA      *This,
  IN  PDF_VALUE    Eof,
  IN  UINT16       *Length OPTIONAL,
  OUT UINT8        *Data OPTIONAL
  )
{
  TBT_STATUS  Status;
  UINT32      Cons;
  UINT16      Len;

  Status = TbtNvmDrvWaitForResponse (This, Eof, &Cons, &Len);
  if (TBT_STATUS_ERR (Status)) {
    return Status;
  }
  if (Length != NULL && Len != *Length) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRxCfgPkt: ERROR! The frame received has length other than expected\n"));
    DEBUG ((DEBUG_ERROR, "                   Expected - 0x%x, Received - 0x%x\n", *Length, Len));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  if (Data != NULL) {
    gBS->CopyMem (Data, This->Impl->pSharedMem->rxBuf[Cons], Len);
  }
  TbtNvmDrvFreeRxDesc (This, Cons);
  return TBT_STATUS_SUCCESS;
}

/**
  Write DMA MMIO register.

  @param[in] This          Pointer to the TBT_DMA instance on which the function is activated.
  @param[in] RegOffset     Offset in DMA MMIO to write to.
  @param[in] Data          Data to write.

**/
STATIC
VOID
TbtNvmDrvDmaWriteMmio (
  IN TBT_DMA *This,
  IN UINT32  RegOffset,
  IN UINT32  Data
  )
{
  DEBUG ((DEBUG_VERBOSE, "Write MMIO offset - 0x%x, wdata - 0x%x\n", RegOffset, Data));

  This->Impl->PciIoProtoPtr->Mem.Write (
                                   This->Impl->PciIoProtoPtr,
                                   EfiPciIoWidthUint32,
                                   0,
                                   RegOffset,
                                   1,
                                   &Data
                                   );
}

/**
  Read DMA MMIO register.

  @param[in] This           Pointer to the TBT_DMA instance on which the function is activated.
  @param[in] RegOffset      Offset in DMA MMIO to read from.

**/
STATIC
UINT32
TbtNvmDrvDmaReadMmio (
  IN TBT_DMA *This,
  IN UINT32  RegOffset
  )
{
  UINT32 ReadVal;

  This->Impl->PciIoProtoPtr->Mem.Read (
                                   This->Impl->PciIoProtoPtr,
                                   EfiPciIoWidthUint32,
                                   0,
                                   RegOffset,
                                   1,
                                   &ReadVal
                                   );

  DEBUG ((DEBUG_VERBOSE, "Read MMIO offset - 0x%x, rdata - 0x%x\n", RegOffset, ReadVal));
  return ReadVal;
}

STATIC
VOID
TbtNvmDrvDmaDebugPrint (
  TBT_DMA *This
)
{
/*
  UINT32 i;
  //UINT32 *addr;
  DEBUG ((DEBUG_INFO, "-------------------------------------------------------------------------\n"));
  DEBUG ((DEBUG_INFO, "Printing the DMA internal state for debug:\n"));
  DEBUG ((DEBUG_INFO, "ARC_DEBUG - 0x%x\n\n", TbtNvmDrvDmaReadMmio(This, TBT_ARC_DEBUG_REG_MMIO_OFFSET)));
  for (i = 0; i < 4; i++) {
    DEBUG ((DEBUG_INFO, "TX ring descriptor DW%d - 0x%x\n", i, TbtNvmDrvDmaReadMmio(This, REG_TX_RING_BASE + i*4)));
  }
  DEBUG ((DEBUG_INFO, "\n"));
  for (i = 0; i < 5; i++) {
    DEBUG ((DEBUG_INFO, "TX ring table DW%d - 0x%x\n", i, TbtNvmDrvDmaReadMmio(This, REG_TX_OPTIONS_BASE + i*4)));
  }
  DEBUG ((DEBUG_INFO, "\n"));
  for (i = 0; i < 4; i++) {
    DEBUG ((DEBUG_INFO, "RX ring descriptor DW%d - 0x%x\n", i, TbtNvmDrvDmaReadMmio(This, REG_RX_RING_BASE + i*4)));
  }
  DEBUG ((DEBUG_INFO, "\n"));
  for (i = 0; i < 5; i++) {
    DEBUG ((DEBUG_INFO, "RX ring table DW%d - 0x%x\n", i, TbtNvmDrvDmaReadMmio(This, REG_RX_OPTIONS_BASE + i*4)));
  }
  DEBUG ((DEBUG_INFO, "\n"));
  //addr = (UINT32 *)(TbtNvmDrvDmaReadMmio(This, REG_TX_RING_BASE) | (TbtNvmDrvDmaReadMmio(This, REG_TX_RING_BASE + 4) << 32));
  */
}

/**
  Dma destructor. Deallocates all the resources for DMA operation

  @param[in] This       Pointer to the TBT_DMA instance on which the function is activated.

**/
STATIC
VOID
TbtNvmDrvDmaDtor(
  TBT_DMA *This
  )
{
  UINT32 Data;

  DEBUG ((DEBUG_INFO, "TbtNvmDrvDmaDtor is called.\n"));

  // Release RX ring
  Data = This->ReadMmio (This, REG_RX_OPTIONS_BASE);
  Data &= ~(REG_OPTS_VALID);
  This->WriteMmio (This, REG_RX_OPTIONS_BASE, Data);

  // Release TX ring
  Data = This->ReadMmio (This, REG_TX_OPTIONS_BASE);
  Data &= ~(REG_OPTS_VALID);
  This->WriteMmio (This, REG_TX_OPTIONS_BASE, Data);

  This->Impl->PciIoProtoPtr->Unmap (This->Impl->PciIoProtoPtr, This->Impl->mapping);
  This->Impl->PciIoProtoPtr->FreeBuffer (
                               This->Impl->PciIoProtoPtr,
                               EFI_SIZE_TO_PAGES (sizeof(*This->Impl->pSharedMem)),
                               This->Impl->pSharedMem
                               );
  TbtNvmDrvDeAllocateMem (This->Impl);
  TbtNvmDrvDeAllocateMem (This);
}

/**
  Dma constructor. Allocates all the required resources for DMA operation

  @param[in] PciIoProto               Pointer to the PCI Protocol instance.
  @param[in] TBTControllerWasPowered  Store the TBT controller power status to DMA structure.

  @retval    TBT_DMA*    Pointer to the TBT_DMA structure

**/
TBT_DMA*
TbtNvmDrvDmaCtor (
  EFI_PCI_IO_PROTOCOL *PciIoProto,
  BOOLEAN             *TBTControllerWasPowered
  )
{
  EFI_STATUS            Status;
  TBT_DMA_IMPL          *DmaImplPtr;
  UINTN                 NumBytes;
  EFI_PHYSICAL_ADDRESS  PhyAddr;
  UINT32                RegVal;
  UINT32                Index;
  TBT_DMA               *DmaPtr;

  DmaImplPtr = TbtNvmDrvAllocateMem (sizeof(TBT_DMA_IMPL));
  if (!DmaImplPtr) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvDmaCtor: AllocateRuntimeZeroPool failed, Status = %r\n", EFI_OUT_OF_RESOURCES));
    return NULL;
  }
  DmaImplPtr->PciIoProtoPtr = PciIoProto;
  // Allocate DMA memory
  Status = DmaImplPtr->PciIoProtoPtr->AllocateBuffer (
                                        DmaImplPtr->PciIoProtoPtr,
                                        AllocateAnyPages,
                                        EfiRuntimeServicesData,
                                        EFI_SIZE_TO_PAGES (sizeof(*DmaImplPtr->pSharedMem)),
                                        (VOID**) &DmaImplPtr->pSharedMem,
                                        0
                                        );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvDmaCtor: AllocateBuffer failed, Status = %r\n", Status));
    goto free_dma_impl;
  }
  // Map the memory to the physical location
  // Get physical address of the memory along with mapping to free it later.
  NumBytes = sizeof (*DmaImplPtr->pSharedMem);
  Status = DmaImplPtr->PciIoProtoPtr->Map (
                                        DmaImplPtr->PciIoProtoPtr,
                                        EfiPciIoOperationBusMasterCommonBuffer,
                                        DmaImplPtr->pSharedMem,
                                        &NumBytes,
                                        &DmaImplPtr->PhyAddr,
                                        &DmaImplPtr->mapping
                                        );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvDmaCtor: Map failed, Status=%r\n", Status));
    goto free_shared_mem;
  }

  // Check the mapping is done fully
  if (NumBytes != sizeof (*DmaImplPtr->pSharedMem)) {
    Status = EFI_OUT_OF_RESOURCES;
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvDmaCtor: Map returned %u bytes, Status=%r\n", NumBytes, Status));
    goto unmap;
  }

  // Initialize the DMA public object
  DmaPtr = TbtNvmDrvAllocateMem (sizeof(TBT_DMA));
  if (!DmaPtr) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvDmaCtor: AllocateRuntimeZeroPool failed, Status=%r\n", EFI_OUT_OF_RESOURCES));
    goto unmap;
  }
  DmaPtr->Impl                    = DmaImplPtr;
  DmaPtr->RxCfgPkt                = TbtNvmDrvRxCfgPkt;
  DmaPtr->TxCfgPkt                = TbtNvmDrvTxCfgPkt;
  DmaPtr->ReadMmio                = TbtNvmDrvDmaReadMmio;
  DmaPtr->WriteMmio               = TbtNvmDrvDmaWriteMmio;
  DmaPtr->DbgPrint                = TbtNvmDrvDmaDebugPrint;
  DmaPtr->Dtor                    = TbtNvmDrvDmaDtor;
  DmaPtr->TBTControllerWasPowered = TBTControllerWasPowered;

  // Configure the TX descriptors
  for (Index = 0, PhyAddr = DmaImplPtr->PhyAddr;
       Index < FW_RING_NUM_TX_BUFS;
       ++Index, PhyAddr += MAX_FW_FRAME_SIZE) {
    DmaImplPtr->pSharedMem->txBufDesc[Index].Phys = PhyAddr;
    TbtNvmDrvClearDescriptor (DmaImplPtr->pSharedMem->txBufDesc, Index);
  }

  // Configure the RX descriptors
  for (Index = 0;
       Index < FW_RING_NUM_RX_BUFS;
       ++Index, PhyAddr += MAX_FW_FRAME_SIZE) {
    DmaImplPtr->pSharedMem->rxBufDesc[Index].Phys = PhyAddr;
    TbtNvmDrvClearDescriptor (DmaImplPtr->pSharedMem->rxBufDesc, Index);
  }

  // Configure the TX registers for FW ring
  PhyAddr = DmaImplPtr->PhyAddr + OFFSET_OF (SHARED_MEMORY, txBufDesc);
  DmaPtr->WriteMmio (DmaPtr, REG_TX_RING_BASE, (UINT32)PhyAddr);
  DmaPtr->WriteMmio (DmaPtr, REG_TX_RING_BASE+4, (UINT32)(PhyAddr>>32));
  RegVal = (FW_RING_NUM_TX_BUFS << REG_RING_SIZE_SHIFT) & REG_RING_SIZE_MASK;
  DmaPtr->WriteMmio (DmaPtr, REG_TX_RING_BASE + REG_RING_SIZE_OFFSET, RegVal);
  RegVal = REG_OPTS_RAW | REG_OPTS_VALID;
  DmaPtr->WriteMmio (DmaPtr, REG_TX_OPTIONS_BASE, RegVal);

  // Configure the RX registers for FW ring
  PhyAddr = DmaImplPtr->PhyAddr + OFFSET_OF (SHARED_MEMORY, rxBufDesc);
  DmaPtr->WriteMmio (DmaPtr, REG_RX_RING_BASE, (UINT32)PhyAddr);
  DmaPtr->WriteMmio (DmaPtr, REG_RX_RING_BASE + 4, (UINT32)(PhyAddr >> 32));
  RegVal = ((FW_RING_NUM_RX_BUFS << REG_RING_SIZE_SHIFT) & REG_RING_SIZE_MASK)
         | ((MAX_FW_FRAME_SIZE << REG_RING_BUF_SIZE_SHIFT)
         & REG_RING_BUF_SIZE_MASK);
  DmaPtr->WriteMmio (DmaPtr, REG_RX_RING_BASE + REG_RING_SIZE_OFFSET, RegVal);
  RegVal = ((FW_RING_NUM_RX_BUFS - 1) << REG_RING_CONS_SHIFT) & REG_RING_CONS_MASK;
  DmaPtr->WriteMmio (DmaPtr, REG_RX_RING_BASE + REG_RING_CONS_PROD_OFFSET, RegVal);
  RegVal = REG_OPTS_RAW | REG_OPTS_VALID;
  DmaPtr->WriteMmio (DmaPtr, REG_RX_OPTIONS_BASE, RegVal);

  return DmaPtr;

unmap:
  DmaImplPtr->PciIoProtoPtr->Unmap (DmaImplPtr->PciIoProtoPtr, DmaImplPtr->mapping);
free_shared_mem:
  DmaImplPtr->PciIoProtoPtr->FreeBuffer (
                               DmaImplPtr->PciIoProtoPtr,
                               EFI_SIZE_TO_PAGES (sizeof(*DmaImplPtr->pSharedMem)),
                               DmaImplPtr->pSharedMem
                               );
free_dma_impl:
  TbtNvmDrvDeAllocateMem (DmaImplPtr);
  return NULL;
}
