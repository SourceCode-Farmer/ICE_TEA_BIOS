/** @file
  Implements the interface for TbtNvmDrvHr class.
  This class represents TBT HR abstraction.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/


#include <Library/UefiBootServicesTableLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci22.h>
#include <Usb4PlatformHob.h>
#include <Usb4CmMode.h>
#include <Library/HobLib.h>
#include "TbtNvmDrvHr.h"
#include "Crc32c.h"

#define TBT_EE_ARC_EN_BIT_OFFSET 0
#define TBT_EE_ARC_EN_XOR_BIT_OFFSET 1
#define TBT_DEVICE_CS_LC_PER_CONTROLLER_VSEC0_BASE 0x113
#define TBT_DEVICE_CS_LC_PER_CONTROLLER_VSEC1_BASE 0x213
#define TBT_DEVICE_CS_LC_SX_CTRL_OFFSET TBT_DEVICE_CS_LC_PER_CONTROLLER_VSEC0_BASE + 0x96
#define INTEL_CM_BIT    (1<<28)

BOOLEAN   mTBTControllerWasPowered; // Check TBT controller power Status in CM mode

#define USB4_CLASS_CODE 0x0C0340

#define USB_VENDOR_ID_1 0x8086
#define USB_VENDOR_ID_2 0x8087

typedef struct {
  UINT32 reqCode;
  UINT32 crc;
} DRV_RDY_CMD;

/*
* SW->FW commands, supported by this implementation
* CC = Command Code
*/
enum {
  CC_DRV_READY = 3
};

/*
* FW->SW responses, supported by this implementation
* RC = response code
*/
enum {
  RC_DRV_READY = 3
};

typedef struct {
  UINT16 VendorId;
  UINT16 DeviceId;
} PCI_ID;

/**
  Send driver ready using DMA.

  @param[in] DmaPtr DMA instance to work with

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
STATIC
TBT_STATUS
TbtNvmDrvHrSendDrvReady (
  TBT_DMA *DmaPtr
  )
{
  EFI_STATUS    Status;
  DRV_RDY_CMD   Message;
  UINT32        Crc32;

  if (DmaPtr == NULL) {
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  // Prepare the driver ready message to be send
  Message.reqCode = SwapBytes32 (CC_DRV_READY);
  Status = CalCrc32c ((UINT8 *)&Message, OFFSET_OF (DRV_RDY_CMD, crc), &Crc32);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrSendDrvReady: Failed to generate CRC from the data. Status - %d. Exiting...\n", Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  Message.crc = SwapBytes32 (Crc32);
  // Send the message
  Status = DmaPtr->TxCfgPkt (DmaPtr, PDF_SW_TO_FW_COMMAND, sizeof(Message), (UINT8 *)&Message);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrSendDrvReady: The driver ready command failed. Status - %d. Exiting...\n", Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  // Make sure response is received
  Status = DmaPtr->RxCfgPkt (DmaPtr, PDF_FW_TO_SW_RESPONSE, NULL, NULL);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrSendDrvReady: Didn't receive response for driver ready command. Status - %d. Exiting...\n", Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvHrSendDrvReady: Sent successfully\n"));
  return TBT_STATUS_SUCCESS;
}

/* Write request pointer to reuse in TbtNvmDrvHrWriteCioReg only */
STATIC WRITE_CONFIGURATION_REGISTERS_REQUEST *mWriteDevReq = NULL;
/* Buffer to be used in TbtNvmDrvHrWriteCioReg. Predefined as optimization */
STATIC WRITE_CONFIGURATION_REGISTERS_REQUEST mWriteReqSwapped;

/**
  Write register to TBT CIO config space

  @param[in]  This               Pointer to the TBT_HOST_ROUTER structure on which the method is invoked
  @param[in]  ConfigurationSpace TBT device config space.
  @param[in]  Adapter            Adapter to be accessed.
  @param[in]  RegNum             Register offset in TBT device config space.
  @param[in]  Length             How many byte to write
  @param[in]  DataPtr            Data pointer to write

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
STATIC
TBT_STATUS
TbtNvmDrvHrWriteCioReg (
  IN TBT_HOST_ROUTER *This,
  IN UINT8           ConfigurationSpace,
  IN UINT8           Adapter,
  IN UINT16          RegNum,
  IN UINT8           Length,
  IN UINT32          *DataPtr
  )
{
  EFI_STATUS Status;
  UINT16     DataPtrIndex;
  UINT32     Len;
  UINT32     Crc32;
  UINT16     ExpectedLength;

  if ( (DataPtr == NULL)
    || (This == NULL)
    || (Length <= 0)
    || (Length > 16))
  {
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  DEBUG_CODE_BEGIN ();

  DEBUG ((DEBUG_VERBOSE, "TbtNvmDrvHrWriteCioReg: ConfigurationSpace - %d, Adapter - %d, RegNum - 0x%x\n", ConfigurationSpace, Adapter, RegNum));
  DEBUG ((DEBUG_VERBOSE, "TbtNvmDrvHrWriteCioReg: Length - 0x%x, Data - ", Length));
  for (DataPtrIndex = 0; DataPtrIndex < Length; DataPtrIndex++) {
    DEBUG ((DEBUG_VERBOSE, "%x ", *(DataPtr + DataPtrIndex)));
  }
  DEBUG ((DEBUG_VERBOSE, "\n"));

  DEBUG_CODE_END ();

  ASSERT (Length > 0 && Length <= 16);

  if (mWriteDevReq == NULL) {  // If not allocated yet
    mWriteDevReq = TbtNvmDrvAllocateMem (sizeof(WRITE_CONFIGURATION_REGISTERS_REQUEST));
    if (mWriteDevReq == NULL) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrWriteCioReg: Couldn't allocate memory for write request struct. Exiting...\n"));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Initialize constant values for read request
    mWriteDevReq->RouteString = 0x0;    // HR
    mWriteDevReq->SequenceNumber = 0;   // Always use the same one, no two outstanding requests anyway;
  }
  mWriteDevReq->ConfigurationSpace = ConfigurationSpace;
  mWriteDevReq->Adapter = Adapter;
  mWriteDevReq->DwIndex = RegNum;
  mWriteDevReq->Length  = Length;
  for (DataPtrIndex = 0; DataPtrIndex < Length; DataPtrIndex++) {
    mWriteDevReq->WrData[DataPtrIndex] = *(DataPtr + DataPtrIndex);
  }
  // DataPtrIndex - points after the data

  // Swap the data
  Len = OFFSET_OF (WRITE_CONFIGURATION_REGISTERS_REQUEST, WrData) + DataPtrIndex * 4; // Length in Byte without CRC
  if (Len > sizeof (WRITE_CONFIGURATION_REGISTERS_REQUEST)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrWriteCioReg: Avoid buffer overflow.\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  TbtNvmDrvSwapEndianess ((UINT32 *)mWriteDevReq, Len / 4, (UINT32 *)&mWriteReqSwapped);
  // Calculate CRC
  Status = CalCrc32c ((UINT8 *)&mWriteReqSwapped, Len, &Crc32);
  if (TBT_STATUS_ERR (Status)) {
    Print(L"TbtNvmDrvHrWriteCioReg: Failed to generate CRC from the data. Status - %d. Exiting...\n", Status);
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  mWriteReqSwapped.WrData[DataPtrIndex] = SwapBytes32 (Crc32);    // Insert CRC
  // Transmit the request
  Status = This->Impl->pDma->TxCfgPkt (
                               This->Impl->pDma,
                               PDF_WRITE_CONFIGURATION_REGISTERS,
                               OFFSET_OF (WRITE_CONFIGURATION_REGISTERS_REQUEST, WrData) + Length * 4 + 4,  // RS + CMD + WRDATA + CRC
                               (UINT8 *)&mWriteReqSwapped
                               );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrWriteCioReg: Couldn't transmit DMA frame to write to reg - 0x%x, length - 0x%x. Exiting...\n", RegNum, Length));
    goto error_while_writing;
  }
  ExpectedLength = (UINT16)sizeof(WRITE_CONFIGURATION_REGISTERS_RESPONSE);
  // Receive the response
  Status = This->Impl->pDma->RxCfgPkt (
                               This->Impl->pDma,
                               PDF_WRITE_CONFIGURATION_REGISTERS,
                               &ExpectedLength,
                               NULL                                              // No data in the response
                               );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrReadCioDevReg: TbtNvmDrvHrWriteCioReg: Couldn't receive response while writing to reg - 0x%x, length - 0x%x. Exiting...\n", RegNum, Length));
    goto error_while_writing;
  }

  return TBT_STATUS_SUCCESS;

error_while_writing:
  This->Impl->pDma->DbgPrint (This->Impl->pDma);
  return TBT_STATUS_NON_RECOVERABLE_ERROR;
}

/* Read request pointer to reuse in TbtNvmDrvHrReadCioDevReg only */
STATIC READ_CONFIGURATION_REGISTERS_REQUEST *mReadDevReq = NULL;

/* Buffer to be used in TbtNvmDrvHrReadCioDevReg. Predefined as optimization */
STATIC READ_CONFIGURATION_REGISTERS_REQUEST mReadReqSwapped;

/* Buffer to be used in TbtNvmDrvHrReadCioDevReg. Predefined as optimization */
STATIC READ_CONFIGURATION_REGISTERS_RESPONSE mReadReqResponse;

/**
  Reads the a register with a given address from TBT device config space.
  Operation:
    Prepare the read request
    Swap bytes
    Calculate CRC and swap it too

  @param[in]  This               Pointer to the TBT_HOST_ROUTER structure on which the method is invoked
  @param[in]  ConfigurationSpace TBT device config space.
  @param[in]  Adapter            Adapter to be accessed.
  @param[in]  RegNum             Register offset in TBT device config space.
  @param[out] DataPtr            Read data, one DW.

**/
STATIC
TBT_STATUS
TbtNvmDrvHrReadCioDevReg (
  IN  TBT_HOST_ROUTER *This,
  IN  UINT8           ConfigurationSpace,
  IN  UINT8           Adapter,
  IN  UINT16          RegNum,
  OUT UINT32          *DataPtr
  )
{
  EFI_STATUS Status;
  UINT32     Len;
  UINT32     Crc32;
  UINT32     ReadData;
  UINT16     ExpectedLength;

  if ( (DataPtr == NULL)
    || (This == NULL) )
  {
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  DEBUG ((DEBUG_VERBOSE, "TbtNvmDrvHrReadCioDevReg: ConfigurationSpace - %d, Adapter - %d, RegNum - 0x%x\n", ConfigurationSpace, Adapter, RegNum));

  if (mReadDevReq == NULL) {  // If not allocated yet
    mReadDevReq = TbtNvmDrvAllocateMem (sizeof(READ_CONFIGURATION_REGISTERS_REQUEST));
    if (mReadDevReq == NULL) {
      DEBUG ((DEBUG_ERROR, "Couldn't allocate memory for read request struct\n"));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Initialize constant values for read request
    mReadDevReq->RouteString = 0x0;    // HR
    mReadDevReq->SequenceNumber = 0;   // Always use the same one, no two outstanding requests anyway;
    mReadDevReq->Length = 1;          // Always read only one register
  }
  mReadDevReq->ConfigurationSpace = ConfigurationSpace;
  mReadDevReq->Adapter = Adapter;
  mReadDevReq->DwIndex = RegNum;
  // Swap the data
  Len = (UINT32) ((UINTN) &(mReadReqSwapped.Crc) - (UINTN) &mReadReqSwapped);
  if (Len > sizeof(READ_CONFIGURATION_REGISTERS_REQUEST)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrWriteCioReg: Avoid buffer overflow.\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  TbtNvmDrvSwapEndianess ((UINT32 *)mReadDevReq, Len / 4, (UINT32 *)&mReadReqSwapped);
  // Calculate CRC
  Status = CalCrc32c ((UINT8 *)&mReadReqSwapped, Len, &Crc32);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrWriteCioReg: Failed to generate CRC from the data. Status - %d. Exiting...\n", Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  mReadReqSwapped.Crc = SwapBytes32 (Crc32);
  // Transmit the request
  Status = This->Impl->pDma->TxCfgPkt (
                               This->Impl->pDma,
                               PDF_READ_CONFIGURATION_REGISTERS,
                               (UINT16)(Len + 4),                                // Length + CRC
                               (UINT8 *)&mReadReqSwapped
                               );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrReadCioDevReg: Couldn't transmit DMA frame to read from reg - 0x%x. Exiting...\n", RegNum));
    goto error_while_reading;
  }
  ExpectedLength = (UINT16)OFFSET_OF (READ_CONFIGURATION_REGISTERS_RESPONSE, Crc) + 4;
  // Receive the response
  Status = This->Impl->pDma->RxCfgPkt (
                               This->Impl->pDma,
                               PDF_READ_CONFIGURATION_REGISTERS,
                               &ExpectedLength,
                               (UINT8 *)&mReadReqResponse
                               );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrReadCioDevReg: Couldn't receive response while reading from reg - 0x%x. Exiting...\n", RegNum));
    goto error_while_reading;
  }

  // Prepare the read data for return
  ReadData = mReadReqResponse.ReadData;
  ReadData = SwapBytes32 (ReadData);
  *DataPtr = ReadData;
  DEBUG ((DEBUG_VERBOSE, "Read data is - 0x%x\n", ReadData));

  return TBT_STATUS_SUCCESS;

error_while_reading:
  This->Impl->pDma->DbgPrint (This->Impl->pDma);
  return TBT_STATUS_NON_RECOVERABLE_ERROR;
}

STATIC
VOID
TbtNvmDrvHrDtor (
  TBT_HOST_ROUTER *This
  )
{
  DEBUG ((DEBUG_VERBOSE, "TbtNvmDrvHrDtor is called.\n"));

  if (This->Impl->ForcePwrFunc != NULL) {
    This->Impl->ForcePwrFunc (This->Impl->pPciIoProto, FALSE, This->Impl->pDma->TBTControllerWasPowered);   // Disable the force power to HR
  }
  This->Impl->pDma->Dtor (This->Impl->pDma);
  TbtNvmDrvDeAllocateMem (This->Impl);
  TbtNvmDrvDeAllocateMem (This);
}

/**
  TBT HR constructor.

  @param[in] FirmwareType    The firmware type of target.
  @param[in] PcieRpConfig    PCIe root port config.
  @param[in] PcieBdf         PCIe BDF identifier
  @param[in] ForcePwrFunc    Force power function, will be performed on a found TBT device.
                             If NULL -> no force pwr.

  @retval The TBT_HR object pointer

**/
TBT_HOST_ROUTER*
TbtNvmDrvHrCtor (
  IN UINT8           FirmwareType,
  IN PCIE_RP_CONFIG  *PcieRpConfig,
  IN PCIE_BDF        *TbtDmaPcieBdf, OPTIONAL
  IN FORCE_PWR_HR    ForcePwrFunc OPTIONAL
  )
{
  EFI_STATUS          Status;
  UINTN               HandleCount;
  EFI_HANDLE          *Handles;
  UINT32              Index;
  TBT_HR_IMPL         *HrImplPtr;
  UINTN               SegmentNumber;
  UINTN               BusNumber;
  UINTN               DeviceNumber;
  UINTN               FunctionNumber;
  UINT64              Attributes;
  TBT_HOST_ROUTER     *HrPtr;
  BOOLEAN             TBTControllerWasPowered;
  UINT32              *ClassCodePtr;
  PCI_ID              PciId;
  PCI_TYPE00          PciData;
  UINT8               RootPortIndex;
  UINT64              PcieRpBaseAddr;
  UINT8               SecondaryBus;
  UINT8               SubordinateBus;
  UINT8               CmMode;
  VOID                *HobPtr;
  USB4_PLATFORM_HOB   *Usb4PlatformHob;
  USB4_PLATFORM_INFO  *Usb4PlatformInfo;

  // Initialize UEFI PCI protocol
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPciIoProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: LocateHandleBuffer of gEfiPciIoProtocolGuid failed, Status = %r\n", Status));
    return NULL;
  }

  HrImplPtr = TbtNvmDrvAllocateMem (sizeof(TBT_HR_IMPL));
  if (!HrImplPtr) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: TbtNvmDrvAllocateMem failed, Status = %r\n", EFI_OUT_OF_RESOURCES));
    return NULL;
  }

  if ( (FirmwareType == DISCRETE_TBT_RETIMER)
    || (FirmwareType == DISCRETE_TBT))
  {
    RootPortIndex  = PcieRpConfig->PcieRootPort-1;
    switch (PcieRpConfig->PcieRpType) {
      case (SUPPORT_PCH_TYPE):
        PcieRpBaseAddr = PchPcieRpPciCfgBase (RootPortIndex);
        DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: PchPcieRpPciCfgBase %x \n", PcieRpBaseAddr));
        break;
      default:
        DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Failed at unsupported type value %x in PCIE_RP_TYPE.\n", PcieRpConfig->PcieRpType));
        goto free_hr_impl;
    }
    SecondaryBus   = PciSegmentRead8 (PcieRpBaseAddr + PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET);
    SubordinateBus = PciSegmentRead8 (PcieRpBaseAddr + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET);
    DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: FirmwareType %x, RootPortIndex %d, SecondaryBus %x, SubordinateBus %x\n",
      FirmwareType,
      RootPortIndex,
      SecondaryBus,
      SubordinateBus
      ));
  }

  // Search for the device with a given BDF or root port index.
  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (Handles[Index], &gEfiPciIoProtocolGuid, (VOID**) &HrImplPtr->pPciIoProto);
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: HandleProtocol of gEfiPciIoProtocolGuid failed, Status = %r\n", Status));
      continue;
    }
    Status = HrImplPtr->pPciIoProto->Pci.Read (
                                           HrImplPtr->pPciIoProto,
                                           EfiPciIoWidthUint32,
                                           0,
                                           sizeof (PciId) / sizeof (UINT32),
                                           &PciId
                                           );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Pci.Read failed, Status = %r\n", Status));
      continue;
    }
    if ( (PciId.VendorId != USB_VENDOR_ID_1)
      && (PciId.VendorId != USB_VENDOR_ID_2) )
    {
      Status = EFI_NOT_FOUND;
      continue;
    }
    //check for USB class
    Status = HrImplPtr->pPciIoProto->Pci.Read (
                                           HrImplPtr->pPciIoProto,
                                           EfiPciIoWidthUint8,
                                           PCI_CLASSCODE_OFFSET,
                                           sizeof ( PciData.Hdr.ClassCode ),
                                           PciData.Hdr.ClassCode
                                           );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: check for USB class failed, Status = %r\n", Status));
      continue;
    }

    ClassCodePtr = (UINT32 *)PciData.Hdr.ClassCode;
    *ClassCodePtr = *ClassCodePtr & 0xFFFFFF;
    DEBUG ((DEBUG_INFO, "ClassCode = %x\n", *ClassCodePtr));

    if (*ClassCodePtr == USB4_CLASS_CODE) {
      DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: USB4 PCI device\n"));

      Status = HrImplPtr->pPciIoProto->GetLocation (
                                         HrImplPtr->pPciIoProto,
                                         &SegmentNumber,
                                         &BusNumber,
                                         &DeviceNumber,
                                         &FunctionNumber
                                         );
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: GetLocation failed, Status = %r\n", Status));
        continue;
      }
      if (FirmwareType == INTEGRATED_TBT_RETIMER) {
        if ( (BusNumber != TbtDmaPcieBdf->Bus)
          || (DeviceNumber != TbtDmaPcieBdf->Device)
          || (FunctionNumber != TbtDmaPcieBdf->Function))
        {
          Status = EFI_NOT_FOUND;
          continue;
        }
      }
      if ( (FirmwareType == DISCRETE_TBT_RETIMER)
        || (FirmwareType == DISCRETE_TBT))
      {
        if ( (BusNumber < SecondaryBus)
          || (BusNumber > SubordinateBus))
        {
          DEBUG ((DEBUG_ERROR,"The TBT Controller %x: %04x:%02x:%02x.%02x is not the target.\n",
            PciId,
            SegmentNumber,
            BusNumber,
            DeviceNumber,
            FunctionNumber
            ));
          Status = EFI_NOT_FOUND;
          continue;
        }
      }

      DEBUG ((DEBUG_INFO,"TbtNvmDrvHrCtor: Found Thunderbolt Controller %x: %04x:%02x:%02x.%02x\n",
        PciId,
        SegmentNumber,
        BusNumber,
        DeviceNumber,
        FunctionNumber
        ));
      Status = EFI_SUCCESS;

      break;
    } else {
        Status = EFI_NOT_FOUND;
        continue;
    }
  } // end of search for the HR

  if (EFI_ERROR (Status)) {
    if (FirmwareType == INTEGRATED_TBT_RETIMER) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: PCI device with bdf = 0x%x,0x%x,0x%x is not found!, Status = %r\n",
        TbtDmaPcieBdf->Bus,
        TbtDmaPcieBdf->Device,
        TbtDmaPcieBdf->Function,
        Status
        ));
    } else {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: PCI device is not found with root port = %d!, Status = %r\n",
        PcieRpConfig->PcieRootPort,
        Status
        ));
    }
    goto free_hr_impl;
  }


  // Get the PCI Command options that are supported by this controller.
  Status = HrImplPtr->pPciIoProto->Attributes (
                                     HrImplPtr->pPciIoProto,
                                     EfiPciIoAttributeOperationSupported,
                                     0,
                                     &Attributes
                                     );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Attributes get is failed, Status = %r\n", Status));
    goto free_hr_impl;
  }

  // Set the PCI Command options to enable device memory mapped IO,
  // port IO, and bus mastering.
  Status = HrImplPtr->pPciIoProto->Attributes (
                                     HrImplPtr->pPciIoProto,
                                     EfiPciIoAttributeOperationEnable,
                                     Attributes | (EFI_PCI_DEVICE_ENABLE | EFI_PCI_IO_ATTRIBUTE_DUAL_ADDRESS_CYCLE),
                                     NULL
                                     );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Attributes set failed, Status = %r\n", Status));
    goto free_hr_impl;
  }

  if (ForcePwrFunc != NULL) {
    // Force power the ITBT
    TBTControllerWasPowered = FALSE;

    DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: Force power the ITBT\n"));
    Status = ForcePwrFunc (HrImplPtr->pPciIoProto, TRUE, &TBTControllerWasPowered);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Could not perform device %x force pwr, Status = %d\n", PciId, Status));
      goto free_hr_impl;
    }
    DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: Force powered device %x\n", PciId));
  }
  HrImplPtr->ForcePwrFunc = ForcePwrFunc;

  Crc32cInit ();
  // Initiate DMA and config it
  HrImplPtr->pDma = TbtNvmDrvDmaCtor (HrImplPtr->pPciIoProto, &mTBTControllerWasPowered);
  if (!HrImplPtr->pDma) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Could not initialize DMA for device \n"));
    goto free_hr_impl;
  }

  // Allocate the interface and assign it
  HrPtr = TbtNvmDrvAllocateMem (sizeof(TBT_HOST_ROUTER));
  if (!HrPtr) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Could not allocate memory for TBT_HOST_ROUTER\n"));
    goto free_dma;
  }
  HrPtr->Impl = HrImplPtr;
  HrPtr->ReadCioDevReg  = TbtNvmDrvHrReadCioDevReg;
  HrPtr->WriteCioDevReg = TbtNvmDrvHrWriteCioReg;
  HrPtr->Dtor           = TbtNvmDrvHrDtor;

  //
  // Get CM mode from Usb4PlatformHob
  //
  CmMode = USB4_CM_MODE_SW_CM;
  HobPtr = NULL;
  HobPtr = GetFirstGuidHob (&gUsb4PlatformHobGuid);

  if (HobPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Unable to get USB4 platform hob!\n"));
    TbtNvmDrvDeAllocateMem (HrPtr);
    goto free_dma;
  } else {
    Usb4PlatformHob = GET_GUID_HOB_DATA (HobPtr);
    Usb4PlatformInfo = &(Usb4PlatformHob->Usb4PlatformInfo);
    CmMode = (Usb4PlatformInfo->CmMode & 0x07);
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: CM Mode = %x\n", CmMode));

  // Send driver ready (needed when FW CM mode is detected)
  if (CmMode == USB4_CM_MODE_FW_CM) {
    mTBTControllerWasPowered = FALSE;
    *HrImplPtr->pDma->TBTControllerWasPowered = mTBTControllerWasPowered;
    Status = TbtNvmDrvHrSendDrvReady (HrImplPtr->pDma);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvHrCtor: Failed to send driver ready, Status=%d\n", Status));
      HrImplPtr->pDma->DbgPrint (HrImplPtr->pDma);
      TbtNvmDrvDeAllocateMem (HrPtr);
      goto free_dma;
    }
  } else {
    mTBTControllerWasPowered = TRUE;
    *HrImplPtr->pDma->TBTControllerWasPowered = mTBTControllerWasPowered;
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvHrCtor: Completed\n"));
  return HrPtr;

free_dma:
  HrImplPtr->pDma->Dtor (HrImplPtr->pDma);
free_hr_impl:
  TbtNvmDrvDeAllocateMem (HrImplPtr);
  return NULL;
}
