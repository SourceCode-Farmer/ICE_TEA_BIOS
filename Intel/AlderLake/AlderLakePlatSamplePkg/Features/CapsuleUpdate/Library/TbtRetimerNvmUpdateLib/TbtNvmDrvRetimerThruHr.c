/** @file
;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

/** @file
  Implements the interface for TbtNvmDrvRetimerThruHr class.
  This class is in charge of providing the way to access the retimer
  through TBT integrated HR.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#include <Library/UefiBootServicesTableLib.h>

#include "TbtNvmDrvRetimerThruHr.h"

#define IECS_CMD_ADDR      8
#define IECS_METADATA_ADDR 9
#define IECS_MSG_OUT_RDATA 18

#define TBT_LC_CMD_SUCCESS 0x0

#define TBT_IECS_CMD_LSUP  0x5055534c /* SET_INBOUND_LSTX */
#define TBT_IECS_CMD_USUP  0x50555355 /* UNSET_INBOUND_LSTX */
#define TBT_IECS_CMD_ENUM  0x4d554e45 /* ENUMERATE_RETIMERS */
#define TBT_IECS_CMD_LSEN  0x4e45534c /* RETIMER_OFFLINE_MODE */

#define TBT_TOTAL_ENUM_ACCESSES 4
#define TBT_TOTAL_LSUP_ACCESSES 3
#define TBT_WAIT_TIME_BETWEEN_ENUM_ACCESSES   10000   // time in us, 10ms
#define TBT_WAIT_TIME_BEFORE_NEXT_IECS_ACCESS 100     // time in us
#define TBT_WAIT_TIME_BEFORE_NEXT_MSG_OUT_ACCESS 50   // time in us
#define TBT_TOTAL_WAIT_TIME_UNTIL_TIMEOUT 200000      // 200ms in us

#define TBT_TOTAL_ACCESSES_WHILE_WAIT_FOR_IECS  \
 TBT_TOTAL_WAIT_TIME_UNTIL_TIMEOUT / TBT_WAIT_TIME_BEFORE_NEXT_IECS_ACCESS
#define TBT_TOTAL_ACCESSES_WHILE_WAIT_FOR_MSG_OUT \
 TBT_TOTAL_WAIT_TIME_UNTIL_TIMEOUT / TBT_WAIT_TIME_BEFORE_NEXT_MSG_OUT_ACCESS

#define TBT_TOTAL_RETRIES_ON_MSG_OUT 10

#define TBT_MSG_OUT_CMD_VALID       BIT(31)
#define TBT_MSG_OUT_ATCT1_LT0       BIT(30)
#define TBT_MSG_OUT_WR_RD_OFFSET    23
#define TBT_MSG_OUT_TIMEOUT         BIT(25)
#define TBT_MSG_OUT_INVALID         BIT(26)
#define TBT_MSG_OUT_LENGTH_OFFSET   16
#define TBT_MSG_OUT_REG_ADDR_OFFSET 8
#define TBT_MSG_OUT_START_CT        BIT(6)
#define TBT_MSG_OUT_RETIMER_INDEX_OFFSET 1
#define TBT_MSG_OUT_CMD             BIT(0)

#define TBT_USB4_PORT_CAPABILITY_LENGTH_OFFSET          8
#define TBT_USB4_PORT_CAPABILITY_TARGET_OFFSET         16
#define TBT_USB4_PORT_CAPABILITY_RETIMER_INDEX_OFFSET  20
#define TBT_USB4_PORT_CAPABILITY_WnR_OFFSET            24
#define TBT_USB4_PORT_CAPABILITY_TIMEOUT_OFFSET        25
#define TBT_USB4_PORT_CAPABILITY_RC_OFFSET             26
#define TBT_USB4_PORT_CAPABILITY_PND_OFFSET            31

#define PORT_CS_1 1
#define PORT_CS_2 2

#define ROUTER_CS_26 26

#define TBT_MSG_OUT_DEFAULT_VALS TBT_MSG_OUT_CMD_VALID | \
                                 TBT_MSG_OUT_ATCT1_LT0 | \
                                 TBT_MSG_OUT_START_CT  | \
                                 TBT_MSG_OUT_CMD

#define TBT_MSG_OUT_PA_TAR_ADDR 0x176
#define TBT_MSG_OUT_PB_TAR_ADDR 0x276
#define TBT_MSG_OUT_PA_WRDATA_0_TAR_ADDR 0x177
#define TBT_MSG_OUT_PB_WRDATA_0_TAR_ADDR 0x277
#define TBT_MSG_OUT_PA_RDDATA_0_TAR_ADDR 0x187
#define TBT_MSG_OUT_PB_RDDATA_0_TAR_ADDR 0x287

#define TBT_MSG_OUT_RDATA_LENGTH 1    // in DWs

#define TBT_IECS_MAILBOX_LEGTH_IN_DW 16

STATIC UINT16 TBT_USB4_PORT_CAPABILITY_OFFSET;

/**
  Wait for command to be completed
  Operation:
    Poll on MSG_OUT_CMD register to see when local LC completes the sending of a command to retimer

  @param[in] RetimerPtr        Pointer to the current implementation of TBT_RETIMER interface.
  @param[in] MsgOutCmdOffset   The MSG_OUT_CMD address in target space, different for each CIO port group
  @param[in] DbgData           Point to the caller of the function.

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_RETRY - the caller might retry
    TBT_STATUS_SUCCESS - indicate operation success
**/
STATIC
TBT_STATUS
WaitForMsgOutTxDone (
  IN RETIMER_THRU_HR     *RetimerPtr,
  IN UINT16              MsgOutCmdOffset,
  IN UINT8               DbgData
  )
{
  TBT_STATUS  Status;
  UINT32      Data;
  UINT32      AccessCnt;

  AccessCnt = 0;
  do {
    Status = RetimerPtr->Hr->ReadCioDevReg (
                               RetimerPtr->Hr,
                               ADAPTER_CONFIG_SPACE,
                               RetimerPtr->TbtPort,
                               MsgOutCmdOffset,
                               &Data
                               );
    DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: Read data = 0x%x\n", Data));
    if (TBT_STATUS_ERR (Status)) {
       DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: ERROR! Reading register MSG_OUT_CMD is failed. \
                             Status is %d. d=%dExiting...\n", Status, DbgData));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: IECS transaction read data=%x\n", Data));
    gBS->Stall(TBT_WAIT_TIME_BEFORE_NEXT_MSG_OUT_ACCESS);
    AccessCnt++;
  } while ((Data & TBT_MSG_OUT_CMD_VALID) != 0 && AccessCnt < TBT_TOTAL_ACCESSES_WHILE_WAIT_FOR_MSG_OUT);

  if ((Data & TBT_MSG_OUT_CMD_VALID) != 0) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: ERROR! Local LC seems to be stuck. \
                          d=%d, Data=%x, m=%x AccessCnt=%d Exiting...\n", DbgData, Data, MsgOutCmdOffset, AccessCnt));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  if ((Data & TBT_MSG_OUT_TIMEOUT) != 0) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: IECS transaction was timeouted \
                          d=%d, Data=%x, m=%x AccessCnt=%d\n", DbgData, Data, MsgOutCmdOffset, AccessCnt));
  } else if ((Data & TBT_MSG_OUT_INVALID) != 0) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: IECS transaction was invalid \
                          d=%d, Data=%x, m=%x AccessCnt=%d\n", DbgData, Data, MsgOutCmdOffset, AccessCnt));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  } else {
    return TBT_STATUS_SUCCESS;
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::WaitForMsgOutTxDone: TBT_STATUS_RETRY, d=%d\n", DbgData));
  return TBT_STATUS_RETRY;
}



/**
  Write Retimer's IECS register
  Operation:
    Write the data to write to MSG_OUT_WDATA corresponding to the port.
    Write the command to the MSG_OUT_CMD corresponding to the port.
    Wait for command to complete, return the Status.

  @param[in] This      Pointer to the generic device interface - TBT_RETIMER
  @param[in] RegNum    Retimer's IECS register number to write to
  @param[in] DataPtr   Data pointer to write
  @param[in] Length    How many DWs to write

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
STATIC
TBT_STATUS
WriteIecs (
  IN TBT_RETIMER      *This,
  IN UINT8            RegNum,
  IN UINT32           *DataPtr,
  IN UINT8            Length
  )
{
  TBT_STATUS        Status;
  RETIMER_THRU_HR   *RetimerPtr;
  UINT32            USB4CapRegWrite;
  UINTN             RetryCnt;

  DEBUG ((DEBUG_ERROR, "WriteIecs Length 0x%x\n", Length));
  ASSERT (Length <= TBT_IECS_MAILBOX_LEGTH_IN_DW && Length > 0);
  ASSERT (This != NULL);

  if (Length > TBT_IECS_MAILBOX_LEGTH_IN_DW || Length == 0 || This == NULL) {
    return TBT_STATUS_INVALID_PARAM;
  }

  DEBUG ((DEBUG_VERBOSE, "RetimerThruHr::WriteIecs: IECS reg num - 0x%x, Length - 0x%x\n", RegNum, Length));

  RetimerPtr = (RETIMER_THRU_HR *)This->Impl;

  Status = RetimerPtr->Hr->WriteCioDevReg (
                             RetimerPtr->Hr,
                             ADAPTER_CONFIG_SPACE,
                             RetimerPtr->TbtPort,
                             TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_2,
                             Length,
                             DataPtr
                             );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::WriteIecs: ERROR when writing the data, Status - %d. \
                          Exiting...\n", Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  USB4CapRegWrite = 0;
  for (RetryCnt = 0; RetryCnt < TBT_TOTAL_RETRIES_ON_MSG_OUT; RetryCnt++) {
    USB4CapRegWrite = (1 << TBT_USB4_PORT_CAPABILITY_PND_OFFSET)
                    | (1 << TBT_USB4_PORT_CAPABILITY_WnR_OFFSET)
                    | (RetimerPtr->RetimerIndex << TBT_USB4_PORT_CAPABILITY_RETIMER_INDEX_OFFSET)
                    | (2 << TBT_USB4_PORT_CAPABILITY_TARGET_OFFSET)
                    | ((Length << 2) << TBT_USB4_PORT_CAPABILITY_LENGTH_OFFSET)
                    | RegNum;

    DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHrWriteIecs: Writing USB 4.0 reg, USB4CapRegWrite = 0x%08x\n",
                          USB4CapRegWrite));
    Status = RetimerPtr->Hr->WriteCioDevReg (
                               RetimerPtr->Hr,
                               ADAPTER_CONFIG_SPACE,
                               RetimerPtr->TbtPort,
                               TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1,
                               1,
                               &USB4CapRegWrite
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::WriteIecs: ERROR when writing the data, Status - %d. \
                            Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    Status = WaitForMsgOutTxDone (RetimerPtr, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, 0);
    if (Status == TBT_STATUS_NON_RECOVERABLE_ERROR || Status == TBT_STATUS_SUCCESS) {
       if (Status == TBT_STATUS_NON_RECOVERABLE_ERROR)
       DEBUG ((DEBUG_ERROR, "RetimerThruHr::WriteIecs WaitForMsgOutTxDone::failed: \
                             RegNum=%x, Data[0]=%x, Length=%d, Status - %d. \
                             Exiting...\n", RegNum, *DataPtr, Length, Status));
      return Status;
    } else if (Status == TBT_STATUS_RETRY) {
        DEBUG ((DEBUG_ERROR, "RetimerThruHr::WriteIecs TBT_STATUS_RETRY WaitForMsgOutTxDone::failed: \
                              RegNum=%x, Data[0]=%x, Length=%d, Status - %d. \
                              Exiting...\n", RegNum, *DataPtr, Length, Status));
    }
    DEBUG ((DEBUG_ERROR, "RetimerThruHr::WriteIecs: RetryCnt=%d, Off=%x, Cmd=%x\n",
           RetryCnt, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, RegNum));
  }

  return Status;

}


/**
  Read Retimer's IECS register
  Operation:
    Write the command to the MSG_OUT_CMD corresponding to the port.
    Wait for command to complete
    Read MSG_OUT_RDDATA and assign it to buffer pointed by Data
    Return Status

  @param[in]  This        Pointer to the generic device interface - TBT_RETIMER
  @param[in]  RegNum      Retimer's IECS register number to read from
  @param[in]  DataLength  How many DWs to read
  @param[out] DataPtr     Data pointer to a buffer where the read data will be assigned

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes

  Limitation: read suppors only one DW
**/
STATIC
TBT_STATUS
ReadIecs (
  IN  TBT_RETIMER    *This,
  IN  UINT8          RegNum,   // TODO: RegNum type should be enum
  IN  UINT32         DataLength,
  OUT UINT32         *DataPtr
  )
{
  TBT_STATUS        Status;
  RETIMER_THRU_HR   *RetimerPtr;
  UINT32            USB4CapRegWrite;
  UINTN             RetryCnt;

  ASSERT (This != NULL);
  ASSERT (DataPtr != NULL);

  if ((This == NULL) || (DataPtr == NULL)) {
    return TBT_STATUS_INVALID_PARAM;
  }

  DEBUG ((DEBUG_VERBOSE, "RetimerThruHr::ReadIecs: IECS reg num - 0x%x\n", RegNum));

  RetimerPtr = (RETIMER_THRU_HR *)This->Impl;

  USB4CapRegWrite = 0;
  for (RetryCnt = 0; RetryCnt < TBT_TOTAL_RETRIES_ON_MSG_OUT; RetryCnt++) {
    USB4CapRegWrite = (1 << TBT_USB4_PORT_CAPABILITY_PND_OFFSET)
                    | (RetimerPtr->RetimerIndex << TBT_USB4_PORT_CAPABILITY_RETIMER_INDEX_OFFSET)
                    | (2 << TBT_USB4_PORT_CAPABILITY_TARGET_OFFSET)
                    | (DataLength << 2) << TBT_USB4_PORT_CAPABILITY_LENGTH_OFFSET
                    | RegNum;

    Status = RetimerPtr->Hr->WriteCioDevReg (
                               RetimerPtr->Hr,
                               ADAPTER_CONFIG_SPACE,
                               RetimerPtr->TbtPort,
                               TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1,
                               1,
                               &USB4CapRegWrite
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR,
              "RetimerThruHr::ReadIecs: ERROR when reading from IECS rdata, Status - %d. Exiting...\n",
              Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    Status = WaitForMsgOutTxDone (RetimerPtr, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, 1);
    if (Status == TBT_STATUS_NON_RECOVERABLE_ERROR) {
      return Status;
    } else if (Status == TBT_STATUS_SUCCESS) {
      break;
    }
    DEBUG ((DEBUG_ERROR, "RetimerThruHr::ReadIecs: RetryCnt=%d, Off=%x, RegNum=%x\n",
            RetryCnt, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, RegNum));
  }

  if (Status != TBT_STATUS_SUCCESS) {
    DEBUG ((DEBUG_ERROR,
            "RetimerThruHr::ReadIecs: Read IECS operation failed after several retries, Status - %d. Exiting...\n",
            Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  Status = RetimerPtr->Hr->ReadCioDevReg (
                             RetimerPtr->Hr,
                             ADAPTER_CONFIG_SPACE,
                             RetimerPtr->TbtPort,
                             TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_2,
                             DataPtr
                             );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR,
            "RetimerThruHr::ReadIecs: ERROR when reading from IECS rdata, Status - %d. Exiting...\n",
            Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  return TBT_STATUS_SUCCESS;

}

/**
  Send command to the local LC
  Operation:
    If there is data to write it writes it to the LC_SW_FW_MAILBOX_DATA_IN1 register.
    Writes the command to the LC_SW_FW_MAILBOX_DATA_IN0
    Issues command indication to LC in TBT_LC_SW_FW_MAILBOX_IN
    Wait for command to be cleared in LC_SW_FW_MAILBOX_DATA_IN0
    Check the Status and return


  @param RetimerPtr         Pointer to the current implementation of TBT_RETIMER interface
  @param ConfigurationSpace TBT device config space.
  @param Cmd                The command to local LC
  @param DataPtr              Pointer to a buffer where the write data

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
STATIC
TBT_STATUS
SendCommandToLocalLc (
  IN  RETIMER_THRU_HR *RetimerPtr,
  IN  UINT8           ConfigurationSpace,
  IN  UINT32          Cmd,
  OUT UINT32          *DataPtr OPTIONAL
  )
{
  TBT_STATUS   Status;
  UINT32       AccessCnt;
  UINT32       USB4CapRegWrite;
  UINT32       USB4CapRegRead;

  DEBUG ((DEBUG_INFO, "\nSendCommandToLocalLc::Sending Cmd - 0x%x to adapter %s of the HR\n",
    Cmd, RetimerPtr->TbtPort == FIRST_MASTER_LANE ? "A" : "B"));

  USB4CapRegWrite = 0;
  USB4CapRegRead  = 0;
  if (DataPtr != NULL) {
    Status = RetimerPtr->Hr->WriteCioDevReg (
                               RetimerPtr->Hr,
                               ConfigurationSpace,
                               RetimerPtr->TbtPort,
                               TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_2,
                               1,
                               DataPtr
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                            ERROR when writing 0x%x data to local LC, Status - %d. \
                            Exiting...\n", DataPtr, Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: Write USB4 port cap = %x\n", USB4CapRegWrite));
    USB4CapRegWrite = ((UINT32)1 << TBT_USB4_PORT_CAPABILITY_PND_OFFSET)
                    | ((UINT32)1 << TBT_USB4_PORT_CAPABILITY_WnR_OFFSET)
                    | ((UINT32)0 << TBT_USB4_PORT_CAPABILITY_TARGET_OFFSET)
                    | ((UINT32)4 << TBT_USB4_PORT_CAPABILITY_LENGTH_OFFSET)
                    | IECS_METADATA_ADDR;

    Status = RetimerPtr->Hr->WriteCioDevReg (
                               RetimerPtr->Hr,
                               ConfigurationSpace,
                               RetimerPtr->TbtPort,
                               TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1,
                               1,
                               &USB4CapRegWrite
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                           ERROR when writing 0x%x data to local LC, Status - %d. \
                           Exiting...\n", DataPtr, Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    Status = WaitForMsgOutTxDone (RetimerPtr, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, 2);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                            ERROR when writing 0x%x data to local LC, Status - %d. \
                            Exiting...\n", DataPtr, Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  }

  Status = RetimerPtr->Hr->WriteCioDevReg (
                             RetimerPtr->Hr,
                             ConfigurationSpace,
                             RetimerPtr->TbtPort,
                             TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_2,
                             1,
                             &Cmd
                             );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                          ERROR when writing 0x%x cmd to local LC, Status - %d.\
                          Exiting...\n", Cmd, Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  USB4CapRegWrite = ((UINT32)1 << TBT_USB4_PORT_CAPABILITY_PND_OFFSET)
                  | ((UINT32)1 << TBT_USB4_PORT_CAPABILITY_WnR_OFFSET)
                  | ((UINT32)4 << TBT_USB4_PORT_CAPABILITY_LENGTH_OFFSET)
                  | IECS_CMD_ADDR;

  Status = RetimerPtr->Hr->WriteCioDevReg (
                             RetimerPtr->Hr,
                             ConfigurationSpace,
                             RetimerPtr->TbtPort,
                             TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1,
                             1,
                             &USB4CapRegWrite
                             );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                          ERROR when writing 0x%x cmd to local LC, Status - %d.\
                          Exiting...\n", Cmd, Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  Status = WaitForMsgOutTxDone (RetimerPtr, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, 2);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                          ERROR when writing 0x%x data to local LC, Status - %d. \
                          Exiting...\n", DataPtr, Status  ));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  // TODO improve this with timer
  AccessCnt = 0;
  do {
    gBS->Stall (TBT_WAIT_TIME_BEFORE_NEXT_IECS_ACCESS);
    USB4CapRegWrite = ((UINT32)1 << TBT_USB4_PORT_CAPABILITY_PND_OFFSET)
                    | ((UINT32)4 << TBT_USB4_PORT_CAPABILITY_LENGTH_OFFSET)
                    | IECS_CMD_ADDR;

    Status = RetimerPtr->Hr->WriteCioDevReg (
                               RetimerPtr->Hr,
                               ConfigurationSpace,
                               RetimerPtr->TbtPort,
                               TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1,
                               1,
                               &USB4CapRegWrite
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                            ERROR when writing 0x%x data to local LC, Status - %d. \
                            Exiting...\n", DataPtr, Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    Status = WaitForMsgOutTxDone (RetimerPtr, TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_1, 2);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                            ERROR when writing 0x%x data to local LC, Status - %d. \
                            Exiting...\n", DataPtr, Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    Status = RetimerPtr->Hr->ReadCioDevReg (
                               RetimerPtr->Hr,
                               ConfigurationSpace,
                               RetimerPtr->TbtPort,
                               TBT_USB4_PORT_CAPABILITY_OFFSET + PORT_CS_2,
                               &USB4CapRegRead
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                            ERROR when waiting for command 0x%x to complete, \
                            Status - %d. Exiting...\n", Cmd, Status));

      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    AccessCnt++;
  } while (USB4CapRegRead == Cmd && AccessCnt < TBT_TOTAL_ACCESSES_WHILE_WAIT_FOR_IECS);

  if (USB4CapRegRead == Cmd) {  // Timeouted
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                          Local LC Couldn't perform 0x%x command - timeouted. \
                          Exiting...\n", Cmd));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  } else if (USB4CapRegRead != TBT_LC_CMD_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::SendCommandToLocalLc: \
                          Local LC reported error while performing 0x%x command - got error: 0x%x. \
                          Exiting...\n", Cmd, USB4CapRegRead));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  return TBT_STATUS_SUCCESS;
}

/** Capability Parser.
  Check for offset of USB4 Capability.

  @param[in] This pointer to the generic device interface - TBT_RETIMER

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation success
**/
STATIC
TBT_STATUS
CapabilityParsing (
  IN TBT_RETIMER *This
  )
{
  TBT_STATUS        Status;
  RETIMER_THRU_HR   *RetimerPtr;
  UINT32            Data;
  UINT8             NextCapability;
  UINT8             CapabilityID;
  UINT8             NoOfAttempts;
  //[-start-211209-IB19270002-add]//
  UINT32            CustomDelay;
  //[-end-211209-IB19270002-add]//

  ASSERT (This != NULL);
  DEBUG ((DEBUG_INFO, "RetimerThruHr::CapabilityParsing.\n"));
  if (This == NULL) {
    DEBUG ((DEBUG_ERROR, "RetimerThruHr::CapabilityParsing, NULL device provided.\n"));
    return TBT_STATUS_INVALID_PARAM;
  }

  RetimerPtr = (RETIMER_THRU_HR *)This->Impl;
  NoOfAttempts = 10;
  //[-start-211209-IB19270002-modify]//
  //[-start-211104-IB17040212-add]//
  CustomDelay = PcdGet32(PcdTbtRetimerCapParsingDelay);
  //[-end-211104-IB17040212-add]//
  //[-end-211209-IB19270002-modify]//
  // WA for handling TBT controller responding only after 8-10 seconds after first read register attempt
  while (NoOfAttempts--) {
    RetimerPtr->Hr->ReadCioDevReg ( RetimerPtr->Hr, DEVICE_CONFIG_SPACE, 0, 0x0, &Data );
    DEBUG ((DEBUG_INFO, "RetimerThruHr::CapabilityParsing: Data read 0x%08x.\n", Data));
      Data = Data & 0xFFFF;
      if ((Data == 0x8086) || (Data == 0x8087)) {
        break;
      }
    //[-start-211104-IB17040212-modify]//
    if (CustomDelay == 0) {
      gBS->Stall (2 * 1000 * 1000);
    } else {
      gBS->Stall(CustomDelay);
    }
	//[-end-211104-IB17040212-modify]//
  }

  Status = RetimerPtr->Hr->ReadCioDevReg (
                             RetimerPtr->Hr,
                             ADAPTER_CONFIG_SPACE,
                             RetimerPtr->TbtPort,
                             0x1,
                             &Data
                             );
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR,
            "RetimerThruHr::CapabilityParsing: ERROR when reading from Adapter config space, Status - %d. \
             Exiting...\n", Status ));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  NextCapability = 0x0;
  CapabilityID = 0x0;
  while (1) {
    NextCapability = ((UINT8)Data) & MASK (7, 0);
    DEBUG ((DEBUG_INFO, "RetimerThruHr::CapabilityParsing: Read NextCapability = %x\n", NextCapability));

    if (NextCapability == 0x0) {
      DEBUG ((DEBUG_ERROR, "RetimerThruHr::CapabilityParsing: ERROR, USB4 Port Capability was not found.\n"));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    Status = RetimerPtr->Hr->ReadCioDevReg (
                               RetimerPtr->Hr,
                               ADAPTER_CONFIG_SPACE,
                               RetimerPtr->TbtPort,
                               NextCapability,
                               &Data
                               );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR,
            "RetimerThruHr::CapabilityParsing: ERROR when reading from Adapter config space, Status - %d. \
             Exiting...\n", Status ));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    CapabilityID = (UINT8)(Data >> 8);
    DEBUG ((DEBUG_INFO, "RetimerThruHr::CapabilityParsing: Capability ID = 0x%x.\n", CapabilityID));

    if (CapabilityID == 0x6) {
      DEBUG ((DEBUG_INFO, "RetimerThruHr::CapabilityParsing: USB4 Port Capability found at offset 0x%x.\n", NextCapability ));
      TBT_USB4_PORT_CAPABILITY_OFFSET = (UINT16)NextCapability;
      return TBT_STATUS_SUCCESS;
    }
  }
  DEBUG ((DEBUG_ERROR, "RetimerThruHr::CapabilityParsing, failed to parse capabilities.\n"));
  return TBT_STATUS_NON_RECOVERABLE_ERROR;
}

/**
  Send enumeration command to local LC
  Need to repeat it for a number of times.
  Write to SW_FW_MAILBOX_IN0 corresponding to the port number the enum command.
  Then trigger cmd valid in SW_FW_MAILBOX_IN[0]

  Poll on the SW_FW_MAILBOX_IN0 register:
    if !ENUM -> Success
    if timeout exit
  Read SW_FW_MAILBOX_OUT0 reg
    if 0x1 error -> exit
    if 0x2 success
    else error -> exit

  @param[in] RetimerPtr pointer to the current implementation of TBT_RETIMER interface

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
TBT_STATUS
SendEnumCmd (
  IN RETIMER_THRU_HR *RetimerPtr
  )
{
  TBT_STATUS Status;
  UINTN      Index;

  DEBUG ((DEBUG_INFO, "\nTbtNvmDrvRetimerThruHr::Sending ENUM to adapter %s of the HR\n",
    RetimerPtr->TbtPort == FIRST_MASTER_LANE ? "A" : "B"));

  for (Index = 0; Index < TBT_TOTAL_ENUM_ACCESSES; Index++) {
    // Doing this 4 times because there is no indication whether the operation succeeded or not
    Status = SendCommandToLocalLc (RetimerPtr, ADAPTER_CONFIG_SPACE, TBT_IECS_CMD_ENUM, NULL);
    gBS->Stall(TBT_WAIT_TIME_BETWEEN_ENUM_ACCESSES);
  }
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::Enum wasn't sent successfully, got error...\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::Enum was sent successfully!\n"));

  return TBT_STATUS_SUCCESS;
}


/**
  Indicate to the device about the flow state

  @param[in] This The pointer to the interface, where this function is defined.
  @param[in] State Indicate to the device about flow state, as it might be required to perform some steps.

**/
STATIC
VOID
StateFromDriver (
  IN TBT_RETIMER      *This,
  IN DRIVER_STATE     State
  )
{
  if (State == AFTER_AUTH) {
    // Need to send ENUM again
    SendEnumCmd ((RETIMER_THRU_HR *)This->Impl);
  }
}

/**
  Send LSUP disable command (USUP) to the target retimer
  TODO: add description

  @param[in] RetimerPtr Pointer to the current implementation of TBT_RETIMER interface

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
STATIC
TBT_STATUS
SendLsupCmdDis (
  IN RETIMER_THRU_HR *RetimerPtr
  )
{
  TBT_STATUS Status;

  DEBUG ((DEBUG_INFO, "\nTbtNvmDrvRetimerThruHr::Sending USUP to adapter %s of the HR, retimer index - %d.\
    The required operation is disable\n",
    RetimerPtr->TbtPort == FIRST_MASTER_LANE ? "A" : "B", (UINT32)RetimerPtr->RetimerIndex));

  Status = TbtNvmDrvSendCmd ((VOID *)RetimerPtr->Comm, TBT_IECS_CMD_USUP, FALSE);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::USUP send is failed!\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::USUP was sent successfully!\n"));

  return TBT_STATUS_SUCCESS;
}

/**
  Send LSUP enable command to the target retimer
  TODO: add description

  @param[in] RetimerPtr pointer to the current implementation of TBT_RETIMER interface

  @retval Status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes
**/
STATIC
TBT_STATUS
SendLsupCmdEn (
  IN RETIMER_THRU_HR *RetimerPtr
  )
{
  TBT_STATUS Status;
  UINTN      Index;

  DEBUG ((DEBUG_INFO, "\nTbtNvmDrvRetimerThruHr::Sending LSUP to adapter %s of the HR, retimer index - %d. \
         The required operation is enable\n",
    RetimerPtr->TbtPort == FIRST_MASTER_LANE ? "A" : "B", (UINT32)RetimerPtr->RetimerIndex));

  for (Index = 0; Index < TBT_TOTAL_LSUP_ACCESSES; Index++) {
    // Doing this 3 times because there might be no indication
    // whether the operation succeeded or not on the first or second executions
    Status = TbtNvmDrvSendCmd ((VOID *)RetimerPtr->Comm, TBT_IECS_CMD_LSUP, TRUE);
    if (Status == TBT_STATUS_SUCCESS) {
      DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::LSUP command send had succeeded.\n"));
      return Status;
    }
    gBS->Stall (TBT_WAIT_TIME_BEFORE_NEXT_IECS_ACCESS);
  }
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::LSUP command send had failed\n"));
    return Status;
  }

  DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::LSUP was send successfully!\n"));
  return TBT_STATUS_SUCCESS;
}

/**
  Destroy the given connection
  This module destructor.

  @param[in] This      Pointer to the generic device interface - TBT_RETIMER
**/
STATIC
VOID
Dtor (
  IN TBT_RETIMER *This
  )
{
  TBT_STATUS        Status;
  RETIMER_THRU_HR   *RetimerPtr;
  UINT32            Data;

  DEBUG ((DEBUG_INFO, "TbtNvmDrvRetimerThruHr::Dtor was called.\n"));

  RetimerPtr = (RETIMER_THRU_HR *)This->Impl;
  Status = SendLsupCmdDis (RetimerPtr);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::Dtor Failed to send LSUP to retimer.\n"));
  }
  Data = 0x1;
  Status = SendCommandToLocalLc (RetimerPtr, ADAPTER_CONFIG_SPACE, TBT_IECS_CMD_LSEN, &Data);  // Enable LS back
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHr::Dtor Failed to enable LS in HR.\n"));
  }

  // Destroy all resources
  RetimerPtr->Hr->Dtor (RetimerPtr->Hr);
  TbtNvmDrvDeAllocateMem (RetimerPtr);
  TbtNvmDrvDeAllocateMem (This);
}

/**
  Constructs the Retimer thru HR module.
  Initializes all the internal data structures and initialize the required HW.

  @param[in] FirmwareType       The type of update firmware.
  @param[in] TbtPcieRootPort    The parent pcie root port of discrete TBT device.
  @param[in] TbtPort            The port connected to the target retimer.
  @param[in] RetimerIndex       The index of the retimer in the path from HR to the target retimer.
  @param[in] PcieRpConfig       PCIe root port config.
  @param[in] ForcePwrFunc       HR Force power function, applied if supplied.

  @retval A pointer to the device interface.

**/
TBT_RETIMER *
TbtNvmDrvRetimerThruHrCtor (
  IN UINT8          FirmwareType,
  IN PCIE_BDF       *TbtDmaPcieBdf,
  IN TBT_PORT       TbtPort,
  IN UINT32         RetimerIndex,
  IN PCIE_RP_CONFIG *PcieRpConfig,
  IN FORCE_PWR_HR   ForcePwrFunc OPTIONAL
  )
{
  EFI_STATUS        Status;
  RETIMER_THRU_HR   *RetimerPtr;
  TBT_RETIMER       *CommunicationPtr;
  TBT_HOST_ROUTER   *HrPtr;
  UINT32            Data;

  // Create all the resources
  RetimerPtr = TbtNvmDrvAllocateMem (sizeof (RETIMER_THRU_HR));
  if (RetimerPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: Failed to allocate memory for RETIMER_THRU_HR\n"));
    return NULL;
  }
  CommunicationPtr = TbtNvmDrvAllocateMem (sizeof (TBT_RETIMER));
  if (CommunicationPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: Failed to allocate memory for DEVICE_COMMUNICATION_INTERFACE\n"));
    goto free_retimer;
  }
  HrPtr = TbtNvmDrvHrCtor (FirmwareType, PcieRpConfig, TbtDmaPcieBdf, ForcePwrFunc);
  if (HrPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: Failed to create DMA\n"));
    goto free_communication;
  }

  // Assign communication interface implementation
  CommunicationPtr->Impl            = RetimerPtr;
  CommunicationPtr->WriteIecsReg    = WriteIecs;
  CommunicationPtr->ReadIecsReg     = ReadIecs;
  CommunicationPtr->Destroy         = Dtor;
  CommunicationPtr->IndicateState   = StateFromDriver;

  RetimerPtr->Comm = CommunicationPtr;

  if ( TbtPort == PORT_A ) {
    RetimerPtr->TbtPort = FIRST_MASTER_LANE;
  } else {
    RetimerPtr->TbtPort = SECOND_MASTER_LANE;
  }
  RetimerPtr->RetimerIndex = RetimerIndex;
  RetimerPtr->Hr = HrPtr;

  Status = CapabilityParsing (CommunicationPtr);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: The retimer could not perform Capability Parsing, \
                          Status %d. Exiting...\n", Status));
    goto free_hr;
  }

  // Send LS disable, to prevent LC to bring up the link
  Data = 0;
  Status = SendCommandToLocalLc (RetimerPtr, ADAPTER_CONFIG_SPACE, TBT_IECS_CMD_LSEN, &Data);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: The retimer could not perform LS disable, \
                          Status %d. Exiting...\n", Status));
    goto free_hr;
  }
  // Enumerate retimers on a path to the target retimer
  Status = SendEnumCmd (RetimerPtr);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: The retimer could not enumerate, Status %d. \
                          Exiting...\n", Status));
    goto free_hr;
  }

  Status = SendLsupCmdEn (RetimerPtr);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: The retimer could not perform LSUP, Status %d. \
                          Exiting...\n", Status));
    goto free_hr;
  }

  return CommunicationPtr;

free_hr:
  HrPtr->Dtor (HrPtr);
free_communication:
  TbtNvmDrvDeAllocateMem (CommunicationPtr);
free_retimer:
  TbtNvmDrvDeAllocateMem (RetimerPtr);
  return NULL;
}
