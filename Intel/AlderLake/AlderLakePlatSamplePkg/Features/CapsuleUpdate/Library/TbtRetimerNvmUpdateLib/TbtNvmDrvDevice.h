/** @file
Defines the device iecs communication interface

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#ifndef TBT_NVM_DRV_TARGET_DEVICE_
#define TBT_NVM_DRV_TARGET_DEVICE_

#include "TbtNvmDrvUtils.h"

typedef struct _TBT_RETIMER TBT_RETIMER;

typedef enum {
  AFTER_AUTH
} DRIVER_STATE;


/**
  Function to write IECS register in the target device

  @param[in] This        The pointer to the interface, where this function is defined.
  @param[in] RegNum      IECS register number
  @param[in] Data        The pointer to 32b data to write
  @param[in] Length      How many DWs to write

**/
typedef
TBT_STATUS
(*WRITE_IECS_REG) (
  IN TBT_RETIMER      *This,
  IN UINT8            RegNum,
  IN UINT32           *Data,
  IN UINT8            Length
  );

/**
  Function to read IECS register from the target device

  @param[in] This        The pointer to the interface, where this function is defined.
  @param[in] RegNum      IECS register number
  @param[in] DataLength The length of the Data buffer parameter in DWs
  @param[out] Data       The pointer to the buffer

**/
typedef
TBT_STATUS
(*READ_IECS_REG) (
  IN TBT_RETIMER      *This,
  IN UINT8            RegNum,
  IN UINT32           DataLength,
  OUT UINT32          *Data
  );

/**
  Indicate to the device about the flow state

  @param[in] This  The pointer to the interface, where this function is defined.
  @param[in] State Indicate to the device about flow state, as it might be required to perform some steps.

**/
typedef
VOID
(*INDICATE_STATE) (
  IN TBT_RETIMER     *This,
  IN DRIVER_STATE    State
  );


/**
  A function to disconnect the interface and release the implementation

  @param[in] This The pointer to the interface, where this function is defined.

**/
typedef
VOID
(*DESTROY) (
  IN TBT_RETIMER   *This
  );


// The interface to access the target device IECS registers.
struct _TBT_RETIMER {
  VOID                *Impl;
  WRITE_IECS_REG      WriteIecsReg;
  READ_IECS_REG       ReadIecsReg;
  DESTROY             Destroy;
  INDICATE_STATE      IndicateState;
};

#endif /* TBT_NVM_DRV_DEVICE_IECS_COMMUNICATION_IF_ */
