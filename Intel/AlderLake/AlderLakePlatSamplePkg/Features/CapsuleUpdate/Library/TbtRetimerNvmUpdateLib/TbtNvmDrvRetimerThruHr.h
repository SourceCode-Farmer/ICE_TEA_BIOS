/** @file
  Declares the interface for TbtNvmDrvRetimerThruHr class.
  This class is in charge of providing the way to access the retimer
  through TBT integrated HR.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef TBT_NVM_DRV_RETIMER_THRU_HR_
#define TBT_NVM_DRV_RETIMER_THRU_HR_

#include "TbtNvmDrvUtils.h"
#include "TbtNvmDrvDevice.h"
#include "TbtNvmDrvHr.h"


/// A struct to store the required fields during the operation
typedef struct {
  USB4_LANE         TbtPort;
  UINT32            RetimerIndex;
  TBT_HOST_ROUTER   *Hr;
  TBT_RETIMER       *Comm;
} RETIMER_THRU_HR;

/**
  Sends IECS command and waits for completion.

  @param[in] DevCom Pointer to the device interface
  @param[in] Cmd IECS CMD to send
  @param[in] ResponseRequired Is response required

  @retval TBT_STATUS_SUCCESS Command was successfully sent
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR A device error has occured.
  @retval TBT_STATUS_RETRY LC reported error, the command might be retried

**/
TBT_STATUS
TbtNvmDrvSendCmd (
  IN VOID          *DevCom,
  UINT32           Cmd,
  BOOLEAN          ResponseRequired
  );

/**
  Send enumeration command to local LC
  Need to repeate it for a number of times.
  Write to SW_FW_MAILBOX_IN0 corresponding to the port number the enum command.
  Then trigger cmd valid in SW_FW_MAILBOX_IN[0]

  Poll on the SW_FW_MAILBOX_IN0 register:
    if !ENUM -> Success
    if timeout exit
  Read SW_FW_MAILBOX_OUT0 reg
    if 0x1 error -> exit
    if 0x2 success
    else error -> exit

  @param[in] RetimerPtr pointer to the current implementation of TBT_RETIMER interface

  @retval status:
    TBT_STATUS_NON_RECOVERABLE_ERROR - need to terminate the execution
    TBT_STATUS_SUCCESS - indicate operation succes

**/
TBT_STATUS
SendEnumCmd (
  RETIMER_THRU_HR *RetimerPtr
  );

/**
  Constructs the Retimer thru HR module.
  Initializes all the internal data structures and initialize the required HW.

  @param[in] FirmwareType       The type of update firmware.
  @param[in] TbtPcieRootPort    The parent pcie root port of discrete TBT device.
  @param[in] TbtPort            The port connected to the target retimer.
  @param[in] RetimerIndex       The index of the retimer in the path from HR to the target retimer.
  @param[in] PcieRpConfig       PCIe root port config.
  @param[in] ForcePwrFunc       HR Force power function, applied if supplied.

  @retval A pointer to the device interface.

**/
TBT_RETIMER *
TbtNvmDrvRetimerThruHrCtor (
  IN UINT8          FirmwareType,
  IN PCIE_BDF       *TbtDmaPcieBdf,
  IN TBT_PORT       TbtPort,
  IN UINT32         RetimerIndex,
  IN PCIE_RP_CONFIG *PcieRpConfig,
  IN FORCE_PWR_HR   ForcePwrFunc OPTIONAL
  );

#endif /* TBT_NVM_DRV_RETIMER_THRU_HR_ */
