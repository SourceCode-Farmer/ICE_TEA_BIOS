/** @file
  Force power on YFL router

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#include <Library/UefiBootServicesTableLib.h>
#include "TbtNvmDrvYflLib.h"


#define FORCE_PWR_REG_OFFSET 0xFC
#define FORCE_PWR_REG_BIT_INDEX 1
#define FW_READY_REG_OFFSET 0xC8
#define FW_READY_REG_BIT_INDEX 31

#define TOTAL_WAIT_FOR_PWR_ON_MS 10000

/**
  TbtNvmDrvYflForcePwrFunc function to perform force power on YFL router
  Operation:
    Read modify write PCI CFG space, byte offset - 0xfc[1] = 1
    Wait for PCI CFG space, byte offset - 0xC8[31] to be set.
**/
TBT_STATUS
TbtNvmDrvYflForcePwrFunc (
  IN       EFI_PCI_IO_PROTOCOL   *PciIoProto,
  IN       BOOLEAN               Enable,
  IN  OUT  BOOLEAN               *TBTControllerWasPowered
  )
{
  EFI_STATUS Status;
  UINT32     Reg;
  UINT32     Index;

  // Force power
  if (Enable) {
    DEBUG ((DEBUG_INFO, "TbtNvmDrvYflForcePwrFunc: Performing force power on TBT Controller\n"));
  } else {
    DEBUG ((DEBUG_INFO, "TbtNvmDrvYflForcePwrFunc: Disabling force power on TBT Controller\n"));
  }

  if (PciIoProto == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvYflForcePwrFunc: PciIoProto parameter is NULL\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  if (TBTControllerWasPowered == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvYflForcePwrFunc: TBTControllerWasPowered parameter is NULL\n"));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }

  // Check if TBT Controller was already powered in SW CM mode.
  if (*TBTControllerWasPowered == TRUE) {
    return TBT_STATUS_SUCCESS;
  }

  Status = PciIoProto->Pci.Read (PciIoProto, EfiPciIoWidthUint32, FORCE_PWR_REG_OFFSET, 1, &Reg);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvYflForcePwrFunc: Couldn't read from PCIe register - 0x%x, Status - %r\n", FORCE_PWR_REG_OFFSET, Status));
    return TBT_STATUS_NON_RECOVERABLE_ERROR;
  }
  if (Enable) {
    // Check if TBT Controller was already powered
    if (Reg & MASK (FORCE_PWR_REG_BIT_INDEX, FORCE_PWR_REG_BIT_INDEX)) {
      *TBTControllerWasPowered = TRUE;
      return TBT_STATUS_SUCCESS;
    }
    Reg = Reg | BIT (FORCE_PWR_REG_BIT_INDEX);
  }
  else {
    Reg = Reg & ~BIT (FORCE_PWR_REG_BIT_INDEX);
  }
  Status = PciIoProto->Pci.Write (PciIoProto, EfiPciIoWidthUint32, FORCE_PWR_REG_OFFSET, 1, &Reg);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvYflForcePwrFunc: Couldn't write to PCIe register - 0x%x, Status - %r\n", FORCE_PWR_REG_OFFSET, Status));
    return Status;
  }

  // In case the operation is disable, no need to verify FW is ready.
  if (!Enable) {
    return TBT_STATUS_SUCCESS;
  }

  // Wait for FW is ready
  for (Index = 0; Index < TOTAL_WAIT_FOR_PWR_ON_MS; Index++) {
    Status = PciIoProto->Pci.Read (PciIoProto, EfiPciIoWidthUint32, FW_READY_REG_OFFSET, 1, &Reg);
    if (EFI_ERROR (Status)) {
      continue;
    }
    Reg = Reg & MASK (FW_READY_REG_BIT_INDEX, FW_READY_REG_BIT_INDEX);
    if (Reg != 0) {
      DEBUG ((DEBUG_INFO, "FW of TBT controller is ready.\n"));
      *TBTControllerWasPowered = TRUE;
      return TBT_STATUS_SUCCESS;
    }
      DEBUG ((DEBUG_WARN, "FW of TBT controller is not yet ready.\n"));
    gBS->Stall (1000);   // Wait 1ms
  }
  DEBUG ((DEBUG_ERROR, "TbtNvmDrvYflForcePwrFunc: Waiting too much on power on. Exiting...\n"));
  return TBT_STATUS_NON_RECOVERABLE_ERROR;

}