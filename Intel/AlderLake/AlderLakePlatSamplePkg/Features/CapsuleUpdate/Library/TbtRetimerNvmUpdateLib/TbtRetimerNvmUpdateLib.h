/** @file
  Defines the device communication interface.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef _TBT_RETIMER_NVM_UPDATE_LIB_
#define _TBT_RETIMER_NVM_UPDATE_LIB_

#define ROUTER_CMD_ADDR                26   //Opcode
#define ROUTER_METADATA_ADDR           25   //Metadata
#define ROUTER_DATA_ADDR                9   //Data

#define OPCODE_NVM_WRITE_CMD         0x20
#define OPCODE_NVM_AUTH_CMD          0x21
#define OPCODE_NVM_READ_CMD          0x22
#define OPCODE_NVM_SETOFFSET_CMD     0x23
#define OPCODE_NVM_GETSECTORSIZE_CMD 0x25

#define IECS_CMD_ADDR                   8
#define IECS_METADATA_ADDR              9
#define IECS_MSG_OUT_RDATA             18

// The commands below are 4cc encoded
#define IECS_BOPS_CMD          0x53504f42 /* NVM_SET_OFFSET */
#define IECS_BLKW_CMD          0x574b4c42 /* NVM_BLOCK_WRITE */
#define IECS_AUTH_CMD          0x48545541 /* NVM_AUTH_WRITE */
#define IECS_PCYC_CMD          0x43594350 /* ? Not in CIO spec */
#define IECS_AFRR_CMD          0x52524641 /* NVM_READ */

#define GET_NVM_SECTOR_SIZE    0x53534E47 /* GET_NVM_SECTOR_SIZE */
#define NVM_SET_OFFSET         0x53504f42 /* NVM_SET_OFFSET */
#define NVM_BLOCK_WRITE        0x574b4c42 /* NVM_BLOCK_WRITE */
#define NVM_AUTH_WRITE         0x48545541 /* NVM_AUTH_WRITE */
#define NVM_READ               0x52524641 /* NVM_READ */

// 4cc error indications
#define IECS_USB4_CMD_NOTSUPPORTED 0x444D4321
#define IECS_USB4_CMD_ERR          0x20525245

#define TBT_IECS_CMD_USUP             0x50555355 /* UNSET_INBOUND_LSTX */ /* WA to mitigate no response issue */

// This is number of accesses to remote IECS CMD before declaring timeout
#define TBT_TOTAL_OF_ACCESSES_TO_CMD_UNTIL_TIMEOUT 2000

#define TBT_NVM_MAX_DWS_TO_WRITE 16

#define TBT_TOTAL_NUM_OF_RECOVERIES_DURING_IMAGE_WRITE 1

#define TBT_CMD_VALID              BIT(31)
#define TBT_CMD_NOTSUPPORTED       BIT(30)
#define TBT_METADATA_VALID         BIT(24)

#define ASCII_CODE_LOWERCASE_N 110
#define ASCII_CODE_LOWERCASE_Y 121

// Success status from LC
#define TBT_AUTH_SUCCCESS 1

#define TBT_NVM_VERSION_OFFSET_DW 2

#define TBT_TIME_TO_WAIT_FOR_AUTH_US 1000 * 1000 * 5              // 5s

#define TBT_NVM_VERSION_MASK MASK(23, 0)

// Offset in DROM to Model Revison fields
#define TBT_TBT3_DROM_MODEL_REVISION_OFFSET 0x14
#define TBT_SIMPLIFIED_DROM_MODEL_REVISION_OFFSET 0x4 // It is actually at offset 0x5 but we need to read from DWORD aligned address.

#define UNUSED_ADAPTER_ENTRY_TYPE 2
#define DP_ADAPTER_ENTRY_TYPE 5

#define TBT3_LANE_ADAPTER_ENTRY_TYPE 8
#define TBT3_PCIE_UPSTREAM_ADAPTER_ENTRY_TYPE 11
#define TBT3_PCIE_DOWNSTREAM_ADAPTER_ENTRY_TYPE 3

#define USB4_DROM_GENERIC_ENTRY_TYPE_OFFSET 1
#define USB4_DROM_GENERIC_ENTRY_DATA_OFFSET 2

#define USB4_DROM_GENERIC_ENTRY_ASCII_VENDOR_NAME_TYPE 0x1
#define USB4_DROM_GENERIC_ENTRY_ASCII_MODEL_NAME_TYPE 0x2

#endif
