/** @file
  Implementations of the CRC-32C standard for Software Connection Manager

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include "Crc32c.h"

UINT32 mCrc32cTable[256];

/**
  This internal function reverses bits for 32bit data.

  @param  Value                 The data to be reversed.

  @return                       Data reversed.

**/
UINT32
ReverseBits (
  UINT32  Value
  )
{
  UINTN   Index;
  UINT32  NewValue;

  NewValue = 0;
  for (Index = 0; Index < 32; Index++) {
    if ((Value & (1 << Index)) != 0) {
      NewValue = NewValue | (1 << (31 - Index));
    }
  }

  return NewValue;
}

/**
  Generate a CRC-32C table that can speed up CRC calculation by table look up
**/
VOID
Crc32cInit(
  VOID
  )
{
  UINT32 TableEntry;
  UINT32 Index;
  UINT32 CrcVal;

  for (TableEntry = 0; TableEntry < 256; TableEntry++) {
    CrcVal = ReverseBits ((UINT32) TableEntry);
    for (Index = 0; Index < 8; Index++) {
      if ((CrcVal & 0x80000000) != 0) {
        CrcVal = (CrcVal << 1) ^ CRC32C_POLY_NORMAL;
      } else {
        CrcVal = CrcVal << 1;
      }
    }

    mCrc32cTable[TableEntry] = ReverseBits (CrcVal);
  }
}

/**
  Calculate CRC value of the input data based on CRC-32C standard

  @param[in] Data     - Pointer to source data for calculating CRC codes
  @param[in] DataSize - Data length in bytes of source data
  @param[out] CrcOut  - Calculated CRC codes for source data

  @retval EFI_SUCCESS - CRC calculation success
  @retval EFI_INVALID_PARAMETER - Invalid input parameter is specified
**/
EFI_STATUS
CalCrc32c(
  UINT8 *Data,
  UINT32 DataSize,
  UINT32 *CrcOut
  )
{
  UINT32  Crc;
  UINT32  Index;
  UINT8 *Ptr;

  if (Data == NULL || DataSize == 0 || CrcOut == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Crc = CRC32C_INITIAL_REMAINDER;
  for (Index = 0, Ptr = Data; Index < DataSize; Index++, Ptr++) {
    Crc = (Crc >> 8) ^ mCrc32cTable[(UINT8)Crc ^ *Ptr];
  }

  *CrcOut = Crc ^ 0xffffffff;
  return EFI_SUCCESS;
}
