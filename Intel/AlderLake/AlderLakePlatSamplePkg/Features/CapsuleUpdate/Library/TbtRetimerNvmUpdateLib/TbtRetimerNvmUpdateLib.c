/** @file
  TbtRetimerNvmUpdateLib instance to support TBT Retimer firmware update

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <PiDxe.h>

#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/TcssTbtRetimerNvmUpdateLib.h>

#include "TbtNvmDrvRetimerThruHr.h"
#include "TbtNvmDrvYflLib.h"
#include "TbtRetimerNvmUpdateLib.h"

static UPDATE_TARGET_TYPE mUpdateTargetType = TARGET_RETIMER;    // TARGET_TBT_HOST or TARGET_RETIMER

STATIC
TBT_STATUS
TbtNvmDrvReadIECS_9 (
  IN TBT_RETIMER *DevCom
  )
{
  TBT_STATUS  Status;
  UINT32      ReadData;
  UINT32      AccessCount;

  ASSERT (DevCom != NULL);
  if (DevCom == NULL) {
    return TBT_STATUS_INVALID_PARAM;
  }

  AccessCount = 0;

  // Wait for command to be completed
  do {
    Status = DevCom->ReadIecsReg (DevCom, IECS_METADATA_ADDR, 1, &ReadData);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((
        DEBUG_ERROR,
        "TbtNvmDrvReadIECS_9: Got an error while reading from IECS_9 register, Status - %d, AccessCount = %d. Exiting...\n",
        Status,
        AccessCount
        ));

      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    AccessCount++;
  } while (AccessCount < TBT_TOTAL_OF_ACCESSES_TO_CMD_UNTIL_TIMEOUT);

  DEBUG ((DEBUG_INFO, "TbtNvmDrvReadIECS_9: AccessCount = %d, ReadData = %x\n", AccessCount, ReadData));
  return TBT_STATUS_SUCCESS;
}

/**
  Waits for completion of IECS command by polling the IECS CMD remote register

  @param[in] DevCom Pointer to the device interface
  @param[in] Cmd IECS CMD which was send

  @retval TBT_STATUS_SUCCESS Command was successfully sent
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR A device error has occured.
  @retval TBT_STATUS_RETRY LC reported error, the command might be retried
**/
STATIC
TBT_STATUS
TbtNvmDrvWaitForCmdCpl (
  IN VOID          *DevCom,
  IN UINT32        Cmd
  )
{
  TBT_STATUS      Status;
  UINT32          ReadData;
  UINT32          AccessCount;
  TBT_RETIMER     *DevComRetimer;
  TBT_HOST_ROUTER *DevComHost;

  ASSERT (DevCom != NULL);
  ASSERT (Cmd != 0);

  if (DevCom == NULL || Cmd == 0) {
    return TBT_STATUS_INVALID_PARAM;
  }
  AccessCount = 0;
  DEBUG ((DEBUG_INFO, "TbtNvmDrvWaitForIecsCmdCpl: TbtNvmDrvWaitForCmdCpl() Called.\n"));

  if (mUpdateTargetType == TARGET_RETIMER) {
    DevComRetimer = (TBT_RETIMER *)DevCom;
    // Wait for command to be completed
    do {
      DEBUG ((DEBUG_INFO, "TbtNvmDrvWaitForIecsCmdCpl: Check reg for completion\n"));
      // Bypass the completion check for IECS_USUP_CMD,
      // because the operation kills the SBTx from the retimer side,
      // so it is most likely that we will not get response for this command
      // which still need to be considered as a success.
      if ((Cmd == TBT_IECS_CMD_USUP) && (AccessCount == 10)) {
        break;
      }
      Status = DevComRetimer->ReadIecsReg (DevComRetimer, IECS_CMD_ADDR, 1, &ReadData);
      if (TBT_STATUS_ERR (Status)) {
        DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForIecsCmdCpl: Got an error while reading from IECS_CMD register, Status - %d, AccessCount = %d, Cmd = %x. Exiting...\n", Status, AccessCount, Cmd));
        return TBT_STATUS_NON_RECOVERABLE_ERROR;
      }
      AccessCount++;
    } while ((ReadData == Cmd) && (AccessCount < TBT_TOTAL_OF_ACCESSES_TO_CMD_UNTIL_TIMEOUT));

    if (ReadData == Cmd) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForIecsCmdCpl: Remote IECS CMD is timeouted. AccessCount = %d, ReadData = %x Exiting...\n", AccessCount, ReadData));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    if (ReadData == IECS_USB4_CMD_NOTSUPPORTED) {  // !CMD (command not supported)
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForIecsCmdCpl: Command not supported. AccessCount = %d, ReadData = %x, Cmd = %x\n", AccessCount, ReadData, Cmd));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    if (ReadData == IECS_USB4_CMD_ERR) { // ERR
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForIecsCmdCpl: Got error from remote LC while performing command = %x, AccessCount = %d, ReadData = %x\n", Cmd, AccessCount, ReadData));
      TbtNvmDrvReadIECS_9 (DevComRetimer);
      return TBT_STATUS_RETRY;
    }
  } else {
    DevComHost = (TBT_HOST_ROUTER *)DevCom;
    // Wait for command to be completed
    do
    {
      Status = DevComHost->ReadCioDevReg (DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_CMD_ADDR, &ReadData);

      if (TBT_STATUS_ERR (Status)) {
        DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForCmdCpl: Got an error while reading from CMD register, Status - %d, AccessCount = %d, Cmd = %x. Exiting...\n", Status, AccessCount, Cmd));
        return TBT_STATUS_NON_RECOVERABLE_ERROR;
      }

      AccessCount++;
    } while ((ReadData & TBT_CMD_VALID) != 0 && AccessCount < TBT_TOTAL_OF_ACCESSES_TO_CMD_UNTIL_TIMEOUT );

    if ((ReadData & TBT_CMD_VALID) != 0) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForCmdCpl: CMD is timeouted. AccessCount = %d, ReadData = %x Exiting...\n", AccessCount, ReadData));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    if ((ReadData & TBT_CMD_NOTSUPPORTED) != 0) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForCmdCpl: CMD is timeouted. AccessCount = %d, ReadData = %x Exiting...\n", AccessCount, ReadData));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    if ((ReadData != Cmd) != 0) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvWaitForCmdCpl: CMD 0x%x encountered an error. AccessCount = %d, ReadData = %x Exiting...\n", Cmd, AccessCount, ReadData));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  }
  return TBT_STATUS_SUCCESS;
}

/**
  Sends IECS command and waits for completion.

  @param[in] DevCom Pointer to the device interface
  @param[in] Cmd IECS CMD to send
  @param[in] ResponseRequired Is response required

  @retval TBT_STATUS_SUCCESS Command was successfully sent
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR A device error has occured.
  @retval TBT_STATUS_RETRY LC reported error, the command might be retried

**/
TBT_STATUS
TbtNvmDrvSendCmd (
  IN VOID          *DevCom,
  UINT32           Cmd,
  BOOLEAN          ResponseRequired
  )
{
  TBT_STATUS       Status;
  TBT_RETIMER      *DevComRetimer;
  TBT_HOST_ROUTER  *DevComHost;
  UINT32           Opcode;

  DEBUG ((DEBUG_VERBOSE, "Sending CMD 0x%x\n", Cmd));

  if (DevCom == NULL || Cmd == 0) {
    return TBT_STATUS_INVALID_PARAM;
  }

  if (mUpdateTargetType == TARGET_RETIMER) {
    DevComRetimer = (TBT_RETIMER *)DevCom;
    // Write command
    Status = DevComRetimer->WriteIecsReg (DevComRetimer, IECS_CMD_ADDR, &Cmd, 1);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvSendCmd: Fail to write to IECS CMD reg, Status = %d.\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  } else {
    DevComHost = (TBT_HOST_ROUTER *)DevCom;
    Opcode     = Cmd | TBT_CMD_VALID;
    Status = DevComHost->WriteCioDevReg (DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_CMD_ADDR, 1, &Opcode);

    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvSendCmd: Fail to write to  CMD reg, Status = %d.\n", Status));
      DEBUG ((DEBUG_ERROR, "                  Command was 0x%x. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  }

  if (ResponseRequired) {
    // Wait for command to be completed
    Status = TbtNvmDrvWaitForCmdCpl (DevCom, Cmd);
  }

  return Status;
}

/**
  Writes data block to the target device's NVM

  @param[in] DevCom Pointer to the device interface
  @param[in] Data to send
  @param[in] Length data length in DW

  @retval TBT_STATUS_SUCCESS Command was successfully sent
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR A device error has occured.
  @retval TBT_STATUS_RETRY LC reported error, the command might be retried
**/
STATIC
TBT_STATUS
TbtNvmDrvDeviceWrBlk (
  IN VOID           *DevCom,
  UINT32            *Data,
  UINT8             Length
  )
{
  TBT_STATUS       Status;
  TBT_RETIMER      *DevComRetimer;
  TBT_HOST_ROUTER  *DevComHost;

  ASSERT (DevCom != NULL);
  ASSERT (Length <= TBT_NVM_MAX_DWS_TO_WRITE);
  ASSERT (Length > 0);
  ASSERT (Data != NULL);
  if ((DevCom == NULL) || (Length == 0) || (Length > TBT_NVM_MAX_DWS_TO_WRITE) || (Data == NULL)) {
    return TBT_STATUS_INVALID_PARAM;
  }

  DEBUG ((DEBUG_VERBOSE, "Writing block with length - 0x%x\n", Length));

  if (mUpdateTargetType == TARGET_RETIMER) {
    DevComRetimer = (TBT_RETIMER *)DevCom;
    // Write data
    Status = DevComRetimer->WriteIecsReg (DevComRetimer, IECS_MSG_OUT_RDATA, Data, Length);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrBlk: ERROR! The data to REMOTE_WDATA register wasn't written. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    // Send the command
    Status = TbtNvmDrvSendCmd (DevCom, IECS_BLKW_CMD, TRUE);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrBlk: ERROR! Sending IECS_BLKW_CMD command failed. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  } else {
    DevComHost = (TBT_HOST_ROUTER *)DevCom;
    // Write data
    Status = DevComHost->WriteCioDevReg (DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_DATA_ADDR, Length, Data);

    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrBlk: ERROR! The data to REMOTE_WDATA register wasn't written. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    // Send the command
    Status = TbtNvmDrvSendCmd (DevCom, OPCODE_NVM_WRITE_CMD, TRUE);

    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrBlk: ERROR! Sending OPCODE_NVM_WRITE_CMD command failed. Status - %d. Exiting...\n", Status));
    }
  }
  DEBUG ((DEBUG_VERBOSE, "Finished writing block with length - 0x%x. Status - %d\n", Length, Status));
  return Status;
}

/**
  Writes offset in the NVM, to start from it

  @param[in] DevCom Pointer to the device interface
  @param[in] OffsetInDW in NVM in DW resolution

  @retval TBT_STATUS_SUCCESS Command was successfully sent
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR A device error has occured.
  @retval TBT_STATUS_RETRY LC reported error, the command might be retried
**/
STATIC
TBT_STATUS
TbtNvmDrvDeviceWrOffset (
  IN VOID   *DevCom,
  IN UINT32 OffsetInDW
  )
{
  TBT_STATUS         Status;
  UINT32             OffsetInB;
  TBT_RETIMER        *DevComRetimer;
  TBT_HOST_ROUTER    *DevComHost;

  ASSERT (DevCom != NULL);
  if (DevCom == NULL) {
    return TBT_STATUS_INVALID_PARAM;
  }

  DEBUG ((DEBUG_VERBOSE, "Write device OffsetInDW - 0x%x\n", OffsetInDW));

  OffsetInB = OffsetInDW << 2;

  if (mUpdateTargetType == TARGET_RETIMER) {
    DevComRetimer = (TBT_RETIMER *)DevCom;
    // Write data
    Status = DevComRetimer->WriteIecsReg (DevComRetimer, IECS_METADATA_ADDR, &OffsetInB, 1);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrOffset: ERROR! Writing data to IECS_METADATA_ADDR register failed. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Send the command
    Status = TbtNvmDrvSendCmd (DevCom, IECS_BOPS_CMD, TRUE);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrOffset: ERROR! Sending command IECS_BOPS_CMD failed. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  } else {
    DevComHost = (TBT_HOST_ROUTER *)DevCom;
    // Write data
    Status = DevComHost->WriteCioDevReg ( DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_METADATA_ADDR, 1, &OffsetInB ); //metadata

    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrOffset: ERROR! The data to Metadata register wasn't written. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Send the command
    Status = TbtNvmDrvSendCmd ( DevCom, OPCODE_NVM_SETOFFSET_CMD, TRUE );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceWrOffset: ERROR! The data to Metadata register wasn't written. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  }
  DEBUG ((DEBUG_VERBOSE, "Finished writing device OffsetInDW - 0x%x (%d)\n", OffsetInDW, Status));
  return Status;
}

/**
  Initiate Auth at the device, wait for it to complete and check the status

  @param[in] DevCom Pointer to the device interface

  @retval TBT_STATUS_SUCCESS If auth passed
  @retval TBT_STATUS_TIMEOUT If device is not responding
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR On any other communication error
  @retval TBT_STATUS_GENERIC_ERR If auth is failed

**/
STATIC
TBT_STATUS
TbtNvmDrvDeviceExecAuth (
  IN VOID *DevCom
  )
{
  UINT32            ReadData;
  TBT_STATUS        Status;
  UINT32            WriteData;
  TBT_RETIMER       *DevComRetimer;
  RETIMER_THRU_HR   *RetimerPtr;
  TBT_HOST_ROUTER   *DevComHost;
  UINT32            Opcode;
  UINT32            Cmd;

  DEBUG ((DEBUG_INFO, "Sending Auth command\n"));
  if (DevCom == NULL) {
    return TBT_STATUS_INVALID_PARAM;
  }

  if (mUpdateTargetType == TARGET_RETIMER) {
    DevComRetimer = (TBT_RETIMER *)DevCom;
    RetimerPtr = (RETIMER_THRU_HR *)DevComRetimer->Impl;
    WriteData = 0;
    Status = DevComRetimer->WriteIecsReg (DevComRetimer, IECS_METADATA_ADDR, &WriteData, 1); // Turn Auth on
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceExecAuth: Got an error while writing to IECS_DATA register, status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    WriteData = IECS_AUTH_CMD;
    Status = DevComRetimer->WriteIecsReg (DevComRetimer, IECS_CMD_ADDR, &WriteData, 1); // Send Auth command
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceExecAuth: Got an error while writing to IECS_CMD register, status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    DEBUG ((DEBUG_INFO,
            "Waiting %d seconds for authentication to complete...\n",
            TBT_TIME_TO_WAIT_FOR_AUTH_US / 1000000));
    gBS->Stall (TBT_TIME_TO_WAIT_FOR_AUTH_US);

    // Enumerate retimers on a path to the target retimer
    Status = SendEnumCmd (RetimerPtr);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceExecAuth: The retimer could not enumerate, status %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    Status = TbtNvmDrvWaitForCmdCpl (DevCom, IECS_AUTH_CMD);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR,
              "TbtNvmDrvDeviceExecAuth: Authentication failed!!! status from LC - %d. Exiting...\n",
              Status));

      Status = DevComRetimer->ReadIecsReg (DevComRetimer, IECS_METADATA_ADDR, 1, &ReadData);
      if (TBT_STATUS_ERR (Status)) {
        DEBUG ((DEBUG_ERROR,
                "TbtNvmDrvDeviceExecAuth: Got an error while reading from IECS_DATA register, status - %d. Exiting...\n",
                Status));
        return TBT_STATUS_NON_RECOVERABLE_ERROR;
      }
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceExecAuth: Auth has failed with metadata = %x. Exiting...\n", ReadData));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
  } else {
    DevComHost = (TBT_HOST_ROUTER *)DevCom;
    Cmd = OPCODE_NVM_AUTH_CMD;
    Opcode = Cmd | TBT_CMD_VALID;
    WriteData = 0x0;

    // Write command
    Status = DevComHost->WriteCioDevReg ( DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_METADATA_ADDR, 1, &WriteData ); //turn auth on

    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceExecAuth: ERROR! The data to Metadata register wasn't written. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    Status = DevComHost->WriteCioDevReg ( DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_CMD_ADDR, 1, &Opcode );

    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtNvmDrvDeviceExecAuth: Fail to write to  CMD reg, Status = %d.\n", Status));
      DEBUG ((DEBUG_ERROR, "                  Command was 0x%x. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    DEBUG ((DEBUG_INFO, "Waiting %d seconds for authentication to complete...\n", TBT_TIME_TO_WAIT_FOR_AUTH_US / 1000000));
    gBS->Stall ( TBT_TIME_TO_WAIT_FOR_AUTH_US );
  }
  DEBUG ((DEBUG_INFO, "TbtNvmDrvDeviceExecAuth: Authentication success!\n"));
  return TBT_STATUS_SUCCESS;
}

/** Reading data from flash memory.

  @param[in]   DevCom     Pointer to the device interface
  @param[in]   Offset     TBT NVM offset
  @param[in]   DataLength How many DWORDS we want to read
  @param[out]  RdData     Pointer to read data

  @retval EFI_INVALID_PARAMETER
  @retval TBT_STATUS_NON_RECOVERABLE_ERROR
  @retval EFI_SUCCESS

**/
STATIC
TBT_STATUS
TbtDrvReadDwFromNvm (
  IN VOID           *DevCom,
  IN UINT32         Offset,
  IN UINT32         DataLength,
  OUT UINT32        *RdData
  )
{
  TBT_STATUS      Status;
  TBT_HOST_ROUTER *DevComHost;
  UINT32          WriteData;

  ASSERT (DevCom != NULL);
  if (DevCom == NULL) {
    return TBT_STATUS_INVALID_PARAM;
  }

  DEBUG ((DEBUG_INFO, "Reading DW from NVM, offset 0x%x\n", Offset));
  if (mUpdateTargetType == TARGET_RETIMER) {
    TBT_RETIMER *DevComRetimer = (TBT_RETIMER *)DevCom;

    // Write Offset to IECS_DATA register
    Status = DevComRetimer->WriteIecsReg (DevComRetimer, IECS_METADATA_ADDR, &Offset, 1);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtDrvReadDwFromNvm: ERROR! Writing data to IECS_METADATA_ADDR register failed. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Send the command
    Status = TbtNvmDrvSendCmd (DevCom, IECS_AFRR_CMD, TRUE);
    if (TBT_STATUS_ERR (Status)) {
     DEBUG ((DEBUG_ERROR, "TbtDrvReadDwFromNvm: ERROR! Sending command IECS_AFRR_CMD failed. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Read data from MSG_OUT
    Status = DevComRetimer->ReadIecsReg (DevComRetimer, IECS_MSG_OUT_RDATA, (DataLength<16?DataLength:16), RdData);
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtDrvReadDwFromNvm: ERROR! Failed to read data from IECS_DATA register. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

  } else {
    DevComHost = (TBT_HOST_ROUTER *)DevCom;
    WriteData = TBT_METADATA_VALID | Offset;
    // Write Offset to Metadata register
    Status = DevComHost->WriteCioDevReg ( DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_METADATA_ADDR, 1, &WriteData );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtDrvReadDwFromNvm: ERROR! The data to DATA register wasn't written. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }
    // Send the command
    Status = TbtNvmDrvSendCmd ( DevCom, OPCODE_NVM_READ_CMD, TRUE );


    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtDrvReadDwFromNvm: ERROR! The command wasn't send. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

    // Read data from DATA register
    Status = DevComHost->ReadCioDevReg ( DevComHost, DEVICE_CONFIG_SPACE, 0, ROUTER_DATA_ADDR, RdData );
    if (TBT_STATUS_ERR (Status)) {
      DEBUG ((DEBUG_ERROR, "TbtDrvReadDwFromNvm: ERROR! The read from RDATA has failed. Status - %d. Exiting...\n", Status));
      return TBT_STATUS_NON_RECOVERABLE_ERROR;
    }

  }
  DEBUG ((DEBUG_INFO, "Finished reading DW from NVM offset - 0x%x\n", Offset));

  return TBT_STATUS_SUCCESS;
}


/** Reads NVM version from the target device

  @param[in]  DevCom Pointer to the device interface
  @param[out] Pointer where to put the read version

  @retval EFI_INVALID_PARAMETER
  @retval EFI_DEVICE_ERROR
  @retval EFI_SUCCESS

**/
EFI_STATUS
TbtDrvReadNvmVersion (
  IN VOID          *DevCom,
  OUT UINT32       *Version
  )
{
  TBT_STATUS       Status;

  ASSERT (DevCom != NULL);
  if (DevCom == NULL) {
    return TBT_STATUS_INVALID_PARAM;
  }

  DEBUG ((DEBUG_INFO, "Reading nvm version\n"));

  Status = TbtDrvReadDwFromNvm (DevCom, TBT_NVM_VERSION_OFFSET_DW << 2, 1 /* in DWs */, Version);
  if (TBT_STATUS_ERR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtDrvReadNvmVersion: ERROR! Couldn't read from NVM. Status %d\n", Status));
    return EFI_DEVICE_ERROR;
  }

  DEBUG ((DEBUG_INFO, "Finished reading NVM version - 0x%x\n", *Version));

  return EFI_SUCCESS;
}

/**
  Create and initialize the required data structure to access a Retimer device.
  Besides, set up a communication with it if needed.

  @param[in]   FirmwareType       The type of update firmware.
  @param[in]   PcieRpType         PCIE root port type.
  @param[in]   PchPcieRootPort    The parent pcie root port of discrete TBT device.
  @param[in]   DevAddress         The address info of target Retimer device.
  @param[out]  RetimerDevice      Pointer to a Retimer device instance corresponding to DevAddress.

  @retval EFI_SUCCESS             RetimerDevice is created successfully and is ready to accept commands.
  @retval Others                  An error occurred attempting to access the Retimer device.

**/
EFI_STATUS
EFIAPI
CreateRetimerDevInstance (
  IN   UINT8                                          FirmwareType,
  IN   UINT8                                          PcieRpType,
  IN   UINT8                                          PcieRootPort,
  IN   RETIMER_DEV_ADDRESS                            *DevAddress,
  OUT  RETIMER_DEV_INSTANCE                           *RetimerDevice
  )
{
  EFI_STATUS           Status;
  TBT_RETIMER          *Device;
  PCIE_BDF             *TbtDmaPciLocation;
  TBT_PORT             TBT_TARGET_PORT;
  PCIE_RP_CONFIG       *PcieRpConfig;
  FORCE_PWR_HR         ForcePwrFunc;

  ASSERT (DevAddress != NULL);
  ASSERT (RetimerDevice != NULL);

  if ((DevAddress == NULL) || (RetimerDevice == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  DEBUG ((DEBUG_INFO, "Thunderbolt Capsule Update for Retimer.\n"));

  //
  // Initialize device start
  //
  mUpdateTargetType = TARGET_RETIMER;
  TbtDmaPciLocation = TbtNvmDrvAllocateMem (sizeof (PCIE_BDF));
  if (TbtDmaPciLocation == NULL) {
    DEBUG ((DEBUG_ERROR, "CreateRetimerDevInstance: Failed to allocate memory for PCIE_BDF\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  PcieRpConfig = TbtNvmDrvAllocateMem (sizeof (PCIE_RP_CONFIG));
  if (PcieRpConfig == NULL) {
    DEBUG ((DEBUG_ERROR, "CreateTBTDevInstance: Failed to allocate memory for PCIE_RP_CONFIG\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  Device = NULL;
  Status = EFI_SUCCESS;

  if (DevAddress->Port == 0) {
    TBT_TARGET_PORT = PORT_A;
  } else if (DevAddress->Port == 1) {
    TBT_TARGET_PORT = PORT_B;
  } else {
    DEBUG ((DEBUG_ERROR, "Error DevAddress->Port value.\n"));
    return EFI_INVALID_PARAMETER;
  }

  TbtDmaPciLocation->Bus      = DevAddress->Bus;
  TbtDmaPciLocation->Device   = DevAddress->Device;
  TbtDmaPciLocation->Function = DevAddress->Function;
  PcieRpConfig->PcieRpType    = PcieRpType;
  PcieRpConfig->PcieRootPort  = PcieRootPort;

  if (FirmwareType == INTEGRATED_TBT_RETIMER) {
    ForcePwrFunc = TbtNvmDrvYflForcePwrFunc;
  } else{
    ForcePwrFunc = NULL;
  }

  Device = TbtNvmDrvRetimerThruHrCtor (
             FirmwareType,
             TbtDmaPciLocation,
             TBT_TARGET_PORT,
             DevAddress->RetimerIndex,
             PcieRpConfig,
             ForcePwrFunc
             );

  if (Device == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to create a device context for the retimer.\n"));
    Status = EFI_DEVICE_ERROR;
  }

  TbtNvmDrvDeAllocateMem (TbtDmaPciLocation);
  TbtNvmDrvDeAllocateMem (PcieRpConfig);

  *RetimerDevice = Device;
  return Status;
}

/**
  Create and initialize the required data structure to access a TBT device.
  Besides, set up a communication with it if needed.

  @param[in]   FirmwareType       The type of update firmware.
  @param[in]   PcieRpType         PCIE root port type.
  @param[in]   PcieRootPort       The parent pcie root port of discrete TBT device.
  @param[out]  DiscreteTbtDevice  Pointer to a Tbt device instance.

  @retval EFI_SUCCESS             DiscreteTbtDevice is created successfully and is ready to accept commands.
  @retval Others                  An error occurred attempting to access the Retimer device.

**/
EFI_STATUS
EFIAPI
CreateTBTDevInstance (
  IN   UINT8                                          FirmwareType,
  IN   UINT8                                          PcieRpType,
  IN   UINT8                                          PcieRootPort,
  OUT  DISCRETE_TBT_DEV_INSTANCE                      *DiscreteTbtDevice
  )
{
  TBT_HOST_ROUTER   *HrPtr;
  PCIE_RP_CONFIG    *PcieRpConfig;

  ASSERT (DiscreteTbtDevice != NULL);

  if (DiscreteTbtDevice == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  DEBUG ((DEBUG_INFO, "Discrete Thunderbolt Capsule Update.\n"));

  //
  // Initialize device start
  //
  mUpdateTargetType = TARGET_TBT_HOST;

  PcieRpConfig = TbtNvmDrvAllocateMem (sizeof (PCIE_RP_CONFIG));
  if (PcieRpConfig == NULL) {
    DEBUG ((DEBUG_ERROR, "CreateTBTDevInstance: Failed to allocate memory for PCIE_RP_CONFIG\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  PcieRpConfig->PcieRpType   = PcieRpType;
  PcieRpConfig->PcieRootPort = PcieRootPort;

  HrPtr = TbtNvmDrvHrCtor (FirmwareType, PcieRpConfig, NULL, NULL);

  TbtNvmDrvDeAllocateMem (PcieRpConfig);
  if (HrPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "TbtNvmDrvRetimerThruHrCtor: Failed to create DMA\n"));
    return EFI_DEVICE_ERROR;
  }

  *DiscreteTbtDevice = (VOID *)HrPtr;
  return EFI_SUCCESS;
}

/**
  Release the data structure corresponding to given RetimerDevice.
  Besides, close the communication with it if needed.

  @param[in]  RetimerDevice       Retimer device instance corresponding to DevAddress.

  @retval EFI_SUCCESS             RetimerDevice is released successfully.
  @retval Others                  An error occurred attempting to access the Retimer device.

**/
EFI_STATUS
EFIAPI
DestroyRetimerDevInstance (
  IN  RETIMER_DEV_INSTANCE                            RetimerDevice
  )
{
  TBT_RETIMER        *Device;

  Device = (TBT_RETIMER *) RetimerDevice;

  if (Device != NULL) {
    Device->Destroy (Device);
  }

  return EFI_SUCCESS;
}

/**
  Release the data structure corresponding to given DiscreteTbtDevice.
  Besides, close the communication with it if needed.

  @param[in]  DiscreteTbtDevice   Discrete TBT device instance corresponding to DevAddress.

  @retval EFI_SUCCESS             DiscreteTbtDevice is released successfully.
  @retval Others                  An error occurred attempting to access the Tbt device.

**/
EFI_STATUS
EFIAPI
DestroyTbtDevInstance (
  IN  DISCRETE_TBT_DEV_INSTANCE    DiscreteTbtDevice
  )
{
  TBT_HOST_ROUTER        *Device;

  Device = (TBT_HOST_ROUTER *) DiscreteTbtDevice;

  if (Device != NULL) {
    Device->Dtor (Device);
  }

  return EFI_SUCCESS;
}

/**
  Read Retimer NVM FW version on given RetimerDevice.

  Note: Caller is supposed to invoke CreateRetimerDevInstance() first to ensure the data structure
        of RetimerDevice is created properly and the communication to the target device is settled down.
        DestroyRetimerDevInstance() should be invoked after the caller is done with all Reimter accesses.

  @param[in]   RetimerDevice      Retimer device instance corresponding to DevAddress.
  @param[out]  Version            Retimer NVM FW version.

  @retval EFI_SUCCESS             Retimer FW version is successfully get.
  @retval Others                  An error occurred attempting to access the Retimer firmware

**/
EFI_STATUS
EFIAPI
ReadRetimerNvmVersion (
  IN   RETIMER_DEV_INSTANCE                           RetimerDevice,
  OUT  UINT32                                         *Version
  )
{
  EFI_STATUS           Status;
  TBT_RETIMER          *Device;
  UINT32               RetimerVersion;

  if (Version == NULL) {
    ASSERT (FALSE);
    return EFI_INVALID_PARAMETER;
  }

  RetimerVersion = 0;
  Device = (TBT_RETIMER *) RetimerDevice;

  Status = TbtDrvReadNvmVersion (Device, &RetimerVersion);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TbtDrvReadNvmVersion error %r.\n", Status));
    return EFI_DEVICE_ERROR;
  }

  RetimerVersion = RetimerVersion & TBT_NVM_VERSION_MASK;
  DEBUG ((DEBUG_INFO, "Current NVM version is rev3.%02x.%02x\n", (RetimerVersion>>16), ((RetimerVersion>>8) & 0xFF)));

  *Version = RetimerVersion;
  return EFI_SUCCESS;
}

/**
  Update Retimer NVM Firmware on given RetimerDevice.

  Note: Caller is supposed to invoke CreateRetimerDevInstance() first to ensure the data structure
        of RetimerDevice is created properly and the communication to the target device is settled down.
        DestroyRetimerDevInstance() should be invoked after the caller is done with all Reimter accesses.

  @param[in]  RetimerDevice       Retimer device instance corresponding to DevAddress.
  @param[in]  RetimerImage        Pointer to an image buffer contains Retimer FW to be updated with.
  @param[in]  ImageSize           The size of RetimerImage memory buffer.
  @param[in]  Progress            A function used to report the progress of updating the firmware
                                  device with the new firmware image.
  @param[in]  StartPercentage     The start completion percentage value that may be used to report progress
                                  during the flash write operation. The value is between 1 and 100.
  @param[in]  EndPercentage       The end completion percentage value that may be used to report progress
                                  during the flash write operation.

  @retval EFI_SUCCESS             Retimer Firmware is successfully updated.
  @retval Others                  An error occurred attempting to access the Retimer firmware

**/
EFI_STATUS
EFIAPI
UpdateRetimerNvmFirmware (
  IN  RETIMER_DEV_INSTANCE                           RetimerDevice,
  IN  UINT8                                          *RetimerImage,
  IN  UINTN                                          ImageSize,
  IN  EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,          OPTIONAL
  IN  UINTN                                          StartPercentage,
  IN  UINTN                                          EndPercentage
  )
{
  EFI_STATUS           Status;
  UINT32               *BufferPointer;
  TBT_RETIMER          *Device;
  UINT32               Offset;
  TBT_STATUS           TbtStatus;
  UINT32               RecoveryCount;
  UINT8                WriteLength;
  UINTN                DisplayLength;

  ASSERT (RetimerDevice != NULL);
  ASSERT (RetimerImage != NULL);
  ASSERT (EndPercentage > StartPercentage);

  if ((RetimerDevice == NULL) || (RetimerImage == NULL) || (EndPercentage < StartPercentage)) {
    return EFI_INVALID_PARAMETER;
  }

  DisplayLength = EndPercentage - StartPercentage;

  Device = (TBT_RETIMER *) RetimerDevice;

  BufferPointer = (UINT32 *) RetimerImage;
  ImageSize = (UINT32) ImageSize;
  ImageSize -= (BufferPointer[0] & TBT_NVM_VERSION_MASK);
  BufferPointer += ((BufferPointer[0] & TBT_NVM_VERSION_MASK) / 4); // Point to digital

  DEBUG ((DEBUG_INFO, "UpdateDiscreteTbtNvmFirmware START (0x%06x bytes to write):\n", ImageSize));
  ImageSize /= 4;
  for (RecoveryCount = 0; RecoveryCount < TBT_TOTAL_NUM_OF_RECOVERIES_DURING_IMAGE_WRITE; RecoveryCount++) {
    // Write BOPS once at the start
    TbtStatus = TbtNvmDrvDeviceWrOffset (Device, 0x0);
    if (TBT_STATUS_ERR (TbtStatus)) {
      if (TBT_STATUS_FATAL_ERR (TbtStatus)) {
        DEBUG ((DEBUG_ERROR, "Got a FATAL error, exiting...\n"));
        Status = EFI_DEVICE_ERROR;
        goto finish_set_image;
      }
      continue;  // retry image write from the start
    }
    DEBUG ((DEBUG_INFO, "The total data to write is 0x%6x\n", ImageSize));

    for (Offset = 0; Offset < ImageSize; Offset += TBT_NVM_MAX_DWS_TO_WRITE) {
      if ((Progress != NULL) & (DisplayLength > 0) && ((Offset & 0xFF) == 0)) {
        //
        // Display current progress
        //
        Progress (StartPercentage + ((Offset * (DisplayLength)) / ImageSize));
      }
      if ((Offset % 3200) == 0) {
        DEBUG ((DEBUG_INFO, "Written so far : 0x%05x bytes\n", Offset * 4));
      }
      WriteLength = (ImageSize - Offset) >= TBT_NVM_MAX_DWS_TO_WRITE ? TBT_NVM_MAX_DWS_TO_WRITE :
        (UINT8)(ImageSize - Offset);

      if ((Offset % 64) == 0) {
        DEBUG ((DEBUG_INFO, "Writing at Offset - 0x%x length of 0x%0x, first reg 0x%04x\n",
          Offset, WriteLength, (BufferPointer + Offset) [ 0 ]));
      }

      TbtStatus = TbtNvmDrvDeviceWrBlk (Device, BufferPointer + Offset, WriteLength);
      if (TBT_STATUS_ERR (TbtStatus)) {
        DEBUG ((DEBUG_ERROR, "Got an error at offset 0x%08x\n", Offset));
        if (TBT_STATUS_FATAL_ERR (TbtStatus)) {
          DEBUG ((DEBUG_ERROR, "Got a FATAL error, exiting...\n"));
          Status = EFI_DEVICE_ERROR;
          goto finish_set_image;
        }
        break;  // retry image write from the start
                // TODO more fine tuned recovery is possible
      }
    }

    if (!TBT_STATUS_ERR (TbtStatus)) {  // If got here without errors => Success
      DEBUG ((DEBUG_INFO, "Image write finished.\n"));
      Status = EFI_SUCCESS;
      break;
    }
    else {
      DEBUG ((DEBUG_ERROR, "Got an error while writing the image. As a recovery, starting again\n"));
    }
  }
  if (RecoveryCount >= TBT_TOTAL_NUM_OF_RECOVERIES_DURING_IMAGE_WRITE) {
    DEBUG ((DEBUG_ERROR, "Image write wasn't successefull due to a device error\n"));
    Status = EFI_DEVICE_ERROR;
    goto finish_set_image;
  }

  TbtStatus = TbtNvmDrvDeviceExecAuth (Device);
  if (TBT_STATUS_ERR (TbtStatus)) {
    Status = EFI_DEVICE_ERROR;
    goto finish_set_image;
  }

  DEBUG ((DEBUG_INFO, "UpdateRetimerNvmFirmware: Retimer firmware update and authentication success\n"));

  Status = EFI_SUCCESS;

finish_set_image:

  if (Progress != NULL) {
    Progress (EndPercentage);
  }

  return Status;
}

/**
  Update Discrete TBT NVM Firmware on given DiscreteTbtDevice.

  Note: Caller is supposed to invoke CreateTBTDevInstance() first to ensure the data structure
        of DiscreteTbtDevice is created properly and the communication to the target device is settled down.
        DestroyRetimerDevInstance() should be invoked after the caller is done with all Reimter accesses.

  @param[in]  DiscreteTbtDevice   Discrete TBT device instance corresponding to DISCRETE_TBT_ITEM.
  @param[in]  DiscreteTbtImage    Pointer to an image buffer contains discrete TBT FW to be updated with.
  @param[in]  ImageSize           The size of DiscreteTbtImage memory buffer.
  @param[in]  Progress            A function used to report the progress of updating the firmware
                                  device with the new firmware image.
  @param[in]  StartPercentage     The start completion percentage value that may be used to report progress
                                  during the flash write operation. The value is between 1 and 100.
  @param[in]  EndPercentage       The end completion percentage value that may be used to report progress
                                  during the flash write operation.

  @retval EFI_SUCCESS             Retimer Firmware is successfully updated.
  @retval Others                  An error occurred attempting to access the Retimer firmware

**/
EFI_STATUS
EFIAPI
UpdateDiscreteTbtNvmFirmware (
  IN  DISCRETE_TBT_DEV_INSTANCE                      DiscreteTbtDevice,
  IN  UINT8                                          *DiscreteTbtImage,
  IN  UINTN                                          ImageSize,
  IN  EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,          OPTIONAL
  IN  UINTN                                          StartPercentage,
  IN  UINTN                                          EndPercentage
  )
{
  EFI_STATUS           Status;
  UINT32               *BufferPointer;
  TBT_HOST_ROUTER      *Device;
  UINT32               Offset;
  TBT_STATUS           TbtStatus;
  UINT32               RecoveryCount;
  UINT8                WriteLength;
  UINTN                DisplayLength;

  ASSERT (DiscreteTbtDevice != NULL);
  ASSERT (DiscreteTbtImage != NULL);
  ASSERT (EndPercentage > StartPercentage);

  if ((DiscreteTbtDevice == NULL) || (DiscreteTbtImage == NULL) || (EndPercentage < StartPercentage)) {
    return EFI_INVALID_PARAMETER;
  }

  DisplayLength = EndPercentage - StartPercentage;

  Device = (TBT_HOST_ROUTER *) DiscreteTbtDevice;

  BufferPointer = (UINT32 *) DiscreteTbtImage;
  ImageSize = (UINT32) ImageSize;
  ImageSize -= (BufferPointer[0] & TBT_NVM_VERSION_MASK);
  BufferPointer += ((BufferPointer[0] & TBT_NVM_VERSION_MASK) / 4); // Point to digital

  DEBUG ((DEBUG_INFO, "UpdateDiscreteTbtNvmFirmware START (0x%06x bytes to write):\n", ImageSize));
  ImageSize /= 4;
  for (RecoveryCount = 0; RecoveryCount < TBT_TOTAL_NUM_OF_RECOVERIES_DURING_IMAGE_WRITE; RecoveryCount++) {
    // Write BOPS once at the start
    TbtStatus = TbtNvmDrvDeviceWrOffset (Device, 0x0);
    if (TBT_STATUS_ERR (TbtStatus)) {
      if (TBT_STATUS_FATAL_ERR (TbtStatus)) {
        DEBUG ((DEBUG_ERROR, "Got a FATAL error, exiting...\n"));
        Status = EFI_DEVICE_ERROR;
        goto finish_set_image;
      }
      continue;  // retry image write from the start
    }
    DEBUG ((DEBUG_INFO, "The total data to write is 0x%6x\n", ImageSize));

    for (Offset = 0; Offset < ImageSize; Offset += TBT_NVM_MAX_DWS_TO_WRITE) {
      if ((Progress != NULL) & (DisplayLength > 0) && ((Offset & 0xFF) == 0)) {
        //
        // Display current progress
        //
        Progress (StartPercentage + ((Offset * (DisplayLength)) / ImageSize));
      }
      if ((Offset % 3200) == 0) {
        DEBUG ((DEBUG_INFO, "Written so far : 0x%05x bytes\n", Offset * 4));
      }
      WriteLength = (ImageSize - Offset) >= TBT_NVM_MAX_DWS_TO_WRITE ? TBT_NVM_MAX_DWS_TO_WRITE :
        (UINT8)(ImageSize - Offset);

      if ((Offset % 64) == 0) {
        DEBUG ((DEBUG_INFO, "Writing at Offset - 0x%x length of 0x%0x, first reg 0x%04x\n",
          Offset, WriteLength, (BufferPointer + Offset) [ 0 ]));
      }

      TbtStatus = TbtNvmDrvDeviceWrBlk (Device, BufferPointer + Offset, WriteLength);
      if (TBT_STATUS_ERR (TbtStatus)) {
        DEBUG ((DEBUG_ERROR, "Got an error at offset 0x%08x\n", Offset));
        if (TBT_STATUS_FATAL_ERR (TbtStatus)) {
          DEBUG ((DEBUG_ERROR, "Got a FATAL error, exiting...\n"));
          Status = EFI_DEVICE_ERROR;
          goto finish_set_image;
        }
        break;  // retry image write from the start
                // TODO more fine tuned recovery is possible
      }
    }

    if (!TBT_STATUS_ERR (TbtStatus)) {  // If got here without errors => Success
      DEBUG ((DEBUG_INFO, "Image write finished.\n"));
      Status = EFI_SUCCESS;
      break;
    }
    else {
      DEBUG ((DEBUG_ERROR, "Got an error while writing the image. As a recovery, starting again\n"));
    }
  }
  if (RecoveryCount >= TBT_TOTAL_NUM_OF_RECOVERIES_DURING_IMAGE_WRITE) {
    DEBUG ((DEBUG_ERROR, "Image write wasn't successefull due to a device error\n"));
    Status = EFI_DEVICE_ERROR;
    goto finish_set_image;
  }

  TbtStatus = TbtNvmDrvDeviceExecAuth (Device);
  if (TBT_STATUS_ERR (TbtStatus)) {
    Status = EFI_DEVICE_ERROR;
    goto finish_set_image;
  }

  DEBUG ((DEBUG_INFO, "UpdateDiscreteTbtNvmFirmware: Discrete TBT NVM firmware update and authentication success\n"));

  Status = EFI_SUCCESS;

finish_set_image:

  if (Progress != NULL) {
    Progress (EndPercentage);
  }

  return Status;
}
