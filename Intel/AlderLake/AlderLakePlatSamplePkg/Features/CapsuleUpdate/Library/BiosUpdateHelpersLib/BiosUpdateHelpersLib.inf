## @file
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
#  This library provide some common functions can be shared among different FmpDeviceLib instances
#  especially for the instance which involves BIOS and/or multiple components.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = BiosUpdateHelpersLib
  FILE_GUID                      = ED72169C-CACB-44F9-8E94-0140C1C5F72A
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = BiosUpdateHelpersLib
  CONSTRUCTOR                    = BiosUpdateHelpersLibConstructor

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64
#

[Sources]
  BiosUpdateHelpersLib.c
  ParseConfigProfile.c

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
#[-start-201030-IB16810136-remove]#
#  ClientOneSiliconPkg/SiPkg.dec
#[-end-201030-IB16810136-remove]#
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-201030-IB16810136-add]#
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-201030-IB16810136-add]#
#[-start-201030-IB16810136-remove]#
#  AlderLakeBoardPkg/BoardPkg.dec
#[-end-201030-IB16810136-remove]#

[LibraryClasses]
  DebugLib
  BaseLib
  BaseMemoryLib
  UefiRuntimeServicesTableLib
  MemoryAllocationLib
  IoLib
  IniParsingLib
  ComponentUpdateLib
  SeamlessRecoverySupportLib
  CpuPlatformLib
  DxeBootStateLib
#[-start-201030-IB16810136-add]#
  VariableLib
#[-end-201030-IB16810136-add]#

[Pcd]
  gPlatformModuleTokenSpaceGuid.PcdTopSwapEnableSwSmi              ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdTopSwapDisableSwSmi             ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable                ## CONSUMES
#[-start-201030-IB16810136-remove]#
#  gBoardModuleTokenSpaceGuid.PcdFlashObbOffset                     ## CONSUMES
#  gBoardModuleTokenSpaceGuid.PcdFlashObbSize                       ## CONSUMES
#  gSiPkgTokenSpaceGuid.PcdBiosSize                                 ## CONSUMES
#[-end-201030-IB16810136-remove]#
#[-start-201030-IB16810136-add]#
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize 
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize
  gSiPkgTokenSpaceGuid.PcdBiosAreaBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashAreaSize
 gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled
#[-end-201030-IB16810136-add]#

[Guids]
  gCapsuleBiosConfigFileGuid                                       ## CONSUMES
  gCapsuleBiosImageFileGuid                                        ## CONSUMES
  gSetupVariableGuid                                               ## CONSUMES
  gPdtExistGuid                                                    ## CONSUMES
#[-start-201030-IB16810136-remove]#
#  gMeInfoSetupGuid                                                 ## CONSUMES
#  gMeBiosExtensionSetupGuid                                        ## CONSUMES
#[-end-201030-IB16810136-remove]#
  gBootStateGuid                                                   ## CONSUMES
