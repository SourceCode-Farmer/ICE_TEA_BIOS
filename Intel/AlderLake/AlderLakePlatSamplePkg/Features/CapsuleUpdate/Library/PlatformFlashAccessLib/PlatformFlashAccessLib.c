/** @file
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Platform Flash Access library to update different system firmware components

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <PiDxe.h>

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/PlatformFlashAccessLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/IoLib.h>
#include <Library/FWUpdateLib.h>
#include <Library/DxeMeLib.h>
#include <Library/TimerLib.h>
#include <Library/BiosUpdateHelpersLib.h>

#include <Protocol/Spi.h>
#include <Library/HobLib.h>

#include <BiosGuard.h>
#include <Register/HeciRegs.h>
#include <Library/CpuPlatformLib.h>

#include "PlatformFlashAccessLibInternal.h"

//
// Used in BiosGuard update function
//
// BGUPC header 8 + PKCS1 1.5 Certificate 516 (256+4+256) = 524 bytes
#define BIOSGUARD_CERTIFICATE_SIZE             524

PCH_SPI_PROTOCOL                               *mSpiProtocol = NULL;
/*
  Get and return the pointer of SPI protocol.

  @param[in]    VOID

  @retval       PCH_SPI_PROTOCOL    Pointer point to the SPI procotol.
*/
PCH_SPI_PROTOCOL*
EFIAPI
GetPchSpiProtocol(
  VOID
  )
{
  return mSpiProtocol;
}

/**
  Generic interface for read data from SPI flash.

  @param[in]  FLASH_ACCESS_REQUEST     Request strucutre for read.

  @retval     EFI_SUCCESS.             Operation is successful.
  @retval     EFI_OUT_OF_RESOURCES     Failed to allocate needed memory buffer.
  @retval     EFI_VOLUME_CORRUPTED     The block is not updated as expected.
  @retval     Others                   If there is any device errors.

**/
//[-start-201030-IB16810136-modify]//
EFI_STATUS
EFIAPI
FlashReadAdl (
  IN FLASH_ACCESS_REQUEST  *ReadReq
  )
//[-end-201030-IB16810136-modify]//
{
  EFI_STATUS  Status;

  //
  // Compare Buffer block with the destination.
  //    Caution! If the library is used in Resilience udpate, FlashRead shouldn't use MMIO read but has to directly use sSPI address
  //
  Status = mSpiProtocol->FlashRead (
                           mSpiProtocol,
                           ReadReq->FlashRegionType,
                           (UINT32)ReadReq->FlashAddress,
                           (UINT32)ReadReq->DataSize,
                           ReadReq->Data
                           );

  return Status;
}

/**
  Interface for flash update, call internal function to update flash.

  @param[in]  WriteReq                 Request information for update flash.

  @retval     EFI_SUCCESS.             Operation is successful.
  @retval     EFI_OUT_OF_RESOURCES     Failed to allocate needed memory buffer.
  @retval     EFI_VOLUME_CORRUPTED     The block is not updated as expected.
  @retval     Others                   If there is any device errors.

**/
EFI_STATUS
EFIAPI
FlashUpdate (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  )
{
  EFI_STATUS  Status;
  BOOLEAN     BiosGuardEnabled;

  BiosGuardEnabled = IsBiosGuardEnabled ();

  //
  // This is an example shows how to implement a generic Flash Read/Write Interface
  //
  if (BiosGuardEnabled && WriteReq->BgupImage != NULL && WriteReq->BgupImageSize != 0) {
    Status = InternalBiosGuardUpdate (WriteReq, Progress, StartPercentage, EndPercentage);
  } else {
    Status = InternalSpiFlashUpdate (WriteReq, Progress, StartPercentage, EndPercentage);
  }

  return Status;
}

EFI_STATUS
EFIAPI
FlashAccessLibConstructor (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                      Status;
  BOOLEAN                         BiosGuardEnabled;
  BiosGuardEnabled = IsBiosGuardEnabled ();

  DEBUG ((DEBUG_INFO, "FlashAccessLib Init\n"));
  DEBUG ((DEBUG_INFO, "BiosGuard enabled = %d\n", BiosGuardEnabled));

  Status = EFI_SUCCESS;
  if (mSpiProtocol == NULL) {
    Status = gBS->LocateProtocol (
                    &gPchSpiProtocolGuid,
                    NULL,
                    (VOID **)&mSpiProtocol
                    );
    ASSERT_EFI_ERROR (Status);
  }

  return Status;
}
