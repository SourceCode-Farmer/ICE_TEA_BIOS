/** @file
  Debug Lib for PlatformFlashAccessLib.

  Copyright (c) 2016 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include "PlatformFlashAccessDebugLib.h"

extern EFI_RUNTIME_SERVICES  *gRT;

/*
  For fault tolerant test usage. Will clean it after moving to CR Common

*/
EFI_STATUS
EFIAPI
TestDeadLoop (
  VOID
  )
{
  EFI_STATUS  Status;
  BOOLEAN     IsFirstDeadLoop;
  UINTN       DataSize;

  DataSize = sizeof (BOOLEAN);
  Status = gRT->GetVariable (
                  L"IsFirstDeadLoop",
                  &gEfiCallerIdGuid,
                  NULL,
                  &DataSize,
                  &IsFirstDeadLoop
                  );
  if (Status != EFI_NOT_FOUND) {
    DEBUG ((DEBUG_INFO, "Recovery from crash! Skip deadloop.\n"));
    return Status;
  }

  IsFirstDeadLoop = TRUE;
  Status = gRT->SetVariable (
                  L"IsFirstDeadLoop",
                  &gEfiCallerIdGuid,
                  EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                  sizeof(BOOLEAN),
                  &IsFirstDeadLoop
                  );

  DEBUG ((DEBUG_INFO, "Recovery from crash! deadloop.\n"));
  //
  // Either stop and manual remove power to cause crash or auto reset system can be used
  //
  CpuDeadLoop ();
  //gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);

  return EFI_SUCCESS;
}
