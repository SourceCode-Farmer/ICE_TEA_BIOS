/** @file
  ME Resiliency init Dxe driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/FirmwareManagement.h>
#include <Protocol/PlatformSpecificResetFilter.h>
#include <Register/HeciRegs.h>
#include <Register/MeRegs.h>
#include <Library/SeamlessRecoverySupportLib.h>
#include <PchResetPlatformSpecific.h>
#include <MeFwHob.h>
#include <Library/MeFwStsLib.h>
#include <Library/HobLib.h>
#include <Library/SpiAccessLib.h>

/**
  MeResiliencyCallBack

  ME Resiliency Feature to determine the CSE Image health and then
  initiate capsule based recovery.

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
MeResiliencyCallBack (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS                                     Status;
  UINT64                                         HeciBaseAddress;
  HECI_FWS_REGISTER                              MeFwSts;
  HECI_GS_SHDW_REGISTER                          MeFwSts2;
  PCH_RESET_DATA                                 ResetData;
  UINTN                                          ResetDataSize;
  SYSTEM_FIRMWARE_UPDATE_PROGRESS                PreviousProgress;
  ME_FW_HOB                                      *MeFwHob;
  UINT32                                         Index;

  DEBUG ((DEBUG_INFO, "MeResiliencyCallBack Start\n"));

  Status  = EFI_SUCCESS;
  MeFwHob = NULL;

  HeciBaseAddress = PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, 0);
  //
  // Read ME FWS
  //
  MeFwSts.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS);
  //
  // Read ME FWS2
  //
  MeFwSts2.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS_2);
  if ((MeFwSts.ul == 0xFFFFFFFF) || (MeFwSts2.ul == 0xFFFFFFFF)) {
    DEBUG ((DEBUG_INFO, "ME is disabled, read status from MeFwHob\n"));
    MeFwHob = GetFirstGuidHob (&gMeFwHobGuid);
    if (MeFwHob == NULL) {
      return;
    }
    Index = FindMeFwStsIndex (R_ME_HFS);
    if (Index != 0xFF) {
      MeFwSts.ul = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
    }

    Index = FindMeFwStsIndex (R_ME_HFS_2);
    if (Index != 0xFF) {
      MeFwSts2.ul = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
    }
  }
  DEBUG ((DEBUG_INFO, "MeFwSts = 0x%x!\n", MeFwSts.ul));
  DEBUG ((DEBUG_INFO, "MeFwSts2 = 0x%x!\n", MeFwSts2.ul));

  if ((MeFwSts.r.CurrentState == ME_STATE_RECOVERY) ||
      (MeFwSts.r.FtBupLdFlr == 1) ||
      (MeFwSts2.r.FwUpdIpu == 1)) {
    DEBUG ((DEBUG_INFO, "ME region is corrupted, initiate ME recovery\n"));

    IsPreviousUpdateUnfinished (&PreviousProgress);
    if (IsMeRecoveryCapsuleExistOnStorage () &&
        ((PreviousProgress.Component == NotStarted) || (PreviousProgress.Component == UpdatingResiliency))) {
      //
      // Set Update Progress variable for ME resiliency in the next boot.
      //
      SetUpdateProgress (UpdatingMeResiliency, 0);
      //
      // Issue a global reset to initiate capsule based recovery.
      //
      ResetDataSize = sizeof (PCH_RESET_DATA);
      CopyMem (&ResetData.Guid, &gPchGlobalResetGuid, sizeof (EFI_GUID));
      StrCpyS (ResetData.Description, PCH_RESET_DATA_STRING_MAX_LENGTH, PCH_PLATFORM_SPECIFIC_RESET_STRING);

      DEBUG ((DEBUG_INFO, "Issue global reset for capsule based recovery\n"));
      if ((PreviousProgress.Component == UpdatingResiliency) || SpiIsTopSwapEnabled ()) {
        DEBUG ((DEBUG_INFO, "Defer global reset to Resileincy callback\n"));
      } else {
        gRT->ResetSystem (EfiResetPlatformSpecific, EFI_SUCCESS, ResetDataSize, &ResetData);
      }
    } else {
      DEBUG ((DEBUG_ERROR, "ME Recovery capsule is not found on storage - skip recovery.\n"));
    }
  } else {
    //
    // Sync ME Recovery Capsules.
    //
    Status = SyncMeRecoveryCapsules ();
    DEBUG ((DEBUG_INFO, "SyncMeRecoveryCapsules - %r\n", Status));
  }

  DEBUG((DEBUG_INFO, "MeResiliencyCallBack End\n"));
  return;
}

/**
  Initialize Resiliency Support for ME.

  @retval EFI_SUCCESS    Resiliency Support is initialized successfully
  @retval EFI_NOT_FOUND  Resiliency Support is not initialized successfully
**/

EFI_STATUS
EFIAPI
MeResiliencyDxeEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS                Status;
  EFI_EVENT                 BeforeEndOfDxeEvent;

  Status                    = EFI_SUCCESS;
  BeforeEndOfDxeEvent       = NULL;

  DEBUG ((DEBUG_INFO, "MeResiliencyDxeEntryPoint Start\n"));

  DEBUG ((DEBUG_INFO, "Register an EndOfDxe Callback Function for ME Resiliency Health Check\n"));
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  MeResiliencyCallBack,
                  NULL,
                  &gPlatformBeforeEndOfDxeEventGroupGuid,
                  &BeforeEndOfDxeEvent
                  );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to Register an EndOfDxe CallBack function for ME Resiliency, Status: %d\n", Status));
  }

  DEBUG ((DEBUG_INFO, "MeResiliencyDxeEntryPoint End\n"));
  return Status;
}
