/**@file
  NvmeRecovery Dxe driver, main entry of the Nvme recovery feature.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "NvmeRecoveryDxe.h"

extern PCH_SPI_PROTOCOL *mSpiProtocol;

//
// A default Nonce value in case we can't get random value.
//
GLOBAL_REMOVE_IF_UNREFERENCED CONST UINT8 mNvmeDefaultNonce[16] = {
  0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88
  };

//
// Key value for Aes-SHA-256 validation. (Wrapping key)
//
GLOBAL_REMOVE_IF_UNREFERENCED CONST UINT8 mFakeWrapping256Key[32] = {
  0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c,
  0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c
  };

GLOBAL_REMOVE_IF_UNREFERENCED CONST UINT8 mAes128CbcIvec[] = {
  0x56, 0x2e, 0x17, 0x99, 0x6d, 0x09, 0x3d, 0x28, 0xdd, 0xb3, 0xba, 0x69, 0x5a, 0x2e, 0x6f, 0x58
  };

GLOBAL_REMOVE_IF_UNREFERENCED UINT8 mRpmbKey256[RANDOM_SEED_SIZE] = {0};

/**
  Initialize PCH SpiProtocol interface module pointer

  @retval EFI_SUCCESS      PCH SpiProtocol interface module pointer is initialized successfully
  @retval Others           Fail to locate PCH SpiProtocol interface

**/
EFI_STATUS
InitializeSpiProtocolInterface (
  VOID
  )
{
  EFI_STATUS                   Status;

  Status = EFI_SUCCESS;

  if (mSpiProtocol == NULL) {
    Status = gBS->LocateProtocol (
                    &gPchSpiProtocolGuid,
                    NULL,
                    (VOID **)&mSpiProtocol
                    );
    ASSERT_EFI_ERROR (Status);
  }

  return Status;
}

/**
  Make a check if it is a NVMe device by device path.

  @param  DevicePath       The NVMe device path.

  @retval TRUE     it is an NVMe device.
  @retval FALSE    it is not an NVMe device.

**/
BOOLEAN
IsNvmeDevice (
  IN  CONST EFI_DEVICE_PATH_PROTOCOL   *DevicePath
  )
{
  EFI_DEVICE_PATH_PROTOCOL          *TempDevicePath;

  TempDevicePath = (EFI_DEVICE_PATH_PROTOCOL *) DevicePath;
  while (!IsDevicePathEndType (TempDevicePath)) {
    if (DevicePathType (TempDevicePath) == MESSAGING_DEVICE_PATH &&
          DevicePathSubType (TempDevicePath) == MSG_NVME_NAMESPACE_DP) {
      return TRUE;
    }
    TempDevicePath = NextDevicePathNode (TempDevicePath);
  }
  return FALSE;
}

/**
  Program RpmbKey and WrappedKey to Nvme Device, WrappedKey should be the generation
  by RpmbKey + AesCbc encryption.

  @param  Ssp        The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId    Id of the media, changes every time the media is replaced.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands, or Csme seed
                                       is not available.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeBasedRecoveryGetSetRpmbKey (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId
  )
{
  EFI_STATUS               Status;
  UINT8                    WrappedKey[SHA256_DIGEST_SIZE];
  UINT8                    WrappingKey[SHA256_DIGEST_SIZE];
  UINT8                    RPMBTarget;
  UINT8                    SectorCount;
  EFI_HOB_GUID_TYPE        *GuidHob;
  GET_BIOS_SEED_RESP       *BiosSeedContent;
  UINTN                    BlockCount;
  UINT64                   TempSeed[2];
  UINTN                    Index;

  RPMBTarget  = 0;
  SectorCount = 1;
  BlockCount  = 0;
  Index       = 0;
  BiosSeedContent = NULL;

  BlockCount = sizeof (mRpmbKey256) / 16;
  while (Index < BlockCount) {
    GetRandomNumber128 (TempSeed);
    CopyMem (mRpmbKey256 + Index * 16, TempSeed, 16);
    Index++;
  }

  GuidHob = GetFirstGuidHob (&gCsmeSeedHobGuid);
  if (GuidHob != NULL) {
    BiosSeedContent = (GET_BIOS_SEED_RESP*) GET_GUID_HOB_DATA (GuidHob);
    CopyMem (WrappingKey, &(BiosSeedContent->BiosSeed), SHA256_DIGEST_SIZE);
    ZeroMem (BiosSeedContent, sizeof (GET_BIOS_SEED_RESP));
  } else {
    DEBUG ((DEBUG_ERROR, "Wrapping Key is not available. \n"));
    return EFI_UNSUPPORTED;
  }

  Status = NvmeAuthenticatedKeyDataProgramming (Ssp, MediaId, mRpmbKey256, SHA256_DIGEST_SIZE);
  if (EFI_ERROR (Status)) {
    //
    // ***** could be already programmed before *****
    //
    DEBUG ((DEBUG_ERROR,"RPMB Key Programming is falied, Status[%x]. \n", Status));
    //
    // Generate Rpmb by csme seed + WrappedKey
    //
    Status = NvmeAuthenticatedDataRead (Ssp, MediaId, RPMBTarget, WrappedKey, SectorCount, SHA256_DIGEST_SIZE);
    if (EFI_ERROR (Status)) {
      ZeroMem (WrappingKey, SHA256_DIGEST_SIZE);
      ZeroMem (WrappedKey, SHA256_DIGEST_SIZE);
      DEBUG ((DEBUG_ERROR, "Can't get WrappedKey from RPMB target[%x], Status:[%x]. \n", RPMBTarget, Status));
      return Status;
    }

    if (AesCbcRequest (mRpmbKey256, WrappingKey, WrappedKey, SHA256_DIGEST_SIZE, FALSE) == FALSE) {
      DEBUG ((DEBUG_ERROR, "RPMB Key Provision: AesCbcSha256Request is failed. \n"));
      ASSERT (FALSE);
    }

    ZeroMem (WrappingKey, SHA256_DIGEST_SIZE);
  } else {
    DEBUG ((DEBUG_INFO,"RPMB Key Programming : Successful! \n"));
    //
    // Get Rpmb from Random value which was saved to gWrappingKey256
    //
    AesCbcRequest (WrappedKey, WrappingKey, mRpmbKey256, SHA256_DIGEST_SIZE, TRUE);
    ZeroMem (WrappingKey, SHA256_DIGEST_SIZE);

    Status = NvmeAuthenticatedDataWrite (Ssp, MediaId, RPMBTarget, WrappedKey, SectorCount, SHA256_DIGEST_SIZE);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "WrappedKey programming is failed, Status:[%x]. \n", Status));
    }
  }

  ZeroMem (WrappedKey, SHA256_DIGEST_SIZE);
  return Status;
}

/**
  Get CSME offset/size by platform layout, it depends on the content should
  be stored in NVMe boot partition.
  Caller should free the memory.

  @param  NonBiosBuffer        A pointer to the image of CSME.
  @param  CsmeAddress          A pointer returned as csme image address.
  @param  CsmeSize             A pointer returned as csme size.

  @retval EFI_SUCCESS             Read success
  @retval EFI_INVALID_PARAMETER   Invalid region type given
  @retval EFI_DEVICE_ERROR        The region is not used

**/
EFI_STATUS
EFIAPI
GetCSMEImageInfo (
  OUT UINT8       **NonBiosBuffer,
  OUT UINT32      *CsmeAddress,
  OUT UINT32      *CsmeSize
  )
{
  EFI_STATUS               Status;

  Status = mSpiProtocol->GetRegionAddress (mSpiProtocol, FlashRegionMe, CsmeAddress, CsmeSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "The CSME region can not be read1, status == [%x] \n", Status));
    return Status;
  }

  *NonBiosBuffer = AllocateZeroPool (*CsmeSize);
  if (*NonBiosBuffer == NULL) {
    DEBUG ((DEBUG_ERROR,"  Warning: Run out memory for SPI read. \n"));
  }

  Status = mSpiProtocol->FlashRead (
                             mSpiProtocol,
                             FlashRegionMe,
                             0,
                             *CsmeSize,
                             *NonBiosBuffer
                             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "The CSME region can not be read2, status == [%x] \n", Status));
    return Status;
  }

  return EFI_SUCCESS;
}

/**
  Get Bios offset/size by platform layout, it depends on the content should
  be stored in NVMe boot partition.

  @param  BiosSourceBuffer        A pointer return as bios buffer address
  @param  BiosSourceSize          A pointer return as bios buffer size.

  @retval EFI_SUCCESS      Successfully get the identify controller data.
  @retval EFI_DEVICE_ERROR Fail to get the identify controller data.

**/
EFI_STATUS
EFIAPI
GetBiosImageInfo (
  OUT VOID        **BiosSourceBuffer,
  OUT UINT32      *BiosSourceSize,
  OUT UINT32      *BiosAddress
  )
{
  EFI_STATUS               Status;
  *BiosSourceSize   = FixedPcdGet32 (PcdBiosSize);

  Status = mSpiProtocol->GetRegionAddress (mSpiProtocol, FlashRegionBios, BiosAddress, BiosSourceSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "The Bios region can not be read1, status == [%x] \n", Status));
    return Status;
  }

  *BiosSourceBuffer = AllocateCopyPool (
                        *BiosSourceSize,
                        (VOID *) (UINTN) FixedPcdGet32 (PcdBiosAreaBaseAddress)
                        );

  return EFI_SUCCESS;
}

/**
  Create a firmware descriptor which records the layout of every firmware components,
  EC will execute restore operation by detailed address mapping between SPI and NVME
  Boot Partition. Following up headers, the images of different IPs will be connected
  one by one.

  @param  DescriptorTable        A pointer the buffer which contains the data need to
                                 backup to NVMe Boot Partition.
  @param  BPDataSize             A pointer to buffer size

  @retval EFI_SUCCESS              Successfully create descriptor before image backup.
  @retval RETURN_INVALID_PARAMETER Fail to get the identify controller data.

**/
EFI_STATUS
EFIAPI
ConstructNvmeRecoveryDescriptorAndData (
  IN OUT UINT8     *DescriptorTable,
  IN OUT UINTN     *BPDataSize
  )
{
  EFI_STATUS               Status;
  NVME_IMAGE_HEADER        NvmeHeader;
  NVME_IMAGE_STRUCT        RestoreStruct;
  NVME_RESTORE_ELEMENT     RestoreElement;
  UINT8                    *TempBuffHeader;
  UINT8                    *NonBiosBuffer;
  UINT32                   BPStoreLengh;
  UINT32                   BPOffset;
  VOID                     *BiosSourceBuffer;
  UINT32                   BiosSourceSize;
  UINT32                   NonBiosAddress;
  UINT32                   NonBiosSize;
  UINT32                   CSMECode1M;
  UINT32                   BiosAddress;

  BPOffset        = NVME_BP_HEADER_SIZE;
  TempBuffHeader  = DescriptorTable;
  BPStoreLengh    = 0;
  NonBiosSize     = 0;
  CSMECode1M      = 0x100000;                    // fake image in short term
  NonBiosBuffer    = NULL;
  BiosSourceBuffer = NULL;

  Status = InitializeSpiProtocolInterface ();
  if (EFI_ERROR (Status)) {
    return EFI_NOT_READY;
  }
  //
  // Read Non-Bios region
  //
  Status = GetCSMEImageInfo (&NonBiosBuffer, &NonBiosAddress, &NonBiosSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "NVMe Descriptor: can't get csme info with Status. [%x].\n", Status));
    FreePool (NonBiosBuffer);
    return Status;
  }
  DEBUG ((DEBUG_INFO, "<RecoveryDescriptor> NonBiosAddress = [%x], NonBiosSize = [%x].\n", NonBiosAddress, NonBiosSize));

  //
  // Read Bios region
  //
  Status = GetBiosImageInfo (&BiosSourceBuffer, &BiosSourceSize, &BiosAddress);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "NVMe Descriptor: can't get bios info with Status. [%x].\n", Status));
    FreePool (BiosSourceBuffer);
    FreePool (NonBiosBuffer);
    return Status;
  }
  DEBUG ((DEBUG_INFO, "<RecoveryDescriptor>, BiosSourceBuffer = [%x], BiosSourceSize = [%x], BiosAddress = [%x].\n", BiosSourceBuffer, BiosSourceSize, BiosAddress));

  //
  //   The layout of recovery descriptor based on SAS 0.8+
  //
  //   |--------------------------------------|
  //   | Main header                          |
  //   |--------------------------------------|
  //   | CSME main header                     |
  //   |--------------------------------------|
  //   | Sub header for CSME code             |
  //   |--------------------------------------|
  //   | Sub header for CSME data             |
  //   |--------------------------------------|
  //   | Bios main header                     |
  //   |--------------------------------------|
  //   | Sub header for Bios                  |
  //   |--------------------------------------|
  //   | CSME code image                      |
  //   |--------------------------------------|
  //   | CSME data image                      |
  //   |--------------------------------------|
  //   | Bios image                           |
  //   |--------------------------------------|
  //
  ZeroMem (&NvmeHeader, sizeof (NVME_IMAGE_HEADER));
  ZeroMem (&RestoreStruct, sizeof (NVME_IMAGE_STRUCT));
  ZeroMem (&RestoreElement, sizeof (NVME_RESTORE_ELEMENT));

  //
  // Descriptor Header
  //
  NvmeHeader.NvmeImageStruct.Type             = NVME_RECOVERY_DESCRIPTOR_HEADER_SIGNATURE;
  NvmeHeader.NvmeImageStruct.StructVersion    = 1;
  NvmeHeader.NvmeImageStruct.IpType           = RECV_HEAD;                            // 0x00 means the header
  NvmeHeader.NvmeImageStruct.TotalSize        = BiosSourceSize + NonBiosSize;
  NvmeHeader.NvmeImageStruct.NumberOfElements = 2;                                    // inclues Bios and CSME(code/data)
  NvmeHeader.HashType                         = RECV_SHA256;                          // SHA256
  NvmeHeader.NvmeImageStruct.Reserved         = 0;
  CopyMem (TempBuffHeader, &NvmeHeader, sizeof (NVME_IMAGE_HEADER));
  TempBuffHeader = TempBuffHeader + sizeof (NVME_IMAGE_HEADER);
  BPStoreLengh   = BPStoreLengh + sizeof (NVME_IMAGE_HEADER);

  //
  // CSME header
  //
  RestoreStruct.Type             = NVME_RECOVERY_DESCRIPTOR_ELEMENT_SIGNATURE;        // recv
  RestoreStruct.StructVersion    = 1;                                                 // Init version
  RestoreStruct.IpType           = RECV_CSME;                                         // CSME = 2, there will be an enum list in future.
  RestoreStruct.TotalSize        = NonBiosSize;                                       // valuable??
  RestoreStruct.NumberOfElements = 2;                                                 // includes csme code/data
  RestoreStruct.Reserved         = 0;
  CopyMem (TempBuffHeader, &RestoreStruct, sizeof (NVME_IMAGE_STRUCT));
  TempBuffHeader = TempBuffHeader + sizeof (NVME_IMAGE_STRUCT);
  BPStoreLengh   = BPStoreLengh + sizeof (NVME_IMAGE_STRUCT);

  //
  // CSME Code Element
  //
  BPOffset = BPOffset + NVME_RECOVERY_DESCRIPTOR_SIZE;                              // incude default BP header 16bytes, take care!
  RestoreElement.UpdateFlag   = 0;
  RestoreElement.OffsetImage  = BPOffset;
  RestoreElement.SizeImage    = CSMECode1M;
  RestoreElement.OffsetSpi    = NonBiosAddress;
  RestoreElement.SizeSpi      = CSMECode1M;
  CopyMem (TempBuffHeader, &RestoreElement, sizeof (NVME_RESTORE_ELEMENT));
  TempBuffHeader = TempBuffHeader + sizeof (NVME_RESTORE_ELEMENT);
  BPStoreLengh   = BPStoreLengh + sizeof (NVME_RESTORE_ELEMENT);

  //
  // CSME Data Element
  //
  BPOffset = BPOffset + CSMECode1M;
  RestoreElement.UpdateFlag   = 0;
  RestoreElement.OffsetImage  = BPOffset;
  RestoreElement.SizeImage    = NonBiosSize - CSMECode1M;
  RestoreElement.OffsetSpi    = NonBiosAddress + CSMECode1M;
  RestoreElement.SizeSpi      = NonBiosSize - CSMECode1M;
  CopyMem (TempBuffHeader, &RestoreElement, sizeof (NVME_RESTORE_ELEMENT));
  TempBuffHeader = TempBuffHeader + sizeof (NVME_RESTORE_ELEMENT);
  BPStoreLengh   = BPStoreLengh + sizeof (NVME_RESTORE_ELEMENT);

  //
  // Bios Area
  //
  RestoreStruct.Type             = NVME_RECOVERY_DESCRIPTOR_ELEMENT_SIGNATURE;        // recv
  RestoreStruct.StructVersion    = 1;                                                 // Init version
  RestoreStruct.IpType           = RECV_BIOS;                                         // bios = 1, there will be an enum list in future.
  RestoreStruct.TotalSize        = BiosSourceSize;
  RestoreStruct.NumberOfElements = 1;
  RestoreStruct.Reserved         = 0;
  CopyMem (TempBuffHeader, &RestoreStruct, sizeof (NVME_IMAGE_STRUCT));
  TempBuffHeader = TempBuffHeader + sizeof (NVME_IMAGE_STRUCT);
  BPStoreLengh   = BPStoreLengh + sizeof (NVME_IMAGE_STRUCT);

  BPOffset = BPOffset + NonBiosSize - CSMECode1M;
  RestoreElement.UpdateFlag   = 0;
  RestoreElement.OffsetImage  = BPOffset;
  RestoreElement.SizeImage    = BiosSourceSize;
  RestoreElement.OffsetSpi    = BiosAddress;             // read bios offset in spi
  RestoreElement.SizeSpi      = BiosSourceSize;
  CopyMem (TempBuffHeader, &RestoreElement, sizeof (NVME_RESTORE_ELEMENT));
  TempBuffHeader = TempBuffHeader + sizeof (NVME_RESTORE_ELEMENT);
  BPStoreLengh   = BPStoreLengh + sizeof (NVME_RESTORE_ELEMENT);

  //
  // Csme Code Image
  //
  CopyMem (TempBuffHeader, NonBiosBuffer, CSMECode1M);
  TempBuffHeader = TempBuffHeader + CSMECode1M;

  //
  // Csme Data Image
  //
  CopyMem (TempBuffHeader, NonBiosBuffer + CSMECode1M, NonBiosSize - CSMECode1M);
  TempBuffHeader = TempBuffHeader + NonBiosSize - CSMECode1M;

  //
  // Bios Image
  //
  CopyMem (TempBuffHeader, (UINT8 *)BiosSourceBuffer, BiosSourceSize);
  TempBuffHeader = TempBuffHeader + BiosSourceSize;
  BPStoreLengh   = BPStoreLengh + NonBiosSize + BiosSourceSize;
  *BPDataSize   = BPStoreLengh;

  if (NonBiosBuffer != NULL) {
    FreePool (NonBiosBuffer);
  }

  if (BiosSourceBuffer != NULL) {
    FreePool (BiosSourceBuffer);
  }

  return EFI_SUCCESS;
}

/**
  Implemented a series of Replay Protected Memory Block(RPMB) command to configure
  Device Configuration Block(DCB) value.

  @param  Passthru          A pointer to EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL.
  @param  NamespaceId       Current device name space ID.
  @param  Ssp               A pointer to EFI_STORAGE_SECURITY_COMMAND_PROTOCOL.
  @param  MediaId           The media ID that the write request is for.

  @retval EFI_SUCCESS      Successfully get the identify controller data.
  @retval EFI_DEVICE_ERROR Fail to get the identify controller data.

**/
EFI_STATUS
EFIAPI
ImageBackUpSpi2Nvme (
  IN  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL      *Passthru,
  IN  UINT32                                  NamespaceId,
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId
  )
{
  EFI_STATUS                              Status;
  UINT8                                   DCBDStru[NVME_RPMB_CONTENTS_SIZE];
  UINT8                                   *ReadReceiveBuffer;
  UINTN                                   NvmeRecBPSize;
  UINT8                                   *TempNvmeRecBPBuffer;
  UINTN                                   TempNvmeRecBPSize;
  UINT8                                   *NvmeRecBPBuffer;
  UINT8                                   *HostMemoryBuffer;
  UINT8                                   Sha256[SHA256_DIGEST_SIZE];

  TempNvmeRecBPBuffer = NULL;
  NvmeRecBPBuffer     = NULL;
  HostMemoryBuffer    = NULL;
  NvmeRecBPSize       = 0;

  ZeroMem (DCBDStru, NVME_RPMB_CONTENTS_SIZE);
  ReadReceiveBuffer = AllocateZeroPool (NVME_RPMB_DATA_TOTAL_SIZE);
  //
  // Allocate a physically contiguous memory buffer in the host to store the contents of a Boot Partition.
  //
  TempNvmeRecBPSize   = NVME_RECOVERY_DESCRIPTOR_TEM_SIZE + NVME_IFWI_IMAGE_SIZE;      // 64KB is enough for recovery descriptor
  TempNvmeRecBPBuffer = AllocateZeroPool (TempNvmeRecBPSize);
  if (TempNvmeRecBPBuffer == NULL) {
    DEBUG ((DEBUG_ERROR,"Warning: Run out memory. \n"));
    goto Exit;
  }
  Status = ConstructNvmeRecoveryDescriptorAndData (TempNvmeRecBPBuffer, &TempNvmeRecBPSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Create NvmeRecovery Descriptor Fail [%r] \n", Status));
  }

  NvmeRecBPSize = TempNvmeRecBPSize % (NVME_MINIMUM_PORTION_WRITE_UNIT) == 0 ?
                  TempNvmeRecBPSize :
                  (TempNvmeRecBPSize / NVME_MINIMUM_PORTION_WRITE_UNIT + 1) * NVME_MINIMUM_PORTION_WRITE_UNIT ;
  NvmeRecBPBuffer = AllocateZeroPool (NvmeRecBPSize);
  SetMem (NvmeRecBPBuffer, NvmeRecBPSize, 0xFF);
  CopyMem (NvmeRecBPBuffer, TempNvmeRecBPBuffer, TempNvmeRecBPSize);
  CreateSha256Hash (NvmeRecBPBuffer, TempNvmeRecBPSize, Sha256);
  CopyMem (((NVME_IMAGE_HEADER *)NvmeRecBPBuffer)->Hash, Sha256, SHA256_DIGEST_SIZE);

  if (TempNvmeRecBPBuffer != NULL) {
    FreePool(TempNvmeRecBPBuffer);
    TempNvmeRecBPBuffer = NULL;
  }

  Status = NvmeAuthenticatedDeviceConfigurationBlockRead (Ssp, MediaId, ReadReceiveBuffer, NVME_RPMB_DATA_FRAME_SIZE + NVME_RPMB_CONTENTS_SIZE);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Warning: RPMB Configuration Block Read: something wrong happened, Status[%x]. \n", Status));
    goto Exit;
  } else {
    CopyMem (DCBDStru, ReadReceiveBuffer + NVME_RPMB_DATA_FRAME_SIZE, NVME_RPMB_CONTENTS_SIZE);
    DEBUG ((DEBUG_INFO, "DCBDS[BPPEnable]=[%x], DCBDS[BPLock]=[%x], DCBDS[NWPAuthCtrl]=[%x].\n", DCBDStru[BPPEnable], DCBDStru[BPLock], DCBDStru[NWPAuthCtrl]));
  }

  if ((DCBDStru[BPLock] & BIT0) != 0x00) { // so far, only partition 0 support image backup, BP2 will be ignored.
    DEBUG ((DEBUG_INFO,"  Boot Partition 0 is locked. \n"));

    DCBDStru[BPLock] = 0;
    Status = NvmeAuthenticatedDeviceConfigurationBlockWrite (Ssp, MediaId, DCBDStru, NVME_RPMB_CONTENTS_SIZE);   // unlock partition 0
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR,"Boot Partition unlocking is failed, Status[%x]. \n", Status));
      goto Exit;
    } else {
      DEBUG ((DEBUG_INFO,"RPMB BP unlock is Successful! \n"));
    }

    Status = NvmeAuthenticatedDeviceConfigurationBlockRead (Ssp, MediaId, ReadReceiveBuffer, NVME_RPMB_DATA_FRAME_SIZE + NVME_RPMB_CONTENTS_SIZE);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR," Warning: RPMB Configuration Block Read: something wrong happened, Status[%x]. \n", Status));
      goto Exit;
    } else {
      CopyMem (DCBDStru, ReadReceiveBuffer + NVME_RPMB_DATA_FRAME_SIZE, NVME_RPMB_CONTENTS_SIZE);
      DEBUG ((DEBUG_INFO,"DCBDS[BPPEnable]=[%x], DCBDS[BPLock]=[%x], DCBDS[NWPAuthCtrl]=[%x].\n", DCBDStru[BPPEnable], DCBDStru[BPLock], DCBDStru[NWPAuthCtrl]));
    }

    Status = NvmeBootPartitionWrite (Passthru, NamespaceId, NvmeRecBPBuffer, NvmeRecBPSize);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR,"DCBD write is failed with status:[%x].\n", Status));
      goto Exit;
    }
  } else {
    DEBUG ((DEBUG_INFO,"Boot Partition 0 is not locked.\n"));
    Status = NvmeBootPartitionWrite (Passthru, NamespaceId, NvmeRecBPBuffer, NvmeRecBPSize);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR,"DCBD write is failed with status:[%x].\n", Status));
      goto Exit;
    }
  }
  //
  //******************************************Lock Partition********************************************************//
  //
  DCBDStru[BPPEnable] = 1;
  DCBDStru[BPLock]    = 1;
  Status = NvmeAuthenticatedDeviceConfigurationBlockWrite (Ssp, MediaId, DCBDStru, NVME_RPMB_CONTENTS_SIZE);   // lock partition 0
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"  Boot Partition unlocking is failed, Status[%x]. \n", Status));
    goto Exit;
  } else {
    DEBUG ((DEBUG_INFO," RPMB BP unlock is Successful. \n"));
  }

  Status = NvmeAuthenticatedDeviceConfigurationBlockRead (Ssp, MediaId, ReadReceiveBuffer, NVME_RPMB_DATA_FRAME_SIZE + NVME_RPMB_CONTENTS_SIZE);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR," Warning: RPMB Configuration Block Read: something wrong happened, Status[%x]. \n", Status));
    goto Exit;
  } else {
    CopyMem (DCBDStru, ReadReceiveBuffer + NVME_RPMB_DATA_FRAME_SIZE, NVME_RPMB_CONTENTS_SIZE);
    DEBUG ((DEBUG_INFO,"DCBDS[BPPEnable]=[%x], DCBDS[BPLock]=[%x], DCBDS[NWPAuthCtrl]=[%x].\n", DCBDStru[BPPEnable], DCBDStru[BPLock], DCBDStru[NWPAuthCtrl]));
  }

  //
  // Read back and do integrity check
  //
  Status = NvmeBootPartitionRead (Ssp, Passthru, NamespaceId, &HostMemoryBuffer, NvmeRecBPSize);
  if (EFI_ERROR (Status)) {
     DEBUG ((DEBUG_ERROR,"  Boot Partition Read is failed, Status[%x]. \n", Status));
  }

  if (CompareMem (HostMemoryBuffer, NvmeRecBPBuffer, NvmeRecBPSize) == 0) {
     DEBUG ((DEBUG_INFO,"  SPI2NVMe Backup Succeeded! \n"));
  }

Exit:
  if (HostMemoryBuffer != NULL) {
    FreePool (HostMemoryBuffer);
  }

  if (NvmeRecBPBuffer != NULL) {
    FreePool (NvmeRecBPBuffer);
  }

  if (ReadReceiveBuffer != NULL) {
    FreePool (ReadReceiveBuffer);
  }

  return Status;
}

/**
  Check current device is an VMD device or not.

  @param  PciIoProtocol        A pointer to pciio protocol

  @retval TRUE      vmd enabled on this device.
  @retval FALSE     vmd disabled on this device

**/

BOOLEAN
EFIAPI
IsVmdDevice(
  IN EFI_PCI_IO_PROTOCOL       *PciIoProtocol
  )
{
  BOOLEAN              IsVmd = FALSE;
  UINTN                Segment;
  UINTN                Bus;
  UINTN                Device;
  UINTN                Function;

  if (PciIoProtocol == NULL) {
    return IsVmd;
  }

  //
  // Now further check the PCI header
  //
  PciIoProtocol->GetLocation (PciIoProtocol, &Segment, &Bus, &Device, &Function);
  DEBUG ((DEBUG_INFO, "Bus Device and Function are: %x %x %x. \n", Bus, Device, Function));
  if ((GetVmdBusNumber () == Bus) && (GetVmdDevNumber () == Device) && (GetVmdFuncNumber () == Function)) {
    IsVmd = TRUE;
  }
  else
    IsVmd = FALSE;

  return IsVmd;
}

/**
  get current device's namespace ID

  @param  DevicePath        Current device's path

  @retval NamespaceId       Current device's namespace ID

**/

UINT32
EFIAPI
GetNamespaceId (
  IN  EFI_DEVICE_PATH_PROTOCOL                    *DevicePath
  )
{
  EFI_DEVICE_PATH_PROTOCOL         *TempDevicePath;
  NVME_NAMESPACE_DEVICE_PATH       *Node;

  TempDevicePath = (EFI_DEVICE_PATH_PROTOCOL *) DevicePath;
  while (!IsDevicePathEndType (TempDevicePath)) {
    if (DevicePathType (TempDevicePath) == MESSAGING_DEVICE_PATH &&
          DevicePathSubType (TempDevicePath) == MSG_NVME_NAMESPACE_DP) {
      Node  = (NVME_NAMESPACE_DEVICE_PATH *)TempDevicePath;
      return Node->NamespaceId;
    }
    TempDevicePath = NextDevicePathNode (TempDevicePath);
  }
  return NVME_ALL_NAMESPACEID;
}

/**
  get current device's protocol's interface

  @param  DevicePath        Current device's path
  @param  Protocol          Current protocol GUID

  @retval Interface       Current protocol GUID's content return as a pointer

**/

EFI_STATUS
EFIAPI
GetProtocolInterface (
  IN EFI_DEVICE_PATH_PROTOCOL      *DevicePath,
  IN EFI_GUID                      *Protocol,
  OUT VOID                         **Interface
  )
{
  EFI_STATUS                       Status;
  EFI_HANDLE                       Handle;
  EFI_DEVICE_PATH_PROTOCOL         *DevicePathTemp;

  if((DevicePath == NULL) || (Protocol == NULL) || (Interface == NULL)) {
    return EFI_INVALID_PARAMETER;
  }
  DevicePathTemp = DevicePath;
  Status = gBS->LocateDevicePath (
                  Protocol,
                  &DevicePathTemp,
                  &Handle
                  );
  if (!EFI_ERROR (Status)) {
    Status = gBS->HandleProtocol (
                  Handle,
                  Protocol,
                  (VOID**)Interface
                  );
    if(EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "%a: HandleProtocol for interface is %r. \n", __FUNCTION__, Status));
      return EFI_NOT_FOUND;
    }
  } else {
    DEBUG ((DEBUG_ERROR, "%a: LocateDevicePath for handle is %r. \n", __FUNCTION__, Status));
    return EFI_NOT_FOUND;
  }
  return EFI_SUCCESS;
}

/**
  read current device boot partition capability and information

  @param  CfgSpaceAddress        Current device's config space address.
  @param  PciIo                  Current device's pciio protocol pointer.
  @param  IsVmd                  VMD enabled flag.
  @param  NamespaceId            Current device's namespace ID

  @retval TRUE                   boot partition is supported on this device.
  @retval FALSE                  boot partition is not supported on this device.

**/

BOOLEAN
EFIAPI
ReadBpCapability (
  EFI_PHYSICAL_ADDRESS        CfgSpaceAddress,
  EFI_PCI_IO_PROTOCOL         *PciIo,
  BOOLEAN                     IsVmd,
  UINT32                      NamespaceId
  )
{
  EFI_STATUS      Status;
  BOOLEAN         IsBpSupport;
  NVME_BPINFO     *BPINFO;
  UINTN           TempdataSize;
  UINTN           TempCap;
  NVME_CAP        *Cap;
  EFI_PHYSICAL_ADDRESS MemSpaceAddress;
  UINT32          TempInfo;
  UINT32          Bar0Address;
  UINT32          Bar1Address;

  IsBpSupport = FALSE;

  if(PciIo == NULL) {
    return IsBpSupport;
  }

  if(IsVmd) {
    //
    // get mem space bar address
    //
    Bar0Address = MmioRead32 (CfgSpaceAddress + CFG_SPACE_BAR0_OFFSET);
    Bar1Address = MmioRead32 (CfgSpaceAddress + CFG_SPACE_BAR1_OFFSET);

    Bar0Address = Bar0Address & 0xFFFFFFF0;
    MemSpaceAddress = Bar1Address;
    MemSpaceAddress = LShiftU64 (MemSpaceAddress, 32);
    MemSpaceAddress += Bar0Address;

    TempCap = MmioRead64 (MemSpaceAddress + NVME_CAP_OFFSET);
    TempInfo = MmioRead32 (MemSpaceAddress + NVME_BPINFO_OFFSET);
    DEBUG ((DEBUG_INFO, "%a: Bar0Address = 0x%x, Bar1Address =0x%x\n", __FUNCTION__, Bar0Address, Bar1Address));
    DEBUG ((DEBUG_INFO, "%a: BpInfo = 0x%x, Capability =0x%lx\n", __FUNCTION__, TempInfo, TempCap));
  } else {

    Status = ReadNvmeController64 (PciIo, &TempCap, NVME_CAP_OFFSET);
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "Warning: Read Offset 0h: CAP, failed with status[%x]. \n", Status));
      return IsBpSupport;
    }
    //
    // Check if it was actived or not
    //
    Status = ReadNvmeController32 (PciIo, &TempInfo, NVME_BPINFO_OFFSET);
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "Warning: Read Offset 40h: BPINFO, failed with status[%x]. \n", Status));
      return IsBpSupport;
    }
  }

  Cap = (NVME_CAP *)(&TempCap);
  BPINFO = (NVME_BPINFO *)(&TempInfo);
  //
  // Check if support Boot Partition
  //
  if (Cap->BPS != 0x01) {
     DEBUG ((DEBUG_INFO, "The NVMe Controller doesn't support boot Partition. \n"));
     return IsBpSupport;
  }
  //
  // check key capabilities -- Read Partition size
  //
  if (BPINFO->BPSZ != 0x00) {
    TempdataSize = (BPINFO->BPSZ * 128) * 1024;
    DEBUG ((DEBUG_INFO,"  The size of NVMe Boot Partition is [%d] KB. \n" , TempdataSize / 1024));
  }

  if (BPINFO->ABPID == 0x00) {
    DEBUG ((DEBUG_INFO,"  Warning: NVMe Boot Partition is not actived. \n"));
  }
  IsBpSupport = TRUE;
  DEBUG ((DEBUG_INFO, "%a: Is Boot partition Support: %d. \n", __FUNCTION__, IsBpSupport));
  return IsBpSupport;
}

/**
  Get the bridge information from device path.

  @param[IN]  DevicePath            The NVMe device path.
  @param[OUT] BridgeDev             The device id of the bridge.
  @param[OUT] BridgeFun             The function id of the bridge.

  @return EFI_SUCCESS      Successfully get the bridge information.
  @return EFI_NOT_FOUND    Fail to find the bridge information.

**/
EFI_STATUS
EFIAPI
NvmeRecoveryGetDFFromDevicePath (
  IN  EFI_DEVICE_PATH_PROTOCOL         *DevicePath,
  OUT UINTN                            *BridgeDev,
  OUT UINTN                            *BridgeFun
  )
{
  EFI_DEVICE_PATH_PROTOCOL          *TempDevicePath;
  PCI_DEVICE_PATH                   *PciNode;

  if((DevicePath == NULL) || (BridgeDev == NULL) || (BridgeFun == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  TempDevicePath = (EFI_DEVICE_PATH_PROTOCOL *) DevicePath;
  while (!IsDevicePathEndType (TempDevicePath)) {
    if (DevicePathType (TempDevicePath) == HARDWARE_DEVICE_PATH &&
          DevicePathSubType (TempDevicePath) == HW_PCI_DP) {
      PciNode = (PCI_DEVICE_PATH*)TempDevicePath;
      *BridgeDev = PciNode->Device;
      *BridgeFun = PciNode->Function;
      return EFI_SUCCESS;
    }
    TempDevicePath = NextDevicePathNode (TempDevicePath);
  }

  return EFI_NOT_FOUND;
}

/**
  check current device is on the right port or not.

  @param[IN]  IsVmd                 VMD enabled or not.
  @param[IN] DevicePath             Current device path.
  @param[OUT] NameSpace             Current device name space ID.
  @param[OUT] CfgSpaceAddress       config space address with VMD enabled case

  @return TRUE                      This is the supported slot for image write
  @return FALSE                     This is not an supported slot

**/
BOOLEAN
EFIAPI
IsSupportedRootportDevice (
  IN  BOOLEAN                                 IsVmd,
  IN  EFI_DEVICE_PATH_PROTOCOL                *DevicePath,
  OUT UINT32                                  *NameSpace,
  OUT EFI_PHYSICAL_ADDRESS                    *CfgSpaceAddress
  )
{
  EFI_STATUS                                Status;
  EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL      *RstCfgSpace;
  UINT32                                    PortId;
  UINT32                                    NamespaceId;
  UINTN                                     Bus;
  UINTN                                     Device;
  UINTN                                     RpDevice;
  UINTN                                     Function;
  UINTN                                     RpFunction;
  UINTN                                     PortNumber;
  UINTN                                     RefPortNumber;
  UINT64                                    PciBase;
  UINT8                                     CapHeaderOffset;


  RstCfgSpace         = NULL;
  Bus                 = 0;
  Device              = 0;
  Function            = 0;
  NamespaceId         = NVME_ALL_NAMESPACEID;

  RefPortNumber = PcdGet32 (PcdNvmeRecoveryPrimarySlotPortNumber);

  if (RefPortNumber == 0) {
    DEBUG ((DEBUG_ERROR, " not supported RVP board\n"));
    return FALSE;
  }
  //
  // call rst nvme passthrough protocol to get cfg space, namespace and BDF data
  //
  if (IsVmd) {
    DEBUG ((DEBUG_INFO, "%a: try to get rst config space protocol\n", __FUNCTION__));
    Status = GetProtocolInterface (
               DevicePath,
               &gRSTConfigSpaceAccessProtocolGuid,
               (VOID**)&RstCfgSpace
               );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "%a: can't get RST config space protocol %r. \n", __FUNCTION__, Status));
      RstCfgSpace = NULL;
      return FALSE;
    }
    //
    // get namespaceID for current device with VMD enabled.
    //
    NamespaceId = GetNamespaceId (DevicePath);
    DEBUG ((DEBUG_INFO, "%a: NamespaceId is 0x%x. \n", __FUNCTION__, NamespaceId));
    NamespaceId = NamespaceId & 0x7FFFFFFF;
    PortId = 0;
    while (PortId < 31) {
    if ((NamespaceId & (1 << PortId)) != 0) {
      break;
    }
    PortId++;
    }
    NamespaceId = PortId + 1;
    //
    // get current device BDF for further clarify which device is righe one for backup image
    //
    Status = RstCfgSpace->GetBDF (RstCfgSpace, NamespaceId, &Bus, &Device, &Function);
    if (Status != EFI_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "unable to get BDF from RST = %r.\n", Status));
    }
    DEBUG ((DEBUG_INFO, "VMD enabled Bus Device and Function are: %x %x %x. \n", Bus, Device, Function));
    //
    // call RST api to check Caps and BPINFO
    //
    Status = RstCfgSpace->GetAddress (RstCfgSpace, NamespaceId, CfgSpaceAddress);
    if (Status != EFI_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "unable to get config space address from RST = %r.\n", Status));
    }
    //
    // port number started from index 0, so -1 here
    //
    GetPchPcieRpDevFun ((RefPortNumber - 1), &RpDevice, &RpFunction);
    *NameSpace = NamespaceId;
    DEBUG ((DEBUG_INFO, "VMD enabled calculated Bus Device and Function are: %x %x %x. \n", Bus, RpDevice, RpFunction));
    if ((Device != RpDevice) || (Function != RpFunction))
      return FALSE;
  } else {
    *NameSpace = NVME_CONTROLLER_ID;
    //
    // Get BDF from device path
    //
    Status = NvmeRecoveryGetDFFromDevicePath (DevicePath, &Device, &Function);
    PciBase = PCI_SEGMENT_LIB_ADDRESS (0, 0, Device, Function, 0);
    CapHeaderOffset = PcieFindCapId (0, 0, (UINT8)Device, (UINT8)Function, EFI_PCI_CAPABILITY_ID_PCIEXP);
    PortNumber = PciSegmentRead8 ((UINTN)PciBase + CapHeaderOffset + 0x0F);
    DEBUG ((DEBUG_INFO, "calculated d f port are: %d %d %d %d %lx\n", Device, Function, PortNumber, CapHeaderOffset, PciBase));
    if (PortNumber != RefPortNumber)
      return FALSE;
  }
  return TRUE;
}

/**
  It determines if current boot process is normal, then healthy image will
  be saved to NVMe Boot Partition.

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
NvmeRecoveryCallBack (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS                              Status;
  UINTN                                   HandleCount;
  EFI_HANDLE                              *HandleBuffer;
  UINTN                                   Index;
  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp;
  EFI_DEVICE_PATH_PROTOCOL                *DevicePath;
  EFI_BLOCK_IO_PROTOCOL                   *BlockIo;
  UINT32                                  MediaId;
  SYSTEM_FIRMWARE_UPDATE_PROGRESS         *PreviousUpdateProgress;
  VOID                                    *BootState;
  BOOLEAN                                 IsVmd;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL      *NvmeDevice;
  NVME_ADMIN_CONTROLLER_DATA              *ControllerData;
  EFI_PCI_IO_PROTOCOL                     *PciIo;
  UINT32                                  NamespaceId;
  BOOLEAN                                 BpSupport;
  BOOLEAN                                 IsPortSupported;
  EFI_PHYSICAL_ADDRESS                    CfgSpaceAddress;

  Ssp                 = NULL;
  Status              = EFI_SUCCESS;
  MediaId             = 0;
  NamespaceId         = NVME_ALL_NAMESPACEID;
  IsVmd               = FALSE;
  IsPortSupported     = FALSE;

  //
  // Disarm EC WDT first, since from RPL EC, WDT is armed as default, need disarm at the beginning.
  //
  Status = DisArmEcWdt ();
  DEBUG ((DEBUG_INFO, "DisArmEcWdt: %r\n", Status));

  //
  // First boot check
  //
  Status = GetVariable2 (SYSFW_UPDATE_PROGRESS_VARIABLE_NAME, &gSysFwUpdateProgressGuid, (VOID **) &PreviousUpdateProgress, NULL);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "<NVME debug>Get UpdateProgress variable = %r.\n", Status));
  }

  GetVariable2 (L"BootState", &gBootStateGuid, &BootState, NULL);
  if ((BootState != NULL)
   && (PreviousUpdateProgress != NULL)
   && (PreviousUpdateProgress->Component != UpdatingResiliency)) {
    DEBUG ((DEBUG_INFO, "<NVMe backup check> not first boot, no need backup. \n"));
    return;
  }
  DEBUG ((DEBUG_INFO, "<NVMe backup check> This is the first boot. \n"));

  //
  // Locate all SSP protocol instances.
  //
  HandleCount  = 0;
  HandleBuffer = NULL;
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiStorageSecurityCommandProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status) || (HandleCount == 0) || (HandleBuffer == NULL)) {
    DEBUG ((DEBUG_ERROR, "Warning: gEfiStorageSecurityCommandProtocolGuid is not existed. \n"));
    ClearUpdateProgress ();
    return;
  }
  DEBUG ((DEBUG_INFO, "We found [%d] StorageSecurity instances totally. \n", HandleCount));

  for (Index = 0; Index < HandleCount; Index ++) {
    Status = gBS->HandleProtocol(
                    HandleBuffer[Index],
                    &gEfiDevicePathProtocolGuid,
                    (VOID **) &DevicePath
                    );

    if (EFI_ERROR (Status)) {
      continue;
    }
    DEBUG ((DEBUG_INFO, "%a: DevicePath: %s\n",
      __FUNCTION__, ConvertDevicePathToText (DevicePath, TRUE, TRUE)));

    if (!IsNvmeDevice (DevicePath)) {
       DEBUG ((DEBUG_ERROR, "It is not a NVMe device. \n"));
       continue;
    } else {
      //
      // try to get the PciIo interface.
      //
      Status = GetProtocolInterface (
                 DevicePath,
                 &gEfiPciIoProtocolGuid,
                 (VOID**)&PciIo
                 );
      if(EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "%a: HandleProtocol for PciIo is %r. \n", __FUNCTION__, Status));
        PciIo = NULL;
      } else {
        IsVmd = IsVmdDevice (PciIo);
      }

      //
      // try to get the PassThrou interface.
      //
      Status = GetProtocolInterface (
                 DevicePath,
                 &gEfiNvmExpressPassThruProtocolGuid,
                 (VOID**)&NvmeDevice
                 );
      if(EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "%a: HandleProtocol for Passthrough is %r. \n", __FUNCTION__, Status));
        NvmeDevice = NULL;
      }

      if((NvmeDevice == NULL) || (PciIo == NULL)) {
        DEBUG ((DEBUG_ERROR, "%a: passthrough or PciIo is NULL %r. \n", __FUNCTION__, Status));
        continue;
      }

      DEBUG ((DEBUG_INFO, "%a: NvmeDevice = 0x%x, PciIo =0x%x\n", __FUNCTION__, NvmeDevice, PciIo));

      IsPortSupported = IsSupportedRootportDevice (
                         IsVmd,
                         DevicePath,
                         &NamespaceId,
                         &CfgSpaceAddress
                         );

      if (!IsPortSupported)
        continue;
      //
      // Found the NVMe controller, try to get the SSP interface.
      //
      Status = gBS->HandleProtocol(
                      HandleBuffer[Index],
                      &gEfiStorageSecurityCommandProtocolGuid,
                      (VOID **) &Ssp
                      );

      if (EFI_ERROR (Status)) {
        continue;
      } else {
        Status = gBS->HandleProtocol(
                    HandleBuffer[Index],
                    &gEfiBlockIoProtocolGuid,
                    (VOID **) &BlockIo
                    );

        if (EFI_ERROR (Status)) {
          DEBUG ((DEBUG_ERROR, "Warning: Not support BlockIo, continue to find another one. \n"));
          MediaId = 0;
        } else {
          MediaId = BlockIo->Media->MediaId;
        }
        DEBUG ((DEBUG_INFO, "NVMe: Current MediaID == [%x].\n", MediaId));

        ControllerData = (NVME_ADMIN_CONTROLLER_DATA *)AllocateZeroPool (sizeof (NVME_ADMIN_CONTROLLER_DATA));
        Status = DumpNvmeIdentifyControllerData (NvmeDevice, NamespaceId, ControllerData);
        if (EFI_ERROR (Status)) {
           DEBUG ((DEBUG_ERROR, "Warning: Dump RPMB data failed. \n"));
        }

        //
        // Check the capability if support Boot partition or not
        //
        BpSupport = ReadBpCapability (CfgSpaceAddress, PciIo, IsVmd, NamespaceId);
        if (BpSupport == FALSE) {
          continue;
        }

        //
        // ******* To be carefule here, don't programm key to your NVMe device without cosensus.
        //
        Status = NvmeBasedRecoveryGetSetRpmbKey (Ssp, MediaId);
        if (EFI_ERROR (Status)) {
          DEBUG ((DEBUG_ERROR,"Nvme RPMB Key Provisioning is failed, Status[%x]. \n", Status));
        } else {
          DEBUG ((DEBUG_INFO,"Nvme RPMB Key Provisioning is Successful! \n"));
        }

        Status = ImageBackUpSpi2Nvme (NvmeDevice, NamespaceId, Ssp, MediaId);
        DEBUG ((DEBUG_INFO,"  SPI2NVMe Backup is executed with [%x]! \n", Status));
      }
    }
    if ((Ssp  == NULL)) {
      DEBUG ((DEBUG_ERROR, "NVMe does not exist. \n"));
    }
  }

  if (HandleBuffer != NULL) {
    FreePool (HandleBuffer);
  }

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Unknow Failure is found. \n"));
  }

  //
  // Clear progress variable
  //
  ClearUpdateProgress ();

  ZeroMem (mRpmbKey256, sizeof (mRpmbKey256));
  DEBUG ((DEBUG_INFO, "NvmeRecoveryCallBack End.\n"));

  return;
}


/**
  Main entrypoint for NVMe Recovery Initialization.

  @param[in]  ImageHandle     The image handle.
  @param[in]  SystemTable     The system table.

  @retval EFI_SUCCESS            Command completed successfully.
  @retval EFI_INVALID_PARAMETER  Command usage error.
  @retval EFI_OUT_OF_RESOURCES   Not enough resources were available to run the command.
  @retval EFI_ABORTED            Aborted by user.
  @retval EFI_NOT_FOUND          The specified PCD is not found.
  @retval Others                 Error status returned from gBS->LocateProtocol.
**/
EFI_STATUS
EFIAPI
NvmeRecoveryDxeEntryPoint (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS                Status;
  EFI_EVENT                 BeforeEndOfDxeEvent;

  Status                    = EFI_SUCCESS;
  BeforeEndOfDxeEvent       = NULL;

  DEBUG ((DEBUG_INFO, "NvmeRecoveryDxeEntryPoint Start.\n"));
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  NvmeRecoveryCallBack,
                  NULL,
                  &gPlatformBeforeEndOfDxeEventGroupGuid,
                  &BeforeEndOfDxeEvent
                  );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to Register an EndOfDxe CallBack function for NVMe recovery, Status:[%x].\n", Status));
  }

  DEBUG ((DEBUG_INFO, "NvmeRecoveryDxeEntryPoint End.\n"));
  return Status;
}

