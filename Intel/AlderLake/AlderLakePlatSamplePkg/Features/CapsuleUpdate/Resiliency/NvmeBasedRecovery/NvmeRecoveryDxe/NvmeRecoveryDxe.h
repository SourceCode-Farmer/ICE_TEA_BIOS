/**@file
  NvmeRecovery Dxe driver, main entry of the Nvme recovery feature.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _NVME_RECOVERY_H_
#define _NVME_RECOVERY_H_

#include <Uefi.h>
#include <IndustryStandard/Pci.h>

#include <Library/BaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/PchPcieRpLib.h>
#include <Protocol/Spi.h>
#include <Guid/SysFwUpdateProgress.h>
#include <Library/EcMiscLib.h>
#include <Library/VmdInfoLib.h>
#include "NvmeDevice.h"
#include <Protocol/RstConfigSpaceAccess.h>
#include <Library/IoLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PcieHelperLib.h>
#include <Library/SeamlessRecoverySupportLib.h>
#include <Library/HobLib.h>
#include <BupMsgs.h>


#define SYSFW_UPDATE_PROGRESS_VARIABLE_NAME              L"SysFwUpdateProgress"

#define NVME_RECOVERY_DESCRIPTOR_HEADER_SIGNATURE     SIGNATURE_32 ('N','R','D','H')
#define NVME_RECOVERY_DESCRIPTOR_ELEMENT_SIGNATURE    SIGNATURE_32 ('R','E','C','V')
#define NVME_NORECOVERY_DESCRIPTOR_ELEMENT_SIGNATURE  SIGNATURE_32 ('N','O','N','E')

#define CFG_SPACE_BAR0_OFFSET      0x10
#define CFG_SPACE_BAR1_OFFSET      0x14

//
// NVMe Recovery componnets list
//
typedef enum {
  RECV_HEAD = 0,
  RECV_BIOS,
  RECV_CSME,
} NVME_RECOVERY_ELEMENT_INDEX;

typedef enum {
  BPPEnable    = 0,
  BPLock,
  NWPAuthCtrl
} NVME_DCBD_STRUCT;

//
// Hash algorithm support list
//
typedef enum {
  RECV_SHA256 = 1,
  RECV_SHA384,
  RECV_SHA512
} NVME_SUPPORTED_HASH_TYPE;

typedef struct{
  UINT32    Type;
  UINT32    StructVersion;
  UINT32    IpType;
  UINT32    TotalSize;
  UINT32    NumberOfElements;
  UINT32    Reserved;
} NVME_IMAGE_STRUCT;

typedef struct{
  NVME_IMAGE_STRUCT  NvmeImageStruct;
  UINT32             HashType;          // 1: SHA128 2: SHA256 3:SHA384
  UINT8              Hash[128];
} NVME_IMAGE_HEADER;

typedef struct{
  UINT32    UpdateFlag;
  UINT32    OffsetImage;
  UINT32    SizeImage;
  UINT32    OffsetSpi;
  UINT32    SizeSpi;
} NVME_RESTORE_ELEMENT;

#define NVME_RECOVERY_NUMOFIMAGES             2                           // Bios and CSME
#define NVME_RECOVERY_NUMOFELEMENTS           3                           // Bios, CSME Code and CSME Data
#define NVME_RECOVERY_DESCRIPTOR_SIZE         sizeof (NVME_IMAGE_HEADER) + NVME_RECOVERY_NUMOFIMAGES * sizeof (NVME_IMAGE_STRUCT) + NVME_RECOVERY_NUMOFELEMENTS * sizeof (NVME_RESTORE_ELEMENT)
#define NVME_ALL_NAMESPACEID                  0XFFFFFFFF

#endif

