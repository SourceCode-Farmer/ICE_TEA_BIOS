/**@file
  The file includes major RPMB access and Boot partion read/write commands
  based on NVMe1.4 spec.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <NvmeDevice.h>

extern GLOBAL_REMOVE_IF_UNREFERENCED CONST UINT8 mNvmeDefaultNonce[16];

/**
  Return the result of RPMB Data Frame based on Figure 465: RPMB Operation Result in Nvme 1.4.

  @param  RpmbDataFrame          The pointer to the GetNvmeRPMBOperationResult data structure.

  @retval EFI_SUCCESS            Successfully read/write the RPMB Configuration Block.
  @retval EFI_DEVICE_ERROR       Fail to read/write the RPMB Configuration Block.
  @retval EFI_INVALID_PARAMETER  the input pointer is invalid.

**/
EFI_STATUS
GetNvmeRpmbOperationResult (
  IN NVME_RPMB_DATA_FRAME                     *RpmbDataFrame
  )
{
  if (RpmbDataFrame == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (RpmbDataFrame->Result == 0x0000) {
    DEBUG ((DEBUG_INFO, "Operation successful ! \n" ));
    return EFI_SUCCESS;
  } else if (RpmbDataFrame->Result == 0x0001) {
    DEBUG ((DEBUG_ERROR, "General failure ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0002) {
    DEBUG ((DEBUG_ERROR, "Authentication failure ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0003) {
    DEBUG ((DEBUG_ERROR, "Counter failure ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0004) {
    DEBUG ((DEBUG_ERROR, "Address failure ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0005) {
    DEBUG ((DEBUG_ERROR, "Write failure(data/counter/result write failure) ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0006) {
    DEBUG ((DEBUG_ERROR, "Read failure(data/counter/result write failure) ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0007) {
    DEBUG ((DEBUG_ERROR, "Authentication Key not yet programmed ! \n" ));
    return EFI_DEVICE_ERROR;
  } else if (RpmbDataFrame->Result == 0x0008) {
    DEBUG ((DEBUG_ERROR, "Invalid RPMB Device Configuration Block ! \n" ));
    return EFI_DEVICE_ERROR;
  } else {
    DEBUG ((DEBUG_ERROR, "Unknow Operation Result ! \n" ));
    return EFI_DEVICE_ERROR;
  }
}

/**
  The Firmware Image Download command is used to download all or a portion of an image for a future
  update to the controller.

  @param  Passthru      The pointer to the EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL protocol.
  @param  NamespaceId   The NamespaceId for current device.
  @param  Buffer        The buffer used to store value read from an external image.
  @param  Length        The size of image.
  @param  DataOffset    Current dataoffset of the buffer in a loop.

  @retval EFI_SUCCESS                The NVM Express Command Packet was sent by the host. TransferLength bytes were transferred
                                     to, or from DataBuffer.
  @retval EFI_BAD_BUFFER_SIZE        The NVM Express Command Packet was not executed. The number of bytes that could be transferred
                                     is returned in TransferLength.
  @retval EFI_NOT_READY              The NVM Express Command Packet could not be sent because the controller is not ready. The caller
                                     may retry again later.
  @retval EFI_DEVICE_ERROR           A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER      NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid. The NVM
                                     Express Command Packet was not sent, so no additional status information is available.
  @retval EFI_UNSUPPORTED            The command described by the NVM Express Command Packet is not supported by the NVM Express
                                     controller. The NVM Express Command Packet was not sent so no additional status information
                                     is available.
  @retval EFI_TIMEOUT                A timeout occurred while waiting for the NVM Express Command Packet to execute.

**/
EFI_STATUS
EFIAPI
NvmeStorageFirmwareDownload (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL       *Passthru,
  IN UINT32                                   NamespaceId,
  IN VOID                                     *Buffer,
  IN UINTN                                    Length,
  IN UINTN                                    DataOffset
  )
{
  EFI_STATUS                               Status;
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                  Command;
  EFI_NVM_EXPRESS_COMPLETION               Completion;

  Status  = EFI_SUCCESS;

  if (Passthru == NULL || Buffer == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));

  Command.Nsid                 = NVME_CONTROLLER_ID;
  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;

  Command.Cdw0.Opcode          = NVME_ADMIN_FW_IAMGE_DOWNLOAD_CMD;
  CommandPacket.TransferBuffer = Buffer;
  CommandPacket.TransferLength = (UINT32) Length;
  CommandPacket.NvmeCmd->Cdw10 = (UINT32) ((Length / 4) - 1);
  CommandPacket.NvmeCmd->Cdw11 = (UINT32) (DataOffset / 4);

  CommandPacket.NvmeCmd->Flags = CDW10_VALID | CDW11_VALID;
  CommandPacket.NvmeCmd->Nsid  = 0;
  CommandPacket.CommandTimeout = NVME_GENERIC_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;

  Status = Passthru->PassThru (
                       Passthru,
                       NamespaceId,
                       &CommandPacket,
                       NULL
                       );

  return Status;
}

/**
  The Firmware Commit command is used to modify the firmware image or Boot Partitions.

  @param  Passthru      The pointer to the EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL protocol.
  @param  NamespaceId   The NamespaceId for current device.
  @param  CommitAction  This field specifies the action that is taken on the image downloaded with the Firmware Image
                        Download command or on a previously downloaded and placed image. (refer to Nvme1.4, Figure 175)

  @retval EFI_SUCCESS                The NVM Express Command Packet was sent by the host. TransferLength bytes were transferred
                                     to, or from DataBuffer.
  @retval EFI_BAD_BUFFER_SIZE        The NVM Express Command Packet was not executed. The number of bytes that could be transferred
                                     is returned in TransferLength.
  @retval EFI_NOT_READY              The NVM Express Command Packet could not be sent because the controller is not ready. The caller
                                     may retry again later.
  @retval EFI_DEVICE_ERROR           A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER      NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid. The NVM
                                     Express Command Packet was not sent, so no additional status information is available.
  @retval EFI_UNSUPPORTED            The command described by the NVM Express Command Packet is not supported by the NVM Express
                                     controller. The NVM Express Command Packet was not sent so no additional status information
                                     is available.
  @retval EFI_TIMEOUT                A timeout occurred while waiting for the NVM Express Command Packet to execute.

**/
EFI_STATUS
EFIAPI
NvmeStorageFirmwareCommit (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL     *Passthru,
  IN UINT32                                 NamespaceId,
  IN UINT32                                 CommitAction
  )
{
  EFI_STATUS                               Status;
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                  Command;
  EFI_NVM_EXPRESS_COMPLETION               Completion;

  if (Passthru == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status  = EFI_SUCCESS;
  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));

  Command.Nsid                 = NVME_CONTROLLER_ID;
  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;

  Command.Cdw0.Opcode          = NVME_ADMIN_FW_COMMIT_CMD;
  CommitAction = CommitAction << 3;
  CommandPacket.NvmeCmd->Cdw10 = 0 | CommitAction;

  CommandPacket.NvmeCmd->Flags = CDW10_VALID;
  CommandPacket.NvmeCmd->Nsid  = 0;
  CommandPacket.CommandTimeout = NVME_GENERIC_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;

  Status = Passthru->PassThru (
                       Passthru,
                       NamespaceId,
                       &CommandPacket,
                       NULL
                       );

  return Status;
}

/**
  Read Nvm Express Boot Partition data by Get Log Page command.

  @param  Passthru          The pointer to the EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL data structure.
  @param  NamespaceId       The NamespaceId for current device.
  @param  HostMemoryBuffer  The buffer which is allocated by Host used to store value read from controller.
  @param  DataByWords       The data number for each read.
  @param  DataOffset        The current data offset for whole block read.

  @retval EFI_SUCCESS      Successfully read the controller capability register content.
  @retval EFI_DEVICE_ERROR Fail to read the controller capability register.

**/
EFI_STATUS
EFIAPI
NvmeBootPartitionReadToHost2 (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL   *Passthru,
  IN UINT32                               NamespaceId,
  IN OUT VOID                             *HostMemoryBuffer,
  IN UINTN                                DataByWords,
  IN UINTN                                DataOffset
  )
{
  EFI_STATUS                                Status;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL        *NvmeDevice;
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET  CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                   Command;
  EFI_NVM_EXPRESS_COMPLETION                Completion;

  if (Passthru == NULL || HostMemoryBuffer == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status      = EFI_SUCCESS;
  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));
  NvmeDevice = Passthru;

  Command.Cdw0.Opcode          = NVME_ADMIN_GET_LOG_PAGE_CMD;
  Command.Nsid                 = NVME_CONTROLLER_ID;

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;
  CommandPacket.TransferBuffer = HostMemoryBuffer;
  CommandPacket.TransferLength = sizeof (NVME_BOOT_PARTITION_INFO_LOG);
  CommandPacket.CommandTimeout = NVME_GENERIC_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;
  //
  // Set Number of Dwords Lower (NUMDL) to the size of NVME_SMART_HEALTH_INFO_LOG,
  // Log Page Identifier (LID) to LID_SMART_INFO
  //
  Command.Cdw10                = ((((UINT32) DataByWords) / 4 - 1) << 16) | LID_BP_INFO;     // Bit 8 is 0, means parition 0;
  Command.Cdw11                = 0;                                                             // Hight value for GetlogPage, 0x100 0000;
  Command.Cdw12                = (UINT32) DataOffset + 16;
  Command.Cdw13                = 0;
  Command.Flags                = CDW10_VALID | CDW11_VALID | CDW12_VALID | CDW13_VALID;

  Status = NvmeDevice->PassThru (
                         NvmeDevice,
                         NamespaceId,
                         &CommandPacket,
                         NULL
                         );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "NvmeBootPartitionReadToHost2 : Sending Get Log Page Command, Status=[%r]. \n", Status));
  }

  return Status;
}

/**
  Authentication Key programming is initiated by a Security Send command to program the Authentication
  Key to the specified RPMB target, followed by a subsequent Security Send command to request the result,
  and lastly, the host issues a Security Receive command to retrieve the result.

  @param  Ssp        The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId    Id of the media, changes every time the media is replaced.
  @param  Key        The Authentication key should be updated to the specified RPMB target.
  @param  KeySize    Size of the key.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeAuthenticatedKeyDataProgramming (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId,
  IN  CONST VOID                              *Key,
  IN  UINTN                                   KeySize
  )
{
  EFI_STATUS                               Status;
  NVME_RPMB_DATA_FRAME                     RpmbDataFrame;
  UINT8                                    *ReceiveBuffer;
  UINTN                                    BufferSize;

  BufferSize    = 0;
  ReceiveBuffer = AllocateZeroPool (sizeof (NVME_RPMB_DATA_FRAME));
  if (ReceiveBuffer == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  if (KeySize >= RPMB_FRAME_STUFF_BYTES) {
    DEBUG ((DEBUG_ERROR, "Warning: The size of key is [%d], it is a invalidate key. \n", KeySize));
    return EFI_INVALID_PARAMETER;
  }

  ZeroMem (&RpmbDataFrame, sizeof (NVME_RPMB_DATA_FRAME));
  //
  // Security Send 1
  //
  CopyMem (&(RpmbDataFrame.SBAKAMC[RPMB_FRAME_STUFF_BYTES - KeySize]), Key, KeySize);
  RpmbDataFrame.RPMessage = 0x0001;
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                      // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,         // SecurityProtocol
                  0x0100,                         // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,      // PayloadBufferSize
                  &RpmbDataFrame                  // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Key Programming : Authenticated Key Data Send 1 Command operation Successfully. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Key Programming : Authenticated Key Data send 1 Command operation failed, Status:[%x]. \n", Status));
  }

  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Key Programming : Operation Result 1 is abnormal. \n"));
    goto Exit;
  }

  //
  // Security Send 2
  //
  ZeroMem (&RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  RpmbDataFrame.RPMessage = 0x0005;
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                     // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,        // SecurityProtocol
                  0x0100,                        // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,     // PayloadBufferSize,
                  &RpmbDataFrame                 // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Key Programming : Authenticated Key Data Send 2 Command operation succeeded.\n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Key Programming : Authenticated Key Data send 2 Command operation failed, Status:[%x].\n", Status));
  }

  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Key Programming : Operation Result 2 is abnormal. \n"));
    goto Exit;
  }

  //
  // Security Receive 1
  //
  Status = Ssp->ReceiveData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  (VOID *)ReceiveBuffer,        // PayloadBuffer
                  &BufferSize
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Key Programming : Data Receive failed, status [%x]. \n", Status));
    goto Exit;
  }

  Status = GetNvmeRpmbOperationResult ((NVME_RPMB_DATA_FRAME *)ReceiveBuffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Key Programming: Operation Result 3 is abnormal. \n"));
    goto Exit;
  }

Exit:

  if (ReceiveBuffer != NULL) {
    FreePool (ReceiveBuffer);
  }
  return Status;
}

/**
  The Read Write Counter Value sequence is initiated by a Security Send command to request the Write
  Counter value, followed by a Security Receive command to retrieve the Write Counter result.

  @param  Ssp            The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId        Id of the media, changes every time the media is replaced.
  @param  ReceiveBuffer  The buffer used to store RPMB data frame value read from controller.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeRpmbReadWriteCounterValue (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId,
  OUT NVME_RPMB_DATA_FRAME                    *ReceiveBuffer
  )
{
  EFI_STATUS                               Status;
  NVME_RPMB_DATA_FRAME                     RpmbDataFrame;
  UINTN                                    BufferSize;
  UINT8                                    MAC[SHA256_DIGEST_SIZE];
  UINTN                                    MACSize;
  UINT8                                    *Bufferhead;

  BufferSize = 0;
  MACSize    = SHA256_DIGEST_SIZE;
  ZeroMem (&RpmbDataFrame, sizeof (NVME_RPMB_DATA_FRAME));
  if (GetRandomNumber128 (RpmbDataFrame.Nonce) != TRUE) {
     DEBUG ((DEBUG_ERROR, "Warning: It is not a random value ! \n"));
     CopyMem (RpmbDataFrame.Nonce, mNvmeDefaultNonce, 16);
  }

  RpmbDataFrame.RPMessage = 0x0002;
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                      // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,         // SecurityProtocol
                  0x0100,                         // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,      // PayloadBufferSize
                  &RpmbDataFrame                  // PayloadBuffer
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB RWCounter : send Command failed, Status:[%x]. \n", Status));
    goto Exit;
  } else {
    DEBUG ((DEBUG_INFO, "RPMB RWCounter : Send Command succeeded. \n"));
  }

  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB ReadWriteCounterValue : Operation Result 1 is abnormal. \n"));
    goto Exit;
  }

  //
  // Read full supported security protocol list from device.
  //
  ZeroMem (ReceiveBuffer, sizeof (NVME_RPMB_DATA_FRAME));
  Status = Ssp->ReceiveData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  (VOID *)ReceiveBuffer,      // PayloadBuffer
                  &BufferSize
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Nvme RWCounter: receive Command failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  Bufferhead = (UINT8 *) ReceiveBuffer;
  CopyMem (&RpmbDataFrame, ReceiveBuffer, sizeof (NVME_RPMB_DATA_FRAME));
  CopyMem (MAC, &(RpmbDataFrame.SBAKAMC[RPMB_FRAME_STUFF_BYTES - SHA256_DIGEST_SIZE]), MACSize);

  if (HmacSha256Request (MAC, Bufferhead + RPMB_FRAME_STUFF_BYTES, NVME_RPMB_DATA_FRAME_SIZE - RPMB_FRAME_STUFF_BYTES, TRUE) == FALSE) {
    Status = EFI_DEVICE_ERROR;
  }

  Status = GetNvmeRpmbOperationResult ((NVME_RPMB_DATA_FRAME *)ReceiveBuffer);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB ReadWriteCounterValue : Operation Result 2 is abnormal. \n"));
    goto Exit;
  }

Exit:

  return Status;
}

/**
  The Authenticated Data Read implementation initiated by a Security Send command.

  @param  Ssp                The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId            Id of the media, changes every time the media is replaced.
  @param  RPMBTarget         RPMB target to access.
  @param  ReceiveBuffer      The pointer to the The data Number of 512B blocks.
  @param  SectorCount        Number of 512B blocks.
  @param  ReceiveBufferSize  The size of RPMB configuration block data.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeAuthenticatedDataRead (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId,
  IN  UINT8                                   RPMBTarget,
  OUT VOID                                    *ReceiveBuffer,
  IN  UINT8                                   SectorCount,
  IN  UINT32                                  ReceiveBufferSize
  )
{
  EFI_STATUS                               Status;
  NVME_RPMB_DATA_FRAME                     RpmbDataFrame;
  UINTN                                    BufferSize;
  UINT8                                    *PayloadBuffer;
  UINT8                                    MAC[SHA256_DIGEST_SIZE];

  BufferSize = 0;
  Status     = EFI_SUCCESS;
  //
  // Security Send, Fill RPMB Send Data Frame
  //
  ZeroMem (&RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  if (GetRandomNumber128 (RpmbDataFrame.Nonce) != TRUE) {
     DEBUG ((DEBUG_ERROR, "It is not a random value ! \n"));
     CopyMem (RpmbDataFrame.Nonce, mNvmeDefaultNonce, 16);
  }

  RpmbDataFrame.RPMBT      = RPMBTarget;
  RpmbDataFrame.SCount     = SectorCount;
  RpmbDataFrame.Address    = 0;
  RpmbDataFrame.RPMessage  = 0x0004;
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  &RpmbDataFrame                // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Data Block Read : Send Command operation succeeded. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Read : Send Command operation failed, Status:[%x]. \n", Status));
  }

  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Read : Operation Result is abnormal. \n"));
    goto Exit;
  }

  PayloadBuffer = AllocateZeroPool (ReceiveBufferSize);
  if (PayloadBuffer == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  Status = Ssp->ReceiveData (
                  Ssp,
                  MediaId,
                  100000000,                                // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,                   // SecurityProtocol
                  0x0100,                                   // SecurityProtocolSpecificData
                  ReceiveBufferSize,                        // PayloadBufferSize,
                  (VOID *)PayloadBuffer,                    // PayloadBuffer
                  &BufferSize
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Read : receive Command failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  CopyMem (&RpmbDataFrame, PayloadBuffer, NVME_RPMB_DATA_FRAME_SIZE);
  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Read : Operation Result is abnormal. \n"));
    goto Exit;
  }

  CopyMem (MAC, &(RpmbDataFrame.SBAKAMC[RPMB_FRAME_STUFF_BYTES - SHA256_DIGEST_SIZE]), SHA256_DIGEST_SIZE);
  if (HmacSha256Request (MAC, PayloadBuffer + RPMB_FRAME_STUFF_BYTES, NVME_RPMB_DATA_FRAME_SIZE - RPMB_FRAME_STUFF_BYTES + NVME_RPMB_CONTENTS_SIZE, TRUE) == FALSE) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Read : HmacSha256Request data digest failed. \n"));
    Status = EFI_DEVICE_ERROR;
  }

  CopyMem (ReceiveBuffer, PayloadBuffer, NVME_RPMB_DATA_TOTAL_SIZE);

Exit:
  if (PayloadBuffer != NULL) {
    FreePool(PayloadBuffer);
  }

  return Status;
}

/**
  The Authenticated Data Write implementation initiated by a Security Send command.

  @param  Ssp                          The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId                      Id of the media, changes every time the media is replaced.
  @param  RPMBTarget                   RPMB target to access.
  @param  RpmbTargetData               The pointer to the The data Number of 512B blocks.
  @param  SectorCount                  Number of 512B blocks.
  @param  ReceiveBufferSize            The size of RPMB configuration block data.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeAuthenticatedDataWrite (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId,
  IN  UINT8                                   RPMBTarget,
  IN  VOID                                    *RpmbTargetData,
  IN  UINT8                                   SectorCount,
  IN  UINT32                                  ReceiveBufferSize
  )
{
  EFI_STATUS                               Status;
  NVME_RPMB_DATA_FRAME                     RpmbDataFrame;
  NVME_RPMB_DATA_FRAME                     RpmbDataFrameCounter;
  UINT8                                    *RpmbDataFramBlock;
  UINT8                                    *PayloadBuffer;
  UINTN                                    PayloadSize;
  UINTN                                    BufferSize;
  UINT8                                    HostMAC[SHA256_DIGEST_SIZE];

  PayloadSize   = 0;
  BufferSize    = 0;
  PayloadBuffer = NULL;

  PayloadSize = NVME_RPMB_DATA_FRAME_SIZE + NVME_RPMB_CONTENTS_SIZE * SectorCount;
  RpmbDataFramBlock = AllocateZeroPool (PayloadSize);
  if (RpmbDataFramBlock == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  Status = NvmeAuthenticatedDeviceConfigurationBlockRead (Ssp, MediaId, RpmbDataFramBlock, NVME_RPMB_DATA_TOTAL_SIZE);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Read/Write Counter Value failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  ZeroMem (&RpmbDataFrameCounter, NVME_RPMB_DATA_FRAME_SIZE);
  CopyMem (&RpmbDataFrameCounter, RpmbDataFramBlock, NVME_RPMB_DATA_FRAME_SIZE);
  ZeroMem (&RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  RpmbDataFrame.WCounter  = RpmbDataFrameCounter.WCounter;
  RpmbDataFrame.RPMBT     = RPMBTarget;
  RpmbDataFrame.SCount    = SectorCount;
  RpmbDataFrame.Address   = 0;
  RpmbDataFrame.RPMessage = 0x0003;

  CopyMem (RpmbDataFramBlock, &RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  CopyMem (RpmbDataFramBlock + NVME_RPMB_DATA_FRAME_SIZE, RpmbTargetData, NVME_RPMB_CONTENTS_SIZE);

  //
  // Generate MAC value by RPMB configuration data
  //
  if (HmacSha256Request (HostMAC, RpmbDataFramBlock + RPMB_FRAME_STUFF_BYTES, (NVME_RPMB_DATA_TOTAL_SIZE - RPMB_FRAME_STUFF_BYTES), FALSE) == FALSE) {
    DEBUG ((DEBUG_ERROR, "Warning : HmacSha256Request data digest failed. \n"));
    goto Exit;
  }

  CopyMem (&(RpmbDataFrame.SBAKAMC[RPMB_FRAME_STUFF_BYTES - SHA256_DIGEST_SIZE]), HostMAC, SHA256_DIGEST_SIZE);
  CopyMem (RpmbDataFramBlock, &RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);

  //
  // Security Send 1
  //
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                      // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,         // SecurityProtocol
                  0x0100,                         // SecurityProtocolSpecificData
                  PayloadSize,                    // PayloadBufferSize
                  RpmbDataFramBlock               // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Data Block Write : Send 1 Command operation succeeded. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Write : Send 1 Command operation failed, Status:[%x]. \n", Status));
  }

  CopyMem (&RpmbDataFrame, RpmbDataFramBlock, NVME_RPMB_DATA_FRAME_SIZE);
  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Write : Operation Result is abnormal. \n"));
    goto Exit;
  }

  //
  // Security Send 2
  //
  ZeroMem (&RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  RpmbDataFrame.RPMBT     = RPMBTarget;
  RpmbDataFrame.RPMessage = 0x0005;

  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  &RpmbDataFrame                // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Data Block Write : Send 2 Command operation succeeded. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Write : Send 2 Command operation failed, Status:[%x]. \n", Status));
  }

  PayloadBuffer = AllocateZeroPool (NVME_RPMB_DATA_FRAME_SIZE);
  if (PayloadBuffer == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  // Security Receive
  Status = Ssp->ReceiveData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  PayloadBuffer,                // PayloadBuffer
                  &BufferSize
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Write : receive Command failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  ZeroMem (&RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  CopyMem (&RpmbDataFrame, PayloadBuffer, NVME_RPMB_DATA_FRAME_SIZE);
  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Data Block Write : Operation Result is abnormal. \n"));
    goto Exit;
  }

  if (CompareMem (HostMAC, &(RpmbDataFrame.SBAKAMC[RPMB_FRAME_STUFF_BYTES - SHA256_DIGEST_SIZE]), SHA256_DIGEST_SIZE) == 0) {
     DEBUG ((DEBUG_INFO,"RPMB Data Block Write : MAC validation is passed. \n"));
  }

Exit:

  if (RpmbDataFramBlock != NULL) {
    FreePool (RpmbDataFramBlock);
  }

  if (PayloadBuffer != NULL) {
    FreePool(PayloadBuffer);
  }

  return Status;
}

/**
  The Authenticated Device Configuration Block Read implementation initiated by a Security Send command.

  @param  Ssp                The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId            Id of the media, changes every time the media is replaced.
  @param  ReceiveBuffer      The buffer used to store RPMB configuration block data read from controller.
  @param  ReceiveBufferSize  The size of RPMB configuration block data.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeAuthenticatedDeviceConfigurationBlockRead (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId,
  OUT VOID                                    *ReceiveBuffer,
  IN  UINT32                                  ReceiveBufferSize
  )
{
  EFI_STATUS                               Status;
  NVME_RPMB_DATA_FRAME                     RpmbDataFrame;
  UINTN                                    BufferSize;
  UINT8                                    *PayloadBuffer;
  UINT8                                    MAC[SHA256_DIGEST_SIZE];

  BufferSize = 0;
  Status     = EFI_SUCCESS;
  //
  // Security Send, Fill RPMB Send Data Frame
  //
  ZeroMem (&RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  if (GetRandomNumber128 (RpmbDataFrame.Nonce) != TRUE) {
     DEBUG ((DEBUG_ERROR, "It is not a random value ! \n"));
     CopyMem (RpmbDataFrame.Nonce, mNvmeDefaultNonce, 16);
  }

  RpmbDataFrame.SCount    = 1;
  RpmbDataFrame.RPMessage = 0x0007;
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  &RpmbDataFrame                // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Configuration Block Read : Send Command operation succeeded. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Read : Send Command operation failed, Status:[%x]. \n", Status));
  }

  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Read : Operation Result is abnormal. \n"));
    goto Exit;
  }

  PayloadBuffer = AllocateZeroPool (ReceiveBufferSize);
  if (PayloadBuffer == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  Status = Ssp->ReceiveData (
                  Ssp,
                  MediaId,
                  100000000,                                // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,                   // SecurityProtocol
                  0x0100,                                   // SecurityProtocolSpecificData
                  ReceiveBufferSize,                      // PayloadBufferSize,
                  (VOID *)PayloadBuffer,                    // PayloadBuffer
                  &BufferSize
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Read : receive Command failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  CopyMem (&RpmbDataFrame, PayloadBuffer, NVME_RPMB_DATA_FRAME_SIZE);
  Status = GetNvmeRpmbOperationResult (&RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Read : Operation Result is abnormal. \n"));
    goto Exit;
  }

  CopyMem (MAC, &(RpmbDataFrame.SBAKAMC[RPMB_FRAME_STUFF_BYTES - SHA256_DIGEST_SIZE]), SHA256_DIGEST_SIZE);
  if (HmacSha256Request (MAC, PayloadBuffer + RPMB_FRAME_STUFF_BYTES, NVME_RPMB_DATA_FRAME_SIZE - RPMB_FRAME_STUFF_BYTES + NVME_RPMB_CONTENTS_SIZE, TRUE) == FALSE) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Read : HmacSha256Request data digest failed. \n"));
    Status = EFI_DEVICE_ERROR;
  }

  CopyMem (ReceiveBuffer, PayloadBuffer, NVME_RPMB_DATA_TOTAL_SIZE);

Exit:
  if (PayloadBuffer != NULL) {
    FreePool(PayloadBuffer);
  }

  return Status;
}

/**
  The Authenticated Device Configuration Block Write implementation initiated by a Security Send command.

  @param  Ssp                          The pointer to the EFI_STORAGE_SECURITY_COMMAND_PROTOCOL protocol.
  @param  MediaId                      Id of the media, changes every time the media is replaced.
  @param  RPMBDeviceConfigurationData  The buffer used to store RPMB configuration block data is ready to write
                                       to the configuration register of controller.
  @param  ReceiveBufferSize            The size of RPMB configuration block data.

  @retval EFI_SUCCESS                  Successfully read the controller capability register content.
  @retval EFI_OUT_OF_RESOURCES         There is not enough memory resource.
  @retval EFI_INVALID_PARAMETER        The size of input key exceeded the scope of definition in NVMe1.4 spec,
                                       or the PayloadBuffer or PayloadTransferSize is NULL and PayloadBufferSize
                                       is non-zero.
  @retval EFI_WARN_BUFFER_TOO_SMALL    The PayloadBufferSize was too small to store the available
                                       data from the device. The PayloadBuffer contains the truncated data.
  @retval EFI_UNSUPPORTED              The given MediaId does not support security protocol commands.
  @retval EFI_DEVICE_ERROR             The security protocol command completed with an error.
  @retval EFI_NO_MEDIA                 There is no media in the device.
  @retval EFI_MEDIA_CHANGED            The MediaId is not for the current media.
  @retval EFI_TIMEOUT                  A timeout occurred while waiting for the security
                                       protocol command to execute.

**/
EFI_STATUS
NvmeAuthenticatedDeviceConfigurationBlockWrite (
  IN  EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN  UINT32                                  MediaId,
  IN  VOID                                    *RpmbDeviceConfigurationData,
  IN  UINT32                                  ReceiveBufferSize
  )
{
  EFI_STATUS                               Status;
  NVME_RPMB_DATA_FRAME                     *RpmbDataFrame;
  UINT8                                    *RpmbDeviceConfigurationBlock;
  UINTN                                    PayloadSize;
  UINTN                                    BufferSize;
  UINT32                                   WCounter;

  PayloadSize   = 0;
  BufferSize    = 0;

  RpmbDeviceConfigurationBlock = AllocateZeroPool (NVME_RPMB_DATA_TOTAL_SIZE);
  if (RpmbDeviceConfigurationBlock == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  Status = NvmeAuthenticatedDeviceConfigurationBlockRead (Ssp, MediaId, RpmbDeviceConfigurationBlock, NVME_RPMB_DATA_TOTAL_SIZE);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Read/Write Counter Value failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  //
  // Just need to revice the write counter and all other fields clear to 0
  //
  RpmbDataFrame = (NVME_RPMB_DATA_FRAME *)RpmbDeviceConfigurationBlock;
  WCounter = RpmbDataFrame->WCounter;
  ZeroMem (RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  RpmbDataFrame->WCounter  = WCounter;
  RpmbDataFrame->SCount    = 1;
  RpmbDataFrame->RPMessage = 0x0006;

  CopyMem (RpmbDeviceConfigurationBlock + NVME_RPMB_DATA_FRAME_SIZE, RpmbDeviceConfigurationData, NVME_RPMB_CONTENTS_SIZE);
  PayloadSize = NVME_RPMB_DATA_TOTAL_SIZE;

  //
  // Generate MAC value by RPMB configuration data
  //
  if (HmacSha256Request (&(RpmbDataFrame->SBAKAMC[RPMB_FRAME_STUFF_BYTES - SHA256_DIGEST_SIZE]), RpmbDeviceConfigurationBlock + RPMB_FRAME_STUFF_BYTES, (NVME_RPMB_DATA_TOTAL_SIZE - RPMB_FRAME_STUFF_BYTES), FALSE) == FALSE) {
    DEBUG ((DEBUG_ERROR, "Warning : HmacSha256Request data digest failed. \n"));
    goto Exit;
  }

  //
  // Security Send 1
  //
  Status = Ssp->SendData (
                  Ssp,
                  MediaId,                        // ?????
                  100000000,                      // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,         // SecurityProtocol
                  0x0100,                         // SecurityProtocolSpecificData
                  PayloadSize,                    // PayloadBufferSize
                  RpmbDeviceConfigurationBlock   // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Configuration Block Write : Send 1 Command operation succeeded. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Write : Send 1 Command operation failed, Status:[%x]. \n", Status));
  }

  Status = GetNvmeRpmbOperationResult (RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Write : Operation Result is abnormal. \n"));
    goto Exit;
  }

  //
  // Security Send 2
  //
  ZeroMem (RpmbDataFrame, NVME_RPMB_DATA_FRAME_SIZE);
  RpmbDataFrame->RPMessage = 0x0005;

  Status = Ssp->SendData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  RpmbDataFrame                // PayloadBuffer
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "RPMB Configuration Block Write : Send 2 Command operation succeeded. \n"));
  } else {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Write : Send 2 Command operation failed, Status:[%x]. \n", Status));
  }

  // Security Receive
  Status = Ssp->ReceiveData (
                  Ssp,
                  MediaId,
                  100000000,                    // Timeout 10-sec
                  SECURITY_PROTOCOL_NVME,       // SecurityProtocol
                  0x0100,                       // SecurityProtocolSpecificData
                  NVME_RPMB_DATA_FRAME_SIZE,    // PayloadBufferSize,
                  RpmbDataFrame,                // PayloadBuffer
                  &BufferSize
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Write : receive Command failed, Status:[%x]. \n", Status));
    goto Exit;
  }

  Status = GetNvmeRpmbOperationResult (RpmbDataFrame);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "RPMB Configuration Block Write : Operation Result is abnormal. \n"));
    goto Exit;
  }

Exit:

  if (RpmbDeviceConfigurationBlock != NULL) {
    FreePool (RpmbDeviceConfigurationBlock);
  }

  return Status;
}

/**
  Read Nvm Express Controller Registers data with 32-bit width.

  @param  PciIo            The pointer to the EFI_PCI_IO_PROTOCOL data structure.
  @param  NVMeReg          The buffer used to store value read from according the offset of registers.
  @param  RegOffset        The controller register offset.

  @retval EFI_SUCCESS           Successfully read the controller capability register content.
  @retval EFI_DEVICE_ERROR      Fail to read the controller capability register.
  @retval EFI_UNSUPPORTED       The address range specified by Offset, Width, and Count is not
                                valid for the PCI BAR specified by BarIndex. Or BarIndex not valid for
                                this PCI controller.
  @retval EFI_OUT_OF_RESOURCES  The request could not be completed due to a lack of resources.
  @retval EFI_INVALID_PARAMETER One or more parameters are invalid.

**/

EFI_STATUS
ReadNvmeController32 (
  IN EFI_PCI_IO_PROTOCOL                  *PciIo,
  IN OUT VOID                             *NvmeReg,
  IN UINT32                               RegOffset
  )
{
  EFI_STATUS            Status;
  UINT32                Data;
  if(PciIo == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = PciIo->Mem.Read (
                        PciIo,
                        EfiPciIoWidthUint32,
                        NVME_BAR,
                        RegOffset,
                        1,
                        &Data
                        );
  if (EFI_ERROR(Status)) {
    return Status;
  }

  WriteUnaligned32 ((UINT32*)NvmeReg, Data);
  return EFI_SUCCESS;
}

/**
  Write Nvm Express Controller Registers data with 32-bit width.

  @param  PciIo            The pointer to the EFI_PCI_IO_PROTOCOL data structure.
  @param  NVMeReg          The buffer used to store value read from according the offset of registers.
  @param  RegOffset        The controller register offset.

  @retval EFI_SUCCESS           Successfully read the controller capability register content.
  @retval EFI_DEVICE_ERROR      Fail to read the controller capability register.
  @retval EFI_UNSUPPORTED       The address range specified by Offset, Width, and Count is not
                                valid for the PCI BAR specified by BarIndex. Or BarIndex not valid for
                                this PCI controller.
  @retval EFI_OUT_OF_RESOURCES  The request could not be completed due to a lack of resources.
  @retval EFI_INVALID_PARAMETER One or more parameters are invalid.

**/
EFI_STATUS
WriteNvmeController32(
  IN EFI_PCI_IO_PROTOCOL                  *PciIo,
  IN VOID                                 *NvmeReg,
  IN UINT32                               RegOffset
  )
{
  EFI_STATUS            Status;
  UINT32                Data;

  if(PciIo == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Data   = ReadUnaligned32 ((UINT32*)NvmeReg);
  Status = PciIo->Mem.Write (
                        PciIo,
                        EfiPciIoWidthUint32,
                        NVME_BAR,
                        RegOffset,
                        1,
                        &Data
                        );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  WriteUnaligned32 ((UINT32*)NvmeReg, Data);
  return EFI_SUCCESS;
}

/**
  Read Nvm Express Controller Registers data with 64-bit width.

  @param  PciIo            The pointer to the EFI_PCI_IO_PROTOCOL data structure.
  @param  NvmeReg          The buffer used to store value read from according the offset of registers.
  @param  RegOffset        The controller register offset.

  @retval EFI_SUCCESS           Successfully read the controller capability register content.
  @retval EFI_DEVICE_ERROR      Fail to read the controller capability register.
  @retval EFI_UNSUPPORTED       The address range specified by Offset, Width, and Count is not
                                valid for the PCI BAR specified by BarIndex. Or BarIndex not valid for
                                this PCI controller.
  @retval EFI_OUT_OF_RESOURCES  The request could not be completed due to a lack of resources.
  @retval EFI_INVALID_PARAMETER One or more parameters are invalid.

**/
EFI_STATUS
ReadNvmeController64 (
  IN EFI_PCI_IO_PROTOCOL                  *PciIo,
  IN OUT VOID                             *NvmeReg,
  IN UINT32                               RegOffset
  )
{
  EFI_STATUS            Status;
  UINT64                Data;

  if(PciIo == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  Data   = 0;
  Status = PciIo->Mem.Read (
                        PciIo,
                        EfiPciIoWidthUint64,
                        NVME_BAR,
                        RegOffset,
                        1,
                        &Data
                        );
  if (EFI_ERROR(Status)) {
    return Status;
  }

  WriteUnaligned64 ((UINT64*)NvmeReg, Data);

  return EFI_SUCCESS;
}

/**
  Write Nvm Express Controller Registers data with 64-bit width.

  @param  PciIo            The pointer to the EFI_PCI_IO_PROTOCOL data structure.
  @param  NVMeReg          The buffer used to store value read from according the offset of registers.
  @param  RegOffset        The controller register offset.

  @retval EFI_SUCCESS           Successfully read the controller capability register content.
  @retval EFI_DEVICE_ERROR      Fail to read the controller capability register.
  @retval EFI_UNSUPPORTED       The address range specified by Offset, Width, and Count is not
                                valid for the PCI BAR specified by BarIndex. Or BarIndex not valid for
                                this PCI controller.
  @retval EFI_OUT_OF_RESOURCES  The request could not be completed due to a lack of resources.
  @retval EFI_INVALID_PARAMETER One or more parameters are invalid.

**/
EFI_STATUS
WriteNvmeController64(
  IN EFI_PCI_IO_PROTOCOL                  *PciIo,
  IN VOID                                 *NvmeReg,
  IN UINT32                               RegOffset
  )
{
  EFI_STATUS            Status;
  UINT64                Data;

  if(PciIo == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Data   = ReadUnaligned64 ((UINT64*)NvmeReg);
  Status = PciIo->Mem.Write (
                        PciIo,
                        EfiPciIoWidthUint32,
                        NVME_BAR,
                        RegOffset,
                        2,
                        &Data
                        );
  if (EFI_ERROR(Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}

/**
  Write data to Boot partition after partition is unlocked.

  @param  Passthru                   A pointer to EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL.
  @param  NamespaceId                Current device NameSpaceId.
  @param  FirmwareImageBuffer        A pointer to the data should be saved to BP.
  @param  NvmeRecBPSize              The size of image in memory.

  @retval EFI_SUCCESS                The NVM Express Command Packet was sent by the host. TransferLength bytes were transferred
                                     to, or from DataBuffer.
  @retval EFI_BAD_BUFFER_SIZE        The NVM Express Command Packet was not executed. The number of bytes that could be transferred
                                     is returned in TransferLength.
  @retval EFI_NOT_READY              The NVM Express Command Packet could not be sent because the controller is not ready. The caller
                                     may retry again later.
  @retval EFI_DEVICE_ERROR           A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER      NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid. The NVM
                                     Express Command Packet was not sent, so no additional status information is available.
  @retval EFI_UNSUPPORTED            The command described by the NVM Express Command Packet is not supported by the NVM Express
                                     controller. The NVM Express Command Packet was not sent so no additional status information
                                     is available.
  @retval EFI_TIMEOUT                A timeout occurred while waiting for the NVM Express Command Packet to execute.

**/
EFI_STATUS
NvmeBootPartitionWrite (
  IN  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL      *Passthru,
  IN  UINT32                                  NamespaceId,
  IN  UINT8                                   *FirmwareImageBuffer,
  IN  UINTN                                   NvmeRecBPSize
  )
{
  EFI_STATUS                               Status;
  EFI_STATUS                               DLStatus;
  UINTN                                    WriteCount;
  UINTN                                    WriteLoop;

  DLStatus   = EFI_SUCCESS;
  WriteCount = 0;

  //
  // firmware download by 128K per chunk
  //
  WriteCount = ((NvmeRecBPSize % NVME_128K_PORTION_WRITE_UNIT) == 0 ?
               (NvmeRecBPSize / NVME_128K_PORTION_WRITE_UNIT) :((NvmeRecBPSize / NVME_128K_PORTION_WRITE_UNIT) + 1));
  for (WriteLoop = 0; WriteLoop < WriteCount; WriteLoop++) {
    Status = NvmeStorageFirmwareDownload (Passthru, NamespaceId, FirmwareImageBuffer + WriteLoop * NVME_128K_PORTION_WRITE_UNIT,
                                          NVME_128K_PORTION_WRITE_UNIT, WriteLoop * NVME_128K_PORTION_WRITE_UNIT);
    DLStatus = DLStatus | Status;
  }

  if (EFI_ERROR (DLStatus)) {
    DEBUG ((DEBUG_ERROR, "Firmware download command failed, Status:[%x].\n", Status));
    return DLStatus;
  }

  //
  // start commit image
  //
  Status = NvmeStorageFirmwareCommit (Passthru, NamespaceId, 6);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Firmware Commit command failed, Status:[%x].\n", Status));
    return Status;
  }

  return Status;
}

/**
  Read data from Boot partition, caller should free the memory.

  @param  Ssp                   A pointer to EFI_STORAGE_SECURITY_COMMAND_PROTOCOL.
  @param  Passthru              A pointer to EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL.
  @param  NamespaceId           Current device NameSpaceId.
  @param  HostMemoryBuffer      A pointer to memory can be saved the data read from BP.
  @param  NvmeRecBPSize         The size of image in memory.

  @retval EFI_SUCCESS                The NVM Express Command Packet was sent by the host. TransferLength bytes were transferred
                                     to, or from DataBuffer.
  @retval EFI_BAD_BUFFER_SIZE        The NVM Express Command Packet was not executed. The number of bytes that could be transferred
                                     is returned in TransferLength.
  @retval EFI_NOT_READY              The NVM Express Command Packet could not be sent because the controller is not ready. The caller
                                     may retry again later.
  @retval EFI_DEVICE_ERROR           A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER      NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid. The NVM
                                     Express Command Packet was not sent, so no additional status information is available.
  @retval EFI_UNSUPPORTED            The command described by the NVM Express Command Packet is not supported by the NVM Express
                                     controller. The NVM Express Command Packet was not sent so no additional status information
                                     is available.
  @retval EFI_TIMEOUT                A timeout occurred while waiting for the NVM Express Command Packet to execute.

**/
EFI_STATUS
NvmeBootPartitionRead (
  IN   EFI_STORAGE_SECURITY_COMMAND_PROTOCOL   *Ssp,
  IN   EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL      *Passthru,
  IN   UINT32                                  NamespaceId,
  OUT  UINT8                                   **HostMemoryBuffer,
  OUT  UINTN                                   NvmeRecBPSize
  )
{
  EFI_STATUS                               Status;
  EFI_STATUS                               DLStatus;
  UINTN                                    ReadLoop;
  UINTN                                    ReadCount;

  DLStatus   = EFI_SUCCESS;

  *HostMemoryBuffer = AllocateZeroPool (NvmeRecBPSize);
  if (*HostMemoryBuffer == NULL) {
     return EFI_OUT_OF_RESOURCES;
  }

  ReadCount = ((NvmeRecBPSize % NVME_16K_PORTION_READ_UNIT) == 0 ?
              (NvmeRecBPSize / NVME_16K_PORTION_READ_UNIT) :
              ((NvmeRecBPSize / NVME_16K_PORTION_READ_UNIT) + 1));
  for (ReadLoop = 0; ReadLoop < ReadCount; ReadLoop++) {
   Status = NvmeBootPartitionReadToHost2 (Passthru, NamespaceId, *HostMemoryBuffer + ReadLoop * NVME_16K_PORTION_READ_UNIT,
                                          NVME_16K_PORTION_READ_UNIT, ReadLoop * NVME_16K_PORTION_READ_UNIT);
   DLStatus = DLStatus | Status;
  }

  return DLStatus;
}

/**
  Get identify controller data.

  @param  Passthru         The pointer to the EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL data structure.
  @param  NamespaceId      Current device NameSpaceId.
  @param  Buffer           The buffer used to store the identify controller data.

  @retval EFI_SUCCESS                The NVM Express Command Packet was sent by the host. TransferLength bytes were transferred
                                     to, or from DataBuffer.
  @retval EFI_BAD_BUFFER_SIZE        The NVM Express Command Packet was not executed. The number of bytes that could be transferred
                                     is returned in TransferLength.
  @retval EFI_NOT_READY              The NVM Express Command Packet could not be sent because the controller is not ready. The caller
                                     may retry again later.
  @retval EFI_DEVICE_ERROR           A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER      NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid. The NVM
                                     Express Command Packet was not sent, so no additional status information is available.
  @retval EFI_UNSUPPORTED            The command described by the NVM Express Command Packet is not supported by the NVM Express
                                     controller. The NVM Express Command Packet was not sent so no additional status information
                                     is available.
  @retval EFI_TIMEOUT                A timeout occurred while waiting for the NVM Express Command Packet to execute.

**/
EFI_STATUS
NvmeIdentifyController2 (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL  *Passthru,
  IN UINT32                             NamespaceId,
  IN VOID                               *Buffer
  )
{
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                  Command;
  EFI_NVM_EXPRESS_COMPLETION               Completion;
  EFI_STATUS                               Status;

  ZeroMem (&CommandPacket, sizeof(EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof(EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof(EFI_NVM_EXPRESS_COMPLETION));

  Command.Cdw0.Opcode = NVME_ADMIN_IDENTIFY_CMD;
  //
  // According to Nvm Express 1.1 spec Figure 38, When not used, the field shall be cleared to 0h.
  // For the Identify command, the Namespace Identifier is only used for the Namespace data structure.
  //
  Command.Nsid        = NVME_CONTROLLER_ID;

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;
  CommandPacket.TransferBuffer = Buffer;
  CommandPacket.TransferLength = sizeof (NVME_ADMIN_CONTROLLER_DATA);
  CommandPacket.CommandTimeout = NVME_GENERIC_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;
  //
  // Set bit 0 (Cns bit) to 1 to identify a controller
  //
  Command.Cdw10                = 1;
  Command.Flags                = CDW10_VALID;

  Status = Passthru->PassThru (
                       Passthru,
                       NamespaceId,
                       &CommandPacket,
                       NULL
                       );

  return Status;
}

/**
  Dump NVMe identify information.

  @param  Passthru          The pointer to the EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL data structure.
  @param  NamespaceId       Current device NameSpaceId.
  @param  ControllerData    output for current device's controller data.

  @retval EFI_SUCCESS      Successfully get the identify controller data.
  @retval EFI_NOT_FOUND    Fail to find NVMe controller.

**/
EFI_STATUS
DumpNvmeIdentifyControllerData (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL  *Passthru,
  IN UINT32                              NamespaceId,
  OUT NVME_ADMIN_CONTROLLER_DATA         *ControllerData
  )
{
  EFI_STATUS                      Status;
  UINT8                           Sn[21];
  UINT8                           Mn[41];
  //
  // Get current Identify Controller Data
  //
  if((Passthru == NULL) || (ControllerData == NULL))
    return RETURN_INVALID_PARAMETER;

  Status = NvmeIdentifyController2 (Passthru, NamespaceId, ControllerData);

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Can't find NVMe Controller! \n"));
    return EFI_NOT_FOUND;
  }

  //
  // Dump NvmExpress Identify Controller Data
  //
  CopyMem (Sn, ControllerData->Sn, sizeof (ControllerData->Sn));
  Sn[20] = 0;
  CopyMem (Mn, ControllerData->Mn, sizeof (ControllerData->Mn));
  Mn[40] = 0;
  DEBUG ((DEBUG_INFO, " == NVME IDENTIFY CONTROLLER DATA ==\n"));
  DEBUG ((DEBUG_INFO, "    PCI VID   : 0x%x\n", ControllerData->Vid));
  DEBUG ((DEBUG_INFO, "    PCI SSVID : 0x%x\n", ControllerData->Ssvid));
  DEBUG ((DEBUG_INFO, "    SN        : %a\n",   Sn));
  DEBUG ((DEBUG_INFO, "    MN        : %a\n",   Mn));
  DEBUG ((DEBUG_INFO, "   FR        : 0x%x\n", *((UINT64*)ControllerData->Fr)));
  DEBUG ((DEBUG_INFO, "    RAB       : 0x%x\n", ControllerData->Rab));
  DEBUG ((DEBUG_INFO, "    IEEE      : 0x%x\n", *(UINT32*)ControllerData->Ieee_oui));
  DEBUG ((DEBUG_INFO, "    AERL      : 0x%x\n", ControllerData->Aerl));
  DEBUG ((DEBUG_INFO, "    SQES      : 0x%x\n", ControllerData->Sqes));
  DEBUG ((DEBUG_INFO, "    CQES      : 0x%x\n", ControllerData->Cqes));
  DEBUG ((DEBUG_INFO, "    NN        : 0x%x\n", ControllerData->Nn));

  DEBUG ((DEBUG_INFO, "    MTFA      : 0x%x\n", ControllerData->MTFA));
  DEBUG ((DEBUG_INFO, "    HMPRE     : 0x%x\n", ControllerData->HMPRE));
  DEBUG ((DEBUG_INFO, "    HMMIN     : 0x%x\n", ControllerData->HMMIN));
  DEBUG ((DEBUG_INFO, "    RPMBS     : 0x%x\n", ControllerData->RPMBS));
  DEBUG ((DEBUG_INFO, "    FWUG      : 0x%x\n", ControllerData->FWUG));

  return EFI_SUCCESS;
}

