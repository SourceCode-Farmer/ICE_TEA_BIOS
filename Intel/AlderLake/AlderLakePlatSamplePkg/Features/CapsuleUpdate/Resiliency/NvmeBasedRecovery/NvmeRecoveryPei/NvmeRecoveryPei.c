/**@file
#  This PEIM driver request Nvme recovery seed to csme, and save it to hob
#  produce gCsmeSeedHobGuid instance for other driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "NvmeRecoveryPei.h"

static EFI_PEI_NOTIFY_DESCRIPTOR  mSiInitNotifyList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gEfiPeiMasterBootModePpiGuid,
    CsmeSeedRequest
  }
};

/**
  Bios send seed request to CSME before MRC initialization.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_UNSUPPORTED           Current ME mode doesn't support this function
  @retval EFI_SUCCESS               Command succeeded
  @retval EFI_DEVICE_ERROR          HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT               HECI does not return the buffer before timeout
  @retval EFI_NOT_FOUND             Couldn't locate HeciPpi or debug token data
  @retval EFI_INVALID_PARAMETER     Parameter invalid
**/
EFI_STATUS
EFIAPI
CsmeSeedRequest (
  IN  EFI_PEI_SERVICES             **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN  VOID                         *Ppi
  )
{
  EFI_STATUS                   Status;
  SEED_PROPERTIES              SeedContent;
  GET_BIOS_SEED_RESP           BiosSeedResp;


  ZeroMem (&SeedContent, sizeof (SEED_PROPERTIES));
  ZeroMem (&BiosSeedResp, sizeof (GET_BIOS_SEED_RESP));

  SeedContent.CsmeSvn = 1;
  SeedContent.BiosSvn = 1;
  SeedContent.SeedPropertiesBitMap.SeedDebugProperty = 1;    // 1 - seed is same in lock and unlock
  SeedContent.SeedPropertiesBitMap.SeedHwFpfProperty = 1;    // 1 - seed is same regardless source of FPF values (HW or UEP)
  SeedContent.SeedPropertiesBitMap.SeedBtgProperty   = 1;    // 1 - seed is same regardless of BTG state (enabled or disable)
  SeedContent.SeedPropertiesBitMap.SeedType          = 1;    // fixed/constant
  SeedContent.SeedPropertiesBitMap.SeedLength        = 1;    // 256 bits

  Status = PeiHeciGetBiosSeed (&SeedContent, 0, &BiosSeedResp.EphemeralKeyCounter, &BiosSeedResp.BiosSeedInfo, (UINT8 *)&(BiosSeedResp.BiosSeed));
  if (EFI_ERROR (Status)) {
    DEBUG((DEBUG_ERROR, "Seed request is failed. \n"));
    return Status;
  }

  //
  // Build HOB to transfer csme seed to NvmeRecovery Dxe driver
  //
  BuildGuidDataHob (
    &gCsmeSeedHobGuid,
    &BiosSeedResp,
    sizeof (GET_BIOS_SEED_RESP)
    );

   return EFI_SUCCESS;
}

/**
  The user code starts with this function.

  @param  FileHandle             Handle of the file being invoked.
  @param  PeiServices            Describes the list of possible PEI Services.

  @retval EFI_SUCCESS            The driver is successfully initialized.
  @retval Others                 Can't initialize the driver.

**/
EFI_STATUS
EFIAPI
NvmeRecoveryPeimEntry (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS         Status;

  Status = EFI_SUCCESS;

  Status = PeiServicesNotifyPpi (mSiInitNotifyList);
  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;
}
