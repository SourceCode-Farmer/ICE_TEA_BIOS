/** @file
  Library to handle specific reset requirements after firmware update.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 -2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef __CAPSULE_UPDATE_RESET_LIB_H__
#define __CAPSULE_UPDATE_RESET_LIB_H__

#define  CAPSULE_UPDATE_RESET_TYPE_GLOBAL_RESET    0x01
#define  CAPSULE_UPDATE_RESET_TYPE_EC_RESET        0x02

/**
  Register a ResetFilter notification function to change system reset type according
  to given CapsuleResetType.

  @param[in] CapsuleResetType   The platform specific reset type that the caller intends
                                to change to. This is different to EFI_RESET_TYPE.

  @retval EFI_SUCCESS           ResetFilter notification is registered successfully.
  @retval Other                 Failed to register ResetFilter notify function.

**/
EFI_STATUS
EFIAPI
CapsuleUpdateRegisterResetFilter (
  UINT8                CapsuleResetType
  );

/**
  Register ResetHandler notification function to handle specific platform reset functionality
  according to CapsuleResetType.

  @param[in] CapsuleResetType   The platform specific reset type that the caller intends to support.
                                This is different to EFI_RESET_TYPE.

  @retval EFI_SUCCESS           ResetHandler notification is registered successfully.
  @retval Other                 Failed to register ResetHandler notify function.

**/
EFI_STATUS
EFIAPI
CapsuleUpdateRegisterResetHandler (
  UINT8                CapsuleResetType
  );

/**
  Register a reset notification to arm EC WDT right before system reset.
  This WDT is intended to deal with the case when BIOS/ACM/uCode is broken
  and BIOS cannot even be loaded after capsule update.

  With the assistance of EC WDT, TopSwap would be set and system would have a chance to recover.

  @param[in] EcWdtTimeOut       The timeout value (seconds) set to EC WDT.

  @retval EFI_SUCCESS           ResetNotification is registered successfully.
  @retval Other                 Failed to register ResetNotification function.

**/
EFI_STATUS
EFIAPI
CapsuleUpdateArmEcWdtBeforeReset (
  UINT8                EcWdtTimeOut
  );

#endif
