/** @file
  Library for FmpDeviceLib to support Fmp Capsule Dependency.

  To support getting and saving dependency, parameter "Image" of FmpDeviceSetImage
  and FmpDeviceGetImage is extended to include dependency data inside.
  The Layout of image:
  +--------------------------+
  |   Dependency Op-codes    |
  +--------------------------+
  |    Fmp Payload Image     |
  +--------------------------+

  This library provides function to save dependency into a protected storage and
  get dependency from the storage.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/


#ifndef __FMP_DEPENDENCY_SUPPORT_H__
#define __FMP_DEPENDENCY_SUPPORT_H__

#include <Protocol/FirmwareManagement.h>

/**
  Calculate the size of dependency op-codes associated with the image.

  The param "Image" of FmpDeviceSetImage consists of dependency op-codes
  and Fmp Payload Image.

  Assumption: The dependency passes validation in Fmp->CheckImage().

  @param[in]   Image       Pointer to Image.
  @param[in]   ImageSize   Size of Image.

  @retval  Size of dependency op-codes.

**/
UINTN
CalculateFmpDependencySizeInImage (
  IN  UINT8                             *Image,
  IN  CONST UINTN                       ImageSize
  );

/**
  Save dependency op-codes.

  @param[in]   Depex       Dependency op-codes.
  @param[in]   DepexSize   Size of dependency op-codes.

  @retval  EFI_SUCCESS  Succeed to save the dependency op-codes.
  @retval  Others       Fail to save the dependency op-codes.

**/
EFI_STATUS
SaveFmpDependencyToStorage (
  IN  EFI_FIRMWARE_IMAGE_DEP             *Depex,
  IN  UINTN                              DepexSize
  );

/**
  Get dependency op-codes.

  @param[out]   DepexSize  Size of dependency op-codes.

  @retval   Pointer to the dependency op-codes.

**/
EFI_FIRMWARE_IMAGE_DEP *
GetFmpDependencyFromStorage (
  OUT UINTN                              *DepexSize
  );

#endif
