/** @file
  Component Update Libaray
  Called by each FmpDeviceLib instance to do capsule update.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/FWUpdateLib.h>
#include <Library/PrintLib.h>
#include <Library/PlatformFlashAccessLib.h>
#include <Library/PciSegmentLib.h>

#include <Register/HeciRegs.h>
#include <Register/MeRegs.h>

/**
  Perform ME firmware update.

  @param[in] WriteReq                Request information for update flash.
  @param[in] Progress                A function used report the progress of the
                                     firmware update.  This is an optional parameter
                                     that may be NULL.
  @param[in] StartPercentage         The start completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.
  @param[in] EndPercentage           The end completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.

  @retval EFI_SUCCESS                ME FW is updated successfully.
  @retval Others                     The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateMeFirmware (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  );

/**
  Update BIOS region.

  @param[in] WriteReq                    Request information for update flash.
  @param[in] Progress                    A function used report the progress of the
                                         firmware update.  This is an optional parameter
                                         that may be NULL.
  @param[in] StartPercentage             The start completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.
  @param[in] EndPercentage               The end completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.

  @retval EFI_SUCCESS                    The flash region is updated successfully.
  @retval EFI_INVALID_PARAMETER          The input buffer is invalid.
  @retval Others                         The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateBiosFirmware (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  );

/**
  Update EC region.

  @param[in] WriteReq                    Request information for update flash.
  @param[in] Progress                    A function used report the progress of the
                                         firmware update.  This is an optional parameter
                                         that may be NULL.
  @param[in] StartPercentage             The start completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.
  @param[in] EndPercentage               The end completion percentage value that may
                                         be used to report progress during the flash
                                         write operation.

  @retval EFI_SUCCESS                    The flash region is updated successfully.
  @retval EFI_INVALID_PARAMETER          The input buffer is invalid.
  @retval Others                         The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateEcFirmware (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  );


/**
  Perform ISH PDT configuration update

  @param[in] WriteReq                Request information for update flash.
  @param[in] Progress                A function used report the progress of the
                                     firmware update.  This is an optional parameter
                                     that may be NULL.
  @param[in] StartPercentage         The start completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.
  @param[in] EndPercentage           The end completion percentage value that may
                                     be used to report progress during the flash
                                     write operation.

  @retval EFI_SUCCESS                PDT is updated successfully.
  @retval Others                     The update operation fails.

**/
EFI_STATUS
EFIAPI
UpdateIshPdt (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  );

