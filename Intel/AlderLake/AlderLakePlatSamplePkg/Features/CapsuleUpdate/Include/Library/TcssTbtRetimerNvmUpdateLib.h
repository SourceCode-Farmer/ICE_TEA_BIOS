/** @file
  Library API definitions for TcssTbtRetimerNvmUpdateLib

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef __TCSS_TBT_RETIMER_NVM_UPDATE_LIB_H__
#define __TCSS_TBT_RETIMER_NVM_UPDATE_LIB_H__

#include <Protocol/FirmwareManagement.h>

typedef VOID   *RETIMER_DEV_INSTANCE;
typedef VOID   *DISCRETE_TBT_DEV_INSTANCE;

#pragma pack(1)

typedef struct {
  UINT16  Bus;
  UINT16  Device;
  UINT16  Function;
  UINT8   Port;           // DMA retimer
  UINT8   RetimerIndex;
  UINT8   Reserved[8];
} RETIMER_DEV_ADDRESS;

#pragma pack()

/**
  Create and initialize the required data structure to access an Retimer device.
  Besides, set up a communication with it if needed.

  @param[in]   FirmwareType       The type of update firmware.
  @param[in]   PcieRpType         PCIE root port type.
  @param[in]   PcieRootPort       The parent pcie root port of discrete TBT device.
  @param[in]   DevAddress         The address info of target Retimer device.
  @param[out]  RetimerDevice      Pointer to a Retimer device instance corresponding to DevAddress.

  @retval EFI_SUCCESS             RetimerDevice is created successfully and is ready to accept commands.
  @retval Others                  An error occurred attempting to access the Retimer device.

**/
EFI_STATUS
EFIAPI
CreateRetimerDevInstance (
  IN   UINT8                                          FirmwareType,
  IN   UINT8                                          PcieRpType,
  IN   UINT8                                          PcieRootPort,
  IN   RETIMER_DEV_ADDRESS                            *DevAddress,
  OUT  RETIMER_DEV_INSTANCE                           *RetimerDevice
  );

/**
  Create and initialize the required data structure to access a Thunderbolt device.
  Besides, set up a communication with it if needed.

  @param[in]   FirmwareType       The type of update firmware.
  @param[in]   PcieRpType         PCIE root port type.
  @param[in]   PcieRootPort       The parent pcie root port of discrete TBT device.
  @param[out]  DiscreteTbtDevice  Pointer to a Tbt device instance.

  @retval EFI_SUCCESS             DiscreteTbtDevice is created successfully and is ready to accept commands.
  @retval Others                  An error occurred attempting to access the TBT device.

**/
EFI_STATUS
EFIAPI
CreateTBTDevInstance (
  IN   UINT8                                          FirmwareType,
  IN   UINT8                                          PcieRpType,
  IN   UINT8                                          PcieRootPort,
  OUT  DISCRETE_TBT_DEV_INSTANCE                      *DiscreteTbtDevice
  );

/**
  Release the data structure corresponding to given RetimerDevice.
  Besides, close the communication with it if needed.

  @param[in]  RetimerDevice       Retimer device instance corresponding to DevAddress.

  @retval EFI_SUCCESS             RetimerDevice is released successfully.
  @retval Others                  An error occurred attempting to access the Retimer device.

**/
EFI_STATUS
EFIAPI
DestroyRetimerDevInstance (
  IN  RETIMER_DEV_INSTANCE                            RetimerDevice
  );

/**
  Release the data structure corresponding to given DiscreteTbtDevice.
  Besides, close the communication with it if needed.

  @param[in]  DiscreteTbtDevice   Discrete TBT device instance corresponding to DevAddress.

  @retval EFI_SUCCESS             DiscreteTbtDevice is released successfully.
  @retval Others                  An error occurred attempting to access the Tbt device.

**/
EFI_STATUS
EFIAPI
DestroyTbtDevInstance (
  IN  DISCRETE_TBT_DEV_INSTANCE                       DiscreteTbtDevice
  );

/**
  Read Retimer NVM FW version on given RetimerDevice.

  Note: Caller is supposed to invoke CreateRetimerDevInstance() first to ensure the data structure
        of RetimerDevice is created properly and the communication to the target device is settled down.
        DestroyRetimerDevInstance() should be invoked after the caller is done with all Reimter accesses.

  @param[in]   RetimerDevice      Retimer device instance corresponding to DevAddress.
  @param[out]  Version            Retimer NVM FW version.

  @retval EFI_SUCCESS             Retimer FW version is successfully get.
  @retval Others                  An error occurred attempting to access the Retimer firmware

**/
EFI_STATUS
EFIAPI
ReadRetimerNvmVersion (
  IN   RETIMER_DEV_INSTANCE                           RetimerDevice,
  OUT  UINT32                                         *Version
  );

/**
  Update Retimer NVM Firmware on given RetimerDevice.

  Note: Caller is supposed to invoke CreateRetimerDevInstance() first to ensure the data structure
        of RetimerDevice is created properly and the communication to the target device is settled down.
        DestroyRetimerDevInstance() should be invoked after the caller is done with all Reimter accesses.

  @param[in]  RetimerDevice       Retimer device instance corresponding to DevAddress.
  @param[in]  RetimerImage        Pointer to an image buffer contains Retimer FW to be updated with.
  @param[in]  ImageSize           The size of RetimerImage memory buffer.
  @param[in]  Progress            A function used to report the progress of updating the firmware
                                  device with the new firmware image.
  @param[in]  StartPercentage     The start completion percentage value that may be used to report progress
                                  during the flash write operation. The value is between 1 and 100.
  @param[in]  EndPercentage       The end completion percentage value that may be used to report progress
                                  during the flash write operation.

  @retval EFI_SUCCESS             Retimer Firmware is successfully updated.
  @retval Others                  An error occurred attempting to access the Retimer firmware

**/
EFI_STATUS
EFIAPI
UpdateRetimerNvmFirmware (
  IN  RETIMER_DEV_INSTANCE                           RetimerDevice,
  IN  UINT8                                          *RetimerImage,
  IN  UINTN                                          ImageSize,
  IN  EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,          OPTIONAL
  IN  UINTN                                          StartPercentage,
  IN  UINTN                                          EndPercentage
  );

/**
  Update Discrete TBT NVM Firmware on given DiscreteTbtDevice.

  Note: Caller is supposed to invoke CreateTBTDevInstance() first to ensure the data structure
        of DiscreteTbtDevice is created properly and the communication to the target device is settled down.
        DestroyTbtDevInstance() should be invoked after the caller is done with all TBT accesses.

  @param[in]  DiscreteTbtDevice   Discrete TBT device instance corresponding to DISCRETE_TBT_ITEM.
  @param[in]  DiscreteTbtImage    Pointer to an image buffer contains discrete TBT FW to be updated with.
  @param[in]  ImageSize           The size of DiscreteTbtImage memory buffer.
  @param[in]  Progress            A function used to report the progress of updating the firmware
                                  device with the new firmware image.
  @param[in]  StartPercentage     The start completion percentage value that may be used to report progress
                                  during the flash write operation. The value is between 1 and 100.
  @param[in]  EndPercentage       The end completion percentage value that may be used to report progress
                                  during the flash write operation.

  @retval EFI_SUCCESS             TBT Firmware is successfully updated.
  @retval Others                  An error occurred attempting to access the TBT firmware

**/
EFI_STATUS
EFIAPI
UpdateDiscreteTbtNvmFirmware (
  IN  DISCRETE_TBT_DEV_INSTANCE                      DiscreteTbtDevice,
  IN  UINT8                                          *DiscreteTbtImage,
  IN  UINTN                                          ImageSize,
  IN  EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,          OPTIONAL
  IN  UINTN                                          StartPercentage,
  IN  UINTN                                          EndPercentage
  );

#endif
