/** @file
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Support Seamless Recovery based system firmware update.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef __SEAMLESS_RECOVERY_SUPPORT_LIB_H__
#define __SEAMLESS_RECOVERY_SUPPORT_LIB_H__

#include <Guid/SysFwUpdateProgress.h>

/**
  Delete all backup files from the external storage plus the associated NV variable.

**/
VOID
DeleteBackupFiles (
  VOID
  );

/**
  Save Obb image from both current/new BIOS to external storages.
  If NewObbImage is NULL or NewObbImageSize is 0, means to bacup current Obb image only.

  @param[in] NewObbImage       Pointers to Obb image in new BIOS.
  @param[in] NewObbImageSize   The size of NewObbImageImage.

  @retval  EFI_SUCCESS         Successfully backed up necessary files on external storage.
  @retval  Others              Failed to back up necessary files.

**/
EFI_STATUS
SaveObbToStorage (
  IN VOID                         *NewObbImage,
  IN UINTN                        NewObbImageSize
  );

/**
  Save current FMP Capsule and UX Capsule to external storages.

  @param[in] PayloadImage       Pointer to FMP payload image (FMP image header is stripped off).
  @param[in] PayloadImageSize   The size of PayloadImage.

  @retval  EFI_SUCCESS    Successfully backed up necessary files on external storage.
  @retval  Others         Failed to back up necessary files.

**/
EFI_STATUS
SaveCurrentCapsuleToStorage (
  IN VOID                         *PayloadImage,
  IN UINTN                        PayloadImageSize
  );

/**
  Search for ME recovery capsule on storage and return TRUE if present.

  @retval TRUE               ME recovery image is present.
  @retval FALSE              ME recovery image is not present.

**/
BOOLEAN
IsMeRecoveryCapsuleExistOnStorage (
  VOID
  );

/**
  Save current ME/Monolithic FMP Capsule as ME recovery capsule.

  @param[in] PayloadImage       Pointer to ME/Monolithic FMP payload image (FMP image header is stripped off).
  @param[in] PayloadImageSize   The size of PayloadImage.

  @retval  EFI_SUCCESS    Successfully backed up necessary files on external storage.
  @retval  Others         Failed to back up necessary files.

**/
EFI_STATUS
SaveMeRecoveryCapsule (
  IN VOID                         *PayloadImage,
  IN UINTN                        PayloadImageSize
  );

/**
  Replace MeRecovCap with MeRecovCapNew upon successful capsule update.


  @retval  EFI_SUCCESS    Successfully old recovery capsule with new recovery capsule.
  @retval  Others         Failed to sync old/new recovery capsules.

**/
EFI_STATUS
SyncMeRecoveryCapsules (
  VOID
  );

/**
  Check platform capability to support Fault tolerance based system firmware update.

  @retval TRUE  Current platform is capable of supporting Fault tolerance based system firmware update.
  @retval FALSE Current platform is incapable of supporting Fault tolerance based system firmware update.

**/
BOOLEAN
IsBiosFaultTolerantUpdateSupported (
  VOID
  );

/**
  Check if system firmware update got interrupted last time.

  @param[in,out] PreviousUpdateProgress      Pointers to the progress where updating process got
                                             interrupted last time.

  @retval TRUE   Previous update process got interrupted.
  @retval FALSE  There is no indication that update was in progress.

**/
BOOLEAN
IsPreviousUpdateUnfinished (
  IN OUT SYSTEM_FIRMWARE_UPDATE_PROGRESS       *PreviousUpdateProgress
  );

/**
  Get the current update progress from variable

  @param[out] CurrentUpdateProgress       Pointers to the current progress from NV storage

  @retval EFI_SUCCESS             Update progress is get from to NV storage successfully or it is not set.
  @retval EFI_INVALID_PARAMETER   CurrentUpdateProgress is NULL.

**/
EFI_STATUS
GetCurrentUpdateProgress (
  IN OUT SYSTEM_FIRMWARE_UPDATE_PROGRESS     *CurrentUpdateProgress
  );

/**
  Record the current update progress

  @param[in] UpdatingComponent      The FW component being updated now.
  @param[in] UpdatingProgress       The updating stage associated to UpdatingComponent

  @retval EFI_SUCCESS   Update progress is recorded to NV storage successfully.
  @retval Others        Update progress is not recorded.

**/
EFI_STATUS
SetUpdateProgress (
  IN SYSTEM_FIRMWARE_COMPONENT  UpdatingComponent,
  IN UINT32                     UpdatingProgress
  );

/**
  Clear update progress in NV storage. This indicates that no FW update process is happening.

**/
VOID
ClearUpdateProgress (
  VOID
  );

/**
  Calculate SHA256 Hash

  @param[in]  Data   data
  @param[in]  Size   data size
  @param[out] Digest SHA256 digest

**/
VOID
CreateSha256Hash (
  IN  UINT8     *Data,
  IN  UINTN     Size,
  OUT UINT8     *Digest
  );

/**
  Delete a file from an assigned file system.

  @param[in] FileSystemHandle    Handle of the file system that file would be deleted from.
  @param[in] FileName            Pointer to file name.

  @retval EFI_SUCCESS            File does not exist or deleted the file successfully.
  @retval Others                 Failed to delete the file.

**/
EFI_STATUS
DeleteFile (
  IN   EFI_HANDLE       FileSystemHandle,
  IN   CHAR16           *FileName
  );

//[-start-201112-IB16810138-modify]//
/**
  Write a file to an assigned file system.

  @param[in] FileSystemHandle    Handle of the file system that file would be write into.
  @param[in] FileName            Pointer to file name.
  @param[in] FileBuffer          The buffer to be written into file system.
  @param[in] FileSize            The size of FileBuffer.
  @param[in] IsWriteToDirectory  The file is writed to UpdateCapsule folder.
  @retval EFI_SUCCESS            Wrote the file successfully.
  @retval Others                 Failed to write the file.

**/
EFI_STATUS
WriteBackupFile (
  IN   EFI_HANDLE       FileSystemHandle,
  IN   CHAR16           *FileName,
  IN   UINT8            *FileBuffer,
  IN   UINTN            FileSize,
  IN   BOOLEAN          IsWriteToDirectory
  );
//[-end-201112-IB16810138-modify]//
/**
  Read a file from an assigned file system.

  @param[in] FileSystemHandle    Handle of the file system that file would be read.
  @param[in] FileName            Pointer to file name.
  @param[out] Buffer             Address of the buffer to which file is read.
  @param[out] BufferSize         The size of Buffer.

  @retval EFI_SUCCESS            Read the file successfully.
  @retval Others                 Failed to read the file.

**/
EFI_STATUS
ReadBackupFile (
  IN  EFI_HANDLE *FileSystemHandle,
  IN  CHAR16     *FileName,
  OUT VOID       **Buffer,
  OUT UINTN      *BufferSize
  );

/**
  Initialize mBackUpFileSystemHandle module variable

  @retval EFI_SUCCESS             Backup file system is found and assign to mBackUpFileSystemHandle
  @retval Others                  Cannot find an available file system to initialize mBackUpFileSystemHandle.

**/
EFI_STATUS
InitializeBackupFileSystem (
  VOID
  );

//[-start-201030-IB16810136-add]//
EFI_STATUS
CreateImageHash (
  IN VOID                         *FvImage,
  IN UINTN                        FvImageSize
);
//[-end-201030-IB16810136-add]//

//[-start-201112-IB16810138-add]//
EFI_STATUS
SaveMeCapsuleForRollback (
  VOID
  );

EFI_STATUS
GetBackupMeAsCapsule(
  VOID
  );
//[-end-201112-IB16810138-add]//

#endif
