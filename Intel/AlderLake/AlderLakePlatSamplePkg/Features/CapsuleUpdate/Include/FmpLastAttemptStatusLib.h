/** @file
  Defines private last attempt status codes used in FmpDevicePkg.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef __FMP_LAST_ATTEMPT_STATUS_LIB_H__
#define __FMP_LAST_ATTEMPT_STATUS_LIB_H__

///
/// The TBT Retimer value allowed for FMP device library errors.
///
#define LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_CODE_VALUE             0x1900

///
/// The Discrete Thunderbolt value allowed for FMP device library errors.
///
#define LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_CODE_VALUE                    0x1980

///
/// Last attempt status codes defined for additional granularity in FmpDevicePkg components.
///
/// These codes are defined within the last attempt status FMP device library range which extends from
/// LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_MIN_ERROR_CODE_VALUE to LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_MAX_ERROR_CODE_VALUE.
///
/// The following last attempt status code ranges are currently defined for the corresponding component:
///   * LAST_ATTEMPT_STATUS_DEVICE_LIBRARY - FMP devoce library
///
/// Future last attempt status code additions in FmpDevicePkg should be added as follows:
///   * FMP devoce library: Onto the end of the LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_ERROR range
///   * Other components: Add a new range onto the end of the last existing range in the enum within the limits of
///     [LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_MIN_ERROR_CODE_VALUE,LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_MAX_ERROR_CODE_VALUE]
///
/// The value of pre-existing last attempt status codes should never be modified to ensure the values remain
/// consistent over time.
///
enum LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_EXPANDED_ERROR_LIST
{
  ///
  /// Last attempt status codes used in FMP device library
  ///
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_ERROR_GET_FMP_HEADER                              = LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_MIN_ERROR_CODE_VALUE,

  ///
  /// Last attempt status codes used in FMP TBT Retimer device library
  ///
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_IMAGE_TABLE_NOT_PROVIDED        = LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_CODE_VALUE,
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_IMAGE_PARAMETER_IS_NULL         , // 0X1901
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_SIGNATURE_IS_NOT_DETECTED       , // 0X1902
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_RETIMER_COUNT_HEADER_IS_ZERO    , // 0X1903
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_OVER_ONE_RETIMER_COUNT          , // 0X1904
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_PAYLOAD_SIZE_TOO_SMALL          , // 0X1905
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_PAYLOAD_IS_OUT_OF_BOUNDS        , // 0X1906
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_IMAGE_NOT_PROVIDED              , // 0X1907
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_PROGRESS_CALLBACK_ERROR         , // 0X1908
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_TCSS_RETIMER_PROTOCOL_NOT_FOUND , // 0X1909
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_DRIVE_TBT_MODE_FAILED           , // 0X190A
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_USB2HC_PROTOCOL_NOT_FOUND       , // 0X190B
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_PAYLOAD_IS_OUT_OF_BOUNDS_2      , // 0X190C
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_UNSUPPORT_FIRMWARE_TYPE         , // 0X190D
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_INITIALIZATION_FAILED           , // 0X190E
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_RESTORE_ORIGINAL_MODE_FAILED    , // 0X190F
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_TBT_RETIMER_ERROR_UPDATE_FAILED                   , // 0X1910
  ///
  /// Last attempt status codes used in FMP Discrete Thunderbolt device library
  ///
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_IMAGE_TABLE_NOT_PROVIDED        = LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_CODE_VALUE,
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_IMAGE_PARAMETER_IS_NULL         , // 0X1981
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_SIGNATURE_IS_NOT_DETECTED       , // 0X1982
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_RETIMER_COUNT_HEADER_IS_ZERO    , // 0X1983
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_OVER_ONE_RETIMER_COUNT          , // 0X1984
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_PAYLOAD_SIZE_TOO_SMALL          , // 0X1985
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_PAYLOAD_IS_OUT_OF_BOUNDS        , // 0X1986
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_IMAGE_NOT_PROVIDED              , // 0X1987
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_PROGRESS_CALLBACK_ERROR         , // 0X1988
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_USB2HC_PROTOCOL_NOT_FOUND       , // 0X1989
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_PAYLOAD_IS_OUT_OF_BOUNDS_2      , // 0X198A
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_UNSUPPORT_FIRMWARE_TYPE         , // 0X198B
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_INITIALIZATION_FAILED           , // 0X198C
  LAST_ATTEMPT_STATUS_DEVICE_LIBRARY_DTBT_ERROR_UPDATE_FAILED                     // 0X198D
};

#endif
