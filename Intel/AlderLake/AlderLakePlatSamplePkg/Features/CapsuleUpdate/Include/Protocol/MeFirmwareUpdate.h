/** @file
  ME Firmware Update Protocol.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#ifndef __ME_FIRMWARE_UPDATE_PROTOCOL_H__
#define __ME_FIRMWARE_UPDATE_PROTOCOL_H__

#include <Uefi.h>
#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/FWUpdateLib.h>

#define ME_FIRMWARE_UPDATE_PROTOCOL_GUID \
  { \
    0xf72bf4c2, 0x92c9, 0x4a49, {0xa1, 0x11, 0x2d, 0x23, 0x66, 0xd0, 0xad, 0x02 } \
  }

typedef struct _ME_FIRMWARE_UPDATE_PROTOCOL ME_FIRMWARE_UPDATE_PROTOCOL;


/**
  Starting a Full FW Update from a buffer.

  @param[in]  Buffer        Buffer of Update Image read from Update Image file.
  @param[in]  BufferLength  Length of the buffer in bytes.
  @param[in]  OemId         OEM ID to compare with OEM ID in FW (if exist). Can be NULL.
  @param[in]  Func          A callback function that reports the progress of sending the buffer
                            to FW (not the progress of the update itself). Can be NULL.
                            VOID* Func(UINT32 BytesSentToFw, UINT32 TotalBytesToSendToFw);

  @retval     SUCCESS       If Update started successfully. Error code otherwise.
**/
typedef
UINT32
(*ME_FIRMWARE_UPDATE_PROTOCOL_FULL_UPDATE_FROM_BUFFER)(
  IN  UINT8                  *Buffer,
  IN  UINT32                 BufferLength,
  IN  _UUID                  *OemId,
  IN  VOID                   (*Func)(UINT32, UINT32)
  );

/**
  Check for Update progress.

  If in progress, return the current percent.
  If finished, return the status of the update, and the needed reset type after the update.
  This function should be called only after starting the update by calling FwuUpdateFull/Partial...

  @param[out] InProgress       True if Update is still in progress. False if Update finished. Caller allocated.
  @param[out] CurrentPercent   Current percent of the update, if Update is in progress. Caller allocated.
  @param[out] FwUpdateStatus   FW error code status of the update, if it finished (success or error code). Caller allocated.
  @param[out] NeededResetType  Needed reset type after the update, if it finished. Caller allocated.
                               MFT_PART_INFO_EXT_UPDATE_ACTION_NONE         0
                               MFT_PART_INFO_EXT_UPDATE_ACTION_HOST_RESET   1
                               MFT_PART_INFO_EXT_UPDATE_ACTION_CSE_RESET    2
                               MFT_PART_INFO_EXT_UPDATE_ACTION_GLOBAL_RESET 3

  @retval     SUCCESS          If Update is still in progress, or finished successfully. Error code otherwise.
**/
typedef
UINT32
(*ME_FIRMWARE_UPDATE_PROTOCOL_CHECK_UPDATE_PROGRESS)(
  OUT BOOLEAN                *InProgress,
  OUT UINT32                 *CurrentPercent,
  OUT UINT32                 *FwUpdateStatus,
  OUT UINT32                 *NeededResetType
  );

/**
  Get FW Update enabling state.

  @param[out] EnabledState  FW Update enabling state. Caller allocated.
                            FW_UPDATE_DISABLED = 0. Full Disabled. Partial Enabled.
                            FW_UPDATE_ENABLED = 1. Full Enabled. Partial Enabled.
                            FW_UPDATE_FULL_AND_PARTIAL_DISABLED = 3. Full Disabled. Partial Disabled.

  @retval     SUCCESS       If succeeded. Error code otherwise.
**/
typedef
UINT32
(*ME_FIRMWARE_UPDATE_PROTOCOL_ENABLED_STATE)(
  OUT UINT16                 *EnabledState
  );

/**
  Set FW Update enabling state.
  Supported only before EOP.

  @param[in]  EnabledState  FW Update enabling state.
                            FW_UPDATE_DISABLED = 0. Full Disabled. Partial Enabled.
                            FW_UPDATE_ENABLED = 1. Full Enabled. Partial Enabled.
                            FW_UPDATE_FULL_AND_PARTIAL_DISABLED = 3. Full Disabled. Partial Disabled.

  @retval     SUCCESS       If succeeded. Error code otherwise.
*/
typedef
UINT32
(*ME_FIRMWARE_UPDATE_PROTOCOL_SET_ENABLED_STATE)(
  IN                         UINT32 EnabledState
  );

/**
  Get OEM ID from flash.

  @param[out] OemId    OEM ID from flash. Caller allocated.

  @retval     SUCCESS  If succeeded. Error code otherwise.
**/
typedef
UINT32
(*ME_FIRMWARE_UPDATE_PROTOCOL_OEM_ID)(
  OUT _UUID                  *OemId
  );

/**
  Set ISH configuration file.
  Receive PDT file payload, create bios2ish file as a composition of
  bios2ish header (with PDT Update data type) and PDT file payload
  and send it to FW to set file.
  Supported only before EOP.

  @param[in] Buffer        Buffer of PDT file payload.
  @param[in] BufferLength  Length of the buffer in bytes.

  @retval    SUCCESS       If succeeded. Error code otherwise.
**/

typedef
UINT32
(*ME_FIRMWARE_UPDATE_PROTOCOL_SET_ISH_CONFIG)(
  IN  UINT8                 *Buffer,
  IN  UINT32                BufferLength
  );

///
/// FIRMWARE_UPDATE_PROTOCOL
/// The protocol providing the different generation FwUpdateLib implementation for ME capsule update
/// as the following services.
/// - Starting a Full FW Update from a buffer.
/// - Check for Update progress.
/// - Get FW Update enabling state.
/// - Get OEM ID from flash.
/// - Set ISH configuration file.
///
struct _ME_FIRMWARE_UPDATE_PROTOCOL {
  ME_FIRMWARE_UPDATE_PROTOCOL_FULL_UPDATE_FROM_BUFFER        FullUpdateFromBuffer;
  ME_FIRMWARE_UPDATE_PROTOCOL_CHECK_UPDATE_PROGRESS          CheckUpdateProgress;
  ME_FIRMWARE_UPDATE_PROTOCOL_SET_ENABLED_STATE              SetEnabledState;
  ME_FIRMWARE_UPDATE_PROTOCOL_ENABLED_STATE                  EnabledState;
  ME_FIRMWARE_UPDATE_PROTOCOL_OEM_ID                         OemId;
  ME_FIRMWARE_UPDATE_PROTOCOL_SET_ISH_CONFIG                 SetIshConfig;
};

extern EFI_GUID gMeFwuProtocol;

#endif
