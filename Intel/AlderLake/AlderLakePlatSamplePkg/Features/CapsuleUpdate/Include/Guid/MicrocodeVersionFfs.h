/** @file

  Copyright (c) 2019, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#ifndef __INTEL_MICROCODE_VERSION_FFS_FILE_GUID_H__
#define __INTEL_MICROCODE_VERSION_FFS_FILE_GUID_H__

#define INTEL_MICROCODE_VERSION_FFS_FILE_GUID { 0xE0562501, 0xB41B, 0x4566, { 0xAC, 0x0F, 0x7E, 0xA8, 0xEE, 0x81, 0x7F, 0x20 } }

///
/// Format of the raw data in the FFS file gIntelMicrocodeVersionFfsFileGuid.
/// This include the 32-bit Version, 32-bit LowestSupportVersion, and Null
/// terminated Unicode Version String associated with the array of microcode
/// patches that are stored in the gIntelMicrocodeFirmwareVolumeGuid Firmware
/// Volume.
///
typedef struct {
  UINT32  Version;
  UINT32  LowestSupportedVersion;
  CHAR16  VersionString[0];
} INTEL_MICROCODE_VERSION_FFS_DATA;

extern EFI_GUID gIntelMicrocodeVersionFfsFileGuid;

#endif
