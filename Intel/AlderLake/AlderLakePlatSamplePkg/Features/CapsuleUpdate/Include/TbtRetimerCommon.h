/** @file
  TbtRetimer common definition.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef __TBT_RETIMER_COMMON_H__
#define __TBT_RETIMER_COMMON_H__

typedef struct {
  UINT16 Bus;
  UINT16 Device;
  UINT16 Function;
} PCIE_BDF;

typedef struct {
  UINT8 PcieRpType;
  UINT8 PcieRootPort;
} PCIE_RP_CONFIG;

typedef enum {
  INTEGRATED_TBT_RETIMER = 0x0,
  DISCRETE_TBT_RETIMER = 0x1,
  DISCRETE_TBT = 0x02,
  FIRMWARE_TYPE_MAX_VALUE
} FIRMWARE_TYPE;

typedef enum {
  SUPPORT_PCH_TYPE = 0x0,
  PCIE_RP_TYPE_MAX_VALUE
} PCIE_RP_TYPE;

#endif
