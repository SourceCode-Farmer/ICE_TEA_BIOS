## @file
#
#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# FmpDxe driver for BIOS update.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2019 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##
#[-start-210426-IB16810151-modify]#
#[-start-210104-IB19010013-modify]#
#[-start-201030-IB16810136-modify]#
  $(CHIPSET_PKG)/Override/EDK2/FmpDevicePkg/FmpDxe/FmpDxe.inf {
#[-end-201030-IB16810136-modify]#
#[-end-210104-IB19010013-modify]#
#[-end-210426-IB16810151-modify]#
    <Defines>
      #
      # ESRT and FMP GUID for system firmware capsule update
      #
      FILE_GUID = $(FMP_CLIENT_PLATFORM_SYSTEM_BIOS)
    <PcdsFixedAtBuild>
      #
      # Unicode name string that is used to populate FMP Image Descriptor for this capsule update module
      #
      gFmpDevicePkgTokenSpaceGuid.PcdFmpDeviceImageIdName|L"AlderLake BIOS System Firmware Device"

      #
      # ESRT and FMP Lowest Support Version for this capsule update module
      # xxxx.xx (BIOS Major version, BIOS Minor version)
#[-start-201030-IB16810136-modify]#
      gPlatformModuleTokenSpaceGuid.PcdSystemFirmwareFmpLowestSupportedVersion|0x52430000
#[-end-201030-IB16810136-modify]#
      gFmpDevicePkgTokenSpaceGuid.PcdFmpDeviceProgressWatchdogTimeInSeconds|0

      #
      # Capsule Update Progress Bar Color.  Set to blue (RGB) (0, 0, 255)
      #
      gFmpDevicePkgTokenSpaceGuid.PcdFmpDeviceProgressColor|0x000000FF
#[-start-201030-IB16810136-remove]#
#      #
#      # Certificates used to authenticate capsule update image
#      #
#      !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpCertificate.dsc
#[-end-201030-IB16810136-remove]#
    <LibraryClasses>
      #
      # Generic libraries that are used "as is" by all FMP modules
      #
      FmpPayloadHeaderLib|FmpDevicePkg/Library/FmpPayloadHeaderLibV1/FmpPayloadHeaderLibV1.inf
#[-start-201030-IB16810136-modify]#
      FmpAuthenticationLib|$(CHIPSET_PKG)/Override/EDK2/SecurityPkg/Library/FmpAuthenticationLibPkcs7/FmpAuthenticationLibPkcs7.inf
      FmpDependencyLib|FmpDevicePkg/Library/FmpDependencyLib/FmpDependencyLib.inf
#[-end-201030-IB16810136-modify]#
      #
      # Platform specific capsule policy library
      #
#[-start-201030-IB16810136-modify]#
      CapsuleUpdatePolicyLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/CapsuleUpdatePolicyLib/CapsuleUpdatePolicyLib.inf
#[-emd-201030-IB16810136-modify]#
      #
      # Platform specific library that processes a capsule and updates the FW storage device
      #
#[-start-201109-IB16810137-modify]#
#[-start-201030-IB16810136-modify]#
      FmpDeviceLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/FmpDeviceLibBios/FmpDeviceLibBios.inf
      BiosUpdateHelpersLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/BiosUpdateHelpersLib/BiosUpdateHelpersLib.inf
      IniParsingLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/IniParsingLib/IniParsingLib.inf
      FmpDependencySupportLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/FmpDependencySupportLib/FmpDependencySupportLib.inf
      FmpDependencyCheckLib|FmpDevicePkg/Library/FmpDependencyCheckLibNull/FmpDependencyCheckLibNull.inf
      FmpDependencyDeviceLib|FmpDevicePkg/Library/FmpDependencyDeviceLibNull/FmpDependencyDeviceLibNull.inf
      CapsuleUpdateResetLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/CapsuleUpdateResetLib/CapsuleUpdateResetLib.inf
#[-end-201030-IB16810136-modify]#
#[-end-201109-IB16810137-modify]#
  }
