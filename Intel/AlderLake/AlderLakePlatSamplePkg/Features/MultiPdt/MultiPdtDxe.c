/** @file
  MultiPdt Support Dxe Driver implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <PiDxe.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DxeMeLib.h>
#include <Uefi/UefiBaseType.h>
#include <SetupVariable.h>
#include <Library/DxeServicesLib.h>
#include <Library/PdtUpdateLib.h>
#include <Library/HobLib.h>

/*
  Entry point for the MultiPdtDxe driver

  @param[in]  ImageHandle  Image Handle.
  @param[in]  SystemTable  EFI System Table.

  @retval     EFI_SUCCESS  Function has completed successfully.
  @retval     Others       All other error conditions encountered result in an ASSERT.

*/

EFI_STATUS
EFIAPI
MultiPdtDxeEntryPoint (
  IN EFI_HANDLE                             ImageHandle,
  IN EFI_SYSTEM_TABLE                       *SystemTable
  )
{
  EFI_STATUS                                Status;
  EFI_BOOT_MODE                             BootMode;
  SETUP_DATA                                SetupData;
  UINTN                                     Size;
  UINT32                                    BomId;
  UINT8                                     *MPdtFile;
  UINTN                                     MPdtFileSize;
  UINT32                                    MPdtBufferSize;
  BOOLEAN                                   PdtExist;

  DEBUG ((DEBUG_INFO, "MultiPdtDxe Entry start\n"));

  //
  // Get boot mode.
  //
  BootMode = GetBootModeHob ();

  //
  // Skip MultiPdt update if boot mode is BOOT_ON_FLASH_UPDATE
  //
  if (BootMode == BOOT_ON_FLASH_UPDATE) {
    DEBUG ((DEBUG_INFO, "Skip PDT update, BootMode is BOOT_ON_FLASH_UPDATE\n"));
    return EFI_SUCCESS;
  }


  Size = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &Size,
                  &SetupData
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "SetupData : Get variable failure.\n"));
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  //
  // Skip MultiPdt update if MPdtSupport is disabled
  //
  if (!SetupData.MPdtSupport) {
    DEBUG ((DEBUG_INFO, "MultiPdtDxe MPdtSupport is Disabled\n"));
    return Status;
  }

  //
  // Get BomId value form Setup option
  //
  BomId = SetupData.MPdtSupport;

  //
  // Skip MultiPdt update if PdtExist non-volatile variable exist
  //
  Size = sizeof (PdtExist);
  Status = gRT->GetVariable (
                  L"PdtExist",
                  &gPdtExistGuid,
                  NULL,
                  &Size,
                  &PdtExist
                  );
  if ((Status != EFI_NOT_FOUND) && EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "PdtExist : Get variable failure, status= %r\n", Status));
    return Status;
  }
  if (Status == EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "PdtExist non-volatile variable exist\n"));
    return Status;
  }

  //
  // Extract M-PDT buffer in BIOS
  //
  Status = GetSectionFromFv (
             &gMPdtFileGuid,
             EFI_SECTION_RAW,
             0,
             (VOID **) &MPdtFile,
             &MPdtFileSize
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "MPdtFile buffer : GetSectionFromFv failure, status= %r\n", Status));
    return Status;
  }

  //
  // Get correct PDT file from M-PDT buffer based on BomId value,
  // then Update PDT image into ME data region through Heci interface
  //
  MPdtBufferSize = MPdtFileSize & 0xFFFFFFFF;
  Status = PdtUpdate(BomId, MPdtFile, MPdtBufferSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "PdtUpdate failure, status= %r\n", Status));
    return Status;
  }

  //
  // Set PdtExist NV variable after updating PDT to avoid repetitive update.
  //
  PdtExist = TRUE;
  gRT->SetVariable (
         L"PdtExist",
         &gPdtExistGuid,
         EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
         sizeof (PdtExist),
         &PdtExist
         );

  DEBUG ((DEBUG_INFO, "MultiPdtDxe Entry end\n"));

  return Status;
}
