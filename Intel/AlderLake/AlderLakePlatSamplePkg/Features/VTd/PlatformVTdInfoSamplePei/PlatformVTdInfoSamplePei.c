/** @file
  Platform VTd Info Sample PEI driver.

  @copyright
   INTEL CONFIDENTIAL
   Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

**/

#include <PiPei.h>
#include <Ppi/VtdInfo.h>
#include <Ppi/VtdNullRootEntryTable.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PcdLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/VtdInfoLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/TmeInfoLib.h>
#include <SetupVariable.h>
#include <Register/SaRegsHostBridge.h>
#include <Guid/VtdPmrInfoHob.h>
#include <IndustryStandard/Vtd.h>
#include <Ppi/VtdEnableDmaBufferPpi.h>

typedef struct {
  EFI_ACPI_DMAR_HEADER                         DmarHeader;
  //
  // VTd engine - IOP
  //
  EFI_ACPI_DMAR_DRHD_HEADER                    IopDrhd;
} PLATFORM_VTD_INFO_IOP_PPI;

PLATFORM_VTD_INFO_IOP_PPI mPlatformIopVTdSample = {
  { // DmarHeader
    { // Header
      EFI_ACPI_4_0_DMA_REMAPPING_TABLE_SIGNATURE,
      sizeof(PLATFORM_VTD_INFO_IOP_PPI),
      EFI_ACPI_DMAR_REVISION,
    },
    0x00, // HostAddressWidth -- it'll be updated dynamically
  },
  { // IopDrhd
    { // Header
      EFI_ACPI_DMAR_TYPE_DRHD,
      sizeof(EFI_ACPI_DMAR_DRHD_HEADER)
    },
    EFI_ACPI_DMAR_DRHD_FLAGS_INCLUDE_PCI_ALL, // Flags
    0, // Reserved
    0, // SegmentNumber
    /*
       IOP RegisterBaseAddress -- TO BE PATCHED:
       This address should match to the value in Silicon Policy
       In the preMem phase, we will only use one VTd hardware unit for PMR
    */
    0xFED91000
  },
};

/**
  The function installs PLATFORM_VTD_INFO_IOP_PPI.

  @retval  EFI_SUCCESS           The function completed successfully.
  @retval  EFI_OUT_OF_RESOURCES  The function isn't able to allocate memory.
**/
EFI_STATUS
InstallPlatformIopVtdInfoPpi (
  VOID
  )
{
  EFI_STATUS                Status;
  PLATFORM_VTD_INFO_IOP_PPI *PlatformIopVtdInfo;
  EFI_PEI_PPI_DESCRIPTOR    *PlatformIopVtdInfoPpi;

  PlatformIopVtdInfo = AllocateZeroPool (sizeof (PLATFORM_VTD_INFO_IOP_PPI));
  if (PlatformIopVtdInfo == NULL) {
    ASSERT (PlatformIopVtdInfo != NULL);
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (PlatformIopVtdInfo, &mPlatformIopVTdSample, sizeof (PLATFORM_VTD_INFO_IOP_PPI));

  // Update a value of HostAddressWidth
  if (IsTmeActivated ()) {
    PlatformIopVtdInfo->DmarHeader.HostAddressWidth = 45;
  } else {
    PlatformIopVtdInfo->DmarHeader.HostAddressWidth = GetMaxPhysicalAddressSize () - 1;
  }

  PlatformIopVtdInfoPpi = AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
  if (PlatformIopVtdInfoPpi == NULL) {
    FreePool (PlatformIopVtdInfo);
    ASSERT (PlatformIopVtdInfoPpi != NULL);
    return EFI_OUT_OF_RESOURCES;
  }

  PlatformIopVtdInfoPpi->Flags = (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST);
  PlatformIopVtdInfoPpi->Guid  = &gEdkiiVTdInfoPpiGuid;
  PlatformIopVtdInfoPpi->Ppi   = PlatformIopVtdInfo;

  Status = PeiServicesInstallPpi (PlatformIopVtdInfoPpi);

  return Status;
}

// BIOS uses TE with a null root entry table to block VT-d engine access
//  to block any DMA traffic in pre-memory phase.
// Assign a system reserved address which can abort IOMMU access as root entry table.
EDKII_VTD_NULL_ROOT_ENTRY_TABLE_PPI mNullRootEntryTable = 0xFED20000;

EFI_PEI_PPI_DESCRIPTOR mPlatformNullRootEntryTableDesc = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEdkiiVTdNullRootEntryTableGuid,
  &mNullRootEntryTable
};

typedef struct {
  BOOLEAN IopVtdEnabled;
  BOOLEAN IpuVtdEnabled;
  BOOLEAN IgdVtdEnabled;
} PLATFORM_VTD_PEI_STATE_INFO;

EFI_GUID mPlatformVtdPeiStateInfoGuid = {
  0x78e4b506, 0x7e2e, 0x4947, { 0xa6, 0xb6, 0x48, 0x1e, 0x5b, 0x9f, 0xcc, 0x50 }
};

/**
  Initialize VTd register.
**/
VOID
InitGlobalVtd (
  VOID
  )
{
  UINT32              McD0BaseAddress;
  UINT32              MchBar;

  DEBUG ((DEBUG_INFO, "InitGlobalVtd\n"));

  McD0BaseAddress  = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  MchBar = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) & ~B_SA_MCHBAR_MCHBAREN_MASK;
  DEBUG ((DEBUG_INFO, "MchBar - %x\n", MchBar));

  MmioWrite32 (MchBar + GetIopVtdOffset(), (UINT32)mPlatformIopVTdSample.IopDrhd.RegisterBaseAddress | BIT0);
  DEBUG ((DEBUG_INFO, "IOP VTd - %x\n", (MmioRead32 (MchBar + GetIopVtdOffset()))));

}

/**
  Platform enable PMR
  @param VtdUnitBaseAddress The base address of the VTd engine.
**/
VOID
PlatformEnablePMR (
  IN UINTN            VtdUnitBaseAddress
  )
{
  UINT32              Reg32;
  EFI_STATUS          Status;
  VTD_CAP_REG         CapReg;
  VTD_PMR_INFO_HOB    *VtdPmrHob;
  VOID                *VtdPmrHobPtr;
  UINTN               LowBottom;
  UINTN               LowTop;
  UINT64              HighBottom;
  UINT64              HighTop;

  //
  // Initialization
  //
  Status              = EFI_SUCCESS;
  VtdPmrHobPtr        = GetFirstGuidHob (&gVtdPmrInfoDataHobGuid);
  if (VtdPmrHobPtr == NULL) {
    Status = EFI_UNSUPPORTED;
    DEBUG ((DEBUG_ERROR, "VtdPmrHobPtr == NULL, Status - 0x%r\n", Status));
    return;
  }

  VtdPmrHob           = GET_GUID_HOB_DATA (VtdPmrHobPtr);
  LowBottom = VtdPmrHob->ProtectedLowBase;
  LowTop = VtdPmrHob->ProtectedLowLimit - VtdPmrHob->ProtectedLowBase;
  HighBottom = VtdPmrHob->ProtectedHighBase;
  HighTop = VtdPmrHob->ProtectedHighLimit - VtdPmrHob->ProtectedHighBase;

  DEBUG ((DEBUG_INFO, "SetDmaProtectedRange(0x%x) \n", VtdUnitBaseAddress));
  DEBUG ((DEBUG_INFO, "[0x%x, 0x%x] [0x%016lx, 0x%016lx]\n", LowBottom, LowTop, HighBottom, HighTop));

  MmioWrite32 (VtdUnitBaseAddress + R_PMEN_LOW_BASE_REG,    VtdPmrHob->ProtectedLowBase);
  MmioWrite32 (VtdUnitBaseAddress + R_PMEN_LOW_LIMITE_REG,  VtdPmrHob->ProtectedLowLimit - 1);
  MmioWrite64 (VtdUnitBaseAddress + R_PMEN_HIGH_BASE_REG,   VtdPmrHob->ProtectedHighBase);
  MmioWrite64 (VtdUnitBaseAddress + R_PMEN_HIGH_LIMITE_REG, VtdPmrHob->ProtectedHighLimit - 1);

  CapReg.Uint64 = MmioRead64 (VtdUnitBaseAddress + R_CAP_REG);
  if (CapReg.Bits.PLMR == 0 || CapReg.Bits.PHMR == 0) {
    DEBUG ((DEBUG_ERROR, "CapReg.PLMR - 0x%x, CapReg.PHMR - 0x%x\n", CapReg.Bits.PLMR, CapReg.Bits.PHMR));
    Status = EFI_UNSUPPORTED;
  }

  Reg32 = MmioRead32 (VtdUnitBaseAddress + R_PMEN_ENABLE_REG);
  if (Reg32 == 0xFFFFFFFF) {
    DEBUG ((DEBUG_ERROR, "R_PMEN_ENABLE_REG - 0x%x\n", Reg32));
    Status = EFI_ABORTED;
  }

  if (((Reg32 & BIT0) == 0) && (Status == EFI_SUCCESS)) {
    MmioWrite32 (VtdUnitBaseAddress + R_PMEN_ENABLE_REG, BIT31);
    do {
      Reg32 = MmioRead32 (VtdUnitBaseAddress + R_PMEN_ENABLE_REG);
    } while((Reg32 & BIT0) == 0);
  }

  DEBUG ((DEBUG_INFO, "EnablePmr Status - %r\n", Status));
}

/**
  Platform Disable PMR
  @param VtdUnitBaseAddress The base address of the VTd engine.
**/
VOID
PlatformDisablePMR (
  IN UINTN            VtdUnitBaseAddress
  )
{
  UINT32              Reg32;
  EFI_STATUS          Status;
  VTD_CAP_REG         CapReg;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;
  DEBUG ((DEBUG_INFO, "IOMMU BaseAdr(0x%x) \n", VtdUnitBaseAddress));

  CapReg.Uint64 = MmioRead64 (VtdUnitBaseAddress + R_CAP_REG);
  if (CapReg.Bits.PLMR == 0 || CapReg.Bits.PHMR == 0) {
    Status = EFI_UNSUPPORTED;
  }

  Reg32 = MmioRead32 (VtdUnitBaseAddress + R_PMEN_ENABLE_REG);
  if (Reg32 == 0xFFFFFFFF) {
    DEBUG ((DEBUG_ERROR, "R_PMEN_ENABLE_REG - 0x%x\n", Reg32));
    Status = EFI_ABORTED;
  }

  if (((Reg32 & BIT0) != 0) && (Status == EFI_SUCCESS)) {
    MmioWrite32 (VtdUnitBaseAddress + R_PMEN_ENABLE_REG, 0x0);
    do {
      Reg32 = MmioRead32 (VtdUnitBaseAddress + R_PMEN_ENABLE_REG);
    } while((Reg32 & BIT0) != 0);
  }

  DEBUG ((DEBUG_INFO, "DisablePmr Status - %r\n", Status));
}

/**
  The callback function for SiInitDone.
  It reinstalls VTD_INFO_PPI.

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
EFIAPI
SiInitDonePpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  VOID                         *Hob;
  PLATFORM_VTD_PEI_STATE_INFO  *VtdPeiStateInfo;
  EFI_STATUS                   Status;
#if FixedPcdGetBool(PcdIpuEnable) == 1
  UINT32                       VtdBase;
#endif


  Status                   = EFI_UNSUPPORTED;

  Hob = GetFirstGuidHob (&mPlatformVtdPeiStateInfoGuid);
  if (Hob == NULL) {
    ASSERT (0);
    return EFI_NOT_FOUND;
  }
  VtdPeiStateInfo = GET_GUID_HOB_DATA (Hob);

  //
  // Enable IOP IOMMU PMR
  //
  if (!VtdPeiStateInfo->IopVtdEnabled) {
    DEBUG ((DEBUG_INFO, "SiInitDonePpiNotifyCallback for iommu: Enable IOP PMR\n"));
    Status = InstallPlatformIopVtdInfoPpi ();
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR , "Failed to install a ppi - %r\n", Status));
    } else {
      VtdPeiStateInfo->IopVtdEnabled = TRUE;
    }
  }

  //
  // Enable IPU IOMMU PMR
  // TODO: WA Tag for PMR enabling on individual IOMMU
  //
#if FixedPcdGetBool(PcdIpuEnable) == 1
  if (!VtdPeiStateInfo->IpuVtdEnabled) {
    DEBUG ((DEBUG_INFO, "SiInitDonePpiNotifyCallback for iommu: Enable IPU PMR\n"));
    VtdBase = ReadVtdBaseAddress(1);
    if (VtdBase != 0) {
      PlatformEnablePMR(VtdBase);
      VtdPeiStateInfo->IpuVtdEnabled = TRUE;
    } else {
      DEBUG ((DEBUG_ERROR, "IPU VTd base is zero, don't enable PMR\n"));
      Status = EFI_UNSUPPORTED;
    }
  }
#endif

  return Status;
}

#if FixedPcdGetBool(PcdFspModeSelection) == 1
EFI_PEI_NOTIFY_DESCRIPTOR mSiInitDoneNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gFspSiliconInitDonePpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) SiInitDonePpiNotifyCallback
};
#else
EFI_PEI_NOTIFY_DESCRIPTOR mSiInitDoneNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEndOfSiInitPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) SiInitDonePpiNotifyCallback
};
#endif

/**
  The callback function for EndOfPeiPpi.
  It enables IGD IOMMU PMR

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS            The function completed successfully.
  @retval     EFI_UNSUPPORTED        Either IGD or IPU VTd is disabled or not supported
**/
EFI_STATUS
EFIAPI
EndOfPeiPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  VOID                         *Hob;
  PLATFORM_VTD_PEI_STATE_INFO  *VtdPeiStateInfo;
  EFI_STATUS                   Status;
  UINT32                       VtdBase;


  Status = EFI_SUCCESS;

  Hob = GetFirstGuidHob (&mPlatformVtdPeiStateInfoGuid);
  if (Hob == NULL) {
    ASSERT (0);
    return EFI_NOT_FOUND;
  }
  VtdPeiStateInfo = GET_GUID_HOB_DATA (Hob);

  //
  // Enable IGD IOMMU PMR
  //
  if (!VtdPeiStateInfo->IgdVtdEnabled) {
    DEBUG ((DEBUG_INFO, "EndOfPeiPpiNotifyCallback for iommu: Enable IGD PMR\n"));

    VtdBase = ReadVtdBaseAddress(0);
    if (VtdBase != 0) {
      PlatformEnablePMR(VtdBase);
      VtdPeiStateInfo->IgdVtdEnabled = TRUE;
    } else {
      DEBUG ((DEBUG_ERROR, "IGD VTd base is zero\n"));
      Status = EFI_UNSUPPORTED;
    }
  }

  return Status;
}

EFI_PEI_NOTIFY_DESCRIPTOR mEndOfPeiNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) EndOfPeiPpiNotifyCallback
};

/*
  Enable DMA buffer in VTd

  @param[in] VtdEngineIndex         VTd engine index.

  @retval EFI_SUCCESS               Enable successfully.
  @retval EFI_INVALID_PARAMETER     Input parameters are invalid.
  @retval EFI_UNSUPPORTED           VTd base is zero.
  @retval EFI_OUT_OF_RESOURCES      There is no additional space in the PPI database.
*/
EFI_STATUS
EFIAPI
EnableDmaBuffer (
  IN VTD_ENGINE_INDEX VtdEngineIndex
  )
{
  VOID                         *Hob;
  PLATFORM_VTD_PEI_STATE_INFO  *VtdPeiStateInfo;
  EFI_STATUS                   Status;
  UINT32                       VtdBase;

  Status                       = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "Platform VTd Enable Dma Buffer PPI : EnableDmaBuffer(%d)\n", VtdEngineIndex));

  Hob = GetFirstGuidHob (&mPlatformVtdPeiStateInfoGuid);
  if (Hob == NULL) {
    ASSERT (0);
    return EFI_NOT_FOUND;
  }
  VtdPeiStateInfo = GET_GUID_HOB_DATA (Hob);

  if (VtdEngineIndex == IOP_VTD) {
    //
    //Enable IOP IOMMU PMR
    //
    if (!VtdPeiStateInfo->IopVtdEnabled) {
      Status = InstallPlatformIopVtdInfoPpi ();
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR , "Failed to install a ppi - %r\n", Status));
      } else {
        VtdPeiStateInfo->IopVtdEnabled = TRUE;
        DEBUG ((DEBUG_INFO, "Enable Dma for IOP done\n"));
      }
    }
  } else if (VtdEngineIndex == IPU_VTD) {
#if FixedPcdGetBool(PcdIpuEnable) == 1
    //
    //Enable IPU IOMMU PMR
    //
    if (!VtdPeiStateInfo->IpuVtdEnabled) {
      VtdBase = ReadVtdBaseAddress(1);
      if (VtdBase != 0) {
        PlatformEnablePMR(VtdBase);
        VtdPeiStateInfo->IpuVtdEnabled = TRUE;
      } else {
        DEBUG ((DEBUG_ERROR, "IPU VTd base is zero, don't enable PMR\n"));
        Status = EFI_UNSUPPORTED;
      }
    }
#endif
  } else if (VtdEngineIndex == IGD_VTD) {
    //
    // Enable IGD IOMMU PMR
    //
    if (!VtdPeiStateInfo->IgdVtdEnabled) {
      VtdBase = ReadVtdBaseAddress(0);
      if (VtdBase != 0) {
        PlatformEnablePMR(VtdBase);
        VtdPeiStateInfo->IgdVtdEnabled = TRUE;
      } else {
        DEBUG ((DEBUG_ERROR, "IGD VTd base is zero\n"));
        Status = EFI_UNSUPPORTED;
      }
    }
  } else {
    Status = EFI_INVALID_PARAMETER;
  }
  return Status;
}

EFI_PEI_PPI_DESCRIPTOR mPlatformVtdEnableDmaBufferPpiDesc = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPlatformVTdEnableDmaBufferPpiGuid,
  (VOID*) (UINTN) EnableDmaBuffer
};

/**
  Check Iommu Ability base on Vtd Policy.
**/
VOID
PeiCheckIommuSupport (
  VOID
  )
{
  EFI_STATUS                      Status;
  SA_SETUP                        *SaSetup;
  SETUP_DATA                      *SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINT32                          McD0BaseAddress;
  UINT32                          MchBar;
  BOOLEAN                         IopVtdSupport;
  UINT32                          MchBarIopVtdOffsetValue;
  UINT32                          VtdBaseAddress;

  SetupData                       = NULL;
  SaSetup                         = NULL;
  VarSize                         = 0;
  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  VarSize = sizeof (SETUP_DATA);
  SetupData = AllocateZeroPool (VarSize);
  if (SetupData == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to allocate SetupData size\n"));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetVariable (SetupData) failed, Status:%r\n", Status));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  //
  // Locate SaSetup variables
  //
  VarSize = sizeof (SA_SETUP);
  SaSetup = AllocateZeroPool (VarSize);
  if (SaSetup == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to allocate SaSetup size\n"));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"SaSetup",
                               &gSaSetupVariableGuid,
                               NULL,
                               &VarSize,
                               SaSetup
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetVariable (SaSetup) failed, Status:%r\n", Status));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  if (SaSetup->EnableVtd != 0x00) {
    McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
    MchBar = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) & ~B_SA_MCHBAR_MCHBAREN_MASK;

    //
    // Check if Vtd is hardware supported.
    //
    MchBarIopVtdOffsetValue = MmioRead32 (MchBar + GetIopVtdOffset ());
    if (MchBarIopVtdOffsetValue == 0) {
      VtdBaseAddress = (UINT32) mPlatformIopVTdSample.IopDrhd.RegisterBaseAddress;
    } else {
      VtdBaseAddress = MchBarIopVtdOffsetValue;
    }

    MmioWrite32 (MchBar + GetIopVtdOffset (), VtdBaseAddress | BIT0);
    IopVtdSupport = (BOOLEAN) (MmioRead32 (MchBar + GetIopVtdOffset ()) & BIT0);

    DEBUG ((DEBUG_INFO, "Iop Vtd Base Address: 0x%x\n", MchBarIopVtdOffsetValue));
    DEBUG ((DEBUG_INFO, "Iop Vtd Support: %d\n", IopVtdSupport));

    //
    // Restore to previous value
    //
    MmioWrite32 (MchBar + GetIopVtdOffset (), MchBarIopVtdOffsetValue);
  } else {
    IopVtdSupport = FALSE;
  }

  if ((SaSetup->EnableVtd == 0x00) || (IopVtdSupport == FALSE) || (SaSetup->VtdIgdEnable == 0 && SaSetup->VtdIpuEnable == 0 && SaSetup->VtdIopEnable == 0)) {
    //
    // Set PcdVTdPolicyPropertyMask to 0 when VTd disable
    //
    PcdSet8S (PcdVTdPolicyPropertyMask, 0x00);
  } else {

    //
    // This Control Iommu setup option is mainly for debug purpose.
    // It might be removed when feature is stable
    //
    if (SetupData->ControlIommu != 0x00) {
      PcdSet8S (PcdVTdPolicyPropertyMask, 0x01);
    } else {
      PcdSet8S (PcdVTdPolicyPropertyMask, 0x00);
    }
  }

Exit:
  DEBUG ((DEBUG_INFO, "PcdVTdPolicyPropertyMask value: %x\n", PcdGet8(PcdVTdPolicyPropertyMask)));

  if (SetupData != NULL) {
    FreePool (SetupData);
  }

  if (SaSetup != NULL) {
    FreePool (SaSetup);
  }

}

/**
  This function handles S3 resume task at the end of PEI

  @param[in] PeiServices    Pointer to PEI Services Table.
  @param[in] NotifyDesc     Pointer to the descriptor for the Notification event that
                            caused this function to execute.
  @param[in] Ppi            Pointer to the PPI data associated with this function.

  @retval EFI_STATUS        Always return EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
S3EndOfPeiNotify(
  IN EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDesc,
  IN VOID                      *Ppi
  )
{
  EFI_STATUS          Status;
  UINT32              VtdBase;

  Status = EFI_SUCCESS;

  DEBUG((DEBUG_INFO, "PlatfromVTdPmr S3EndOfPeiNotify\n"));

  if ((PcdGet8(PcdVTdPolicyPropertyMask) & BIT1) == 0) {

    //Disable IGD IOMMU PMR
    VtdBase = ReadVtdBaseAddress(0);
    if (VtdBase != 0) {
        PlatformDisablePMR(VtdBase);
    } else {
      DEBUG ((DEBUG_ERROR, "IGD VTd base is zero\n"));
      Status = EFI_UNSUPPORTED;
    }

    //Disable IPU IOMMU PMR
#if FixedPcdGetBool(PcdIpuEnable) == 1
    VtdBase = ReadVtdBaseAddress(1);
    if (VtdBase != 0) {
        PlatformDisablePMR(VtdBase);
    } else {
      DEBUG ((DEBUG_ERROR, "IPU VTd base is zero\n"));
      Status = EFI_UNSUPPORTED;
    }
#endif
  }
  return Status;
}

EFI_PEI_NOTIFY_DESCRIPTOR mS3EndOfPeiNotifyDesc = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  S3EndOfPeiNotify
};

/**
  Platform VTd Info sample driver.

  @param[in] FileHandle         Handle of the file being invoked.
  @param[in] PeiServices        Describes the list of possible PEI Services.

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_UNSUPPORTED       Iommu is unsupported.
**/
EFI_STATUS
EFIAPI
PlatformVTdInfoSampleInitialize (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                      Status;
  EFI_BOOT_MODE                   BootMode;
  PLATFORM_VTD_PEI_STATE_INFO     *VtdPeiStateInfo;

  Status = EFI_UNSUPPORTED;
  BootMode = 0;

  DEBUG ((DEBUG_INFO, "PlatformVTdInfoSampleInitialize START\n"));

  //
  // check if we should enable Iommu
  //
  PeiCheckIommuSupport ();

  if ((PcdGet8(PcdVTdPolicyPropertyMask) & BIT0) == 0) {
    DEBUG ((DEBUG_INFO, "Iommu is unsupported\n"));
    DEBUG ((DEBUG_INFO, "PlatformVTdInfoSampleInitialize END\n"));
    return Status;
  }

  if (!PcdGetBool (PcdVTdDisablePeiPmrProtection)) {
    //
    // Register a notification for SiInitDone to reprogram the IOP and IPU PMR
    //
    Status = PeiServicesNotifyPpi (&mSiInitDoneNotifyList);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR , "Failed to register a notification - %r\n", Status));
    }

    //
    // Register a notification for End of PEI to program the IGD PMR
    //
    Status = PeiServicesNotifyPpi (&mEndOfPeiNotifyList);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR , "Failed to register a notification - %r\n", Status));
    }
  } else {
    DEBUG ((DEBUG_INFO, "PEI VTd DMA PMR protection disabled !\n"));
  }

  InitGlobalVtd ();

  Status = PeiServicesInstallPpi (&mPlatformNullRootEntryTableDesc);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR , "Failed to install a null root entry table ppi - %r\n", Status));
  }

  if (!PcdGetBool (PcdVTdDisablePeiPmrProtection)) {
    Status = InstallPlatformIopVtdInfoPpi ();
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR , "Failed to install a ppi - %r\n", Status));
    }

    //
    // Register EndOfPei Notify for S3
    //
    PeiServicesGetBootMode (&BootMode);
    if (BootMode == BOOT_ON_S3_RESUME) {
      Status = PeiServicesNotifyPpi (&mS3EndOfPeiNotifyDesc);
      DEBUG ((DEBUG_ERROR , "Failed to install a notification - %r\n", Status));
    }
  }

  VtdPeiStateInfo = BuildGuidHob (&mPlatformVtdPeiStateInfoGuid, sizeof(PLATFORM_VTD_PEI_STATE_INFO));
  if (VtdPeiStateInfo == NULL) {
    ASSERT (0);
    return EFI_OUT_OF_RESOURCES;
  }
  VtdPeiStateInfo->IopVtdEnabled = FALSE;
  VtdPeiStateInfo->IpuVtdEnabled = FALSE;
  VtdPeiStateInfo->IgdVtdEnabled = FALSE;

  if (!PcdGetBool (PcdVTdDisablePeiPmrProtection)) {
    Status = PeiServicesInstallPpi (&mPlatformVtdEnableDmaBufferPpiDesc);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR , "Failed to install vtd enable dma buffer ppi - %r\n", Status));
    }
  }

  DEBUG ((DEBUG_INFO, "PlatformVTdInfoSampleInitialize END\n"));
  return Status;
}

