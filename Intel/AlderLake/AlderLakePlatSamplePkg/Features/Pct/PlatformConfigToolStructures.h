/** @file
 Header file for Platform Config Tool Structures.

@copyright
 INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PLATFORM_CONFIG_TOOL_STRUCTURES_H_
#define _PLATFORM_CONFIG_TOOL_STRUCTURES_H_

#include <Uefi.h>

#pragma pack(push, 1)

typedef enum {
  PctPreMemGpio         = 1,
  PctPostMemGpio        = 2,
  PctAcpi               = 3
} PCT_CONFIG_DATA_TYPE;

typedef struct {
  UINT16  BoardId;
  UINT16  Revision;
  UINT8   Rsvd[8];
} PCT_SECTION_HEADER;

typedef struct {
  EFI_GUID  Guid;
  UINT8     Revision;
  UINT16    Count;
  UINT8     PctRevision[3];
  UINT8     Rsvd[2];
} PCT_GPIO_BLOCK_HDR, PCT_VERB_TABLE_HDR, PCT_USB_OCC_MAP_HDR, PCT_PCI_CLOCK_MAP_HDR, PCT_ACPI_BLOCK_HDR;

#pragma pack(pop)

typedef struct {
  PCT_CONFIG_DATA_TYPE  PctDataType;
  EFI_GUID              *Guid;
} PCT_CONFIG_DATA_TYPE_GUID;

typedef struct {
  UINTN GpioPinData;
  CHAR8 *GpioPinDataString;
} GPIO_PIN_STRING;

#endif // _PLATFORM_CONFIG_TOOL_STRUCTURES_H_
