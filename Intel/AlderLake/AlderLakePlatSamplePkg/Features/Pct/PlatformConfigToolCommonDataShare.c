/** @file
 Common Data Share for Platform Configuration Data Tool Init.

@copyright
 INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.rporation.

@par Specification Reference:
**/

#ifndef _PLATFORM_CONFIG_TOOL_COMMON_DATA_SHARE_H_
#define _PLATFORM_CONFIG_TOOL_COMMON_DATA_SHARE_H_

#include "PlatformConfigToolInit.h"
#include "PlatformConfigToolStructures.h"
#include  <Pins/GpioPinsVer2Lp.h>
#include  <Pins/GpioPinsVer2H.h>

//
// Commom File Names
//
CHAR8 *PctConfigFileNames[] = {
  "GpioPreMem",
  "GpioPostMem",
  "AcpiConfig"
};

//
// Guid File Names
//
EFI_GUID  *PctConfigFileGuids[] = {
            &gPctPreMemFileNameGuid,
            &gPctPostMemFileNameGuid,
            &gPctAcpiFileNameGuid
};

UINTN  PctConfigFileCount = sizeof (PctConfigFileGuids) / sizeof (EFI_GUID *);
//
// Data Type Guids
//
PCT_CONFIG_DATA_TYPE_GUID  PctConfigDataTypeGuids[] = {
                             {PctPreMemGpio, &gPctPreMemGpioDataTypeGuid},
                             {PctPostMemGpio, &gPctPostMemGpioDataTypeGuid},
                             {PctAcpi, &gPctAcpiDataTypeGuid}
};

UINTN PctConfigDataTypeGuidsCount = sizeof (PctConfigDataTypeGuids) / sizeof (PCT_CONFIG_DATA_TYPE_GUID);

static GPIO_PIN_STRING ChipsetId [] = {
  {GPIO_VER2_H_CHIPSET_ID,  "GPIO_VER2_H_GROUP_"},
  {GPIO_VER2_LP_CHIPSET_ID, "GPIO_VER2_LP_GROUP_"}
};

static GPIO_PIN_STRING Group [] = {
  {GPIO_VER2_H_GROUP_GPP_A,    "GPP_A"},
  {GPIO_VER2_H_GROUP_GPP_R,    "GPP_R"},
  {GPIO_VER2_H_GROUP_GPP_B,    "GPP_B"},
  {GPIO_VER2_H_GROUP_VGPIO_0,  "VGPIO_0"},
  {GPIO_VER2_H_GROUP_GPP_D,    "GPP_D"},
  {GPIO_VER2_H_GROUP_GPP_C,    "GPP_C"},
  {GPIO_VER2_H_GROUP_GPP_S,    "GPP_S"},
  {GPIO_VER2_H_GROUP_GPP_G,    "GPP_G"},
  {GPIO_VER2_H_GROUP_VGPIO,    "VGPIO"},
  {GPIO_VER2_H_GROUP_GPD,      "GPD"},
  {GPIO_VER2_H_GROUP_GPP_E,    "GPP_E"},
  {GPIO_VER2_H_GROUP_GPP_F,    "GPP_F"},
  {GPIO_VER2_H_GROUP_VGPIO_3,  "VGPIO_3"},
  {GPIO_VER2_H_GROUP_GPP_H,    "GPP_H"},
  {GPIO_VER2_H_GROUP_GPP_J,    "GPP_J"},
  {GPIO_VER2_H_GROUP_GPP_K,    "GPP_K"},
  {GPIO_VER2_H_GROUP_GPP_I,    "GPP_I"},
  {GPIO_VER2_H_GROUP_JTAG,     "JTAG"},
  {GPIO_VER2_H_GROUP_CPU,      "CPU"},
  {GPIO_VER2_LP_GROUP_GPP_B,   "GPP_B"},
  {GPIO_VER2_LP_GROUP_GPP_T,   "GPP_T"},
  {GPIO_VER2_LP_GROUP_GPP_A,   "GPP_A"},
  {GPIO_VER2_LP_GROUP_GPP_R,   "GPP_R"},
  {GPIO_VER2_LP_GROUP_SPI,     "SPI"},
  {GPIO_VER2_LP_GROUP_GPD,     "GPD"},
  {GPIO_VER2_LP_GROUP_GPP_S,   "GPP_S"},
  {GPIO_VER2_LP_GROUP_GPP_H,   "GPP_H"},
  {GPIO_VER2_LP_GROUP_GPP_D,   "GPP_D"},
  {GPIO_VER2_LP_GROUP_GPP_U,   "GPP_U"},
  {GPIO_VER2_LP_GROUP_VGPIO,   "VGPIO"},
  {GPIO_VER2_LP_GROUP_GPP_C,   "GPP_C"},
  {GPIO_VER2_LP_GROUP_GPP_F,   "GPP_F"},
  {GPIO_VER2_LP_GROUP_HVCMOS,  "HVCMOS"},
  {GPIO_VER2_LP_GROUP_GPP_E,   "GPP_E"},
  {GPIO_VER2_LP_GROUP_JTAG,    "JTAG"},
  {GPIO_VER2_LP_GROUP_CPU,     "CPU"},
  {GPIO_VER2_LP_GROUP_VGPIO_3, "VGPIO_3"}
};

static GPIO_PIN_STRING GpioPadMode [] = {
  {GpioHardwareDefault, "Default"},
  {GpioPadModeGpio, "Gpio"},
  {GpioPadModeNative1, "Native1"},
  {GpioPadModeNative2, "Native2"},
  {GpioPadModeNative3, "Native3"},
  {GpioPadModeNative4, "Native4"},
  {GpioPadModeNative5, "Native5"},
  {GpioPadModeNative6, "Native6"},
  {GpioPadModeNative6, "Native7"}
};

static GPIO_PIN_STRING GpioPadOwner [] = {
  {GpioHostOwnDefault, "Default"},
  {GpioHostOwnAcpi, "Acpi"},
  {GpioHostOwnGpio, "Gpio"}
};

static GPIO_PIN_STRING GpioDirection [] = {
  {GpioDirDefault, "Default"},
  {GpioDirInOut, "InOut"},
  {GpioDirInInvOut, "InInvOut"},
  {GpioDirIn, "In"},
  {GpioDirInInv, "InInv"},
  {GpioDirOut, "Out"},
  {GpioDirNone, "None"}
};

static GPIO_PIN_STRING GpioOutputState [] = {
  {GpioOutDefault, "Default"},
  {GpioOutLow, "Low"},
  {GpioOutHigh, "High"}
};

static GPIO_PIN_STRING GpioInterruptConfiguration [] = {
  {GpioIntDefault, "Default"},
  {GpioIntDis, "Dis"},
  {GpioIntNmi, "Nmi"},
  {GpioIntSmi, "Smi"},
  {GpioIntSci, "Sci"},
  {GpioIntApic, "Apic"},
  {GpioIntLevel, "Level"},
  {GpioIntEdge, "Edge"},
  {GpioIntLvlEdgDis, "LvlEdgDis"},
  {GpioIntBothEdge, "BothEdge"}
};

static GPIO_PIN_STRING GpioResetConfiguration [] = {
  {GpioResetDefault, "Default"},
  {GpioResumeReset, "Resume"},
  {GpioHostDeepReset, "HostDeep"},
  {GpioPlatformReset, "Platform"},
  {GpioDswReset, "Dsw"}
};

static GPIO_PIN_STRING GpioElectricalConfiguration [] = {
  {GpioTermDefault, "Default"},
  {GpioTermNone, "None"},
  {GpioTermWpd5K, "Wpd5K"},
  {GpioTermWpd20K, "Wpd20K"},
  {GpioTermWpu1K, "Wpu1K"},
  {GpioTermWpu2K, "Wpu2K"},
  {GpioTermWpu5K, "Wpu5K"},
  {GpioTermWpu20K, "Wpu20K"},
  {GpioTermWpu1K2K, "Wpu1K2K"},
  {GpioTermNative, "Native"},
};

static GPIO_PIN_STRING GpioLockConfiguration [] = {
  {GpioLockDefault, "Default"},
  {GpioPadConfigUnlock, "PadConfigUn"},
  {GpioPadConfigLock, "PadConfig"},
  {GpioOutputStateUnlock, "OutputStateUn"},
  {GpioPadUnlock, "PadUn"},
  {GpioPadLock, "Pad"}
};

GPIO_PIN_STRING GpioOtherConfiguration [] = {
  {GpioRxRaw1Default, "Default"},
  {GpioRxRaw1Dis, "Dis"},
  {GpioRxRaw1En, "En"}
};

GPIO_PIN_STRING *GpioConfiguration [] = {
  ChipsetId,
  Group,
  GpioPadMode,
  GpioPadOwner,
  GpioDirection,
  GpioOutputState,
  GpioInterruptConfiguration,
  GpioResetConfiguration,
  GpioElectricalConfiguration,
  GpioLockConfiguration,
  GpioOtherConfiguration
};

UINTN DebugOutputGpioCount [] = {
  sizeof (ChipsetId) / sizeof (GPIO_PIN_STRING),
  sizeof (Group) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioPadMode) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioPadOwner) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioDirection) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioOutputState) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioInterruptConfiguration) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioResetConfiguration) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioElectricalConfiguration) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioLockConfiguration) / sizeof (GPIO_PIN_STRING),
  sizeof (GpioOtherConfiguration) / sizeof (GPIO_PIN_STRING)
};

#endif // _PLATFORM_CONFIG_DATA_SHARE_H_
