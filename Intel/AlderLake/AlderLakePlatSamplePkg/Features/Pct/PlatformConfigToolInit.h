/** @file
 General Header file for PCT initialization.

@copyright
 INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PLATFORM_CONFIG_INIT_H_
#define _PLATFORM_CONFIG_INIT_H_

#include <Uefi.h>
#include "PlatformConfigToolGuids.h"
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BoardConfigLib.h>
#include <Protocol/AcpiSystemDescriptionTable.h>
#include "PlatformConfigToolStructures.h"
#include <Include/Pi/PiFirmwareFile.h>
#include <Include/PlatformBoardId.h>

extern  UINTN                       PctConfigFileCount;
extern  UINTN                       PctConfigDataTypeGuidsCount;
extern  CHAR8                       *PctConfigFileNames[];
extern  EFI_GUID                    *PctConfigFileGuids[];
extern  PCT_CONFIG_DATA_TYPE_GUID   PctConfigDataTypeGuids[];

extern  UINTN                       DebugOutputGpioCount[];
extern  GPIO_PIN_STRING             *GpioConfiguration [];

typedef enum {
  GpioConfigTypeChipsetId,
  GpioConfigTypeGroup,
  GpioConfigTypePadMode,
  GpioConfigTypeOwner,
  GpioConfigTypeDirection,
  GpioConfigTypeOutput,
  GpioConfigTypeInterrupt,
  GpioConfigTypePower,
  GpioConfigTypeElectrical,
  GpioConfigTypeLock,
  GpioConfigTypeOther
} DEBUG_GPIO_CONFIG_TYPE;

/**
  OutData

  Output debug bytes to serial port.

  @param    DataPointer          Address of byte buffer.

  @param    Count                Number of bytes.

**/
VOID
OutData (
  UINT8     *DataPointer,
  UINT16    Count
  );

/**
  OutGpios

  Output GPIO configuration to serial in raw format.

  @param    GpioPointer          Address of byte buffer.

  @param    GpioCount            Number of bytes.

**/
VOID
OutGpios (
  UINT8     *GpioPointer,
  UINTN     GpioCount
  );

/**
  OutGpiosVerbose

  Output GPIO configuration to serial formatted
  with GPIO Config field strings.

  @param    GpioPointer          Address of byte buffer.

  @param    GpioCount            Number of bytes.

**/
VOID
OutGpiosVerbose (
  UINT8     *GpioPointer,
  UINTN     GpioCount
  );

/**
  GetBoardId

  Return board ID read from EC.

**/
UINT16
GetBoardId (
  );


#endif // _PLATFORM_CONFIG_INIT_H_
