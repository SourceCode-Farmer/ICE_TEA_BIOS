/** @file
  Source code for Platform Configuration Tool in Pre/Post Mem.

@copyright
 INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.rporation.

@par Specification Reference:
**/

#include "PlatformConfigToolInit.h"
#include "PlatformConfigToolStructures.h"
#include <Library/EcCommands.h>
#include <Library/EcMiscLib.h>
#include <Include/PlatformBoardId.h>
#include <Library/UefiLib.h>

CHAR8 UnknownStr[] = "Unknown";


/**
  OutData

  Output debug bytes to serial port.

  @param    DataPointer          Address of byte buffer.

  @param    Count                Number of bytes.

**/

VOID
OutData (
  UINT8     *DataPointer,
  UINT16    Count
  )
{
  UINT16    Index;

  DEBUG_CODE_BEGIN ();
  DEBUG ((DEBUG_INFO, "PCT_DBG: Debug out, count = %d", Count));

  for (Index = 0; Index < Count; Index++) {
    if ((Index % 16) == 0) {
      DEBUG ((DEBUG_INFO, "\n"));
    }
    DEBUG ((DEBUG_INFO, "%02x ", DataPointer[Index]));
  }

  DEBUG ((DEBUG_INFO, "\n"));
  DEBUG_CODE_END ();

  return;
}

CHAR8*
ReturnGpioConfigString (
  UINTN  GpioConfigType,
  UINTN  GpioConfigTypeData
  )
{
  UINTN  Index;
  CHAR8  *ReturnString;

  Index = 0;
  ReturnString = UnknownStr;

  while ((Index < DebugOutputGpioCount[GpioConfigType]) &&
          (GpioConfiguration[GpioConfigType][Index].GpioPinData != GpioConfigTypeData)) Index++;

  if (Index < DebugOutputGpioCount[GpioConfigType]) {
    ReturnString = GpioConfiguration[GpioConfigType][Index].GpioPinDataString;
  }

  return ReturnString;
}

/**
  OutGpios

  Output GPIO configuration to serial in raw format.

  @param    GpioPointer          Address of byte buffer.

  @param    GpioCount            Number of bytes.

**/
VOID
OutGpios (
  UINT8   *GpioPointer,
  UINTN   GpioCount
  )
{
  UINTN   Index;

  DEBUG_CODE_BEGIN ();
  DEBUG ((DEBUG_INFO, "\nGPIO set, Count = %d\n", GpioCount));
  DEBUG ((DEBUG_INFO, "Index   Pad    , PM, H, Dr, O, Icg, Pc, Ecg, L, Ost\n"));

  for (Index = 0; Index < GpioCount; Index++, GpioPointer += sizeof (GPIO_INIT_CONFIG)) {
    DEBUG ((DEBUG_INFO, "%04d,  %8x, %2x, %1x, %2x, %1x, %3x, %2x, %3x, %1x, %3x\n",
    Index,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioPad,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.PadMode,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.HostSoftPadOwn,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.Direction,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.OutputState,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.InterruptConfig,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.PowerConfig,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.ElectricalConfig,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.LockConfig,
    ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.OtherSettings
    ));
  }

  DEBUG ((DEBUG_INFO, "\n"));
  DEBUG_CODE_END ();

  return;
}

/**
  OutGpiosVerbose

  Output GPIO configuration to serial formatted
  with GPIO Config field strings.

  @param    GpioPointer          Address of byte buffer.

  @param    GpioCount            Number of bytes.

**/
VOID
OutGpiosVerbose (
  UINT8   *GpioPointer,
  UINTN   GpioCount
  )
{
  UINT16  Index;

  DEBUG_CODE_BEGIN ();
  DEBUG ((DEBUG_INFO, "\nGPIO set, Count = %d, Addr = %p\n", GpioCount, GpioPointer));
  DEBUG ((DEBUG_INFO, "Index,  {Group, Pin, {Pad Mode, Ownership, Direction, Output State, Interrupt Source, "));
  DEBUG ((DEBUG_INFO, "Interrupt Type, Reset Config, Termination, Lock Config, Lock Output, Other}}\n\n"));
  for (Index = 0; Index < GpioCount; Index++, GpioPointer += sizeof (GPIO_INIT_CONFIG)) {
    DEBUG ((DEBUG_INFO, "%04d, {%a%a, %02d, {GpioPadMode%a, GpioHostOwn%a, GpioDir%a, GpioOut%a, GpioInt%a, ",
      Index,
      ReturnGpioConfigString (GpioConfigTypeChipsetId, (((GPIO_INIT_CONFIG *)GpioPointer)->GpioPad) >> 24),
      ReturnGpioConfigString (GpioConfigTypeGroup, (((GPIO_INIT_CONFIG *)GpioPointer)->GpioPad) >> 16),
      (((GPIO_INIT_CONFIG *)GpioPointer)->GpioPad) & 0xFF,
      ReturnGpioConfigString (GpioConfigTypePadMode, ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.PadMode),
      ReturnGpioConfigString (GpioConfigTypeOwner, ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.HostSoftPadOwn),
      ReturnGpioConfigString (GpioConfigTypeDirection, ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.Direction),
      ReturnGpioConfigString (GpioConfigTypeOutput, ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.OutputState),
      ReturnGpioConfigString (GpioConfigTypeInterrupt,
        (((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.InterruptConfig) & B_GPIO_INT_CONFIG_INT_SOURCE_MASK)
      ));
    DEBUG ((DEBUG_INFO, "GpioInt%a, GpioTerm%a, Gpio%aReset, Gpio%aLock, Gpio%aLock, RwRaw1%a}}\n",
      ReturnGpioConfigString (GpioConfigTypeInterrupt,
        (((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.InterruptConfig) & B_GPIO_INT_CONFIG_INT_TYPE_MASK),
      ReturnGpioConfigString (GpioConfigTypeElectrical,
        (((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.ElectricalConfig) & B_GPIO_ELECTRICAL_CONFIG_TERMINATION_MASK),
      ReturnGpioConfigString (GpioConfigTypePower, ((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.PowerConfig),
      ReturnGpioConfigString (GpioConfigTypeLock,
        (((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.LockConfig) & B_GPIO_LOCK_CONFIG_PAD_CONF_LOCK_MASK),
      ReturnGpioConfigString (GpioConfigTypeLock,
        (((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.LockConfig) & B_GPIO_LOCK_CONFIG_OUTPUT_LOCK_MASK),
      ReturnGpioConfigString (GpioConfigTypeOther,
        (((GPIO_INIT_CONFIG *)GpioPointer)->GpioConfig.OtherSettings) & B_GPIO_OTHER_CONFIG_RXRAW_MASK)
      ));
  }

  DEBUG ((DEBUG_INFO, "\n"));
  DEBUG_CODE_END ();

  return;
}


