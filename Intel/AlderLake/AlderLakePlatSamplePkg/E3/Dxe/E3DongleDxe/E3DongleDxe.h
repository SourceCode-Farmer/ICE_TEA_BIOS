/** @file
  TigerLake iPCM DXE driver

  This DXE driver provides the support of the calibration data.

Copyright (c) 2019, Intel Corporation. All rights reserved.<BR>
This software and associated documentation (if any) is furnished
under a license and may only be used or copied in accordance
with the terms of the license. Except as permitted by such
license, no part of this software or documentation may be
reproduced, stored in a retrieval system, or transmitted in any
form or by any means without the express written consent of
Intel Corporation.

**/       

#ifndef _E3_DONGLE_DXE_H_
#define _E3_DONGLE_DXE_H_

#define E3_CALIBRATION_DATA_OFFSET_IN_PDR  0
#define E3_CALIBRATION_DATA_HEADER_SIGNATURE  0x4C433345 //"E3CL"

#define PDR_SIZE                           4*1024

/**
  E3 Calibration status
**/
typedef enum {
  E3DongleNotAttached,
  E3ReadPDRError,
  E3DataNotInPDR,
  E3DataAlreadyExistingButDifferent,
  E3DataAlreadyExistingAndSame,
  E3DataNotInDongle,
  E3DataI2CError,
  E3DataCommonError,
  E3DataUpdateSuccessful
} E3_CALIBRATION_STATUS;

#pragma pack(1)
/**
  E3 Calibration Data Structure Header
**/

typedef struct {
  UINT32  Signature;
  UINT16  Length;
  UINT8   Revision;
  UINT8   Checksum;
  UINT16  DataOffset;
  UINT16  DataLength;
} E3_CALIBRATION_DATA_HEADER;

#pragma pack()

UINT8
Checksum (
  IN VOID       *Buffer,
  IN UINTN      Size
  );

EFI_STATUS
CheckChecksum (
  IN VOID       *Buffer,
  IN UINTN      Size
  );


EFI_STATUS
ReadPDRData (
  IN UINTN      offset,
  IN UINTN      length,
  OUT UINT8     *buf
  );

#endif

