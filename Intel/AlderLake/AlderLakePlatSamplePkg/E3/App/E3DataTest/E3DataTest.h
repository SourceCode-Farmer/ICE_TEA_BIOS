/** @file
  This file defines the PCH SPI Protocol which implements the
  Intel(R) PCH SPI Host Controller Compatibility Interface.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2006 - 2016 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _E3_DONGLE_TEST_H_
#define _E3_DONGLE_TEST_H_

#define E3_CALIBRATION_DATA_OFFSET_IN_PDR  0
#define E3_CALIBRATION_DATA_HEADER_SIGNATURE  0x4C433345 //"E3CL"

#define PDR_SIZE                           4*1024

/**
  E3 Calibration status
**/
typedef enum {
  E3DongleNotAttached,
  E3DataNotInPDR,
  E3DataAlreadyExistingButDifferent,
  E3DataAlreadyExistingAndSame,
  E3DataNotInDongle,
  E3DataI2CError,
  E3DataCommonError,
  E3DataUpdateSuccessful
} E3_CALIBRATION_STATUS;

#pragma pack(1)
/**
  E3 Calibration Data Structure Header
**/

typedef struct {
  UINT32  Signature;
  UINT16  Length;
  UINT8   Revision;
  UINT8   Checksum;
  UINT16  DataOffset;
  UINT16  DataLength;
} E3_CALIBRATION_DATA_HEADER;

#pragma pack()

UINT8
Checksum (
  IN VOID       *Buffer,
  IN UINTN      Size
  );

EFI_STATUS
CheckChecksum (
  IN VOID       *Buffer,
  IN UINTN      Size
  );

#endif

