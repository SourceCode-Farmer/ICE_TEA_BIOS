/** @file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2012 - 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/IoLib.h>
#include <Library/UefiLib.h>

#include <Library/I2cAccessLib.h> 
//#include <Library/PchSerialIoLib.h> 
#include <Library/MmPciLib.h>
#include <Library/EcMiscLib.h> // AdvancedFeaturesContent
#include <Library/EcLib.h> // AdvancedFeaturesContent

#include "..\..\Include\E3DongleLib.h"
#include "E3DongleLibInternal.h"
#include "E3DongleLibPlatformInit.h"

#include <Library/LfcEcLib.h>

// For dongle measurement mode
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableIRPM_DongleMode[] =
{
   {GPIO_VER2_LP_GPP_B16,  {GpioPadModeNative2, GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SCL_IPCM         PCH to E3
   {GPIO_VER2_LP_GPP_B17,  {GpioPadModeNative2, GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SDA_IPCM         PCH to E3
};
GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mGpioTableIRPM_DongleModeSize = sizeof (mGpioTableIRPM_DongleMode) / sizeof (GPIO_INIT_CONFIG);  

// For online pch measurement mode
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableIRPM_OnlinePCHMode[] =
{
  {GPIO_VER2_LP_GPP_B16,  {GpioPadModeNative2, GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SCL_IPCM         PCH to E3
  {GPIO_VER2_LP_GPP_B17,  {GpioPadModeNative2, GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SDA_IPCM         PCH to E3 
};
GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mGpioTableIRPM_OnlinePCHModeSize = sizeof (mGpioTableIRPM_OnlinePCHMode) / sizeof (GPIO_INIT_CONFIG);  

// For online ish measurement mode
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableIRPM_OnlineISHMode[] =
{
  {GPIO_VER2_LP_GPP_B16,  {GpioPadModeNative4, GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SCL_IPCM         PCH to E3
  {GPIO_VER2_LP_GPP_B17,  {GpioPadModeNative4, GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SDA_IPCM         PCH to E3

};
GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mGpioTableIRPM_OnlineISHModeSize = sizeof (mGpioTableIRPM_OnlineISHMode) / sizeof (GPIO_INIT_CONFIG); 

//For iRPM disablement
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableIRPM_Disabled[] =
{
   {GPIO_VER2_LP_GPP_B16,  {GpioPadModeGpio,    GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SCL_IPCM
   {GPIO_VER2_LP_GPP_B17,  {GpioPadModeGpio,    GpioHostOwnGpio, GpioDirNone,  GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone}},   //ISH_I2C2_SDA_IPCM
};
GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mGpioTableIRPM_DisabledSize = sizeof (mGpioTableIRPM_Disabled) / sizeof (GPIO_INIT_CONFIG); 


/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries

**/
EFI_STATUS
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioDefinition,
  IN UINT16                           GpioTableCount
  )
{
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));

  Status = GpioConfigurePads (GpioTableCount, GpioDefinition);

  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));
  return Status;
}

//bOn - true: set E3 chip power down pin to high. when E3 chip is running, it should be set to high.
//          false:set E3 chip power down pin to low. when E3 chip is disabled, it should be set to low.

EFI_STATUS
iRPMChipPowerDownPin(
  IN BOOLEAN bOn
  )
{
  // On some designs, the E3 chip power down pin is pulled up to high by HW, so do nothing and return.
  
  return EFI_SUCCESS;
}


//bOn - true: turn on vbus for E3 chips, the E3 chips power is supplied by board in this case.
//          false: turn off vbus for E3 chips, in this case, the E3 chips power is disabled or supplied by dongle
EFI_STATUS
iRPMVbusControl(
  IN BOOLEAN bOn
  )

{
  // 5V Vbus for E3 chip power is always supplied by board 5VAUX, won't supplied by dongle
  return EFI_SUCCESS;
}

// Value: 0 - low; non-zero - high.
EFI_STATUS
iRPMGetE3ChipPowerState(
  OUT UINT8* Value
  )

{
  // On some designs, the 3.3V E3 power pin is connecting to EC ADC. 
  // But on some designs, 3.3V E3 power pin is not mornitored, in this case, do nothing and return FALSE.

  return EFI_NOT_FOUND;
}

//bOn - true: turn on power enable pin for E3 chips.
//          false: turn off power enable pin for E3 chips.
EFI_STATUS
iRPME3ChipPowerControl(
  IN BOOLEAN bOn
  )

{
  EFI_STATUS  Status;
	UINT8       Data = 0;

	Status = LfcEcLibEcRamRead (0x23, &Data);
	
	if (EFI_ERROR (Status)) {
    return Status;
  } 
	
  GPIO_INIT_CONFIG GpioTableIRPM_E3ChipPower_On[] =
    {
       {GPIO_VER2_LP_GPP_H13,  {GpioPadModeGpio,    GpioHostOwnGpio, GpioDirOut,   GpioOutHigh,    GpioIntDis, GpioResumeReset,    GpioTermNone}},// E3_PWR_EN_3V   
    };
  GPIO_INIT_CONFIG GpioTableIRPM_E3ChipPower_Off[] =
    {
       {GPIO_VER2_LP_GPP_H13,  {GpioPadModeGpio,    GpioHostOwnGpio, GpioDirOut,   GpioOutLow,    GpioIntDis, GpioResumeReset,    GpioTermNone}},// E3_PWR_EN_3V 
    };  

  if (bOn) {
    // Turn on E3 chips power
   Status = ConfigureGpio(GpioTableIRPM_E3ChipPower_On, 1);
   if ((Data & BIT1) != BIT1) {
      Data |= BIT1;
      Status = LfcEcLibEcRamWrite (0x23, Data);
   }
  }
  else {
    // Turn off E3 chips power
   Status = ConfigureGpio(GpioTableIRPM_E3ChipPower_Off, 1);
	 if (Data & BIT1) {
      Data &= (UINT8)~BIT1;
      Status = LfcEcLibEcRamWrite (0x23, Data);
   }
  }
  DEBUG((DEBUG_INFO, "iRPME3ChipPowerControl: set E3 chip power: Status=%r, bON=%d.\n", Status, bOn)); 

  return Status;
}

//bFast - true: turn on E3 chip fast mode. (Usually, set the E3 Slow pin to low)
//          false: set E3 chip to run in slow mode. (Usually, set the E3 Slow pin to high)
EFI_STATUS
iRPME3ChipFastSlowControl(
  IN BOOLEAN bFast
  )

{
  // On some designs, the E3 chip power down pin is pulled to low(fast mode) by HW, so do nothing and return. 

  return EFI_SUCCESS;
}

//bI2C - true: switch USB port to I2C mode
//          false: switch USB port to USB mode
EFI_STATUS
iRPMMuxSwitch(
  IN BOOLEAN bI2C
  )
{  
  EFI_STATUS  Status;
  GPIO_INIT_CONFIG GpioTableIRPM_Usb[] =
    {
       {GPIO_VER2_LP_GPP_H12,  {GpioPadModeGpio,    GpioHostOwnGpio, GpioDirOut,   GpioOutLow,    GpioIntDis, GpioResumeReset,    GpioTermNone}},// E3_MUX_SEL   
    };
  GPIO_INIT_CONFIG GpioTableIRPM_I2c[] =
    {
       {GPIO_VER2_LP_GPP_H12,  {GpioPadModeGpio,    GpioHostOwnGpio, GpioDirOut,   GpioOutHigh,    GpioIntDis, GpioResumeReset,    GpioTermNone}},// E3_MUX_SEL  
    };  

  if (bI2C) {
    // set to I2C
   Status = ConfigureGpio(GpioTableIRPM_I2c, 1);
  }
  else {
    // set to USB
   Status = ConfigureGpio(GpioTableIRPM_Usb, 1);
  }
  DEBUG((DEBUG_INFO, "iRPMMuxSwitch: Status=%r, bI2C=%d.\n", Status, bI2C)); 

  return Status;
}

// Do miscellaneous initialization (like GPIO initialization) on PCH to enable the dongle mode. 
EFI_STATUS
iRPMPCHGpioInitForDongleMode(
  VOID
  )
{
  GPIO_INIT_CONFIG                *GpioTablePreMem;
  UINT16                          GpioTablePreMemSize;  

  // Configure PCH GPIOs
  GpioTablePreMem = mGpioTableIRPM_DongleMode; 
  GpioTablePreMemSize = mGpioTableIRPM_DongleModeSize; 
  if (GpioTablePreMem != NULL && GpioTablePreMemSize != 0) {
    ConfigureGpio (GpioTablePreMem, GpioTablePreMemSize);
  }
  
  return EFI_SUCCESS;
}

//Do miscellaneous initialization (like GPIO initialization) on PCH to enable the ISH I2C mode. 
EFI_STATUS
iRPMPCHGpioInitForISHMode(
  VOID
  )
{
  GPIO_INIT_CONFIG                *GpioTablePreMem;
  UINT16                          GpioTablePreMemSize;  
  
  // Configure PCH GPIOs
  GpioTablePreMem = mGpioTableIRPM_OnlineISHMode; //(GPIO_INIT_CONFIG *) (UINTN) PcdGet32 (PcdBoardGpioTableIRPM_OnlineModePreMem);
  GpioTablePreMemSize = mGpioTableIRPM_OnlineISHModeSize; //PcdGet16 (PcdBoardGpioTableIRPM_OnlineModePreMemSize);
  if (GpioTablePreMem != NULL && GpioTablePreMemSize != 0) {
    ConfigureGpio (GpioTablePreMem, GpioTablePreMemSize);
  }

  return EFI_SUCCESS;
}

//Do miscellaneous initialization (like GPIO initialization) on PCH to enable the PCH I2C mode. 
EFI_STATUS
iRPMPCHGpioInitForPCHMode(
  VOID
  )
{
  GPIO_INIT_CONFIG                *GpioTablePreMem;
  UINT16                          GpioTablePreMemSize;  

  // Configure PCH GPIOs
  GpioTablePreMem = mGpioTableIRPM_OnlinePCHMode; //(GPIO_INIT_CONFIG *) (UINTN) PcdGet32 (PcdBoardGpioTableIRPM_OnlineModePreMem);
  GpioTablePreMemSize = mGpioTableIRPM_OnlinePCHModeSize; //PcdGet16 (PcdBoardGpioTableIRPM_OnlineModePreMemSize);
  if (GpioTablePreMem != NULL && GpioTablePreMemSize != 0) {
    ConfigureGpio (GpioTablePreMem, GpioTablePreMemSize);
  }
 
  return EFI_SUCCESS;
}

//Do miscellaneous initialization (like GPIO initialization) on PCH to disable the E3 solution.
EFI_STATUS
iRPMPCHGpioInitForDisablementMode(
  VOID
  )
{
  GPIO_INIT_CONFIG                *GpioTablePreMem;
  UINT16                          GpioTablePreMemSize;  
  
  // Configure PCH GPIOs
  GpioTablePreMem = mGpioTableIRPM_Disabled; //(GPIO_INIT_CONFIG *) (UINTN) PcdGet32 (PcdBoardGpioTableIRPM_DisabledPreMem);
  GpioTablePreMemSize = mGpioTableIRPM_DisabledSize; //PcdGet16 (PcdBoardGpioTableIRPM_DisabledPreMemSize);
  if (GpioTablePreMem != NULL && GpioTablePreMemSize != 0) {
    ConfigureGpio (GpioTablePreMem, GpioTablePreMemSize);
  }

  return EFI_SUCCESS;
}

