#ifndef _E3_DONGLE_LIB_PLATFORM_INIT_H_
#define _E3_DONGLE_LIB_PLATFORM_INIT_H_

#include <Base.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>


#define E3_DONGLE_ON_I2CBUS_NUM               5   //Start from 0

#endif //_E3_DONGLE_LIB_PLATFORM_INIT_H_