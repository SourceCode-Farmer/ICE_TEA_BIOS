#ifndef _E3_DONGLE_LIB_INTERNAL_H__
#define _E3_DONGLE_LIB_INTERNAL_H__

#define IOEXP_DEBUG 0
#define IOEXPPRINT(...)  DEBUG((DEBUG_INFO, ## __VA_ARGS__));

#define PREPARE_I2C_FLAG                    1
#define RESTORE_I2C_FLAG                    2

#define FLAG_24C32_READ_PHASE1              1


#define E3DONGLE_IOEXPANDER_I2C_SLAVE_ADDR    0x38  //PCA9534A
#define PCA9534A_INPUTPORT_REGISTER           0x00
#define PCA9534A_OUTPUTPORT_REGISTER          0x01
#define PCA9534A_POLARITY_INVERSION_REGISTER  0x02
#define PCA9534A_CONFIGURATION_REGISTER       0x03


#define EEPROM_24C32_I2C_SLAVE_ADDR        0x50
#define E3_DONGLE_I2C_SLAVE_ADDRESS        EEPROM_24C32_I2C_SLAVE_ADDR
#define EEPROM_24C32_CACHE_SIZE            8
#define EEPROM_24C32_MAX_CACHE_SIZE        64


EFI_STATUS
PrepareOrRestoreI2C (
  IN UINTN    bus,
  IN UINTN    flag
  );

EFI_STATUS
PeiPrepareOrRestoreI2C (
  IN UINTN    bus,
  IN UINTN    flag
  );

EFI_STATUS
ECSpaceRead (
  IN UINT8 Offset,
  OUT UINT8 *Value
  );

EFI_STATUS
ECSpaceWrite (
  IN UINT8 Offset,
  IN UINT8 Value
  );

EFI_STATUS
ECSpaceAndOr (
  IN UINT8 Offset,  
  IN UINT8 AndValue,
  IN UINT8 OrValue
  );

EFI_STATUS
SendEcPortCommandAndDataTimeout (
  IN UINT8                  CmdPort,
  IN UINT8                  Command,
  IN UINT8                  DataPort,
  IN UINT8                  Data,
  IN UINT8                  StatusPort,
  IN UINT32                 Timeout
  );



EFI_STATUS
EFIAPI
I2cWriteRead_EEPROM24CXX (
  IN UINTN  MmioBase,
  IN UINT8  SlaveAddress,
  IN UINTN  WriteLength,
  IN UINT8  *WriteBuffer,
  IN UINTN  ReadLength,
  IN UINT8  *ReadBuffer,
  IN UINT64  TimeBudget,
  IN UINT8  Flag
  //TODO: add Speed parameter
  );

#endif
