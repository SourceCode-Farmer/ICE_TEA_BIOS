#ifndef __E3_DONGLE_LIB_H_
#define __E3_DONGLE_LIB_H_

#define IRPM_BIOSETUP_DISABLEMODE       0
#define IRPM_BIOSETUP_DONGLEMODE        1
#define IRPM_BIOSETUP_ONLINEPCHMODE     2
#define IRPM_BIOSETUP_ONLINEISHMODE     3

EFI_STATUS
EFIAPI
ExternalPowerControlForE3Chip (
    IN BOOLEAN   bEnabled
    );

EFI_STATUS
EFIAPI
PeiExternalPowerControlForE3Chip (
    IN BOOLEAN   bEnabled
    );

BOOLEAN 
IsE3DongleAttached(VOID);

BOOLEAN 
PeiIsE3DongleAttached(VOID);

EFI_STATUS
WriteE3DongleByte (
  IN UINTN    offset,
  IN UINTN    value
  );

EFI_STATUS
WriteE3DongleBuffer(
  IN UINTN    offset,
  IN UINTN    length,
  IN UINT8    *buf  
  );

EFI_STATUS
ReadE3DongleByte (
  IN UINTN    offset,
  OUT UINTN*  value
  );

EFI_STATUS
PeiReadE3DongleByte (
  IN UINTN    offset,
  OUT UINTN*  value
  );

EFI_STATUS
ReadE3DongleBuffer (
  IN UINTN    offset,
  IN UINTN    length,
  OUT UINT8   *buf
  );

EFI_STATUS
EFIAPI  
WriteE3DongleIOExp (
  IN UINT8    offset,
  IN UINT8    value
  );

EFI_STATUS
EFIAPI  
ReadE3DongleIOExp (
  IN UINT8    offset,
  OUT UINT8*  value
  );

EFI_STATUS
EFIAPI
PeiWriteE3DongleIOExp (
  IN UINT8    offset,
  IN UINT8    value
  );

EFI_STATUS
EFIAPI  
PeiReadE3DongleIOExp (
  IN UINT8    offset,
  OUT UINT8*  value
  );

//bOn - true: set E3 chip power down pin to high. when E3 chip is running, it should be set to high.
//          false:set E3 chip power down pin to low. when E3 chip is disabled, it should be set to low.

EFI_STATUS
iRPMChipPowerDownPin(
  IN BOOLEAN bOn
  );

//bOn - true: turn on vbus for E3 chips, the E3 chips power is supplied by board in this case.
//          false: turn off vbus for E3 chips, in this case, the E3 chips power is disabled or supplied by dongle
EFI_STATUS
iRPMVbusControl(
  IN BOOLEAN bOn
  );

// Value: 0 - low; non-zero - high.
EFI_STATUS
iRPMGetE3ChipPowerState(
  OUT UINT8* Value
  );

//bOn - true: turn on power enable pin for E3 chips.
//          false: turn off power enable pin for E3 chips.
EFI_STATUS
iRPME3ChipPowerControl(
  IN BOOLEAN bOn
  );

//bFast - true: turn on E3 chip fast mode.
//          false: set E3 chip to run in slow mode.
EFI_STATUS
iRPME3ChipFastSlowControl(
  IN BOOLEAN bFast
  );

//bI2C - true: switch USB port to I2C mode
//          false: switch USB port to USB mode
EFI_STATUS
iRPMMuxSwitch(
  IN BOOLEAN bI2C
  );

EFI_STATUS
iRPMPCHGpioInitForDongleMode(
  VOID
  );

EFI_STATUS
iRPMPCHGpioInitForISHMode(
  VOID
  );

EFI_STATUS
iRPMPCHGpioInitForPCHMode(
  VOID
  );

EFI_STATUS
iRPMPCHGpioInitForDisablementMode(
  VOID
  );

#endif //__E3_DONGLE_LIB_H_