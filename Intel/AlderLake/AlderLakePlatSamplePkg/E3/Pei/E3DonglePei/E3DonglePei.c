/** @file
  This file initialises and installs the PEI E3 Driver

@copyright
 Copyright (c) 2019 - 2021 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains a 'Sample Driver' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may be modified by the user, subject to
 the additional terms of the license agreement.

@par Specification Reference:
**/
#include <PiPei.h>
#include <Ppi/EndOfPeiPhase.h>
#include <Ppi/ReadOnlyVariable2.h> 
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/TimerLib.h>
#include <Library/PeiServicesLib.h>
#include <SetupVariable.h>

#include "..\..\Include\E3DongleLib.h"
#include "E3DonglePei.h"


static EFI_PEI_NOTIFY_DESCRIPTOR  mNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiI2cMasterPpiGuid, //&gEfiPeiMemoryDiscoveredPpiGuid, //&gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PeiHookE3DongleInit
};

/**
  @brief
  Entry point for Pei E3 Dongle Driver.

  @param[in] FileHandle           The file handle of the file, Not used.
  @param[in] PeiServices          General purpose services available to every PEIM.

  @retval EFI_SUCCESS             The function completes successfully
**/
EFI_STATUS
EFIAPI
E3DonglePeiDriverEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
)
{
  EFI_STATUS                        Status;

  Status = PeiServicesNotifyPpi (&mNotifyList);
  ASSERT_EFI_ERROR(Status);

  return EFI_SUCCESS;
}

/**

  Routine Description:
    Callback function to initialize the platforms for E3 chips.

  Arguments:
    @param[in] PeiServices          General purpose services available to every PEIM.
    @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
    @param[in] Ppi                  The memory discovered PPI.  Not used.

  Returns:
    @retval EFI_SUCCESS             Succeeds.
**/
EFI_STATUS
EFIAPI
PeiHookE3DongleInit (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
)
{
  EFI_STATUS                        Status;

  DEBUG ((DEBUG_INFO, "PeiHookE3DongleInit Callback Entry.\n"));
  
  Status = iRPMGpioInitPei();
  
  DEBUG ((DEBUG_INFO, "PeiHookE3DongleInit Callback Exit.\n"));

  return (Status);
}


/**
  This function configures the GPIOs for iRPM .

  @param[in] 

  @retval 
**/
EFI_STATUS
iRPMGpioInitPei(
  VOID
  )
{
  EFI_STATUS                      Status;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  SETUP_DATA                      SystemConfiguration;
//  UINT8                           Value;
  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);
  
  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SystemConfiguration
                               );
  if (EFI_ERROR (Status)) {
    return Status;
  }  
  
/*
  // Turn off VBUS by default.
  iRPMVbusControl(FALSE);

  // Turn on VBUS for online mode/disable mode and off for dongle mode
  if (SystemConfiguration.iRPMMode != IRPM_BIOSETUP_DONGLEMODE) {
    Value = 0;
    Status = iRPMGetE3ChipPowerState(&Value);
    if ((!EFI_ERROR(Status) && (Value != 0)) || EFI_ERROR(Status)) {
      // Before turn on the VBus,  turn off the power from dongle
      // if E3 chip power is supplied by dongle or cannot detect the E3 chip power.
      iRPMPCHGpioInitForDongleMode();
      iRPMMuxSwitch(TRUE);
      PeiExternalPowerControlForE3Chip(FALSE);
    }
    
    iRPMVbusControl(TRUE);
  }
*/

  switch (SystemConfiguration.iRPMMode) {
    case IRPM_BIOSETUP_DONGLEMODE:
      //
      // iRPM/E3 dongle Measurement mode is enabled, do the following:
      // 
      
      // Turn off VBUS.
      iRPMVbusControl(FALSE);
      
      //Turn on E3 chip power
      iRPME3ChipPowerControl(TRUE);
      
      // Configure PCH GPIOs for dongle mode
      iRPMPCHGpioInitForDongleMode();

      // Configure E3 Chip PowerDown pin to high
      iRPMChipPowerDownPin(TRUE);

      // Set Mux to I2C
      iRPMMuxSwitch(TRUE);

      //Configure the E3 chip working mode
      // Set to fast mode
      iRPME3ChipFastSlowControl(TRUE);
     
      // Set the IOExpander on the dongle to turn on the power from dongle to E3 chip
      PeiExternalPowerControlForE3Chip(TRUE);
      break;
    case IRPM_BIOSETUP_ONLINEPCHMODE:
      //
      // Online PCH mode
      //

      // Turn on VBUS.
      iRPMVbusControl(TRUE);
      
      // Turn on E3 chip power
      iRPME3ChipPowerControl(TRUE);
      
      // Configure PCH GPIOs for online PCH mode
      iRPMPCHGpioInitForPCHMode();

      // Configure E3 Chip PowerDown pin to high
      iRPMChipPowerDownPin(TRUE);

      // Set Mux to USB
      iRPMMuxSwitch(FALSE);

      //Configure the E3 chip working mode
      // Set to fast mode
      iRPME3ChipFastSlowControl(TRUE);
      break;
    case IRPM_BIOSETUP_ONLINEISHMODE:
      //
      // Online ISH mode
      //

      // Turn on VBUS.
      iRPMVbusControl(TRUE);
      
      // Turn on E3 chip power
      iRPME3ChipPowerControl(TRUE);
      
      // Configure PCH GPIOs for online PCH mode
      iRPMPCHGpioInitForISHMode();

      // Configure E3 Chip PowerDown pin to high
      iRPMChipPowerDownPin(TRUE);

      // Set Mux to USB
      iRPMMuxSwitch(FALSE);

      //Configure the E3 chip working mode
      // Set to fast mode
      iRPME3ChipFastSlowControl(TRUE);
      break;
    default:
      //
      // Disable iRPM function
      //

      // Turn on VBUS.
      iRPMVbusControl(TRUE);
      
      // Turn off  E3 chip power
      iRPME3ChipPowerControl(FALSE);
      
      // Configure PCH GPIOs for online PCH mode
//[-start-211208-TAMT000035-A-remove]//
//      iRPMPCHGpioInitForDisablementMode(); 
//[-end-211208-TAMT000035-A-remove]//
      // Configure E3 Chip PowerDown pin to low
      iRPMChipPowerDownPin(FALSE);

      // Set Mux to USB
      iRPMMuxSwitch(FALSE);

      //Configure the E3 chip working mode to slow
      iRPME3ChipFastSlowControl(FALSE);    
  } 
  
  return EFI_SUCCESS;
}
