/** @file
  Header file for PEI E3 Dongle driver

@copyright
 Copyright (c) 2015 - 2019 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains a 'Sample Driver' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may be modified by the user, subject to
 the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _E3_DONGLE_PEI_H_
#define _E3_DONGLE_PEI_H_

/**
  @brief
  Entry point for E3 Dongle Pei Driver.

  @param[in] FileHandle           The file handle of the file, Not used.
  @param[in] PeiServices          General purpose services available to every PEIM.

  @retval EFI_SUCCESS             The function completes successfully
**/
EFI_STATUS
EFIAPI
E3DonglePeiDriverEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  );

/*++

Routine Description:

Callback function to configure E3 at the end of Pei.


Arguments:
@param[in] PeiServices          General purpose services available to every PEIM.
@param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
@param[in] Ppi                  The memory discovered PPI.  Not used.

Returns:
@retval EFI_SUCCESS             Succeeds.
--*/
EFI_STATUS
EFIAPI
PeiHookE3DongleInit (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
);

EFI_STATUS
iRPMGpioInitPei(
  VOID
  );

#endif //_E3_DONGLE_PEI_H_