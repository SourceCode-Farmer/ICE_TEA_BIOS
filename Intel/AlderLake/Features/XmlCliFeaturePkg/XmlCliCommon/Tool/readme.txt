Step 1: Test your Python script - lets say the python script which you need to convert is hello.py

>>>python hello.py

Step 2: Create & edit Setup.py - Once your python script is successfully tested, create a setup.py file in same folder as hello.py and should have following lines

# ------------ Setup.py starts here -----------------
from distutils.core import setup
import py2exe

setup(console=['hello.py'])
# ------------ Setup.py ends here -----------------

Step 3: install Setup.py - "install" is a Distutils command that installs something (typically a Python module or package). The details Distutils needs to do that installation are contained in setup.py (and sometimes other associated files).

>>python setup.py install

3. Run your setup script - The next step is to run your setup script. Make sure to give the py2exe command and expect to see lots and lots of output:

>>>python setup.py py2exe

4. your desired exe file along with other required files will be generated under dist folder
