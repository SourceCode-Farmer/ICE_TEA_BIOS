/** @file
  Xml Cli Pei driver implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/CmosAccessLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PeimEntryPoint.h>
#include <Library/PeiServicesLib.h>
#include <Library/HobLib.h>
#include <Library/PerformanceLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/PrintLib.h>
#include <XmlCliSetup.h>

#define CMOS_DRAM_SHARED_MB_ADDR_REG  (0xF0)

/**
  Main entry for XmlCli PEIM.
  This routine is to restore Dram Shared mailbox address during S3 RESUME.

  @param[in] FileHandle   Handle of the file being invoked.
  @param[in] PeiServices  General purpose services available to every PEIM.

**/
EFI_STATUS
EFIAPI
PeimXmlCliEntryPoint (
  IN EFI_PEI_FILE_HANDLE       FileHandle,
  IN CONST EFI_PEI_SERVICES    **PeiServices
  )
{
  UINT32                           DramMbAddress=0;
  UINTN                            DataSize;
  EFI_BOOT_MODE                    BootMode;
  EFI_STATUS                       Status;
  XMLCLI_SETUP                     XmlCliSetup;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *Variable;

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  if (BootMode != BOOT_ON_S3_RESUME) {
    return EFI_SUCCESS;
  }

  Status = PeiServicesLocatePpi (&gEfiPeiReadOnlyVariable2PpiGuid, 0, NULL, (VOID **)&Variable);
  ASSERT_EFI_ERROR (Status);

  DataSize = sizeof(XMLCLI_SETUP);
  Status = Variable->GetVariable (Variable, XMLCLI_SETUP_NAME, &gXmlCliSetupGuid, NULL, &DataSize, &XmlCliSetup);
  if (!EFI_ERROR (Status)) {
    //
    // Restoring Dram Shared mailBox address
    //
    DramMbAddress = XmlCliSetup.XmlCliDramCmosAddr;
    // Restoring the Mailbox address in Cmos so that the tool can read it, this address is reserved area and may change, hence saving in cmos for the tool to read
    CmosWrite8((UINT8)CMOS_DRAM_SHARED_MB_ADDR_REG, (UINT8)(DramMbAddress >> 16));  // save lower byte of higher UINT16
    CmosWrite8((UINT8)CMOS_DRAM_SHARED_MB_ADDR_REG + 1, (UINT8)(DramMbAddress >> 24));  // save upper byte of higher UINT16
  }
  return Status;
}
