## @file
# This is a build description for the XmlCli advanced feature.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
#@par Specification Reference:
##
################################################################################
#
# Library Class section - list of all Library Classes needed by this feature.
#
################################################################################
[LibraryClasses.X64]
  XmlCliCommonLib|XmlCliFeaturePkg/LibraryPrivate/XmlCliCommonLib/XmlCliCommonLib.inf

################################################################################
#
# Component section - list of all components that need built for this feature.
#
# Note: The EDK II DSC file is not used to specify how compiled binary images get placed
#       into firmware volume images. This section is just a list of modules to compile from
#       source into UEFI-compliant binaries.
#       It is the FDF file that contains information on combining binary files into firmware
#       volume images, whose concept is beyond UEFI and is described in PI specification.
#       There may also be modules listed in this section that are not required in the FDF file,
#       When a module listed here is excluded from FDF file, then UEFI-compliant binary will be
#       generated for it, but the binary will not be put into any firmware volume.
#
################################################################################
[Components.IA32]
  #####################################
  # XmlCli Feature Package
  #####################################
  XmlCliFeaturePkg/XmlCliCommon/Pei/XmlCliCommonPei.inf

[Components.X64]
  #####################################
  # XmlCli Feature Package
  #####################################
  XmlCliFeaturePkg/XmlCliCommon/Dxe/XmlCliCommonDxe.inf {
    <LibraryClasses>
      RngLib|MdePkg/Library/BaseRngLib/BaseRngLib.inf
  }
  XmlCliFeaturePkg/XmlCliCommon/Smm/XmlCliCommonSmm.inf


[PcdsFeatureFlag]
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliFeatureEnable|TRUE

[PcdsFixedAtBuild]
  # XmlCli Feature Package Token Space defining Default Setup Values


  # PCD values for setting default values for setup knobs
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliSupport|0x0
  gXmlCliFeaturePkgTokenSpaceGuid.PcdEnableXmlCliLite|0x0

  gXmlCliFeaturePkgTokenSpaceGuid.PcdLiteXmlReserved|0xC0000

!if gXmlCliFeaturePkgTokenSpaceGuid.PcdPlatformXmlCli == 0x2
  gXmlCliFeaturePkgTokenSpaceGuid.PcdPublishSetupPgPtr|0x0
  # PCD values for reserving memory size
  gXmlCliFeaturePkgTokenSpaceGuid.PcdKnobsEntriesSize|0x60000
  gXmlCliFeaturePkgTokenSpaceGuid.PcdKnobValueMapSize|0x60000
  gXmlCliFeaturePkgTokenSpaceGuid.PcdKnobPatchDataBufferSize|0x60000

  !if $(MAX_SOCKET) == 8
    # 22 MB reserved, 19 MB XML + 2.5 MB Dram Shared MB
    gXmlCliFeaturePkgTokenSpaceGuid.PcdGbtXmlReserved|0x1600000
  !elseif $(MAX_SOCKET) == 4
    # 18 MB reserved, 15 MB XML + 2.5 MB Dram Shared MB
    gXmlCliFeaturePkgTokenSpaceGuid.PcdGbtXmlReserved|0x1200000
  !else
    ## 16 MB reserved, 13 MB XML + 2.5 MB Dram Shared MB
    gXmlCliFeaturePkgTokenSpaceGuid.PcdGbtXmlReserved|0x1000000
  !endif

  # PCD values for setting default values for setup knobs
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliSupport|0x0
!else
  gXmlCliFeaturePkgTokenSpaceGuid.PcdPublishSetupPgPtr|0x1

  gXmlCliFeaturePkgTokenSpaceGuid.PcdGbtXmlReserved|0x800000
  gXmlCliFeaturePkgTokenSpaceGuid.PcdKnobsEntriesSize|0x40000
  gXmlCliFeaturePkgTokenSpaceGuid.PcdKnobValueMapSize|0x40000
  gXmlCliFeaturePkgTokenSpaceGuid.PcdKnobPatchDataBufferSize|0x40000
!endif
