/** @file
  This is the driver that set/get Mebx Config data.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/HobLib.h>
#include <Protocol/MebxConfigProtocol.h>
#include <MebxData.h>
#include <MeBiosPayloadHob.h>

#define MEBX_DATA_VARIABLE_NAME L"MebxData"

EFI_STATUS
EFIAPI
SetMebxConfig (
  IN  UINTN                        DataSize,
  IN  VOID                         *Data
  );

EFI_STATUS
EFIAPI
GetMebxConfig (
  IN OUT  UINTN                    *DataSize,
     OUT  VOID                     *Data
  );

MEBX_CONFIG_PROTOCOL               mMebxConfig = {
  MEBX_CONFIG_PROTOCOL_REVISION,
  SetMebxConfig,
  GetMebxConfig
};

/**
  This function is used to set MEBx settings.

  @param[in]  DataSize           The size in bytes of the MEBx Data buffer.
  @param[in]  Data               The contents for the MEBx Data.

  @retval EFI_SUCCESS            MEBx config saved.
  @retval EFI_OUT_OF_RESOURCES   Not enough storage is available to hold MEBx data.
  @retval EFI_DEVICE_ERROR       MEBx config could not be retrieved due to a hardware error.
**/
EFI_STATUS
EFIAPI
SetMebxConfig (
  IN  UINTN                        DataSize,
  IN  VOID                         *Data
  )
{
  EFI_STATUS              Status;
  MEBX_DATA               MebxData;
  UINTN                   VariableSize;
  EFI_HANDLE              Handle;

  DEBUG ((DEBUG_INFO, "%a enter\n", __FUNCTION__));

  VariableSize = 0;
  Status = gRT->GetVariable (
                  MEBX_DATA_VARIABLE_NAME,
                  &gMebxDataGuid,
                  NULL,
                  &VariableSize,
                  &MebxData
                  );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    DEBUG ((DEBUG_INFO, "VariableSize is 0x%x\n", VariableSize));
    Status = gRT->GetVariable (
                    MEBX_DATA_VARIABLE_NAME,
                    &gMebxDataGuid,
                    NULL,
                    &VariableSize,
                    &MebxData
                    );
  }

  if ((!EFI_ERROR (Status) && ((VariableSize != DataSize) || (CompareMem (&Data, &MebxData, VariableSize) != 0))) ||
      (Status == EFI_NOT_FOUND)) {
    DEBUG ((DEBUG_INFO, "Update MebxData Variable.\n"));
    Status = gRT->SetVariable (
                    MEBX_DATA_VARIABLE_NAME,
                    &gMebxDataGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                    DataSize,
                    Data
                    );

    Handle = NULL;
    Status = gBS->InstallProtocolInterface (
                    &Handle,
                    &gUpdateSmbiosHwAssetTableGuid,
                    EFI_NATIVE_INTERFACE,
                    NULL
                    );
    ASSERT_EFI_ERROR (Status);

  }

  DEBUG ((DEBUG_INFO, "%a - %r.\n", __FUNCTION__, Status));

  return Status;
}

/**
  This function is used to retrieve MEBx settings.

  @param[in,out] DataSize        The size in bytes of the MEBx data buffer.
  @param[out]    Data            The pointer to MEBx configuration data buffer.

  @retval EFI_SUCCESS            MEBx config was successfully retrieved.
  @retval EFI_INVALID_PARAMETER  Parameter Data or Datasize is NULL.
  @retval EFI_DEVICE_ERROR       MEBx config could not be retrieved due to a hardware error.
**/
EFI_STATUS
EFIAPI
GetMebxConfig (
  IN OUT  UINTN                    *DataSize,
     OUT  VOID                     *Data
  )
{
  EFI_STATUS    Status;
  UINTN         VariableSize;

  DEBUG ((DEBUG_INFO, "%a enter\n", __FUNCTION__));

  if (Data == NULL || DataSize == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  VariableSize = *DataSize;

  Status = gRT->GetVariable (
                  MEBX_DATA_VARIABLE_NAME,
                  &gMebxDataGuid,
                  NULL,
                  &VariableSize,
                  Data
                  );

  if (Status == EFI_BUFFER_TOO_SMALL) {
    *DataSize = VariableSize;
  }

  DEBUG ((DEBUG_INFO, "%a - %r.\n", __FUNCTION__, Status));

  return Status;
}

/**
  This is the standard EFI driver entrypoint to install protocol to set/get MEBX data from silicon package.

  @param[in] ImageHandle         Handle for the image of this driver
  @param[in] SystemTable         Pointer to the EFI System Table

  @retval EFI_SUCCESS            The protocol interface was installed.
  @retval EFI_OUT_OF_RESOURCES   Space for a new handle could not be allocated.
  @retval EFI_INVALID_PARAMETER  Handle or Protocol is NULL.
**/
EFI_STATUS
EFIAPI
MebxConfigEntry (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS            Status;
  ME_BIOS_PAYLOAD_HOB   *MbpHob;

  MbpHob = NULL;
  MbpHob = GetFirstGuidHob (&gMeBiosPayloadHobGuid);
  if (MbpHob != NULL) {
    if (MbpHob->MeBiosPayload.FwPlatType.RuleData.Fields.IntelMeFwImageType != IntelMeCorporateFw ||
        MbpHob->MeBiosPayload.FwCapsSku.FwCapabilities.Fields.Amt != 1) {
      return EFI_UNSUPPORTED;
    }
  }

  Status = gBS->InstallMultipleProtocolInterfaces (
                  &ImageHandle,
                  &gMebxConfigProtocolGuid,
                  &mMebxConfig,
                  NULL
                  );

  ASSERT_EFI_ERROR (Status);
  return Status;
}
