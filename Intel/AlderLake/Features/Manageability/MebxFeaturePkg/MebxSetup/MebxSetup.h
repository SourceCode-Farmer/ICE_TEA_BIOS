/** @file
  This file contains definitions for MEBx initialization, HECI Initialization,
  BPF Obtaining, DriverCallback.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

@par Specification Reference:
**/

#ifndef _MEBX_SETUP_H
#define _MEBX_SETUP_H

//[-start-211229-IB17040222-modify]//
//
// This GUID "MEBX_FORMSET_GUID" copy to "MebxFeaturePkg.dec" for Insyde setup feature use.
// If this GUID have any change, please update it to "MebxFeaturePkg.dec".
//
#define MEBX_FORMSET_GUID { 0xed6d18b3, 0x16bd, 0x40d8, { 0x89, 0x50, 0xf0, 0xc5, 0x92, 0xf6, 0xfa, 0x16 } }
//[-end-211229-IB17040222-modify]//

#define INPUT_OK                   0
#define HDN_INVALID_CHARACTER      1
#define HDN_BAD_DOT                2
#define HDN_LABEL_TOO_LONG         3
#define HDN_NAME_TOO_LONG          4
#define HDN_BAD_HYPHEN             5
#define BAD_IP_ADDR                6
#define BAD_IP_MASK                7
#define BAD_IP_ADDR_MASK_COMBO     8
#define BAD_NEW_PWD                9
#define HDN_DOMAIN_TOO_LONG        10

#define SHOW_LOGIN                 0
#define SHOW_EVERYTHING            1
#define SHOW_PWD_EXCEEDED          2
#define SHOW_TOO_EARLY             3
#define SHOW_AMT_DISABLED          4
#define SHOW_AFTER_EOP             5
#define SHOW_REDIR_ACTIVE          6
#define SHOW_UNSUPPORTED_INTERFACE 7

#define FORM_ID_MEBX               9001
#define FORM_ID_POWER_CTRL         9003
#define FORM_ID_AMT                9004
#define FORM_ID_SOLSTORAGEREDIRKVM 9005
#define FORM_ID_USERCONSENT        9006
#define FORM_ID_NETWORK_SETUP      9007
#define FORM_ID_ME_NETWORK_NAME    9008
#define FORM_ID_TCPIP              9009
#define FORM_ID_WIRED_IPV4         9010
#define FORM_ID_REMOTE_SETUP       9011
#define FORM_ID_REMOTE_CONFIG      9012
#define FORM_ID_TLS_PKI            9013
#define FORM_ID_MANAGE_HASHES      9014
#define FORM_ID_OEM_DEBUG          9015
#define FORM_ID_CERT_00            9100
#define FORM_ID_CERT_01            9101
#define FORM_ID_CERT_02            9102
#define FORM_ID_CERT_03            9103
#define FORM_ID_CERT_04            9104
#define FORM_ID_CERT_05            9105
#define FORM_ID_CERT_06            9106
#define FORM_ID_CERT_07            9107
#define FORM_ID_CERT_08            9108
#define FORM_ID_CERT_09            9109
#define FORM_ID_CERT_10            9110
#define FORM_ID_CERT_11            9111
#define FORM_ID_CERT_12            9112
#define FORM_ID_CERT_13            9113
#define FORM_ID_CERT_14            9114
#define FORM_ID_CERT_15            9115
#define FORM_ID_CERT_16            9116
#define FORM_ID_CERT_17            9117
#define FORM_ID_CERT_18            9118
#define FORM_ID_CERT_19            9119
#define FORM_ID_CERT_20            9120
#define FORM_ID_CERT_21            9121
#define FORM_ID_CERT_22            9122
#define FORM_ID_CERT_23            9123
#define FORM_ID_CERT_24            9124
#define FORM_ID_CERT_25            9125
#define FORM_ID_CERT_26            9126
#define FORM_ID_CERT_27            9127
#define FORM_ID_CERT_28            9128
#define FORM_ID_CERT_29            9129
#define FORM_ID_CERT_30            9130
#define FORM_ID_CERT_31            9131
#define FORM_ID_CERT_32            9132
#define FORM_ID_CERT_33            9133

#define KEY_MEPWD                  0x900
#define KEY_CHANGEMEPWD            0x1101
#define KEY_HOSTDOMAINNAME         0x7654
#define KEY_FQDNTYPE               0x4302
#define KEY_IPV4ADDR               0x5101
#define KEY_IPV4MASKADDR           0x5102
#define KEY_IPV4GATEWAYADDR        0x5103
#define KEY_IPV4PREFDNSADDR        0x5104
#define KEY_IPV4ALTDNSADDR         0x5105
#define KEY_PROVSERVERIPV6ADDR     0x6902
#define KEY_PROVSERVERFQDNADDR     0x6903
#define KEY_STARTCONFIG            0x7500
#define KEY_HALTCONFIG             0x7501
#define KEY_TLS_PKI                0x8000
#define KEY_TLSPPKREMOTECONFIG     0x8200
#define KEY_PKIDNSSUFFIX           0x8101

#define KEY_SOLENABLE              0x8102
#define KEY_REMOTEITOPTINCONFIG    0x8103
#define KEY_USEROPTIN              0x8104
#define KEY_UPDATEINTERVAL         0x8105
#define KEY_TTL                    0x8106
#define KEY_IDLETIMEOUT            0x8107
#define KEY_STORAGEREDIRENABLE     0x8108

#endif // _MEBX_SETUP_H
