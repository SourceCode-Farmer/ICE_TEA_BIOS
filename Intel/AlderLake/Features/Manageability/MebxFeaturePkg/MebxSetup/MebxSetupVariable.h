/** @file
  Variable definition for MEBx Setup Configuration.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

@par Specification Reference:
**/

#ifndef _MEBX_SETUP_VARIABLE_H
#define _MEBX_SETUP_VARIABLE_H

#define ME_FW_VERSION_MAJOR_17       17

#define MAX_HASH_ENTRIES             33
#define HASHDATA_CHARACTERS          121

#define MAX_PASSWORD_SIZE            32
#define MAX_PASSWORD_SIZE_TERMINATED 33

#define MAX_IPV4_CHARACTERS          16
#define MAX_HOST_DOMAIN_NAME_LENGTH  256


/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define MEBX_CONFIGURATION_REVISION  1

typedef struct {
  UINT16       FwVersionMajor;
  // Settings
  UINT8        ShowError;
  UINT32       NetAccess;
  UINT8        MeOnHostSlpStates;
  UINT16       IdleTimeout;
  UINT32       PwdPolicy;
  UINT8        AmtState;
  UINT8        SolEnable;
  UINT8        StorageRedirEnable;
  UINT8        KvmEnable;
  UINT32       UserOptIn;
  UINT32       RemoteItOptInConfig;
  CHAR16       HostDomainName[MAX_HOST_DOMAIN_NAME_LENGTH];
  UINT8        FqdnType;
  UINT8        DynamicDnsUpdate;
  UINT32       UpdateInterval;
  UINT32       Ttl;
  UINT8        DhcpMode;
  CHAR16       Ipv4Addr[MAX_IPV4_CHARACTERS];
  CHAR16       Ipv4MaskAddr[MAX_IPV4_CHARACTERS];
  CHAR16       Ipv4GatewayAddr[MAX_IPV4_CHARACTERS];
  CHAR16       Ipv4PrefDnsAddr[MAX_IPV4_CHARACTERS];
  CHAR16       Ipv4AltDnsAddr[MAX_IPV4_CHARACTERS];
  CHAR16       ProvServerAddr[255];
  UINT16       ProvServerPort;
  UINT32       TlsPpkRemoteConfig;
  CHAR16       PkiDnsSuffix[223];
  UINT8        PrivacyLevel;
  UINT8        LanLessPlatform;
  UINT8        NumberOfPowerPkgs;
  UINT8        NoDefaultHashes; // IF one then we show "No Default Hashes" message
  UINT8        HashValid[MAX_HASH_ENTRIES];
  CHAR16       Pwd[MAX_PASSWORD_SIZE_TERMINATED];
  UINT32       MOffOverride;
  UINT32       SouthClinkState;

  // Exposures
  UINT32       GlobalExposure;

  UINT32       AmtStateExposure;
  UINT32       AmtSubmenuExposure;
  UINT32       KvmExposure;
  UINT32       ProvisioningState;
  UINT32       OptInConfigExposure;
  UINT32       RemoteItOptInConfigExposure;
  UINT32       AddrMaskInvalid;

  // Read-only Provisioning record fields
  UINT8        ProvTlsMode;
  UINT32       SecureDns;
  UINT32       HostInitiated;
  UINT32       SelectedHashType;
  UINT16       SelectedHashData[79];       ///< 64 bytes of data + 15 hyphens
  UINT16       CaCertificateSerials[59];   ///< 48 bytes of data + 11 hyphens
  UINT32       AdditionalCaSerialNums;
  UINT32       IsOemDefault;
  UINT32       IsTimeValid;
  CHAR16       ProvRecordServerIp[30];
  EFI_HII_DATE Date;
  EFI_HII_TIME Time;
  CHAR16       ProvRecordServerFqdn[192];

  UINT32       CertQuantity;
  UINT32       CertHandle[MAX_HASH_ENTRIES];
  UINT32       CertActive[MAX_HASH_ENTRIES];
  UINT32       CertDefault[MAX_HASH_ENTRIES];
  UINT8        CertHashType[MAX_HASH_ENTRIES];
  CHAR16       CertHash00[HASHDATA_CHARACTERS];
  CHAR16       CertHash01[HASHDATA_CHARACTERS];
  CHAR16       CertHash02[HASHDATA_CHARACTERS];
  CHAR16       CertHash03[HASHDATA_CHARACTERS];
  CHAR16       CertHash04[HASHDATA_CHARACTERS];
  CHAR16       CertHash05[HASHDATA_CHARACTERS];
  CHAR16       CertHash06[HASHDATA_CHARACTERS];
  CHAR16       CertHash07[HASHDATA_CHARACTERS];
  CHAR16       CertHash08[HASHDATA_CHARACTERS];
  CHAR16       CertHash09[HASHDATA_CHARACTERS];
  CHAR16       CertHash10[HASHDATA_CHARACTERS];
  CHAR16       CertHash11[HASHDATA_CHARACTERS];
  CHAR16       CertHash12[HASHDATA_CHARACTERS];
  CHAR16       CertHash13[HASHDATA_CHARACTERS];
  CHAR16       CertHash14[HASHDATA_CHARACTERS];
  CHAR16       CertHash15[HASHDATA_CHARACTERS];
  CHAR16       CertHash16[HASHDATA_CHARACTERS];
  CHAR16       CertHash17[HASHDATA_CHARACTERS];
  CHAR16       CertHash18[HASHDATA_CHARACTERS];
  CHAR16       CertHash19[HASHDATA_CHARACTERS];
  CHAR16       CertHash20[HASHDATA_CHARACTERS];
  CHAR16       CertHash21[HASHDATA_CHARACTERS];
  CHAR16       CertHash22[HASHDATA_CHARACTERS];
  CHAR16       CertHash23[HASHDATA_CHARACTERS];
  CHAR16       CertHash24[HASHDATA_CHARACTERS];
  CHAR16       CertHash25[HASHDATA_CHARACTERS];
  CHAR16       CertHash26[HASHDATA_CHARACTERS];
  CHAR16       CertHash27[HASHDATA_CHARACTERS];
  CHAR16       CertHash28[HASHDATA_CHARACTERS];
  CHAR16       CertHash29[HASHDATA_CHARACTERS];
  CHAR16       CertHash30[HASHDATA_CHARACTERS];
  CHAR16       CertHash31[HASHDATA_CHARACTERS];
  CHAR16       CertHash32[HASHDATA_CHARACTERS];
} MEBX_CONFIGURATION;

#endif // _MEBX_SETUP_VARIABLE_H
