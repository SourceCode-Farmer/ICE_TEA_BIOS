/**@file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "AhciPciHcPei.h"


/**
  Check whether the controller is a Ahci host controller.

  @param[in] PciHcBase    PCI address of a specified controller.

  @retval TRUE     The specified controller is an NVM Express host controller.
  @retval FALSE    The specified controller is not an NVM Express host controller.

**/
BOOLEAN
IsPciAhciHc (
  IN UINTN    PciHcBase
  )
{
  UINT8    SubClass;
  UINT8    BaseClass;
  UINT8    ProgInt;

  ProgInt   = PciRead8 (PciHcBase + PCI_CLASSCODE_OFFSET);
  SubClass  = PciRead8 (PciHcBase + PCI_CLASSCODE_OFFSET + 1);
  BaseClass = PciRead8 (PciHcBase + PCI_CLASSCODE_OFFSET + 2);

  if ((BaseClass != PCI_CLASS_MASS_STORAGE) ||
      (SubClass != PCI_CLASS_MASS_STORAGE_SATADPA)) {
    return FALSE;
  }

  return TRUE;
}

/**
  Check if the PCI device valid.

  @param[in]  Bus            Bus Number.
  @param[in]  Dev            Device Number.
  @param[in]  Fun            Function Number.

  @retval TRUE               There is a bridge on that bus.
  @retval FALSE              There is a no bridge on that bus.

**/
BOOLEAN
IsValidPci(
  IN UINT8    Bus,
  IN UINT8    Dev,
  IN UINT8    Fun
  )
{
  UINT64 PciAddr;
  PciAddr = PCI_SEGMENT_LIB_ADDRESS ( 0, Bus, Dev, Fun, 0 );
  if (PciRead16 ((UINTN) PciAddr + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    return FALSE;
  }
  return TRUE;
}

/**
  Check if there is a PCI bridge on the Bus.

  @param[in]  Bus            Bus Number.
  @param[in]  Dev            Device Number.
  @param[in]  Fun            Function Number.

  @retval TRUE               There is a PCI bridge on that bus.
  @retval FALSE              There is a no PCI bridge on that bus.

**/
BOOLEAN
IsPciBridge(
  IN UINT8    Bus,
  IN UINT8    Dev,
  IN UINT8    Fun
  )
{
  UINT64 PciAddr;
  PciAddr = PCI_SEGMENT_LIB_ADDRESS ( 0, Bus, Dev, Fun, 0 );

  if ((PciRead8 ((UINTN) PciAddr + PCI_HEADER_TYPE_OFFSET) & HEADER_LAYOUT_CODE) != HEADER_TYPE_PCI_TO_PCI_BRIDGE) {
    return FALSE;
  }
  return TRUE;
}


/**
  Check if the PCI bridge support multi function

  @param[in]  Bus            Bus Number.
  @param[in]  Dev            Device Number.
  @param[in]  Fun            Function Number.

  @retval TRUE               It support multi function.
  @retval FALSE              It dose not support multi function.

**/
BOOLEAN
IsPciMultiFunc (
  IN UINT8    Bus,
  IN UINT8    Dev,
  IN UINT8    Fun
  )
{
  UINT64 PciAddr;
  PciAddr = PCI_SEGMENT_LIB_ADDRESS ( 0, Bus, Dev, Fun, 0 );

  if (PciRead8 ((UINTN) PciAddr + PCI_HEADER_TYPE_OFFSET) & HEADER_TYPE_MULTI_FUNCTION) {
    return TRUE;
  }
  return FALSE;
}
