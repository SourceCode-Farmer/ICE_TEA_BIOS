<!--
@file

 This file provide the details for Platform SATA feature.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
-->
# Overview
* **Feature Name:** Platform SATA feature
* **PI Phase(s) Supported:** PEI
* **SMM Required?** No

## Purpose
This feature package implements SATA related platform features.

# High-Level Theory of Operation
This feature provides SATA related platform features:

1. AhciPciHcPei.inf will call a library function to get the PCI bridge information, and scan PCI devices on PCI bus. Platform can implement its own library.

## Firmware Volumes
* PostMemory

## Modules
* AhciPciHcPei
* AhciPcieRpLibNull

## AhciPciHcPei
The driver scans SATA devices on PCI bus and provides SATA controller information via EDKII_ATA_AHCI_HOST_CONTROLLER_PPI.

## AhciPcieRpLibNull
A NULL implemention of library AhciPcieRpLib, it return NULL for function GetNextRootPortComplex().
Platform maybe implemente its own AhciPcieRpLib.

## Configuration
There is not special configuration.
But the platform maybe implemente its own AhciPcieRpLib.

## Data Flows
Architecturally defined data structures and flows for the feature.

### Data flows of NvmePciHcPei
The driver install EDKII_ATA_AHCI_HOST_CONTROLLER_PPI which is consumed by EDK2 SATA drivers.

## Control Flows
Consumer gets SATA controller configuration via EDKII_ATA_AHCI_HOST_CONTROLLER_PPI.

## Build Flows
No any special build flows is needed.

## Test Point Results
Verified by bios debug log.

## Functional Exit Criteria
Check by bios debug log to see if EDKII_ATA_AHCI_HOST_CONTROLLER_PPI is installed successfully.

## Feature Enabling Checklist
Include SataFeature.dsc

## Common Optimizations