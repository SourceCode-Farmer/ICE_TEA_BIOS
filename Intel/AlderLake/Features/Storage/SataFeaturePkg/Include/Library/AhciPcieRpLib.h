/** @file
  AhciPcieRpLib provide the interface to get the PCI bridge.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef _AHCI_PCIE_RP_LIB_H_
#define _ACHI_PCIE_RP_LIB_H_

#define DEFAULT_BUS_NUM  0xFFFF

//
// Pci root port complex
//
typedef struct {
  UINT8    Bus;
  UINT32   MmioBase;
  UINT32   MmioLimit;
} PCI_ROOT_PORT_COMPLEX;


/**
  Get the next root port complex information.

  @param[in]  Bus            current bus number. This function shall return first root port complex if Bus number is DEFAULT_BUS_NUM.
  @param[out] RpComplex      root port complex information

  @retval EFI_SUCCESS    The function completes successfully.
  @retval others         The function fails.

**/
EFI_STATUS
GetNextRootPortComplex (
  IN  UINT16   Bus,
  OUT PCI_ROOT_PORT_COMPLEX *RpComplex
  );

#endif  // _AHCI_PCIE_RP_LIB_H_
