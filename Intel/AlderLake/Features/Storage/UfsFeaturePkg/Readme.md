# Overview

* Feature Name: Universal Flash Storage(UFS) platform driver
* PI Phase(s) Supported: DXE
* SMM Required? No

## Purpose

This feature enables platform logic required to fully support boot from UFS.

Currently supported use cases:
* Applying controller specific work arounds

# High-Level Theory of Operation

Feature consists of a single driver(UfsPlatform.inf) which is responsible for installing the protocol instance of EDKII\_UFS\_HC\_PLATFORM\_PROTOCOL. The protocol
is called from UFS driver and allows the platform to introduce platform-specific behavior at predefined points in UFS flows.

## Modules

* UfsPlatform.inf

## Configuration

There is no build-time configuration of the feature at the moment.

## Data flows

UfsPlatform driver installs the EDKII\_UFS\_HC\_PLATFORM\_PROTOCOL which is consumed by standard UFS driver.

## Build flows

The UfsPlatform.inf module has to be dispatched before UFS driver starts binding with hardware. It is platform responsibility to make
sure the module is correctly located in the flash memory.