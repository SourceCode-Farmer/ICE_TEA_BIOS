/**@file
  Power Loss Notification Dxe driver provides functionlity to enable or disable PLN feature.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _POWER_LOSS_NOTIFY_H_
#define _POWER_LOSS_NOTIFY_H_

#include <Protocol/NvmExpressPassthru.h>
#include <Protocol/PciIo.h>
#include <Protocol/DevicePath.h>
#include <Protocol/PowerLossNotifySetting.h>
#include <IndustryStandard/Pci22.h>
#include <IndustryStandard/Nvme.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/PrintLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>

//
// define fields of NVME Get/Set Feature command. Refer to NVMe Specification
//
#define NVME_NAMESPACE_ID                       0      //A value of 0 denotes the NVM Express controller
#define PLN_FEATURE_ID                          0xD8
#define FEATURE_SV_OFFSET                       31
#define FEATURE_SEL_OFFSET                      8
#define FEATURE_SEL_MASK                        0xF00
#define FEATURE_SEL_CURRENT                     0x0

typedef struct {
  UINT32                            Reserved1:17;
  UINT32                            StatusCode:8;
  UINT32                            StatusCodeType:3;
  UINT32                            Reserved2:2;
  UINT32                            More:1;
  UINT32                            DoNotRetry:1;
} SAVE_FEATURE_DW3;

typedef struct {
  UINT32                            PlnEnable:1;
  UINT32                            Reserved:31;
} SAVE_FEATURE_DW0;

typedef struct {
  UINT32                            PlnEnable:1;
  UINT32                            Reserved:31;
} GET_FEATURE_ATTR_CUR;

#endif
