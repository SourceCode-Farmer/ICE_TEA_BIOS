/**@file
  Power Loss Notification Dxe driver provides functionlity to enable or disable PLN feature.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PowerLossNotify.h"

VOID                                *mNvmePassthruRegistration  = NULL;
BOOLEAN                             mSetPlnController = FALSE;
POWER_LOSS_NOTIFY_SETTING_PROTOCOL  *mPlnProtocol = NULL;

/**
  Set feature command.

  @param[in]   NvmeDevice       The pointer to the NVME_PASS_THRU_DEVICE data structure.
  @param[in]   EnablePLN        Indicate if PLN is enabled or not.

  @return EFI_SUCCESS           Successfully send command to device.
  @return EFI_DEVICE_ERROR      Fail to send command to device.

**/
EFI_STATUS
NvmeSetFeature (
  IN  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmeDevice,
  IN  UINT8                                 EnablePln
  )
{
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                  Command;
  EFI_NVM_EXPRESS_COMPLETION               Completion;
  EFI_STATUS                               Status;
  SAVE_FEATURE_DW3                         *ResultDw3;
  SAVE_FEATURE_DW0                         *ResultDw0;
  NVME_ADMIN_SET_FEATURES                  SetFeatureCdw10;

  if (NvmeDevice == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));
  ZeroMem (&SetFeatureCdw10, sizeof (UINT32));

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;

  SetFeatureCdw10.Fid = PLN_FEATURE_ID;
  SetFeatureCdw10.Sv = BIT0;

  Command.Cdw0.Opcode          = NVME_ADMIN_SET_FEATURES_CMD;
  CommandPacket.NvmeCmd->Cdw10 = *((UINT32*) &SetFeatureCdw10);
  CommandPacket.NvmeCmd->Cdw11 = (UINT32)(EnablePln & BIT0);
  CommandPacket.NvmeCmd->Flags = CDW10_VALID | CDW11_VALID;
  CommandPacket.NvmeCmd->Nsid  = NVME_NAMESPACE_ID;
  CommandPacket.CommandTimeout = EFI_TIMER_PERIOD_SECONDS (5);
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;

  Status = NvmeDevice->PassThru (
                         NvmeDevice,
                         NVME_NAMESPACE_ID,
                         &CommandPacket,
                         NULL
                         );

  if (!EFI_ERROR (Status)) {
    ResultDw3 = (SAVE_FEATURE_DW3*) &Completion.DW3;
    ResultDw0 = (SAVE_FEATURE_DW0*) &Completion.DW0;
    if ((ResultDw3->StatusCode == 0) && (ResultDw3->StatusCodeType == 0) && ((UINT8)ResultDw0->PlnEnable == EnablePln)) {
      return EFI_SUCCESS;
    }
    return EFI_UNSUPPORTED;
  }

  return Status;
}

/**
  Get feature command.

  @param[in]   NvmeDevice       The pointer to the NVME_PASS_THRU_DEVICE data structure.
  @param[in]   Sel              Select field for get feature command.
  @param[in]   Capability       The pointer used to store return data structure .

  @return EFI_SUCCESS           Successfully send command to device.
  @return EFI_DEVICE_ERROR      Fail to send command to device.

**/
EFI_STATUS
NvmeGetFeature (
  IN  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmeDevice,
  IN  UINT8                                 Sel,
  OUT UINT32                                *Result
  )
{
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                  Command;
  EFI_NVM_EXPRESS_COMPLETION               Completion;
  EFI_STATUS                               Status;
  NVME_ADMIN_GET_FEATURES                  GetFeatureCdw10;

  if (Result == NULL || NvmeDevice == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));
  ZeroMem (&GetFeatureCdw10, sizeof (UINT32));

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;

  GetFeatureCdw10.Fid = PLN_FEATURE_ID;
  GetFeatureCdw10.Sel = Sel;

  Command.Cdw0.Opcode          = NVME_ADMIN_GET_FEATURES_CMD;
  CommandPacket.NvmeCmd->Cdw10 = *((UINT32*) &GetFeatureCdw10);
  CommandPacket.NvmeCmd->Flags = CDW10_VALID;
  CommandPacket.NvmeCmd->Nsid  = NVME_NAMESPACE_ID;
  CommandPacket.CommandTimeout = EFI_TIMER_PERIOD_SECONDS (5);
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;

  Status = NvmeDevice->PassThru (
                         NvmeDevice,
                         NVME_NAMESPACE_ID,
                         &CommandPacket,
                         NULL
                         );

  if (!EFI_ERROR (Status)) {
    CopyMem (Result, &Completion.DW0, sizeof (UINT32));
  }

  return Status;
}

/**
  Nvme Pass Thru Protocol notification event handler.

  @param[in] Event    Event whose notification function is being invoked.
  @param[in] Context  Pointer to the notification function's context.

**/
VOID
EFIAPI
NvmePassthruNotificationEvent (
  IN  EFI_EVENT       Event,
  IN  VOID            *Context
  )
{
  EFI_STATUS                         Status;
  UINTN                              BufferSize;
  EFI_HANDLE                         Handle;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL *NvmePassthru;
  GET_FEATURE_ATTR_CUR               CurrentAttr;
  UINT8                              PlnEnabled=0;
  UINTN                              PlnSetting;

  BufferSize = sizeof (EFI_HANDLE);
  Status = gBS->LocateHandle (
                  ByRegisterNotify,
                  NULL,
                  mNvmePassthruRegistration,
                  &BufferSize,
                  &Handle
                  );
  if (EFI_ERROR (Status)) {
    return ;
  }

  //
  // Get NvmePassThruProtocol interface
  //
  Status = gBS->HandleProtocol (
                  Handle,
                  &gEfiNvmExpressPassThruProtocolGuid,
                  (VOID **) &NvmePassthru
                  );
  if (EFI_ERROR (Status)) {
    return;
  }

  //
  // Get PLN Setting
  //
  if(mPlnProtocol != NULL) {
    Status = mPlnProtocol->GetPlnSetting (mPlnProtocol, &PlnSetting);
    if (EFI_ERROR (Status)) {
      return;
    }
  }

  //
  // Test if PLN is supported
  //
  Status = NvmeGetFeature (NvmePassthru, FEATURE_SEL_CURRENT, (UINT32*) &CurrentAttr);
  if (EFI_ERROR (Status)) {
    return;
  }

  PlnEnabled = (UINT8) CurrentAttr.PlnEnable;
  DEBUG ((DEBUG_INFO, "PLN :: %a : PlnEnabled = %d \n", __FUNCTION__, PlnEnabled));

  //
  // Set NVMe feature command to enable/disable PLN
  //
  if (((PlnSetting == PLN_ENABLE) && (PlnEnabled == 0))
    || ((PlnSetting == PLN_DISABLE) && (PlnEnabled == 1))) {
    PlnEnabled = (PlnSetting == PLN_ENABLE)? 1:0;
    Status = NvmeSetFeature (NvmePassthru, PlnEnabled);
    if (EFI_ERROR (Status)) {
      return;
    }
  }

  //
  // set the GPIO and EC
  //
  if(!mSetPlnController) {
    mPlnProtocol->SetPlnController (mPlnProtocol, PlnSetting);
    mSetPlnController = TRUE;
  }

  return;
}

/**
  Driver entry point to register Nvme PassThru Protocol callback.

  @param[in] ImageHandle      The firmware allocated handle for the EFI image.
  @param[in] SystemTable      A pointer to the EFI System Table.

  @retval EFI_SUCCESS         Notify event has been created
**/
EFI_STATUS
EFIAPI
PowerLossNotifyEntryPoint (
  IN EFI_HANDLE                       ImageHandle,
  IN EFI_SYSTEM_TABLE                 *SystemTable
  )
{
  EFI_STATUS  Status;
  UINTN       PlnSetting;

  //
  // Check the PLN Setting protocol installed
  //
  Status = gBS->LocateProtocol (
                  &gPowerLossNotifySettingProtocolGuid,
                  NULL,
                  (VOID **) &mPlnProtocol
                  );
  DEBUG ((DEBUG_INFO, "PLN :: %a : Status = %r\n", __FUNCTION__, Status));
  if (!EFI_ERROR (Status)) {
    //
    // Get the Pln Setting
    //
    mPlnProtocol->GetPlnSetting (mPlnProtocol, &PlnSetting);
    if((PlnSetting == PLN_DISABLE) || (PlnSetting == PLN_ENABLE)) {
      //
      // Register NvmePassthruNotificationEvent() notify function.
      //
      EfiCreateProtocolNotifyEvent (
        &gEfiNvmExpressPassThruProtocolGuid,
        TPL_CALLBACK,
        NvmePassthruNotificationEvent,
        NULL,
        &mNvmePassthruRegistration
        );
    }
    return EFI_SUCCESS;
  }

  return EFI_UNSUPPORTED;
}
