#
# This file contains 'Framework Code' and is licensed as such
# under the terms of your license agreement with Intel or your
# vendor.  This file may not be modified, except as allowed by
# additional terms of your license agreement.
#
## @file
# BluetoothPkg Package
#
# This package provides common modules on Bailey Park.
# Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.<BR>
#
#    This software and associated documentation (if any) is furnished
#    under a license and may only be used or copied in accordance
#    with the terms of the license. Except as permitted by such
#    license, no part of this software or documentation may be
#    reproduced, stored in a retrieval system, or transmitted in any
#    form or by any means without the express written consent of
#    Intel Corporation.
#
##

[Defines]
  PLATFORM_NAME                  = BluetoothPkg
  PLATFORM_GUID                  = 2245967E-656B-4C35-A3B4-83EF4824359C
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  OUTPUT_DIRECTORY               = Build/BluetoothPkg
  SUPPORTED_ARCHITECTURES        = IA32|X64
  BUILD_TARGETS                  = DEBUG|RELEASE|NOOPT
  SKUID_IDENTIFIER               = DEFAULT

[LibraryClasses]
  UefiDriverEntryPoint|MdePkg/Library/UefiDriverEntryPoint/UefiDriverEntryPoint.inf
  BaseLib|MdePkg/Library/BaseLib/BaseLib.inf
  BaseMemoryLib|MdePkg/Library/BaseMemoryLib/BaseMemoryLib.inf
  PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
  UefiBootServicesTableLib|MdePkg/Library/UefiBootServicesTableLib/UefiBootServicesTableLib.inf
  UefiRuntimeServicesTableLib|MdePkg/Library/UefiRuntimeServicesTableLib/UefiRuntimeServicesTableLib.inf
  UefiLib|MdePkg/Library/UefiLib/UefiLib.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf
  HiiLib|MdeModulePkg/Library/UefiHiiLib/UefiHiiLib.inf
  DevicePathLib|MdePkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
  PerformanceLib|MdePkg/Library/BasePerformanceLibNull/BasePerformanceLibNull.inf
  BluetoothLib|BluetoothPkg/Library/BluetoothLib/BluetoothLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  HidLib|BluetoothPkg/Library/HidLib/HidLib.inf
  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
  BluetoothLib|BluetoothPkg/Library/BluetoothLib/BluetoothLib.inf
  UefiUsbLib|MdePkg/Library/UefiUsbLib/UefiUsbLib.inf

###################################################################################################
#
# Components Section - list of the modules and components that will be processed by compilation
#                      tools and the EDK II tools to generate PE32/PE32+/Coff image files.
#
# Note: The EDK II DSC file is not used to specify how compiled binary images get placed
#       into firmware volume images. This section is just a list of modules to compile from
#       source into UEFI-compliant binaries.
#       It is the FDF file that contains information on combining binary files into firmware
#       volume images, whose concept is beyond UEFI and is described in PI specification.
#       Binary modules do not need to be listed in this section, as they should be
#       specified in the FDF file. For example: Shell binary (Shell_Full.efi), FAT binary (Fat.efi),
#       Logo (Logo.bmp), and etc.
#       There may also be modules listed in this section that are not required in the FDF file,
#       When a module listed here is excluded from FDF file, then UEFI-compliant binary will be
#       generated for it, but the binary will not be put into any firmware volume.
#
###################################################################################################

[Components]
  BluetoothPkg/UsbBtHciDxe/UsbBtHciDxe.inf
  BluetoothPkg/BluetoothBusDxe/BluetoothBusDxe.inf
  BluetoothPkg/BluetoothHidDxe/BluetoothHidDxe.inf
  BluetoothPkg/HidKbDxe/HidKbDxe.inf
  BluetoothPkg/HidMouseDxe/HidMouseDxe.inf
  BluetoothPkg/BluetoothConnectionManagerDxe/BluetoothConnectionManagerDxe.inf

[BuildOptions]
  *_*_*_CC_FLAGS = -D DISABLE_NEW_DEPRECATED_INTERFACES
