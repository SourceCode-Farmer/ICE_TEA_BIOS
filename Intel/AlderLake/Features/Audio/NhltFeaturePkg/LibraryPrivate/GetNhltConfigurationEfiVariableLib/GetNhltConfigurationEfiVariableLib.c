/** @file
  Library for getting NHLT configuration from EFI Variable

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <Uefi/UefiMultiPhase.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Uefi/UefiBaseType.h>
#include <Pi/PiFirmwareFile.h>
#include <Library/DxeServicesLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <NhltConfiguration.h>
#include <NhltEndpointsConfigurationVariable.h>

/**
  Functions provide Nhlt configuration based on user input stored in Efi Variable.

  @param[in][out]     *NhltConfiguration      Pointer to Nhlt configuration.

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_NOT_FOUND           NHLT user configuration is corrupted
**/
EFI_STATUS
GetNhltConfiguration (
  IN OUT NHLT_CONFIGURATION  *NhltConfiguration
  )
{
  UINTN                                        VariableSize;
  NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE  NhltConfigurationEfiVariable;
  EFI_STATUS                                   Status;

  VariableSize = sizeof (NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE);
  Status = gRT->GetVariable (
                  NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_NAME,
                  &gNhltEndpointsTableConfigurationVariableGuid,
                  NULL,
                  &VariableSize,
                  &NhltConfigurationEfiVariable
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetNhltConfiguration: Failed to get NHLT configuration variable.\n"));
    DEBUG ((DEBUG_INFO, "%a () End.\n", __FUNCTION__));
    return Status;
  }

  CopyMem (NhltConfiguration->NhltConfigurationEnabled,
    NhltConfigurationEfiVariable.NhltConfigurationEnabled,
    sizeof (NhltConfiguration->NhltConfigurationEnabled)
    );

  return EFI_SUCCESS;
}
