/** @file
  Definition of the configuration variable for Nhlt Endpoints table.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_H_
#define _NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_H_

#define NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_VERSION 1

#define NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_NAME  L"NhltEndpointsTableConfigurationVariable"

#define NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_GUID \
 { 0xA1D89A3A, 0x4A90, 0x429D, { 0x43, 0x65, 0x1F, 0x64, 0xC3, 0xA2, 0x96, 0x14 } }

#define NHLT_ENDPOINTS_TABLE_MAX_CONFIGURATION_NUMBER 16

#pragma pack (1)

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE_REVISION  1

typedef struct {
  UINT8  Version;
  UINT8  NhltConfigurationEnabled[NHLT_ENDPOINTS_TABLE_MAX_CONFIGURATION_NUMBER]; ///< Nhlt config enablement: <b>0: Disable</b>; 1: Enable
} NHLT_ENDPOINTS_TABLE_CONFIGURATION_VARIABLE;
#pragma pack ()

#endif
