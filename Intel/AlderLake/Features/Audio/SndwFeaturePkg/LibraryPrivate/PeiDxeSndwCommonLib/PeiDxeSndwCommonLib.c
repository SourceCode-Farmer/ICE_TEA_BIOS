/** @file
  Sndw Audio Common Library for enumerating audio codecs and providing support for tone generator

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Library/BaseLib.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/BaseMemoryLib.h>

#include <SndwBeep.h>
#include <SndwAccess.h>
#include <SndwMipiCmd.h>
#include <Register/SndwCadenceRegs.h>

#define SNDW_MAX_RETRY_NUMBER         10
#define SNDW_MAX_PERIPHERAL_NUMBER    12
#define SNDW_MAX_LOOP_TIME            4000
#define SNDW_WAIT_PERIOD              4

GLOBAL_REMOVE_IF_UNREFERENCED SNDW_COMMAND mMipiSndwReadPeripheralIdsCmds[] = {
  {
    .TxRead = {
      .OpCode = SndwCmdRead,
      .RegAddr = 0x0050,
    }
  },
  {
    .TxRead = {
      .OpCode = SndwCmdRead,
      .RegAddr = 0x0051,
    }
  },
  {
    .TxRead = {
      .OpCode = SndwCmdRead,
      .RegAddr = 0x0052,
    }
  },
  {
    .TxRead = {
      .OpCode = SndwCmdRead,
      .RegAddr = 0x0053,
    }
  },
  {
    .TxRead = {
      .OpCode = SndwCmdRead,
      .RegAddr = 0x0054,
    }
  },
  {
    .TxRead = {
      .OpCode = SndwCmdRead,
      .RegAddr = 0x0055,
    }
  }
};


/**
  Polling the Status bit.

  @param[in] StatusReg            The register address to read the status
  @param[in] PollingBitMap        The bit mapping for polling
  @param[in] PollingData          The Data for polling

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Polling the bit map time out
**/
EFI_STATUS
SndwStatusPolling (
  IN      UINTN           StatusReg,
  IN      UINT32          PollingBitMap,
  IN      UINT32          PollingData
  )
{
  UINT32  LoopTime;
  UINT32  Reg32;
  Reg32 = 0;

  for (LoopTime = 0; LoopTime < SNDW_MAX_LOOP_TIME; LoopTime++) {
    Reg32 = MmioRead32 (StatusReg);
    if ((Reg32 & PollingBitMap) == PollingData) {
      break;
    } else {
      MicroSecondDelay (SNDW_WAIT_PERIOD);
    }
  }

  if (LoopTime >= SNDW_MAX_LOOP_TIME) {
    return EFI_TIMEOUT;
  }

  return EFI_SUCCESS;
}


/**
  Print response message from Sndw codec

  @param[in] RxCommands           Pointer to Sndw message
**/
STATIC
VOID
SndwPrintRxMsg (
  IN  SNDW_COMMAND*   RxCommands
  )
{
  DEBUG ((DEBUG_INFO, "Sndw Rx Response\n"));
  DEBUG ((DEBUG_INFO, "OpCode: %d\n",      RxCommands->Rx.OpCode));
  DEBUG ((DEBUG_INFO, "Data  : 0x%X\n",    RxCommands->Rx.Data));
  DEBUG ((DEBUG_INFO, "Ack   : %d\n",      RxCommands->Rx.Ack));
  DEBUG ((DEBUG_INFO, "Nak   : %d\n",      RxCommands->Rx.Nak));
}


/**
  Print message for Sndw codec

  @param[in] TxCommands           Pointer to Sndw message
**/
STATIC
VOID
SndwPrintTxMsg (
  IN  SNDW_COMMAND*   TxCommands
  )
{
  DEBUG ((DEBUG_INFO, "Sndw Tx Commands\n"));
  DEBUG ((DEBUG_INFO, "SspTag                : %d\n",     TxCommands->Tx.SspTag));
  DEBUG ((DEBUG_INFO, "OpCode                : %d\n",     TxCommands->Tx.OpCode));
  DEBUG ((DEBUG_INFO, "Peripheral Address    : 0x%X\n",   TxCommands->Tx.DeviceAddress));
  switch (TxCommands->Tx.OpCode) {
    case SndwCmdRead:
      DEBUG ((DEBUG_INFO, "Register Address : 0x%X\n", TxCommands->TxRead.RegAddr));
      break;
    case SndwCmdWrite:
      DEBUG ((DEBUG_INFO, "Register Address : 0x%X\n", TxCommands->TxWrite.RegAddr));
      DEBUG ((DEBUG_INFO, "Register Data    : 0x%X\n", TxCommands->TxWrite.RegData));
      break;
    default:
      DEBUG ((DEBUG_INFO, "End of command unavailable.\n"));
      break;
  }
}


/**
  Function operating on Sndw Fifo for sending message to codecs.

  @param[in]  SndwControllerSpace Sndw controller memory space address
  @param[in]  TxCommands          Pointer to send messages
  @param[in]  NumOfMessages       Size of messages to send
**/
VOID
Send (
  IN  UINTN           SndwControllerSpace,
  IN  SNDW_COMMAND*   TxCommands,
  IN  UINTN           NumOfMessages
  )
{
  UINTN               Index;
  UINT32              FifoStatusFree;
  UINTN               TxIndex;
  UINT32              Val;

  TxIndex = 0;

  FifoStatusFree = (UINT32) ((MmioRead32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_FIFOSTAT) & B_SNDW_MEM_FIFOSTAT_FREE) >> N_SNDW_MEM_FIFOSTAT_FREE);

  for (Index = 0; Index < FifoStatusFree; ++Index) {
    if (TxIndex >= NumOfMessages) {
      break;
    }
    Val = TxCommands[TxIndex++].Command;
    SndwPrintTxMsg ((SNDW_COMMAND *) &Val);
    MmioWrite32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_COMMAND, Val);
  }
}


/**
  Function operating on Sndw Fifo for receiving messages from codecs.

  @param[in]  SndwControllerSpace Sndw controller memory space address
  @param[out] RxCommands          Pointer to pointer to received messages.
                                  Memory pool is allocated in this function,
                                  but the caller is responsible for
                                  freeing the memory with FreePool.
  @param[out] RxSize              Size of received messages
**/
VOID
Receive (
  IN  UINTN           SndwControllerSpace,
  OUT SNDW_COMMAND    **RxCommands,
  OUT UINTN*          RxSize
  )
{
  UINTN               Index;
  UINT32              FifoStatusAvailable;
  UINTN               RxIndex;
  UINT32              Val;

  RxIndex = 0;

  FifoStatusAvailable = (UINT32) ((MmioRead32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_FIFOSTAT) & B_SNDW_MEM_FIFOSTAT_AVAIL) >> N_SNDW_MEM_FIFOSTAT_AVAIL);

  *RxCommands = (SNDW_COMMAND *) AllocateZeroPool ((sizeof (SNDW_COMMAND)) * FifoStatusAvailable);
  if (*RxCommands == NULL) {
    DEBUG ((DEBUG_ERROR, "Memory allocation failed.\n"));
    return;
  }

  for (Index = 0; Index < FifoStatusAvailable; ++Index) {

    Val = (UINT32) MmioRead32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_COMMAND);
    SndwPrintRxMsg ((SNDW_COMMAND *) &Val);
    (*RxCommands)[RxIndex++].Command = Val;
  }

  *RxSize = RxIndex;

  //
  // Clear interrupt flag
  //
  MmioOr32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_INTMASK, B_SNDW_MEM_INTMASK_RXNE);
}


/**
  Function sends one message through the Sndw interface and waits
  for the corresponding ACK message.

  @param[in]  SndwControllerSpace Sndw controller memory space address
  @param[in]  TxCommands          Message to send
  @param[out] RxCommands          Pointer to receiving message

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Sending command has failed
  @retval EFI_DEVICE_ERROR        Response command has failed
**/
EFI_STATUS
SendwAck (
  IN  UINTN           SndwControllerSpace,
  IN  SNDW_COMMAND    TxCommand,
  OUT SNDW_COMMAND*   RxCommand OPTIONAL
  )
{
  EFI_STATUS      Status;
  SNDW_COMMAND    TmpRxCommand;

  Send (SndwControllerSpace, &TxCommand, 1);

  Status = SndwStatusPolling (
             (UINTN) SndwControllerSpace + R_SNDW_MEM_STAT,
             B_SNDW_MEM_STAT_RXNE,
             (UINT32) B_SNDW_MEM_STAT_RXNE_FIFO_EMPTY);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Sending command failed. No response from codec. Status = %r\n", Status));
    return Status;
  }

  TmpRxCommand.Command = (UINT32) MmioRead32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_COMMAND);
  SndwPrintRxMsg ((SNDW_COMMAND *) &TmpRxCommand);

  if (RxCommand != NULL) {
    CopyMem (RxCommand, &TmpRxCommand, sizeof (SNDW_COMMAND));
  }

  return EFI_SUCCESS;
}


/**
  Function asks connected codec about identification information.

  @param[in]  SndwControllerSpace      Sndw controller memory space address
  @param[in]  SndwPeripheralAddress    Peripheral address
  @param[in]  SndwCodecId              Pointer to Sndw codec information structure

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Not able to read ids
  @retval EFI_DEVICE_ERROR        Responses from codec are incorrect
**/
EFI_STATUS
ReadPeripheralId (
  IN UINTN            SndwControllerSpace,
  IN UINTN            SndwPeripheralAddress,
  OUT SNDW_CODEC_ID   *SndwCodecId
  )
{
  EFI_STATUS          Status;
  SNDW_COMMAND*       RxCommands;
  UINTN               FifoStatusAvailable;
  UINTN               RetryNumber;
  UINTN               RxSize;
  UINTN               Index;
  UINT32              LoopTime;
  BOOLEAN             Ack;

  Status      = EFI_SUCCESS;
  RetryNumber = 0;
  RxSize      = 0;
  RxCommands  = NULL;
  Ack         = TRUE;

  for (Index = 0; Index < ARRAY_SIZE (mMipiSndwReadPeripheralIdsCmds); Index++) {
    mMipiSndwReadPeripheralIdsCmds[Index].TxRead.DeviceAddress = (UINT32) SndwPeripheralAddress;
  }

  do {
    if (RetryNumber > SNDW_MAX_RETRY_NUMBER) {
      Status = EFI_DEVICE_ERROR;
      break;
    }

    Send (SndwControllerSpace, (SNDW_COMMAND *) &mMipiSndwReadPeripheralIdsCmds, ARRAY_SIZE (mMipiSndwReadPeripheralIdsCmds));

    for (LoopTime = 0; LoopTime < SNDW_MAX_LOOP_TIME; LoopTime++) {
      FifoStatusAvailable = ((MmioRead32 ((UINTN) SndwControllerSpace + R_SNDW_MEM_FIFOSTAT) & B_SNDW_MEM_FIFOSTAT_AVAIL) >> N_SNDW_MEM_FIFOSTAT_AVAIL);
      if (ARRAY_SIZE (mMipiSndwReadPeripheralIdsCmds) == FifoStatusAvailable) {
        break;
      } else {
        MicroSecondDelay (SNDW_WAIT_PERIOD);
      }
    }

    if (LoopTime >= SNDW_MAX_LOOP_TIME) {
      RetryNumber++;
      continue;
    }

    Receive (SndwControllerSpace, &RxCommands, &RxSize);

    if (RxCommands == NULL) {
      Status = EFI_DEVICE_ERROR;
      continue;
    }

    Ack = TRUE;

    SndwPrintRxMsg ((SNDW_COMMAND *) RxCommands);
    for (Index = 0; Index < 6; Index++) {
      if (RxCommands[Index].Rx.Ack == 0) {
        Ack = FALSE;
        RetryNumber++;
        break;
      }

      SndwCodecId->Data[Index] = (UINT8) RxCommands[Index].Rx.Data;
    }
  } while (Ack == FALSE);

  FreePool (RxCommands);

  return Status;
}


/**
  Function initializes Sndw controller for enumerating connected codecs

  @param[in]  SndwControllerMmioAddress       Sndw controller memory space address

  @retval EFI_SUCCESS                         Sndw controller initialization completed successfully
  @retval EFI_TIMEOUT                         Polling FIFO status timed out
**/
EFI_STATUS
SndwControllerInit (
  IN UINTN          SndwControllerMmioAddress
  )
{
  EFI_STATUS Status;

  DEBUG ((DEBUG_INFO, "%a () - Start.\n", __FUNCTION__));

  //
  // Program MCP_Config.CMDMode = 0, to enable PIO mode for Command TX FIFO, and Response RXFIFO.
  // Internally the FIFOs will be accessed by AHB Port.
  //
  MmioAnd32 ((UINTN) SndwControllerMmioAddress + R_SNDW_MEM_CONFIG, (UINT32) ~B_SNDW_MEM_CONFIG_MODE_AHB);

  //
  // Soundwire controller enumeration: Note** if >1 devices are being connected to the link, enumeration flow is a must.
  // Write 0 to MCP_Config.OperationMode to bring the controller into normal mode
  //
  MmioAnd32 ((UINTN) SndwControllerMmioAddress + R_SNDW_MEM_CONFIG, (UINT32) ~B_SNDW_MEM_CONFIG_OM_NORMAL);

  //
  // Write 0 to MCP_ConfigUpdate to update controller settings
  //
  MmioWrite32 ((UINTN) SndwControllerMmioAddress + R_SNDW_MEM_CONFIGUPDATE, (UINT32) B_SNDW_MEM_CONFIGUPDATE_UPDATE_DONE);

  ///
  /// Waiting for FIFO to be ready to send MCP message
  ///
  Status = SndwStatusPolling (
            (UINTN) (SndwControllerMmioAddress + R_SNDW_MEM_STAT),
            B_SNDW_MEM_STAT_TXE,
            B_SNDW_MEM_STAT_TXE_FIFO_EMPTY);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Sndw controller not ready to send commands to codecs.\n"));
    return Status;
  }

  DEBUG ((DEBUG_INFO, "Sndw controller ready to send commands to codecs.\n"));
  DEBUG ((DEBUG_INFO, "%a () - End.\n", __FUNCTION__));

  return EFI_SUCCESS;
}


/**
  Function checks an access to Sndw controller

  @param[in]   SndwControllerSpaceAddress    Sndw controller memory space address

  @retval TRUE           Sndw Controller is ready
  @retval FALSE          Sndw Controller is not ready
**/
BOOLEAN
SndwIoAccessReady (
  IN UINTN          SndwControllerSpaceAddress
  )
{
  if (MmioRead32 ((UINTN) (SndwControllerSpaceAddress + R_SNDW_MEM_CONFIG)) == 0xFFFFFFFF) {
    return FALSE;
  }
  return TRUE;
}


/**
  Function reads and returns Sndw codecs status

  @param[in]  SndwControllerSpaceAddress    Sndw controller memory space address
  @param[in]  PeripheralsStatus             Pointer to variable where codecs status should be written

  @retval EFI_SUCCESS                       Sndw codecs status read successfully
  @retval EFI_TIMEOUT                       No codecs detected
**/
EFI_STATUS
SndwReadCodecsStatus (
  IN  UINTN         SndwControllerSpaceAddress,
  OUT UINT32        *PeripheralsStatus
)
{
  UINT32                LoopTime;

  for (LoopTime = 0; LoopTime < SNDW_MAX_LOOP_TIME; LoopTime++) {
    *PeripheralsStatus = MmioRead32 ((UINTN) (SndwControllerSpaceAddress + R_SNDW_MEM_PERIPHERALSTAT));
    if (*PeripheralsStatus != 0) {
      break;
    } else {
      MicroSecondDelay (SNDW_WAIT_PERIOD);
    }
  }

  if (LoopTime >= SNDW_MAX_LOOP_TIME) {
    DEBUG ((DEBUG_ERROR, "Sndw controller did not detect any codecs.\n"));
    return EFI_TIMEOUT;
  }
  return EFI_SUCCESS;
}


/**
  Function prints information about given Sndw Codec

  @param[in]  SndwCodecInfo    Pointer to Sndw codec information
**/
VOID
PrintCodecInfo (
  IN SNDW_CODEC_INFO  *SndwCodecInfo
  )
{
  DEBUG ((DEBUG_INFO, "Link Number: %d", SndwCodecInfo->SndwLinkIndex));
  DEBUG ((DEBUG_INFO, "Codec ID:\n"));
  DEBUG ((DEBUG_INFO, "  Version           [0x50]: %02x\n", SndwCodecInfo->CodecId.Encoding.Version));
  DEBUG ((DEBUG_INFO, "  ManufacturerID[0] [0x51]: %02x\n", SndwCodecInfo->CodecId.Encoding.ManufacturerID[0]));
  DEBUG ((DEBUG_INFO, "  ManufacturerID[1] [0x52]: %02x\n", SndwCodecInfo->CodecId.Encoding.ManufacturerID[1]));
  DEBUG ((DEBUG_INFO, "  PartId[0]         [0x53]: %02x\n", SndwCodecInfo->CodecId.Encoding.PartId[0]));
  DEBUG ((DEBUG_INFO, "  PartId[1]         [0x54]: %02x\n", SndwCodecInfo->CodecId.Encoding.PartId[1]));
  DEBUG ((DEBUG_INFO, "  Class             [0x55]: %02x\n", SndwCodecInfo->CodecId.Encoding.Class));
}