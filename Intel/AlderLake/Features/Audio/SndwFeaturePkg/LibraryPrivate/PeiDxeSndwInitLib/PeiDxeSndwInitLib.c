/** @file
  Sndw Audio Initialization Library for controller initialization

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiDxeSndwCommonLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/SndwCavsRegs.h>
#include <Register/SndwCadenceRegs.h>

#define SNDW_MAX_PERIPHERAL_NUMBER    12
#define SNDW_MAX_LOOP_TIME            4000
#define SNDW_WAIT_PERIOD              4


/**
  Function sets Sndw shim registers to perform controller initialization.

  @param[in]  DspBar              Memory map address of Dsp
  @param[in]  SndwLinkIndex       Index of Sndw link whose registers should be set

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Dsp memory space is not enabled
**/
VOID
SetSndwShimRegisters (
  IN UINTN    DspBar,
  IN UINT32   SndwLinkIndex
  )
{
  ///
  /// SW enables Integration Glue to enforce IO AC timing in preparation for Manager IP being put into "Normal" Operation
  /// Write Shim register SNDWxACTMCTL, bit DACTQE=1b
  ///
  MmioOr16 ((UINTN) (DspBar + R_HDA_MEM2_SNDW_SNDWxACTMCTL (SndwLinkIndex)), B_HDA_MEM2_SNDW_SNDWxACTMCTL_DACTQE);

  ///
  /// SW releases Override allowing Manager IP control
  /// Write Shim register SNDWxIOCTL, bit MIF=1b
  ///
  MmioOr16 ((UINTN) (DspBar + R_HDA_MEM2_SNDW_SNDWxIOCTL (SndwLinkIndex)), B_HDA_MEM2_SNDW_SNDWxIOCTL_MIF);
}


/**
  Checks HD Audio Controller reset state.

  @param[in] HdaBar             Memory Space Base Address

  @return BOOLEAN               TRUE if controller is in reset, FALSE otherwise
**/
BOOLEAN
IsHdaControllerReset (
  IN  UINT64      HdaBar
  )
{
  ///
  /// Read RESET# bit - GCTL.CRSTB (offset 08h, BIT0)
  /// TRUE:  controller in reset state (CRSTB = 0b)
  /// FALSE: controller out of reset state (CRSTB = 1b)
  ///
  return (MmioRead32 ((UINTN) (HdaBar + R_HDA_MEM_GCTL)) & B_HDA_MEM_GCTL_CRST) ? FALSE : TRUE;
}


/**
  Function gets DSP out of reset and confirms that DSP is alive

  @param[out]  DspBar             Pointer to DSP BAR

  @retval EFI_SUCCESS             DSP is out of reset
  @retval EFI_UNSUPPORTED         HDA is not enabled
  @retval EFI_DEVICE_ERROR        Polling CPA bit timed out, DSP is not out of reset
**/
EFI_STATUS
GetDspOutOfReset (
  IN  UINT64    HdaBar,
  OUT UINT64    DspBar
  )
{
  EFI_STATUS Status;
  BOOLEAN    IsHdaControllerInResetState;

  IsHdaControllerInResetState = IsHdaControllerReset (HdaBar);
  if (IsHdaControllerInResetState) {
    DEBUG ((DEBUG_INFO, "HDA Controller is in reset state!\n"));
    return EFI_UNSUPPORTED;
  }

  DEBUG ((DEBUG_INFO, "DSP: set SPA, waiting for CPA.\n"));
  ///
  /// Set ADSPCS.SPA = 1 to get DSP subsystem out of reset.
  ///
  MmioOr32 ((UINTN) (DspBar + R_HDA_MEM2_ADSPCS), B_HDA_MEM2_ADSPCS_SPA (HDA_ADSP_CORE1));

  ///
  /// Wait until ADSPCS.CPA = 1 to confirm DSP subsystem is alive.
  ///
  Status = SndwStatusPolling (
    (UINTN) (DspBar + R_HDA_MEM2_ADSPCS),
    (UINT32) B_HDA_MEM2_ADSPCS_CPA (HDA_ADSP_CORE1),
    (UINT32) B_HDA_MEM2_ADSPCS_CPA (HDA_ADSP_CORE1));
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "DSP stalled! Status = %r\n", Status));
    DEBUG ((DEBUG_INFO, "%a () - End. Status = %r\n", __FUNCTION__, Status));
    return EFI_DEVICE_ERROR;
  }

  DEBUG ((DEBUG_INFO, "DSP: CPA is set, DSP out of reset. Status = %r\n", Status));

  ///
  /// Enable the Host interrupt generation for SNDW interface
  /// Set ADSPIC2.SNDW = 1
  ///
  MmioOr32 ((UINTN) (DspBar + R_HDA_MEM2_ADSPIC2), B_HDA_MEM2_ADSPIC2_SNDW);

  return EFI_SUCCESS;
}


/**
  Function resets DSP and confirms that DSP is in reset state

  @param[out]  DspBar             DSP BAR

  @retval EFI_SUCCESS             DSP is in reset
  @retval EFI_DEVICE_ERROR        Polling CPA bit timed out, DSP is still out of reset
**/
EFI_STATUS
ResetDsp (
  OUT UINT64    DspBar
  )
{
  EFI_STATUS Status;

  //
  // Set ADSPCS.SPA = 0 to get DSP subsystem to reset
  //
  MmioAnd32 ((UINTN) (DspBar + R_HDA_MEM2_ADSPCS), (UINT32) (~B_HDA_MEM2_ADSPCS_SPA (HDA_ADSP_CORE1)));

  DEBUG ((DEBUG_INFO, "DSP: SPA cleared, waiting for CPA.\n"));

  //
  // Wait until ADSPCS.CPA = 0 to confirm DSP subsystem is not alive.
  //
  Status = SndwStatusPolling (
    (UINTN) (DspBar + R_HDA_MEM2_ADSPCS),
    (UINT32) B_HDA_MEM2_ADSPCS_CPA (HDA_ADSP_CORE1),
    (UINT32) ~(B_HDA_MEM2_ADSPCS_CPA (HDA_ADSP_CORE1)));
  if (EFI_ERROR (Status)) {
    return Status;
  }

  DEBUG ((DEBUG_INFO, "DSP: CPA is cleared, DSP is in reset.\n"));

  return EFI_SUCCESS;
}


/**
  Function gets Sndw links out of reset

  @param[in] SndwLinkIndex        Soundwire link index

  @retval EFI_SUCCESS             Sndw link is out of reset
  @retval EFI_TIMEOUT             Polling CPA bit timed out, Sndw link is not out of reset
**/
EFI_STATUS
GetSndwLinkOutOfReset (
  IN UINT64   DspBar,
  IN UINTN    SndwLinkIndex
  )
{
  EFI_STATUS Status;

  DEBUG ((DEBUG_INFO, "Sndw%d: set SPA, waiting for CPA.\n", SndwLinkIndex));
  ///
  /// Set SNDWLCTL.SPA = 1 to get SoundWire IP power up and out of reset.
  ///
  MmioOr32 ((UINTN) (DspBar + R_HDA_MEM2_SNDW_SNDWLCTL), B_HDA_MEM2_SNDW_SNDWLCTL_SPA (SndwLinkIndex));

  ///
  /// Wait until SNDWLCTL.CPA = 1 to confirm SoundWire IP is alive.
  ///
  Status = SndwStatusPolling (
    (UINTN) (DspBar + R_HDA_MEM2_SNDW_SNDWLCTL),
    B_HDA_MEM2_SNDW_SNDWLCTL_CPA (SndwLinkIndex),
    B_HDA_MEM2_SNDW_SNDWLCTL_CPA (SndwLinkIndex));
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Sndw%d stalled!\n", SndwLinkIndex));
    return Status;
  }

  DEBUG ((DEBUG_INFO, "Sndw%d: CPA is set, controller out of reset. Status = %r\n", SndwLinkIndex, Status));
  return EFI_SUCCESS;
}


/**
  Function allows to define Sndw controller mmio address.

  @param[in]  DspBar                      Memory map address of Dsp
  @param[in]  SndwLinkIndex               Number of Sndw link
  @param[out] *SndwControllerMmioAddress  Pointer to memory space address of Sndw controller

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Dsp memory space is not enabled
  @retval EFI_DEVICE_ERROR        Not able to get Sndw controller mmio address
**/
EFI_STATUS
GetLinkControllerMmioAddress (
  IN  UINTN   DspBar,
  IN  UINTN   SndwLinkIndex,
  OUT UINTN   *SndwControllerMmioAddress
  )
{
  if (DspBar == 0) {
    return EFI_INVALID_PARAMETER;
  }

  *SndwControllerMmioAddress = (UINTN) (MmioRead32 ((UINTN) (DspBar + R_HDA_MEM2_SNDW_SNDWIPPTR)) & B_HDA_MEM2_SNDW_SNDWIPPTR_PRT) + SndwLinkIndex * R_HDA_MEM2_SNDW_OFFSETS;
  if ((*SndwControllerMmioAddress == 0) || (*SndwControllerMmioAddress == (UINT64) ~0)) {
    return EFI_DEVICE_ERROR;
  }
  return EFI_SUCCESS;
}


/**
  Function reads and returns number of supported Sndw links.

  @param[in]  DspBar                        Memory map address of Dsp
  @param[out] *NumberOfSupportedSndwLinks   Pointer to variable where number of supported Sndw links should be written

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Dsp memory space is not enabled
**/
EFI_STATUS
GetNumberOfSupportedSndwLinks (
  IN  UINTN   DspBar,
  OUT UINTN   *NumberOfSupportedSndwLinks
  )
{
  if (DspBar == 0) {
    return EFI_INVALID_PARAMETER;
  }

  *NumberOfSupportedSndwLinks = (UINTN) (MmioRead32 ((UINTN) (DspBar + R_HDA_MEM2_SNDW_SNDWLCAP)) & B_HDA_MEM2_SNDW_SNDWLCAP_LCOUNT);

  return EFI_SUCCESS;
}