/** @file
  Header file for Sndw Audio Common Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PEI_DXE_SNDW_COMMON_LIB_H_
#define _PEI_DXE_SNDW_COMMON_LIB_H_

#include <SndwMipiCmd.h>
#include <SndwAccess.h>


/**
  Polling the Status bit.

  @param[in] StatusReg            The register address to read the status
  @param[in] PollingBitMap        The bit mapping for polling
  @param[in] PollingData          The Data for polling

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Polling the bit map time out
**/
EFI_STATUS
SndwStatusPolling (
  IN      UINTN           StatusReg,
  IN      UINT32          PollingBitMap,
  IN      UINT32          PollingData
  );


/**
  Function asks connected codec about identification information.

  @param[in]  SndwControllerSpace      Sndw controller memory space address
  @param[in]  SndwPeripheralAddress    Peripheral address
  @param[in]  SndwCodecId              Pointer to Sndw codec information structure

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Not able to read ids
  @retval EFI_DEVICE_ERROR        Responses from codec are incorrect
**/
EFI_STATUS
ReadPeripheralId (
  IN UINTN            SndwControllerSpace,
  IN UINTN            SndwPeripheralAddress,
  OUT SNDW_CODEC_ID   *SndwCodecId
  );


/**
  Function checks an access to Sndw controller

  @param[in]   SndwControllerSpaceAddress    Sndw controller memory space address

  @retval TRUE           Sndw Controller is ready
  @retval FALSE          Sndw Controller is not ready
**/
BOOLEAN
SndwIoAccessReady (
  IN UINTN          SndwControllerSpaceAddress
  );


/**
  Function operating on Sndw Fifo for sending message to codecs.

  @param[in]  SndwControllerSpace Sndw controller memory space address
  @param[in]  TxCommands          Pointer to send messages
  @param[in]  NumOfMessages       Size of messages to send
**/
VOID
Send (
  IN  UINTN           SndwControllerSpace,
  IN  SNDW_COMMAND    *TxCommands,
  IN  UINTN           NumOfMessages
  );


/**
  Function operating on Sndw Fifo for receiving message from codecs.

  @param[in]  SndwControllerSpace Sndw controller memory space address
  @param[out] RxCommands          Pointer to pointer to received messages.
                                  Memory pool is allocated in this function,
                                  but the caller is responsible for
                                  freeing the memory with FreePool.
  @param[out] RxSize              Size of received messages
**/
VOID
Receive (
  IN  UINTN           SndwControllerSpace,
  OUT SNDW_COMMAND    **RxCommands,
  OUT UINTN*          RxSize
  );


/**
  Function sends one message through the Sndw interface and waits
  for the corresponding ACK message.

  @param[in]  SndwControllerSpace Sndw controller memory space address
  @param[in]  TxCommands          Message to send
  @param[out] RxCommands          Pointer to receiving message

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Sending command has failed
  @retval EFI_DEVICE_ERROR        Response command has failed
**/
EFI_STATUS
SendwAck (
  IN  UINTN           SndwControllerSpace,
  IN  SNDW_COMMAND    TxCommand,
  OUT SNDW_COMMAND*   RxCommand OPTIONAL
  );


/**
  Function initializes Sndw controller for enumerating connected codecs

  @param[in]  SndwControllerMmioAddress       Sndw controller memory space address

  @retval EFI_SUCCESS                         Sndw controller initialization completed successfully
  @retval EFI_TIMEOUT                         Polling FIFO status timed out
**/
EFI_STATUS
SndwControllerInit (
  IN UINTN          SndwControllerMmioAddress
  );


/**
  Function reads and returns Sndw codecs status

  @param[in]  SndwControllerSpaceAddress         Sndw controller memory space address
  @param[in]  PeripheralsStatus                  Pointer to variable where codecs status should be written
**/
EFI_STATUS
SndwReadCodecsStatus (
  IN  UINTN         SndwControllerSpaceAddress,
  OUT UINT32        *PeripheralsStatus
);


/**
  Function prints information about given Sndw Codec

  @param[in]  SndwCodecInfo    Pointer to Sndw codec information
**/
VOID
PrintCodecInfo (
  IN SNDW_CODEC_INFO  *SndwCodecInfo
  );

#endif
