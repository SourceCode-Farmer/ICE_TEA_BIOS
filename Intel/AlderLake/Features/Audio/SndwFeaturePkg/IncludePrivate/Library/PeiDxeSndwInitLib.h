/** @file
  Header file for Sndw Audio Init Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PEI_DXE_SNDW_INIT_LIB_
#define _PEI_DXE_SNDW_INIT_LIB_


/**
  Function allows to define Sndw controller mmio address.

  @param[in]  DspBar                      Memory map address of Dsp
  @param[in]  SndwLinkIndex               Number of Sndw link
  @param[out] *SndwControllerMmioAddress  Pointer to memory space address of Sndw controller

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Dsp memory space is not enabled
  @retval EFI_DEVICE_ERROR        Not able to get Sndw controller mmio address
**/
EFI_STATUS
GetLinkControllerMmioAddress (
  IN  UINTN     DspBar,
  IN  UINTN     SndwLinkIndex,
  OUT UINTN     *SndwControllerMmioAddress
  );


/**
  Function reads and returns number of supported Sndw links.

  @param[in]  DspBar                        Memory map address of Dsp
  @param[out] *NumberOfSupportedSndwLinks   Pointer to variable where number of supported Sndw links should be written

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Dsp memory space is not enabled
**/
EFI_STATUS
GetNumberOfSupportedSndwLinks (
  IN  UINTN     DspBar,
  OUT UINTN     *NumberOfSupportedSndwLinks
  );


/**
  Function sets Sndw shim registers to perform controller initialization.

  @param[in]  DspBar              Memory map address of Dsp
  @param[in]  SndwLinkIndex       Index of Sndw link whose registers should be set

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Dsp memory space is not enabled
**/
VOID
SetSndwShimRegisters (
  IN UINTN      DspBar,
  IN UINT32     SndwLinkIndex
  );


/**
  Function gets DSP out of reset and confirms that DSP is alive

  @param[out]  DspBar             Pointer to DSP BAR

  @retval EFI_SUCCESS             DSP is out of reset
  @retval EFI_UNSUPPORTED         HDA is not enabled
  @retval EFI_DEVICE_ERROR        Polling CPA bit timed out, DSP is not out of reset
**/
EFI_STATUS
GetDspOutOfReset (
  IN  UINT64    HdaBar,
  OUT UINT64    DspBar
  );


/**
  Function resets DSP and confirms that DSP is in reset state

  @param[out]  DspBar             DSP BAR

  @retval EFI_SUCCESS             DSP is in reset
  @retval EFI_DEVICE_ERROR        Polling CPA bit timed out, DSP is still out of reset
**/
EFI_STATUS
ResetDsp (
  OUT UINT64    DspBar
  );


/**
  Function gets Sndw links out of reset

  @param[in] SndwLinkIndex        Soundwire link index

  @retval EFI_SUCCESS             Sndw link is out of reset
  @retval EFI_TIMEOUT             Polling CPA bit timed out, Sndw link is not out of reset
**/
EFI_STATUS
GetSndwLinkOutOfReset (
  IN UINT64   DspBar,
  IN UINTN    SndwLinkIndex
  );

#endif
