/** @file
  TBD

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SNDW_MIPI_CMD_H_
#define _SNDW_MIPI_CMD_H_

#define SNDW_CMD_DEFAULT    0

enum SNDW_COMMAND_TYPE {
  SndwCmdPing      = 0,
  SndwCmdReserved  = 1,
  SndwCmdRead      = 2,
  SndwCmdWrite     = 3,
  SndwCmdInvalid   = 4
};

typedef union {
  UINT32 Command;
  struct {
    UINT32 Reserved0     :  8;
    UINT32 SSync         :  8;
    UINT32 Reserved1     :  1;
    UINT32 DSync         :  4;
    UINT32 Reserved2     :  5;
    UINT32 Ssp           :  1;
    UINT32 Reserved3     :  1;
    UINT32 OpCode        :  3;
    UINT32 SspTag        :  1;
  } TxPing;
  struct {
    UINT32 RegData       :  8;
    UINT32 RegAddr       :  16;
    UINT32 DeviceAddress :  4;
    UINT32 OpCode        :  3;
    UINT32 SspTag        :  1;
  } TxWrite;
  struct {
    UINT32 Reserved0     :  8;
    UINT32 RegAddr       :  16;
    UINT32 DeviceAddress :  4;
    UINT32 OpCode        :  3;
    UINT32 SspTag        :  1;
  } TxRead;
  struct {
    UINT32 Reserved0     :  24;
    UINT32 DeviceAddress :  4;
    UINT32 OpCode        :  3;
    UINT32 SspTag        :  1;
  } Tx;
  struct {
    UINT32 Ack           :  1;
    UINT32 Nak           :  1;
    UINT32 Reserved0     :  2;
    UINT32 OpCode        :  3;
    UINT32 Reserved1     :  1;
    UINT32 Data          :  8;
    UINT32 Reserved2     : 16;
  } Rx;
} SNDW_COMMAND;

#endif
