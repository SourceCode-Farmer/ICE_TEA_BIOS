/** @file
  Sndw Access protocol definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SNDW_ACCESS_H_
#define _SNDW_ACCESS_H_

#include <SndwMipiCmd.h>

typedef union {
  UINT8       Data[6];
  struct {
    UINT8     Version;
    UINT8     ManufacturerID[2];
    UINT8     PartId[2];
    UINT8     Class;
  } Encoding;
} SNDW_CODEC_ID;

typedef struct {
  UINT32         SndwLinkIndex;
  SNDW_CODEC_ID  CodecId;
} SNDW_CODEC_INFO;

typedef struct _SNDW_ACCESS SNDW_ACCESS;


/**
  Function operating on Sndw Fifo buffer for sending messages to codecs.

  @param[in]  This                Pointer to Sndw Access Protocol
  @param[in]  SndwCodecInfo       Sndw codec information
  @param[in]  TxCommands          Pointer to send messages
  @param[in]  TxSize              Size of messages to send

  @retval EFI_SUCCESS             Message sent successfully
  @retval EFI_NOT_FOUND           Given codec not found
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_SEND) (
  IN  CONST SNDW_ACCESS             *This,
  IN  SNDW_CODEC_INFO               SndwCodecInfo,
  IN  SNDW_COMMAND                  *TxCommand,
  OUT UINTN                         TxSize
  );


/**
  Function sends one message through the Sndw interface and waits
  for the corresponding ACK message.

  @param[in]  This                Pointer to Sndw Access Protocol
  @param[in]  SndwCodecInfo       Sndw codec information
  @param[in]  TxCommands          Pointer to sending message
  @param[out] RxCommands          Pointer to receiving message

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_NOT_FOUND           Given codec not found
  @retval EFI_TIMEOUT             Sending command has failed
  @retval EFI_DEVICE_ERROR        Response command has failed
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_SEND_WITH_ACK) (
  IN  CONST SNDW_ACCESS         *This,
  IN  SNDW_CODEC_INFO           SndwCodecInfo,
  IN  SNDW_COMMAND              TxCommand,
  OUT SNDW_COMMAND*             RxCommand OPTIONAL
  );


/**
  Function operating on Sndw Fifo for receiving messages from codecs.

  @param[in]  This                Pointer to Sndw Access Protocol
  @param[in]  SndwCodecInfo       Sndw codec information
  @param[out] RxCommands          Pointer to pointer to received messages.
                                  Memory pool is allocated in this function,
                                  but the caller is responsible for
                                  freeing the memory with FreePool.
  @param[out] RxSize              Size of received messages

  @retval EFI_SUCCESS             Message received successfully
  @retval EFI_NOT_FOUND           Given codec not found
  @retval EFI_OUT_OF_RESOURCES    Memory allocation for response failed
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_RECEIVE) (
  IN  CONST SNDW_ACCESS         *This,
  IN  SNDW_CODEC_INFO           SndwCodecInfo,
  OUT SNDW_COMMAND              **RxCommands,
  OUT UINTN*                    RxSize
  );


/**
  Function return first codec saved in codecs list.
  If it does not exist, function sets *CodecInfo to NULL.

  @param[in]  This                Pointer to Sndw Access Protocol
  @param[out] SndwCodecInfo           Pointer to pointer to Sndw codec information structure.
                                  The caller is responsible for freeing the memory.

  @retval EFI_SUCCESS             Codec found
  @retval EFI_NOT_FOUND           Codec not found
  @retval EFI_OUT_OF_RESOURCES    Memory allocation for codec info failed
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_GET_FIRST_CODEC) (
  IN  CONST SNDW_ACCESS           *This,
  OUT       SNDW_CODEC_INFO       **SndwCodecInfo
  );


/**
  Function retrieves the next codec saved in codecs list that follows the given one.
  If it does not exist, function sets *NextCodecInfo to NULL.

  @param[in]  This                Pointer to Sndw Access Protocol
  @param[in]  SndwCodecInfo       Pointer to Sndw codec information structure.
  @param[out] NextCodecInfo       Pointer to pointer to Sndw codec information structure.
                                  The caller is responsible for freeing the memory.

  @retval EFI_SUCCESS             Codec found
  @retval EFI_NOT_FOUND           Codec not found
  @retval EFI_OUT_OF_RESOURCES    Memory allocation for codec info failed
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_GET_NEXT_CODEC) (
  IN  CONST SNDW_ACCESS           *This,
  IN        SNDW_CODEC_INFO       *SndwCodecInfo,
  OUT       SNDW_CODEC_INFO       **NextCodecInfo
  );


/**
  Function enables Soundwire interface and enumerates attached codecs

  @param[in] This           Pointer to Sndw Access Protocol

  @retval EFI_SUCCESS
  @retval EFI_DEVICE_ERROR
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_ACCESS_ENABLE) (
    IN CONST SNDW_ACCESS        *This
  );


/**
  Function disables Soundwire interface

  @param[in] This           Pointer to Sndw Access Protocol

  @retval EFI_SUCCESS
  @retval EFI_DEVICE_ERROR
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_ACCESS_DISABLE) (
  IN CONST SNDW_ACCESS *This
  );


///
/// Sndw Access
///
/// This ppi/protocol allows to initialize and communicate with the Sndw controllers.
///
struct _SNDW_ACCESS {
  ///
  /// Send message
  ///
  SNDW_SEND             Send;
  ///
  /// Send message and wait for corresponding ACK message
  ///
  SNDW_SEND_WITH_ACK    SendWithAck;
  ///
  /// Receive message
  ///
  SNDW_RECEIVE          Receive;
  ///
  /// Enable Sndw interface and enumerate attached codecs
  ///
  SNDW_ACCESS_ENABLE    Enable;
  ///
  /// Disable Sndw interface
  ///
  SNDW_ACCESS_DISABLE   Disable;
  ///
  /// Get first codec saved in codecs list created during the enumeration process
  ///
  SNDW_GET_FIRST_CODEC  GetFirstCodec;
  ///
  /// Retrieve the next codec saved in codecs list that follows the given one
  ///
  SNDW_GET_NEXT_CODEC   GetNextCodec;
};

#endif
