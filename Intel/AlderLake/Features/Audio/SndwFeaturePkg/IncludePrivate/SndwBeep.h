/** @file
  Sndw beep definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef __SNDW_BEEP_H__
#define __SNDW_BEEP_H__

typedef struct _SNDW_BEEP SNDW_BEEP;

/**
  Function sends command which disables sine tone generator.

  @param[in]  This                Pointer to Sndw ppi

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Not able to send command to codec
  @retval EFI_DEVICE_ERROR        Responses from codec are incorrect
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_BEEP_OFF) (
  IN CONST SNDW_BEEP *This
  );

/**
  Function sends command which enables sine tone generator.

  @param[in] This                 Pointer to Sndw Ppi
  @param[in] Frequency            Frequency value for tone generator. Refer to codec specification.
  @param[in] Amplitude            Amplitude value for tone generator. Refer to codec specification.

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Not able to send command to codec
  @retval EFI_DEVICE_ERROR        Responses from codec are incorrect
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_BEEP_ON) (
  IN CONST SNDW_BEEP              *This,
  IN UINTN                        Frequency,
  IN INTN                         Amplitude
  );

/**
  Function send command which enable sine tone generator

  @param[in] This                 Pointer to Sndw beep protocol.
  @param[in] Frequency            Frequency value for tone generator. Refer to codec specification.
  @param[in] Amplitude            Amplitude value for tone generator. Refer to codec specification.
  @param[in] NumOfBeeps           Number of beeps.
  @param[in] BeepDuration         Length of beep sound in miliseconds.
  @param[in] PauseDuration        Duration of pause between beeps.

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_TIMEOUT             Not able to send command to codec
  @retval EFI_DEVICE_ERROR        Responses from codec are incorrect
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_BEEP_GEN) (
  IN CONST SNDW_BEEP              *This,
  IN UINTN                        Frequency,
  IN INTN                         Amplitude,
  IN UINTN                        NumOfBeeps,
  IN UINTN                        BeepDuration,
  IN UINTN                        PauseDuration
  );

/**
  This function enables Sndw interface.

  @param[in] *This              Pointer to Sndw beep protocol.

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_DEVICE_ERROR      Not able to enable DSP memory space.
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_ENABLE) (
  IN CONST SNDW_BEEP              *This
  );

/**
  This function disables Sndw interface.

  @param[in] *This              Pointer to Sndw beep protocol.

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_DEVICE_ERROR      Not able to enable DSP memory space.
**/
typedef
EFI_STATUS
(EFIAPI *SNDW_DISABLE) (
  IN CONST SNDW_BEEP              *This
  );

///
/// Sndw beep
///
/// This ppi/protocol manipulates the Sndw controller to perform transactions as a
/// manager on the Sndw bus and connected codec. Support allows to initialize
/// tone generator built in Sndw codec and perform beep sound.
///
struct _SNDW_BEEP {
  ///
  /// Stop of beep generation
  ///
  SNDW_BEEP_OFF BeepOff;
  ///
  /// Start of beep generation
  ///
  SNDW_BEEP_ON  BeepOn;
  ///
  /// Generate multiple beep sounds of a certain length
  ///
  SNDW_BEEP_GEN  Beep;
  ///
  /// Enable Sndw interface
  ///
  SNDW_ENABLE  Enable;
  ///
  /// Disable Sndw interface
  ///
  SNDW_DISABLE Disable;
};

#endif  //  __SNDW_BEEP_H__
