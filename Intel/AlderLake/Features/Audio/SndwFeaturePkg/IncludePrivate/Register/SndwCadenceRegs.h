/** @file
  Definitions of registers for Sndw Cadence controllers

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SNDW_CADENCE_REGS_H_
#define _SNDW_CADENCE_REGS_H_

#define R_SNDW_MEM_CONFIG                              0x00
#define B_SNDW_MEM_CONFIG_MODE_AHB                     BIT3
#define B_SNDW_MEM_CONFIG_OM_NORMAL                    (BIT2 | BIT1 | BIT0)
#define R_SNDW_MEM_CTRL                                0x04
#define B_SNDW_MEM_CTRL_BLOCKWAKEUP                    BIT0
#define B_SNDW_MEM_CTRL_CLOCKSTOPCLEAR                 BIT2

#define R_SNDW_MEM_CMDCTRL                             0x08
#define R_SNDW_MEM_SSPSTAT                             0x0C
#define R_SNDW_MEM_FRAMESHAPE                          0x10
#define R_SNDW_MEM_FRAMESHAPEINIT                      0x14
#define R_SNDW_MEM_CONFIGUPDATE                        0x18
#define B_SNDW_MEM_CONFIGUPDATE_UPDATE_DONE            BIT0
#define R_SNDW_MEM_PHYCTRL                             0x1C

#define R_SNDW_MEM_STAT                                0x40
#define B_SNDW_MEM_STAT_TXE                            BIT1
#define B_SNDW_MEM_STAT_TXE_FIFO_EMPTY                 BIT1
#define B_SNDW_MEM_STAT_RXNE                           BIT3
#define B_SNDW_MEM_STAT_RXNE_FIFO_EMPTY                BIT3
#define R_SNDW_MEM_INTSTAT                             0x44
#define R_SNDW_MEM_INTMASK                             0x48
#define B_SNDW_MEM_INTMASK_RXNE                        BIT3
#define B_SNDW_MEM_INTMASK_RXNE_FIFO_EMPTY             BIT3

#define R_SNDW_MEM_INTSET                              0x4C

#define R_SNDW_MEM_PERIPHERALSTAT                      0x50
#define B_SNDW_MEM_PERIPHERALSTAT_STATUS(x)            ((BIT1 | BIT0) << (x * 2))
#define R_SNDW_MEM_PERIPHERALINTSTAT                   0x54
#define B_SNDW_MEM_PERIPHERALINTSTAT_STATUS(x)         ((BIT1 | BIT0) << (x * 2))

#define R_SNDW_MEM_FIFOSTAT                            0x7C
#define B_SNDW_MEM_FIFOSTAT_AVAIL                      (BIT5 | BIT4 | BIT3 | BIT2 | BIT1 | BIT0)
#define N_SNDW_MEM_FIFOSTAT_AVAIL                      0
#define B_SNDW_MEM_FIFOSTAT_FREE                       (BIT13 | BIT12 | BIT11 | BIT10 | BIT9 | BIT8)
#define N_SNDW_MEM_FIFOSTAT_FREE                       8
#define R_SNDW_MEM_COMMAND                             0x80

#define SNDW_MAX_PERIPHERAL_NUMBER                     12

enum SNDW_PERIPHERAL_STATUS {
  SndwPeripheralNotPresent   = 0,
  SndwPeripheralAttachedOk   = 1,
  SndwPeripheralAlert        = 2,
  SndwPeripheralReserved     = 3
};

#endif
