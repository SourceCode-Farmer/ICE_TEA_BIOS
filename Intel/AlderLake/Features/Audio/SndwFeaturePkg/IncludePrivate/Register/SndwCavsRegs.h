/** @file
  Definitions of registers for Sndw cAvs controller

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SNDW_CAVS_REGS_H_
#define _SNDW_CAVS_REGS_H_

///
/// The default PCH PCI bus number
///
#define DEFAULT_PCI_BUS_NUMBER_PCH              0
#define DEFAULT_PCI_SEGMENT_NUMBER_PCH          0

#define B_PCI_BAR_MEMORY_TYPE_MASK              (BIT1 | BIT2)
#define B_PCI_BAR_MEMORY_TYPE_64                BIT2

//
// PCI Configuration Space Registers
//
#define PCI_DEVICE_NUMBER_PCH_HDA               31
#define PCI_FUNCTION_NUMBER_PCH_HDA             3

#define R_HDA_CFG_HDALBA                        0x10
#define B_HDA_CFG_HDALBA_LBA                    0xFFFFC000
#define V_HDA_CFG_HDABAR_SIZE                   0x100000
#define R_HDA_CFG_HDAUBA                        0x14
#define B_HDA_CFG_HDAUBA_UBA                    0xFFFFFFFF
#define R_HDA_CFG_ADSPLBA                       0x20
#define B_HDA_CFG_ADSPLBA_LBA                   0xFFF00000
#define R_HDA_CFG_ADSPUBA                       0x24
#define B_HDA_CFG_ADSPUBA_UBA                   0xFFFFFFFF

#define R_HDA_MEM_GCTL                          0x08
#define B_HDA_MEM_GCTL_CRST                     BIT0

#define R_HDA_MEM2_ADSPCS                       0x04
#define V_HDA_MEM2_ADSPCS_DSPMPC                4
#define B_HDA_MEM2_ADSPCS_SPA(x)                (BIT0 << ((x) + N_HDA_MEM2_ADSPCS_SPA))
#define N_HDA_MEM2_ADSPCS_SPA                   16
#define B_HDA_MEM2_ADSPCS_CPA(x)                (BIT0 << ((x) + N_HDA_MEM2_ADSPCS_CPA))
#define N_HDA_MEM2_ADSPCS_CPA                   24

#define R_HDA_MEM2_ADSPIS                       0x0C
#define B_HDA_MEM2_ADSPIS_IPC                   BIT0

#define R_HDA_MEM2_ADSPIC2                      0x10
#define B_HDA_MEM2_ADSPIC2_SNDW                 BIT5

#define R_HDA_MEM2_IDCCP                        0x20
#define B_HDA_MEM2_IDCCP_INST                   (BIT27 | BIT26 | BIT25 | BIT24)
#define N_HDA_MEM2_IDCCP_INST                   24

#define R_HDA_MEM2_SNDW                         0x2C000
#define R_HDA_MEM2_SNDW_SNDWLCAP                R_HDA_MEM2_SNDW + 0x00
#define B_HDA_MEM2_SNDW_SNDWLCAP_LCOUNT         (BIT2 | BIT1 | BIT0)
#define R_HDA_MEM2_SNDW_SNDWLCTL                R_HDA_MEM2_SNDW + 0x04
#define N_HDA_MEM2_SNDW_SNDWLCTL_SPA            0
#define B_HDA_MEM2_SNDW_SNDWLCTL_SPA(x)         (BIT0 << ((x) + N_HDA_MEM2_SNDW_SNDWLCTL_SPA))
#define N_HDA_MEM2_SNDW_SNDWLCTL_CPA            8
#define B_HDA_MEM2_SNDW_SNDWLCTL_CPA(x)         (BIT0 << ((x) + N_HDA_MEM2_SNDW_SNDWLCTL_CPA))
#define R_HDA_MEM2_SNDW_SNDWIPPTR               R_HDA_MEM2_SNDW + 0x08
#define B_HDA_MEM2_SNDW_SNDWIPPTR_PRT           0xFFFFF

#define R_HDA_MEM2_SNDW_SNDWxCTLSCAP(x)         R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x10
#define R_HDA_MEM2_SNDW_SNDWxCTLS0CM(x)         R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x12
#define R_HDA_MEM2_SNDW_SNDWxCTLS1CM(x)         R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x14
#define R_HDA_MEM2_SNDW_SNDWxCTLS2CM(x)         R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x16
#define R_HDA_MEM2_SNDW_SNDWxCTLS3CM(x)         R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x18
#define R_HDA_MEM2_SNDW_SNDWxIOCTL(x)           R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x6C
#define B_HDA_MEM2_SNDW_SNDWxIOCTL_WPDD         BIT6
#define B_HDA_MEM2_SNDW_SNDWxIOCTL_BKE          BIT5
#define B_HDA_MEM2_SNDW_SNDWxIOCTL_DOE          BIT4
#define B_HDA_MEM2_SNDW_SNDWxIOCTL_DO           BIT3
#define B_HDA_MEM2_SNDW_SNDWxIOCTL_COE          BIT2
#define B_HDA_MEM2_SNDW_SNDWxIOCTL_MIF          BIT0
#define R_HDA_MEM2_SNDW_SNDWxACTMCTL(x)         R_HDA_MEM2_SNDW + (0x60 * (x)) + 0x6E
#define B_HDA_MEM2_SNDW_SNDWxACTMCTL_DACTQE     BIT0
#define R_HDA_MEM2_SNDW_SNDWWAKEEN              0x190
#define B_HDA_MEM2_SNDW_SNDWWAKEEN_PWE          (BIT3 | BIT2 | BIT1 | BIT0)
#define R_HDA_MEM2_SNDW_SNDWWAKESTS             0x192
#define B_HDA_MEM2_SNDW_SNDWWAKESTS_PWS         (BIT3 | BIT2 | BIT1 | BIT0)

#define R_HDA_MEM2_SNDW_OFFSETS                 0x10000
#define HDA_ADSP_CORE1                          0
#define HDA_ADSP_CORE2                          1
#define HDA_ADSP_CORE3                          2
#define HDA_ADSP_CORE4                          3

#define SNDW_MAX_LOOP_TIME                      4000
#define SNDW_WAIT_PERIOD                        4

#endif
