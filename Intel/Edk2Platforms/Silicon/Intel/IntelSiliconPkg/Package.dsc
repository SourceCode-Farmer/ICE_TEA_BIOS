## @file
# Component description file for the TigerLakeBoardPkg.
#
#******************************************************************************
#* Copyright (c) 2013 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
# Copyright (c) 2015, Intel Corporation. All rights reserved.<BR>
#
# This program and the accompanying materials are licensed and made available under
# the terms and conditions of the BSD License which accompanies this distribution.
# The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

[Defines]
  PLATFORM_NAME                  = IntelSiliconPkg
  PLATFORM_GUID                  = 9B96228E-1155-4967-8E16-D0ED8E1B4297
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  OUTPUT_DIRECTORY               = Build/IntelSiliconPkg
  SUPPORTED_ARCHITECTURES        = IA32|X64
  BUILD_TARGETS                  = DEBUG|RELEASE|NOOPT
  SKUID_IDENTIFIER               = DEFAULT

[LibraryClasses.IA32]
#
# SmmAccess
#
  SmmAccessLib|IntelSiliconPkg/Feature/SmmAccess/Library/PeiSmmAccessLib/PeiSmmAccessLib.inf

[Components.IA32]
  IntelSiliconPkg/Feature/VTd/IntelVTdDmarPei/IntelVTdDmarPei.inf
#
# VTd Iommu Security for Pei
#
  IntelSiliconPkg/Feature/VTd/IntelVTdPmrPei/IntelVTdPmrPei.inf {
#    !if gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable == TRUE
    !if gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled == TRUE
    # To reduce FvPrememory size in Resiliency debug build
      <LibraryClasses>
        DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
    !endif
  }


[Components.X64]
#
# VTd Iommu Security
#
  IntelSiliconPkg/Feature/VTd/IntelVTdDxe/IntelVTdDxe.inf
#
# SmmAccess
#
  IntelSiliconPkg/Feature/SmmAccess/SmmAccessDxe/SmmAccess.inf

[LibraryClasses.common.DXE_DRIVER]
  DxeSmbiosDataHobLib|IntelSiliconPkg/Library/DxeSmbiosDataHobLib/DxeSmbiosDataHobLib.inf
#
# Workaround for SATA recovery fail if VMD and IOMMU are both enabled.
# Remove it when RstVmdPeim.efi revised allocating size of DMA buffer.
#
[PcdsFixedAtBuild]
!if gSiPkgTokenSpaceGuid.PcdVmdEnable == TRUE
  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPeiDmaBufferSize|0x00500000
!endif
