;------------------------------------------------------------------------------
;
; Copyright (c) 2016 - 2019, Intel Corporation. All rights reserved.<BR>
; SPDX-License-Identifier: BSD-2-Clause-Patent
;
; Abstract:
;
;   Switch the stack from temporary memory to permanent memory.
;
;------------------------------------------------------------------------------

    SECTION .text

extern ASM_PFX(SwapStack)

;------------------------------------------------------------------------------
; UINT32
; EFIAPI
; Pei2LoaderSwitchStack (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(Pei2LoaderSwitchStack)
ASM_PFX(Pei2LoaderSwitchStack):
    xor     eax, eax
    jmp     ASM_PFX(FspSwitchStack)

;------------------------------------------------------------------------------
; UINT32
; EFIAPI
; Loader2PeiSwitchStack (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(Loader2PeiSwitchStack)
ASM_PFX(Loader2PeiSwitchStack):
    jmp     ASM_PFX(FspSwitchStack)

;------------------------------------------------------------------------------
; UINT32
; EFIAPI
; FspSwitchStack (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(FspSwitchStack)
ASM_PFX(FspSwitchStack):
    ; Save current contexts
    push    eax
    pushfd
    cli
    pushad
    sub     esp, 8
    sidt    [esp]

    ; Load new stack
    push    esp
    call    ASM_PFX(SwapStack)
    mov     esp, eax

;[-start-190822-IB10180005-add]
    ; Please don't remove these code otherwise com port DDT will hang up.
    ; Restore int 1/2/3
    mov     edi, [esp + 2]
    sub     esp, 8
    sidt    [esp]
    mov     eax, [esp + 2]
    or      eax, eax
    jz      Skip
    mov     edx, [eax + 8]
    mov     [edi + 8], edx
    mov     edx, [eax + 12]
    mov     [edi + 12], edx
    mov     edx, [eax + 16]
    mov     [edi + 16], edx
    mov     edx, [eax + 20]
    mov     [edi + 20], edx
    mov     edx, [eax + 24]
    mov     [edi + 24], edx
    mov     edx, [eax + 28]
    mov     [edi + 28], edx
Skip:
    add     esp, 8
;[-end-190822-IB10180005-add]

    ; Restore previous contexts
    lidt    [esp]
    add     esp, 8
    popad
    popfd
    add     esp, 4
    ret

