/** @file

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Source code file for Platform Init PEI module

Copyright (c) 2017 - 2019, Intel Corporation. All rights reserved.<BR>
SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <IndustryStandard/Pci30.h>
#include <Ppi/EndOfPeiPhase.h>

#include <Guid/FirmwareFileSystem2.h>
#include <Protocol/FirmwareVolumeBlock.h>

#include <Library/TimerLib.h>
#include <Library/BoardInitLib.h>
#include <Library/TestPointCheckLib.h>
#include <Library/SetCacheMtrrLib.h>
//[-start-201105-IB09480110-add]//
#include <Ppi/ReadOnlyVariable2.h>
#include <SetupVariable.h>
//[-end-201105-IB09480110-add]//
//[-start-210127-IB05660155-modify]//
#include <HybridGraphicsDefine.h>
//[-end-210127-IB05660155-modify]//
//[-start-210428-IB05660158-add]//
#include <ChipsetSetupConfig.h>
//[-end-210428-IB05660158-add]//

EFI_STATUS
EFIAPI
PlatformInitEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR  mEndOfPeiNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PlatformInitEndOfPei
};

//[-start-190829-IB15410298-add]//
GLOBAL_REMOVE_IF_UNREFERENCED EFI_PEI_PPI_DESCRIPTOR  mHybridGraphicsReadyForPowerSequenceInitPpi = {
  EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gHybridGraphicsReadyForPowerSequenceInit,
  NULL
};
//[-end-190829-IB15410298-add]//

/**
  This function handles PlatformInit task at the end of PEI

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
EFI_STATUS
EFIAPI
PlatformInitEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                    Status;

  Status = BoardInitAfterSiliconInit ();
  ASSERT_EFI_ERROR (Status);

  TestPointEndOfPeiSystemResourceFunctional ();

  TestPointEndOfPeiPciBusMasterDisabled ();

  Status = SetCacheMtrrAfterEndOfPei ();
  ASSERT_EFI_ERROR (Status);

  TestPointEndOfPeiMtrrFunctional ();

  return Status;
}


/**
  Platform Init PEI module entry point

  @param[in]  FileHandle           Not used.
  @param[in]  PeiServices          General purpose services available to every PEIM.

  @retval     EFI_SUCCESS          The function completes successfully
  @retval     EFI_OUT_OF_RESOURCES Insufficient resources to create database
**/
EFI_STATUS
EFIAPI
PlatformInitPostMemEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                       Status;

  Status = BoardInitBeforeSiliconInit ();
  ASSERT_EFI_ERROR (Status);

//[-start-201207-IB16740125-remove]// Temporarily remove, this SaSetup.HgSlot member has removed in RC1491
////[-start-201105-IB09480110-add]//
////[-start-190829-IB15410298-add]//
//  if (FeaturePcdGet (PcdHybridGraphicsSupported)) {
//    EFI_PEI_READ_ONLY_VARIABLE2_PPI    *VariableServices;
//    UINTN                              VariableSize;
//    SA_SETUP                           SaSetup;
//
//    Status = PeiServicesLocatePpi (
//               &gEfiPeiReadOnlyVariable2PpiGuid,
//               0,
//               NULL,
//               (VOID **)&VariableServices
//               );
//    ASSERT_EFI_ERROR (Status);
//
//    VariableSize = sizeof (SA_SETUP);
//    Status = VariableServices->GetVariable (
//                                 VariableServices,
//                                 SA_SETUP_VARIABLE_NAME,
//                                 &gSaSetupVariableGuid,
//                                 NULL,
//                                 &VariableSize,
//                                 &SaSetup
//                                 );
//    ASSERT_EFI_ERROR (Status);
//
//[-start-210428-IB05660158-modify]//
  if (FeaturePcdGet (PcdHybridGraphicsSupported)) {
    EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariableServices;
    UINTN                            VariableSize;
    CHIPSET_CONFIGURATION            ChipsetConfiguration;

    Status = PeiServicesLocatePpi (
               &gEfiPeiReadOnlyVariable2PpiGuid,
               0,
               NULL,
               (VOID **)&VariableServices
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    VariableSize = sizeof (CHIPSET_CONFIGURATION);
    VariableServices->GetVariable (
                        VariableServices,
                        L"Setup",
                        &gSystemConfigurationGuid,
                        NULL,
                        &VariableSize,
                        &ChipsetConfiguration
                        );

//[-start-210113-IB05660151-modify]//
  //
  // For PCH PCIe slot, install gHybridGraphicsReadyForPowerSequenceInit PPI to invoke
  // callback function after configure GPIO (Post-Mem phase).
  // But whether HgSlot is PCH or PEG, HoldRst and PwrEnable of CRB ADL-S GPIO configure on Post-Mem phase,
  // so when PcdCrbSkuId is S-platform to install gHybridGraphicsReadyForPowerSequenceInit PPI on Post-Mem phase.
  // About PcdCrbSkuId definition, please refer as below.
  // AdlSSkuType: 1 ,AdlPSkuType: 2, AdlMSkuType: 3, AdlSimicsSkuType: 4
  //
//[-start-210127-IB05660155-modify]//
//[-start-210318-IB05660157-modify]//
    if (((ChipsetConfiguration.HgSlot == PCH) && (PcdGet16 (PcdCrbSkuId) == 2)) ||
//[-end-210318-IB05660157-modify]//
//[-end-210127-IB05660155-modify]//
      (PcdGet16 (PcdCrbSkuId) == 1)) {
      DEBUG ((DEBUG_INFO, "HybridGraphicsSlotPostMem \n"));
      Status = PeiServicesInstallPpi (&mHybridGraphicsReadyForPowerSequenceInitPpi);
      ASSERT_EFI_ERROR (Status);
    }
  }
//[-end-210428-IB05660158-modify]//
//[-end-210113-IB05660151-modify]//
//  }
////[-end-190829-IB15410298-add]//
////[-end-201105-IB09480110-add]//
//[-end-201207-IB16740125-remove]//

  //
  // Performing PlatformInitEndOfPei after EndOfPei PPI produced
  //
  Status = PeiServicesNotifyPpi (&mEndOfPeiNotifyList);

  return Status;
}
