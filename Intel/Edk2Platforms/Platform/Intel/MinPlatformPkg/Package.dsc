## @file
# Component description file for the MinPlatformPkg.
#
#******************************************************************************
#* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
# Copyright (c) 2015, Intel Corporation. All rights reserved.<BR>
#
# This program and the accompanying materials are licensed and made available under
# the terms and conditions of the BSD License which accompanies this distribution.
# The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

[Defines]
  DEFINE      PLATFORM_PACKAGE                  = MinPlatformPkg
  DEFINE      PLATFORM_FULL_PACKAGE             = AlderLakePlatSamplePkg
  DEFINE      PLATFORM_FEATURES_PATH            = $(PLATFORM_FULL_PACKAGE)/Features
  DEFINE      PLATFORM_SI_PACKAGE               = ClientOneSiliconPkg
  DEFINE      CLIENT_COMMON_PACKAGE             = ClientCommonPkg
  DEFINE      PLATFORM_BOARD_PACKAGE            = AlderLakeBoardPkg
  DEFINE      BOARD_TGL_U_BOARDS                = AlderLakeUBoards
  DEFINE      PROJECT_TGL_U_BOARDS              = $(PLATFORM_BOARD_PACKAGE)/$(BOARD_TGL_U_BOARDS)
  DEFINE      BOARD_TGL_H_BOARDS                = AlderLakeHBoards
  DEFINE      PROJECT_TGL_H_BOARDS              = $(PLATFORM_BOARD_PACKAGE)/$(BOARD_TGL_H_BOARDS)
  DEFINE      BOARD_TGL_Y_BOARDS                = AlderLakeYBoards
  DEFINE      PROJECT_TGL_Y_BOARDS              = $(PLATFORM_BOARD_PACKAGE)/$(BOARD_TGL_Y_BOARDS)

  PLATFORM_NAME = MinPlatformPkg
  PLATFORM_GUID = 8FE55D15-3ABF-4175-8169-74B87E5CD175
  PLATFORM_VERSION = 0.4
  DSC_SPECIFICATION = 0x00010005
  OUTPUT_DIRECTORY = Build/MinPlatformPkg
  SUPPORTED_ARCHITECTURES = IA32|X64
  BUILD_TARGETS = DEBUG|RELEASE
  SKUID_IDENTIFIER = DEFAULT

[LibraryClasses.common]
#
# Platform
#
  BoardInitLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/BoardInitLibNull/BoardInitLibNull.inf
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLibNull/TestPointCheckLibNull.inf
  DxeUpdatePlatformInfoLib|$(PLATFORM_BOARD_PACKAGE)/Library/DxeUpdatePlatformInfoLib/DxeUpdatePlatformInfoLib.inf
  SiliconPolicyInitLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/SiliconPolicyInitLibNull/SiliconPolicyInitLibNull.inf
  SiliconPolicyUpdateLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/SiliconPolicyUpdateLibNull/SiliconPolicyUpdateLibNull.inf
  PeiLib|$(PLATFORM_PACKAGE)/Library/PeiLib/PeiLib.inf
  PlatformBootManagerLib|$(PLATFORM_FULL_PACKAGE)/Library/DxePlatformBootManagerLib/DxePlatformBootManagerLib.inf
  LargeVariableReadLib|MinPlatformPkg/Library/BaseLargeVariableLib/BaseLargeVariableReadLib.inf
  VariableReadLib|MinPlatformPkg/Library/BaseVariableReadLibNull/BaseVariableReadLibNull.inf

[LibraryClasses.IA32]
  MultiBoardInitSupportLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/MultiBoardInitSupportLib/PeiMultiBoardInitSupportLib.inf
  BoardInitLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/MultiBoardInitSupportLib/PeiMultiBoardInitSupportLib.inf
!if $(TARGET) == DEBUG
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLib/PeiTestPointCheckLib.inf
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLibNull/TestPointCheckLibNull.inf
!endif
  BoardConfigLib|$(PLATFORM_BOARD_PACKAGE)/Library/PeiBoardConfigLib/PeiBoardConfigLib.inf

[LibraryClasses.IA32.SEC]
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLib/SecTestPointCheckLib.inf
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLibNull/TestPointCheckLibNull.inf

[LibraryClasses.common.PEIM]
SetCacheMtrrLib|MinPlatformPkg/Library/SetCacheMtrrLib/SetCacheMtrrLibNull.inf

[LibraryClasses.X64]
  #
  # DXE phase common
  #
  BoardInitLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/MultiBoardInitSupportLib/DxeMultiBoardInitSupportLib.inf
!if $(TARGET) == DEBUG
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLib/DxeTestPointCheckLib.inf
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLibNull/TestPointCheckLibNull.inf
!endif
  BoardConfigLib|$(PLATFORM_BOARD_PACKAGE)/Library/DxeBoardConfigLib/DxeBoardConfigLib.inf
  SiliconPolicyUpdateLib|$(PLATFORM_BOARD_PACKAGE)/Policy/Library/DxeSiliconPolicyUpdateLib/DxeSiliconPolicyUpdateLib.inf

[LibraryClasses.X64.DXE_CORE]
!if $(TARGET) == DEBUG
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLib/SmmTestPointCheckLib.inf
  TestPointCheckLib|$(PLATFORM_PACKAGE)/Test/Library/TestPointCheckLibNull/TestPointCheckLibNull.inf
!endif

[Components.IA32]
  $(PLATFORM_PACKAGE)/PlatformInit/PlatformInitPei/PlatformInitPreMem.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
      NULL|$(PROJECT_ADL_S_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPreMemLib.inf
!else
      NULL|$(PROJECT_ADL_P_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPreMemLib.inf
      NULL|$(PROJECT_ADL_M_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPreMemLib.inf
      NULL|$(PROJECT_ADL_U_SIMICS_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPreMemLib.inf
!endif
  }

  $(PLATFORM_PACKAGE)/PlatformInit/PlatformInitPei/PlatformInitPostMem.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
      NULL|$(PROJECT_ADL_S_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPostMemLib.inf
!else
    NULL|$(PROJECT_ADL_P_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPostMemLib.inf
    NULL|$(PROJECT_ADL_M_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPostMemLib.inf
    NULL|$(PROJECT_ADL_U_SIMICS_BOARDS)/Library/BoardInitLib/Pei/PeiMultiBoardInitPostMemLib.inf
!endif
  }

  $(PLATFORM_PACKAGE)/PlatformInit/SiliconPolicyPei/SiliconPolicyPeiPreMem.inf {
    <LibraryClasses>
    !if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1  # API mode
      SiliconPolicyInitLib|$(PLATFORM_SI_PACKAGE)/Library/PeiPreMemSiliconPolicyInitLib/PeiPreMemSiliconPolicyInitLibFsp.inf
      SiliconPolicyUpdateLib|$(PLATFORM_BOARD_PACKAGE)/Policy/Library/PeiSiliconPolicyUpdateLib/PeiSiliconPolicyUpdateLibFsp.inf
    !else
      SiliconPolicyInitLib|$(PLATFORM_SI_PACKAGE)/Library/PeiPreMemSiliconPolicyInitLib/PeiPreMemSiliconPolicyInitLib.inf
      SiliconPolicyUpdateLib|$(PLATFORM_BOARD_PACKAGE)/Policy/Library/PeiSiliconPolicyUpdateLib/PeiSiliconPolicyUpdateLib.inf
    !endif
  }

  $(PLATFORM_PACKAGE)/PlatformInit/SiliconPolicyPei/SiliconPolicyPeiPostMem.inf {
    <LibraryClasses>
    !if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1  # API mode
      SiliconPolicyInitLib|$(PLATFORM_SI_PACKAGE)/Library/PeiPostMemSiliconPolicyInitLib/PeiPostMemSiliconPolicyInitLibFsp.inf
      SiliconPolicyUpdateLib|$(PLATFORM_BOARD_PACKAGE)/Policy/Library/PeiSiliconPolicyUpdateLib/PeiSiliconPolicyUpdateLibFsp.inf
    !else
      SiliconPolicyInitLib|$(PLATFORM_SI_PACKAGE)/Library/PeiPostMemSiliconPolicyInitLib/PeiPostMemSiliconPolicyInitLib.inf
      SiliconPolicyUpdateLib|$(PLATFORM_BOARD_PACKAGE)/Policy/Library/PeiSiliconPolicyUpdateLib/PeiSiliconPolicyUpdateLib.inf
    !endif
  }


[Components.X64]
#
# Platform
#
  $(PLATFORM_PACKAGE)/PlatformInit/SiliconPolicyDxe/SiliconPolicyDxe.inf
  $(PLATFORM_PACKAGE)/PlatformInit/PlatformInitDxe/PlatformInitDxe.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
      NULL|$(PROJECT_ADL_S_BOARDS)/Library/BoardInitLib/Dxe/DxeMultiBoardInitlib.inf
!else
      NULL|$(PROJECT_ADL_P_BOARDS)/Library/BoardInitLib/Dxe/DxeMultiBoardInitlib.inf
      NULL|$(PROJECT_ADL_M_BOARDS)/Library/BoardInitLib/Dxe/DxeMultiBoardInitlib.inf
      NULL|$(PROJECT_ADL_U_SIMICS_BOARDS)/Library/BoardInitLib/Dxe/DxeMultiBoardInitlib.inf
!endif
  }
