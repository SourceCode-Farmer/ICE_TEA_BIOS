 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _EFI_SHELL_EC_READ_H_
#define _EFI_SHELL_EC_READ_H_

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/ShellCEntryLib.h>
#include <Library/ShellLib.h>

#define ECFLASH_VERSION          L"1.02"

// Try to program EC ROM within retry count times. The default retry count time is 3.
#define PROG_ROM_RETRY_COUNT    3

#define ITE_8386_ID             SIGNATURE_64 ('I','T','E','8','3','8','0','-')
#define ITE_8386_ID_OFFSET     (0x90)

#define ITE_8586_ID             SIGNATURE_64 ('I','T','E',' ','E','C','-','V')
#define ITE_8586_ID_OFFSET      (0x50)

#define ITE_8986_ID             0xB5A5A5A5A5A5A5A5
#define ITE_8986_ID_OFFSET      (0x40)

#define BLOCK_SIZE_64K          (64 * 1024)
#define BLOCK_SIZE_1K           (1 * 1024)

//#define EC_STATUS_PORT          0x6C
//#define EC_CMD_PORT             0x6C
//#define EC_DATA_PORT            0x68
  
#define EC_STATUS_PORT          0x66        
#define EC_CMD_PORT             0x66        
#define EC_DATA_PORT            0x62

#define EC_OBF                  0x01
#define EC_IBF                  0x02

#define KB_STATUS_PORT          0x64
#define KB_CMD_PORT             0x64
#define KB_DATA_PORT            0x60
#define KB_OBF                  0x01
#define KB_IBF                  0x02

#define SPICMD_WRSR				      0x01	// Write Status Register
#define SPICMD_BYTE_PROG			  0x02	// To Program One Data Byte
#define SPICMD_WRDI				      0x04	// Write disable
#define SPICMD_READ_STATUS		  0x05	// Read Status Register
#define SPICMD_WREN				      0x06	// Write Enable
#define SPICMD_HIGH_SPEED_READ	0x0B	// High-Speed Read
#define SPICMD_EWSR				      0x50	// Enable Write Status Register
#define SPICMD_RDID			      	0xAB	// Read ID
#define SPICMD__DEVICE_ID			  0x9F	// Manufacture ID command
#define SPICMD_AAI_WORD_PROG		0xAD	// Auto Address Increment Programming (word)
#define SPICMD_AAI_BYTE_PROG		0xAF	// Auto Address Increment Programming (byte)
#define SPICMD_64K_BYTE_BE	  	0xD8	// Erase 64 KByte block of memory array
//#define SPICMD_64K_BYTE_BE		  0x60  // Erase entire EC chip
#define SPICMD_1K_BYTE_BE		  	0xD7	// Erase 1 KByte block of memory array

#define SPICMD_CHIP_ERASE		    0x60

#define SSTID					          0xBF
#define WINBOND_ID				  	  0xEF
#define ATMEL_ID					  	  0x9F
#define ST_ID					          0x20
#define SPANSION_ID					    0x01
#define MXIC_ID					  	    0xC2
#define AMIC_ID					  	    0x37
#define EON_ID					        0x1C
#define ESMT_ID					  	    0x8C
#define ITE_ID					  	    0xFF

//
// EFI Required Block Elements Code Chart
//
#define BLOCKELEMENT_FULL_BLOCK   0x2588
#define PROGRESS_BAR_LEN          32
#define  PROGRESS_BAR_OFFSET      28
//
// EFI Console Colours
//
#define EFI_BLACK                 0x00
#define EFI_BLUE                  0x01
#define EFI_GREEN                 0x02
#define EFI_CYAN                  (EFI_BLUE | EFI_GREEN)
#define EFI_RED                   0x04
#define EFI_MAGENTA               (EFI_BLUE | EFI_RED)
#define EFI_BROWN                 (EFI_GREEN | EFI_RED)
#define EFI_LIGHTGRAY             (EFI_BLUE | EFI_GREEN | EFI_RED)
#define EFI_BRIGHT                0x08
#define EFI_DARKGRAY              (EFI_BRIGHT)
#define EFI_LIGHTBLUE             (EFI_BLUE | EFI_BRIGHT)
#define EFI_LIGHTGREEN            (EFI_GREEN | EFI_BRIGHT)
#define EFI_LIGHTCYAN             (EFI_CYAN | EFI_BRIGHT)
#define EFI_LIGHTRED              (EFI_RED | EFI_BRIGHT)
#define EFI_LIGHTMAGENTA          (EFI_MAGENTA | EFI_BRIGHT)
#define EFI_YELLOW                (EFI_BROWN | EFI_BRIGHT)
#define EFI_WHITE                 (EFI_BLUE | EFI_GREEN | EFI_RED | EFI_BRIGHT)

#define EFI_TEXT_ATTR(f, b)       ((f) | ((b) << 4))

#define EFI_BACKGROUND_BLACK      0x00
#define EFI_BACKGROUND_BLUE       0x10
#define EFI_BACKGROUND_GREEN      0x20
#define EFI_BACKGROUND_CYAN       (EFI_BACKGROUND_BLUE | EFI_BACKGROUND_GREEN)
#define EFI_BACKGROUND_RED        0x40
#define EFI_BACKGROUND_MAGENTA    (EFI_BACKGROUND_BLUE | EFI_BACKGROUND_RED)
#define EFI_BACKGROUND_BROWN      (EFI_BACKGROUND_GREEN | EFI_BACKGROUND_RED)
#define EFI_BACKGROUND_LIGHTGRAY  (EFI_BACKGROUND_BLUE | EFI_BACKGROUND_GREEN | EFI_BACKGROUND_RED)

typedef enum {
  OP_HELP = 0,
  OP_DUMP,
  OP_PROG
} OPERATION_EC;

typedef struct {
	UINT8 Muid;
	VOID (* ShowId) (VOID);	
} FLASH_DEV_VENDOR;

typedef struct {
  UINT8                 EcRomName[8];
  UINT8                 EcVerMajor;
  UINT8                 EcVerMinor;
  UINT8                 Reserved0;
  UINT8                 Reserved1;
  UINT32                EcRomSize;
  UINT64                EcRomIdOffset;
  UINT64                EcRomSignature;
} EC_ROM_HEADER;

#endif //_EFI_SHELL_EC_READ_H_