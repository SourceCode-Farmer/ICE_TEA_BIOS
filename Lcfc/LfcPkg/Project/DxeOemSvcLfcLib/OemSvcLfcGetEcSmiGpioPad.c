//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#include <PiDxe.h>
//[-start-210513-KEBIN00001-remover]#
//#include <GpioPinsCnlLp.h>
//[-end-210513-KEBIN00001-remover]#
#include <LfcOemDefine.h>

// this function will be build in DxeOemSvcLfcLib (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcGetEcSmiGpioPad (
  IN OUT  UINT32                         *LfcEcSmiGpioPad
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
// LCFCTODO  *LfcEcSmiGpioPad = GPIO_CNL_LP_GPP_E4;

  return Status;
}

