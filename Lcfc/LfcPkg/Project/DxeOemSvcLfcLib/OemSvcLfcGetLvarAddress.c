//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
#include <PiDxe.h>
#include <Library/OemSvcLfcGetLvarAddress.h>
#include <LfcOemDefine.h>

//
// Do not modify this routine
//
EFI_STATUS
EFIAPI
OemSvcLfcDxeGetLvarAddress (
  IN OUT  UINT32                         *Sub1Base,
  IN OUT  UINT32                         *Sub1Size,
  IN OUT  UINT32                         *Sub2Base,
  IN OUT  UINT32                         *Sub2Size,
  IN OUT  UINT32                         *DebugBase,
  IN OUT  UINT32                         *DebugSize
  )
{
  *Sub1Base = FixedPcdGet32 (PcdFlashFvLvarSub1Base);
  *Sub1Size = FixedPcdGet32 (PcdFlashFvLvarSub1Size);
  *Sub2Base = FixedPcdGet32 (PcdFlashFvLvarSub2Base);
  *Sub2Size = FixedPcdGet32 (PcdFlashFvLvarSub2Size);
  *DebugBase = FixedPcdGet32 (PcdFlashFvLvarDebugBase);
  *DebugSize = FixedPcdGet32 (PcdFlashFvLvarDebugSize);

  return EFI_SUCCESS;
}

