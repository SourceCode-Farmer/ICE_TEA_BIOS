//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
#include <PiDxe.h>
#include <Protocol/LfcNvsArea.h>
#include <LfcOemDefine.h>

//
// Update Lfc Oem Nvs Regin at SetLfcNvsData funciton (AcpiTableOverrideDxe.c)
//
EFI_STATUS
EFIAPI
OemSvcLfcDxeUpdateOemNvsRegion (
  IN OUT LFC_NVS_AREA  *LfcNvsBuffer
  )
{
  EFI_STATUS           Status = EFI_SUCCESS;

  //
  // If no need to use Oem Nvs Region, please leave it empty
  //

  return Status;
}

