//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
 //******************************************************************************
#include <PiDxe.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <SetupConfig.h>
#include <Protocol/LfcNvsArea.h>
#include <Library/LfcEcLib.h>
#include <LfcOemDefine.h>

//
// Update Lfc Oem Nvs Regin at ExitBootServices
//
EFI_STATUS
EFIAPI
OemSvcLfcExitBootServicesUpdateOemNvsRegion (
  IN OUT LFC_NVS_AREA  *LfcNvsBuffer
  )
{
  EFI_STATUS           Status = EFI_SUCCESS;

  UINTN                         SetupSize;
  SYSTEM_CONFIGURATION          Setup;

  //
  // If no need to use Oem Nvs Region, please leave it empty
  //
  SetupSize = sizeof (SYSTEM_CONFIGURATION);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  NULL,
                  &SetupSize,
                  &Setup
                  );

  if (!EFI_ERROR(Status)) {
// LCFCTODO: performance mode start
#if 0
// sample code from Whiskylake
    if (Setup.EnableHignPerformance == 1) { //high performance mode
      LfcNvsBuffer->PerformanceMode = 0;
      LfcEcLibSetPerformanceMode (HIGH_PERFORMANCE_MODE);
    } else {                    // quiet mode
      LfcNvsBuffer->PerformanceMode = 1;
      LfcEcLibSetPerformanceMode (QUIET_MODE);
    }
#endif
// LCFCTODO: performance mode end
  }

  return Status;
}

EFI_STATUS
EFIAPI
OemSvcLfcExitBootServices(
  VOID
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
  LFC_NVS_AREA_PROTOCOL         *LfcNvsAreaProtocol;

  Status = gBS->LocateProtocol (
                  &gLfcNvsAreaProtocolGuid,
                  NULL,
                  (VOID **)&LfcNvsAreaProtocol
                  );

  if (!EFI_ERROR(Status) && (LfcNvsAreaProtocol->Area != NULL)) {
    OemSvcLfcExitBootServicesUpdateOemNvsRegion (LfcNvsAreaProtocol->Area);
  }

  return Status;
}

