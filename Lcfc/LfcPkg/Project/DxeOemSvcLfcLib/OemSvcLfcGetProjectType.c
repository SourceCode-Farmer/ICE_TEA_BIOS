//*****************************************************************************
//
//
// Copyright (c) 2012 - 2020, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Uefi/UefiBaseType.h>
#include <Uefi/UefiSpec.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/LenovoVariable.h>
//[-start-211223-JEPLIUT210-add]//
#include <L05Config.h>
//[-end-211223-JEPLIUT210-add]//
//
// Yoga or Xiaoxin project Check 
//
// FUNCTIONAL DESCRIPTION.
//      Check project Yoga or Xiaoxin   
//
// MODIFICATION HISTORY.
//      
//
// ENTRY PARAMETERS.
//      None.
//
// EXIT PARAMETERS.
//      Function Return: 0: Yoga  1: Xiaoxin
//
// WARNINGS.
//      None.
//
//[-start-211223-JEPLIUT210-modify]//
UINT8 
OemSvcLfcGetProjectType(
)
{
  UINT8 ReturnValue = 0;
  EFI_STATUS Status; 
  LENOVO_VARIABLE_PROTOCOL    *LenovoVariable = NULL;
  EFI_GUID                    FamilyNameGuid = LVAR_FAMILY_NAME_GUID;
  UINT32                      DataSize;
  UINT8                      *TempBufferPtr = NULL;
 //  CHAR8                      *XiaoXinString =  "XiaoXinAir 15 IAL7" ;

  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable);
  if (!EFI_ERROR (Status)) {
    DataSize = L05_EEPROM_FAMILY_NAME_LENGTH ; 
    TempBufferPtr = AllocatePool(DataSize);
    Status = LenovoVariable->GetVariable (LenovoVariable, &FamilyNameGuid, &DataSize, TempBufferPtr);
    if (!EFI_ERROR (Status)) {
      if (!CompareMem(TempBufferPtr, "Xiao", 4)){
        ReturnValue = 1;  // XiaoXin project
      }else {
        ReturnValue = 0;
      }
    }else {
      //as  when can't find family name
      ReturnValue = 0;
    }
  }  
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
#if defined(S77013_SUPPORT)
//[-end-220412-Ching000044-modify]//
  //
  // Yoga or LenovoNA project Check 
  //
  // FUNCTIONAL DESCRIPTION.
  //	  Check project Yoga or LenovoNA   
  //
  // MODIFICATION HISTORY.
  //	  
  //
  // ENTRY PARAMETERS.
  //	  None.
  //
  // EXIT PARAMETERS.
  //	  Function Return: 0: Yoga	1: LenovoNA
  //
  // WARNINGS.
  //	  None.
  //
  if (!EFI_ERROR (Status)) {
    DataSize = L05_EEPROM_FAMILY_NAME_LENGTH ; 
    TempBufferPtr = AllocatePool(DataSize);
    Status = LenovoVariable->GetVariable (LenovoVariable, &FamilyNameGuid, &DataSize, TempBufferPtr);
    if (!EFI_ERROR (Status)) {
      if (!CompareMem(TempBufferPtr, "Slim", 4)){
        ReturnValue = 1;	// LenovoNA project
      }else {
        ReturnValue = 0;
      }
    }else {
      //as  when can't find family name
      ReturnValue = 0;
    }
  }
#endif
//[-end-220410-Ching000042-modify]//
//[-start-220422-Ching000046-modify]//
#if defined(S77014_SUPPORT)
  //
  // Yoga or LenovoNA or XiaoXin project Check 
  //
  // FUNCTIONAL DESCRIPTION.
  //		Check project Yoga or LenovoNA or XiaoXin
  //
  // MODIFICATION HISTORY.
  //		
  //
  // ENTRY PARAMETERS.
  //		None.
  //
  // EXIT PARAMETERS.
  //		Function Return: 0: Yoga  2: LenovoNA  1: XiaoXinIAH
  //
  // WARNINGS.
  //		None.
  //
  if (!EFI_ERROR (Status)) {
    DataSize = L05_EEPROM_FAMILY_NAME_LENGTH ; 
    TempBufferPtr = AllocatePool(DataSize);
    Status = LenovoVariable->GetVariable (LenovoVariable, &FamilyNameGuid, &DataSize, TempBufferPtr);
    if (!EFI_ERROR (Status)) {
      if (!CompareMem(TempBufferPtr, "Slim", 4)){
        ReturnValue = 2;    // LenovoNA project
      }else if (!CompareMem(TempBufferPtr, "Xiao", 4)){
        ReturnValue = 1;    // XiaoXin project
      }else {
        ReturnValue = 0;
      }
    }else {
      //as  when can't find family name
      ReturnValue = 0;
    }
  }
#endif
//[-end-220422-Ching000046-modify]//
  return ReturnValue;
}
//[-end-211223-JEPLIUT210-add]//
