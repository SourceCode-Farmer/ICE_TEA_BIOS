//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
#include <PiDxe.h>
#include <Library/PcdLib.h>
#include <LfcOemDefine.h>

//
// Do not modify this routine
//
EFI_STATUS
EFIAPI
OemSvcLfcDxeGetPlatformType (
  OUT  UINT8                           *PlatformType
  )
{
  *PlatformType = FixedPcdGet8 (PcdPlatformIntelOrAmd);

  return EFI_SUCCESS;
}


