//*****************************************************************************
// *
// * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// * And/or its affiliates. All rights reserved.
// * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// * Use is subject to license terms.
// *
// *****************************************************************************/
/*
Data          Name          Version    Description
2015.07.29    Antony.Guo    v1.00      Initial release
**/
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Lfc.h>
#include <Protocol/SmmSxDispatch2.h>
#include <LfcCmos.h>

EFI_STATUS
EFIAPI
OemSvcLfcSmmS3S4CbWbCallback (
  IN UINT8    SleepType
  )
{

  // add code here
  switch(SleepType){

    case 1:  //S1

          break;
    case 3:  //S3

          break;
    case 4:  //S4
 //[-start-211117-Dongxu0029-add]// 	
         //record flag for S4->S0 ,keep ITS mode
         IoWrite8 (LFC_CMOS_INDEX, LFC_WAKE_S4_INDEX);
         IoWrite8 (LFC_CMOS_DATA, 0x07);
 //[-end-211117-Dongxu0029-add]//   
          break;
    case 5:  //S5

          break;
    case 6:  //WB

          break;
    case 7:  //Power Button

          break;
    default:  //default

          break;

  }

  return EFI_SUCCESS;
}


