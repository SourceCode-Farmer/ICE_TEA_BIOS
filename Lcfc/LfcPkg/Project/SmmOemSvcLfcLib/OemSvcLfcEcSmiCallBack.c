//*****************************************************************************
//
//
// Copyright (c) 2012 - 2020, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*
History:
  Date          Name          Version    Change Notes
  2020.03.17    clyde.xie     v1.00      Initial release it for EDK2 projects
*/
#include <Library/IoLib.h>
#include <Library/LfcLib.h>
#include <Library/LfcEcLib.h>
#include <Library/Gpiolib.h>
//[-start-210819-YUNLEI0123-add]//
#ifdef C770_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-210819-YUNLEI0123-add]//

//[start-210801-STORM1102-modify]//
//[-start-210802-QINGLIN0006-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//#if defined(C970_SUPPORT)
//[-start-210918-Ching000002-modify]//
//[-start-210924-TAMT000012-modify]//
#if defined(C970_SUPPORT) || defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210924-TAMT000012-modify]//
//[-end-210918-Ching000002-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210802-QINGLIN0006-modify]//
//[end-210801-STORM1102-modify]//
#define GPIO_VER2_LP_GPP_H20                 0x09070014 //FN_LED
#define GPIO_VER2_LP_GPP_H22                 0x09070016 //CAPS_LED
#endif
//[-start-210802-QINGLIN0006-add]//
#if defined(S570_SUPPORT)
#define GPIO_VER2_LP_GPP_H21                 0x09070015 //NUM_LED
#endif
//[-end-210802-QINGLIN0006-add]//
//[-start-210803-QINGLIN0008-add]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210819-YUNLEI0123-modify]//
#if defined(S370_SUPPORT) || defined(C770_SUPPORT)
#define GPIO_VER2_LP_GPP_D4                  0x09080004 //NUM_LED
#endif
//[-end-210819-YUNLEI0123-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-add]//


EFI_STATUS
HangCheck (
)
{
  UINTN                      Num;
  UINT8                      Data;

  //Send command to EC for starting to light shine
  LfcEcLibStartLight();
  
  //Beep
  for (Num = 0; Num < 2; Num++) {
    Data = IoRead8 ( 0x61 );
    Data |= 0x03;
    IoWrite8 ( 0x61, Data );
    LfcProjectLibStall (200000);
    Data = IoRead8 ( 0x61 );
    Data = Data & 0xFC;
    IoWrite8 ( 0x61, Data );
    LfcProjectLibStall (200000);
  }

  //Send command to EC for stopping to light shine  
  LfcEcLibStopLight();

  //Write ExCMOS log
  IoWrite8(0x72, 0xFD);
  IoWrite8(0x73, 0xFD);

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
OemSvcLfcSmmEcSmiCallBack (
  VOID
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
  UINT8         EcData = 0;

  LfcEcLibEcRamRead(EC_SMI_DATA, &EcData);

  //Hang check function
  if (EcData & BIT0) {
    Status = HangCheck();

    EcData &= ~BIT0;
    LfcEcLibEcRamWrite (EC_SMI_DATA, EcData);
  }

//[-start-210802-QINGLIN0006-modify]//
//[-start-210731-BAIN000026-modify]//
//[start-210801-STORM1102-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//#if defined(C970_SUPPORT)
#if 0
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[end-210801-STORM1102-modify]//
  //FnLock LED light
  if (EcData & BIT1) {
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x0);
  }else{
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x1);
  }
#endif
//[-end-210731-BAIN000026-modify]//
//[-start-210918-Ching000002-modify]//
//[-start-210924-TAMT000012-A-modify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210924-TAMT000012-A-modify]//
  /*
  //FnLock LED light
  if (EcData & BIT1) {
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x1);
  }else{
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x0);
  }
  */
  //Caps Lock LED light
  if (EcData & BIT3) {
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H22,0x1);
  }else{
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H22,0x0);
  }
#endif
//[-end-210918-Ching000002-modify]//

#if defined(S570_SUPPORT)
  //Num Lock LED light
  if (EcData & BIT2) {
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H21,0x0);
  }else{
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H21,0x1);
  }
#endif

//[-start-210803-QINGLIN0008-add]//
//[-start-210802-SHAONN0003-modify]//
#if defined(S370_SUPPORT)
  //Num Lock LED light
  if (EcData & BIT2) {
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_D4,0x0);
  }else{
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_D4,0x1);
  }
#endif
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-add]//
//[-start-210819-YUNLEI0123-modify]//
#if defined(C770_SUPPORT)
  UINT8                Panel_Size;
  OemSvcLfcGetBoardID(PANEL_SIZE, &Panel_Size);
  if(Panel_Size==PANEL_SIZE_16)
  {
    //Num Lock LED light
    if (EcData & BIT2) {
      Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_D4,0x0);
    }else{
      Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_D4,0x1);
    }
  }
#endif
//[-end-210819-YUNLEI0123-modify]//

//[-start-210731-BAIN000026-modify]//
//[start-210801-STORM1102-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//#if defined(C970_SUPPORT)
//[-start-210924-TAMT000012-A-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT)
//[-end-210924-TAMT000012-A-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[end-210801-STORM1102-modify]//
  //Caps Lock LED light
  if (EcData & BIT3) {
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H22,0x0);
  }else{
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H22,0x1);
  }
#endif
//[-end-210731-BAIN000026-modify]//
//[-end-210802-QINGLIN0006-modify]//

  return Status;
}

