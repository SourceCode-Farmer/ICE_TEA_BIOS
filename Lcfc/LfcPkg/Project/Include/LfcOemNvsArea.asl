//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

// Reserve 100 byte for project use
// !!! Please make sure total size is 100 byte and sync with LfcOemNvsArea.h !!!
//[-start-210908-QINGLIN0050-modify]//
//[-start-211014-YUNLEI0142-modify]//
//[-start-211122-JEPLIUT199-modify]// 
//[-start-211214-JEPLIUT207-modify]//
//[-start-210614-JEPLIUT223-modify]//
#if defined(S370_SUPPORT)  || defined(S570_SUPPORT)
    OPTY, 8,   //Project Type  0: S170; 1:S370; 2:V141517; 3:S1415
    CPTY, 8 ,   //CPUType , 7 ==  i7,  5 == i5 , 3 == i3
    CPTD, 8 ,   //CPUType , 1 == U28,  0:U15
    CIPU, 8 ,  //CPU IPU supoort , 1 : support 0:none
//[-start-220322-QINGLIN0167-modify]//
#if defined(S370_SUPPORT) 
    SRVR, 8 ,
    RSVD, 768, 
#else
    RSVD, 768, 
#endif
//[-end-210614-JEPLIUT223-modify]//
//[-end-220322-QINGLIN0167-modify]//
//[-end-211214-JEPLIUT207-modify]//    
//[-end-211122-JEPLIUT199-modify]//	
#elif defined(C770_SUPPORT)
    PTCI, 8,   //For use PackageTdp to distinguish CPU U15/U28   U15:15 U28:28 
    NIPU, 8,   // For use PackageTdp to distinguish CPU U15/U28   U15:15 U28:28  
    RSVD, 784,
#else
    RSVD, 800,
#endif
//[-end-211014-YUNLEI0142-modify]//
//[-end-210908-QINGLIN0050-modify]//


