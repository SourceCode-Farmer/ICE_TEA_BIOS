//*****************************************************************************
//
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

// Reserve 100 byte for project use
// !!! Please make sure total size is 100 byte and sync with LfcOemNvsArea.asl !!!
//[-start-210908-QINGLIN0050-modify]//
//[-start-211014-YUNLEI0142-modify]//
//[-start-211122-JEPLIUT199-add]//
//[-start-211214-JEPLIUT207-modify]// 
//[-start-220603-JEPLIUT223-modify ]// 
#if defined(S370_SUPPORT) || defined(S570_SUPPORT)
  UINT8       OemProjectType;
  UINT8       OemCpuType;
  UINT8       OemCpuTdp;
  UINT8       IpuSupport; // distinguish CPU U15 IPU OR NOT, 1:NON-IPU , 0:IPU(Default) 
 //[-start-220322-QINGLIN0167-modify]//
#if defined(S370_SUPPORT) 
  UINT8       SuperResolutionVersion;
  UINT8       Reserved[96];
#else
  UINT8       Reserved[96];
#endif
//[-end-220603-JEPLIUT223-modify ]// 
//[-end-220322-QINGLIN0167-modify]//
//[-start-211214-JEPLIUT207-modify]//   
//[-end-211122-JEPLIUT199-add]//
#elif defined(C770_SUPPORT)
  UINT8       PTdpCpuIdentifier;// For use PackageTdp to distinguish CPU U15/U28/H45...
  UINT8       NonIpuCpuIdentifier;// distinguish CPU U15 IPU OR NOT, 1:NON-IPU , 0:IPU(Default) 
  UINT8       Reserved[98];
#else
  UINT8       Reserved[100];
#endif
//[-end-211014-YUNLEI0142-modify]//
//[-end-210908-QINGLIN0050-modify]//
