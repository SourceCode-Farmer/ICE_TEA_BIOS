//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_DXE_UPDATE_OEM_NVS_REGION_LIB_H_
#define _LFC_PROJECT_DXE_UPDATE_OEM_NVS_REGION_LIB_H_

//
// Update Lfc Oem Nvs Regin at SetLfcNvsData funciton (AcpiTableOverrideDxe.c)
//
EFI_STATUS
EFIAPI
OemSvcLfcDxeUpdateOemNvsRegion (
  IN OUT LFC_NVS_AREA  *LfcNvsBuffer
  );

#endif


