//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
#ifndef _LFC_PROJECT_DXE_LVAR_ADDRESS_LIB_H_
#define _LFC_PROJECT_DXE_LVAR_ADDRESS_LIB_H_

EFI_STATUS
EFIAPI
OemSvcLfcDxeGetLvarAddress (
  IN OUT  UINT32                         *Sub1Base,
  IN OUT  UINT32                         *Sub1Size,
  IN OUT  UINT32                         *Sub2Base,
  IN OUT  UINT32                         *Sub2Size,
  IN OUT  UINT32                         *DebugBase,
  IN OUT  UINT32                         *DebugSize
  );

#endif

