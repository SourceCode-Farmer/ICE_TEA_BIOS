//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_DXE_GET_PLATFORM_TYPE_LIB_H_
#define _LFC_PROJECT_DXE_GET_PLATFORM_TYPE_LIB_H_

#define INTEL_PLATFORM    0x01
#define AMD_PLATFORM      0x02

//
// Do not modify this routine
//
EFI_STATUS
EFIAPI
OemSvcLfcDxeGetPlatformType (
  OUT  UINT8                           *PlatformType
  );

#endif


