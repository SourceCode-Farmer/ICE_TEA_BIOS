//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_PEI_BAYHUB_CARDREADER_CFG_LIB_H_
#define _LFC_PROJECT_PEI_BAYHUB_CARDREADER_CFG_LIB_H_
#include <Library/IoLib.h>                     // For MmioWrite32
#include <Library/DebugLib.h>
#include <Library/PciExpressLib.h>

//---------------------------------------------------------------------------
// Structure Defination
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// Marco Defination
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Device Information
//---------------------------------------------------------------------------
#define BayHub_Card_Reader_VID 0x1217  // BayHub Card Reader VID
#define BayHub_Card_Reader_DID 0x8621  // BayHub Card Reader DID

//---------------------------------------------------------------------------
// Card Reader Root Bridge
//-

#define LTRMaxLatencyValueScale   0x10031003      // 3ms
#define L12ThresholdValueScale    0x406E000F      // 110us

// this function will be executed right after end of pei  (project speific code here)
EFI_STATUS
EFIAPI
CheckAndProgramBayHubCardReader (
  IN EFI_PEI_SERVICES          **PeiServices,
  UINT8   BrgBusNum,
  UINT8   BrgDevNum,
  UINT8   BrgFunNum,
  UINT8   BusNum,
  UINTN   MemBaseAddr,
  UINT16  VendorId,
  UINT16  DeviceId
  );

#endif

