//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_PEI_EARLY_PEI_LIB_H_
#define _LFC_PROJECT_PEI_EARLY_PEI_LIB_H_

// this function will be executed at early pei  (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcPeiEarly (
  VOID
  );

#endif

