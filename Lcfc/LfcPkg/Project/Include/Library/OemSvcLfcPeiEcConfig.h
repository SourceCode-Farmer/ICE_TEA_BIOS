 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _LFC_PROJECT_PEI_EC_CONFIG_LIB_H_
#define _LFC_PROJECT_PEI_EC_CONFIG_LIB_H_

EFI_STATUS
EFIAPI
OemSvcLfcPeiEcConfig (
  );

#endif
