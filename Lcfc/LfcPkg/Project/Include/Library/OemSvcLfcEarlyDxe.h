//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_DXE_EARLY_DXE_LIB_H_
#define _LFC_PROJECT_DXE_EARLY_DXE_LIB_H_

// this function will be executed at early dxe  (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcDxeEarly (
  VOID
  );

//[-start-210906-QINGLIN0043-add]//
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
#if defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
UINT8 
OemSvcLfcGetProjectType(
);
#endif
//[-end-220412-Ching000044-modify]//
//[-end-220410-Ching000042-modify]//
//[-end-210906-QINGLIN0043-add]//
#endif

