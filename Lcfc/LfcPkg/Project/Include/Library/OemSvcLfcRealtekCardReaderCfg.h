//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_PEI_REALTEK_CARDREADER_CFG_LIB_H_
#define _LFC_PROJECT_PEI_REALTEK_CARDREADER_CFG_LIB_H_
#include <Library/IoLib.h>                     // For MmioWrite32
#include <Library/DebugLib.h>
#include <Library/PciExpressLib.h>

//---------------------------------------------------------------------------
// Structure Defination
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Marco Defination
//---------------------------------------------------------------------------

#define Realtek_Card_Reader_VID 0x10EC // Realtek Card Reader VID
#define Realtek_Card_Reader_DID 0x522A // Realtek Card Reader DID

#define REALTEK_CMD_REGISTER           0xE8
#define REALTEK_SD_BUSY_BIT            0x80000000
#define REALTEK_SD_COMMAND_READY       0
#define REALTEK_SD_DURATION            0x10
#define REALTEK_SD_LOOP_TIMES          0x1000
//---------------------------------------------------------------------------
// Device Information
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// Card Reader Root Bridge
//-


// this function will be executed right after end of pei  (project speific code here)
EFI_STATUS
EFIAPI
CheckAndProgramRealtekCardReader (
  IN EFI_PEI_SERVICES          **PeiServices,
  UINT8   BrgBusNum,
  UINT8   BrgDevNum,
  UINT8   BrgFunNum,
  UINT8   BusNum,
  UINTN   MemBaseAddr,
  UINT16  VendorId,
  UINT16  DeviceId
  );

#endif

