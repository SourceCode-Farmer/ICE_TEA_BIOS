//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_PEI_CARDREADER_CFG_LIB_H_
#define _LFC_PROJECT_PEI_CARDREADER_CFG_LIB_H_
#include <Library/IoLib.h>                     // For MmioWrite32
#include <Library/DebugLib.h>
#include <Library/PciExpressLib.h>

//---------------------------------------------------------------------------
// Structure Defination
//---------------------------------------------------------------------------
typedef struct _PCI_REG_TABLE{
    UINT32  Offset;
    UINT32  AndMask;
    UINT32  OrValue;
} PCI_REG_TABLE;

//---------------------------------------------------------------------------
// Marco Defination
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Device Information
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
// Card Reader Root Bridge
//-
//[start-210915-STORM1113-modify]
#if defined(C770_SUPPORT)
#define CARDREADER_ROOTBRIDGE_BUS 0    // Card Reader Root Bridge Bus
#define CARDREADER_ROOTBRIDGE_DEV 0x1D // Card Reader Root Bridge Device
#define CARDREADER_ROOTBRIDGE_FUN 1    // Card Reader Root Bridge Function

#define LENOVO_OZ711LV2_SSVID_SSID     0x386217AA
#define LENOVO_GL9750_OIYL4_SSVID_SSID 0x380117AA
#endif
//[end-210915-STORM1113-modify]

#if defined(S370_SUPPORT)
#define CARDREADER_ROOTBRIDGE_BUS 0    // Card Reader Root Bridge Bus
#define CARDREADER_ROOTBRIDGE_DEV 0x1D // Card Reader Root Bridge Device
//[-start-210916-QINGLIN0067-modify]//
//#define CARDREADER_ROOTBRIDGE_FUN 0    // Card Reader Root Bridge Function
#define CARDREADER_ROOTBRIDGE_FUN 1    // Card Reader Root Bridge Function
//[-end-210916-QINGLIN0067-modify]//

#define LENOVO_OZ711LV2_SSVID_SSID     0x387417AA
#define LENOVO_GL9750_OIYL4_SSVID_SSID 0x384A17AA
#endif


#define LENOVO_RTS5232S_SSVID_SSID 0x384517AA


#define CARD_READER_TMP_BUS       0xF0
// this function will be executed right after end of pei  (project speific code here)

VOID*
GetPciExpressBaseAddress (
  VOID
  );

EFI_STATUS
EFIAPI
OemSvcLfcCardReaderCfg (
  IN EFI_PEI_SERVICES          **PeiServices
  );

#endif

