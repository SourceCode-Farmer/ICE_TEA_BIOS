//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_DXE_READY_TO_BOOT_LIB_H_
#define _LFC_PROJECT_DXE_READY_TO_BOOT_LIB_H_

// this function will be executed when ready to boot (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcDxeReadyToBoot (
  VOID
  );

#endif

