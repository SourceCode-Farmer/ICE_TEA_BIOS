//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************

#ifndef _LFC_PROJECT_PEI_EARLY_PEI_LIB_H_
#define _LFC_PROJECT_PEI_EARLY_PEI_LIB_H_


#define PROJECT_ID                  0x0
//[-start-210803-QINGLIN0008-add]//
#ifdef S370_SUPPORT
  #define PROJECT_ID_S170                  0x0
  #define PROJECT_ID_S370                  0x1
  #define PROJECT_ID_V141517               0x2
  #define PROJECT_ID_S1415                 0x3
#endif
//[-end-210803-QINGLIN0008-add]//
#define GPU_ID                      0x1
  #define GPU_ID_UMA_ONLY                  0x0
  #define GPU_ID_DIS_NVIDIA                0x1
  #define GPU_ID_DIS_AMD                   0x2
  #define GPU_ID_DIS_DIS                   0x3
  #define GPU_ID_DIS_INTEL                 0x4
#define MEMORY_DOWN_ID              0x2
#define TOUCH_ID                    0x3
#define FINGER_PRINT_ID             0x4
#define PANEL_ID                    0x5
#define PANEL_SIZE                  0x6
  #define PANEL_SIZE_14                    0x14
  #define PANEL_SIZE_15                    0x15
  #define PANEL_SIZE_16                    0x16
  #define PANEL_SIZE_17                    0x17



// LCFCTODO, modified the GPIO pin according project design
//e.g. OUT
//  0 -- 330G
//  1	-- Yoga530
//  2	-- Slim 530S
EFI_STATUS
EFIAPI
OemSvcLfcGetProjectID(
  OUT  UINT8    *ProjectID
);

// !!! EC & debug page use this value, required the value as below
//  GPU_ID_UMA_ONLY   -- UMA only
//  GPU_ID_DIS_NVIDIA -- DIS + Nvidia (One dGPU)
//  GPU_ID_DIS_AMD    -- DIS + AMD (One dGPUs)
//  GPU_ID_DIS_DIS    -- DIS + DIS (Two dGPUs)
//  GPU_ID_DIS_INTEL  -- DIS + Intel

EFI_STATUS
EFIAPI
OemSvcLfcGetGPUID(
  OUT  UINT8    *GPUID
);

//e.g. OUT
// 0 -- Non_MD
// 1 -- SAM4G
// 2 -- SAM8G
// 3 -- MICRON4G
// 4 -- MICRON8G
EFI_STATUS
EFIAPI
OemSvcLfcGetMemoryDownID(
  OUT  UINT8    *MemoryDownID
);

//e.g. OUT
// 0 -- Touch
// 1 -- Non_Touch
EFI_STATUS
EFIAPI
OemSvcLfcGetTouchID(
  OUT  UINT8    *TouchID
);

//e.g. OUT
// 0 -- FP
// 1 -- Non_FP
EFI_STATUS
EFIAPI
OemSvcLfcGetFingerPrintID(
  OUT  UINT8    *FingerprintID
);

//e.g. OUT
// 0 -- UHD
// 1 -- FHD
EFI_STATUS
EFIAPI
OemSvcLfcGetPanelID(
  OUT  UINT8    *PanelID
);

//e.g. OUT
// !!! EC use this value, required the value as below
// PANEL_SIZE_14 -- 14"
// PANEL_SIZE_15 -- 15"
// PANEL_SIZE_17 -- 17"

EFI_STATUS
EFIAPI
OemSvcLfcGetPanelSizeID(
  OUT  UINT8    *PanelSizeID
);

// this function will be executed at early pei  (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcGetBoardID (
  IN   UINT8    BoardIDType,
  OUT  UINT8    *MachineSize
);

#endif

