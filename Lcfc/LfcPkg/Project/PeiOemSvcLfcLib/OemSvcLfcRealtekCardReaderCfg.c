 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/
//---------------------------------------------------------------------------
// Header Files 
//---------------------------------------------------------------------------
#include <PiPei.h>
#include <Pi/PiBootMode.h>
#include <Ppi/Stall.h>
#include <Library/PeiServicesLib.h>

#include <Library/OemSvcLfcCardReaderCfg.h>
#include <Library/OemSvcLfcRealtekCardReaderCfg.h>

//---------------------------------------------------------------------------
// Variables Defination
//---------------------------------------------------------------------------

PCI_REG_TABLE RTS5232SPciRegTable[] = {
  { 0x817, 0x00000000, 0x00000080 }, //Unlock
  { 0x724, 0x00000000, 0x000000AA }, 
  { 0x725, 0x00000000, 0x000000FF }, 
  { 0x726, 0x00000000, 0x000000FF }, 
  { 0x727, 0x00000000, 0x000000EE }, //Disable L0s=bit0, Enable L1=bit1.
  { 0x814, 0x00000000, 0x00000060 }, 
  { 0x815, 0x00000000, 0x00000000 }, 
  { 0x816, 0x00000000, 0x00000004 }, 
  { 0x817, 0x00000000, 0x00000000 }, //Lock
};

//---------------------------------------------------------------------------
// Functions
//---------------------------------------------------------------------------

EFI_STATUS
EFIAPI
CheckAndProgramRealtekCardReader (
  IN EFI_PEI_SERVICES          **PeiServices,
  UINT8   BrgBusNum,
  UINT8   BrgDevNum,
  UINT8   BrgFunNum,
  UINT8   BusNum,
  UINTN   MemBaseAddr,
  UINT16  VendorId,
  UINT16  DeviceId
  )
{
  EFI_STATUS           Status;
  UINT32               Temp32;
  UINT8                Temp8;
  UINT8                Index;

  EFI_PEI_STALL_PPI       *StallPpi;

  DEBUG((DEBUG_INFO, __FUNCTION__" Entry\n"));
  //
  // Determine device by Vendor ID and Device ID.
  //
  if ((VendorId != Realtek_Card_Reader_VID)||(DeviceId != Realtek_Card_Reader_DID)) {
    return EFI_UNSUPPORTED;
  }
  Status = (**PeiServices).LocatePpi (
                             PeiServices,
                             &gEfiPeiStallPpiGuid,
                             0,
                             NULL,
                             (VOID **)&StallPpi
                             );
  if(EFI_ERROR(Status))
  {
    DEBUG((DEBUG_ERROR, "StallPpi Not Ready\n"));
    return Status;
  }

  DEBUG((DEBUG_INFO, "Config Start\n"));
  //
  // Write Value To Crc Register
  //
  for(Index = 0 ; Index < sizeof(RTS5232SPciRegTable)/sizeof(PCI_REG_TABLE) ; Index ++) {
    UINT8 i = 0;

    if(RTS5232SPciRegTable[Index].Offset == 0xFFFFFFFF) break;

    do {
      //
      // Get Current setting.
      //
      Temp8 = MmioRead8((UINTN) (MemBaseAddr + RTS5232SPciRegTable[Index].Offset));

      //
      // Prepare OEM Setting.
      //
      Temp8 = (UINT8) ((Temp8 & RTS5232SPciRegTable[Index].AndMask) | RTS5232SPciRegTable[Index].OrValue);

      //
      // Write OEM Setting.
      //
      MmioWrite8((UINTN) (MemBaseAddr + RTS5232SPciRegTable[Index].Offset), Temp8);

      //
      // Delay 100 us
      //
      StallPpi->Stall (PeiServices, StallPpi, 100);
      i ++ ;
    } while ( (MmioRead8((UINTN) (MemBaseAddr + RTS5232SPciRegTable[Index].Offset))!=Temp8) && (i<5) );
  }

  DEBUG((DEBUG_INFO, "Config SSID\n"));

  //
  // Config SSID
  //
  Temp32 = 0xFE35FF2C;
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  StallPpi->Stall (PeiServices, StallPpi, 50);

  Temp32 = 0xFE36FF00;
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  Temp32 = 0xFE37FF00 | (LENOVO_RTS5232S_SSVID_SSID&0xFF);
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  Temp32 = 0xFE38FF00 | ((LENOVO_RTS5232S_SSVID_SSID>>8)&0xFF);
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  Temp32 = 0xFE39FF00 | ((LENOVO_RTS5232S_SSVID_SSID>>16)&0xFF);
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  Temp32 = 0xFE3AFF00 | ((LENOVO_RTS5232S_SSVID_SSID>>24)&0xFF);
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  Temp32 = 0xFE3BFF8F;
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  StallPpi->Stall (PeiServices, StallPpi, 50);
  Temp32 = 0xBE3BFF00;
  MmioWrite32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER), Temp32);
  while (MmioRead32((UINTN) (MemBaseAddr + REALTEK_CMD_REGISTER)) & REALTEK_SD_BUSY_BIT);

  DEBUG((DEBUG_INFO, __FUNCTION__" End\n"));
  return EFI_SUCCESS;
}

