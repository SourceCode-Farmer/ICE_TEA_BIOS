//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*
History:
  Date          Name          Version    Change Notes
  2014.12.28    dahai.zhou    v1.00      Initial release it for EDK2 projects
*/

#include <PiPei.h>
#include <Library/GpioLib.h>
//[-start-210513-KEBIN00001-modify]//
//#include <GpioPinsCnlLp.h>
#include <Pins/GpioPinsVer2Lp.h>
//[-start-210513-KEBIN00001-modify]//
#include <Library/OemSvcLfcPeiGetBoardID.h>


// LCFCTODO, modified the GPIO pin
//e.g. OUT
//  0 -- 330G
//  1	-- Yoga530
//  2	-- Slim 530S
EFI_STATUS
EFIAPI
OemSvcLfcGetProjectID(
  OUT  UINT8    *ProjectID
)
{
  EFI_STATUS    Status;
//  UINT32        ID0;
//  UINT32        ID1;

  Status = EFI_SUCCESS;
 // GpioGetInputValue (GPIO_CNL_LP_GPP_F12, &ID0);
//  GpioGetInputValue (GPIO_CNL_LP_GPP_F13, &ID1);
 // *ProjectID = (UINT8)(( ID1 << 1 ) + ID0);
//[-start-210803-QINGLIN0008-modify]//
#ifdef S370_SUPPORT
  UINT32 BoardID14, BoardID15;
  UINT8  Data8;
  GpioGetInputValue (GPIO_VER2_LP_GPP_F17, &BoardID14);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F18, &BoardID15);
  Data8 = (UINT8)(( BoardID14 << 1 ) + BoardID15);
  if (Data8 == 0x00) {
    *ProjectID = PROJECT_ID_S170;
  } else if (Data8 == 0x01) {
    *ProjectID = PROJECT_ID_S370;
  } else if (Data8 == 0x02) {
    *ProjectID = PROJECT_ID_V141517;
  } else {
    *ProjectID = PROJECT_ID_S1415;
  }
#else
  *ProjectID = 1;
#endif
//[-end-210803-QINGLIN0008-modify]//

  return Status;
}


// LCFCTODO
//e.g. OUT
// !!! EC & debug page use this value, required the value as below
//  GPU_ID_UMA_ONLY   -- UMA only
//  GPU_ID_DIS_NVIDIA -- DIS + Nvidia (One dGPU)
//  GPU_ID_DIS_AMD    -- DIS + AMD (One dGPUs)
//  GPU_ID_DIS_DIS    -- DIS + DIS (Two dGPUs)
//  GPU_ID_DIS_INTEL  -- DIS + Intel

EFI_STATUS
EFIAPI
OemSvcLfcGetGPUID(
  OUT  UINT8    *GPUID
)
{
  EFI_STATUS    Status;
//[-start-210625-Dongxu0004-add]//  
#ifdef C970_SUPPORT
    *GPUID = GPU_ID_UMA_ONLY;
#endif
//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT

  UINT32  BoardID12;
  UINT32  BoardID13;


// baord ID, 12:13
// 00: UMA
// 01: DIS SUMSANG
// 10: DIS HYNIX
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if(0x01 == (BoardID12 ^ BoardID13 )){
    *GPUID = GPU_ID_DIS_INTEL;
  } else {
    *GPUID = GPU_ID_UMA_ONLY;
  }

#endif
//[end-210720-STORM1100-modify]//
//[-end-210625-Dongxu0004-add]//
//[-start-210721-QINGLIN0001-add]//
#ifdef S570_SUPPORT
  UINT32  BoardID12;

  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);

  if (BoardID12 == 0) {
    *GPUID = GPU_ID_UMA_ONLY;
  } else {
    *GPUID = GPU_ID_DIS_NVIDIA;
  }
#endif
//[-end-210721-QINGLIN0001-add]//

//[-start-210803-QINGLIN0008-add]//
#if defined(S370_SUPPORT)
  UINT32  BoardID12;
  UINT32  BoardID13;
  
  // baord ID, 12:13
  // 00: UMA
  // 01: DIS SUMSANG
  // 10: DIS HYNIX
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);
  
  if(0x01 == (BoardID12 ^ BoardID13 )){
    *GPUID = GPU_ID_DIS_INTEL;
  } else {
    *GPUID = GPU_ID_UMA_ONLY;
  }
#endif
//[-end-210803-QINGLIN0008-add]//
//[-start-211217-GEORGE0034-modify]//
//[-start-210830-GEORGE0001-add]//
#if defined(S77014_SUPPORT)
  UINT32  BoardID12;
  UINT32  BoardID13;

// baord ID, 12:13
// 00: UMA
// 01: DIS GN18-S5
// 10: DIS GN20-S5
// 11: DIS GN20-S7
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if((BoardID12 == 0) && (BoardID13 == 0)){
    *GPUID = GPU_ID_UMA_ONLY;
  } else {
    *GPUID = GPU_ID_DIS_NVIDIA;
  }

#endif

#if defined(S77014IAH_SUPPORT)
  UINT32  BoardID12;
  UINT32  BoardID13;

// baord ID, 12:13
// 00: H45 PCR i7 UMA
// 01: H45IAH UMA
// 10: H45IAH N18
// 11: H45IAH N20
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if(((BoardID12 == 0) && (BoardID13 == 0)) || ((BoardID12 == 0) && (BoardID13 == 1))){
    *GPUID = GPU_ID_UMA_ONLY;
  } else {
    *GPUID = GPU_ID_DIS_NVIDIA;
  }
#endif
//[-end-210830-GEORGE0001-add]//
//[-end-211217-GEORGE0034-modify]//

  Status = EFI_SUCCESS;

  return Status;
}

// LCFCTODO
//e.g. OUT
// 0 -- Non_MD
// 1 -- SAM4G
// 2 -- SAM8G
// 3 -- MICRON4G
// 4 -- MICRON8G
EFI_STATUS
EFIAPI
OemSvcLfcGetMemoryDownID(
  OUT  UINT8    *MemoryDownID
)
{
  EFI_STATUS    Status;

  Status = EFI_SUCCESS;

  return Status;
}


// LCFCTODO
//e.g. OUT
// 0 -- Touch
// 1 -- Non_Touch
EFI_STATUS
EFIAPI
OemSvcLfcGetTouchID(
  OUT  UINT8    *TouchID
)
{
  EFI_STATUS    Status;

  Status = EFI_SUCCESS;

  return Status;
}

// LCFCTODO
//e.g. OUT
// 0 -- FP
// 1 -- Non_FP
EFI_STATUS
EFIAPI
OemSvcLfcGetFingerPrintID(
  OUT  UINT8    *FingerprintID
)
{
  EFI_STATUS    Status;

  Status = EFI_SUCCESS;

  return Status;
}

// LCFCTODO
//e.g. OUT
// 0 -- UHD
// 1 -- FHD
EFI_STATUS
EFIAPI
OemSvcLfcGetPanelID(
  OUT  UINT8    *PanelID
)
{
  EFI_STATUS    Status;

  Status = EFI_SUCCESS;

  return Status;
}


// LCFCTODO
//e.g. OUT
// !!! EC use this value, required the value as below
// PANEL_SIZE_14 -- 14"
// PANEL_SIZE_15 -- 15"
// PANEL_SIZE_17 -- 17"
// PANEL_SIZE_16 -- 16"

EFI_STATUS
EFIAPI
OemSvcLfcGetPanelSizeID(
  OUT  UINT8    *PanelSizeID
)
{
  EFI_STATUS    Status;
//[-start-210726-YUNLEI0114-add]//
#ifdef C770_SUPPORT
  UINT32  BoardID5, BoardID6;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_R6,&BoardID5);  
  GpioGetInputValue(GPIO_VER2_LP_GPP_R7,&BoardID6);
  Data8 |= (UINT8)(BoardID6 << 0);
  Data8 |= (UINT8)(BoardID5 << 1);  
  if(Data8==1){
    *PanelSizeID = PANEL_SIZE_14; //14'
  }else if(Data8==2)
  {
    *PanelSizeID = PANEL_SIZE_16; //16'
  }else{
    *PanelSizeID = PANEL_SIZE_14; //14'
  }
 #endif
//[-end-210726-YUNLEI0114-add]//

//[-start-210803-QINGLIN0008-add]//
//[-start-210802-SHAONN0003-modify]//
//[-start-211108-Dennis0008-modify]//
#if defined(S370_SUPPORT)
  if(PcdGet8(Pcd14inchSku)){
    *PanelSizeID = PANEL_SIZE_14;
  } else if (PcdGet8(Pcd15inchSku)) {
    *PanelSizeID = PANEL_SIZE_15;
  } else if (PcdGet8(Pcd17inchSku)) {
    *PanelSizeID = PANEL_SIZE_17;
  } else {
    *PanelSizeID = PANEL_SIZE_14;
  }
#endif
//[-end-211108-Dennis0008-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-add]//
//[-start-210924-QINGLIN0081-add]//
#if defined(S570_SUPPORT)
  *PanelSizeID = PANEL_SIZE_15; //15'
#endif
//[-end-210924-QINGLIN0081-add]//

  Status = EFI_SUCCESS;

  return Status;
}

// ProjectID, GPUID, MemoryID, TouchID, FindgerPrintID, PanelID, PanelSize,
EFI_STATUS
EFIAPI
OemSvcLfcGetBoardID (
  IN   UINT8    BoardIDType,
  OUT  UINT8    *BoardIDValue
)
{
  EFI_STATUS    Status;

  Status = EFI_SUCCESS;

  switch (BoardIDType) {
    case PROJECT_ID:
      Status = OemSvcLfcGetProjectID (BoardIDValue);
      break;

    case GPU_ID:
      Status = OemSvcLfcGetGPUID (BoardIDValue);
      break;

    case MEMORY_DOWN_ID:
      Status = OemSvcLfcGetMemoryDownID (BoardIDValue);
      break;

    case TOUCH_ID:
      Status = OemSvcLfcGetTouchID (BoardIDValue);
      break;

    case FINGER_PRINT_ID:
      Status = OemSvcLfcGetFingerPrintID (BoardIDValue);
      break;

    case PANEL_ID:
      Status = OemSvcLfcGetPanelID (BoardIDValue);
      break;

    case PANEL_SIZE:
      Status = OemSvcLfcGetPanelSizeID (BoardIDValue);
      break;


    default:
      *BoardIDValue = 0xff;
      Status = RETURN_INVALID_PARAMETER;
      break;
    }

  return Status;
}

