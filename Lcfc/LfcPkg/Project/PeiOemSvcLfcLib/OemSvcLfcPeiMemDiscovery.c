//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*
History:
  Date          Name          Version    Change Notes
  2014.12.28    dahai.zhou    v1.00      Initial release it for EDK2 projects
*/
#include <PiPei.h>

// this function will be executed right after memory initiate  (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcPeiMemDiscovery (
  VOID
  )
{
  EFI_STATUS    Status;
  Status = TRUE;

  return EFI_SUCCESS;
}

