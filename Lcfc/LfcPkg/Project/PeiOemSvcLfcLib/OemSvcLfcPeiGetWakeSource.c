//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*
History:
  Date          Name          Version    Change Notes
  2014.12.28    dahai.zhou    v1.00      Initial release it for EDK2 projects
*/

#include <PiPei.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/IoLib.h>

#include <Lfc.h>

//#include <Library/PchInfoLib.h>
//#include <Library/PchCycleDecodingLib.h>
//#include <Ppi/H2OPeiChipsetServices.h>
//#include <Library/LfcEcLib.h>
EFI_STATUS
EFIAPI
OemSvcLfcPeiGetWakeSource (
  UINT8  *WakeSource
  )
{

#if 0
  // Sample code for Intel Kaby Lake
  // It need been checked by project owener
  UINT8                                 IsS3Flag;
  UINT16                                PmBase;
  UINT8                                 LocalWakeSource;
  PCH_SERIES                            PchSeries;
  EFI_STATUS                            Status;
  UINT8                                 SleepState;
  BOOLEAN                               NovoButtonStatus;
  H2O_CHIPSET_SERVICES_PPI              *ChipsetSvcPpi;

  LocalWakeSource = WAKE_BY_UNKNOWN;

  Status = PchAcpiBaseGet(&PmBase);
  if(EFI_ERROR(Status)) {
    return Status;
  }

  PchSeries = GetPchSeries ();

  ChipsetSvcPpi = NULL;
  Status = PeiServicesLocatePpi (
             &gH2OChipsetServicesPpiGuid,
             0,
             NULL,
             (VOID **)&ChipsetSvcPpi
             );

  if (EFI_ERROR(Status)) {
    return Status;
  }

  if (ChipsetSvcPpi == NULL ||
    ChipsetSvcPpi->Size < (OFFSET_OF(H2O_CHIPSET_SERVICES_PPI, GetSleepState) + sizeof(VOID*)) ||
    ChipsetSvcPpi->GetSleepState == NULL) {
    return EFI_UNSUPPORTED;
  }

  ChipsetSvcPpi->GetSleepState (&SleepState);
  if(SleepState == 0x03) {
    IsS3Flag = TRUE;
  } else {
    IsS3Flag = FALSE;
  }

  LfcEcLibGetNovoStatus (&NovoButtonStatus);

  // Based on EC design, waked up by PS2 Keyboard from S3 is the same as power button.
  switch (IsS3Flag) {
    case 0:
      //Check wake from Novo button
      if(NovoButtonStatus) {
        LocalWakeSource = WAKE_BY_NOVO_BTN;
        DEBUG ((EFI_D_INFO, "Wake up by Novo button from S4/S5.\n" ));
      } else {
        //Check wake from Power button
        LocalWakeSource = WAKE_BY_POWER_BTN_S4S5;
        DEBUG ((EFI_D_INFO, "Wake up by Power button from S4/S5.\n" ));
      }
      break;
    case 1:
      //Check wake from power button/PS2KB
      if((IoRead16 (PmBase) & 0x100)) {
        LocalWakeSource = WAKE_BY_POWER_BTN_S3;
        DEBUG ((EFI_D_INFO, "Wake up by Power button from S3.\n" ));
      }

      //Check wake from RTC
      if((IoRead16 (PmBase) & 0x400)) {
        LocalWakeSource = WAKE_BY_RTC_S3;
        DEBUG ((EFI_D_INFO, "Wake up by RTC from S3.\n" ));
      }

//      //Check wake from PCIE device
//      if((IoRead16 (PmBase) & 0x4000)) {
//        LocalWakeSource = WAKE_BY_PCIE_S3;
//        DEBUG ((EFI_D_INFO, "Wake up by PCEI device (LAN) from S3.\n" ));
//      }

//      //Check wake from PME# signal
//      if((PchSeries != PchLp)&&(IoRead16 (PmBase + 0x20) & 0x2000)) {
//        LocalWakeSource = WAKE_BY_PME_S3;
//        DEBUG ((EFI_D_INFO, "Wake up by PME# signal(mainly USB MS/KB) from S3.\n" ));
//      }

//      //Check wake from PME# signal
//      if((PchSeries == PchLp)&&(IoRead16 (PmBase + 0x8C) & 0x2000)) {
//        LocalWakeSource = WAKE_BY_PME_S3;
//        DEBUG ((EFI_D_INFO, "Wake up by PME# signal(mainly USB MS/KB) from S3.\n" ));
//      }

//      //Check wake from EC Wake_SCI(GPIO07) for such as LID.
//      if((IoRead16 (PmBase + 0x22) & 0x8000)) {
//        LocalWakeSource = WAKE_BY_EC_WAKE_LID;
//        DEBUG ((EFI_D_INFO, "Wake up by LID from S3.\n" ));
//      }
      break;
    default:

      break;
  }

  *WakeSource = LocalWakeSource;
#else
  *WakeSource = WAKE_BY_UNKNOWN;
#endif

  return EFI_SUCCESS;
}


