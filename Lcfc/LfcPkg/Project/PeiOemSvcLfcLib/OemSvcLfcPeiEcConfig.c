 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
#include <Uefi.h>
#include <PiPei.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PcdLib.h>
#include <ChipsetAccess.h>
#include <Library/PchPcrLib.h>
//[-start-210513-KEBIN00001-modify]//
//#include <Register/PchRegsPcr.h>
#include <Register/PchPcrRegs.h>
#include <PchBdfAssignment.h>
//[-end-210513-KEBIN00001-modify]//

#ifndef R_PCH_LPC_GEN3_DEC
  #define R_PCH_LPC_GEN3_DEC 0x8C
#endif

#ifndef R_PCH_PCR_DMI_LPCLGIR3
  #define R_PCH_PCR_DMI_LPCLGIR3 0x2738
#endif

EFI_STATUS
EFIAPI
OemSvcLfcEnable68h6ChIODecoder (
)
{

  EFI_STATUS                Status = EFI_SUCCESS;

  // [Note] 
  // Get Battery First Used Date has already porting in LfcDxe.c (LfcEcLibGetBatteryFirstUsedDate()).
  // And use 68h/6Ch IO set cmd to EC.

  // Enable 68h/6Ch IO Decoder
  PchLpcPciCfg32AndThenOr (R_PCH_LPC_GEN3_DEC, 0, 0x00040069);
  PchPcrWrite32 (PID_DMI, (UINT16) (R_PCH_PCR_DMI_LPCLGIR3),0x00040069);

  return Status;

}

EFI_STATUS
EFIAPI
OemSvcLfcEnableEcMemoryDecoder (
)
{

  EFI_STATUS                Status = EFI_SUCCESS;

  //Enable 0xFF000000 decode for EC memory.
  Status = PchBiosDecodeEnableSet (0xFFCF & 0xFFFE); // Default: 0xFFCF, mask bit0.
  ASSERT_EFI_ERROR (Status);
  if (!EFI_ERROR(Status)) {
    Status = PchLpcMemRangeSet (0xFF000000);  // Decode memory address 0xFF000000 to EC H2RAM.
    ASSERT_EFI_ERROR (Status);
    }

  return Status;
}

EFI_STATUS
EFIAPI
OemSvcLfcPeiEcConfig (
  )
{
  EFI_STATUS                Status = EFI_SUCCESS;

  // LCFCTODO: EC 0x68/0x6c decoder and EC memory map configure start
//[-start-210519-KEBIN00001-modify]//  
#ifdef LCFC_SUPPORT
    OemSvcLfcEnable68h6ChIODecoder ();  
#endif
//[-end-210519-KEBIN00001-modify]//  
#if 0
// Sample code for Intel Whiskey Lake

  OemSvcLfcEnable68h6ChIODecoder ();  
  OemSvcLfcEnableEcMemoryDecoder ();
#endif
  // LCFCTODO: EC 0x68/0x6c decoder and EC memory map configure end

  return Status;
}

