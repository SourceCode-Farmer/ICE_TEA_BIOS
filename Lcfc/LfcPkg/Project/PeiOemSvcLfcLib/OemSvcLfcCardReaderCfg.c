//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*
History:
  Date          Name          Version    Change Notes
*/
#include <PiPei.h>
#include <Pi/PiBootMode.h>
#include <Library/PeiServicesLib.h>
#include <Library/PciExpressLib.h>

#include <Library/OemSvcLfcCardReaderCfg.h>
#include <Library/OemSvcLfcBayHubCardReaderCfg.h>
#include <Library/OemSvcLfcRealtekCardReaderCfg.h>
#include <Library/OemSvcLfcGenesysLogicCardReaderCfg.h>
//[-start-211105-JAYAN00009-add]//
#ifdef C770_SUPPORT
#include <Library/CnviLib.h>
#endif
//[-end-211105-JAYAN00009-add]//
// this function will be executed right after end of pei  (project speific code here)
EFI_STATUS
EFIAPI
OemSvcLfcCardReaderCfg (
  IN EFI_PEI_SERVICES          **PeiServices
  )
{
  UINT8                BusNum = 0;
  UINT8                DevNum = 0;
  UINT8                FuncNum = 0;
  UINTN                MemBaseAddr;
  UINT16               PciVendorId;
  UINT16               PciDeviceId;
  UINT32               OrgBusNumber;
  UINT8                OrgCommandReg;

//[-start-211105-JAYAN00009-add]//
#ifdef C770_SUPPORT
  UINT8                Fun_Number;
  DEBUG((DEBUG_INFO, __FUNCTION__" Entry\n"));



  if(CnviIsPresent()){
    Fun_Number = 0;
    }else{
    Fun_Number = CARDREADER_ROOTBRIDGE_FUN;
    }

//[-start-220510-BAOBO00020-add]//
  PciVendorId = PciExpressRead16 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_VENDOR_ID_OFFSET));
  if(PciVendorId == 0xFFFF)
  {
    Fun_Number = 0;
  }
//[-end-220510-BAOBO00020-add]//
  PciVendorId = PciExpressRead16 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_VENDOR_ID_OFFSET));
  if(PciVendorId == 0xFFFF)
  {
    DEBUG((DEBUG_ERROR, "The bridge for cardreader not present\n"));
    return EFI_UNSUPPORTED;
  }
  if(PciExpressRead8(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_CLASSCODE_OFFSET+2)) != PCI_CLASS_BRIDGE)
  {
    DEBUG((DEBUG_ERROR, "The device is not bridge\n"));
    return EFI_UNSUPPORTED;
  }
  //
  // Save the original sub bus ,subordinate bus number and Command Register.
  //
  OrgBusNumber = PciExpressRead32 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET));
  OrgCommandReg = PciExpressRead8 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_COMMAND_OFFSET));

  DEBUG((DEBUG_INFO, __FUNCTION__" Entry\n"));
  //
  // Set Sec/Sub buses to 0xF0 and Command Register.
  //
  PciExpressWrite32(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET), (CARD_READER_TMP_BUS << 16) | (CARD_READER_TMP_BUS << 8));
  PciExpressOr8(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_COMMAND_OFFSET), 0x07);
  //
  // Get the PCIE Card Reader Bus number.
  //
  BusNum = PciExpressRead8(PCI_EXPRESS_LIB_ADDRESS(CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET));
  //
  // Get the PCIE Card Reader memory address.
  //
  MemBaseAddr = (UINTN) GetPciExpressBaseAddress () + PCI_EXPRESS_LIB_ADDRESS(BusNum, DevNum, FuncNum, 0);
  DEBUG((DEBUG_ERROR, "MemBaseAddr = 0x%X\n", MemBaseAddr));
  //
  // Get the PCIE Card Reader Vendor ID and Device ID.
  //
  PciVendorId = MmioRead16(MemBaseAddr);
  PciDeviceId = MmioRead16(MemBaseAddr + PCI_DEVICE_ID_OFFSET);
  DEBUG((DEBUG_INFO, "CardReader Vid = 0x%04X, Did = 0x%04X\n", PciVendorId, PciDeviceId));
  //
  // Determine device by Vendor ID and Device ID.
  //
  if (PciVendorId != 0xFFFF) {
#if defined(BAYHUB_CARD_READER_SUPPORT)
    CheckAndProgramBayHubCardReader(  PeiServices,
                                      CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, 
                                      BusNum, MemBaseAddr, PciVendorId, PciDeviceId
                                    );
#endif
#if defined(REALTEK_CARD_READER_SUPPORT)
    CheckAndProgramRealtekCardReader( PeiServices,
                                      CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, 
                                      BusNum, MemBaseAddr, PciVendorId, PciDeviceId
                                    );
#endif
#if defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
    CheckAndProgramGenesysLogicCardReader( PeiServices,
                                           CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, 
                                           BusNum, MemBaseAddr, PciVendorId, PciDeviceId
                                         );
#endif
  }

  //
  // Restore the original sub bus and suordinate bus number and command register
  //
  PciExpressWrite32(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET), OrgBusNumber);
  PciExpressWrite8(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, Fun_Number, PCI_COMMAND_OFFSET), OrgCommandReg);
#else

  PciVendorId = PciExpressRead16 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_VENDOR_ID_OFFSET));
  if(PciVendorId == 0xFFFF)
  {
    DEBUG((DEBUG_ERROR, "The bridge for cardreader not present\n"));
    return EFI_UNSUPPORTED;
  }
  if(PciExpressRead8(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_CLASSCODE_OFFSET+2)) != PCI_CLASS_BRIDGE)
  {
    DEBUG((DEBUG_ERROR, "The device is not bridge\n"));
    return EFI_UNSUPPORTED;
  }
  //
  // Save the original sub bus ,subordinate bus number and Command Register.
  //
  OrgBusNumber = PciExpressRead32 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET));
  OrgCommandReg = PciExpressRead8 (PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_COMMAND_OFFSET));

  //
  // Set Sec/Sub buses to 0xF0 and Command Register.
  //
  PciExpressWrite32(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET), (CARD_READER_TMP_BUS << 16) | (CARD_READER_TMP_BUS << 8));
  PciExpressOr8(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_COMMAND_OFFSET), 0x07);

  //
  // Get the PCIE Card Reader Bus number.
  //
  BusNum = PciExpressRead8(PCI_EXPRESS_LIB_ADDRESS(CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET));

  //
  // Get the PCIE Card Reader memory address.
  //
  MemBaseAddr = (UINTN) GetPciExpressBaseAddress () + PCI_EXPRESS_LIB_ADDRESS(BusNum, DevNum, FuncNum, 0);
  DEBUG((DEBUG_ERROR, "MemBaseAddr = 0x%X\n", MemBaseAddr));
  
  //
  // Get the PCIE Card Reader Vendor ID and Device ID.
  //
  PciVendorId = MmioRead16(MemBaseAddr);
  PciDeviceId = MmioRead16(MemBaseAddr + PCI_DEVICE_ID_OFFSET);
  DEBUG((DEBUG_INFO, "CardReader Vid = 0x%04X, Did = 0x%04X\n", PciVendorId, PciDeviceId));

  //
  // Determine device by Vendor ID and Device ID.
  //
  if (PciVendorId != 0xFFFF) {
#if defined(BAYHUB_CARD_READER_SUPPORT)
    CheckAndProgramBayHubCardReader(  PeiServices,
                                      CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, 
                                      BusNum, MemBaseAddr, PciVendorId, PciDeviceId
                                    );
#endif
#if defined(REALTEK_CARD_READER_SUPPORT)
    CheckAndProgramRealtekCardReader( PeiServices,
                                      CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, 
                                      BusNum, MemBaseAddr, PciVendorId, PciDeviceId
                                    );
#endif
#if defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
    CheckAndProgramGenesysLogicCardReader( PeiServices,
                                           CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, 
                                           BusNum, MemBaseAddr, PciVendorId, PciDeviceId
                                         );
#endif
  }

  //
  // Restore the original sub bus and suordinate bus number and command register
  //
  PciExpressWrite32(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET), OrgBusNumber);
  PciExpressWrite8(PCI_EXPRESS_LIB_ADDRESS (CARDREADER_ROOTBRIDGE_BUS, CARDREADER_ROOTBRIDGE_DEV, CARDREADER_ROOTBRIDGE_FUN, PCI_COMMAND_OFFSET), OrgCommandReg);
#endif
  return EFI_SUCCESS;
}

