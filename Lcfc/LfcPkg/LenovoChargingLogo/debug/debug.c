/** @file
  Copyright (c) - 2014, Lenovo. All rights reserved.<BR>
  This program and the accompanying materials                          
  are licensed and made available under the terms and conditions of the BSD License         
  which accompanies this distribution.  The full text of the license may be found at        
  http://opensource.org/licenses/bsd-license.php                                            

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,                     
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.             

**/


#include "debug.h"

#define DEBUG_LOG_START    0


static UINTN    mMaxRow = 0;
static UINTN    mCurrRow = 0;
static BOOLEAN  mDebugFlag = TRUE;


BOOLEAN
IsDebugEnable()
{
	return mDebugFlag;
}


EFI_STATUS
DebugEnable(
BOOLEAN bEnable
)
{
	mDebugFlag = bEnable;
	return EFI_SUCCESS;
}

#if LENOVOCHARINGLOGO_DEBUG 
/**
Debug Print Override
**/
UINTN
EFIAPI
DebugL(
IN  CONST CHAR16  *FormatString,
...
)
{
	CHAR16 StartOfBuffer[MAX_MESSAGE_SIZE];

	VA_LIST Marker;
	UINTN   NumberOfPrinted;

	if (!mDebugFlag)
		return 0;

	VA_START(Marker, FormatString);
	NumberOfPrinted = UnicodeVSPrint(StartOfBuffer, MAX_MESSAGE_SIZE, FormatString, Marker);
	VA_END(Marker);

#if 0
	gST->ConOut->OutputString(gST->ConOut, StartOfBuffer);
#else
	if (0 == mMaxRow){
		UINTN      Columns;

		gST->ConOut->QueryMode(
			gST->ConOut,
			gST->ConOut->Mode->Mode,
			&Columns,
			&mMaxRow
			);
		mCurrRow = DEBUG_LOG_START;
	}
	gST->ConOut->SetCursorPosition(gST->ConOut, 0, mCurrRow);
	gST->ConOut->OutputString(gST->ConOut, StartOfBuffer);
	gST->ConOut->OutputString(gST->ConOut, L"                                                                                       ");

	mCurrRow++;
	if (mCurrRow >= mMaxRow - 1) {
		mCurrRow = DEBUG_LOG_START;
	}
#endif  

	return NumberOfPrinted;
}

#endif
