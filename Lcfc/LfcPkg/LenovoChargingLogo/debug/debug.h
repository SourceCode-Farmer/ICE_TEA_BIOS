/** @file
  Copyright (c) - 2014, Lenovo. All rights reserved.<BR>
  This program and the accompanying materials                          
  are licensed and made available under the terms and conditions of the BSD License         
  which accompanies this distribution.  The full text of the license may be found at        
  http://opensource.org/licenses/bsd-license.php                                            

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,                     
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.             

**/
#include <Uefi.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/BlockIo.h>


#define LENOVOCHARINGLOGO_DEBUG				  0

#define MAX_MESSAGE_SIZE		512

EFI_STATUS
DebugEnable(BOOLEAN enable);

BOOLEAN
IsDebugEnable();

#if LENOVOCHARINGLOGO_DEBUG 
UINTN
EFIAPI
DebugL(
IN  CONST CHAR16  *FormatString,
...
);
#else
#define DebugL(a,...) NULL  
#endif

#ifdef ENABLE_TRACKING
EFI_STATUS 
DebugF(
IN  CONST CHAR16  *FormatString,
...
);

#else
#define DebugF(a,...) do {} while (0)  
#endif


