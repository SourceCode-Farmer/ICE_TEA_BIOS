/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _TRUETYPE_LIB_H
#define _TRUETYPE_LIB_H

#include <Uefi.h>


/**
 Init freetype2 library

 @param None

 @retval EFI_SUCCESS            Init success
 @retval EFI_LOAD_ERROR         Init TrueType library fail

**/
EFI_STATUS 
TrueTypeInit (
  VOID
  );

/**
 Load TTF file from memory, and get fontid to access

 @param [in]   Buffer           TTF file buffer
 @param [in]   BufferSize

 @retval EFI_SUCCESS            Load TTF file success
 @retval EFI_LOAD_ERROR         Load TTF file fail
 @retval EFI_NOT_READY          TrueType doesn't init

**/
EFI_STATUS 
TrueTypeLoadMemory (
  IN  UINT8      *Buffer, 
  IN  UINT32     BufferSize
  );

/**
 Set font size before load glyph

 @param [in]   FontSize         Font Height

 @retval EFI_SUCCESS            Set font size success

**/
EFI_STATUS 
TrueTypeSetFontSize (
  IN  UINT16    FontSize
  );

/**
 load glyph data

 @param [in]   CharValue        Cahr value by unicode
 @param [in]   GlyphBuffer      GlyphBuffer must have value, if *GlyphBuffer is NULL,
                                then auto allcoate memory for glyph
 @param [in, out] GlyphBufferSize  If *GlyphBuffer Size,
 @param [out]  Cell             Cell information

 @retval EFI_SUCCESS            Set font size success
 @retval EFI_NOT_READY          TrueType doesn't init

**/
EFI_STATUS 
TrueTypeGetGlyph (
  IN     CHAR16             CharValue, 
  IN     UINT8              *GlyphBuffer,   
  IN OUT UINTN              *GlyphBufferSize,
  OUT    EFI_HII_GLYPH_INFO *Cell  OPTIONAL
  );
  
#endif 
