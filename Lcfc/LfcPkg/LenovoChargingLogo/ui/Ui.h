//
// FILENAME.
//      Meta.h
//      -----------------------------------------------------------------------
//
// NOTICE.
//      Copyright (C) 2014 Lenovo   All Rights Reserved.
//

#ifndef _BOKR_UI_H
#define _BOKR_UI_H

//
// Standard header files included by modules in this driver.
//


#include <Uefi.h>
#include <PiDxe.h>

#include <Protocol/FirmwareVolume2.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>

#include "debug/debug.h"



//How to build BOKR module
//build -a X64  -b RELEASE -m D:\Work\BIOS\vienna-m\AIUU1\LenovoPkg\BOKR\BOKR.inf
//

#define BATT_HULL 10
#define BATT_GAP   4
#define BATT_CAP_LENGTH  30
#define BATT_CAP_HALF_WIDTH 40

#define BATT_LEFT_SIDE_SCALE  1/3
#define BATT_RIGHT_SIDE_SCALE 2/3
#define BATT_TOP_SIDE_SCALE 1/2
#define BATT_BOTTOM_SIDE_SCALE 7/10

#define AC_BATT_INFO_FONT_SIZE 50

#define PERCENT_FONT_SIZE 100
#define PERCENT_FONT_TOP_SIDE_SCALE 2/10
#define PERCENT_FONT_BOTTOM_SIDE_SCALE 5/10






// EFI Console Colours
//
#define EFI_BLACK                 0x00
#define EFI_BLUE                  0x01
#define EFI_GREEN                 0x02
#define EFI_CYAN                  (EFI_BLUE | EFI_GREEN)
#define EFI_RED                   0x04
#define EFI_MAGENTA               (EFI_BLUE | EFI_RED)
#define EFI_BROWN                 (EFI_GREEN | EFI_RED)
#define EFI_LIGHTGRAY             (EFI_BLUE | EFI_GREEN | EFI_RED)
#define EFI_BRIGHT                0x08
#define EFI_DARKGRAY              (EFI_BRIGHT)
#define EFI_LIGHTBLUE             (EFI_BLUE | EFI_BRIGHT)
#define EFI_LIGHTGREEN            (EFI_GREEN | EFI_BRIGHT)
#define EFI_LIGHTCYAN             (EFI_CYAN | EFI_BRIGHT)
#define EFI_LIGHTRED              (EFI_RED | EFI_BRIGHT)
#define EFI_LIGHTMAGENTA          (EFI_MAGENTA | EFI_BRIGHT)
#define EFI_YELLOW                (EFI_BROWN | EFI_BRIGHT)
#define EFI_WHITE                 (EFI_BLUE | EFI_GREEN | EFI_RED | EFI_BRIGHT)
#define EFI_ORANGE              (EFI_BROWN)



//
//Global handle
//

//
//Backup filename length
//
#define BACKUP_FILENAME_LENGTH         18
#define MAX_STRING_LENGTH              256
#define PASSWORD_LENGTH         			 16



//
//Global colors define
#define WHITE_COLOR                   0xFFFFFF  
#define BLACK_COLOR                   0x040404  
#define LIGHTRED_COLOR              0xFF0000
#define RED_COLOR                     0xAA1D14
#define LIGHTGRAY_COLOR               0xF4F2F2
#define GRAY_COLOR                    0xEDEBEC
#define BLUE_COLOR                    0x0068B7
#define GREEN_COLOR                    0x00FF00
#define ERROR_COLOR                   0xEB6100
#define ORANGE_COLOR                0xFFA000


//
//Background size and colors define
//
#define DEFAULT_X                     1366   
#define DEFAULT_Y                     768   

#define P1080P_X                      1920   
#define P1080P_Y                      1080  

#define FRONT_STYLE_LEFT_TOP          1
#define FRONT_STYLE_LEFT_BOTTOM       2
#define FRONT_STYLE_LEFT_CENTER       3

#define FRONT_STYLE_MIDLLE_TOP        4
#define FRONT_STYLE_MIDLLE_BOTTOM     5
#define FRONT_STYLE_MIDLLE_CENTER     6

#define FRONT_STYLE_RIGHT_TOP         7
#define FRONT_STYLE_RIGHT_BOTTOM      8
#define FRONT_STYLE_RIGHT_CENTER      9

#define FRONT_STYLE_DISK_INFO         10

//
//UI Page define 0-6 is full screen page
//
typedef enum {
  IMAGE_NONE,
  IMAGE_RETURN,
  IMAGE_RETURN_SELECTED,
  IMAGE_EXTERNAL_DISK,
  IMAGE_EXTERNAL_DISK_SELECTED,
  IMAGE_LOCAL_DISK,
  IMAGE_LOCAL_DISK_SELECTED,
  IMAGE_FILE,
  IMAGE_FILE_SELECTED,
  IMAGE_LEVEL1_BUTTON,
  IMAGE_LEVEL1_BUTTON_SELECTED,
  IMAGE_LEVEL2_BUTTON,
  IMAGE_LEVEL2_BUTTON_SELECTED,
  BOKR_UI_IMAGE_MAX   
} BOKR_UI_IMAGE_TYPE;



#pragma pack(1)

typedef struct _RECT {
  UINTN                                        left;
  UINTN                                        top;
  UINTN                                        right;
  UINTN                                        bottom;
} RECT;

typedef struct {
  UINTN                                        x;
  UINTN                                        y;
} POINT;


#pragma pack()


BOOLEAN
EFIAPI
SetRect(
IN OUT RECT                                  *Rect,
IN     UINTN                                 Left,
IN     UINTN                                 Top,
IN     UINTN                                 Right,
IN     UINTN                                 Bottom
);

EFI_STATUS
InitializeGUI();

EFI_STATUS
DrawBatteryInfo(UINTN Percent,UINT8 ACStatus,UINT8 BattStatus);

EFI_STATUS
DrawBattery(UINTN Percent);

#endif // 
