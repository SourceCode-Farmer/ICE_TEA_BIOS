/*++
Copyright 2014, Lenovo Corporation
All rights reserved. This program and the accompanying materials
are licensed and made available under the terms and conditions of the BSD License
which accompanies this distribution.  The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

++*/

#ifndef _LENOVO_CHARGING_LOGO_H
#define _LENOVO_CHARGING_LOGO_H

#include <Protocol/BlockIo.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <IndustryStandard/Bmp.h>

#include <Uefi.h>

#include <Protocol/SimpleFileSystem.h>
#include <Protocol/BlockIo.h>
#include <Protocol/DevicePath.h>
#include <Protocol/DebugPort.h>
#include <Protocol/LoadFile.h>

#include <Library/DevicePathLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>

#include <Guid/FileSystemVolumeLabelInfo.h>
#include <Guid/ImageAuthentication.h>
#include <Guid/FileInfo.h>
#include <Guid/FileSystemInfo.h>

#include <IndustryStandard/Mbr.h>

/****************************/
#include <Uefi/UefiSpec.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

/****************************/

#include "debug/debug.h"

#define PRODUCT_YOGA 0
#define DISPLAY_TIME_SECONDS 3

EFI_STATUS
InitializeGUI(
);

EFI_STATUS
DrawBatteryInfo(UINTN Percent,UINT8 ACStatus,UINT8 BattStatus);

EFI_STATUS
DrawBattery(UINTN Percent);

EFI_STATUS
GetBatteryPercentage (
  IN OUT  UINT8                             *BatteryPercentage
  );

EFI_STATUS
GetACStatus (
  UINT8 * ACStatus
  );

EFI_STATUS
GetBattStatus (
  UINT8 * BattStatus
  );

#endif


