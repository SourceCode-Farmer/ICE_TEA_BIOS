
#ifndef _LENOVO_BATT_H_
#define _LENOVO_BATT_H_
#include <Uefi.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>

#define KEY_CMD_STATE                     0x64
#define KBC_TIME_OUT                      0x10000
#define KEY_OBF                           1
#define KEY_IBF                           2

#define KBC_DATA                          0x60
#define KBC_CMD_STATE                     0x64

#define EC_DATA                           0x62
#define EC_CMD_STATE                      0x66

#define EC_DATA2                           0x68
#define EC_CMD_STATE2                      0x6C

#define EC_READ_ECRAM_CMD                 0x80

/*AC Status.*/
typedef enum {
  AC_NON_EXIST,
  AC_EXIST,
  MAX_AC_STATUS
} AC_STATUS;

/*Battery Status.*/
typedef enum {
  BATT_NON_EXIST,
  BATT_ABNORMAL,
  BATT_CHARGING,
  BATT_FULL_CHARGED,
  BATT_DISCHARGING,
  BATT_FULL_DISCHARGED,
  BATT_UNKNOWN_STATUS,
  MAX_BATTERY_STATUS
} BATTERY_STATUS;

#endif
