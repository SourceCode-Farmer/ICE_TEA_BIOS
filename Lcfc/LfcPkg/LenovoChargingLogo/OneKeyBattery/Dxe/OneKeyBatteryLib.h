/** @file
;******************************************************************************
;* Copyright (c) 2016, Lenovo Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_ONE_KEY_BATTERY_LIB_H
#define _EFI_ONE_KEY_BATTERY_LIB_H

EFI_STATUS
InitializeGUI(VOID);

EFI_STATUS
DrawBattery(UINTN Percent, BOOLEAN ACStatus);

EFI_STATUS
DrawCharger(BOOLEAN DynamicPicture);

EFI_STATUS
DrawAOUDevice(BOOLEAN DynamicPicture);

#endif
