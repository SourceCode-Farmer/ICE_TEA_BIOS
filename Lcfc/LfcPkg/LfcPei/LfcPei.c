/*****************************************************************************
*
* Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
* And/or its affiliates. All rights reserved. 
* Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
* Use is subject to license terms.
* 
*****************************************************************************/  
//;------------------------------------------------------------------------------ 
/*++
Abstract:
  This driver is running at early pei
 
History:
  Date          Name          Version    Change Notes
  2015.07.17    Antony Guo    v1.00      first release 

Module Name:
  LfcPei.c
--*/
//;
//; Notice:  
//; 
#include <Uefi.h>
#include <PiPei.h>
#include <Pi/PiBootMode.h>
#include <Library/PeiServicesLib.h>
#include <Library/IoLib.h>
#include <Ppi/MemoryDiscovered.h>
#include <Library/DebugLib.h>
#include <Library/LfcEcLib.h>
#include <Lfc.h>
#include <Ppi/MasterBootMode.h>
#include <Library/OemSvcLfcPeiEarlyPei.h>
#include <Library/OemSvcLfcPeiMemDiscovery.h>
#include <Library/OemSvcLfcPeiEcConfig.h>
//[-start-210909-QINGLIN0056-add]//
#if defined(C770_SUPPORT)
#include <Library/OemSvcLfcCardReaderCfg.h>
#include <Library/OemSvcLfcBayHubCardReaderCfg.h>
#include <Library/OemSvcLfcRealtekCardReaderCfg.h>
#include <Library/OemSvcLfcGenesysLogicCardReaderCfg.h>
#endif
//[-end-210909-QINGLIN0056-add]//

EFI_STATUS
EFIAPI
LfcAfterMemInitCallback (
  IN CONST EFI_PEI_SERVICES                 **PeiServices,
  IN       EFI_PEI_NOTIFY_DESCRIPTOR        *NotifyDescriptor,
  IN       VOID                             *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR       mNotifyDesc[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gEfiPeiMemoryDiscoveredPpiGuid,
    LfcAfterMemInitCallback
  }
};

EFI_STATUS
EFIAPI
LfcBootModeCallback (
  IN CONST EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDescriptor,
  IN VOID                      *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR lfcbootmodNotifyDesc[] = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMasterBootModePpiGuid,
  LfcBootModeCallback
};

//[-start-210909-QINGLIN0056-add]//
#if defined(C770_SUPPORT)
EFI_STATUS
EFIAPI
LfcPeiEndOfPeiCallback (
  IN EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDesc,
  IN VOID                      *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR LfcPeiEndOfPeiNotifyDesc = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  LfcPeiEndOfPeiCallback
};
#endif
//[-end-210909-QINGLIN0056-add]//

EFI_STATUS
EFIAPI
LfcPeiEntryPoint (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
  EFI_STATUS                   Status;  

//[-start-211222-JOYID00009-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  Status = LfcEcLibEnableWatchDogForMemoryTrainning();
  ASSERT_EFI_ERROR (Status);
#endif
//[-end-211222-JOYID00009-add]//

  Status = OemSvcLfcPeiEarly ();
  ASSERT_EFI_ERROR (Status);

  Status = OemSvcLfcPeiEcConfig ();
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesNotifyPpi (mNotifyDesc);
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesNotifyPpi (lfcbootmodNotifyDesc);
  ASSERT_EFI_ERROR (Status);

//[-start-210909-QINGLIN0056-add]//
#if defined(C770_SUPPORT)
  Status = PeiServicesNotifyPpi (&LfcPeiEndOfPeiNotifyDesc);
#endif
//[-end-210909-QINGLIN0056-add]//

  return EFI_SUCCESS;
}


EFI_STATUS
EFIAPI
LfcAfterMemInitCallback (
  IN CONST EFI_PEI_SERVICES                 **PeiServices,
  IN       EFI_PEI_NOTIFY_DESCRIPTOR        *NotifyDescriptor,
  IN       VOID                             *Ppi
  )
{

  EFI_STATUS                        Status;
  Status = OemSvcLfcPeiMemDiscovery ();  

  return Status;
}

EFI_STATUS
EFIAPI
LfcBootModeCallback (
  IN CONST EFI_PEI_SERVICES                 **PeiServices,
  IN       EFI_PEI_NOTIFY_DESCRIPTOR        *NotifyDescriptor,
  IN       VOID                             *Ppi
  )
{

  EFI_BOOT_MODE              BootMode;
  EFI_STATUS                 Status;

  Status = PeiServicesGetBootMode (&BootMode);

  if ((Status == EFI_SUCCESS) && (BootMode != BOOT_ON_S3_RESUME)) {	
    LfcEcLibEnableEcPower();
  }
  return EFI_SUCCESS;	
}

//[-start-210909-QINGLIN0056-add]//
#if defined(C770_SUPPORT)
EFI_STATUS
EFIAPI
LfcPeiEndOfPeiCallback (
  IN EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDesc,
  IN VOID                      *Ppi
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
#if defined(BAYHUB_CARD_READER_SUPPORT) || defined(REALTEK_CARD_READER_SUPPORT) || defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
  Status = OemSvcLfcCardReaderCfg (PeiServices);
#endif

  return Status;	
}
#endif
//[-end-210909-QINGLIN0056-add]//