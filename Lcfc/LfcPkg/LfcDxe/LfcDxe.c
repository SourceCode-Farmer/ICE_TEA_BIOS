#include <Protocol/ResetNotification.h>//*****************************************************************************
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
// Project Dxe Common code here
/*
Data          Name          Version    Description
2015.07.17    Antony.Guo    v1.00      First Release
2016.01.29    Jesse.Zheng   V1.01      Add enable ec acpi mode and battery first use data.						                                     
*/

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/UefiLib.h>
#include <Uefi/UefiSpec.h>
#include <IndustryStandard/Acpi10.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Guid/EventGroup.h>
#include <Guid/LfcVariableGuid.h>
#include <Lfc.h>
#include <Library/LfcEcLib.h>
#include <Library/OemSvcLfcEarlyDxe.h>
#include <Library/OemSvcLfcExitBootServices.h>
#include <Library/OemSvcLfcReadyToBoot.h>
//[-start-210705-CISSIE0651-add]//LFC UUID,follow new LfcPkg
#include <Library/LfcLib.h>
//[-end-210705-CISSIE0651-add]//
//[-start-211223-Dongxu0036-add]//
#include <Protocol/ResetNotification.h>
//[-end-211223-Dongxu0036-add]//
//[-start-220407-JEPLIUT219-add]//
#ifdef S570_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-220407-JEPLIUT219-add]//

//common Project ready to boot code here
VOID
LfcReadyToBootDxeCallback (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
/*++
Routine Description:
   Lcfc ready to boot call back function
   
Arguments:
  Event - Event that caused the call
  Context- Context at which event was caused
Returns:
  None
--*/
{
  EFI_STATUS                    Status;

  // Notify EC, enable acpi mode
  //Status = LfcEcLibEcAcpiMode(TRUE);
  gBS->CloseEvent (Event);

  LfcEcLibDisableEcPower();

  gBS->RestoreTPL (TPL_APPLICATION);
  
  Status = OemSvcLfcDxeReadyToBoot ();

  //IoWrite8 (0x70, 0x6A);
  //IoWrite8 (0x71, 0x11);
//[-start-220407-JEPLIUT219-add]//
#if defined(S570_SUPPORT)
 {
  #define                    MSR_IA32_MISC_ENABLE                     0x000001A0
  UINT8                      FlagAdapter65WAc = 0;
  UINT8                      UmaAndDisType = 0;
  UINT64                    Ia32MiscEnable;
  LfcEcLibGetAdapter65WAcOnlyMode (&FlagAdapter65WAc); 
 
  OemSvcLfcGetBoardID(GPU_ID, &UmaAndDisType);
    
  if ( FlagAdapter65WAc  && (UmaAndDisType != GPU_ID_UMA_ONLY) ) {
   Ia32MiscEnable = AsmReadMsr64 (MSR_IA32_MISC_ENABLE);
   Ia32MiscEnable|= BIT38;  // disable Turbo Mode.
   AsmWriteMsr64 (MSR_IA32_MISC_ENABLE, Ia32MiscEnable); 
  }
 }
  #endif
//[-end-220407-JEPLIUT219-add]//

  gBS->RaiseTPL (TPL_CALLBACK);  
 
}


VOID
EFIAPI
LfcExitBootServicesCallback (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  )
{
  EFI_STATUS                    Status;

  Status = OemSvcLfcExitBootServices ();

}

//[-start-211223-Dongxu0036-add]//

/**
   This function do some things before system reset.

   @param[in] ResetType   - Type of reset to perform.
   @param[in] ResetStatus - Status code of the reset.
   @param[in] DataSize    - Size of ResetData, in bytes.
   @param[in] ResetData   - Optional Null-terminated string which can be used to introduce a platform specific reset.
**/
VOID
EFIAPI
LfcPlatformResetSystem (
  IN EFI_RESET_TYPE           ResetType,
  IN EFI_STATUS               ResetStatus,
  IN UINTN                    DataSize,
  IN VOID                     *ResetData OPTIONAL
  )
{
  DEBUG ((DEBUG_INFO, "LfcPlatformResetSystem entry - ResetType = %x\n", ResetType));

  if (ResetType != EfiResetCold) {
    return;
  }

  LfcEcLibEnableEcPower ();//call gRT->ResetSystem will do it ,cold/warm ResetType

}

/**
  This function hooks system reset to allow performing correct reset.

  @param[in] Event   - A pointer to the Event that triggered the callback.
  @param[in] Context - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
ResetSystemHandlerInstall (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  )
{
  EFI_STATUS                        Status;
  EFI_RESET_NOTIFICATION_PROTOCOL   *ResetNotify;

  DEBUG ((DEBUG_INFO, "ResetSystemHandlerInstall entry\n"));
  Status = gBS->LocateProtocol (
                  &gEfiResetNotificationProtocolGuid,
                  NULL,
                  (VOID **) &ResetNotify
                  );

  if (!EFI_ERROR (Status)) {
    ResetNotify->RegisterResetNotify (ResetNotify, LfcPlatformResetSystem);
    DEBUG ((DEBUG_INFO, "LfcPlatformResetSystem() registered\n"));
    gBS->CloseEvent (Event);
  }
  DEBUG ((DEBUG_INFO, "ResetSystemHandlerInstall exit\n"));
}

/**
  Register event callback for platform reset .
**/
VOID
RegisterResetSystemCallback (
  VOID
  )
{
  VOID      *Registration;

  DEBUG ((DEBUG_INFO, "Register reset hook for gRT->ResetSystem\n"));
  EfiCreateProtocolNotifyEvent (
    &gEfiResetNotificationProtocolGuid,
    TPL_CALLBACK,
    ResetSystemHandlerInstall,
    NULL,
    &Registration
    );

}

//[-end-211223-Dongxu0036-add]//

EFI_STATUS
EFIAPI
LfcDxeEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS        Status;
  EFI_EVENT         Event;
  EFI_EVENT         ExitBSEvent;
  
//[-start-210705-CISSIE0651-add]//LFC UUID,follow new LfcPkg
#if OPTION_MBV_LCFC_UUID
  *(UINT8*)((UINTN)(EC_SRAM_BASE+EC_MBV_UUID_FLAG)) |= BIT5;  //EC poll MbvUid Flag, if set, MbvUid from EC EEPROM will be written to EC RAM immediately
#endif
//[-end-210705-CISSIE0651-add]//


  //IoWrite8 (0x70, 0x69);
  //IoWrite8 (0x71, 0x11); 
  
  

//[-start-200327-CLYDE00001-modify]//
//Remove RTC battery, RTC time not ready, will do this porting in RemoveRtcBatteryDxe.c
#if (FixedPcdGetBool(PcdRemoveRtcBatterySupport) == 0)
  // Add battery first use data
  Status = LfcEcLibGetBatteryFirstUsedDate();
#endif
//[-end-200327-CLYDE00001-modify]//

  DEBUG ((DEBUG_INFO, "Register LfcReadyToBootDxe Callback\n"));

  Status = OemSvcLfcDxeEarly ();
//[-start-211223-Dongxu0036-add]//
  RegisterResetSystemCallback();
//[-end-211223-Dongxu0036-add]//
  Status = EfiCreateEventReadyToBootEx (
            TPL_CALLBACK,
            LfcReadyToBootDxeCallback,
            NULL,
            &Event
            );

  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  LfcExitBootServicesCallback,
                  NULL,
                  &gEfiEventExitBootServicesGuid,
                  &ExitBSEvent
                  );

  return Status;
}
