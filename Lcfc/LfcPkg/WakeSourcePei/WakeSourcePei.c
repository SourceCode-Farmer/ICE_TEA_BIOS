 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/  
//;------------------------------------------------------------------------------ 
//; Module Name:
//;   WakeSourcePei.c
//; Abstract: 
//;   A LFC fuction to Save wake source into CMOS function.
//;
//; Notice:  
//;   The LPC I/O decode must be ready before we access. 
//;   So this module must be added into the build.dsc between PlatformStage1.inf and PlatformStage2.inf.
//; 
  
#include <Library/DebugLib.h>
#include <Library/LfcEcLib.h>
#include <Lfc.h>
#include <Library/OemSvcLfcPeiGetWakeSource.h>

EFI_STATUS
EFIAPI
WakeSourcePeiEntryPoint (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  UINT8                                 WakeSource;
//[-start-220224-TAMT000046-add]//
#ifndef S77013_SUPPORT
  BOOLEAN                               NovoButtonStatus;
#endif
//[-end-220224-TAMT000046-add]//
  EFI_STATUS                            Status;
  
  DEBUG ((EFI_D_INFO, "\n!!!LFC Wake Source function start!!!\n"));      
            
  //
  //Get wake source current data.
  //
  IoWrite8 (LFC_CMOS_INDEX, LFC_WAKE_SRC_INDEX);
  WakeSource = IoRead8 (LFC_CMOS_DATA);	

  DEBUG ((EFI_D_INFO, "WakeSource current value: %x\n", WakeSource));

  //
  //Back up the data for OEM debug page.
  //
  IoWrite8 (LFC_CMOS_INDEX, LFC_LAST_WAKE_SRC_INDEX);
  IoWrite8 (LFC_CMOS_DATA, WakeSource);
  
  //
  //Get Novo Button Pressed or not from EC.
  //If Novo Button is Pressed, NovoButtonStatus will be set as 0.
  //
//[-start-220224-TAMT000046-add]//
#ifndef S77013_SUPPORT
  Status = LfcEcLibGetNovoStatus (&NovoButtonStatus);
  if (EFI_ERROR (Status)) {
    NovoButtonStatus = FALSE;
  }

  DEBUG ((EFI_D_INFO, "Novo button status: %x\n", NovoButtonStatus ));

  if (NovoButtonStatus == TRUE) {  
    DEBUG ((EFI_D_INFO, "Novo button had been pressed\n"));
  }
#endif
//[-end-220224-TAMT000046-add]//
  {
    Status = OemSvcLfcPeiGetWakeSource (&WakeSource);
    DEBUG ((EFI_D_INFO, "WakeSource end value: %x\n", WakeSource ));
  }

  IoWrite8 (LFC_CMOS_INDEX, LFC_WAKE_SRC_INDEX);
  IoWrite8 (LFC_CMOS_DATA, WakeSource);
  
  return EFI_SUCCESS;
}