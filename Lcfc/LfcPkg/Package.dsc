#*****************************************************************************
#
#
# Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
# And/or its affiliates. All rights reserved.
# Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
# Use is subject to license terms.
#
#******************************************************************************
## @file
#  LFC Package
#
##

[Defines]
  !include LfcPkg/Project/OemConfig.env


[PcdsFixedAtBuild]
  # LCFCTODO
  gEfiLfcPkgTokenSpaceGuid.PcdPlatformIntelOrAmd|1

  # No need modify, always always TRUE which used at facotry
  gEfiLfcPkgTokenSpaceGuid.PcdChgBootDxeHookFlag|TRUE
  
  # LCFCTODO, If support Optane Memory, please enable this flag.
  gEfiLfcPkgTokenSpaceGuid.PcdOptaneMemorySupportFlag|FALSE
  
  # LCFCTODO
  gEfiLfcPkgTokenSpaceGuid.PcdGetI2cTPVidFlag|TRUE

  # LCFCTODO
#[-start-210920-Ching000003-modify]#
#[-start-210930-YUNLEI0141-modify]
#[-start-211015-Ching000014-modify]#
#[-start-220127-Dennis0013-modify]#
#[-start-220209-OWENWU0037-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(C970_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  gEfiLfcPkgTokenSpaceGuid.PcdI2cPtpWufuSupport|TRUE
!else
  gEfiLfcPkgTokenSpaceGuid.PcdI2cPtpWufuSupport|FALSE
!endif
#[-end-220209-OWENWU0037-modify]#
#[-end-220127-Dennis0013-modify]#
#[-end-211015-Ching000014-modify]#
#[-end-210930-YUNLEI0141-modify]
#[-end-210920-Ching000003-modify]#

#[-start-200327-CLYDE00001-add]#
  # LCFCTODO
  # Need modify, to enable Remove RTC Battery function
  gEfiLfcPkgTokenSpaceGuid.PcdRemoveRtcBatterySupport|FALSE
#[-end-200327-CLYDE00001-add]#

[PcdsDynamicDefault]
  # No need modify, value is changed automatically by env variable
  gEfiLfcPkgTokenSpaceGuid.PcdLcfcGoldenBiosEnable|$(LCFC_GOLDEN_BIOS_ENABLE)

[LibraryClasses.common]
  LfcEcLib|LfcPkg/Library/BaseLfcEcLib/EcLib.inf
  LfcLib|LfcPkg/Library/BaseLfcLib/BaseLfcLib.inf
  TrueTypeLib|LfcPkg/LenovoChargingLogo/ui/library/TrueTypeLib/TrueTypeLib.inf

  # LFC Project lib
  LfcProjectPeiLib|LfcPkg/Project/PeiOemSvcLfcLib/PeiOemSvcLfcLib.inf
#[-start-210731-BAIN000026-modify]#
#  OemSvcLfcS3S4CBWBCallBackLib|LfcPkg/Project/SmmOemSvcLfcLib/SmmOemSvcLfcLib.inf
  SmmOemSvcLfcLib|LfcPkg/Project/SmmOemSvcLfcLib/SmmOemSvcLfcLib.inf
#[-end-210731-BAIN000026-modify]#
  DxeOemSvcLfcLib|LfcPkg/Project/DxeOemSvcLfcLib/DxeOemSvcLfcLib.inf

  UuidLib|LfcPkg/Library/LfcUuidLib/LfcUuidLib.inf

[LibraryClasses.common.DXE_DRIVER]
  DxeLfcFlashDeviceLib|LfcPkg/Library/DxeLfcFlashDeviceLib/DxeLfcFlashDeviceLib.inf
#[-start-210731-BAIN000026-modify]#
#  OemSvcLfcS3S4CBWBCallBackLib|LfcPkg/Project/SmmOemSvcLfcLib/SmmOemSvcLfcLib.inf
  SmmOemSvcLfcLib|LfcPkg/Project/SmmOemSvcLfcLib/SmmOemSvcLfcLib.inf
#[-end-210731-BAIN000026-modify]#
  DxeOemSvcLfcLib|LfcPkg/Project/DxeOemSvcLfcLib/DxeOemSvcLfcLib.inf

[LibraryClasses.common.DXE_SMM_DRIVER]
  LfcSwSmiCpuRegisterLib|LfcPkg/Library/SmmLfcSwSmiCpuRegisterLib/SmmLfcSwSmiCpuRegisterLib.inf
  SmmLfcFlashDeviceLib|LfcPkg/Library/SmmLfcFlashDeviceLib/SmmLfcFlashDeviceLib.inf
#[-start-210731-BAIN000026-modify]#
#  OemSvcLfcS3S4CBWBCallBackLib|LfcPkg/Project/SmmOemSvcLfcLib/SmmOemSvcLfcLib.inf
  SmmOemSvcLfcLib|LfcPkg/Project/SmmOemSvcLfcLib/SmmOemSvcLfcLib.inf
#[-end-210731-BAIN000026-modify]#
  DxeOemSvcLfcLib|LfcPkg/Project/DxeOemSvcLfcLib/DxeOemSvcLfcLib.inf

[Components.IA32]
  LfcPkg/LfcPei/LfcPei.inf
  LfcPkg/WakeSourcePei/WakeSourcePei.inf
!if gEfiLfcPkgTokenSpaceGuid.PcdOptaneMemorySupportFlag == TRUE
  LfcPkg/OptaneMemorySupport/CheckOptaneMemoryExistPei/CheckOptaneMemoryExistPei.inf
!endif

[Components.X64]
!if $(MBV_LCFC_UUID) == YES
  LfcPkg/LfcMbvUid/LfcMbvUid.inf
  LfcPkg/LfcSmbiosTable/LfcSmbiosTable.inf
!endif
!if gEfiLfcPkgTokenSpaceGuid.PcdOptaneMemorySupportFlag == TRUE
  LfcPkg/OptaneMemorySupport/OptaneMemoryExistChgToRAID/OptaneMemoryExistChgToRAID.inf
!endif
  LfcPkg/LenovoVariable/LenovoVariableDxe/LenovoVariableDxe.inf
  LfcPkg/LfcDxe/LfcDxe.inf
  LfcPkg/LenovoVariable/LenovoVariableSmm/LenovoVariableSmm.inf
  LfcPkg/LfcSmm/LfcSmm.inf
  LfcPkg/ChgBootSmm/ChgBootSmm.inf
!if gEfiLfcPkgTokenSpaceGuid.PcdChgBootDxeHookFlag == TRUE
  LfcPkg/ChgBootDxeHook/ChgBootDxeHook.inf
!endif
!if gEfiLfcPkgTokenSpaceGuid.PcdGetI2cTPVidFlag == TRUE
  LfcPkg/I2cTPGetVidDxe/I2cTPGetVidDxe.inf
  !if gEfiLfcPkgTokenSpaceGuid.PcdI2cPtpWufuSupport == TRUE
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ALPS/AlpsCapsuleTPDxe.inf
#[-start-210930-YUNLEI0141-modify]
#[-start-210920-Ching000003-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SB974A_22H0/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/SYNA/3652_002/SynaCapsuleTPDxe.inf
!endif
#[-end-210920-Ching000003-modify]#
!if ($(C770_SUPPORT_ENABLE) == YES) OR ($(C970_SUPPORT_ENABLE) == YES)  
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SD479A_31H0/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/SYNA/3776_001/SynaCapsuleTPDxe.inf
!endif 
#[-end-210930-YUNLEI0141-modify]
#[-start-211015-Ching000014-modify]#
!if $(S77013_SUPPORT_ENABLE) == YES
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SA469A_22HF/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/SYNA/3390_003/SynaCapsuleTPDxe.inf
!endif
#[-end-211015-Ching000014-modify]#
  !endif
#[-start-220127-Dennis0013-add]#
#[-start-220209-OWENWU0037-add]#
#[-start-220224-Dennis0015-add]#
!if ($(S370_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE) == YES)  
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SA62D_22H4/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SA469D_22HR/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SA462D_22H5/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/ELAN/SA469D_22HT/ElanCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/SYNA/3628_001/SynaCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/SYNA/3629_002/SynaCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/CIRQUE/TM104069_1P_02/CirqueCapsuleTPDxe.inf
    LfcPkg/I2cTPGetVidDxe/LfcI2cTPCapsule/FOCAL/FMA4693PFL/FocalCapsuleTPDxe.inf
!endif 
#[-end-220224-Dennis0015-add]#
#[-end-220209-OWENWU0037-add]#
#[-end-220127-Dennis0013-add]#
!endif

  # Always disable it since Insyde L05 have a better implematation, please use L05
  #LfcPkg/LenovoChargingLogo/LenovoChargingLogo.inf
  LfcPkg/AcpiTableOverrideDxe/AcpiTableOverrideDxe.inf
  LfcPkg/EcFlashSmm/EcFlashSmm.inf
  LfcPkg/DebugPageDxe/DebugPageDxe.inf
#[-start-200327-CLYDE00001-add]#
  !if gEfiLfcPkgTokenSpaceGuid.PcdRemoveRtcBatterySupport == TRUE
    LfcPkg/RemoveRtcBattery/RemoveRtcBatteryDxe/RemoveRtcBatteryDxe.inf
    LfcPkg/RemoveRtcBattery/RemoveRtcBatterySmm/RemoveRtcBatterySmm.inf
  !endif
#[-end-200327-CLYDE00001-add]#
  LfcPkg/LfcWmi/Smm/LfcWmiServiceSmm.inf

!if gEfiLfcPkgTokenSpaceGuid.PcdPlatformIntelOrAmd == 1
  # For Insyde + Intel
  #LfcPkg/HangCheckSmm/Intel/HangCheck.inf
!elseif gEfiLfcPkgTokenSpaceGuid.PcdPlatformIntelOrAmd == 2
  # For Insyde + AMD
  #LfcPkg/HangCheckSmm/Amd/HangCheck.inf
!endif

  #LfcPkg/EcFlashSmm/Shell/EcFlashShell.inf
[Components]

[Components.$(PEI_ARCH)]

[Components.$(DXE_ARCH)]  
!if $(L05_MODERN_PRELOAD_SUPPORT) == YES
  LfcPkg/ModernPreload/ModernPreloadDxe/ModernPreloadDxe.inf
  LfcPkg/ModernPreload/ModernPreloadSmm/ModernPreloadSmm.inf
!endif
