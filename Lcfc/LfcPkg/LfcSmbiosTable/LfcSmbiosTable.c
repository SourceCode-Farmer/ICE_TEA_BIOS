//*****************************************************************************
//
// Copyright (c) 2021 - 2022, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
// Project Dxe Common code here
/*
Data          Name          Version    Description
2021.01.04    Feng.Gu       v1.00      First Release
*/
#include <LfcSmbiosTable.h>
#include <Protocol/LenovoVariable.h>

LENOVO_VARIABLE_PROTOCOL      *gLenovoVariable = NULL;
EFI_SMBIOS_PROTOCOL           *ABSmbiosProtocol=NULL;

//EFI_GUID LfcUuid = { 0x3fca54f6, 0xe1a2, 0x4b20, { 0xbe, 0x76, 0x92, 0x6b, 0x4b, 0x48, 0xbf, 0xaa }};
EFI_GUID gLfcMbvUid = { 0x00000000, 0x0000, 0x0000, { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }};


SMBIOS_TABLE_DATA (SMBIOS_TABLE_TYPE222, Type222Handle) = {
  {TYPE222, sizeof (EFI_SMBIOS_TABLE_HEADER), 0x0000}, // Header field.
};



SMBIOS_TABLE_TYPE222_Other_Member TYPE222MemberOtherlist[] = {
  {
  &gLfcMbvUid,
  sizeof(EFI_GUID)
  },
  {NULL,0}
};

SMBIOS_TABLE_TYPE222_String_Member TYPE222MemberStringlist[] = {
  {
    "LCFC",  //signature
  }
  ,
   {NULL}
};


EFI_STATUS
EFIAPI
LfcSmbiosEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{

  EFI_STATUS        Status=EFI_SUCCESS;

  UINTN                   StringSize;
  EFI_SMBIOS_TABLE_HEADER *Data = NULL;
  EFI_SMBIOS_HANDLE       SmbiosHandle;
  UINT8     *TableEnd;
  UINT8     Index;
  UINT8 CountofStrings;
  UINT32      DataSize = sizeof(EFI_GUID);
  
  // Locate Smbios protocol
  Status = gBS->LocateProtocol (&gEfiSmbiosProtocolGuid, NULL, (VOID **) &ABSmbiosProtocol);
  if(EFI_ERROR(Status))
  {
    return Status;
  }
  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &gLenovoVariable);
  if(EFI_ERROR(Status))
  {
    return Status;
  }
  Status = gLenovoVariable->GetVariable (
                                        gLenovoVariable,
                                        &gLfcMbvUidGuid,
                                        &DataSize,
                                        &gLfcMbvUid
                                      );
  if(EFI_ERROR(Status))
  {
    return Status;
  }
  // DEBUG((DEBUG_INFO,"SMBIOS Version: %x.%x\n\n", ABSmbiosProtocol->MajorVersion, ABSmbiosProtocol->MinorVersion);

  //
  // Allocate one page memory buffer to construct Smbios struct type.
  //
  Data = (EFI_SMBIOS_TABLE_HEADER*)AllocateZeroPool (EFI_PAGE_SIZE);
  if (Data == NULL) {
    //DEBUG(DEBUG_INFO,"Allocate memory buffer fail.\n");
    return EFI_UNSUPPORTED;
  }

  //Type22 : {Type,Length,Handle,},1,2,3...,UUIDvalue,othervalue1,othervalue2,..."LCFC",,"String2","String3"...
        
  // Construct SMBIOS Type 222 Header without String-Set.
  //
  CountofStrings =  (sizeof(TYPE222MemberStringlist )/sizeof(SMBIOS_TABLE_TYPE222_String_Member))-1;
      
  CopyMem (Data, &Type222HandleData, sizeof (EFI_SMBIOS_TABLE_HEADER));
  Data->Length += CountofStrings;
  TableEnd= (UINT8 *)((UINTN)Data + sizeof (EFI_SMBIOS_TABLE_HEADER));

  // Construct SMBIOS Type 222  Structure.
  //Part 1:string index
  for (Index = 1; Index <= CountofStrings ; Index++) {
    CopyMem (TableEnd, &Index, sizeof(Index));
    TableEnd = (UINT8 *)((UINTN)TableEnd + 1);
  }
  
  // Construct SMBIOS Type 222  Structure.
    //Part 2:values
  for (Index=0; TYPE222MemberOtherlist[Index].Value != NULL; Index++) {
    CopyMem (TableEnd, TYPE222MemberOtherlist[Index].Value, TYPE222MemberOtherlist[Index].SizeofValue);
    TableEnd = (UINT8 *)((UINTN)TableEnd + TYPE222MemberOtherlist[Index].SizeofValue);
    Data->Length += TYPE222MemberOtherlist[Index].SizeofValue;
  }

  // Construct SMBIOS Type 222  strings.
  for (Index=0; TYPE222MemberStringlist[Index].String != NULL; Index++) {
    StringSize = AsciiStrSize(TYPE222MemberStringlist[Index].String);
//[-start-210705-CISSIE0651-modify]//AsciiStrnCpy can not be used anymore for secure reasion.
    //AsciiStrnCpy (TableEnd, TYPE222MemberStringlist[Index].String, StringSize);
    gBS->CopyMem (TableEnd, TYPE222MemberStringlist[Index].String, StringSize);
//[-end-210705-CISSIE0651-modify]//
    TableEnd = (UINT8 *)((UINTN)TableEnd + StringSize);
  }
  
  //
  // Add a new SMBIOS type.
  //
  SmbiosHandle = 0xfffe;
  Status = ABSmbiosProtocol->Add (ABSmbiosProtocol, NULL, &SmbiosHandle, Data);
  FreePool (Data);
 
  return Status;
}
