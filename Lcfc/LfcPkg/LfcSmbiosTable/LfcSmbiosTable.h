 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/

#ifndef _LFC_SMBIOS_TABLE_H_
#define _LFC_SMBIOS_TABLE_H_

#include <Protocol/Smbios.h>
#include <IndustryStandard/Smbios.h>

#include <Uefi/UefiBaseType.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>

#if 0
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/UefiLib.h>
#include <Uefi/UefiSpec.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Guid/EventGroup.h>
#include <Guid/LfcVariableGuid.h>
#include <Lfc.h>
#include <Library/LfcEcLib.h>
#include <Library/OemSvcLfcDxe.h>
#include <Library/PrintLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/LfcLib.h>
#endif
#define TYPE222                       0xDE
#define SIGNATURE_SIZE                 5


typedef struct _SMBIOS_TABLE_TYPE222 {
  EFI_SMBIOS_TABLE_HEADER Hdr;
  
} SMBIOS_TABLE_TYPE222, *PSMBIOS_TABLE_TYPE222;

typedef struct _SMBIOS_TABLE_TYPE222_String_Member {
  VOID *String;
} SMBIOS_TABLE_TYPE222_String_Member, *PSMBIOS_TABLE_TYPE222_String_Member;

typedef struct _SMBIOS_TABLE_TYPE222_Other_Member {
  VOID *Value;
  UINT8 SizeofValue;
} SMBIOS_TABLE_TYPE222_Other_Member, *PSMBIOS_TABLE_TYPE222_Other_Member;


//
// Global definition macros.
//

#define SMBIOS_TABLE_DATA(NAME1, NAME2) \
NAME1 NAME2 ## Data


#endif
