 //*****************************************************************************
 // //
 // Copyright (c) 2012 - 2017, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 /*++
Abstract:
  This driver is used BIOS Hang Check feature. It will beep twice and blink power LED, when novo button 
  is pressed for longer than 4s, while the unit is software hang.
 
History:
  Date              Name                        Version        Description
  2017.06.06        Mandy Xiao/Annie Wei        v1.00          Initial release

Module Name:
  HangCheck.c
 --*/
#include <Uefi.h>
#include <Library/LfcLib.h>
#include <Library/LfcEcLib.h>
#include <Library/DebugLib.h>
#include <Protocol/FchSmmGpiDispatch2.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/OemSvcLfcGetEcSmiGpioPad.h>

EFI_STATUS
EFIAPI
HangCheckHandler (
   IN EFI_HANDLE                             DispatchHandle,
   IN CONST EFI_SMM_GPI_REGISTER_CONTEXT     *GpiRegisterContext
)
{
  UINTN                      Num;
  UINT8                      Data;

  //Send command to EC for starting to light shine
  LfcEcLibStartLight();
  
  //Beep
  for (Num = 0; Num < 2; Num++) {
     Data = IoRead8 ( 0x61 );
     Data |= 0x03;
     IoWrite8 ( 0x61, Data );
     LfcProjectLibStall (200000);
  
     Data = IoRead8 ( 0x61 );
     Data = Data & 0xFC;
     IoWrite8 ( 0x61, Data );
     LfcProjectLibStall (200000);
  }

  //Send command to EC for stopping to light shine  
  LfcEcLibStopLight();
  
  return EFI_SUCCESS;
}

EFI_STATUS 
HangCheckEntry(
   IN EFI_HANDLE               ImageHandle,
   IN EFI_SYSTEM_TABLE         *SystemTable
  )
{
  EFI_STATUS                                 Status;
  FCH_SMM_GPI_DISPATCH2_PROTOCOL             *mSmmGpiDispatch;
  EFI_SMM_GPI_REGISTER_CONTEXT               GpiRegisterContext;
  EFI_HANDLE                                 GpiHandle;  
  UINT32                                     LfcEcSmiGpioPad;

  // Get GpioNum value for EC SMI#
  OemSvcLfcGetEcSmiGpioPad(&LfcEcSmiGpioPad);
  GpiRegisterContext.GpiNum = LfcEcSmiGpioPad;

  //
  // Register SMI handler EC SMI usage during Connected Standby
  //
  Status = gSmst->SmmLocateProtocol (&gFchSmmGpiDispatch2ProtocolGuid, NULL, &mSmmGpiDispatch);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  DEBUG ((DEBUG_ERROR, "Installing HangCheckHandler Handler \n"));
  Status = mSmmGpiDispatch->Register (
                              mSmmGpiDispatch,
                              (EFI_SMM_HANDLER_ENTRY_POINT2)HangCheckHandler,
                              &GpiRegisterContext,
                              &GpiHandle
                              );   
  DEBUG ((DEBUG_ERROR, "HangCheckHandler Install Status: %x\n", Status)); 

  return Status;
}
