//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************

#include <PiDxe.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/AcpiTable.h>
#include <Protocol/AcpiSupport.h>
#include <Industrystandard/Acpi30.h>
#include <Industrystandard/Acpi20.h>
#include <Protocol/EdidDiscovered.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
//[-start-211014-YUNLEI0142-add]//
#if defined(C770_SUPPORT)
#include <Library/CpuInfoFruLib.h>
#include <Register/CommonMsr.h>
#endif
//[-end-211014-YUNLEI0142-add]//
//[-start-211122-JEPLIUT199-add]//
#include <Library/BaseLib.h>
//[-end-211122-JEPLIUT199-add]//

//[-start-211208-JEPLIUT202-modify]//  
//[-start-220613-JEPLIUT223-modify]//
#if defined(S570_SUPPORT)
#include <Library/GpioLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/CpuInfoFruLib.h>
#endif
//[-end-220613-JEPLIUT223-modify]//
//[-end-211208-JEPLIUT202-modify]//  

#include <Lfc.h>
#include <Library/LfcEcLib.h>
#include <Protocol/LfcNvsArea.h>
#include <Protocol/LenovoVariable.h>
#include <Library/OemSvcLfcUpdateOemNvsRegion.h>
#include <Library/OemSvcLfcPeiGetBoardID.h>
//[-start-211021-QINGLIN0102-add]//
//[-start-211027-YULU0001-modify]//
#if defined(S370_SUPPORT) || defined(C770_SUPPORT)
#include <SetupVariable.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#endif
//[-end-211027-YULU0001-modify]//
//[-end-211021-QINGLIN0102-add]//

#include <LfcCmos.h>
#include <Library/IoLib.h>
//[-start-220323-QINGLIN0167-add]//
#if defined(S370_SUPPORT)
#include <L05Config.h>

#define MAX_BRANDSTRING_SIZE   49
#define CPUID_BRAND_STRING1                     0x80000002
#define CPUID_BRAND_STRING2                     0x80000003
#define CPUID_BRAND_STRING3                     0x80000004

typedef union {
  ///
  /// 4 ASCII characters of Processor Brand String
  ///
  CHAR8   BrandString[4];
  ///
  /// All fields as a 32-bit value
  ///
  UINT32  Uint32;
} CPUID_BRAND_STRING_DATA;
#endif
//[-end-220323-QINGLIN0167-add]//

#define AML_OPREGION_OP  0x80

LFC_NVS_AREA_PROTOCOL              mLfcNvsAreaProtocol;
EFI_ACPI_TABLE_INSTALL_ACPI_TABLE  mOrgInstallAcpiTable;
EFI_ACPI_SET_ACPI_TABLE            mOrgSetAcpiTable;

//[-start-211021-QINGLIN0102-add]//
//[-start-211027-YULU0001-modify]//
#if defined(S370_SUPPORT) || defined(C770_SUPPORT)
EFI_STATUS
EFIAPI
OemSvcLfcNotifyEcReadDgpuTemperature (
  IN UINT8 UmaAndDisType
  )
{
  EFI_STATUS                Status = EFI_SUCCESS;
  UINTN                     SaBufferSize;
  SA_SETUP                  SaSetup;
  UINT8                     EcData = 0;

  if (UmaAndDisType != GPU_ID_UMA_ONLY) { //DIS SKU
    LfcEcLibEcRamRead (0xBD, &EcData);
    EcData &= ~(BIT6 | BIT7); //clear BIT6 and BIT7 firstly

    SaBufferSize = sizeof (SA_SETUP);
    Status = gRT->GetVariable (
                    SA_SETUP_VARIABLE_NAME,
                    &gSaSetupVariableGuid,
                    NULL,
                    &SaBufferSize,
                    (VOID *)&SaSetup
                    );
    if (!EFI_ERROR (Status)) {
      if (SaSetup.PrimaryDisplay == 4) {//switchable
//[-start-220120-QINGLIN0149-modify]//
//#if defined(S370_SUPPORT)
//        EcData |= BIT7; //BIT7 BIT6:10
//#elif defined(C770_SUPPORT)
        EcData |= (BIT6|BIT7); //BIT7 BIT6:11
//#endif
//[-end-220120-QINGLIN0149-modify]//
      }
    }
    LfcEcLibEcRamWrite(0xBD, EcData);
  }

  return Status;
}
#endif
//[-end-211027-YULU0001-modify]//
//[-start-211123-JEPLIUT199-add]//  
/*----------------------------------------------------------------
@Brief  : OemSvcLfcGetCpuBrand
@Descrip:   Get CPUType brand , Alder lake
@Input  : UINT8 *  CpuType
@Output : UINT8 * CpuType = 7:i7, 5:i5,3:i3;
--------------------------------------------------------------- -*/
EFI_STATUS
EFIAPI
OemSvcLfcGetCpuBrand(
  IN OUT  UINT8    *CpuType
)
{
  EFI_STATUS    Status;
  UINT32                               RegEAX;
  UINT32                               RegEBX;
  UINT32                               RegECX;
  UINT32                               RegEDX;

  AsmCpuid (0x80000003, &RegEAX, &RegEBX, &RegECX, &RegEDX);
  *CpuType = ((UINT8 ) RegEDX ) & 0x0f ;
  Status = EFI_SUCCESS;
   return Status;
}
//[-end-211123-JEPLIUT199-add]//  
//[-start-220613-JEPLIUT223-add ]//
//@retval FALSE = IPU is fused off. TRUE = IPU is supported by Sillicon.
#if defined(S570_SUPPORT) 
BOOLEAN
IsIpuSupported (
  VOID
  )
{
  UINT32               Data32;
  ///
  /// Check if Silicon is IPU capable
  ///
  Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_CAPID0_B));
  if (Data32 & BIT31) {
    DEBUG ((DEBUG_INFO, "IPU Fused off\n"));
    return FALSE;
  }
  
  DEBUG ((DEBUG_INFO, "IPU is supported by Silicon\n"));
  return TRUE;
}
#endif
//[-end-220613-JEPLIUT223-add ]//

#if defined(C770_SUPPORT)
/**
  IsIpuSupported: Check if IPU is supported or not by Silicon.

  @retval FALSE = IPU is fused off. TRUE = IPU is supported by Sillicon.
**/
VOID
OemSvcLfcGetNonIpuCpuIdentifier (
  OUT UINT8 *LfcNonCpuId
  )
{
  UINT32               Data32;
  EFI_STATUS           Status;
  Status = EFI_SUCCESS;
  ///
  /// Check if Silicon is IPU capable
  ///
  Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_CAPID0_B));
  if (Data32 & BIT31) {
    DEBUG ((DEBUG_INFO, "IPU Fused off\n"));
	*LfcNonCpuId = 1;
    return;
  }

  DEBUG ((DEBUG_INFO, "IPU is supported by Silicon\n"));
  *LfcNonCpuId = 2;
  return;
}
#endif

VOID
EFIAPI
LfcGetEdidCallBack (
  IN EFI_EVENT                           Event,
  IN VOID                                *Context
  )
{
  EFI_STATUS                            Status = EFI_SUCCESS;
  EFI_EDID_DISCOVERED_PROTOCOL          *EdidDiscovered;

  Status = gBS->LocateProtocol (&gEfiEdidDiscoveredProtocolGuid, NULL, &EdidDiscovered);
  if (EdidDiscovered->Edid == NULL) {
    return;
  }

  CopyMem ((mLfcNvsAreaProtocol.Area)->EdidBuffer, EdidDiscovered->Edid, 128);

//[-start-211208-yulu0004-modify]//
//[-start-211202-Dongxu0034-add]//
//[-start-211210-TAMT000037-add]//
//if OLED ,tell EC not close pannel when lid close on setup menu ,just temporary workaround
//[-start-220307-YUNLEI0163-modify]//
#if defined(C970_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)

    UINT8                    EcData = 0;

    //ATNA40YK01
  if (((mLfcNvsAreaProtocol.Area)->EdidBuffer[8] == 0X4C) && 
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[9] == 0x83) &&
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[10] == 0X52) && 
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[11] ==0x41)) { 
      Status = LfcEcLibEcRamRead (0xFB, &EcData);
      if (EFI_ERROR (Status)) {
        return;
      } 
      EcData |= BIT0;
      Status = LfcEcLibEcRamWrite(0xFB, EcData);
  }
    //ATNA40YN01
  if (((mLfcNvsAreaProtocol.Area)->EdidBuffer[8] == 0X4C) && 
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[9] == 0x83) &&
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[10] == 0X5A) && 
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[11] ==0x41)) { 
      Status = LfcEcLibEcRamRead (0xFB, &EcData);
      if (EFI_ERROR (Status)) {
        return;
      }       
      EcData |= BIT0;
      Status = LfcEcLibEcRamWrite(0xFB, EcData);     
  }

#elif defined(C770_SUPPORT) 
    UINT8                 EcData = 0;

//ATNA40YK08
    if (((mLfcNvsAreaProtocol.Area)->EdidBuffer[8] == 0X4C) && 
       ((mLfcNvsAreaProtocol.Area)->EdidBuffer[9] == 0x83) &&
       ((mLfcNvsAreaProtocol.Area)->EdidBuffer[10] == 0X7A) && 
       ((mLfcNvsAreaProtocol.Area)->EdidBuffer[11] ==0x41)) { 
       Status = LfcEcLibEcRamRead (0xFB, &EcData);
      if (EFI_ERROR (Status)) {
        return;
      } 
     EcData |= BIT0;
     Status = LfcEcLibEcRamWrite(0xFB, EcData);
   }
//ATNA40YK01
  if (((mLfcNvsAreaProtocol.Area)->EdidBuffer[8] == 0X4C) && 
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[9] == 0x83) &&
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[10] == 0X52) && 
      ((mLfcNvsAreaProtocol.Area)->EdidBuffer[11] ==0x41)) { 
      Status = LfcEcLibEcRamRead (0xFB, &EcData);
      if (EFI_ERROR (Status)) {
        return;
      } 
      EcData |= BIT0;
      Status = LfcEcLibEcRamWrite(0xFB, EcData);
  }
#endif
//[-end-220307-YUNLEI0163-modify]//
//[-end-211210-TAMT000037-add]//
//[-end-211202-Dongxu0034-add]//
//[-end-211208-yulu0004-modify]//

  return;
}

EFI_STATUS
LfcGetEdid (
	UINT8  *Buffer
	)
{
  EFI_STATUS                  Status;
  EFI_EVENT                   Event;
  VOID                        *Registration;

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  LfcGetEdidCallBack,
                  NULL,
                  &Event
                  );

  if (!EFI_ERROR (Status)) {
    Status = gBS->RegisterProtocolNotify (
                    &gEfiEdidDiscoveredProtocolGuid,
                    Event,
                    &Registration
                    );
  }

  return Status;
}

EFI_STATUS
LfcGetKbIdAndNotifyEc (
  UINT8  *KeyboardId
  )
{
  EFI_STATUS                   Status;
  EFI_GUID                     KbIdDataGuid = LVAR_KEYBOARD_ID_GUID;
  UINT32                       DataSize = sizeof (KeyboardId);
  LENOVO_VARIABLE_PROTOCOL     *LenovoVariable = NULL;

  // Get KB ID
  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable);
  if (EFI_ERROR(Status)) {
    return EFI_UNSUPPORTED;
  }
  
  ASSERT_EFI_ERROR (Status);

  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &KbIdDataGuid,
                             &DataSize,
                             KeyboardId
                             );
  if (EFI_ERROR(Status)) {
//[-start-220415-SHAONN0032-modify]//
    *KeyboardId = 0x00;
//[-end-220415-SHAONN0032-modify]//
  }

  // Notify EC the keyboard ID is Brazil or non-Brazil
  LfcEcLibNotifyEcKeyboardId (*KeyboardId);

  return EFI_SUCCESS;
}
  
EFI_STATUS
InitializeDYTCData  (
  IN OUT   LFC_NVS_AREA  *LfcNvsArea
  )
{

  UINT8       EcData               =  0;
  EFI_STATUS  Status               =  EFI_SUCCESS;

  LfcNvsArea->DytcCommond          =  0; 

  LfcNvsArea->DytcIcFuncCapability =  ((OPTION_DYTC_SUPPORT_ICFUNC_FBC << DYTC_ICFUNC_FBC) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_MYH << DYTC_ICFUNC_MYH) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_STP << DYTC_ICFUNC_STP) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_APM << DYTC_ICFUNC_APM) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_AQM << DYTC_ICFUNC_AQM) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_IEPM << DYTC_ICFUNC_IEPM) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_IBSM << DYTC_ICFUNC_IBSM) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_AAA << DYTC_ICFUNC_AAA) | \
                                      (OPTION_DYTC_SUPPORT_ICFUNC_MMC << DYTC_ICFUNC_MMC) | \
                                      1);  //OPTION_DYTC_SUPPORT_ICFUNC_STD always 1

  LfcNvsArea->DytcIcFuncValidFlag  =  1; // Function Standard -> always 1

  LfcNvsArea->DytcMYHCapability    =  (OPTION_DYTC_SUPPORT_ICMODE_TBL | \
                                      (OPTION_DYTC_SUPPORT_ICMODE_TNT << DYTC_ICMODE_TNT) | \
                                      (OPTION_DYTC_SUPPORT_ICMODE_LFT << DYTC_ICMODE_LFT));
  
  LfcNvsArea->DytcMMCCapability    =  ((OPTION_DYTC_SUPPORT_ICMODE_EPM << DYTC_ICMODE_EPM) | \
                                      (OPTION_DYTC_SUPPORT_ICMODE_BSM << DYTC_ICMODE_BSM));
  
  LfcNvsArea->DytcSMYH             =  0;
  LfcNvsArea->DytcSMMC             =  DYTC_ICMODE_EPM;  
  
  LfcNvsArea->CurrentIcFuncMode    =  0xF0;    

//[-start-210916-Dongxu0017-modify]//
  //Follow EC RAM 0x20 , change MMC mode
  Status = LfcEcLibEcRamRead (SYSTEM_PERFORMANCE_MODE_EC_RAM_REG, &EcData);
  
  if(EFI_SUCCESS == Status)
  {  
    //Set IC Funcion MMC Vaild flag
    LfcNvsArea->DytcIcFuncValidFlag |=  OPTION_DYTC_SUPPORT_ICFUNC_MMC << DYTC_ICFUNC_MMC;   
    
    if(EXTREME_PERFORMANCE_MODE == EcData) //Extreme Performance Mode
    {
       LfcNvsArea->CurrentIcFuncMode = 0x2B;  //IC Function:MMC, IC Mode:Extreme Performance
       LfcNvsArea->DytcSMMC          = DYTC_ICMODE_EPM;
    }
    else if(BATTERY_SAVING_MODE == EcData) //Battery Saving Mode
    {
      LfcNvsArea->CurrentIcFuncMode  = 0x3B; //IC Function:MMC, IC Mode:Battery Saving
      LfcNvsArea->DytcSMMC           = DYTC_ICMODE_BSM;
    }
    else //Intelligent Mode
    {
      LfcNvsArea->DytcIcFuncValidFlag &=  ~ (OPTION_DYTC_SUPPORT_ICFUNC_MMC << DYTC_ICFUNC_MMC);
      LfcNvsArea->CurrentIcFuncMode  = 0xFB; //IC Function:MMC, IC Mode:Intelligent
      LfcNvsArea->DytcSMMC           = DYTC_ICMODE_INT;
 //[-start-211117-Dongxu0029-add]//     
     //just S4->s0  valid on keep IC Function 
      UINT8 EcmosData;    
      IoWrite8 (LFC_CMOS_INDEX, LFC_WAKE_S4_INDEX);
      EcmosData = IoRead8 (LFC_CMOS_DATA);
      if (EcmosData != 0x07) {
        return Status;
      }
      //clear
      IoWrite8 (LFC_CMOS_INDEX, LFC_WAKE_S4_INDEX);
      IoWrite8 (LFC_CMOS_DATA, 0x00);
//[-end-211117-Dongxu0029-add]//
//[-start-210922-FLINT00022-add]//
      //
      //keep IC Function in Intelligent Mode (S4->s0)
      //
      if (INTELLIGENT_APM_MODE == EcData)//APM
      {
        LfcNvsArea->DytcIcFuncValidFlag |=  (OPTION_DYTC_SUPPORT_ICFUNC_APM << DYTC_ICFUNC_APM);
        LfcNvsArea->CurrentIcFuncMode  = 0xF5; //IC Function:APM, IC Mode:Intelligent
        LfcNvsArea->DytcSMMC           = DYTC_ICFUNC_APM;
      }
      else if (INTELLIGENT_AQM_MODE == EcData)//AQM
      {
        LfcNvsArea->DytcIcFuncValidFlag |=  (OPTION_DYTC_SUPPORT_ICFUNC_AQM << DYTC_ICFUNC_AQM);
        LfcNvsArea->CurrentIcFuncMode  = 0xF6; //IC Function:AQM, IC Mode:Intelligent
        LfcNvsArea->DytcSMMC           = DYTC_ICFUNC_AQM;
      }
//[-start-210919-QINGLIN0072-modify]//
//[-start-211008-TAMT000021-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211008-TAMT000021-modify]//
//[-end-210919-QINGLIN0072-modify]//
      else if (INTELLIGENT_EPM_MODE == EcData)//IEPM
      {
        LfcNvsArea->DytcIcFuncValidFlag |=  (OPTION_DYTC_SUPPORT_ICFUNC_APM << DYTC_ICFUNC_APM);
        LfcNvsArea->DytcIcFuncValidFlag |=  (OPTION_DYTC_SUPPORT_ICFUNC_IEPM << DYTC_ICFUNC_IEPM);
        LfcNvsArea->CurrentIcFuncMode  = 0xF7; //IC Function:IEPM, IC Mode:Intelligent
        LfcNvsArea->DytcSMMC           = DYTC_ICFUNC_IEPM;
      }
      else if (INTELLIGENT_BSM_MODE == EcData)//IBSM
      {
       // LfcNvsArea->DytcIcFuncValidFlag |=  (OPTION_DYTC_SUPPORT_ICFUNC_AQM << DYTC_ICFUNC_AQM);
        LfcNvsArea->DytcIcFuncValidFlag |=  (OPTION_DYTC_SUPPORT_ICFUNC_IBSM << DYTC_ICFUNC_IBSM);
        LfcNvsArea->CurrentIcFuncMode  = 0xF8; //IC Function:IBSM, IC Mode:Intelligent
        LfcNvsArea->DytcSMMC           = DYTC_ICFUNC_IBSM;
      }
#endif      
//[-end-210922-FLINT00022-add]//
    }
  }
//[-end-210916-Dongxu0017-modify]//  
  return Status;
}

//[-start-220322-QINGLIN0167-add]//
#if defined(S370_SUPPORT) 
BOOLEAN
IsShippingPrc (
  VOID
  )
{
  EFI_STATUS                              Status;
  LENOVO_VARIABLE_PROTOCOL                *LenovoVariable   = NULL;
  EFI_GUID                                MtmNumberGuid     = LVAR_MTM_NUMBER_GUID;
  UINT32                                  MtmDataSize       = L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH;
  UINT8                                   MtmBufferPtr[L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH];
  UINT8                                   Index             = 0;
  CHAR8                                   *mCountry[] = {
                                            "CP","CD","CH",
                                            NULL
                                          };

  Status = gBS->LocateProtocol (
                  &gLenovoVariableProtocolGuid, 
                  NULL, 
                  &LenovoVariable
                  );
  if (EFI_ERROR (Status)) {
    return FALSE;
  }
  Status = LenovoVariable->GetVariable (
                             LenovoVariable, 
                             &MtmNumberGuid, 
                             &MtmDataSize, 
                             MtmBufferPtr
                             );
  if (EFI_ERROR (Status)) {
    return FALSE;
  }
  while (mCountry[Index] != NULL) {
    if (MtmBufferPtr[8] == *mCountry[Index] && MtmBufferPtr[9] == *(mCountry[Index] + 1)) {
      return TRUE;
    }
    Index ++;
  }

  return FALSE;
}

EFI_STATUS 
OemSvcLfcGetCpuType (
  IN OUT UINT8 *CpuType
  )
{
  CHAR8                        CpuBrandString[49];
  CHAR8                        *BrandString;
  CPUID_BRAND_STRING_DATA      BrandStringEax;
  CPUID_BRAND_STRING_DATA      BrandStringEbx;
  CPUID_BRAND_STRING_DATA      BrandStringEcx;
  CPUID_BRAND_STRING_DATA      BrandStringEdx;
  
  BrandString = CpuBrandString;
  ///
  /// Get Brand string
  ///
  AsmCpuid (CPUID_BRAND_STRING1, &BrandStringEax.Uint32, &BrandStringEbx.Uint32, &BrandStringEcx.Uint32, &BrandStringEdx.Uint32);
  *(UINT32*) BrandString = BrandStringEax.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEbx.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEcx.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEdx.Uint32; BrandString += 4;
  
  AsmCpuid (CPUID_BRAND_STRING2, &BrandStringEax.Uint32, &BrandStringEbx.Uint32, &BrandStringEcx.Uint32, &BrandStringEdx.Uint32);
  *(UINT32*) BrandString = BrandStringEax.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEbx.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEcx.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEdx.Uint32; BrandString += 4;
  
  AsmCpuid (CPUID_BRAND_STRING3, &BrandStringEax.Uint32, &BrandStringEbx.Uint32, &BrandStringEcx.Uint32, &BrandStringEdx.Uint32);
  *(UINT32*) BrandString = BrandStringEax.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEbx.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEcx.Uint32; BrandString += 4;
  *(UINT32*) BrandString = BrandStringEdx.Uint32; BrandString += 4;
  *BrandString = '\0';

  BrandString = CpuBrandString;
  if (*(UINT16 *)(BrandString + 27) == 0x3769) {  //12th Gen Intel(R) Core(TM) i7-xxxxx
    *CpuType = 0x07;
  } else if (*(UINT16 *)(BrandString + 27) == 0x3569) {  //12th Gen Intel(R) Core(TM) i5-xxxxx
    *CpuType = 0x05;
  } else if (*(UINT16 *)(BrandString + 27) == 0x3369) {  //12th Gen Intel(R) Core(TM) i3-xxxxx
    *CpuType = 0x03;
  } else if  (*(UINT16 *)(BrandString + 9) == 0x6550) {  //Intel(R) Pentium(R) xxxx
    *CpuType = 0x02;
  } else { //Intel(R) Celeron(R) xxxxx
    *CpuType = 0x01;
  }

  return EFI_SUCCESS;
}

EFI_STATUS
OemSvcLfcGetSuperResolutionVersion (
  IN OUT  UINT8     *SuperResolutionVersion
  )
{

  UINT8        CpuType = 0;
  UINT8        MemoryType8 = 0;

  if (IsShippingPrc()) {
    *SuperResolutionVersion = 3;
    return EFI_SUCCESS;
  } else {
    OemSvcLfcGetCpuType (&CpuType);
    IoWrite8 (LFC_CMOS_INDEX, LFC_MEMORY_TYPE_INDEX);
    MemoryType8 = IoRead8 (LFC_CMOS_DATA);

    if ((CpuType == 0x07) || (CpuType == 0x05)) { // i7 or i5
      if ((MemoryType8 == 0x11) || (MemoryType8 == 0x22)) { // 4G+4G or 8G+8G
        *SuperResolutionVersion = 2;
        return EFI_SUCCESS;
      }
    }

  }
  *SuperResolutionVersion = 0;

  return EFI_SUCCESS;
}
#endif
//[-end-220322-QINGLIN0167-add]//

EFI_STATUS
SetLfcNvsData (
  IN OUT   LFC_NVS_AREA  *LfcNvsArea
  )
{
  UINT8                       UmaAndDisType;
  EFI_STATUS                  Status = EFI_SUCCESS;

  // Call EcLib to get TP ID
  Status = LfcEcLibGetTouchpadID (&(LfcNvsArea->Ps2TpVendor));
  if (EFI_ERROR (Status)) {
    LfcNvsArea->Ps2TpVendor = 1;  // If error, load default
  }

  // Get KB ID and notify EC
  LfcGetKbIdAndNotifyEc (&(LfcNvsArea->KeyboardId));

  // Get machine size and notify EC the machine size
  OemSvcLfcGetBoardID(PANEL_SIZE, &(LfcNvsArea->MachineSize));
  LfcEcLibNotifyEcMachineSize (LfcNvsArea->MachineSize);

//[-start-210908-QINGLIN0050-add]//
//[-start-210929-SHAONN0010-add]//
#ifdef S370_SUPPORT
  OemSvcLfcGetBoardID(PROJECT_ID, &(LfcNvsArea->OemProjectType));
  LfcEcLibNotifyEcProjectType(LfcNvsArea->OemProjectType);
#endif
//[-start-211122-JEPLIUT199-add]//
//[-start-211214-JEPLIUT207-modify]// 
#if defined(S570_SUPPORT)
{ 
  UINT32 BoardId15 = 0;
  OemSvcLfcGetCpuBrand( & (LfcNvsArea-> OemCpuType) );
  GpioGetInputValue (GPIO_VER2_LP_GPP_F18, &BoardId15);
  LfcNvsArea-> OemCpuTdp =(UINT8) BoardId15 ;
  LfcNvsArea->IpuSupport = IsIpuSupported() ;
}
#endif  
//[-end-211214-JEPLIUT207-modify]// 
//[-end-211122-JEPLIUT199-add]//

//[-end-210929-SHAONN0010-add]//
//[-end-210908-QINGLIN0050-add]//
//[-start-211014-YUNLEI0142-add]//
#if defined(C770_SUPPORT)
  OemSvcLfcGetCpuIdentifier(&(LfcNvsArea->PTdpCpuIdentifier));
  //OemSvcLfcGetNonIpuCpuIdentifier(&(LfcNvsArea->NonIpuCpuIdentifier));
#endif
//[-end-211014-YUNLEI0142-add]//

  // Get Novo button status
  LfcEcLibGetNovoStatus (&(LfcNvsArea->NovoBtStatus));
  
  // Get EDID
  LfcGetEdid (LfcNvsArea->EdidBuffer);
  
  //Initialize DYTC Data
#if ( OPTION_DYTC_ENABLE ) 
  InitializeDYTCData (LfcNvsArea);
#endif
  // For project modify OEM NVS region
  OemSvcLfcDxeUpdateOemNvsRegion (LfcNvsArea);

  OemSvcLfcGetBoardID(GPU_ID, &UmaAndDisType);
  LfcNvsArea->UMABoard = UmaAndDisType;
  LfcEcLibTellEcDisOrUma(UmaAndDisType);
//[-start-211021-QINGLIN0102-add]//
//[-start-211027-YULU0001-modify]//
#if defined(S370_SUPPORT) || defined(C770_SUPPORT)
  OemSvcLfcNotifyEcReadDgpuTemperature (UmaAndDisType);
#endif
//[-end-211027-YULU0001-modify]//
//[-end-211021-QINGLIN0102-add]//
//[-start-220322-QINGLIN0167-add]//
#ifdef S370_SUPPORT
  OemSvcLfcGetSuperResolutionVersion (&(LfcNvsArea->SuperResolutionVersion));
#endif
//[-end-220322-QINGLIN0167-add]//

  return Status;
}

EFI_STATUS
EFIAPI
LfcInstallAcpiTable (
  IN   EFI_ACPI_TABLE_PROTOCOL                    *This,
  IN   VOID                                       *AcpiTableBuffer,
  IN   UINTN                                      AcpiTableBufferSize,
  OUT  UINTN                                      *TableKey
  )
{
  EFI_ACPI_DESCRIPTION_HEADER   *TableHeader;
  TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) AcpiTableBuffer;
  if ((TableHeader != NULL) && (TableHeader->Signature == EFI_ACPI_3_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE)) {

    UINT8                              *CurrPtr;
    UINT8                              *DsdtPointer;
    UINT32                             *Signature;
    UINT8                              *Operation;
    UINT32                             *Address;
    UINT16                             *Size;

    CurrPtr = (UINT8 *) TableHeader;
    for (DsdtPointer = CurrPtr; DsdtPointer <= (CurrPtr + ((EFI_ACPI_COMMON_HEADER *) CurrPtr)->Length); DsdtPointer++) {
      Signature = (UINT32 *) DsdtPointer;
      if (*Signature == SIGNATURE_32 ('L', 'F', 'C', 'N')) {
        Operation = DsdtPointer - 1;
        if (*Operation == AML_OPREGION_OP) {
          Address   = (UINT32 *) (DsdtPointer + 6);
          *Address  = (UINT32) (UINTN) mLfcNvsAreaProtocol.Area;
          Size      = (UINT16 *) (DsdtPointer + 11);
          *Size     = sizeof (LFC_NVS_AREA);
        }
      }
    }
  }

  return mOrgInstallAcpiTable (This, AcpiTableBuffer, AcpiTableBufferSize, TableKey);
}

VOID
EFIAPI
LfcInstallAcpiTableOverrideAcpiCallBack (
  IN EFI_EVENT                           Event,
  IN VOID                                *Context
  )
{
  EFI_STATUS                    Status;
  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;

  gBS->CloseEvent (Event);

  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, &AcpiTable);
  ASSERT_EFI_ERROR (Status);

  mOrgInstallAcpiTable = AcpiTable->InstallAcpiTable;
  AcpiTable->InstallAcpiTable = LfcInstallAcpiTable;

  return;
}

EFI_STATUS
EFIAPI
LfcSetAcpiTable (
  IN   EFI_ACPI_SUPPORT_PROTOCOL                  *This,
  IN   VOID                                       *Table OPTIONAL,
  IN    BOOLEAN                                   Checksum,
  IN    EFI_ACPI_TABLE_VERSION                    Version,
  IN    OUT UINTN                                 *Handle
  )
{
  EFI_ACPI_DESCRIPTION_HEADER   *TableHeader;
  TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) Table;

  if ((TableHeader != NULL) && (TableHeader->Signature == EFI_ACPI_3_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE)) {

    UINT8                              *CurrPtr;
    UINT8                              *DsdtPointer;
    UINT32                             *Signature;
    UINT8                              *Operation;
    UINT32                             *Address;
    UINT16                             *Size;

    CurrPtr = (UINT8 *) TableHeader;
    for (DsdtPointer = CurrPtr; DsdtPointer <= (CurrPtr + ((EFI_ACPI_COMMON_HEADER *) CurrPtr)->Length); DsdtPointer++) {
      Signature = (UINT32 *) DsdtPointer;
      if (*Signature == SIGNATURE_32 ('L', 'F', 'C', 'N')) {
        Operation = DsdtPointer - 1;
        if (*Operation == AML_OPREGION_OP) {
          Address   = (UINT32 *) (DsdtPointer + 6);
          *Address  = (UINT32) (UINTN) mLfcNvsAreaProtocol.Area;
          Size      = (UINT16 *) (DsdtPointer + 11);
          *Size     = sizeof (LFC_NVS_AREA);
        }
      }
    }
  }

  return mOrgSetAcpiTable (This, Table, Checksum, Version, Handle);
}

VOID
EFIAPI
LfcSetAcpiTableOverrideAcpiCallBack (
  IN EFI_EVENT                           Event,
  IN VOID                                *Context
  )
{
  EFI_STATUS                    Status;
  EFI_ACPI_SUPPORT_PROTOCOL     *AcpiSupport;

  gBS->CloseEvent (Event);
  
  Status = gBS->LocateProtocol (&gEfiAcpiSupportProtocolGuid, NULL, &AcpiSupport);
  ASSERT_EFI_ERROR (Status);
  
  mOrgSetAcpiTable = AcpiSupport->SetAcpiTable;
  AcpiSupport->SetAcpiTable = LfcSetAcpiTable;

  return;
}
EFI_STATUS
EFIAPI
LfcAcpiEntry (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                    Status;
  EFI_HANDLE                    Handle;
  EFI_EVENT                     InstallAcpiTableEvent;
  EFI_EVENT                     SetAcpiTableEvent;
  VOID                          *Registration;
  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;
  EFI_ACPI_SUPPORT_PROTOCOL     *AcpiSupport;

  //
  // Allocate and initialize the NVS area for C and ASL communication.
  //
  Status = gBS->AllocatePool (EfiReservedMemoryType, sizeof (LFC_NVS_AREA), &mLfcNvsAreaProtocol.Area);
  ASSERT_EFI_ERROR (Status);
  gBS->SetMem (mLfcNvsAreaProtocol.Area, sizeof (LFC_NVS_AREA), 0);
  SetLfcNvsData (mLfcNvsAreaProtocol.Area);

  Handle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gLfcNvsAreaProtocolGuid,
                  &mLfcNvsAreaProtocol,
                  NULL
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gBS->LocateProtocol (&gEfiAcpiSupportProtocolGuid, NULL, &AcpiSupport);
  if (!EFI_ERROR (Status)) {
    mOrgSetAcpiTable = AcpiSupport->SetAcpiTable;
    AcpiSupport->SetAcpiTable = LfcSetAcpiTable;
  }
  else {
    //
    // Register Event for override ACPI Tables if ACPI Support Protocol is not ready yet.
    //
    Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    LfcSetAcpiTableOverrideAcpiCallBack,
                    NULL,
                    &SetAcpiTableEvent
                    );

    if (!EFI_ERROR (Status)) {
      Status = gBS->RegisterProtocolNotify (
                      &gEfiAcpiSupportProtocolGuid,
                      SetAcpiTableEvent,
                      &Registration
                      );
    }
  }
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, &AcpiTable);
  if (!EFI_ERROR (Status)) {
    mOrgInstallAcpiTable = AcpiTable->InstallAcpiTable;
    AcpiTable->InstallAcpiTable = LfcInstallAcpiTable;
    return EFI_SUCCESS;
  }

  // Install notify to override ACPI
  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  LfcInstallAcpiTableOverrideAcpiCallBack,
                  NULL,
                  &InstallAcpiTableEvent
                  );
  if (!EFI_ERROR (Status)) {
    Status = gBS->RegisterProtocolNotify (
                    &gEfiAcpiTableProtocolGuid,
                    InstallAcpiTableEvent,
                    &Registration
                    );
  }

  return EFI_SUCCESS;
}

