 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2013, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#include <Uefi.h>
#include <Library/PcdLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmCpu.h>
#include <Protocol/SmmBase2.h>
#include <Library/IoLib.h>
#include <LfcLog.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/LfcLib.h>
#include <Library/OemSvcLfcGetPlatformType.h>

EFI_SMM_CPU_PROTOCOL                    *mSmmCpu = NULL;
UINTN                                   SmiPort = 0xb2;
UINTN                                   SmiDataPort= 0xb3;

EFI_STATUS 
EFIAPI 
SmmLfcSwSmiCpuLibConstructor ( 
IN EFI_HANDLE       ImageHandle, 
IN EFI_SYSTEM_TABLE *SystemTable 
) 
{
  EFI_STATUS                            Status;
  UINT8                                 PlatformType;
  
  Status = OemSvcLfcDxeGetPlatformType (&PlatformType);
  if (PlatformType == INTEL_PLATFORM ) {
    //Intel platform
    SmiPort = 0xb2;
    SmiDataPort = 0xb3;
  } else if (PlatformType == AMD_PLATFORM){
    //AMD platform
    SmiPort = 0xb0;
    SmiDataPort = 0xb1;
  } else {
    LfcLibLogError ( LFC_LOG_LVAR_GET_PLATFROM_ERROR);
  }

  return Status;
}

EFI_STATUS
GetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  OUT UINT32                            *RegisterData
  )
{
  EFI_STATUS                            Status;

  if (mSmmCpu == NULL) {
    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmCpuProtocolGuid,
                      NULL,
                      &mSmmCpu
                      );
    ASSERT_EFI_ERROR (Status);
  }

  return mSmmCpu->ReadSaveState (
                    mSmmCpu,
                    sizeof (UINT32),
                    RegisterNum,
                    CpuIndex,
                    RegisterData
                    );
}

EFI_STATUS
SetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  IN  UINT32                            *RegisterData
  )
{
  EFI_STATUS                            Status;

  if (mSmmCpu == NULL) {
    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmCpuProtocolGuid,
                      NULL,
                      &mSmmCpu
                      );
    ASSERT_EFI_ERROR (Status);
  }

  return mSmmCpu->WriteSaveState (
                    mSmmCpu,
                    sizeof (UINT32),
                    RegisterNum,
                    CpuIndex,
                    RegisterData
                    );
}

//Find out which CPU triggered SW SMI
//EaxValue - Typecally the SW SMI number
//EaxValueMask - Typecally 0xff
//CpuIndex - CPU index
EFI_STATUS
IdentifyCpuIndexByEax (
  IN  UINT32                            EaxValue,
  IN  UINT32                            EaxValueMask,
  OUT UINTN                             *CpuIndex
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  UINT32                                Eax;
  UINT32                                Edx;
//UINT16                                SmiPort = 0xb2; 

  if (mSmmCpu == NULL) {
    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmCpuProtocolGuid,
                      NULL,
                      &mSmmCpu
                      );
    ASSERT_EFI_ERROR (Status);
  }

  //if (PcdGet8 (PcdPlatformIntelOrAmd) == 1) {
    // For Intel platform
    //SmiPort = 0xb2;
  //} else if (PcdGet8 (PcdPlatformIntelOrAmd) == 2) {
    // For AMD platform
    //SmiPort = 0xb0;
  //}

  for (Index = 0; Index < gSmst->NumberOfCpus; Index++) {
    Status = GetDwordRegisterByCpuIndex (EFI_SMM_SAVE_STATE_REGISTER_RAX, Index, sizeof (UINT32), &Eax);
    Status = GetDwordRegisterByCpuIndex (EFI_SMM_SAVE_STATE_REGISTER_RDX, Index, sizeof (UINT32), &Edx);
    // Find out which CPU triggered SW SMI
    if (((Eax & EaxValueMask) == EaxValue) && ((Edx & 0xffff) == SmiPort)) {
      // Cpu found!
      break;
    }
  }
  if (Index == gSmst->NumberOfCpus) {
    // Error out due to CPU not found
    return EFI_NOT_FOUND;
  } else {
    *CpuIndex = Index;
    return EFI_SUCCESS;
  }
}

EFI_STATUS
GetSwSmiSubFunctionNumber (
  OUT UINT8                             *SwSmiSubFunctionNumber
  )
{
  //UINT16 SmiDataPort = 0xb3;

  //if (PcdGet8 (PcdPlatformIntelOrAmd) == 1) {
    // For Intel platform
    //SmiDataPort = 0xb3;
  //} else if (PcdGet8 (PcdPlatformIntelOrAmd) == 2) {
    // For AMD platform
    //SmiDataPort = 0xb1;
  //}
  *SwSmiSubFunctionNumber = IoRead8 (SmiDataPort);

  return EFI_SUCCESS;
}

EFI_STATUS
SetSwSmiSubFunctionNumber (
  IN UINT8                              SwSmiSubFunctionNumber
  )
{
  //UINT16 SmiDataPort = 0xb3;

  //if (PcdGet8 (PcdPlatformIntelOrAmd) == 1) {
    // For Intel platform
    //SmiDataPort = 0xb3;
  //} else if (PcdGet8 (PcdPlatformIntelOrAmd) == 2) {
    // For AMD platform
    //SmiDataPort = 0xb1;
  //}

  IoWrite8 (SmiDataPort, SwSmiSubFunctionNumber);

  return EFI_SUCCESS;
}
