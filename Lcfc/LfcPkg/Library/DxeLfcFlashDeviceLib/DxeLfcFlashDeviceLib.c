 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/FdSupportLib.h>

/*
Read NumBytes from PAddress (flash device) to Buffer
*/
EFI_STATUS
EFIAPI
LfcFdLibRead (
  IN     UINTN                           PAddress,
  IN OUT UINTN                           *NumBytes,
     OUT UINT8                           *Buffer
  )
{
  return FlashRead (Buffer, (UINT8*)PAddress, *NumBytes);
}

/*
Write NumBytes of Butter to PAddress (flash device)
*/
EFI_STATUS 
EFIAPI
LfcFdLibWrite (
  IN      UINTN                          PAddress,
  IN OUT  UINTN                          *NumBytes,
	IN      UINT8                          *Buffer
  )
{
  return FlashProgram ((UINT8*)PAddress, Buffer, NumBytes, (PAddress & ~(0xFFFF)));
}

/*
Erase LbaLength from PAddress (flash device)
*/
EFI_STATUS 
EFIAPI
LfcFdLibBlockErase (
  IN      UINTN                          PAddress,
  IN      UINTN                          LbaLength      
  )
{
  return FlashErase (PAddress, LbaLength);
}

