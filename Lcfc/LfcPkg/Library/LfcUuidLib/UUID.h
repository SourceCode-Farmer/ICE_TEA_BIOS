#ifndef UUID_H
#define UUID_H


typedef struct _unsigned64_t {
    UINT32          lo;
    UINT32          hi;
} unsigned64_t;

typedef struct _uuid_t {
    UINT32          time_low;
    UINT16          time_mid;
    UINT16          time_hi_and_version;
    UINT8           clock_seq_hi_and_reserved;
    UINT8           clock_seq_low;
    UINT8           node[6];    //byte
} uuid_t;


//  Add a 16-bit unsigned integer to a 64-bit unsigned integer.
#define ADD_16b_2_64b(A, B, sum) \
    { \
        (sum)->hi = (B)->hi; \
        if ((B)->lo & 0x80000000UL) { \
            (sum)->lo = (*A) + (B)->lo; \
            if (!((sum)->lo & 0x80000000UL)) (sum)->hi++; \
        } \
        else \
            (sum)->lo = (*A) + (B)->lo; \
    }

// Add two unsigned 64-bit long integers.
#define ADD_64b_2_64b(A, B, sum) \
    { \
        if (!(((A)->lo & 0x80000000UL) ^ ((B)->lo & 0x80000000UL))) { \
            if (((A)->lo&0x80000000UL)) { \
                (sum)->lo = (A)->lo + (B)->lo; \
                (sum)->hi = (A)->hi + (B)->hi + 1; \
            } \
            else { \
                (sum)->lo = (A)->lo + (B)->lo; \
                (sum)->hi = (A)->hi + (B)->hi; \
            } \
        } \
        else { \
            (sum)->lo = (A)->lo + (B)->lo; \
            (sum)->hi = (A)->hi + (B)->hi; \
            if (!((sum)->lo&0x80000000UL)) (sum)->hi++; \
        } \
    }

#define CLOCK_SEQ_LAST  0x3FFF
#define RAND_MASK       CLOCK_SEQ_LAST

EFI_STATUS uuid_init(VOID);
EFI_STATUS uuid_create(uuid_t *uuid);


#endif  // UUID_H