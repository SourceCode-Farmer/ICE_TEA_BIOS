#ifndef GETNODE_H
#define GETNODE_H

#define MD_CTX MD5_CTX
#define MDInit MD5Init
#define MDUpdate MD5Update
#define MDFinal MD5Final

#define HASHLEN 16

void GenNodeID( unsigned char * pDataBuf, long cData, unsigned char NodeID[6]);


#endif  // GETNODE_H