/* 
 *      Lenovo Confidential
 *      Microcode Source Materials
 *      COPYRIGHT LENOVO 2005, 2012 All RIGHTS RESERVED
 */

#define PROTOTYPES      1

#include "GLOBAL.h"
#include "MD5.h"
#include "GETNODE.h"


// *******************************************************************
//
// FUNCTION NAME.
//      LocalDumpHex
//
// FUNCTIONAL DESCRIPTION.
//
// ENTRY PARAMETERS.
//
// EXIT PARAMETERS.
//
// WARNINGS.
//
// *******************************************************************
void GenNodeID(
  unsigned char * pDataBuf,   // concatenated "randomness values"
  long          cData,        // size of randomness values
  unsigned char NodeID[6]     // node ID
)
{
  int i, j;
  unsigned char Hash[HASHLEN];
  MD_CTX context;

  MDInit (&context);
  MDUpdate (&context, pDataBuf, cData);
  MDFinal (Hash, &context);

  for (j = 0; j<6; j++) NodeID[j]=0;
  for (i = 0,j = 0; i < HASHLEN; i++) {
      NodeID[j++] ^= Hash[i];
      if (j == 6) j = 0;
  };

  NodeID[0] |= 0x80;          // set the multicast bit
}

