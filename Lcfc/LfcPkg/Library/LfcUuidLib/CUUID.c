/* 
 *      Lenovo Confidential
 *      Microcode Source Materials
 *      COPYRIGHT LENOVO 2005, 2012 All RIGHTS RESERVED
 */

#include <Library/UefiLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>

#include "CUUID.h"
#include "UUID.h"

// *******************************************************************
//
// FUNCTION NAME.
//      auto_uuid
//
// FUNCTIONAL DESCRIPTION.
//
// ENTRY PARAMETERS.
//
// EXIT PARAMETERS.
//
// WARNINGS.
//
// *******************************************************************
EFI_STATUS auto_uuid(
  OUT UINT8 *inBuff) 
{
  EFI_STATUS        Status;
  INTN              i;
  uuid_t            theUUID;


  // Initialize
  Status  = EFI_SUCCESS;
  ZeroMem (&theUUID, sizeof(theUUID));

  //
  // Generate UUID
  //

  Status = uuid_create( &theUUID );
  if (EFI_ERROR (Status)) {
    DEBUG((EFI_D_ERROR, "ERROR : uuid create, Status=[%r]\n", Status));
    return Status;
  }

  inBuff[0] = (UINT8)( theUUID.time_low >> 24 ) & 0xff;
  inBuff[1] = (UINT8)( theUUID.time_low >> 16 ) & 0xff;
  inBuff[2] = (UINT8)( theUUID.time_low >> 8  ) & 0xff;
  inBuff[3] = (UINT8)( theUUID.time_low       ) & 0xff;
  inBuff[4] = (UINT8)( theUUID.time_mid >> 8  ) & 0xff;
  inBuff[5] = (UINT8)( theUUID.time_mid       ) & 0xff;
  inBuff[6] = (UINT8)( theUUID.time_hi_and_version >> 8 ) & 0xff;
  inBuff[7] = (UINT8)( theUUID.time_hi_and_version      ) & 0xff;
  inBuff[8] = theUUID.clock_seq_hi_and_reserved;
  inBuff[9] = theUUID.clock_seq_low;
  for( i = 0; i < sizeof( theUUID.node ); i++ ){
      inBuff[10 + i] = theUUID.node[i];
  }

  return EFI_SUCCESS;
}



// *******************************************************************
//
// FUNCTION NAME.
//      autoGenerate
//
// FUNCTIONAL DESCRIPTION.
//
// ENTRY PARAMETERS.
//
// EXIT PARAMETERS.
//
// WARNINGS.
//
// *******************************************************************
EFI_STATUS autoGenerate(
  OUT UINT8    *genUUID)
{
  EFI_STATUS        Status;
  UINT8             theBuff[mUUIDLength];

  // Initialize
  Status          = EFI_ABORTED;
  ZeroMem (theBuff, sizeof(theBuff));


  //
  // Automatically generate UUID
  //

  Status = uuid_init();
  if (EFI_ERROR (Status)) {
    DEBUG((EFI_D_ERROR, "ERROR : uuid init, Status=[%r]\n", Status));
    return Status;
  }

  Status = auto_uuid( theBuff );
  if (EFI_ERROR (Status)) {
    DEBUG((EFI_D_ERROR, "ERROR : auto uuid, Status=[%r]\n", Status));
    return Status;
  }

  CopyMem (genUUID, theBuff, 16);

  return EFI_SUCCESS;
}