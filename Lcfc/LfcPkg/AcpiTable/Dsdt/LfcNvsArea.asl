 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

OperationRegion(LFCN, SystemMemory, 0xFFFF0000, 0xAA55)
Field(LFCN, AnyAcc, Lock, Preserve)
{
  ///////////////////////////////////////////////////////////////////////////////
  // Common part, project should not modify it
  // !!! Please sync this layout with LFC_NVS_AREA in LfcNvsArea.h !!!
  PS2V,	8,    // PS2 TP (mouse) vendor ID, 1=ELAN, 2=Synaptics, 3=ALPS
  KBID, 8,    // KB ID, 'B' or 'S'
  MCSZ, 8,    // Machine size, 12 = 12'. 13 = 13'
  OKRB, 8,    // OKR button status
              // 0x01: OKR button pressed
              // 0x00: OKR button not pressed
  EDID, 1024, // EDID buffer
  TPTY, 8,    //I2C Touch Pad   Type (0x01:ELAN, 0x02:SYNA, 0x03:APLS)
  TPTP, 16,   //I2C Touch Pad   PID
  TPNY, 8,    //I2C Touch Panel Type (0xA1:WACOM)
  TPNP, 16,   //I2C Touch Panel PID 
  UMAB, 8,    //UMABoard
  ENQT, 8,    //PerformanceMode
  //Follow ITS 4.0 Spec,LCFCTODO: you need check your project support ITS version and follow ITS to change 
  DYTP, 32,   //DYTC commond parameter,for debug
  FCAP, 16,   //IC Function Capability Status,Bit0=STD supported(always1), Bit2=Dedusting,Bit3=MYH supported,
              //Bit4=STP supported, Bit5=APM,Bit6=AQM,Bit10=AAA supported,Bit11=MMC supported,Bit12=MSC supported, Bit13=RSO supported
  //Function Valid Flag
  VSTD, 1,    //Bit0 - STD Valid Flag, always 1
      , 1,    //Bit1 - Reserved
  VFBC, 1,    //Bit2 - FBC Valid Flag
  VMYH, 1,    //Bit3 - MYH Valid Flag
  VSTP, 1,    //Bit4 - STP Valid Flag
  VAPM, 1,    //Bit5 - APM Valid Flag
  VAQM, 1,    //Bit6 - AQM Valid Flag
  VIEP, 1,    //Bit7 - IEPM Valid Flag
  VIBS, 1,    //Bit8 - IBSM Valid Flag
  VCQL, 1,    //Bit9 - CQL Valid Flag
  VAAA, 1,    //Bit10- AAA Valid Flag
  VMMC, 1,    //Bit11- MMC Valid Flag
      , 1,    //Bit12- Reserved
      , 1,    //Bit13- Reserved
      , 1,    //Bit14- Reserved
      , 1,    //Bit15- Reserved
  MYHC, 8,    //MYH Capability Status,Bit0=Tablet supported,Bit1=Tent supported,Bit2=Lay Flat supported
  MMCC, 8,    //MMC Capability Status,Bit2=Extreme Performance supported, Bit3=Battery Saving supported
  SMYH, 8,    //MYH Current Mode,0=Tablet,1=Tent,2=LayFlat,Fh when clear MYH Valid Flag
  SMMC, 8,    //MMC Current Mode,2=Extreme Performance, 3=Battery Saving,Fh when clear MMC Valid Flag
  CICF, 4,    //Current IC Function
  CICM, 4,    //Current IC Mode,Fh=No Mode(when Current Function is not MYH,MMC,RSO) 
  ///////////////////////////////////////////////////////////////////////////////
  // Project part, please modify it here
  Include("Project/Include/LfcOemNvsArea.asl")
  ///////////////////////////////////////////////////////////////////////////////
}


