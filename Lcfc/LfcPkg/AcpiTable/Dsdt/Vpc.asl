//*****************************************************************************
//
// Copyright (c) 2012 - 2014, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*++
Abstract:
  LCFC VPC common asl code definition, it's for all Hefei LBG projects.

History:
  Date          Name          Version    Change Notes
  2014.6.25     Steven Wang   V1.00      Initial Release
  2014.8.14     Steven Wang   V1.01      Support Lenovo PM Firmware Spec V1.67
  2014.10.08    Steven Wang   V1.02      Support Lenovo PM Firmware Spec V1.68
  2014.10.22    Steven Wang   V1.03      Optimized the code and remove some unused feauture porting TODO.
  2015.03.18    Steven Wang   V1.04      Support Lenovo PM Firmware Spec V1.69, V1.70
  2015.03.25    Cissie Gu     V1.05      Add Usb Charge disable and enable function.
  2015.07.28    Steven Wang   V1.06      Add Mutex scheme for all EC access.
  2015.08.03    Steven Wang   V1.07      Fix hotkey mode switch error in Lenovo Setting App.
  2015.08.19    Steven Wang   V1.08      Add GSBI method as per Spec V1.70, this method will be used
                                         by Lenovo Setting App, Lenovo Companion App.
  2015.08.27    Steven Wang   V1.09      Add ECAV for EC opregion access safely.
  2015.10.23    Steven Wang   V1.10      Add AMD support and other minor change.
  2016.08.12    Steven Wang   V1.11      Optimize the battery access method, due to 68 6C will conflict with some tool, change to 62 66
                                         command method.
  2017.05.05    Steven Wang   V1.12      Add support for Lenovo PM Firmware Spec V1.73
                                         Add _HRV method for Yoga project
  2017.07.31    Steven Wang   V1.13      Add support for Lenovo PM Firmware Spec V1.74
                                         Need based on  Lenovo ITS feature
  2019.11.13    Cissue Gu     V1.14      Support Lenovo PM Firmware Spec V1.77
                                         Dynamic distinguish battery if support quick charge
  2020.04.01    Storm Yin     v1.14      Add KBLC method for Lenovo PM Firmware Spec v1.80.
                                         not add ITHC method which required at v1.78. Reason: ITS3.0 requirement. ITS4.0 no need this.
  2020.10.20    Jesse Zheng   V1.15      Add BSIF method for Lenovo PM Firmware Spec v1.83.                                    
  2021.06.08    Jesse Zheng   V1.16      Add Add conservation mode in GBMD method bit23 with spec v1.84.
Module Name:
  VPC.asl

Note:
  You must take care about the prompt string "LCFCTODO:" and customize your
  project.

Support PM Spec Version:
  Lenovo PM Firmware Spec V1.83
--*/
#define  KBLC_GET_CAPABILITY                   0x01
#define  KBLC_GET_STATUS                       0x02
#define  KBLC_SET_STATUS                       0x03
#define  FUNCTION_ENABLE_HOT_KEY_MODE          0x30
#define  FUNCTION_DISABLE_HOT_KEY_MODE         0x31
#define  BOOT_SEQUENCE_UPDATE_SWSMI            0xCA
#define  FUNCTION_ENABLE_USB_CHARGE_MODE       0x32
#define  FUNCTION_DISABLE_USB_CHARGE_MODE      0x33
#define  PM_Version                            0x00000B80 //Version 1.84, Bit31-Bit4=184
#define  FUNCTION_DISABLE_CHARGE_IN_BATTERY_MODE 0x34
#define  FUNCTION_ENABLE_CHARGE_IN_BATTERY_MODE  0x35

Scope(EC0_SCOPE.EC0)
{
  Device (VPC0)
  {
    Name(_HID, "VPC2004")
    Name(_UID, 0)
    //===============================================================================
    //  0xAABBCCDD definition:
    //  refer to Lenovo PM Firmware Spec V1.67
    //===============================================================================
    //  AA:
    //  Bit 7     1:Support show Camera OSD
    //  Bit 6     1: Touchpad device Exist
    //            0: No Touchpad device
    //  Bit 5     1:Support show Mic OSD
    //  Bit 4     1:Support show CapsLK OSD
    //  Bit 3     1:Support show NumLK OSD
    //  Bit 2     1:Support show Touchpad OSD
    //  BIT [1,0]  : Product information
    //             00 Products belong to other products
    //             01 Products belong to Non-cs Yoga
    //             10 Products belong to Ux
    //-------------------------------------------------------------------------------
    //  BB:
    //  BIT 7 = 1: Support color engine,
    //       0: Don't support color engine
    //  BIT 6~4 = User self define button count, bit4 is least significant bit,
    //      bit6 is most significant bit.
    //          000 indicate 1 user self define button
    //          001 indicate 2 user self define button
    //          110 indicate 7 user self define button
    //      111 indicate no user self define button
    //  BIT 3 = 1: Camera device Exist
    //      0: No Camera device
    //  BIT 2 = 1: Wireless Exist
    //      0: No Wireless
    //  BIT 1 = 1: 3G Exist
    //      0: No 3G
    //  BIT 0 = 1: Bluetooth Exist
    //      0: No Bluetooth
    //-------------------------------------------------------------------------------
    //  CC:
    //  BIT 7~5 = EQ button count, bit5 is least significant bit, bit7 is most
    //      significant bit.
    //      000 indicate there has 1 EQ button
    //      001 indicate there has 2 EQ button
    //
    //      110 indicate there has 7 EQ button
    //      111 indicate there has 0 EQ button
    //      BIT 4 = Which VGA is active
    //          0: Intel VGA is active
    //          1: When bit [210] = 100b, ATI VGA is Active or When bit [210] = 101b
    //      , NVIDIA VGA is active.
    //  BIT 3 = 0: no HDMI device plugged in
    //          1: HDMI device plugged in
    //  BIT 2~0 = 000: reserved
    //      001: only Intel VGA
    //                010: only ATI VGA
    //                011: only NVIDIA VGA
    //                100: both Intel and ATI VGA
    //                101: both Intel and NVIDIA VGA
    //-------------------------------------------------------------------------------
    //  DD:
    //  BIT 7 = DCR
    //          1: Support DCR
    //          0: Don't support DCR
    //  BIT 6 = 1: Support underclock
    //          0: Don't support under clock
    //  BIT 5 = 1: Support Lenovo Super Performance function
    //      0: Don't support Lenovo Super Performance function
    //  BIT 4 = 1: Product support touch
    //          0: Product don't support touch.
    //  BIT 3 = 1:Support "Quite Mode"
    //      0:Don't support "Quite Mode"
    //  BIT 2 = 1: Intel CPU
    //      0: AMD   CPU
    //  BIT 1 = Resvered
    //  BIT 0 = Status of 3G and wireless
    //          0 no mutex
    //          1 mutex wireless and 3G
    //-------------------------------------------------------------------------------

    //
    //LCFCTODO: Need customize the flag as your project definition.
    //
    Name(_VPC, 0x400DE114)

    //
    //LCFCTODO: Need check if Pre-installed System is windows 10 cloud OS,if yes, add the below code
    //
    //Name(_HRV, 0x0001) // If the Pre-installed System is windows 10 cloud OS,
                         //   BIOS should add the _HRV. Otherwise, Ignore this item.
    Name(VPCD, 0)

    Method(_STA, 0, NOTSerialized)
    {
      return (0x0F)
    }

    Method(_CFG, 0, NotSerialized)
    {
      Return(_VPC)
    }
    //===============================================================================
    //  VPCR definition:
    //===============================================================================
    // Read Command/Data Register
    // Args: arg0 Register indicator
    //              0 is Data Register
    //              1 is Command Register
    // Return: The value read from the Register
    Method(VPCR, 1, Serialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          if (LEqual (arg0, 1))
          {
            store(VCMD,VPCD)
          }
          else
          {
            store(VDAT,VPCD)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      return (VPCD)
    }

    //===============================================================================
    //  VPCW definition:
    //===============================================================================
    // Write Command/Data Register
    // Args: arg0  Register indicator
    //              0 is Data Register
    //              1 is Command Register
    //        arg1  The value to be written to the Register
    // Return: Not used
    Method(VPCW, 2, Serialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          if (LEqual (arg0, 1))
          {
              store(arg1,VCMD)
          }
          else
          {
              store(arg1,VDAT)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      return (0)
    }

    //===============================================================================
    //  SVCR definition:
    //===============================================================================
    //PM utility invokes the method to enable/disable cpu working in single phase.
    //EM7 will use this method to control Quite Mode disable/enable .
    //Input Arg (DWORD):
    //  0x00: Disable Quite Mode
    //  0x01: Enable Quite Mode
    //  0x02: Stop CPU auto mode
    //  0x03: Start CPU auto mode
    //  0x04: Enable DCR.
    //  0x05: Disable DCR.
    //  0x06: Enable UnderClock.( Need BIOS overwrite BCLK ratio
    //        to underclock, and fix PEG and DMI ratio.)
    //  0x07: Disable Underclock.
    Method(SVCR, 1, Serialized)
    {
    //Not support currently.
    }

    //===============================================================================
    //  HALS definition:
    //===============================================================================
    //Bit0:  0, ALS sensor disabled
    //       1, ALS sensor enabled
    //Bit1:  0, ALS (Ambient Light Sensor) Keyboard Control Disabled
    //       1, ALS (Ambient Light Sensor) Keyboard Control Enabled
    //Bit2:  0, do not support ALS (Ambient Light Sensor) Keyboard Control
    //       1, support ALS (Ambient Light Sensor) Keyboard Control
    //Bit3:  0, do not support ALS
    //       1, support ALS
//remove BIT4, BIT5  (Replace with KBLC method)
//    //Bit4:  0, do not support keyboard led feature
//    //       1, support keyboard led feature
//    //Bit5:  0: keyboard led turn off
//    //       1: keyboard led turn on
    //Bit6:  0: don't support the feature of Always On USB in s3/s4/s5
    //       1: support the feature of Always On USB in s4/s5
    //Bit7:  0: the feature of Always On USB in s3/s4/s5 disable
    //       1: the feature of Always On USB in s3/s4/s5 enable.
    //Bit8:  0: the feature of auto on/off keyboard/touchpad is disabled.
    //       1: the feature of auto on/off keyboard/touchpad is enabled.
    //       (Note: Just apply to Yoga mode products.)
    //Bit9:  1:Support F1-F12 setting feature
    //       0:don't support F1-F12 setting feature
    //Bit10: 0: Fn+F1-f12 is hotkey
    //       1: F1-f12 is hotkey
    //Bit11: 0: The keyboard layout F1-f12 is main hotkey, Function Key is auxiliary hotkey.
    //       1: The keyboard layout Function Key is main hotkey, F1-f12 is auxiliary hotkey.
    //Bit12: 1: Support Lenovo S0IL.
    //       0: Do not support Lenovo S0IL.
    //Bit13: 1: Performance mode of ITS.
    //       0: Quite mode of ITS.
    //Bit14: 1: Support Charge in Battery Mode of Always On USB
    //       0: Do not support Charge in Battery Mode of Always On USB
    //Bit15: 1: Charge in Battery Mode of Always On USB is enabled.
    //       0: Charge in Battery Mode of Always On USB is disabled.
    //Bit16~Bit31: 0: Reserved.

    Method(HALS, 0, NotSerialized)
    {
      Store(Zero, Local0)
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          //Bit0
          //LCFCTODO: if support ALS sensor, please provide the method to indicate ALS disable or enable.
          //If(LEqual(One, XXXX))
          //  Or(Local0, 0x01, Local0)
          //}

          //Bit1
          //Not support currently

          //Bit2
          //Not support currently

          //Bit3
          //LCFCTODO: if support ALS sensor function at your project, please set Bit3 here.
          Or(Local0, 0x08, Local0)

//remove BIT4, BIT5  (Replace with KBLC method)
//          //Bit4
//          //LCFCTODO: if support keyboard led function at your project, please set Bit4 here.
//          Or(Local0, 0x10, Local0)
//
//          //Bit5
//          //LCFCTODO: if support keyboard led function at your project, please provide the method to indicate keyboard LED on or off.
//          If(LEqual(One, KBLO))  {
//            Or(Local0, 0x20, Local0)
//          }

          //Bit6
          //LCFCTODO: if support Always On USB in S3,S4,S5 at your project, please set Bit6 here.
          //Or(Local0, 0x40, Local0)

          //Bit7
          //LCFCTODO: if support Always On USB in S3,S4,S5 at your project, please provide the method to indicate Always On USB status at S3,S4,S5.
          //If(LEqual(One,   UCHE)) {
          //  Or(Local0, 0x80, Local0)
          //}

          //Bit8
          //Not support currently

          //Bit9
          Or(Local0, 0x200, Local0)
          if(HKDB){
            //Function Key Mode Disable, F1 - F12 = F1 - F12
            //Function Key Mode Enable, Fn + F1 - F12 = F1 - F12
            //Bit10
            Or(Local0, 0x400, Local0)
          }

          //Bit11
          // EM 1.67 feature, for G and Z, Yoga hard coding BIT11 to 1
          Or(Local0, 0x800, Local0)

          //Bit12
          //Not support currently

          //Bit13
          if(ITMD) {
            // 1: Performance mode of ITS.
            // 0: Quite mode of ITS.
            Or(Local0, 0x2000,Local0)
          }
          //Bit14
          //LCFCTODO: if support Charge in Battery Mode in S3,S4,S5 at your project, please set Bit14 here.
          //Or(Local0, 0x4000, Local0)

          //Bit15
          //LCFCTODO: if support Charge in Battery Mode in S3,S4,S5 at your project, please provide the method to indicate Charge in Battery Mode status at S3,S4,S5.
          //If(LEqual(One,   CIBM)) {
          //  Or(Local0, 0x8000, Local0)
          //}
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return(Local0)
    }

    //===============================================================================
    //  SALS definition:
    //===============================================================================
    //Arg0: 0,  Disable Lenovo S0IL
    //      1,  Enable Lenovo S0IL
    //      4,  Enable colour engine
    //      5,  Disable colour engine
    //      6,  Disable ALS (Ambient Light Sensor) Keyboard Control
    //      7,  Enable ALS (Ambient Light Sensor) Keyboard Control
//remove BIT8, BIT9  (Replace with KBLC method)
//    //      8,  turn on keyboard led
//    //      9,  turn off keyboard led
    //      10, enable the feature of Always On USB in s3/s4/s5,if battery RSOC <20%, EC stop Always On USB,but still keep enable status
    //      11, Disable the feature of Always On USB in s3/s4/s5
    //      12, Enable auto on/off keyboard/touchpad feature
    //      13, Disable auto on/off keyboard/touchpad feature
    //      14, Set F1-F12 as hotkey
    //      15, Set Fn+ f1-F12 as hotkey
    //      16, Notify BIOS Enter Lenovo S0IL
    //      17, Notify BIOS exit Lenovo S0IL
    //      18, Disable Charge in Battery Mode of Always On USB
    //      19, Enable Charge in Battery Mode of Always On USB
     Method(SALS, 1, Serialized)
     {
       Store(ToInteger(Arg0), Local0)
       If(EC0_SCOPE.EC0.ECAV){
         If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
           //sub function 00
           //Not support currently

           //sub function 01
           //Not support currently

           //sub function 04
           //Not support currently

           //sub function 05
           //Not support currently

           //sub function 06
           //Not support currently

           //sub function 07
           //Not support currently

//remove BIT8, BIT9  (Replace with KBLC method)
//           //sub function 08
//    //LCFCTODO: if support one level backlight, use the below reference code.
//           if(LEqual(Local0, 0x08)){
//             Store(1, KBLO)
//             Release(EC0_SCOPE.EC0.LfcM)
//             Return(0)
//           }
//           //LCFCTODO: if support two level backlight, use the below reference code.
//           //if(LEqual(Local0, 0x08)){
//           //  Store(1, KLOR)   //Lenovo Setting turn on LED
//           //  Store(1, KLCH)   //Notify EC to turn on LED
//           //  Notify(EC0_SCOPE.VPC0,0x80)
//           //  Release(EC0_SCOPE.EC0.LfcM)
//           //  Return(0)
//           //}
//           //sub function 09
//    //LCFCTODO: if support one level backlight, use the below reference code.
//           if(LEqual(Local0, 0x09)){
//             Store(0, KBLO)
//             Release(EC0_SCOPE.EC0.LfcM)
//             Return(0)
//           }
//           //LCFCTODO: if support two level backlight, use the below reference code.
//    //if(LEqual(Local0, 0x09)){
//           //  Store(0, KLOR)   //Lenovo Setting turn off LED
//           //  Store(1, KLCH)   //Notify EC to turn off LED
//           //  Notify(EC0_SCOPE.EC0.VPC0,0x80)
//           //  Release(EC0_SCOPE.EC0.LfcM)
//           //  Return(0)
//           //}
           //sub function 10
           if(LEqual(Local0, 0x0A)){
             Store(1, UCHE)
             Store(FUNCTION_ENABLE_USB_CHARGE_MODE, SMBB)
             Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(0)
           }

           //sub function 11
           if(LEqual(Local0, 0x0B)){
             Store(0, UCHE)
             Store(FUNCTION_DISABLE_USB_CHARGE_MODE, SMBB)
             Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(0)
           }

           //sub function 12
           //not support currently

           //sub function 13
           //not support currently

           //sub function 14
           if(LEqual(Local0, 0x0E)){
             Store(1, HKDB)
             Store(FUNCTION_DISABLE_HOT_KEY_MODE, SMBB)
             Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(0)
           }

           //sub function 15
           if(LEqual(Local0, 0x0F)){
             Store(0, HKDB)
             Store(FUNCTION_ENABLE_HOT_KEY_MODE, SMBB)
             Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(0)
           }

           //sub function 16
           //Not support currently

           //sub function 17
           //Not support currently

           //sub function 18 disable Charge in battery
           if(LEqual(Local0, 0x12)){
             Store(0, CIBM)
             Store(FUNCTION_DISABLE_CHARGE_IN_BATTERY_MODE, SMBB)
             Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(0)
           }

           //sub function 19 enable Charge in battery
           if(LEqual(Local0, 0x13)){
             Store(1, CIBM)
             Store(FUNCTION_ENABLE_CHARGE_IN_BATTERY_MODE, SMBB)
             Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(0)
           }
           Release(EC0_SCOPE.EC0.LfcM)
         }
       }
       Return(Zero)
    }

    //===============================================================================
    //  GBMD definition:
    //  refer to Lenovo PM Firmware Spec V1.68
    //===============================================================================
    //PM utility provides some ACPI method to do this.
    //
    //GBMD method:
    //PM utility invokes this method to get battery information
    //about the battery's capabilities and current state in
    //relation to battery calibration and charger control
    //features.
    //Output Arg(DOWRD):
    //Bit0: 1 indicates the main battery is running a calibration cycle.
    //Bit1: 1 indicates notify user to select battery Storage Mode.
    //Bit2: 1 indicates current charge mode is quick Charge.
    //      0 indicates current charge mode is normal Charge.
    //Bit3: 1 indicates the main battery is bad.
    //Bit4: 1 indicates the second battery is bad.
    //Bit5: 0 indicates the main battery is longest battery mode.
    //      1 indicates the main battery is battery storage mode.
    //Bit6: 0 indicates the second battery is longest battery mode.
    //      1 indicates the second battery is battery storage mode.
    //Bit7: 1 indicates an illegal main battery plugged in laptop.
    //      0 indicates a legal main battery plugged in laptop.
    //
    //Bit8: 1 indicates the main battery's performance degradation has occurred. EM should show the warning to End-User.
    //      0 indicates the main battery's performance is on the normal status.
    //
    //Bit9: 0 indicates start the storage mode for the main battery.
    //      1 indicates close the storage mode for the main battery.
    //
    //Bit10: 1 indicates the sencond battery is illegal.
    //       0 indicates the second battery battery is legal
    //Bit11: 0 the main battery is in normal.
    //       1 the main battery has been running over 72hrs, EM should pop out a message.
    //Bit12: 0 the second battery is in normal.
    //       1 the second battery has been running over 72hrs, EM should pop out a message.
    //Bit13: 1 indicates the second battery's performance degradation has occurred. EM should show the warning to End-User.
    //       0 indicates the second battery's performance is on the normal status.
    //Bit14: 0 Support one battery.
    //       1 Support two battery.
    //
    //Bit16Bit15:00 - Normal adapter
    //           01- incorrectly adapter
    //           10-adpter power is not suitable.
    //Bit17: 0 Don't support quick charge
    //       1 Support quick charge
    //Bit18: 0 Don't support battery ship mode
    //       1 Support battery ship mode
    //Bit19: 1 Don't support Adapter type detect.
    //       0 Support Adapter type detect.
    //Bit20: 1 indicates the second battery is running a calibration cycle.
    //Bit21: 0 indicates start the storage mode for the second battery.
    //       1 indicates close the storage mode for the second battery.
    //Bit22: 0 indicates battery is not in ship mode.
    //       1 indicates battery is in ship mode.
    //       (The bit should be set to 1 in the following case:
    //        Power on and SBMC Arg=0x09;
    //        The bit should be set to 0 in the following case:
    //        Case 1: Power off;
    //        Case 2: Power on and SBMC Arg=0x10)
    //Bit23: 0: 55%-60% charge threshold of Conservation mode
    //       1: 75%-80% charge threshold of Conservation mode 
    //Bit24-Bit31: Reserved

     Method(GBMD, 0, NotSerialized)
     {
       Store(0x10000000, Local0)
       If(EC0_SCOPE.EC0.ECAV){
         If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
           //Bit0
           If(LEqual(One, CDMB))
           {
             Or(Local0, One, Local0)
           }

           //Bit1
           //Not support currently.

           //Bit2 indicates current charge mode is quick Charge
           If(LEqual(1, EC0_SCOPE.EC0.QCBX)){//if support quick charge
             If(LEqual(One, EC0_SCOPE.EC0.QCHO))
             {
               Or(Local0, 0x04, Local0)
             }
           }
           //Bit3
           If(LEqual(One, BBAD))
           {
             Or(Local0, 0x08, Local0)
           }

           //Bit4
           //SECONDBATTERY, if support, need implement this with EC

           //Bit5
           If(LEqual(One, BTSM)) {
             Or(Local0, 0x20, Local0)
           }

           //Bit6
           //LCFCTODO: If support the second battery, need define a indicator flag for it and
           //must search "SECONDBATTERY" string for doing more implementation.
           //If(LEqual(One, XXXX))
           //{
           //  Or(Local0, 0x40, Local0)
           //}

           //Bit7
           If(LEqual(One, BLEG))
           {
             Or(Local0, 0x80, Local0)
           }

           //Bit8
           If(LEqual(One, BATF))
           {
             Or(Local0, 0x0100, Local0)
           }

           //Bit9
           If(LEqual(Zero, BTSM))
           {
             Or(Local0, 0x200, Local0)
           }

           //Bit10
           //SECONDBATTERY, if support, need implement this with EC

           //Bit11
           If(LEqual(One, BUSG))
           {
             Or(Local0, 0x0800, Local0)
           }

           //Bit12
           //SECONDBATTERY, if support, need implement this with EC

           //Bit13
           //SECONDBATTERY, if support, need implement this with EC

           //Bit14
           //SECONDBATTERY, if support, need implement this with EC

           //Bit15-Bit16
           If(LEqual(0, ADPI))
           {// normal adapter
             And(Local0, 0xFFFE7FFF, Local0)
           }
           If(LEqual(1, ADPI))
           {// illegal adapter
             Or(Local0, 0x8000, Local0)
           }
           If(LEqual(2, ADPI))
           {// illegal adapter
             Or(Local0, 0x10000, Local0)
           }

           //Bit17 report if support quick charge: 0 non-support, 1 support
    If(LEqual(1, EC0_SCOPE.EC0.QCBX)){
        Or(Local0, 0x20000, Local0)
    }
           //Bit18
           //LCFCTODO: If your project support ship mode, please set Bit18
           Or(Local0, 0x040000, Local0)

           //Bit19
           //Always support currently.


           //Bit20
           //SECONDBATTERY, if support, need implement this with EC

           //Bit21
           //SECONDBATTERY, if support, need implement this with EC

           //Bit22
           If(LEqual(One, ESMC))
           {
             Or(Local0, 0x400000, Local0)
           } 
           //Bit23
           //for LBG, always support 55%-60% Conservation mode
           
           Release(EC0_SCOPE.EC0.LfcM)
         }
       }
       Return(Local0)
     }
    //=====================================================================
    //SMTF
    //Read average time to full
    //Input(DWORD) - 0
    //Output(DWORD) (Input index = 0) Average time (minutes) to
    //full charge capacity for the main battery.
    //Output(DWORD) (Input index = 1) Average time (minutes) to
    //full charge capacity for the second battery.
    //Time = ((Fcc-RSOC)*10*1000*60)/pV*AC minutes
    //======================================================================
    Name (VBST, 0)   // Battery Status
    Name (VBAC, 0)   // Battery Average Current
    Name (VBPR, 0)   // Battery Present Rate
    Name (VBRC, 0)   // Battery Remaining Capacity
    Name (VBPV, 0)   // Battery Present Voltage
    Name (VBFC, 0)   // Battery Full Capacity
    Name (VBCT, 0)   // Battery Charge Average Time
    Method(SMTF, 1, NotSerialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          If(LEqual(Arg0, 0x0))
          {
            If(LEqual(EC0_SCOPE.EC0.B1FV, Zero))
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0xFFFF)
            }

            If(LEqual(EC0_SCOPE.EC0.B1AC, Zero))
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0xFFFF)
            }

            Store(EC0_SCOPE.EC0.B1FC,Local0)
            Multiply(Local0,10,Local0)           // mWh(Units:10mWh)
            Store(Local0, VBFC)

            Store(EC0_SCOPE.EC0.B1RC,Local1)
            Multiply(Local1,10,Local1)           // mWh(Units:10mWh)
            Store(Local1, VBRC)

            If(LGreater(VBFC,VBRC))
            {
              Store(EC0_SCOPE.EC0.B1FV,VBPV)
              Store(EC0_SCOPE.EC0.B1AC,VBAC)
              Subtract(Local0, Local1, Local0)
              Store(Multiply(VBAC,VBPV),Local1)
              Store(Multiply(Local0,1000),Local3)
              Store(Multiply(Local3,60), Local3)
              Store(Divide(Local3, Local1),VBCT)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (VBCT)
            }
            else
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0xFFFF)
            }
          }

          If(LEqual(Arg0, 0x1))
          {
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0xFFFF)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return (0xFFFF)
    }

    //=====================================================================
    //SMTE
    //Read average time to empty
    //Input(DWORD) - 0
    //Output(DWORD) (Input index = 0) Average time (minutes) to
    //full charge capacity for the main battery.
    //Output(DWORD) (Input index = 1) Average time (minutes) to
    //full charge capacity for the second battery.
    //Time = (RSOC*10*1000*60)/pV*AC minutes
    //======================================================================
    Name (QBST, 0)   // Battery Status
    Name (QBAC, 0)   // Battery Average Current
    Name (QBPR, 0)   // Battery Present Rate
    Name (QBRC, 0)   // Battery Remaining Capacity
    Name (QBPV, 0)   // Battery Present Voltage
    Name (QBFC, 0)   // Battery Full Capacity
    Name (QBCT, 0)   // Battery Discharge Average Time
    Method(SMTE, 1, NotSerialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          If(LEqual(Arg0, 0x0))
          {
            // Voltage equal to 0
            If(LEqual(EC0_SCOPE.EC0.B1FV, Zero))
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0xFFFF)
            }

            // Average currency euqual to 0
            If(LEqual(EC0_SCOPE.EC0.B1AC, Zero))
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0xFFFF)
            }

            Store(EC0_SCOPE.EC0.B1RC,Local0)
            Multiply(Local0,10,Local0)           // mWh(Units:10mWh)
            Store(Local0, QBRC)

            Store(EC0_SCOPE.EC0.B1FC,Local1)
            Multiply(Local1,10,Local1)           // mWh(Units:10mWh)
            Store(Local1, QBFC)

            If(LGreater(QBFC,QBRC))
            {
              Store(EC0_SCOPE.EC0.B1FV,QBPV)
              If(LEqual(And(EC0_SCOPE.EC0.B1AC, 0x8000), Zero))
              {
                Store(EC0_SCOPE.EC0.B1AC, QBAC)
              }
              else
              {
                Store(Subtract(0xFFFF, EC0_SCOPE.EC0.B1AC), QBAC)
              }
              Store(Multiply(QBAC,QBPV),Local1)
              Store(Multiply(Local0,1000),Local3)
              Store(Multiply(Local3,60), Local3)
              Store(Divide(Local3, Local1),QBCT)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (QBCT)
            }
            else
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0xFFFF)
            }
          }
          If(LEqual(Arg0, 0x1))
          {
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0xFFFF)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return (0xFFFF)
    }

    //===============================================================================
    //  SBMC definition:
    //  refer to Lenovo PM Firmware Spec V1.67
    //===============================================================================
    //Input Arg(DWORD):
    //0x00, End the calibration cycle for the main battery.
    //0x01, Initiate a calibrate cycle for the main battery. KBC need to charge battery
    //      to 100%,then discharge to 1%, After one circle of fully charge and discharge
    //      of battery, end calibration cycle return to the original status. S5/ plug out
    //      AC/plug out battery/ or cancel will end calibration cycle , and return to the
    //      original status. For example, current mode is storage mode, then, battery will
    //      be re-charged to 60%.
    //0x02, Resvered.
    //0x03, Let kbc to apply battery storage mode the main battery.
    //0x04, Reserved
    //0x05, Let kbc to apply longest battery mode for the main battery.
    //0x06, Clear bit of advise user to select battery mode (refer to bit1 and bit2 of
    //      GBMD method), kbc start a new battery mode suggestion.
    //0x07  Let kbc to apply quick charge.
    //0x08  Let kbc to apply normal charge.
    //0x09  Let battery enter into ship mode.
    //0x0a, End the calibration cycle for the second battery.
    //0x0b, Initiate a calibrate cycle for the second
    //      battery. KBC need to charge battery to
    //      100%,then discharge to 1%, After one circle of
    //      fully charge and discharge of battery, end
    //      calibration cycle return to the original
    //      status .S5/ plug out AC/plug out battery/ or
    //      cancel will end calibration cycle , and return
    //      to the original status. For example, current
    //      mode is storage mode, then, battery will be re-
    //      charged to 60%.
    //0x0c, Reserved
    //0x0d, Let kbc to apply battery storage mode for the second battery.
    //0x0e, Reserved
    //0x0f, Let kbc to apply longest battery mode for the second battery.
    //0x10, Let battery exit ship mode.
     Method(SBMC, 1, NotSerialized)
     {
       If(EC0_SCOPE.EC0.ECAV){
         If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
           //sub function 0x00
           If(LEqual(Arg0, Zero))
           {
             Store(0x0, CDMB)
             Store(One, EDCC)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(Zero)
           }

           //sub function 0x01
           If(LEqual(Arg0, One))
           {
             Store(One, CDMB)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(Zero)
           }

           //sub function 0x02
           //reserved

           //sub function 0x03
           If(LEqual(Arg0, 0x03))
           {
             Store(One, BTSM)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(Zero)
           }

           //sub function 0x04
           //reserved

           //sub function 0x05
           If(LEqual(Arg0, 0x05))
           {
             store(0x0, BTSM)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(Zero)
           }

           //sub function 0x06
           //not support currently

           //sub function 0x07 Let kbc to apply quick charge
           If(LEqual(1, EC0_SCOPE.EC0.QCBX)){//if battery support quick charge
             If(LEqual(Arg0, 0x07))
             {
               Store(One, QCHO)
               Store(Zero, BTSM)
               Release(EC0_SCOPE.EC0.LfcM)
               Return(Zero)
             }
    }


           //sub function 0x08 Let kbc to apply normal charge
           If(LEqual(1, EC0_SCOPE.EC0.QCBX)){//if battery support quick charge
             If(LEqual(Arg0, 0x08))
             {
               Store(Zero, QCHO)
               Release(EC0_SCOPE.EC0.LfcM)
               Return(Zero)
             }
           }

           //sub function 0x09
           If(LEqual(Arg0, 0x09))
           {
             store(One, ESMC)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(Zero)
           }

           //sub function 0x0A
           //SECONDBATTERY, if support, need implement this with EC

           //sub function 0x0B
           //SECONDBATTERY, if support, need implement this with EC

           //sub function 0x0C
           //reserved

           //sub function 0x0D
           //SECONDBATTERY, if support, need implement this with EC

           //sub function 0x0E
           //reserved

           //sub function 0x0F
           //SECONDBATTERY, if support, need implement this with EC

           //sub function 0x10
           If(LEqual(Arg0, 0x10))
           {
             store(Zero, ESMC)
             Release(EC0_SCOPE.EC0.LfcM)
             Return(Zero)
           }
           Release(EC0_SCOPE.EC0.LfcM)
         }
       }
       Return(Zero)
     }

    //MHCF : Enable/Disable Battery F/W Update
    //Arguments:
    //      Arg0 = DWORD
    //         bit4:0  = Reserved (must be 0)
    //         bit5    = Enable(1)/Disable(0) Battery F/W Update for Primary Battery
    //         bit6    = Enable(1)/Disable(0) Battery F/W Update for Secondary Battery
    //         bit31:7 = Reserved (must be 0)
    //Return:
    //      Same with Arg0
    Method(MHCF, 1, NotSerialized)
    {
      Store(0x78, P80B)
      Store(Arg0, Local0)
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          And(Local0, 0x20, Local0)
          ShiftRight(Local0, 0x05, Local0)
          Store(Local0, RMBT)
          Sleep(20)           // Wait EC to conmunicate with battery
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return(Local0)
    }

    //MHPF : Process Battery F/W Update
    //Arguments:
    //      Arg0 = 37-byte Buffer, follow SMBus in interface in EC(H8)
    //         Byte 00h = SMB_PRTCL
    //         Byte 01h = SMB_STS
    //         Byte 02h = SMB_ADDR
    //         Byte 03h = SMB_CMD
    //         Byte 04h = SMB_DATA:
    //         Byte 23h = SMB_DATA
    //         Byte 24h = SMB_BCNT
    //Return:
    //      Same with Arg0
    Method(MHPF, 1, NotSerialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          Name(BFWB, Buffer(0x25){})
          CreateByteField(BFWB,Zero,FB0)
          CreateByteField(BFWB,One,FB1)
          CreateByteField(BFWB,0x02,FB2)
          CreateByteField(BFWB,0x03,FB3)
          CreateField(BFWB,0x20,0x0100,FB4)
          CreateByteField(BFWB,0x24,FB5)
          If(LLessEqual(SizeOf(Arg0), 0x25))
          {
            If(LNotEqual(SMPR, Zero))
            {
              Store(SMST, FB1)
            }
            Else
            {
              Store(Arg0, BFWB)
              Store(FB2, SMAD)
              Store(FB3, SMCM)
              Store(FB5, BCNT)
              Store(FB0, Local0)
              If(LEqual(And(Local0, One), Zero))
              {
                Store(FB4, SMDA)
              }
              // Reset SMBus Status data
              Store(0, SMST)
              Store(FB0, SMPR)
             //Set flag to EC to notify EC data are ready now since SMB protocol is via MMIO,EC don't know this.
              Store(0x80, BTFW)
              Store(0x03E8, Local1)
              While(Local1)
              {
                Sleep(One)
                Decrement(Local1)
                If( LOr(LAnd(SMST,0x80), LEqual(SMPR,0x00))) {
                  Break
                }
              }
              Store(FB0, Local0)
              If(LNotEqual(And(Local0, One), Zero))
              {
                Store(SMDA, FB4)
              }
              Store(SMST, FB1)
            // Check SMBus transfer time out?
              If( LOr(LEqual(Local1,0), LNot(LAnd(SMST,0x80)) ) )
              {
              #ifdef WinDbg_EM_Method_MHPF
                If(LEqual(And(FB0, 0x01), 0))
                {
                  Store("- MHPF Write Time Out-", Debug)
                }
                Else
                {
                  Store("- MHPF Read Time Out-", Debug)
                }
                Store(BFWB,Debug)
                Store(FB0, Debug)
                Store(SMST,Debug)
              #endif
                // Reset EC SMBus Protocol data
                Store(0, SMPR)
                //Send fail 0x92 to App
                Store( 0x92, FB1)
              }
            }
            Release(EC0_SCOPE.EC0.LfcM)
            Return(BFWB)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
    }

    //MHIF : Get Battery F/W Information
    //Arguments:
    //      Arg0 = DWORD
    //         bit0    = Primary Battery (0)/Secondary Battery (1)
    //         bit31:1 = Reserved (must be 0)
    //Return:
    //      10-byte Buffer, follow Information Area interface in EC(H8)
    Method(MHIF, 1, NotSerialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          Store(0x50,P80B)
          If(LEqual(Arg0, 0x0))
          {
            Name(RETB, Buffer(0x0A){})
            Name(BUF1, Buffer(0x08){})
            store(FWBT, BUF1)
            // Prepare Byte field
            CreateByteField(BUF1, 0x00, FW0)
            CreateByteField(BUF1, 0x01, FW1)
            CreateByteField(BUF1, 0x02, FW2)
            CreateByteField(BUF1, 0x03, FW3)
            CreateByteField(BUF1, 0x04, FW4)
            CreateByteField(BUF1, 0x05, FW5)
            CreateByteField(BUF1, 0x06, FW6)
            CreateByteField(BUF1, 0x07, FW7)
            // Set Firmware Update Status
            Store(FUSL, Index(RETB, Zero))
            Store(FUSH, Index(RETB, One))
            // Set Firmware version.
            Store(FW0, Index(RETB, 0x02))
            Store(FW1, Index(RETB, 0x03))
            Store(FW2, Index(RETB, 0x04))
            Store(FW3, Index(RETB, 0x05))
            Store(FW4, Index(RETB, 0x06))
            Store(FW5, Index(RETB, 0x07))
            Store(FW6, Index(RETB, 0x08))
            Store(FW7, Index(RETB, 0x09))
            Release(EC0_SCOPE.EC0.LfcM)
            Return(RETB)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
    }

    //===============================================================================
    //  GSBI definition:
    //    Read battery information. If the data is invalid, it should
    //    be filled in 0xFF.
    //    NOTE: Please carefully consider the return values from Smart
    //    Battery commands, some of them (e.g. the units of
    //    DesignCapacity(), FullChargeCapacity() and
    //    RemainingCapacity()) affected by the BatteryMode()and
    //    CAPACITY_MODE bit setting, however the units we need here
    //    are definitely, so please check the return value of
    //    BatteryMode() and CAPACITY_MODE bit setting and do some
    //    convertion if needed.
    //  Input Arg(DWORD):
    //  0x01, main battery.
    //  0x02, second battery

    Method(GSBI, 1, NotSerialized)
    {
      Name(BIFB, Buffer(83) {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                             0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                             0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                             0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                             0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                             0xFF, 0xFF, 0xFF
                             })
      CreateWordField (BIFB,  0, DCAP) // design capacity, Units : 10mWh
      CreateWordField (BIFB,  2, FCAP) // full charge capacity, estimated full charge capacity in 10mWh, Units: 10mWh
      CreateWordField (BIFB,  4, RCAP) // Remaining Capacity, Units: 10 mWh
      CreateWordField (BIFB,  6, ATTE) // AverageTimeToEmpty, Units: minutes
      CreateWordField (BIFB,  8, ATTF) // AverageTimeToFull, Units: minutes
      CreateWordField (BIFB, 10, BTVT) // Voltage, battery terminal voltage in milli-volts, Units: mV
      CreateWordField (BIFB, 12, BTCT) // returns the current being supplited through the battery's terminals(mA), Units: mA
                                       // 0 ~ 32767 mA for charge or 0 ~ -32768 for discharge.
      CreateWordField (BIFB, 14, BTMP) // Temperature, units: 0.1K
      CreateWordField (BIFB, 16, MDAT) // manufacturer date, bit0~bit4 days, Bit5~bit8 month, bit9~bit15 years.
                                       // The date is packed in the following fashion: (year - 1980)*512 + month*32+ days.
      CreateWordField (BIFB, 18, FUDT) // first used date, bit0~bit4 days, Bit5~bit8 month, bit9~bit15 years.
                                       // The date is packed in the following fashion: (year - 1980)*512 + month*32+ days.
      CreateWordField (BIFB, 20, DVLT) // Design voltage, Units : mV
      CreateField (BIFB, 176, 80, DCHE) // Device chemistry
      CreateField (BIFB, 256, 64, DNAM) // Device Name
      CreateField (BIFB, 320, 96, MNAM) // manufacturer name
      CreateField (BIFB, 416, 184, BRNB)// Bar-Code number
      CreateQWordField (BIFB, 75, BFW0) // Battery firmware version

      //0x01 - main battery?
      If(Lor(LEqual(Arg0, 0x00),LEqual(Arg0, 0x01)))
      {
        If(EC0_SCOPE.EC0.ECAV){
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
            // Design capacity
            Store(EC0_SCOPE.EC0.B1DC, Local0)
            Multiply(Local0, 10, Local0)           // mWh(Units:10mWh)
                Store (Local0, DCAP)

            // Full charge capacity
            Store(EC0_SCOPE.EC0.B1FC, Local0)
            Multiply(Local0, 10, Local0)           // mWh(Units:10mWh)
            Store (Local0, FCAP)

            // Remaining Capacity
            Store(EC0_SCOPE.EC0.B1RC, Local0)
            Multiply(Local0, 10, Local0)           // mWh(Units:10mWh)
            Store (Local0, RCAP)

            // AverageTimeToEmpty
            Store(SMTE(0), ATTE)

            // AverageTimeToFull
            Store(SMTF(0), ATTF)

            // Voltage
            Store(EC0_SCOPE.EC0.B1FV, BTVT)

            // Current
            Store(EC0_SCOPE.EC0.B1AC, BTCT)

            // Temperature
            Store(EC0_SCOPE.EC0.B1AT, Local0) // unit: Celsius
            Add(Local0, 273, Local0)               // Degree C + 273 -> Degree K
            Multiply(Local0, 10, Local0)           // convert unit from K to 0.1k
            Store(Local0, BTMP)

            // Manufacturer date
            Store(EC0_SCOPE.EC0.B1DA, MDAT)

            // First used date
            If (LNotEqual(EC0_SCOPE.EC0.BFUD, 0))
            {
              Store(EC0_SCOPE.EC0.BFUD, FUDT)
            }

            // Design voltage
            Store(EC0_SCOPE.EC0.B1DV, DVLT)

            // Device chemistry
            Name (DCH0, Buffer(10) {0x00})
            Name (DCH1, "LION")
            Name (DCH2, "LiP")
            If (LEqual(B1TY, 1))
            {
              Store(DCH1, DCH0)
              Store(DCH0, DCHE)
            }
            Else
            {
              Store(DCH2, DCH0)
              Store(DCH0, DCHE)
            }

            // Device Name.
            Name (BDNT, Buffer(8) {0x00})
            Store(EC0_SCOPE.EC0.BDN0, BDNT)
            Store(BDNT, DNAM)

            // Manufacturer name, for example: "SANYO" +(0x00)*7.
            Name (BMNT, Buffer(12) {0x00})
            Store(EC0_SCOPE.EC0.BMN0, BMNT)
            Store(BMNT, MNAM)

            // Bar-Code number
            Name (BRN0, Buffer(23) {0x00})

            Store(EC0_SCOPE.EC0.BAR1,BRN0)
            Store(BRN0, BRNB)

            // Battery firmware version
            Store(EC0_SCOPE.EC0.FWBT, BFW0)
            Release(EC0_SCOPE.EC0.LfcM)
          }
        }
        Return(BIFB)
      }

      //0x02 - second battery
      If(LEqual(Arg0, 0x02))
      {
        Return(BIFB)
      }

      Return(Zero)
    }

    //===============================================================================
    //  HODD definition:
    //
    //  If the system supports ODD hot plug function, PM Utility
    //  will invoke this method to get ODD status
    //
    //  output param: 0xFF, system don't support ODD hotplug.
    //             0x00, ODD power off.
    //             0x01, ODD power on.
    Method(HODD, 0, NotSerialized)
    {
    // Not support currently.
    }

    //===============================================================================
    //  SODD definition:
    //
    //  If system supports ODD hot plug function, PM Utility will
    //  invoke this method to control ODD power off/on. BIOS must
    //  refer to ACPI specification 3.0 hot-plug devices to notify
    //  OS to stop device then call ejx method to power off ODD.
    //
    //  Arg0: 0x00, ODD power off
    //        0x01, ODD power on
    Method(SODD, 1, Serialized)
    {
    // Not support currently.
    }

    //===============================================================================
    //  GBID definition:
    //Get battery cyclecount and battery firmware version
    //Arguments:
    //  Output Arg = 20-byte Buffer.
    //    Byte 00h-01h: the  Main  battery  cyclecount;
    //    Byte 02h-03h: the  Second  battery  cyclecount;
    //    Byte 04h-0Bh: the  Main  battery  firmware  version;
    //    Byte 0Ch-13h: the  second  battery  firmware  version;
    Method(GBID, 0, Serialized)
    {
      Name(GBUF, Package(4)
      {
        Buffer(0x02){
        0x00, 0x00
        },
        Buffer(0x02){
        0x00, 0x00
        },
        Buffer(0x08){
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        },
        Buffer(0x08){
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        },
      })
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          Store(B1CT, Index(DerefOf(Index(GBUF, Zero)), Zero))
          Store(0x0, Index(DerefOf(Index(GBUF, 1)), Zero))
          Name(BUF1, Buffer(0x08){})
          store(FWBT, BUF1)
          // Prepare Byte field
          CreateByteField(BUF1, 0x00, FW0)
          CreateByteField(BUF1, 0x01, FW1)
          CreateByteField(BUF1, 0x02, FW2)
          CreateByteField(BUF1, 0x03, FW3)
          CreateByteField(BUF1, 0x04, FW4)
          CreateByteField(BUF1, 0x05, FW5)
          CreateByteField(BUF1, 0x06, FW6)
          CreateByteField(BUF1, 0x07, FW7)
          Store(FW0, Index(DerefOf(Index(GBUF, 2)), Zero))
          Store(FW1, Index(DerefOf(Index(GBUF, 2)), 1))
          Store(FW2, Index(DerefOf(Index(GBUF, 2)), 2))
          Store(FW3, Index(DerefOf(Index(GBUF, 2)), 3))
          Store(FW4, Index(DerefOf(Index(GBUF, 2)), 4))
          Store(FW5, Index(DerefOf(Index(GBUF, 2)), 5))
          Store(FW6, Index(DerefOf(Index(GBUF, 2)), 6))
          Store(FW7, Index(DerefOf(Index(GBUF, 2)), 7))
          Store(0x0, Index(DerefOf(Index(GBUF, 3)), Zero))
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return (GBUF)
    }

    // APPC
    // Arg0: 1, bios don't process display toggle event
    //       0, bios process display toggle event
    // AppProcessDisplayToggle: a parameter to store the display
    // when system start or restart, it is 0.
    Name(APDT,0) //display toggle process flag, ODM must initialize it when system restart/start
    Method(APPC, 1, Serialized)
    {
      store(arg0,APDT)
      return (0)
    }

    // DBSL
    // List of supported brightness levels
    // xx:brightness level percent.(from high to
    // low,ex:{100,90,80,70,60,50,40,30,20,10,0})
    // Note:Level 0:  2 -- 5nits
    Method (DBSL, 0, NotSerialized)
    {
      Return (Package(0x10) {201,174,149,126,105,86,69,54,41,30,21,14,9,6,5,0} )
    }

    // SBSL
    // Set special brightness
    // Arg0: 1, set brightness to Lx:   170 -- 180 nits
    //       2, set brightness to Lx:   50 -- 60 nits
    Method(SBSL,1, Serialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          Store(Arg0, Local0)
          If(LEqual(Local0, one))
          {
            Store(0x0E, LCBV)
          }
          If(LEqual(Local0, 0x02))
          {
            Store(0x07, LCBV)
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return(0)
    }

/*

Get or Set Keyboard light. Add support for three level keyboard light.
Input(DWORD)
Output!V(DWORD)
----------------------------------------------------------------------------------------------------------------------------------
Input                          Data                                             Output
----------------------------------------------------------------------------------------------------------------------------------
Command(bit0-bit3)             Data(bit4-bit31)                                 Data(bit0-bit31)
--------------------------------+---------------------------+---------------------------------------------------------------------
KBLC_GET_CAPABILITY(0x01)      Reserved(0)                                      bit0:
                                                                                   0 - failed
                                                                                   1 - success
                                                                                bit1-bit31:
                                                                                   0 - not support keyboard light. 
                                                                                   1 - support two level keyboard light(On/Off)
                                                                                   2 - support three Level keyboard light(Low/High/Off)
                                                                                   3 - support Auto mode
----------------------------------------------------------------------------------------------------------------------------------
KBLC_GET_STATUS (0x02)         Bit4-bit15:
                                   1 - 2 Level Light                            bit0:
                                   2 - 3 Level Light                                0 - failed,
                                   3 - Auto Mode                                    1 - success.
                               bit16~bit31:                                     bit1 - bit15:
                                   Reserved(0)                                      If(Mode = 2level) Then 0: off, 1: on
                                                                                    If(Mode = 3Level) Then 0: off, 1: low, 2: high
                                                                                    If(Mode = Auto Mode) Then 0: off, 1: low, 2: high, 3: Auto
                                                                                bit16:
                                                                                    If(Mode = Auto Mode) Then 0: disable, 1: enable
                                                                                bit17~bit31:
                                                                                    Reserved(0)
----------------------------------------------------------------------------------------------------------------------------------
KBLC_SET_STATUS (0x03)         bit4-bit15:                                       bit0:
                                   1 - 2 Level Light                                 0 - failed,
                                   2 - 3 Level Light                                 1 - success.
                                   3 - Auto mode                                 bit1 - bit15:
                               bit16~bit31:                                          If(Mode = 2level) Then 0: off, 1: on
                                  If( 2 Level Light)                                 If(Mode = 3Level) Then 0: off, 1: low, 2: high
                                      Then 0: off, 1: on                             If(Mode = Auto Mode) Then 0: off, 1: low, 2: high, 3: Auto
                                  If (3 Level Light)                             bit16:
                                      Then 0: off, 1: low, 2: high                   If(Mode = Auto Mode) Then 0: disable, 1: enable
                                  If (Auto Mode)                                 bit17~bit31:
                                      Then 0: off, 1: low, 2: high, 3: Auto          Reserved(0)

*/

    Method(KBLC, 1, NotSerialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){

// keyboard back light get capability
          If(LEqual(Arg0, KBLC_GET_CAPABILITY))
          {
            Store(KBGC, Local0)
            Release(EC0_SCOPE.EC0.LfcM)
            Return(Or(Local0, 1))
          }

// keyboard back light get status
          ElseIf(LEqual(AND (Arg0, 0x000F), KBLC_GET_STATUS ))
          {
            Store(KBGC, Local0)
            AND(Local0, 0xFFFFFFFE, Local0)
            Store(AND (Arg0, 0xFFF0), Local1)
            ShiftRight (Local1, 0x03, Local1) // KBGC is used for success/fail
            If(LNotEqual(Local0, Local1))
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return(0)
            }
            Else
            {
              Store(KBGS, Local3)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local3,1))
            }            
          }

// keyboard back light set status
          ElseIf(LEqual(AND (Arg0, 0x000F), KBLC_SET_STATUS ))
          {
            Store(KBGC, Local0)
            AND(Local0, 0xFFFFFFFE, Local0)
            Store(AND (Arg0, 0xFFF0), Local1)
            ShiftRight (Local1, 0x03, Local1) // KBGC is used for success/fail
            If(LNotEqual(Local0, Local1))
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0)
            }
            Else
            {
              And(Arg0, 0xFFFFFFFF, Local3)
              Store(Local3, KBSS)
              Sleep(300) // need delay for EC to update status register: KBGS
              Store(KBGS, Local3)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local3,1))
            }
          }
        Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return (0x0)
    }
/*  
Get the bios realization information.
Input(DWORD)
Output-(DWORD)
----------------------------------------------------------------------------------------------------------------------------------
Input                          Data                                             Output
----------------------------------------------------------------------------------------------------------------------------------
Command(bit0-bit3)             Data(bit4-bit31)                                 Data(bit0-bit31)
--------------------------------+---------------------------+---------------------------------------------------------------------
BSIF_GET_VERSION(0x01)         1-bios realization spec version.                 bit0:
                                                                                   0 - failed
                                                                                   1 - success
                                                                                bit1-bit3:
                                                                                   error code
                                                                                bit4-bit31:
                                                                                   if input Data =1, the bit4-bit31:
                                                                                   The spec version, such as (183)
*/
    Method(BSIF, 1, NotSerialized)
       {
        If(EC0_SCOPE.EC0.ECAV){
         If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          If(LEqual(AND (Arg0, 0x000F), 0x01 )){ //Get input command , BSIF_GET_VERSION =1
            Store(Arg0, Local0)
            ShiftRight (Local0, 4, Local0)  //get input data
            Store(0x00, Local1) //init Local1 to 0
            if(LEqual(Local0, 0x01)){ //input data is 1
              Or(Local1,PM_Version,Local1)
              Return (Or(Local1,1)) //return PM version, bit0=1 is success.
              }
           }

         Release(EC0_SCOPE.EC0.LfcM)
         }
        }
        Return (0x0)
       }
/*
    Get or Set Battery Mode. 
    Input(DWORD)
    Output!V(DWORD)
    ----------------------------------------------------------------------------------------------------------------------------------
    Input                          Data                                             Output
    ----------------------------------------------------------------------------------------------------------------------------------
    Command(bit0-bit3)             Data(bit4-bit31)                                 Data(bit0-bit31)
    --------------------------------+---------------------------+---------------------------------------------------------------------
    BTMC_GET_STATUS(0x01)          1:battery temporary mode                         bit0:
                                   2.battery health level                              0 - failed
                                   3.battery health tips                               1 - success
                                   4.battery lifespan                               bit1-bit3:
                                                                                       error code.
                                                                                    bit4-bit30:
                                                                                       if input is 1:battery temporary mode 
                                                                                       return current enable mode.
                                                                                        0 - permanent mode
                                                                                        1 - PI mode
                                                                                        2 - DLS mode
                                                                                       if input is 2:battery health level
                                                                                       return current battery health level.

                                                                                       if input is 3:battery health tips
                                                                                       return current battery health tips.

                                                                                       if input is 4:battery lifespan
                                                                                       return current battery lifespan.
                                                                                     bit31: reserved(0)
    ----------------------------------------------------------------------------------------------------------------------------------
    BTMC_SET_STATUS (0x02)         Bit4-bit30: set mode
                                       0 - permanent mode                            bit0:
                                       1 - PI mode                                    0 - failed,
                                       2 - DLS mode                                   1 - success.
                                   bit31: Valid flag                                 bit1 - bit3:
                                       0 - off                                        error code.
                                       1 - on                                        bit4 - bit30:
                                                                                       current enable mode.
                                                                                       0 - permanent mode
                                                                                       1 - PI mode
                                                                                       2 - DLS mode
                                                                                     bit31:
                                                                                        Reserved(0)
    ----------------------------------------------------------------------------------------------------------------------------------
    BTMC_GET_CAP (0x03)            bit4-bit31:                                       bit0:
                                       1 - battery temporary mode                     0 - failed,
                                       2 - smart battery 2.0                          1 - success.
                                                                                     bit1 - bit31:
                                                                                      If Input is 1:battery temporary mode
                                                                                       0 = not support
                                                                                       1 = support
                                                                                       bit1 - PI mode
                                                                                       bit2 - DLS mode
                                                                                      If Input is 2:smart battery 2.0
                                                                                       0 = not support
                                                                                       1 = support smart battery 2.0
*/
    Method(BTMC, 1, NotSerialized)
    {
      If(EC0_SCOPE.EC0.ECAV){
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          // BTMC_GET_STATUS
          If(LEqual(AND (Arg0, 0x000F), 0x01 ))
          {
            Store(Arg0, Local0)
            ShiftRight (Local0, 4, Local0)  //get input data
            Store(0x00, Local1)             //init Local1 to 0
            If(LEqual(Local0, 1))           //battery temporary mode
            {
              If(LEqual(TPMD, 0))           //0 - Permanent mode
              {
                Store(0x00,Local1)  
              }
              ElseIf(LEqual(TPMD, 1))       //1:Temporary Mode [0 - DLS mode,  1 - PI mode]
              {
                If(LEqual(PDMD, 1))         //0 - PI mode
                {
                  Store(0x01,Local1)  
                }
                ElseIf(LEqual(PDMD, 0))     //1 - DLS mode
                {
                  Store(0x02,Local1)  
                } 
              }
              ShiftLeft(Local1, 4, Local1)  //the battery mode offset 0-permanent mode,1 - PI mode,2 - DLS mode
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local1,1))         //1:success
            }
            ElseIf(LEqual(Local0, 2))       //battery health level
            {
              Store(BTHT ,Local1)           //the battery health level offset
              ShiftLeft(Local1, 4, Local1) 
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local1,1))         //1:success
            }
            ElseIf(LEqual(Local0, 3))       //battery health tips
            {
              Store(BTTP ,Local1)           //the battery health tips offset
              ShiftLeft(Local1, 4, Local1)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local1,1))         //1:success
            }
            ElseIf(LEqual(Local0, 4))       //battery lifespan
            {
              Store(BTLF ,Local1)           //the battery health lifespan offset
              ShiftLeft(Local1, 4, Local1) 
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local1,1))         //1:success
            }
            Else
            {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Local1)
            }
          }
          // BTMC_SET_STATUS
          ElseIf(LEqual(AND (Arg0, 0x000F), 0x02 ))
          {
            Store(0x01, Local0) //The battery does not provide a set interface, bit1-bit3 :0x01 mean not support.
            ShiftLeft(Local0, 1, Local0)    
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local0)
          }
          // BTMC_GET_CAP
          ElseIf(LEqual(AND (Arg0, 0x000F), 0x03 ))
          {
            Store(Arg0, Local0)
            ShiftRight (Local0, 4, Local0)   //get input data
            Store(0x00, Local1)              //init Local1 to 0
            Store(0x00, Local2)              //init Local1 to 0
            If(LEqual(Local0, 1)) {          //battery temporary mode
              If(LEqual(TPMD, 1))            //temporary mode
              {
                Store(0x01, Local1)          //support temporary mode
                If(LEqual(BTSB, 0x03))       //support smart battery 2.0
                {
                  Store(0x03, Local2)        //support DLS & PI
                }
              }
              ShiftLeft(Local1, 1, Local1)
              ShiftLeft(Local2, 2, Local2)
              Or(Local1,Local2,Local1)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local1,1))         //success & support temporary mode
            } 
            ElseIf(LEqual(Local0, 2))       //smart battery 2.0
            { 
              If(LEqual(BTSB, 0x03))
              {
                Store(0x01, Local1)         //support smart battery 2.0
              }
              Else
              {
                Store(0x00, Local1)         //not support smart battery 2.0
              } 
              ShiftLeft(Local1, 1, Local1)
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Or(Local1,1))         //1:success
            }
          }
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return (0x0)
    }

    // Switch thermal table
    // Arg0: 0, switch to normal thermal table
    //       1, switch to the thermal table for PM
    //          utility.
    Method(STHT, 1, Serialized)
    {
      return (0)
    }

    Include("Atm.asi")
  }
}

