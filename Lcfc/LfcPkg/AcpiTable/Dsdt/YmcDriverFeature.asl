
//*****************************************************************************
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
/*++
Abstract:
  Lenovo YMC Driver spec
  
History:
  Date           Name          Version    Change Notes
  2019.02.11     Sea Zhou      V1.00      Initial Release 

Module Name:
  YmcDriverFeature.asl

Note:
  You must take care about the prompt string "LCFCTODO:" and customize your
  project.

Support Version:
   25_YMC Driver Specification _V0.7_20180202
   Only implemented the GetUsageMode method, no GetXaxisValue, GetYaxisValue and others
--*/

  Device(CYMC)
  {
    Name(_HID, EISAID("YMC2017"))
    Name(_UID, 0)
    Method(_STA) {
      Return(0x0f)
    }
    
    //
    // _WDG evaluates to a data structure that specifies the data
    // blocks supported by the ACPI device.
    //
    
    Name(_WDG, Buffer() {
      //{180C0A86-2696-4B93-87D0-2E52290B89DD}
      0x86, 0x0A, 0x0C, 0x18, 0x96, 0x26, 0x93, 0x4B, 0x87, 0xD0, 0x2E, 0x52, 0x29, 0x0B, 0x89, 0xDD,
      65, 66, // Object ID (AB)
      1,      // Instance Count
      0x02,   // Flags (WMIACPI_REGFLAG_METHOD)
      
      //{E3BF55C7-CECB-4990-BC50-B5176A5227D6}
      0xC7, 0x55, 0xBF, 0xE3, 0xCB, 0xCE, 0x90, 0x49, 0xBC, 0x50, 0xB5, 0x17, 0x6A, 0x52, 0x27, 0xD6, 
      0x90, 0, // Notfy ID
      1,       // Instance Count
      0x08,    // Flags (WMIACPI_REGFLAG_EVENT)
      
      0x21, 0x12, 0x90, 0x05, 0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0, 0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
      66, 68, // Object ID (BD)
      1,      // Instance Count
      0x00,   // Flags
      })
      
    //
    // ULONG Method data block
    // Arg0 has the instance being queried
    // Arg1 has the method id
    // Arg2 has the data passed
    
    Method(WMAB, 3) {       
      //LCFCTODO: please follow project design to report correct data
      Return (0)  // bit[31…16]:Device Type 0:Default(Lenovo Yoga)
                  // bit[15…0]:Mode Definition
                  //   0:unknow mode
                  //   1:Laptop mode
                  //   2:Tablet mode
                  //   3:Stand mode
                  //   4:Tent mode
    }
    
    Name(WQBD, Buffer(991) {
      0x46,0x4f,0x4d,0x42,0x01,0x00,0x00,0x00,0xac,0x04,0x00,0x00,0xb8,0x17,0x00,0x00,
      0x44,0x53,0x00,0x01,0x1a,0x7d,0xda,0x54,0x28,0xc3,0x8b,0x00,0x01,0x06,0x18,0x42,
      0x10,0x05,0x10,0x0a,0x26,0x81,0x42,0x04,0x0a,0x40,0xa4,0x28,0x30,0x28,0x0d,0x20,
      0x92,0x03,0x21,0x17,0x4c,0x4c,0x80,0x08,0x08,0x79,0x15,0x60,0x53,0x80,0x49,0x10,
      0xf5,0xef,0x0f,0x51,0x12,0x1c,0x4a,0x08,0x84,0x24,0x0a,0x30,0x2f,0x40,0xb7,0x00,
      0xc3,0x02,0x6c,0x0b,0x30,0x2d,0xc0,0x31,0x24,0x95,0x06,0x4e,0x09,0x2c,0x05,0x42,
      0x42,0x05,0x28,0x17,0xe0,0x5b,0x80,0x76,0x44,0x49,0x16,0x60,0x19,0x46,0x04,0x1e,
      0x45,0x64,0xa3,0x71,0x68,0xec,0x30,0x2c,0x13,0x4c,0x83,0x38,0x8c,0xb2,0x91,0x45,
      0xe0,0x09,0x75,0x2a,0x40,0xae,0x00,0x61,0x02,0xc4,0xa3,0x0a,0xa3,0x39,0x28,0x32,
      0x87,0xd0,0x18,0x31,0x63,0x22,0xb0,0x9d,0x63,0x6b,0x14,0xa7,0x51,0xb8,0x00,0xe9,
      0x18,0x1a,0xc1,0x71,0x1d,0x83,0xc1,0x82,0x1c,0x8a,0x21,0x0a,0x12,0x60,0x16,0x43,
      0x9e,0xa7,0x21,0x94,0xa3,0x88,0x72,0x52,0x46,0xe9,0x50,0x80,0x68,0x24,0x41,0x06,
      0x8b,0x63,0x8b,0xa8,0x42,0xf6,0x04,0x12,0x1c,0xd0,0x91,0x60,0x64,0x40,0x08,0xb1,
      0xf5,0x21,0x12,0x02,0xbb,0x9f,0x92,0xb4,0x09,0x30,0x26,0x40,0xd9,0xa0,0x34,0x9e,
      0x10,0x11,0xda,0x02,0x12,0x54,0x71,0x02,0x9c,0xa1,0x68,0x68,0xd1,0xe2,0xc4,0x88,
      0x12,0x28,0x4a,0x83,0x88,0x42,0xa9,0x71,0x7c,0x04,0x38,0x48,0x64,0x7b,0x43,0x10,
      0x6a,0xb4,0x98,0xed,0x8f,0x9d,0x39,0x38,0x10,0xb0,0x48,0xa3,0x41,0x1d,0x02,0x12,
      0x3c,0x14,0xf8,0x40,0x70,0x50,0xc7,0x79,0x52,0x67,0xe6,0x89,0x9e,0x57,0x9d,0xa7,
      0x01,0x32,0x6e,0x36,0x34,0xb3,0xeb,0x78,0x00,0xc1,0x35,0xa0,0xee,0x05,0x09,0xfe,
      0xff,0x4f,0x05,0x9e,0x65,0x38,0xcc,0x10,0x3d,0xf2,0x70,0x27,0x70,0x88,0x0c,0xd0,
      0xc3,0x7a,0x20,0xc0,0x4e,0xfa,0x64,0x0e,0xbc,0xd4,0xb9,0xeb,0x64,0x70,0xd0,0x09,
      0x8e,0xc7,0xc7,0x00,0x8f,0xdc,0x04,0x96,0x3f,0x08,0xd4,0xc8,0x0c,0xed,0x29,0x9e,
      0xd6,0xdb,0x80,0xe7,0x6f,0x02,0x8b,0x3d,0x4b,0xd0,0xf1,0x80,0x5f,0xf1,0xd1,0x40,
      0x08,0xaf,0x0c,0x9e,0xaf,0x21,0x75,0xb4,0x90,0x93,0xf1,0xa0,0xce,0x0b,0x3e,0x23,
      0xf0,0x5b,0x40,0xc8,0x33,0xb7,0xd6,0xf3,0x03,0x21,0x18,0x19,0x42,0xff,0x0c,0x8b,
      0x1a,0x39,0x3d,0x55,0xf0,0x03,0x05,0xc3,0xe5,0x83,0x39,0x27,0x9f,0x3b,0x3c,0x36,
      0xf8,0xa7,0x09,0xe0,0x38,0x68,0xf8,0x17,0x0a,0xa3,0xfa,0xfc,0xf0,0x0a,0x91,0xc0,
      0x58,0x81,0xb1,0xa3,0xb6,0x5f,0x01,0x08,0xc1,0xcf,0xe8,0xa9,0xe0,0xf1,0xe3,0xff,
      0x1f,0xe1,0xa0,0x8c,0xfe,0xf4,0xd0,0xe7,0xb0,0x05,0x11,0xad,0x47,0x28,0x01,0x19,
      0x23,0x4e,0xa5,0x40,0x82,0x09,0x16,0xc8,0x23,0x49,0x60,0x91,0x23,0x45,0x0f,0x82,
      0x03,0x9e,0xc3,0x21,0x05,0x8d,0x7d,0x32,0x51,0xce,0xe3,0x98,0x7c,0x40,0x31,0xc2,
      0x31,0x3d,0x97,0x9c,0xa5,0x35,0x4f,0x52,0xf3,0x7a,0x1c,0x38,0x43,0x8f,0xdb,0x43,
      0xc0,0x9c,0x2b,0x3c,0x04,0x3e,0x80,0x56,0xa7,0x48,0xa6,0x73,0x62,0xb8,0xa3,0x0c,
      0x9f,0x32,0x6e,0x00,0x18,0x91,0x4a,0x40,0x98,0xf8,0x97,0x81,0xa4,0x3d,0x11,0x28,
      0x8c,0x0f,0x37,0x80,0x2b,0xa0,0xe7,0x07,0xf0,0x9c,0x0d,0xe0,0x62,0x9f,0xfc,0xa3,
      0x41,0x88,0xa7,0x87,0xe8,0xe7,0x72,0x32,0x27,0x10,0xe1,0xd1,0x06,0xf6,0xff,0xff,
      0x68,0xe3,0x09,0x55,0x88,0xa1,0x21,0x44,0x78,0x97,0x09,0xf1,0x4a,0xf3,0x3a,0xf0,
      0xfe,0xf0,0x46,0xf3,0x54,0xe3,0x81,0xbd,0x41,0x84,0x78,0xb9,0xf1,0xd1,0xe6,0x38,
      0x7c,0xb6,0x89,0xf2,0x58,0x13,0x38,0x48,0x84,0x88,0x11,0x62,0x05,0x0f,0x1f,0x21,
      0x48,0xe8,0x47,0x1b,0x16,0xef,0x38,0xa0,0x53,0x88,0x8f,0x36,0x00,0x3f,0x8e,0x08,
      0x8f,0x25,0xe0,0x3f,0x2b,0xf0,0x53,0x09,0xd8,0xff,0xff,0xa7,0x12,0x9c,0x00,0x1f,
      0x0c,0x3d,0x0c,0xd4,0x8b,0x95,0x8d,0xc5,0xfa,0x50,0x40,0xe5,0x9e,0x12,0x34,0x9f,
      0xb8,0x31,0x9f,0x4b,0x9e,0x00,0x22,0xbf,0xd5,0xf8,0x6c,0xc3,0x09,0x2c,0x10,0x50,
      0x07,0x38,0x2e,0x8d,0x42,0xc2,0x20,0x34,0x22,0x9f,0x3e,0x08,0x1c,0x05,0xf1,0x11,
      0xc0,0x31,0x21,0x74,0x30,0xc2,0x9d,0x01,0x7c,0x15,0xe2,0x04,0x8e,0x75,0x2a,0xa2,
      0x47,0x06,0xfc,0x5c,0x0e,0xed,0xcc,0x3c,0x0b,0xcb,0x3b,0x7b,0x80,0xe6,0x10,0x84,
      0x99,0x08,0x66,0x18,0x3e,0x2b,0xf1,0x11,0xf8,0x66,0xf0,0x4a,0x63,0x02,0x76,0x0e,
      0x02,0x9f,0xc0,0x13,0x02,0x28,0x80,0x7c,0x1f,0xf0,0xe5,0xe0,0x99,0x80,0xcd,0xe1,
      0x39,0xc7,0xa7,0x1c,0x06,0xcf,0xe3,0x8c,0x8a,0xca,0x1a,0x17,0x6a,0x98,0x3e,0xc6,
      0x30,0xec,0xd7,0x01,0x1f,0x9a,0x8e,0xd1,0x67,0x2a,0x0c,0xac,0x07,0xce,0x61,0x8d,
      0x16,0xf6,0x78,0x1f,0x24,0x7c,0x09,0xf1,0xcc,0x8c,0x11,0xd6,0x83,0x87,0xff,0xff,
      0x1f,0x3c,0x7c,0x49,0x67,0x06,0xb4,0xe8,0x33,0x03,0xe2,0xda,0x11,0xf0,0x05,0x27,
      0xfc,0x29,0x3c,0x84,0xb1,0x79,0xbf,0x33,0x58,0xe6,0x99,0x01,0x75,0x6c,0xb0,0xbc,
      0x33,0x03,0x4a,0x10,0x1c,0x05,0xf1,0x99,0xc1,0x61,0xcf,0x0c,0xd0,0xc3,0x9d,0x19,
      0x40,0x71,0xde,0xc0,0x8f,0x13,0xde,0xe8,0xd8,0x12,0xcf,0x0e,0x03,0x73,0x78,0xb8,
      0x63,0x03,0xe0,0x24,0xd4,0xb1,0x01,0x74,0xa7,0x00,0x1f,0x1b,0xc0,0x35,0x7e,0xff,
      0xff,0xc7,0x0f,0x1c,0x4e,0x0e,0x30,0x27,0xfe,0xe4,0x00,0x38,0x9b,0x1d,0xe6,0xe4,
      0x00,0x18,0x85,0xe5,0x63,0xe7,0xff,0xff,0x93,0x03,0xf0,0x19,0x3f,0xbc,0x93,0x03,
      0x4c,0xb5,0x27,0x07,0x40,0xda,0xec,0x30,0x27,0x07,0xc0,0xde,0xff,0xff,0xe4,0x00,
      0x67,0xec,0x3e,0x39,0x00,0xb7,0xf1,0xc3,0x13,0x75,0x72,0x40,0x71,0x58,0xf6,0xc9,
      0x01,0x31,0xce,0xa7,0x43,0xcf,0xc7,0x77,0xca,0xc8,0xb8,0xa3,0x03,0x60,0xe6,0xd4,
      0xf9,0x3e,0xea,0x33,0x98,0xa1,0xde,0x94,0x3d,0xc4,0xd7,0x41,0x0f,0x21,0xdc,0xd3,
      0x23,0x3b,0x3a,0x00,0x1e,0xfe,0xff,0x47,0x07,0x70,0x5b,0x38,0x3a,0x00,0xad,0xf1,
      0xc3,0x17,0x76,0x74,0x40,0x0b,0x3f,0x3a,0x40,0xb1,0x76,0x74,0x40,0x0b,0x3d,0x3a,
      0xa0,0x24,0x52,0xe8,0xec,0xe0,0xa3,0x03,0x97,0x04,0x47,0x41,0x7c,0x74,0x70,0xdc,
      0xa3,0x03,0xf4,0x78,0x47,0x07,0x50,0x1c,0x54,0x71,0x63,0x85,0x7b,0x21,0x08,0x65,
      0xb8,0x63,0xf7,0xd9,0x01,0x38,0xff,0xff,0xcf,0x0e,0xe0,0x3d,0x9a,0xfa,0xec,0x00,
      0xae,0x33,0xb9,0xcf,0x0e,0xe0,0x3a,0x00,0x00,0x97,0x63,0x0c,0x38,0xae,0xdb,0x8f,
      0x31,0x80,0xa5,0xc1,0xc2,0x39,0x7c,0xf0,0xb3,0x03,0x38,0xff,0xff,0xc7,0x18,0xc0,
      0xc9,0x89,0xda,0xc7,0x18,0xe0,0x34,0x7e,0xf8,0x0a,0x6d,0xfa,0xd4,0x68,0xd4,0xaa,
      0x41,0x99,0x1a,0x65,0x1a,0xd4,0xea,0x53,0xa9,0x31,0x63,0xa7,0x04,0x1f,0xef,0x3c,
      0x3a,0x07,0x02,0xa1,0xc1,0x28,0x04,0xe2,0x98,0x8f,0x0c,0x81,0x58,0xe4,0xda,0x04,
      0xe2,0xe0,0x0b,0x92,0x91,0xf3,0xb7,0x4d,0x00,0x31,0x11,0xeb,0x13,0x90,0x25,0x81,
      0x68,0xbc,0xc4,0x06,0x10,0x13,0x09,0x22,0x20,0x0b,0x3d,0x8b,0x0b,0xc8,0xb2,0x41,
      0x04,0x64,0xf1,0x34,0x02,0x72,0x14,0x0a,0x01,0x39,0xd2,0x7a,0x04,0xe4,0x80,0x20,
      0x02,0x72,0x50,0x2b,0x40,0x2c,0x36,0x88,0x80,0xfc,0xff,0x07
      })
  }