
//*****************************************************************************
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
/*++
Abstract:
  Lenovo Paper Looking Feature
  
History:
  Date           Name          Version    Change Notes
  2016.01.12     Steven Wang   V1.00      Initial Release 

Module Name:
  PaperLookingFeature.asl

Note:
  You must take care about the prompt string "LCFCTODO:" and customize your
  project.

Support Version:
   23_Lenovo Paper Looking Firmware Spec V0.3
--*/

  Device(WMI1) {
   Name(_HID, EISAID("PNP0C14"))
   Name(_UID, "LPLW") 
   Name(_WDG, Buffer(){
     // {56322276-8493-4ce8-A783-98C991274F5E}
     0x76, 0x22, 0x32, 0x56, 0x93, 0x84, 0xe8, 0x4c, 0xA7, 0x83, 0x98, 0xC9, 0x91, 0x27, 0x4f, 0x5e, 
     0x80, 0,
     0x01, 
     0x08,
     0x21, 0x12, 0x90, 0x05, 0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0, 0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
     66, 65, 
     1, 
     0x00,
   })
   Name(WQBA, Buffer(507) {
     0x46, 0x4F, 0x4D, 0x42, 0x01, 0x00, 0x00, 0x00, 0xEB, 0x01, 0x00, 0x00, 0xE0, 0x03, 0x00, 0x00,
     0x44, 0x53, 0x00, 0x01, 0x1A, 0x7D, 0xDA, 0x54, 0x18, 0xCF, 0x81, 0x00, 0x01, 0x06, 0x18, 0x42,
     0x20, 0x42, 0x01, 0x89, 0xC0, 0xF2, 0x69, 0x14, 0x03, 0x06, 0xA5, 0x01, 0x44, 0x72, 0x20, 0xE4,
     0x82, 0x89, 0x09, 0x10, 0x01, 0x21, 0xAF, 0x02, 0x6C, 0x0A, 0x30, 0x09, 0xA2, 0xFE, 0xFD, 0x21,
     0x4A, 0x82, 0x43, 0x09, 0x81, 0x90, 0x44, 0x01, 0xE6, 0x05, 0xE8, 0x16, 0x60, 0x58, 0x80, 0x6D,
     0x01, 0xA6, 0x05, 0x38, 0x86, 0xA4, 0xD2, 0xC0, 0x29, 0x81, 0xA5, 0x40, 0x48, 0xA8, 0x00, 0xE5,
     0x02, 0x7C, 0x0B, 0xD0, 0x8E, 0x28, 0xC9, 0x02, 0x2C, 0xC3, 0x88, 0xC0, 0xA3, 0x88, 0x6C, 0x34,
     0x4E, 0x50, 0x30, 0x34, 0x4A, 0xCC, 0x30, 0x08, 0x6A, 0xE7, 0x93, 0x10, 0x60, 0xCC, 0xC2, 0x05,
     0x48, 0xC7, 0x10, 0xE8, 0xA1, 0x1C, 0x83, 0xC7, 0x12, 0xE8, 0x0C, 0x2C, 0x48, 0x80, 0x4C, 0x28,
     0x21, 0x74, 0x0D, 0x27, 0xB4, 0x9E, 0x81, 0x24, 0x1A, 0x47, 0xA8, 0x41, 0x22, 0x05, 0x3C, 0xAB,
     0x20, 0x31, 0x4E, 0x23, 0x44, 0xB8, 0x50, 0x47, 0x85, 0x1D, 0x9E, 0xE1, 0x3D, 0x8F, 0x63, 0x28,
     0x1B, 0x51, 0x04, 0x9E, 0x68, 0xA7, 0x02, 0xE4, 0x0A, 0x10, 0x26, 0x40, 0x3C, 0xAA, 0x30, 0x9A,
     0x83, 0x22, 0x70, 0xA1, 0x31, 0x32, 0x20, 0x34, 0x92, 0xD6, 0x47, 0x48, 0x08, 0xEC, 0x5E, 0x80,
     0x35, 0x01, 0xDA, 0x04, 0x38, 0x13, 0xA0, 0x0C, 0x81, 0x78, 0x83, 0x91, 0x2D, 0x01, 0xE2, 0xE0,
     0x85, 0x1E, 0x2A, 0x4A, 0x90, 0xE3, 0x38, 0x8C, 0x40, 0x51, 0x1A, 0x84, 0x13, 0x48, 0x28, 0xA3,
     0x05, 0xE9, 0x10, 0x43, 0x08, 0x8D, 0x21, 0x13, 0xC8, 0x1A, 0x47, 0x20, 0x8B, 0x02, 0xEC, 0x8F,
     0x8A, 0x29, 0x80, 0x68, 0x01, 0x44, 0x91, 0x46, 0x83, 0x3A, 0x03, 0x24, 0x78, 0x26, 0xF0, 0x79,
     0xE0, 0x08, 0x8F, 0xF9, 0x0C, 0xCF, 0x2D, 0xC8, 0x61, 0x58, 0xE7, 0x61, 0x80, 0x0C, 0x8F, 0x8D,
     0xD3, 0xFF, 0x03, 0x9F, 0x0E, 0xF0, 0x23, 0x66, 0x87, 0x02, 0xF6, 0xFF, 0x9F, 0x72, 0x38, 0xCC,
     0x10, 0x3D, 0xFE, 0x70, 0x27, 0x70, 0x88, 0x0C, 0xD0, 0x83, 0x3D, 0x6E, 0xEC, 0xCC, 0x4E, 0xE6,
     0xA0, 0x4B, 0x15, 0x60, 0xF6, 0x2E, 0xA0, 0x81, 0x25, 0x38, 0x1E, 0x1F, 0x09, 0x7C, 0x13, 0xF0,
     0xD1, 0x81, 0x0C, 0x02, 0x35, 0x32, 0x43, 0x7B, 0xA8, 0xA7, 0xF5, 0x66, 0xE0, 0xCB, 0x80, 0x09,
     0x2C, 0x16, 0x42, 0x9B, 0xD4, 0x78, 0x80, 0x80, 0xE2, 0x93, 0x02, 0xB9, 0x19, 0x78, 0xBE, 0x26,
     0x78, 0x32, 0x08, 0x8C, 0x1D, 0x90, 0xFD, 0x0A, 0x40, 0x08, 0xFE, 0x92, 0x70, 0x44, 0x4F, 0x08,
     0x11, 0x9E, 0x13, 0x8C, 0x7E, 0xE4, 0x7D, 0x0A, 0xD0, 0x0A, 0x21, 0xB4, 0x43, 0x08, 0x76, 0x0C,
     0x56, 0x0A, 0x23, 0xB4, 0x48, 0x11, 0xBA, 0x1C, 0x89, 0x00, 0x7B, 0x04, 0x12, 0x56, 0xC8, 0xB0,
     0x1E, 0x4C, 0x02, 0x8B, 0x3C, 0x51, 0xA0, 0xC7, 0xC1, 0x01, 0x4F, 0xE0, 0xA8, 0x82, 0x1E, 0xC7,
     0x31, 0x78, 0x2A, 0x87, 0xE5, 0xE3, 0x85, 0xEF, 0x12, 0x1E, 0xD4, 0x53, 0x86, 0x35, 0xCF, 0x4B,
     0xE7, 0x86, 0x33, 0x7B, 0x27, 0xF0, 0xF9, 0xC2, 0x43, 0xC0, 0x1C, 0x0B, 0x3C, 0x04, 0x3E, 0x80,
     0x56, 0xA7, 0x44, 0xE7, 0x86, 0x3B, 0x88, 0x70, 0xB0, 0x77, 0x0B, 0xCC, 0x00, 0x30, 0x0A, 0x6D,
     0xFA, 0xD4, 0x68, 0xD4, 0xAA, 0x41, 0x99, 0x1A, 0x65, 0x1A, 0xD4, 0xEA, 0x53, 0xA9, 0x31, 0x63,
     0xA7, 0x00, 0x42, 0xA1, 0xD3, 0x82, 0xE3, 0x80, 0xD0, 0xFF, 0x3F                               
     })
  }
  
  //
  // Define a Virtual device for Lenovo Paper function
  //
  Device (VPC1)
  {
   Name(_HID, "IDEA2000")
   Name(_UID, 0)
   Method(_STA)
    {
      If(LEqual(OSYS, 2012)) // Win8
      {
        Return(0x0F)
      }
      ElseIf(LEqual(OSYS, 2013)) // Win8.1
      {
        Return(0x0F)
      }
      Else // Win10,Win7
      {
        Return(0x00)
      }
    }
  }