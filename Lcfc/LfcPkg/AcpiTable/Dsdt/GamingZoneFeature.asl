
//*****************************************************************************
//
// Copyright (c) 2012 - 2016, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
/*++
Abstract:
  LCFC Gaming Zone common asl code definition, it's for all Hefei LBG projects.
  
History:
  Date          Name          Version    Change Notes
  2016.01.12     Steven Wang  V1.00      Initial Release 
  2016.03.01     Steven Wang  V1.01      Implement Lenovo Game Zone Firmware SPEC V1.53
  2016.04.06     Steven Wang  V1.02      Implement Lenovo Game Zone Firmware SPEC V1.54
                                         Include Fan Extreme Cooling feature and Error Touch Prevention feature
  2016.09.01     Steven Wang  V1.03      Implement Lenovo Game Zone Firmware SPEC V1.72
  2017.05.05     Steven Wang  V1.04      Implement Lenovo Game Zone Firmware SPEC V1.86
  2019.01.22     Jansen Jia   V2.15      Implement Lenovo Game Zone Firmware SPEC V2.15
  2019.01.25     Jansen Jia   V2.16      Implement Lenovo Game Zone Firmware SPEC V2.16
  2019.01.29     Jansen Jia   V2.17      Implement Lenovo Game Zone Firmware SPEC V2.17
  2019.09.04     Ru Xu        V2.18      Implement Lenovo Game Zone Firmware SPEC V2.18
  2019.09.17     Ru Xu        V2.19      Implement Lenovo Game Zone Firmware SPEC V2.19
  2019.09.20     Ru Xu        V2.20      Implement Lenovo Game Zone Firmware SPEC V2.20
  2019.09.24     Ru Xu        V2.21      Implement Lenovo Game Zone Firmware SPEC V2.21
  2019.09.29     Ru Xu        V2.22      Implement Lenovo Game Zone Firmware SPEC V2.22
  2019.10.14     Ru Xu        V2.23      Implement Lenovo Game Zone Firmware SPEC V2.23
  2019.10.24     Ru Xu        V2.24      Implement Lenovo Game Zone Firmware SPEC V2.24
  2019.10.31     Ru Xu        V2.25      Implement Lenovo Game Zone Firmware SPEC V2.25
  2019.11.04     Ru Xu        V2.26      Implement Lenovo Game Zone Firmware SPEC V2.26
  2019.11.19     Ru Xu        V2.27      Implement Lenovo Game Zone Firmware SPEC V2.27 
  2019.11.27     Ru Xu        V2.28      Implement Lenovo Game Zone Firmware SPEC V2.28
  2019.11.29     Ru Xu        V2.29      Implement Lenovo Game Zone Firmware SPEC V2.29
  2019.12.03     Ru Xu        V2.30      Implement Lenovo Game Zone Firmware SPEC V2.30
  2020.01.15     Ru Xu        V2.31      Implement Lenovo Game Zone Firmware SPEC V2.31
  2020.01.21     Ru Xu        V2.32      Implement Lenovo Game Zone Firmware SPEC V2.32
  2020.04.23     Ru Xu        V2.33      Implement Lenovo Game Zone Firmware SPEC V2.33
  
Module Name:
  GamingZoneFeature.asl
  
Note:
  You must take care about the prompt string "LCFCTODO:" and customize your
  project.
  
Support Version:
   Lenovo Game Zone Firmware SPEC V2.32
--*/
#define CASE_TEMPERATURE  0x4B553A46
//#define GPIO_CNL_H_GPP_K4     0x030A0004
//#define PCH_OD_ON            0x1   ///< Set output to high
//#define PCH_OD_OFF           0x0   ///< Set output to low
//External(\_SB.GGOV, MethodObj)
//External(\_SB.SGOV, MethodObj)
//External(\BDID, IntObj)
//External(\_SB.NPCF, DeviceObj)
//#define SET_GFX_IGPU_DGPU_FUNCTION           0x25
//#define SET_GFX_DGPU_ONLY_FUNCTION           0x26
//#define FUNCTION_RESTORE_DEFAULT_OC          0xCE
Scope (\_SB) {
  Device (GZFD)
  {
    Name (_HID, "PNP0C14")
    Name (_UID, "GMZN")
    Name (_WDG, Buffer () {
    
    //
    // Method Guids "LENOVO_GAMEZONE_DATA class"           
    // {887B54E3-DDDC-4B2C-8B88-68A26A8835D0}
    0xe3, 0x54, 0x7b, 0x88, 0xdc, 0xdd, 0x2c, 0x4b, 0x8b, 0x88, 0x68, 0xA2, 0x6A, 0x88, 0x35, 0xD0,
    65, 65,        // Object Id (AA)
    1,             // Instance Count
    0x02,          // Flags (WMIACPI_REGFLAG_METHOD)
                 
    //
    // Method Guids "LENOVO_GAMEZONE_CPU_OC_DATA class" 
    // {887B54E1-DDDC-4B2C-8B88-68A26A8835D0}
    0xE1, 0x54, 0x7B, 0x88, 0xDC, 0xDD, 0x2C, 0x4B, 0x8B, 0x88, 0x68, 0xA2, 0x6A, 0x88, 0x35, 0xD0,
    65, 48,       // Object ID (A0)
    1,           // Instance Count 
    0x01,         // Flags (WMIACPI_REGFLAG_EXPENSIVE)                                       
                                   
    //
    // Method Guids "LENOVO_GAMEZONE_GPU_OC_DATA class" 
    // {887B54E2-DDDC-4B2C-8B88-68A26A8835D0}
    0xE2, 0x54, 0x7B, 0x88, 0xDC, 0xDD, 0x2C, 0x4B, 0x8B, 0x88, 0x68, 0xA2, 0x6A, 0x88, 0x35, 0xD0,
    65, 49,       // Object ID (A1)
    2,           // Instance Count 
    0x01,         // Flags (WMIACPI_REGFLAG_EXPENSIVE)                                   
                                   
     
    //
    // Event Guids "Temperature change event "                
    // {BFD42481-AEE3-4501-A107-AFB68425C5F8}
    0x81, 0x24, 0xd4, 0xbf, 0xe3, 0xae, 0x01, 0x45, 0xA1, 0x07, 0xAF, 0xB6, 0x84, 0x25, 0xC5, 0xF8,
    0xd0, 0,        // Object Id
    1,              // Instance Count
    0x08,           // Flags (WMIACPI_REGFLAG_EVENT)
    
    //                  
    // Event Guids "OC button pressed event"
    // {D062906B-12D4-4510-999D-4831EE80E985} 
    0x6b, 0x90, 0x62, 0xd0, 0xd4, 0x12, 0x10, 0x45, 0x99, 0x9D, 0x48, 0x31, 0xEE, 0x80, 0xE9, 0x85,
    0xd1, 0,        // Object Id 
    1,              // Instance Count
    0x08,           // Flags (WMIACPI_REGFLAG_EVENT)
    
    //
    // Event Guids "Gpu Temperature change event"
    // {BFD42481-AEE3-4502-A107-AFB68425C5F8}
    0x81, 0x24, 0xD4, 0xBF, 0xE3, 0xAE, 0x02, 0x45, 0xA1, 0x07, 0xAF, 0xB6, 0x84, 0x25, 0xC5, 0xF8,
    0xe0, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)       
     
    //
    // Event Guids "Fancooling finish event"
    //{BC72A435-E8C1-4275-B3E2-D8B8074ABA59} 
    0x35, 0xA4, 0x72, 0xBC, 0xC1, 0xE8, 0x75, 0x42, 0xB3, 0xE2, 0xD8, 0xB8, 0x07, 0x4A, 0xBA, 0x59,
    0xe1, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)       
    
    //
    // Event Guids "Key lock status change event"
    //{10AFC6D9-EA8B-4590-A2E7-1CD3C84BB4B1}
    0xD9, 0xC6, 0xAF, 0x10, 0x8B, 0xEA, 0x90, 0x45, 0xA2, 0xE7, 0x1C, 0xD3, 0xC8, 0x4B, 0xB4, 0xB1,
    0xe2, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)  
                        
    //
    // Event Guids "Smart Fan mode change event"
    //{D320289E-8FEA-41E0-86F9-611D83151B5F}
    0x9E, 0x28, 0x20, 0xD3, 0xEA, 0x8F, 0xE0, 0x41, 0x86, 0xF9, 0x61, 0x1D, 0x83, 0x15, 0x1B, 0x5F,
    0xe3, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)  

    //
    // Event Guids "Smart Fan setting mode change event"
    //{D320289E-8FEA-41E1-86F9-611D83151B5F}
    0x9E, 0x28, 0x20, 0xD3, 0xEA, 0x8F, 0xE1, 0x41, 0x86, 0xF9, 0x61, 0x1D, 0x83, 0x15, 0x1B, 0x5F,
    0xe4, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)  

    //
    // Event Guids "POWER CHARGE MODE Change EVENT"
    //{D320289E-8FEA-41E0-86F9-711D83151B5F}
    0x9E, 0x28, 0x20, 0xD3, 0xEA, 0x8F, 0xE0, 0x41, 0x86, 0xF9, 0x71, 0x1D, 0x83, 0x15, 0x1B, 0x5F,
    0xe5, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)  
    //
    // Event Guids "LENOVO_GAMEZONE_LIGHT_PROFILE_CHANGE_EVENT"
    //{D320289E-8FEA-41E0-86F9-811D83151B5F}
    0x9E, 0x28, 0x20, 0xD3, 0xEA, 0x8F, 0xE0, 0x41, 0x86, 0xF9, 0x81, 0x1D, 0x83, 0x15, 0x1B, 0x5F,
    0xe6, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)  
//Thermal Mode Real Mode change event
//{D320289E-8FEA-41E0-86F9-911D83151B5F}
    0x9E, 0x28, 0x20, 0xD3, 0xEA, 0x8F, 0xE0, 0x41, 0x86, 0xF9, 0x91, 0x1D, 0x83, 0x15, 0x1B, 0x5F,
    0xe7, 0,       // Object Id
    1,             // Instance Count
    0x08,          // Flags (WMIACPI_REGFLAG_EVENT)  

    // The GUID for returning the MOF data, DO NOT CHANGE!!!!!
    0x21, 0x12, 0x90, 0x05, 0x66, 0xD5, 0xD1, 0x11, 0xB2, 0xF0, 0x00, 0xA0, 0xC9, 0x06, 0x29, 0x10,
    68, 68,         // Object Id (DD)
    1,              // Instance Count
    0x00            // Flags  
    })
    
    
    //
    // Package Query data block
    // Arg0 has the instance being queried
    Name(CPOC, Buffer(0x190){})
    Method(WQA0, 1) 
    {
    //LCFCTODO: please provide the information depend on your project
      Return(CPOC)
    }
    
    //
    // ULONG Query data block
    // Arg0 has the instance being queried
    //Graphic OC setting
    Name(GOC0, Buffer(0x2C){
    0,0,0,0,        // P-State ID,default is 0
    0,0,0,0,        // OC ID,0:Graphic clock offset,1:Memory clock offset
    0,0,0,0,        // Default Value , default is 0
    0,0,0x48,0x42,  // OC Value,default is 50 MHz
    0,0,0x48,0xC3,  // Min OC Value,-200 MHz
    0,0,0x48,0x43,  // Max OC Value, 200 MHz
    0,0,0x80,0x3F,  // Scale Value, 1 MHz
    0,0,0,0,        // OC Order,default is 0
    0,0,0,0,        // Non OC Order,default is 0
    0,0,0,0,        // Delay time in ms,default is 0
    0,0,0,0         // Capability,default is 0,support super OC
    })

    //Memory OC setting
    Name(GOC1, Buffer(0x2C){
    0,0,0,0,        // P-State ID,default is 0
    1,0,0,0,        // OC ID,0:Graphic clock offset,1:Memory clock offset
    0,0,0,0,        // Default Value , default is 0
    0,0,0xC8,0x42,  // OC Value,default is 100 MHz
    0,0,0x48,0xC3,  // Min OC Value,-200 MHz
    0,0,0x48,0x43,  // Max OC Value, 200 MHz
    0,0,0x80,0x3F,  // Scale Value,1 MHz
    0,0,0,0,        // OC Order,default is 0
    0,0,0,0,        // Non OC Order,default is 0
    0,0,0,0,        // Delay time in ms,default is 0
    1,0,0,0         // Capability,default is 1,only support default OC,not support super OC
    })    
    
    Method(WQA1, 1) 
    {
    //LCFCTODO: please provide the information depend on your project
        If (LEqual(Arg0, Zero))
        {
           Return(GOC0)            
        }           

        If (LEqual(Arg0, 1))
        {
           Return(GOC1)            
        } 
    }
    
    Method (WMAA, 3)
    {
// Follow spec and remove it
//      // MethodId 1 - GetIRTemp
//      if (LEqual(Arg1, 1))  
//      {
//        If (EC0_SCOPE.EC0.ECAV) {
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            Store (EC0_SCOPE.EC0.CPUT, Local0)
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (Local0)
//          }
//        }
//      }
//      
//      // MethodId 2 - GetThermalTableID
//      if (LEqual(Arg1, 2))
//      {
//        //ID = 0: Normal Thermal Table
//        //ID = 1: Over Clock Thermal Table
//        If (EC0_SCOPE.EC0.ECAV) {
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            Store (EC0_SCOPE.EC0.IRTI, Local0)
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (Local0) 
//          }
//        }
//      }
//      
//      // MethodId 3 - SetThermalTableID
//      if (LEqual(Arg1, 3))
//      {
//        If (EC0_SCOPE.EC0.ECAV) {
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            If(LEqual(ToInteger(Arg2),1)) {
//              Store(1, EC0_SCOPE.EC0.IRTI)  
//            } ElseIf(LEqual(ToInteger(Arg2),0)) {
//              Store(0, EC0_SCOPE.EC0.IRTI)    
//            }
//            Store (EC0_SCOPE.EC0.IRTI, Local0)
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (Local0)    
//          }
//        }
//      }

      // MethodId 4 - IsSupportGpuOC
      if (LEqual(Arg1, 4)) {
      //LCFCTODO: Check if your project support GPU OC function.
      // 0 - Not support GPU OC
      // 1 - Support GPU OC 
      // 2 - Support Super GPU OC (APP Support)
        Return (0)
      }
      
// Follow spec and remove it
//      // MethodId 5 - GetGpuGpsState
//      if (LEqual(Arg1, 5)) {
//        If (EC0_SCOPE.EC0.ECAV) {
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            if(EC0_SCOPE.EC0.ADPT) {
//              If(LEqual(ToInteger(EC0_SCOPE.EC0.GUST),2)) {
//                Release(EC0_SCOPE.EC0.LfcM)
//                Return (0x1)
//              }      
//            }
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (0)  
//          }
//        }                 
//      }
//                       
//      // MethodId 6 - SetGpuGpsState
//      if (LEqual(Arg1, 6)) {       
//        If (EC0_SCOPE.EC0.ECAV) {
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            If(LEqual(ToInteger(Arg2),1)) {    
//              //Notify(\_SB.PCI0.PEG0.PEGP, 0xD1)
//              If(EC0_SCOPE.EC0.ADPT) {
//                Store(0x42, EC0_SCOPE.EC0.DGPU)  
//              } Else {
//                Store(0x2, EC0_SCOPE.EC0.DGPU)  
//              }
//              Sleep(10)  
//            } ElseIf (LEqual(ToInteger(Arg2),0)) {
//              //Notify(\_SB.PCI0.PEG0.PEGP, 0xD2)
//              If(EC0_SCOPE.EC0.ADPT) {
//                Store(0x44, EC0_SCOPE.EC0.DGPU)  
//              } Else {
//                Store(0x4, EC0_SCOPE.EC0.DGPU)  
//              }
//              Sleep(10)          
//            }  
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (0)
//          }
//        }
//      }          

//      // MethodId 7 - GetFancount
//      if (LEqual(Arg1, 7)) {
//      //LCFCTODO: Check your project the fan count
//        Return (2) 
//      }
//      
//      // MethodId 8 - GetFan1Speed
//      if (LEqual(Arg1, 8)) {
//        If (EC0_SCOPE.EC0.ECAV) {                                            
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            Store(EC0_SCOPE.EC0.FANS,Local0)
//            Multiply(Local0,100,Local0)
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (Local0) //Fan1 speed     
//          }
//        }
//      }
//      
//      // MethodId 9 - GetFan2Speed
//      if (LEqual(Arg1, 9)) { 
//        If (EC0_SCOPE.EC0.ECAV) {                                            
//          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
//            Store(EC0_SCOPE.EC0.FA2S,Local0)
//            Multiply(Local0,100,Local0)
//            Release(EC0_SCOPE.EC0.LfcM)
//            Return (Local0) //Fan2 speed
//          }
//        }
//      }

//      // MethodId 10 - GetFanMaxSpeed
//      if (LEqual(Arg1, 10)) {
//      // LCFCTODO: Need check with EC, will support in future
//        Return (3400)
//      }

      // MethodId 11 - GetVersion
      if (LEqual(Arg1, 11)) {
        Return (11) // 1: Version 1.4 before, 2: Version 1.5 +, 10: Version V2.13-V2.17, 11:V2.18-V2.33
      }
      
      // MethodId 12 - IsSupportFanCooling
      if (LEqual(Arg1, 12)) {
      // LCFCTODO: Check if your project support fan cooling
      // 0 - Not support fan cooling
      // 1 - Support fan cooling
        Return (0)  // Project support Fn+Q 2.0, need report FanCooling not support.
      }

      // MethodId 13 - SetFanCooling
      if (LEqual(Arg1, 13)) {
        // 1: Start Fan Cooling, 0: Stop Fan Cooling
        If (LEqual(ToInteger(Arg2), 1)){ 
          EC0_SCOPE.EC0.NCMD(0x59, 0x77)
        } Else {
          EC0_SCOPE.EC0.NCMD(0x59, 0x76) 
        }
      }
      
      // MethodId 14 - IsSupportCpuOC
      if (LEqual(Arg1, 14)) {
      //LCFCTODO: Check if your project support CPU OC function.
        Return (0)
      }

      // MethodId 15 - IsBIOSSupportOC   
      if (LEqual(Arg1, 15)) {
      //LCFCTODO: Check if your project support BIOS OC function.
        Return (0)
      }
      
      // MethodId 16 - SetBIOSOC
      if (LEqual(Arg1, 16)) {
      //LCFCTODO: if your project support BIOS OC, please implement this.
        Return (0)
      }
      
// Follow spec and remove it
//      // MethodId 17 - GetTriggerTemperatureValue 
//      if (LEqual(Arg1, 17)) {
//      //LCFCTODO: if your project support OC, please implement this. 
//        Return (CASE_TEMPERATURE)
//      }
      
      // MethodId 18 - GetCPUTemp 
      if (LEqual(Arg1, 18)) {
      //LCFCTODO: if your project support OC, please implement this. 
        Return (0)
      }
      
      // MethodId 19 - GetGPUTemp 
      if (LEqual(Arg1, 19)) {
      //LCFCTODO: if your project support OC, please implement this. 
        Return (0)
      }
      
      // MethodId 20 - GetFanCoolingStatus 
      if (LEqual(Arg1, 20)) {  
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            If (And(EC0_SCOPE.EC0.FCST, 0x01)){
              Release(EC0_SCOPE.EC0.LfcM)
              Return (1)
            }
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0)
          }
        }
      }

      // MethodId 21 - IsSupportDisableWinKey 
      if (LEqual(Arg1, 21)) {
      //LCFCTODO: Check if your project support WinKey disable function
      //Note: it will not disable the combined key with WinKey, such as, Win + L
      // 0: Don't support Windows Key; 1: Support BIOS disable Windows Key; 2: Use USB KB to lock Windows Key.
        Return (1)
      }

      // MethodId 22 - SetWinKeyStatus
      if (LEqual(Arg1, 22)) {
      // 0: Enable WinKey, 1: Disable WinKey
        If (LEqual(ToInteger(Arg2), 1)){
          EC0_SCOPE.EC0.NCMD(0x59, 0x78)       
        } Else {
          EC0_SCOPE.EC0.NCMD(0x59, 0x79)  
        }
      }

      // MethodId 23 - GetWinKeyStatus
      if (LEqual(Arg1, 23)) {    
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            If (And(EC0_SCOPE.EC0.GDST, 0x02)){
              Release(EC0_SCOPE.EC0.LfcM)
              Return (1)
            }
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0)  
          }
        }
      }

      // MethodId 24 - IsSupportDisableTP 
      if (LEqual(Arg1, 24)) {
      //LCFCTODO: Check if your project support Touch Pad disable function
      // 0: Don't support disable TP; 1: Support BIOS disable TP; 2: Use USB KB to lock TP.
        Return (1)
      }

      // MethodId 25 - SetTPStatus 
      if (LEqual(Arg1, 25)) {   
      // 0: Enable TP, 1: Disable TP
        If (LEqual(ToInteger(Arg2), 1)){
          EC0_SCOPE.EC0.NCMD(0x59, 0x7B)
        } Else {
          EC0_SCOPE.EC0.NCMD(0x59, 0x7A)
        }      
      }

      // MethodId 26 - GetTPStatus 
      if (LEqual(Arg1, 26)) {
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            If (And(EC0_SCOPE.EC0.GDST, 0x04)){
              Release(EC0_SCOPE.EC0.LfcM)
              Return(1)
            }
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0)
          }
        }
      }
      
// Follow spec and remove it
//      // MethodId 27 - GetGPUPow 
//      if (LEqual(Arg1, 27)) {
//      //LCFCTODO: if your project support OC, please implement this.
//        Return (0)
//      }
//      // MethodId 28 - GetGPUOCPow 
//      if (LEqual(Arg1, 28)) {
//      //LCFCTODO: if your project support OC, please implement this.
//        Return (0)
//      }       
      
      // MethodId 29 - GetGPUOCType 
      if (LEqual(Arg1, 29)) {
      //LCFCTODO: if your project support OC, please implement this by spec V1.72
      //1: OC by dGPU
      //2: OC by Software Interface
        Return (1)
      } 
      
      // MethodId 30 - GetKeyboardfeaturelist 
      if (LEqual(Arg1, 30)) {
        Store(0,Local1)
      //LCFCTODO: if USB keyboard, please add the below code
        Or(Local1, 0xF000, Local1)
      //LCFCTODO: if support one key desktop screen recording, please add the below code
      //  Or(Local1, 0x01, Local1)
      //LCFCTODO: if support one key launch NerveCenter, please add the below code
        Or(Local1, 0x02, Local1)
      //LCFCTODO: if support one key disable WinKey function, please add the below code
        Or(Local1, 0x04, Local1)
      //LCFCTODO: if support M Macro Key function of Macro Software, please add the below code
      //  Or(Local1, 0x08, Local1)
      //LCFCTODO: if support Digital Keyboard Function of Macro Software, please add the below code
      //  Or(Local1, 0x10, Local1)
      //LCFCTODO: if one key launch Macro Software Function, please add the below code
      //  Or(Local1, 0x20, Local1)
      //LCFCTODO: if support FN+SPACE to switch keyboard light function, please add the below code
        Or(Local1, 0x40, Local1)
        Return (Local1)
      }   
       
      // MethodId 31 - GetMemoryOCInfo
      if (LEqual(Arg1, 31)) {
        Store(2400, Local1)
        Store(2400, Local2) // Local2 is default is the same as Local1, default memory OC is disable.
      //LCFCTODO: please set the correct base memeory frequency to the "BaseMemoryFrequency" location
      //          Ex: Store(2400, Local1)
      //Store(BaseMemoryFrequency, Local1)
      //LCFCTODO: please set the correct OC memeory frequency to the "OcMemoryFrequency" location
      //          if not support OC,set the same value as "BaseMemoryFrequency"
      //          Ex: Store(3200, Local2)
      //Store(OcMemoryFrequency, Local2)       
        Or(Local1, ShiftLeft(Local2,16), Local1)
        Return (Local1)
      }
        
      // MethodId 32 - IsSupportWaterCooling
      if (LEqual(Arg1, 32)) {
      //LCFCTODO: please set the below return value, 
      //          0: not support water cooling, 1: support water cooling
        Return (0)
      }     
      
      // MethodId 33 - SetWaterCoolingStatus
      if (LEqual(Arg1, 33)) {
      //LCFCTODO: please set the below return value,based on water cooling feature support
      //          0: Disable water cooling, 1: Enable water cooling
        Return (0)
      }      
      
      // MethodId 34 - GetWaterCoolingStatus
      if (LEqual(Arg1, 34)) {
      //LCFCTODO: please set the below return value, need co-work with EC.
      //          0: Haven't launched water cooling
      //          1: Have launched water cooling,  have power but water pump don't launch
      //          2: Have launched water cooling,  have power but water pump launch
        Return (0)
      }     
      
      // MethodId 35 - IsSupportLightingFeature
      if (LEqual(Arg1, 35)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: Not support lighting KB LED
      //          1: Follow Y900/Y910 KB 
      //          2: Follow Y720 KB
      //          3: Follow Y920
      //          4: Follow Y550
      //          5: Follow Y750
      //          0xf0: Support single lightling color KB LED      
        Return (4)
      }  
      
      // MethodId 36 - SetKeyboardLight
      if (LEqual(Arg1, 36)) {
        //Arg2 0: Disable KB backlight, 1: Enable KB backlight
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            If (LEqual(ToInteger(Arg2), 1)){
              //LCFCTODO: if support one level BL
              //Store(1, EC0_SCOPE.EC0.KBLO)
          
              //LCFCTODO: if support two level BL
              Store(1, EC0_SCOPE.EC0.KLOR)
              Store(1, EC0_SCOPE.EC0.KLCH)
            } Else {
              //LCFCTODO: if support one level BL
              //Store(0, EC0_SCOPE.EC0.KBLO)
          
              //LCFCTODO: if support two level BL
              Store(0, EC0_SCOPE.EC0.KLOR)
              Store(1, EC0_SCOPE.EC0.KLCH)
            }   
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0)
          }
        }    
      } 
      // MethodId 37 - GetKeyboardLight
      if (LEqual(Arg1, 37)) {
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            //LCFCTODO: if support one level BL
            //Store(EC0_SCOPE.EC0.KBLO, Local1)
          
            //LCFCTODO: if support two level BL
            Store(EC0_SCOPE.EC0.KLOR, Local1)
            Release(EC0_SCOPE.EC0.LfcM)
            //Return 0: KB backlight disabled, 1: KB backlight enabled
            Return (Local1) 
          }
        }    
      }
      // MethodId 38 - GetMacrokeyScancode
      if (LEqual(Arg1, 38)) {
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
            Store(0, Local1) 
            //LCFCTODO: if support the Macro Key function, returen the M Macro Key scan code
            //Store(EC0_SCOPE.EC0.MKEY, Local1)
            Release(EC0_SCOPE.EC0.LfcM)
            //Return 0: Don't support the Macro Key function
            Return (Local1) 
          }
        }
      }
      // MethodId 39 - GetMacrokeyCount
      if (LEqual(Arg1, 39)) {
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            Store(0, Local1) 
            //LCFCTODO: if support the Macro Key function, returen the M Macro Key count
            //Store(EC0_SCOPE.EC0.NKEY, Local1)
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1) 
          }
        }
      }
      // MethodId 40 - IsSupportGSync
      if (LEqual(Arg1, 40)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: Not support GSync
      //          1: Support discrete graphic switch and GSync 
      //          2: Only Support discrete graphic switch   
        Return (0)
      }
      // MethodId 41 - GetGSyncStatus
      if (LEqual(Arg1, 41)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: EDP panel output by internal graphic 
      //          1: EDP panel output by discrete graphic    
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
      //      If(LEqual(EC0_SCOPE.EC0.MSMF, 1)){
      //       Store(1, Local1)
      //      } ElseIf (LEqual(EC0_SCOPE.EC0.MSMF, 0)){
      //       Store(0, Local1)
      //      }
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1) 
          }
        }
      }
      // MethodId 42 - SetGSyncStatus
      if (LEqual(Arg1, 42)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: Set EDP panel output by internal graphic
      //          1: Set EDP panel output by discrete graphic
        If(LEqual(ToInteger(Arg2),1)) {
      //    Store(SET_GFX_DGPU_ONLY_FUNCTION, SMBB)
      //    Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)
        } ElseIf(LEqual(ToInteger(Arg2),0)) {
      //    Store(SET_GFX_IGPU_DGPU_FUNCTION, SMBB)
      //    Store(BOOT_SEQUENCE_UPDATE_SWSMI, SMBA)   
        }
        Return (0)
      }
      // MethodId 43 - IsSupportSmartFan
      if (LEqual(Arg1, 43)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: Don't support Thermal mode
      //          1: Support FN+Q 2.0 Thermal mode
      //          2: Support FN+Q 3.0 (OC) Thermal mode
      //          3: Support FN+Q 3.0 (OC and DDS) Thermal mode
        Return (0)
      }
      // MethodId 44 - SetSmartFanMode
      if (LEqual(Arg1, 44)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: Set as Quiet Mode - Low Speed Fan
      //          2: Set as Balance Mode - Balance Acoustic
      //          3: Set as Performance Mode - High Speed Fan
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            If(LEqual(ToInteger(Arg2),1)) {
              //If(LNotEqual(EC0_SCOPE.EC0.SPMO, 2)) {
              //  Store(1, EC0_SCOPE.EC0.SSFM) // Notfiy EC the SmartFanMode is changed by APP.
              //}
              //Store(2, EC0_SCOPE.EC0.SPMO)
              //Store(2,\ODV1)
              //Store(0, LTGP)
              //Store(0, \_SB.PCI0.LPCB.EC0.PABD) //User change mode need clear this flag
              //Store(0, PABS)
            } ElseIf(LEqual(ToInteger(Arg2),2)) { //Balance mode
              //If(LNotEqual(EC0_SCOPE.EC0.SPMO, 0)) {
              //  Store(1, EC0_SCOPE.EC0.SSFM) // Notfiy EC the SmartFanMode is changed by APP.
              //}
              //Store(0, EC0_SCOPE.EC0.SPMO)
              //Store(0,\ODV1)
              //Store(0, LTGP)
              //Store(0, \_SB.PCI0.LPCB.EC0.PABD) //User change mode need clear this flag 
      	      //Store(0, PABS)
            } ElseIf(LEqual(ToInteger(Arg2),3)) { //Performance mode
              // If(LEqual(\_SB.PCI0.LPCB.EC0.ADPT,0x0)) //DC mode,  user set balance mode instead of performance mode 
              //{
              //If(LNotEqual(EC0_SCOPE.EC0.SPMO, 0)) {
              //  Store(1, EC0_SCOPE.EC0.SSFM) // Notfiy EC the SmartFanMode is changed by APP.
              // }
              // Store(0, EC0_SCOPE.EC0.SPMO)
              // Store(0,\ODV1)
              // Store(0, LTGP)
              // Store(1, \_SB.PCI0.LPCB.EC0.PABD) //If DC mode set Performance mode, need set this flag 
              //  Store(0, PABS)
              //}
              //Else
              //{
              //If(LNotEqual(EC0_SCOPE.EC0.SPMO, 1)) {
              //  Store(1, EC0_SCOPE.EC0.SSFM) // Notfiy EC the SmartFanMode is changed by APP.
              //}
              //Store(1, EC0_SCOPE.EC0.SPMO)
              //Store(1,\ODV1)
              //Store(1, LTGP)
              //Store(0, \_SB.PCI0.LPCB.EC0.PABD) //User change mode need clear this flag
              //Store(1, PABS)
              //}
            }
            //Store(0, EC0_SCOPE.EC0.MACF) // Clear Mode Auto Change Flag
            //Sleep(5)
            //Notify(GZFD, 0xE7)                // Notify Gaming Zone Smart Fan mode change
            //Notify(\_SB.IETM, 0x88)
            //If (Lor(LEqual(BDID, 0x1),LEqual(BDID, 0x5)))//  only N18E G1-B support CTGP
            //{
            //  Notify(\_SB.PCI0.PEG0.PEGP, 0xC0)
            //}
            //If (LAnd(Lor(LEqual(BDID, 0x0),LEqual(BDID, 0x4)),LEqual(CAMS, 0xA0)))
            //{
            //  Notify(\_SB.NPCF, 0xC0)
            //} 
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0) 
          }
        }
      }
      // MethodId 45 - GetSmartFanMode
      if (LEqual(Arg1, 45)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          Return user set mode
      //          1: Quiet Mode - Low Speed Fan
      //          2: Balance Mode - Balance Acoustic
      //          3: Performance Mode - High Speed Fan
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
            //If(LEqual(EC0_SCOPE.EC0.SPMO, 0x02)){
            //   Store(1, Local1)
            //  } ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x0)){
            //    If(LEqual(\_SB.PCI0.LPCB.EC0.PABD,0x1))   //1:Performance ac mode change to balance dc mode
            //    {
            //      Store(3, Local1)     //user set mode is performance ,so report to APP now is performance mode
            //    }
            //    Else 
            //    {
            //      Store(2, Local1)
            //    }
            //  } ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x1)){
            //   Store(3, Local1)
            //  }
              Release(EC0_SCOPE.EC0.LfcM)
              Return (Local1) 
            }
          }
      }
      // MethodId 46 - GetSmartFanSetting
      if (LEqual(Arg1, 46)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: Support Quiet, Balance and Performance Mode
      //          2: Only support Balance and Performance Mode
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            //If(LEqual(EC0_SCOPE.EC0.GSFS, 1)){
            // Store(1, Local1)
            //} ElseIf (LEqual(EC0_SCOPE.EC0.GSFS, 0)){
            // Store(2, Local1)
            //}
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1) 
          }
        }
      }
      // MethodId 47 - GetPowerChargeMode
      if (LEqual(Arg1, 47)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: AC Mode
      //          2: DC Mode
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            //If(LEqual(EC0_SCOPE.EC0.ADPT, 0x01)){
            // Store(1, Local1)
            //} ElseIf (LEqual(EC0_SCOPE.EC0.ADPT, 0x00)){
            // Store(2, Local1)
            //}
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1) 
          }
        }
      }
      // MethodId 48 - GetProductInfo
      if (LEqual(Arg1, 48)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          100 - Gaming Legion ,for example Y550
      //          101 - Gaming Lite,for example L350
      //          0   - Not gaming 
        Return (100) 
      }
      // MethodId 49 - IsSupportOD
      if (LEqual(Arg1, 49)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: Don't support OverDrive
      //          1: Support OverDrive
      //  If(LAnd(PANT,0x2))  //BIT1:support OD
      //  {
      //    Return (1) 
      //  }
      //  Else
      //  {
          Return (0) 
      //  }
   }
      // MethodId 50 - GetODStatus
      if (LEqual(Arg1, 50)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: OverDrive closed
      //          1: OverDrive open
      //  If(LAnd(PANT,0x2))  //BIT1:support OD
      //  {
      //    If(LEqual(\_SB.GGOV(GPIO_CNL_H_GPP_K4),PCH_OD_ON))  //GPIO K4 is high
      //    {
      //      Return(1)                             //OverDrive open
      //    }
      //    Else
      //    {
      //      Return (0) 
      //    }
      //  }
      //  Else
      //  {
          Return (0) 
      //  }
      }
      // MethodId 51 - SetODStatus
      if (LEqual(Arg1, 51)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: OverDrive closed
      //          1: OverDrive open
      //  If(LAnd(PANT,0x2))  //BIT1:support OD
      //  {
      //    If(LEqual(ToInteger(Arg2),1))  //OverDrive open
      //    {
      //      \_SB.SGOV(GPIO_CNL_H_GPP_K4, PCH_OD_ON)
      //    }
      //    Else
      //    {
      //      \_SB.SGOV(GPIO_CNL_H_GPP_K4, PCH_OD_OFF)
      //    }
      //  }
      //  Else
      //  {
      //    \_SB.SGOV(GPIO_CNL_H_GPP_K4, PCH_OD_OFF)
      //  }
        Return (0) 
      }
      // MethodId 52 - SetLightControlOwner
      if (LEqual(Arg1, 52)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: EC control
      //          1: Software control
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
      //      Store(ToInteger(Arg2), EC0_SCOPE.EC0.LCON)
            Release(EC0_SCOPE.EC0.LfcM)
            Return (0) 
          }
        }
      }
 
      // MethodId 53 - SetDDSControlOwner
      if (LEqual(Arg1, 53)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: BIOS control
      //          1: Software control
      //  Store(ToInteger(Arg2), DDSC)
        Return (0) 
      }
      // MethodId 54 - IsRestoreOCValue
      if (LEqual(Arg1, 54)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          0: Do not need to restore default oc
      //          1: Need to restore default oc
      //  if(LEqual(ToInteger(Arg2), 1)) //Input parameter,1:clear restore default oc
      //  {
          //clear RDOC flag and change RestoreDefaultOverClocking to disable
          //Store (0, RDOC)
          //Store(FUNCTION_RESTORE_DEFAULT_OC, SMBA)
          //clear EC 8s reset flag
          //Store (0, EC0_SCOPE.EC0.RS8F)
          //Return (0) 
      //  }
      //  Else
      //  {
      //    if(Lor(LEqual(RDOC, 1),LEqual(EC0_SCOPE.EC0.RS8F, 1)))
      //    {
      //      Return (1) 
      //    }
      //    else
      //    {
            Return (0) 
      //    }
      //  }
     }
      // MethodId 55 - GetThermalMode
      if (LEqual(Arg1, 55)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          Retrun BIOS real mode
      //          1: Quiet Mode - Low Speed Fan
      //          2: Balance Mode - Balance Acoustic
      //          3: Performance Mode - High Speed Fan
        If (EC0_SCOPE.EC0.ECAV) {
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          //  If(LEqual(EC0_SCOPE.EC0.SPMO, 0x02)){
          //   Store(1, Local1)
          //  } ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x0)){
          //   Store(2, Local1)
          //  } ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x1)){
          //   Store(3, Local1)
          //  }
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1) 
          }
        }
      }
    }
           
    Method (_WED, 1)
    {
      // Temperature change
      if (LEqual(Arg0, 0xd0)) {    
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ 
            Store(EC0_SCOPE.EC0.GUST,Local1)
            Store(0, EC0_SCOPE.EC0.GUST)
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1)    
          }
        }
      }
      
      // OC button press
      if (LEqual(Arg0, 0xd1)) {
        Return (0xff)
      }
      
      // Gpu Temperature change event
      if (LEqual(Arg0, 0xe0)) {
        Return (0xff)
      }

      // Fancooling finish event
      if (LEqual(Arg0, 0xe1)) {
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)) { 
            If (And(EC0_SCOPE.EC0.FCST, 0x01)){
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0x00)
            } Else {
              Release(EC0_SCOPE.EC0.LfcM)
              Return (0x01)
            }
          }
        }
      }
      
      // TP&Key lock status change event
      if (LEqual(Arg0, 0xe2)) {  
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)) { 
            Store (0, Local2)
      //      If (LEqual(And(EC0_SCOPE.EC0.GDST, 0x02), 0x2)){  
      //        Or(Local2, 0x01, Local2)
      //      } 
            
      //      If (LEqual(And(EC0_SCOPE.EC0.GDST, 0x04), 0x4)){  
      //        Or(Local2, 0x02, Local2)
      //      } 
            Release(EC0_SCOPE.EC0.LfcM)
            Return(Local2)
          }
        }
      }

      // Smart Fan mode change event
      if (LEqual(Arg0, 0xe3)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: Quiet Mode - Low Speed Fan
      //          2: Balance Mode - Balance Acoustic
      //          3: Performance Mode - High Speed Fan
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)) { 
            //If(LEqual(EC0_SCOPE.EC0.SPMO, 0x02)){
            // Store(1, Local1)
            //} ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x00)){
            // Store(2, Local1)
            //} ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x01)){
            // Store(3, Local1)
            //}
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1)
          }
        }
      }

      // Smart Fan setting mode change event
      if (LEqual(Arg0, 0xe4)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: Support Quiet, Balance and Performance Mode
      //          2: Only support Balance and Performance Mode
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)) { 
            //If(LEqual(EC0_SCOPE.EC0.GSFS, 1)){
            // Store(1, Local1)
            //} ElseIf (LEqual(EC0_SCOPE.EC0.GSFS, 0)){
            // Store(2, Local1)
            //}
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1)
          }
        }
      }

      // POWER CHARGE MODE Change EVENT
      if (LEqual(Arg0, 0xe5)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: AC Mode
      //          2: DC Mode
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)) { 
            //If(LEqual(EC0_SCOPE.EC0.ADPT, 1)){
            // Store(1, Local1)
            //} ElseIf (LEqual(EC0_SCOPE.EC0.ADPT, 0)){
            // Store(2, Local1)
            //}
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1)
          }
        }
      }
      //Fn+Space click event, light profile change event
      if (LEqual(Arg0, 0xe6)) {
      //LCFCTODO: please set the below return value, need check your project.
        Return (1)  //software control, APP do not care return value ,but need BIOS send event to APP. 
      }
      //Thermal Mode Real Mode change event
      if (LEqual(Arg0, 0xe7)) {
      //LCFCTODO: please set the below return value, need check your project.
      //          1: Quiet Mode - Low Speed Fan
      //          2: Balance Mode - Balance Acoustic
      //          3: Performance Mode - High Speed Fan
        If (EC0_SCOPE.EC0.ECAV) {                                            
          If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)) { 
      //      If(LEqual(EC0_SCOPE.EC0.SPMO, 0x02)){
      //       Store(1, Local1)
      //      } ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x00)){
      //       Store(2, Local1)
      //      } ElseIf (LEqual(EC0_SCOPE.EC0.SPMO, 0x01)){
      //       Store(3, Local1)
      //      }
            Release(EC0_SCOPE.EC0.LfcM)
            Return (Local1)
          }
        }
      }
   }

   Name (WQDD, Buffer () {
     #include "GzMof.asl"
     })
   }
}
