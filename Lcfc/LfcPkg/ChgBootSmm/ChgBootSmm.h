 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/

#ifndef _BOOT_SEQUENCE_UPDATE_SMM_H_
#define _BOOT_SEQUENCE_UPDATE_SMM_H_


#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmBase2.h>
#include <Protocol/SmmCpu.h>
#include <Protocol/SmmVariable.h>
#include <Library/IoLib.h>
#include <SetupConfig.h>
//[-start-220210-Dongxu0040-Modify]//
#include <Library/LfcEcLib.h>
//[-end-220210-Dongxu0040-Modify]//
#include <Lfc.h>
#include <Protocol/LenovoVariable.h>
#include <Library/LfcSwSmiCpuRegisterLib.h>
#include <Guid/LfcVariableGuid.h>


// This section is for 0xB3 port
//0x30 and 0x31 is for EM use, it will call SALS method to trigger SWSMI
#define FUNCTION_ENABLE_HOT_KEY_MODE      0x30
#define FUNCTION_DISABLE_HOT_KEY_MODE     0x31

#define FUNCTION_ENABLE_USB_CHARGE_MODE       0x32
#define FUNCTION_DISABLE_USB_CHARGE_MODE      0x33

#define FUNCTION_DISABLE_CHARGE_IN_BATTERY_MODE       0x34
#define FUNCTION_ENABLE_CHARGE_IN_BATTERY_MODE       0x35
/////////////////////////////////////////////////////////////////////////////////
////////Please keep these defines sync with the corresponding tool code//////////
/////////////////////////////////////////////////////////////////////////////////
#define BOOT_SEQUENCE_UPDATE_FLAG            0x5300
#define BOOT_SEQUENCE_UPDATE_SWSMI           0xca

#define GET_CURRENT_SEQ_FUNCTION             0x1
#define SET_DEFAULT_FUNCTION                 0x2
#define SET_SEQ_FUNCTION                     0x3
#define SET_UEFI_FIRST_FUNCTION              0x4
#define SET_LEGACY_FIRST_FUNCTION            0x5
#define SET_UEFI_ONLY_FUNCTION               0x6
#define SET_DUAL_BOOT_FUNCTION               0x7
#define LOAD_UEFI_OS_DEFAULT_FUNCTION        0x8
#define LOAD_LEGACY_OS_DEFAULT_FUNCTION      0x9
#define SET_SECURE_BOOT_ENABLE_FUNCTION      0xa
#define SET_FAST_BOOT_ENABLE_FUNCTION        0xb
#define SET_FAST_BOOT_DISABLE_FUNCTION       0xc
#define SET_PXE_BOOT_ENABLE_FUNCTION         0xd
#define SET_PXE_BOOT_DISABLE_FUNCTION        0xe
#define SET_BACK_FLASH_ENABLE_FUNCTION       0x10
#define SET_BACK_FLASH_DISABLE_FUNCTION      0x11
#define LCFC_SECURE_FLASH_ENABLE_FUNCTION    0x12
#define SET_TPM_ENABLE_FUNCTION              0x13
#define SET_TPM_DISABLE_FUNCTION             0x14
#define SB_RESTORE_FACTORY_DEFAULT_FUNCTION  0x15
#define SET_FULLRESET_FUNCTION               0x16
//#define SET_POWER_BEEP_ENABLE_FUNCTION       0x17
//#define SET_POWER_BEEP_DISABLE_FUNCTION      0x18
#define SET_OS_OPTINIZED_ENABLE_FUNCTION     0x19
#define SET_OS_OPTINIZED_DISABLE_FUNCTION    0x1A
#define SET_SATA_RAID_FUNCTION               0x1B
#define SET_SATA_AHCI_FUNCTION               0x1C
#define SET_SECURE_BOOT_DISABLE_FUNCTION     0x1D
#define SET_PXE_FIRST_FUNCTION               0x1E
#define SET_HDD_FIRST_FUNCTION               0x1F
#define SET_USB_FIRST_FUNCTION               0x20
#define SET_EFI_PXE_ONE_TIME_FUNCTION        0x21
#define GET_OPTION_FUNCTION                  0x23
#define SET_GFX_IGPU_ONLY_FUNCTION           0x24
#define SET_GFX_IGPU_DGPU_FUNCTION           0x25
#define SET_GFX_DGPU_ONLY_FUNCTION           0x26

#define SUCCESS                              0x00
#define SMI_NOT_READY                        0x01
#define PARA_WRONG                           0x02
#define SETUP_NOT_FOUND                      0x03
#define FUNCTION_UNSUPPORTED                 0x04
#define OTHER_ERROR                          0xff

//#pragma pack(push)
//#pragma pack(1)
//typedef struct {
//  UINT8                         BootTypeOrderSequence[MAX_BOOT_ORDER_NUMBER];
//} BOOT_DEVICE_DATA;
//#pragma pack(pop)
/////////////////////////////////////////////////////////////////////////////////
////////Please keep these defines sync with the corresponding tool code//////////
/////////////////////////////////////////////////////////////////////////////////
#endif
