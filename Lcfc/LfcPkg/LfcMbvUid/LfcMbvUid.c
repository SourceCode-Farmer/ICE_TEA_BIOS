 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2020, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

/*
Data          Name          Version    Description
2020.09.22    Binn.Wang     v1.00      First Release
*/

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/UefiLib.h>
#include <Uefi/UefiSpec.h>
#include <IndustryStandard/Acpi10.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Guid/EventGroup.h>
#include <Guid/LfcVariableGuid.h>
#include <Lfc.h>
#include <Library/LfcEcLib.h>
#include <Protocol/LenovoVariable.h>
#include <Library/LfcUuidLib/CUUID.h>
//[-start-210705-CISSIE0651-add]//
#include <Library/LfcLib.h>
//[-end-210705-CISSIE0651-add]//



LENOVO_VARIABLE_PROTOCOL      *gLenovoVariable = NULL;

BOOLEAN
ValidUuid (
  IN  UINT8    *Uuid
  )
{
  UINT8           i = 0;
  BOOLEAN         AllZero = TRUE;
  BOOLEAN         AllFF = TRUE;
  for (i = 0; i < sizeof(EFI_GUID); i++) {
    if(Uuid[i] && ((UINT8)(~Uuid[i]))){
      return TRUE;
    }
    AllZero = (Uuid[i])?FALSE:AllZero;
    AllFF = ((UINT8)(~Uuid[i]))?FALSE:AllFF;
  }
  return !(AllZero || AllFF);
}

EFI_STATUS  GetBiosMbvUid(EFI_GUID *Uuid)
{
  EFI_STATUS  Status = EFI_SUCCESS;
  UINT32      DataSize = sizeof(EFI_GUID);

  if(Uuid == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }

  Status = gLenovoVariable->GetVariable (
                                          gLenovoVariable,
                                          &gLfcMbvUidGuid,
                                          &DataSize,
                                          Uuid
                                        );
  return Status;
}

EFI_STATUS  SaveBiosMbvUid(EFI_GUID *Uuid)
{
  EFI_STATUS  Status = EFI_SUCCESS;

  if(Uuid == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }
  Status = gLenovoVariable->SetVariable (
                                          gLenovoVariable,
                                          &gLfcMbvUidGuid,
                                          sizeof(EFI_GUID),
                                          Uuid
                                        ); 
  return Status;
}

EFI_STATUS  GetEcMbvUid(EFI_GUID *Uuid)
{
  if(Uuid == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }
//[-start-210705-CISSIE0651-modify]//follow new LfcPkg
  if (((*(UINT8*)((UINTN)(EC_SRAM_BASE+EC_MBV_UUID_FLAG)))& BIT5) ==0){

    CopyMem((UINT8*)Uuid, (UINT8*)((UINTN)(EC_SRAM_BASE+EC_MBV_UUID_REG)), sizeof(EFI_GUID));
    return EFI_SUCCESS;
    } else{
        return EFI_NOT_READY;
    }
//[-end-210705-CISSIE0651-modify]//

}


EFI_STATUS  SaveEcMbvUid(EFI_GUID *Uuid)
{
  if(Uuid == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }

//[-start-210705-CISSIE0651-modify]//follow new LfcPkg
  CopyMem((UINT8*)((UINTN)(EC_SRAM_BASE+EC_MBV_UUID_REG)), (UINT8*)Uuid, sizeof(EFI_GUID));
  *(UINT8*)((UINTN)(EC_SRAM_BASE+EC_MBV_UUID_FLAG)) |= BIT6;  //EC poll MbvUid Flag, if set, MbvUid will be written to EC EEPROM immediately
//[-end-210705-CISSIE0651-modify]//

  return EFI_SUCCESS;
}

EFI_STATUS  GenerateMbvUid(EFI_GUID *Uuid)
{
  return autoGenerate( (UINT8*)Uuid );
}

EFI_STATUS
EFIAPI
LfcMbvUidEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
  EFI_STATUS    BiosMbvUidStatus = FALSE;
  EFI_STATUS    EcMbvUidStatus = FALSE;
  EFI_GUID      BiosMbvUid;
  EFI_GUID      EcMbvUid;
  EFI_GUID      NewMbvUid;

  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &gLenovoVariable);
  if(EFI_ERROR(Status))
  {
    return Status;
  }
  
  gBS->SetMem(&BiosMbvUid, sizeof(BiosMbvUid), 0);
  gBS->SetMem(&EcMbvUid, sizeof(EcMbvUid), 0);
  gBS->SetMem(&NewMbvUid, sizeof(NewMbvUid), 0);

  BiosMbvUidStatus = GetBiosMbvUid(&BiosMbvUid);
  if((!EFI_ERROR(BiosMbvUidStatus)) && (!ValidUuid((UINT8 *)(&BiosMbvUid))))
  {
    DEBUG((EFI_D_INFO, "BiosMbvUid not valid\n"));
    BiosMbvUidStatus = EFI_NOT_FOUND;
  }

  EcMbvUidStatus = GetEcMbvUid(&EcMbvUid);
  if((!EFI_ERROR(EcMbvUidStatus)) && (!ValidUuid((UINT8 *)(&EcMbvUid))))
  {
    DEBUG((EFI_D_INFO, "EcMbvUid not valid\n"));
    EcMbvUidStatus = EFI_NOT_FOUND;
  }

  DEBUG((EFI_D_INFO, "BiosMbvUidStatus = %r, EcMbvUidStatus = %r\n", BiosMbvUidStatus, EcMbvUidStatus));
  DEBUG((EFI_D_INFO, "BiosMbvUid = %g\n", &BiosMbvUid));
  DEBUG((EFI_D_INFO, "EcMbvUid = %g\n", &EcMbvUid));

  if(EFI_ERROR(BiosMbvUidStatus) && EFI_ERROR(EcMbvUidStatus))
  {
    DEBUG((EFI_D_INFO, "GenerateMbvUid\n"));
    Status = GenerateMbvUid(&NewMbvUid);
    if(EFI_ERROR(Status) || (!ValidUuid((UINT8 *)(&NewMbvUid))))
    {
      DEBUG((EFI_D_ERROR, "GenerateMbvUid Failed, %r\n", Status));
      return Status;
    }
    DEBUG((EFI_D_INFO, "NewMbvUid = %g\n", &NewMbvUid));

    Status = SaveBiosMbvUid(&NewMbvUid);
    DEBUG((EFI_D_INFO, "SaveBiosMbvUid = %r\n", Status));
    Status = SaveEcMbvUid(&NewMbvUid);
    DEBUG((EFI_D_INFO, "SaveEcMbvUid = %r\n", Status));
  }
  else if(EFI_ERROR(BiosMbvUidStatus))
  {
    Status = SaveBiosMbvUid(&EcMbvUid);
    DEBUG((EFI_D_INFO, "SaveBiosMbvUid = %r\n", Status));
  }
  else if(EFI_ERROR(EcMbvUidStatus))
  {
    Status = SaveEcMbvUid(&BiosMbvUid);
    DEBUG((EFI_D_INFO, "SaveEcMbvUid = %r\n", Status));
  }
  else if (!(CompareGuid (&BiosMbvUid, &EcMbvUid)))
  {
    DEBUG((EFI_D_INFO, "BiosMbvUid != EcMbvUid\n"));
    Status = SaveBiosMbvUid(&EcMbvUid);
    DEBUG((EFI_D_INFO, "SaveBiosMbvUid = %r\n", Status));
  }

  Status = gBS->InstallMultipleProtocolInterfaces (
                &ImageHandle,
                &gLfcMbvUidProtocolGuid,
                NULL,
                NULL
                );

  return Status;
}

  
