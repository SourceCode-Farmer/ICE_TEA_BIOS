/*
 #*****************************************************************************
 #  
 # Copyright (c) 2012 - 2018, Hefei LCFC Information Technology Co.Ltd. 
 # And/or its affiliates. All rights reserved. 
 # Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 # Use is subject to license terms.
 # 
 #******************************************************************************
 
Data          Name                      Version    Description
2018.09.29    Aladdin.Kong/Rita.Zhang   v1.00      Initial release
2018.11.10    Rita.Zhang                v1.01      New spec to follow, SSD + HDD also need change to RST mode
2018.01.08    Rita.Zhang                v1.02      Keep optane relate settings when flash BIOS restore NVROM. 
2019.01.30    Rita.Zhang                v1.02      To optimize the driver's structure and add support TG detect function.
Module Name:
  OptaneMemoryExistChgToRAID.c
*/ 
   
#include <Library/LfcLib.h>
#include <Library/IoLib.h>
#include <Uefi.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <LfcPeiOptaneMemoryHob.h>
#include <Guid/HobList.h>
#include <Library/HobLib.h>
#include <SetupVariable.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Guid/EventGroup.h>
#include <Guid/H2OBdsCheckPoint.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/LfcEcLib.h>
#include <Protocol/LenovoVariable.h>
//#include <Library/UefiBootServicesTableLib.h>
#include <Library/H2OCpLib.h>
#include <Library/MmPciLib.h>
//
// LCFCTODO: You must follow your project setting. Please modify Optane PCIE port follow project.
//
#define PCIEStorgeDevPort   11

//
// LCFCTODO: You must follow your project setting. Please modify These follow project.
// SATABusNo,SATADevNo,SATAFuncNo need check follow project
// SATAPCSRegister need check PCH spec to know register No.
// SATAPort need check Circuit diagram and PCH spec to  know bit
// SATAPortsShift need sync with SATAPort
#define      SATABusNo   0x00
#define      SATADevNo   0x17
#define      SATAFuncNo   0x00
#define      SATAPCSRegister 0x94  
#define      SATAPort BIT20
#define      SATAPortsShift 20

EFI_STATUS
IsMfgMode (
  OUT BOOLEAN                        *MfgMode
  ) 
{
  EFI_STATUS                             Status ;
  EFI_GUID                               LvarMfgModeFlagGuid = LVAR_MFG_MODE_FLAG_GUID;
  UINT8                                  Flag =  0;
  UINT32                                 FlagDataSize = 1;
  LENOVO_VARIABLE_PROTOCOL               *LenovoVariable;

  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable);
  if (!EFI_ERROR (Status)) {
    Status = LenovoVariable->GetVariable (
                               LenovoVariable, 
                               &LvarMfgModeFlagGuid, 
                               &FlagDataSize, 
                               (VOID*) (&Flag)
                               );
    if (!EFI_ERROR (Status) && (Flag == 'Y')) {
      // MFG mode
      *MfgMode = TRUE; 
      return EFI_SUCCESS;
    }
  }

  *MfgMode = FALSE;
  return EFI_NOT_FOUND;

}

VOID
EnterBdsCallbackFunction  (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  EFI_STATUS                Status;
  EFI_PEI_HOB_POINTERS      Hob;
  PCH_SETUP                 *PchSetup = NULL;
  UINTN                     PchSetupDataSize;
  UINT32                   IsModifySatamodeToRaid = FALSE;
  BOOLEAN                   MfgMode;
  BOOLEAN                   GetPchVariableSuccess;
  UINTN                     PcieBase;
  UINT32                    BridgeSID;
  UINT32                   SATAPceRegisterValue;
  BOOLEAN                   ExistSataDevice = FALSE;
//  EFI_GUID                  VarGuid = LVAR_SATAMODE_STRING_FLAG_GUID;
//  L05_BACKUP_SATAMODE_ITEM  SataModeFlag;
//  UINT32                    SataModeFlagSize = sizeof (L05_BACKUP_SATAMODE_ITEM); 
  LENOVO_VARIABLE_PROTOCOL  *LenovoVariable;
  MfgMode = FALSE;
  GetPchVariableSuccess = TRUE;
  PchSetupDataSize = sizeof(PCH_SETUP);
  PchSetup = AllocateZeroPool(PchSetupDataSize);
  Status = gRT->GetVariable( L"PchSetup",
                  &gPchSetupVariableGuid,
                  NULL,
                  &PchSetupDataSize,
                  PchSetup);
  if(EFI_ERROR(Status)){
     GetPchVariableSuccess = FALSE;
     goto EXIT;
  }
 
  // Get hob list to check whether there is PCIE device(Optane or SSD).
  Status = EfiGetSystemConfigurationTable (&gEfiHobListGuid, (VOID **) &Hob.Raw);
  if (EFI_ERROR (Status)) {
    goto EXIT;    
  }
  for (; !END_OF_HOB_LIST(Hob); Hob.Raw = GET_NEXT_HOB(Hob)) {
    if ((((Hob).Header->HobType) == EFI_HOB_TYPE_GUID_EXTENSION) && (CompareGuid(&Hob.Guid->Name, &gOptaneMemoryHobGuid))) {
      IsModifySatamodeToRaid = ((OPTANE_MEMORY_HOB *)Hob.Guid)->Value;
      break;
    }
  }
  // Check whether there is HDD device.
  PcieBase = MmPciBase (SATABusNo,
                        SATADevNo,
                        SATAFuncNo);
  BridgeSID = MmioRead32(PcieBase);
  if (BridgeSID == 0xFFFFFFFF) {
    goto EXIT;   
  }
  SATAPceRegisterValue = MmioRead32(PcieBase + SATAPCSRegister);
  ExistSataDevice = (BOOLEAN)((SATAPceRegisterValue & SATAPort) >> SATAPortsShift);
   
  IsMfgMode (&MfgMode);

  if(!MfgMode) {
    goto EXIT;  
  }

  if ((IsModifySatamodeToRaid == 0) && (SATA_MODE_RAID == PchSetup->SataInterfaceMode)) {
		// 1.None NVme device, change to AHCI mode.
    PchSetup->SataInterfaceMode = SATA_MODE_AHCI;
    PchSetup->RstPcieRemapEnabled[PCIEStorgeDevPort-1] = 0; // 0:Not RST Controlled; 1:RST Controlled
    PchSetup->SataRstOptaneMemory = 0;
    Status = gRT->SetVariable( L"PchSetup",
                    &gPchSetupVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    PchSetupDataSize,
                    PchSetup);
    if(EFI_ERROR(Status)) {
      goto EXIT;
    }

    FreePool (PchSetup);
    LfcEcLibEnableEcPower ();       
    IoWrite8 (0xcf9, 0x6);   
  }  else if ((IsModifySatamodeToRaid == 2) && (SATA_MODE_AHCI == PchSetup->SataInterfaceMode) && ExistSataDevice) {
    // 2. Optane device exist, change to RST
    PchSetup->SataInterfaceMode = SATA_MODE_RAID;
    PchSetup->RstPcieRemapEnabled[PCIEStorgeDevPort-1] = 1; // 0:Not RST Controlled; 1:RST Controlled
    PchSetup->SataRaidR0 = 0;
    PchSetup->SataRaidR1 = 0;
    PchSetup->SataRaidR5 = 0;
    PchSetup->SataRaidR10 = 0;
    PchSetup->SataRstOptaneMemory = 1;
    Status = gRT->SetVariable( L"PchSetup",
                    &gPchSetupVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    PchSetupDataSize,
                    PchSetup);
    if(EFI_ERROR(Status)) {
      goto EXIT;
    }

    FreePool (PchSetup);
    LfcEcLibEnableEcPower ();       
    IoWrite8 (0xcf9, 0x6);
  }else if ((IsModifySatamodeToRaid == 1) && (SATA_MODE_AHCI == PchSetup->SataInterfaceMode) && ExistSataDevice) {
    // 3. Nvme SSD device, change to RST
		PchSetup->SataInterfaceMode = SATA_MODE_RAID;
    Status = gRT->SetVariable( L"PchSetup",
                    &gPchSetupVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    PchSetupDataSize,
                    PchSetup);
    if(EFI_ERROR(Status)) {
      goto EXIT;
    }
    FreePool (PchSetup);
    LfcEcLibEnableEcPower ();       
    IoWrite8 (0xcf9, 0x6);
  } else if ((IsModifySatamodeToRaid == 3)&& (SATA_MODE_AHCI == PchSetup->SataInterfaceMode)){
    // 4. TG optane exist, change to RST
		PchSetup->SataInterfaceMode = SATA_MODE_RAID;
    Status = gRT->SetVariable( L"PchSetup",
                    &gPchSetupVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    PchSetupDataSize,
                    PchSetup);
    if(EFI_ERROR(Status)) {
      goto EXIT;
    }
    FreePool (PchSetup);
    LfcEcLibEnableEcPower ();       
    IoWrite8 (0xcf9, 0x6);
  } else {
    // 5. Other case, do nothing
  }

EXIT:
/*
  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable);
  if (!EFI_ERROR (Status)) {
    Status = LenovoVariable->GetVariable (
                               LenovoVariable, 
                               &VarGuid, 
                               &SataModeFlagSize,
                               &SataModeFlag
                             );
    if (!EFI_ERROR (Status)) {//Sata mode flag exit, check Sata mode flag
      if((PchSetup->SataInterfaceMode)!= (SataModeFlag.SataInterfaceMode)){//Sata mode flag diff with Setup, update flag 
        SataModeFlag.SataInterfaceMode = PchSetup->SataInterfaceMode;
        SataModeFlag.SataRaidR0 = PchSetup->SataRaidR0;
        SataModeFlag.SataRaidR1 = PchSetup->SataRaidR1;
        SataModeFlag.SataRaidR5 = PchSetup->SataRaidR5;
        SataModeFlag.SataRaidR10 = PchSetup->SataRaidR10;
        CopyMem (SataModeFlag.RstPcieRemapEnabled,PchSetup->RstPcieRemapEnabled, PCH_MAX_PCIE_ROOT_PORTS);
        SataModeFlag.SataRstOptaneMemory = PchSetup->SataRstOptaneMemory; 
        LenovoVariable->SetVariable (
                          LenovoVariable, 
                          &VarGuid, 
                          SataModeFlagSize,
                          &SataModeFlag
                        );
      }     
    } 
  }
*/
  if (GetPchVariableSuccess) {
    Status = gRT->SetVariable( L"PchSetup",
                    &gPchSetupVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    PchSetupDataSize,
                    PchSetup);
    ASSERT_EFI_ERROR (Status);
  }
  FreePool (PchSetup);
}

EFI_STATUS
EFIAPI
OptaneMemoryExistChgToRAIDEntryPoint (
  IN EFI_HANDLE               ImageHandle,
  IN EFI_SYSTEM_TABLE         *SystemTable
  )
{
  H2O_CP_HANDLE                       CpHandle;
  EFI_STATUS                 Status;
  Status = H2OCpRegisterHandler (
             &gH2OBdsCpInitGuid,
             EnterBdsCallbackFunction,
             H2O_CP_MEDIUM,
             &CpHandle
             );
  return EFI_SUCCESS;
}   
