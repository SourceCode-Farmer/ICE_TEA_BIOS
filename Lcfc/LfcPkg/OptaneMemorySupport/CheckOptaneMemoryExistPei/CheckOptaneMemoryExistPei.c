/* 
 #*****************************************************************************
 #
 # Copyright (c) 2012 - 2018, Hefei LCFC Information Technology Co.Ltd. 
 # And/or its affiliates. All rights reserved. 
 # Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 # Use is subject to license terms.
 # 
 #******************************************************************************

Data          Name                      Version    Description
2018.09.29    Aladdin.Kong/Rita.Zhang   v1.00      Initial release
2018.11.10    Rita.Zhang                v1.01      New spec to follow, SSD + HDD also need change to RST mode
2019.01.30    Rita.Zhang                v1.02      To optimize the driver's structure and add support TG detect function.

Module Name:
  CheckOptaneMemoryExistPei.c
*/ 
#include <Uefi.h>
//#include <Library/PeiOemSvcChipsetLib.h>
#include <Library/IoLib.h>
#include <Library/MmPciLib.h>
#include <IndustryStandard/Pci22.h>
#include <LfcPeiOptaneMemoryHob.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Ppi/MemoryDiscovered.h> 
#include <Library/DebugLib.h>

//
// LCFCTODO: You must follow your project setting. Please modify Optane PCIE port follow project.
//
#define      BusNo   0x00
#define      DevNo   0x1D
#define      FuncNo   0x02

typedef struct {
  UINT32  viddid;
} OPTANE_MEMORY_TABLE;


// Define  callback
EFI_STATUS
EFIAPI
CheckOptaneMemoryCallback (
  IN CONST EFI_PEI_SERVICES                 **PeiServices,
  IN       EFI_PEI_NOTIFY_DESCRIPTOR        *NotifyDescriptor,
  IN       VOID                             *Ppi
  );
   
// Define notify descriptor
static EFI_PEI_NOTIFY_DESCRIPTOR       mNotifyList = {
  
  // Define callback or dispatch?
  EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    
  // Interface Guid address
  &gEfiPeiMemoryDiscoveredPpiGuid,
   
  // Callback function
  CheckOptaneMemoryCallback
  
};   
   
EFI_STATUS
EFIAPI
CheckOptaneMemoryExistEntryPoint (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS            Status;
  
  // NotifyPpi, when MemoryDiscovered callback
  Status = (**PeiServices).NotifyPpi (PeiServices, &mNotifyList);   
   
  return EFI_SUCCESS;
}   

// Callback funtion
EFI_STATUS
EFIAPI
CheckOptaneMemoryCallback (
  IN CONST EFI_PEI_SERVICES                 **PeiServices,
  IN       EFI_PEI_NOTIFY_DESCRIPTOR        *NotifyDescriptor,
  IN       VOID                             *Ppi
  )
{
  EFI_STATUS            Status;
  UINTN                 PcieBase;
  UINT8                 oldCmdReg;
  UINT8                 Optaneresult = 0;
  UINTN                 SecondPcieBase;
  UINT32                BridgeSID;
  UINT32                OldBusValue;
  OPTANE_MEMORY_HOB     *Private;  
  UINT16                ClassCode; 
  // UINTN               i = 0;
      
  PcieBase = MmPciBase (BusNo,
                        DevNo,
                        FuncNo);
  BridgeSID = MmioRead32(PcieBase);
  DEBUG ((DEBUG_INFO, "BridgeSID = 0x%X\n", BridgeSID));

  // Now save the original sub bus and subordinate bus number
  OldBusValue = MmioRead32(PcieBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET);
  oldCmdReg = MmioRead8(PcieBase + PCI_COMMAND_OFFSET);
  // Set the sub bus number to 0xE0 and subordinate bus number to 0xE0
  MmioWrite32(PcieBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, 0x00E0E000);
  MmioOr8(PcieBase + PCI_COMMAND_OFFSET, 0x07);
  // Now read the device on the sub bus
  SecondPcieBase = MmPciBase(0xE0, 0x0, 0x0);
  BridgeSID = MmioRead32(SecondPcieBase); 
  ClassCode = MmioRead16(SecondPcieBase + 0x0A);   
  // ClassCod16 = MmioRead32(SecondPcieBase + 0A);
  DEBUG ((DEBUG_INFO, "the bus 0xE0, Device 0, function 0 VID DID = 0x%X\n", BridgeSID));  
  DEBUG ((DEBUG_INFO, "class code = 0x%X\n", ClassCode));
  if(ClassCode == 0x0108){
    IoWrite8 (0x70, 0x7A);   
    IoWrite8 (0x71, 2);// Write cmos =2  Nvme exist show SATA mode
    Optaneresult = 1;//NVme device
    if (BridgeSID == 0x25228086){
      Optaneresult = 2;//Optane M10 or Optane SB device
    }
    //TG support
    if (BridgeSID == 0x09758086){
      Optaneresult = 3;//Optane TG device
    }
  } else {
    IoWrite8 (0x70, 0x7A);
    IoWrite8 (0x71, 0);// Write cmos = 0 nVme not exist
    Optaneresult = 0;
  }
  // Restore the original sub bus and suordinate bus number and command register
  MmioWrite8(PcieBase + PCI_COMMAND_OFFSET, oldCmdReg);
  MmioWrite32(PcieBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, OldBusValue);
    
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (OPTANE_MEMORY_HOB),
             (VOID **) &Private);
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "Create Optane memory hob success!\n")); 
    Private->Header.Name = gOptaneMemoryHobGuid;
    Private->Value = Optaneresult;
  }
  return EFI_SUCCESS;
  
}