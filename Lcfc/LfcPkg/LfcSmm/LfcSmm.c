/** @file
//*****************************************************************************
//
// Copyright (c) 2012 - 2016, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
*/
// Project SMM Common code here
/*
Data          Name          Version    Description
2015.07.18    Antony.Guo    v1.00      Initial Release
2016.01.29    Cissie.Gu     v1.01      Add a new function ,which is used to notify EC to
                                       keep on power when use RTC to wake up PC from S4 
                                       in DC mode.
*/

//
// Module specific Includes
//
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmSxDispatch2.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmPowerButtonDispatch2.h>
#include <Uefi/UefiSpec.h>
#include <Library/UefiLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Guid/HobList.h>
#include <Library/IoLib.h>
#include <IndustryStandard/Pci.h>
#include <Library/LfcEcLib.h>
#include <Library/OemSvcLfcS3S4CBWBCallBack.h>

/**

**/
EFI_STATUS
LfcS3EntryCallback (
  IN  EFI_HANDLE                    DispatchHandle,
  IN  CONST VOID                    *DispatchContext,
  IN  OUT VOID                      *CommBuffer OPTIONAL,
  IN  UINTN                         *CommBufferSize OPTIONAL
  )
{

  EFI_STATUS                                Status = EFI_SUCCESS;

  Status = OemSvcLfcSmmS3S4CbWbCallback ((UINT8)SxS3);

  return Status;
}

EFI_STATUS
LfcS4EntryCallback (
  IN  EFI_HANDLE                    DispatchHandle,
  IN  CONST VOID                    *DispatchContext,
  IN  OUT VOID                      *CommBuffer OPTIONAL,
  IN  UINTN                         *CommBufferSize OPTIONAL
  )
{

  EFI_STATUS                                Status = EFI_SUCCESS;

  Status = OemSvcLfcSmmS3S4CbWbCallback ((UINT8)SxS4);

  return Status;
}

EFI_STATUS
LfcS5EntryCallback (
  IN  EFI_HANDLE                    DispatchHandle,
  IN  CONST VOID                    *DispatchContext,
  IN  OUT VOID                      *CommBuffer OPTIONAL,
  IN  UINTN                         *CommBufferSize OPTIONAL
  )
{

  EFI_STATUS                                Status = EFI_SUCCESS;

  Status = OemSvcLfcSmmS3S4CbWbCallback ((UINT8)SxS5);

  return Status;
}

EFI_STATUS
LfcPowerButtonCallback (
  IN  EFI_HANDLE                   DispatchHandle,
  IN  CONST  VOID                  *DispatchContext,
  IN  OUT  VOID                    *CommBuffer,
  IN  OUT  UINTN                   *CommBufferSize
  )
{
  EFI_STATUS                                Status = EFI_SUCCESS;

  LfcEcLibDisableEcPower();

  Status = OemSvcLfcSmmS3S4CbWbCallback ((UINT8)(SxS5 + 2));

  return Status; 
}

STATIC
EFI_STATUS
LfcSwSmiCallback (
  IN EFI_HANDLE                     DispatchHandle,
  IN  CONST VOID                    *DispatchContext,
  IN  OUT VOID                      *CommBuffer OPTIONAL,
  IN  UINTN                         *CommBufferSize OPTIONAL
  )
{
  EFI_STATUS      Status = EFI_SUCCESS;

  return Status;
}

EFI_STATUS
InSmmFunction (
  IN  EFI_HANDLE        ImageHandle,
  IN  EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_SMM_SX_DISPATCH2_PROTOCOL *SxDispatchProtocol;
  EFI_SMM_SW_DISPATCH2_PROTOCOL *SwDispatch;
  EFI_SMM_SX_REGISTER_CONTEXT   EntryDispatchContext;
  EFI_SMM_POWER_BUTTON_DISPATCH2_PROTOCOL   *PowerButtonDispatch;
  EFI_SMM_POWER_BUTTON_REGISTER_CONTEXT     PowerButtonContext;
  //EFI_SMM_SW_REGISTER_CONTEXT   SwContext; //need add this code in projects
  EFI_HANDLE                    SwDispatchHandle;
  EFI_HANDLE                    S1DispatchHandle;
  EFI_HANDLE                    S3DispatchHandle;
  EFI_HANDLE                    S4DispatchHandle;
  EFI_HANDLE                    S5DispatchHandle;
  EFI_HANDLE                    PowerButtonHandle;
  EFI_STATUS                    Status;

  SwDispatchHandle        = NULL;
  S1DispatchHandle        = NULL;
  S3DispatchHandle        = NULL;
  S4DispatchHandle        = NULL;
  S5DispatchHandle        = NULL;
  PowerButtonHandle       = NULL;


  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmSxDispatch2ProtocolGuid,
                    NULL,
                    (VOID **) &SxDispatchProtocol
                    );
  ASSERT_EFI_ERROR (Status);
  //
  // Register S3 entry phase call back function
  //
  EntryDispatchContext.Type   = SxS3;
  EntryDispatchContext.Phase  = SxEntry;
  Status = SxDispatchProtocol->Register (
                                SxDispatchProtocol,
                                LfcS3EntryCallback,
                                &EntryDispatchContext,
                                &S3DispatchHandle
                                );
  ASSERT_EFI_ERROR (Status);
  //
  // Register S4 entry phase call back function
  //
  EntryDispatchContext.Type   = SxS4;
  EntryDispatchContext.Phase  = SxEntry;
  Status = SxDispatchProtocol->Register (
                                SxDispatchProtocol,
                                LfcS4EntryCallback,
                                &EntryDispatchContext,
                                &S4DispatchHandle
                                );
  ASSERT_EFI_ERROR (Status);
  //
  // Register S5 entry phase call back function
  //
  EntryDispatchContext.Type   = SxS5;
  EntryDispatchContext.Phase  = SxEntry;
  Status = SxDispatchProtocol->Register (
                                SxDispatchProtocol,
                                LfcS5EntryCallback,
                                &EntryDispatchContext,
                                &S5DispatchHandle
                                );
  ASSERT_EFI_ERROR (Status);
  //
  // Register power button entry phase call back function
  //
  Status = gSmst->SmmLocateProtocol (&gEfiSmmPowerButtonDispatch2ProtocolGuid, NULL, (VOID**)&PowerButtonDispatch);
  ASSERT_EFI_ERROR (Status);
  
  //
  // Register the power button SMM event
  //
  PowerButtonContext.Phase = EfiPowerButtonEntry;
  Status = PowerButtonDispatch->Register (
                                  PowerButtonDispatch,
                                  LfcPowerButtonCallback,
                                  &PowerButtonContext,
                                  &PowerButtonHandle
                                  );
  ASSERT_EFI_ERROR (Status);
  //
  // Locate the SMM SW dispatch protocol
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmSwDispatch2ProtocolGuid,
                    NULL,
                    &SwDispatch
                    );

  ASSERT_EFI_ERROR (Status);
  //
  // Register SWSMI handler
  //
/*    //need add this code in projects
  SwContext.SwSmiInputValue = LCFC_SW_SMI_ENUMERATE;
  Status = SwDispatch->Register (
                        SwDispatch,
                        LfcSwSmiCallback,
                        &SwContext,
                        &SwDispatchHandle
                        );
  ASSERT_EFI_ERROR (Status);
*/
  return Status;
}

EFI_STATUS
LfcSmmEntryPoint (
  IN EFI_HANDLE               ImageHandle,
  IN EFI_SYSTEM_TABLE         *SystemTable
  )
{
  EFI_STATUS                    Status;
  Status = TRUE;
  DEBUG ((DEBUG_INFO, "LfcSmmEntryPoint\n"));
  //
  // Do project code here............
  //
  return InSmmFunction (ImageHandle, SystemTable);
}

