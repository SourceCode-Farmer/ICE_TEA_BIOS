 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 /*++
Abstract:
  This driver is used BIOS WMI feature, it will public BIOS WMI SSDT table, register
software SMI handler for dealing with the request by APP.
 
History:
  Date          Name          Version    Change Notes
  2015.12.7     Steven Wang   v1.00      Initial release

Module Name:
  BiosWmiSmm.h
 --*/

#ifndef __BIOS_WMI_SMM_H__
#define __BIOS_WMI_SMM_H__

#include <PiDxe.h>
#include <IndustryStandard/Acpi.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/AcpiTable.h>
#include <Protocol/SmmVariable.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/VariableLib.h>
#include <SetupConfig.h>
#include <Protocol/SysPasswordService.h>
#include <Protocol/LenovoVariable.h>
#include <Library/MemoryAllocationLib.h>
#include <Guid/GlobalVariable.h>
#include <Library/PrintLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Uefi/UefiSpec.h>

#include "BiosWmiDefine.h"

#define SYS_SUPERVISOR_PASSWORD_LOCK                 BIT0
#define SYS_USER_PASSWORD_LOCK                       BIT1
#define HDD_MASTER_PASSWORD_LOCK                     BIT2
#define HDD_USER_PASSWORD_LOCK                       BIT3

#pragma pack(1)
typedef struct {
  UINT8   Password[34];
  UINT8   CurrentPassword[34];
  UINT8   NewPassword[34];
  UINT8   PasswordType;
  UINT8   PasswordIndicator;
  UINT8   EncodeTypeForSave;
  UINT8   EncodeTypeForCheck;
  UINT8   AsciiLang;
  UINT8   ScuItem[25];
  UINT8   ScuValue;
  UINT8   TempValue; 
  UINT8	  SettingFlag;
  UINT8   LoadDefaultFlag;
  UINT8   ScuPreviousValue;
  UINT32  WmiBootorder; 
  UINT32  TempWmiBootorder; 
  UINT32  PreviousWmiBootorder; 
  UINT8   WmiBootCount;
  UINT8   BiosWmiSwSmi;
  UINT8   SmiSubNum;
  UINT32  ReturnCode;
  UINT16  TempValue0;
  UINT16  TempValue1;
  UINT16  TempValue2;
  UINT16  TempValue3;
  UINT16  TempValue4;
  UINT16  TempValue5;
  UINT16  TempValue6;
  UINT16  TempValue7;
  UINT32  TempValue8;
  UINT8   TmpString0[16];
  UINT8   TmpString1[16];
} BIOS_WMI_NVS;

typedef struct {
  UINT8                  OpRegionOp;
  UINT32                 NameString;
  UINT8                  RegionSpace;
  UINT8                  DWordPrefix;
  UINT32                 RegionOffset;
  UINT8                  BytePrefix;
  UINT8                  RegionLen;
} AML_OP_REGION_32_8;

typedef struct {
  UINT16                 BootOrder;
  UINT32                 BootType;
} BOOT_OPTION_MAP;
#pragma pack()
 
#endif  // __BIOS_WMI_SMM_H__
