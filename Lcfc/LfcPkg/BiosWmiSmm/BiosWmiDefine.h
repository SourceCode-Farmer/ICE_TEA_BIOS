 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 /*++
Module Name:
  BiosWmiDefine.h
 --*/

#ifndef __BIOS_WMI_DEFINE_H__
#define __BIOS_WMI_DEFINE_H__

//
// ASL WMI interface definitiions
//

// WMI status code
#define WMI_SUCCESS                                  0
#define WMI_UNSUPPORTED                              0x80000001
#define WMI_INVALID_PARAMETER                        0x80000002
#define WMI_ACCESS_DENIED                            0x80000003
#define WMI_SYSTEM_BUSY                              0x80000004
#define WMI_GET_BOOTORDER_FAIL                       0x80000005   
#define WMI_GET_BOOTOPTION_FAIL                      0x80000006 
#define WMI_UNKNOWN_BOOT_OPTION                      0x80000007
#define WMI_INVALID_BOOT_SORT_REQUEST                0x80000008

// Password encoding
#define WMI_ENCODE_ASCII                             0
#define WMI_ENCODE_SCANCODE                          1
                                                     
// Keyboard language                                 
#define WMI_KBD_US                                   0
#define WMI_KBD_FR                                   1
#define WMI_KBD_GR                                   2
                                                     
// Password type                                     
#define WMI_PSWD_SVP                                 1
#define WMI_PSWD_POP                                 2
#define WMI_PSWD_UHDP1                               3
#define WMI_PSWD_MHDP1                               4
#define WMI_PSWD_UHDP2                               5
#define WMI_PSWD_MHDP2                               6
#define WMI_PSWD_UHDP3                               7
#define WMI_PSWD_MHDP3                               8
                                                     
// Passowrd mode                                     
#define WMI_PMOD_LEGACY                              0
#define WMI_PMOD_PHRASE                              1
#define WMI_PMOD_HPM                                 2
                                                     
// Password install state                            
#define WMI_PSTS_POP                                 0x01
#define WMI_PSTS_SVP                                 0x02
#define WMI_PSTS_HDP1                                0x04
#define WMI_PSTS_HDP2                                0x08
#define WMI_PSTS_HDP3                                0x10
#define WMI_PSTS_HDP4                                0x20
                                                     
// Password Length                                   
#define WMI_MAX_PASSWORD_LENGTH                      16
                                                     
// Supported password encoding                       
#define WMI_ENCCAP_ASCII                             0x01
#define WMI_ENCCAP_SCANCODE                          0x02
                                                     
// Supported keyboard language                       
#define WMI_KBDCAP_US                                0x01
#define WMI_KBDCAP_FR                                0x02
#define WMI_KBDCAP_GR                                0x04
                                                     
// SCU item value                                    
#define WMI_ENABLE                                   0x01
#define WMI_DISABLE                                  0x00
#define WMI_USB_ENABLE                               0x00
#define WMI_USB_DISABLE                              0x01
#define WMI_OS_OPTIMIZE_ENABLE                       0x00
#define WMI_OS_OPTIMIZE_DISABLE                      0x01
#define WMI_UEFI                                     0x02
#define WMI_LEGACY_SUPPORT                           0x00

// SMI number
#define SMM_BIOS_WMI_CALL                            0xCE
#define SMM_SYS_PASSWORD_VALIDATE_SUB_CALL           0x01
#define SMM_SCU_SAVE_BIOS_SETTING_SUB_CALL           0x02
#define SMM_SCU_GET_BIOS_CURRENT_SETTING             0x03
#define SMM_SCU_CHANGE_BIOS_PASSWORD_SETTING         0x04
#define SMM_SCU_GET_PASSWORD_STATE                   0x05

// Device Type
#define EFI_SATA                1
#define EFI_USB                 2
#define EFI_DVD                 3
#define EFI_PXE                 4
#define LEGACY_SATA             5
#define LEGACY_USB              6
#define LEGACY_DVD              7
#define LEGACY_PXE              8 
#define EFI_IPV4                9
#define EFI_IPV6                10
#endif