 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 /*++  
Abstract:
  This ACPI SSDT table is used for communication between BIOS WMI ASL and BIOS SMM C code.
   
History:
  Date          Name          Version    Change Notes
  2015.12.07    Steven Wang   v1.00      Initial release
  2016.04.14    Steven Wang   v1.01      Add Lenovo_GetBiosSelections class method imlementation.

Module Name:
  BiosWmiSmm.aml
 --*/
#include "BiosWmiDefine.h"
DefinitionBlock (
  "BiosWmiSmm.aml",
  "SSDT",
  2,
  "Insyde",
  "WmiTable",
  0x1000
  )
{
  Scope (\_SB)
  {
    //
    // WMI-to-ACPI mapper device.
    //
    Device(WMI6)
    {
      // PNP0C14 is Plug and Play ID assigned to WMI mapper
      Name(_HID, EISAID("PNP0C14"))
      Name(_UID, "BWMI")
      
      //
      // Operational region for Smi port access
      //
      OperationRegion (SMIP, SystemIO, FixedPcdGet16 (PcdSoftwareSmiPort), 1)
      Field (SMIP, ByteAcc, NoLock, Preserve)
      { 
          IOB2, 8
      }
      
      //
      // Operational region for BIOS WMI support
      //
      OperationRegion (BNVS, SystemMemory, 0xFFFF0000, 0xFF)
      Field (BNVS, AnyAcc, NoLock, Preserve)
      {
        IPDB,   272, // Input password, need be validated in SMI handler
        ICHP,   272, // Input current hdd password
        INHP,   272, // Input new hdd password
        PWDT,   8,   // Password type, bit0: supervisor password, bit1: user password, bit2: Hdd master, bit3: Hdd user
        PSWI,   8,   // Password indicator, bit0: supervisor password, bit1: user password, bit2: Hdd master, bit3: Hdd user
        ENCO,   8,   // 0: ascii, 1: scancode (for check)
        ENC1,   8,   // 0: ascii, 1: scancode (for save)
        LANG,   8,   // 0: us, 1:fr, 2:gr
        SCUI,   200, // Pass SCU item string for SMI setting
        SCUC,   8,   // Enable or disable SCU item
        TEMP,   8,   // Temp
        SETF,   8,   // The flag be used for query item and setting item
        LDDF,   8,   // 0: Not Load default, 1: Load default
        BSCU,   8,   // Previous SCU Enable or Disable value
        BORD,   32,  // 0:EFI_SATA, 1:EFI_USB, 2:EFI_DVD, 3:EFI_PXE, 4:SATA, 5:USB, 6:DVD, 7:PXE, 8:EFI_IPV4, 9:EFI_IPV6
                     // [BIT15-BIT0] for UEFI device, [BIT31-BIT16] for legacy device, every device type save into 4 bits space
        TBRD,   32,  // Temp boot order list
        PBRD,   32,  // Previous boot order list
        BCNT,   8,   // Boot option count             
        WMSM,   8,   // Software SMI triggle number
        SWSB,   8,   // Sub SMI function number
        RTNC,   32,  // Return code from SMI handler, WMI ASL need it
        TMP0,   16,  // For debug use, remove later.
        TMP1,   16,  // For debug use, remove later.
        TMP2,   16,  // For debug use, remove later.
        TMP3,   16,  // For debug use, remove later.
        TMP4,   16,  // For debug use, remove later.
        TMP5,   16,  // For debug use, remove later. 
        TMP6,   16,  // For debug use, remove later.
        TMP7,   16,  // For debug use, remove later.
        TMP8,   32,  // For debug use, remove later.
        STR0,   128, // For debug use, remove later.
        STR1,   128, // For debug use, remove later.
      }
      
      //
      // _WDG evaluates to a data structure that specifies the data
      // blocks supported by the ACPI device.
      //
      Name(_WDG, Buffer() {
        //C6BD79C5-E4D1-4EF5-898F-0FD94577D547 "CurrentSetting"
        0xC5, 0x79, 0xBD, 0xC6, 0xD1, 0xE4, 0xF5, 0x4E, 0x89, 0x8F, 0x0F, 0xD9, 0x45, 0x77, 0xD5, 0x47,
        65, 48, // Object ID (A0)
        13,     // Instance Count 
        0x05,   // Flags WMIACPI_REGFLAG_EXPENSIVE & STRING 

        //4D0DA889-2B6D-42A2-9DAF-4556E96C7753 "PasswordMode"
        0x89, 0xA8, 0x0D, 0x4D, 0x6D, 0x2B, 0xA2, 0x42, 0x9D, 0xAF, 0x45, 0x56, 0xE9, 0x6C, 0x77, 0x53,
        65, 49, // Object ID (A1)
        0x1,    // Instance Count
        0x05,   // Flags WMIACPI_REGFLAG_EXPENSIVE & STRING
        
        //71A14ADA-B246-41AB-89E5-6CB41D06B1B3 "SetBiosSetting"
        0xda, 0x4a, 0xa1, 0x71, 0x46, 0xb2, 0xab, 0x41, 0x89, 0xe5, 0x6c, 0xb4, 0x1d, 0x06, 0xb1, 0xb3,
        65, 66, // Object ID (AB) 
        1,      // Instance Count
        0x06,   // Flags (WMIACPI_REGFLAG_METHOD)
                //        WMIACPI_REGFLAG_STRING)

        //3422C47D-B360-4621-89B1-BC92F50C37FE "SaveBiosSettings"
        0x7D, 0xC4, 0x22, 0x34, 0x60, 0xB3, 0x21, 0x46, 0x89, 0xB1, 0xBC, 0x92, 0xF5, 0x0C, 0x37, 0xFE,
        65, 67, // Object ID (AC)
        1,      // Instance Count
        0x06,   // Flags (WMIACPI_REGFLAG_METHOD)
                //        WMIACPI_REGFLAG_STRING)

        //97F72490-8F15-4AE8-B624-CB55D30498E5 "DiscardBiosSettings"
        0x90, 0x24, 0xF7, 0x97, 0x15, 0x8F, 0xE8, 0x4A, 0xB6, 0x24, 0xCB, 0x55, 0xD3, 0x04, 0x98, 0xE5,
        65, 68, // Object ID (AD)
        1,      // Instance Count
        0x06,   // Flags (WMIACPI_REGFLAG_METHOD)
                //        WMIACPI_REGFLAG_STRING)       

        //8FE94375-2A5F-49B2-B18F-9666C1A4D158 "LoadDefaultSettings"
        0x75, 0x43, 0xE9, 0x8F, 0x5F, 0x2A, 0xB2, 0x49, 0xB1, 0x8F, 0x96, 0x66, 0xC1, 0xA4, 0xD1, 0x58,
        65, 69, // Object ID (AE)
        1,      // Instance Count
        0x06,   // Flags (WMIACPI_REGFLAG_METHOD)
                //        WMIACPI_REGFLAG_STRING)
                
        //0ACACD66-93D5-4BC6-A87A-5DAAC5153CEC "SetBiosPassword"
        0x66, 0xCD, 0xCA, 0x0A, 0xD5, 0x93, 0xC6, 0x4B, 0xA8, 0x7A, 0x5D, 0xAA, 0xC5, 0x15, 0x3C, 0xEC, 
        65, 70, // Object ID (AF)
        1,      // Instance Count
        0x06,   // Flags (WMIACPI_REGFLAG_METHOD)
                //        WMIACPI_REGFLAG_STRING)

        //10533FE5-EB85-472F-AF56-9C94A4950C7C "Lenovo_GetBiosSelections"
        0xE5, 0x3F, 0x53, 0x10, 0x85, 0xEB, 0x2F, 0x47, 0xAF, 0x56, 0x9C, 0x94, 0xA4, 0x95, 0x0C, 0x7C,
        65, 50, // Object ID (A2)
        1,      // Instance Count
        0x06,   // Flags WMIACPI_REGFLAG_METHOD & STRING   
        
        0x21, 0x12, 0x90, 0x05, 0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0, 0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
        66, 68, // Object ID (BD)
        1,      // Instance Count
        0x00,   // Flags
        })
      
      //
      // Current BIOS Setting String Item in SCU
      //  
      Name (SITM, Package() {
        "Wireless LAN",
        "Power Beep",
        "Intel Virtual Technology", 
        "BIOS Back Flash", 
        "HotKey Mode", 
        "Intel Platform Trust", 
        "Secure Boot", 
        "Boot Mode", 
        "Fast Boot",
        "USB Boot", 
        "PXE Boot to LAN", 
        "OS Optimized Default",
        "BootOrder",
        "Password",
      })

      Name (SITV, Package() {
        Package() {"Wireless LAN;", "Enable,Disable"},
        Package() {"Power Beep;", "Enable,Disable"},
        Package() {"Intel Virtual Technology;", "Enable,Disable"},
        Package() {"BIOS Back Flash;", "Enable,Disable"},
        Package() {"HotKey Mode;", "Enable, Disable"},
        Package() {"Intel Platform Trust;", "Enable,Disable"},
        Package() {"Secure Boot;", "Enable,Disable"},
        Package() {"Boot Mode;", "UEFI,Legacy Support"},
        Package() {"Fast Boot;", "Enable,Disable"},
        Package() {"USB Boot;", "Enable,Disable"},
        Package() {"PXE Boot to LAN;", "Enable,Disable"},
        Package() {"OS Optimized Default;", "Enable,Disable"},
        Package() {"BootOrder;", "EFI_SATA,EFI_USB,EFI_DVD,EFI_PXE"},
        Package() {"Password;","Administrator Password,User Password"},
      })
      // Configuration Page
      Name(EAD0, "Wireless LAN,Enable")
      Name(DAD0, "Wireless LAN,Disable")  
      Name(EAD1, "Power Beep,Enable")
      Name(DAD1, "Power Beep,Disable")
      Name(EAD2, "Intel Virtual Technology,Enable")  
      Name(DAD2, "Intel Virtual Technology,Disable")     
      Name(EAD3, "BIOS Back Flash,Enable")     
      Name(DAD3, "BIOS Back Flash,Disable")
      Name(EAD4, "HotKey Mode,Enable")     
      Name(DAD4, "HotKey Mode,Disable")

      // Security Page
      Name(EAD5, "Intel Platform Trust,Enable")     
      Name(DAD5, "Intel Platform Trust,Disable")
      Name(EAD6, "Clear Intel PTT Key,Enter")    
      Name(EAD7, "Secure Boot,Enable")     
      Name(DAD7, "Secure Boot,Disable")
      Name(EAD8, "Reset to Setup Mode,Enter")     
      Name(EAD9, "Restore Factory Keys,Enter")

      // Boot Page
      Name(EADA, "Boot Mode,UEFI")     
      Name(DADA, "Boot Mode,Legacy Support")
      Name(EADB, "Fast Boot,Enable")    
      Name(DADB, "Fast Boot,Disable")  
      Name(EADC, "USB Boot,Enable")       
      Name(DADC, "USB Boot,Disable")    
      Name(EADD, "PXE Boot to LAN,Enable")       
      Name(DADD, "PXE Boot to LAN,Disable")     
      
      // Exit Page
      Name(EADE, "OS Optimized Default,Enable") 
      Name(DADE, "OS Optimized Default,Disable") 
     
      Name(INVD, "Invalid Data")

      Name(PSET, Buffer(0x18){})
      
      Method(TSMI, 1, Serialized) {
        Store (ToInteger(Arg0), SWSB)   // Pass sub SMI number
        Store (SMM_BIOS_WMI_CALL, IOB2) // Triggle SMI
      }
        
      //
      // String Query data block
      // Arg0 has the instance being queried
      Method(WQA0, 1, Serialized) {
        Store (0x00, SETF)  // Set query item flag
        If (LEqual(Arg0, Zero)) {
          Store("Wireless LAN", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting   
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD0)  
          }
          Return(EAD0)
        }
        If (LEqual(Arg0, 1)) {
          Store("Power Beep", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD1)  
          }
          Return(EAD1)
        }
        If (LEqual(Arg0, 2)) {
          Store("Intel Virtual Technology", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting  
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD2)  
          }
          Return(EAD2)
        }  
    
        If (LEqual(Arg0, 3)) {
          Store("BIOS Back Flash", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting   
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD3)  
          }
          Return(EAD3)
        } 
        If (LEqual(Arg0, 4)) {
          Store("HotKey Mode", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting  
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD4)  
          }
          Return(EAD4)
        } 

        If (LEqual(Arg0, 5)) {
          Store("Intel Platform Trust", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD5)  
          }
          Return(EAD5)
        } 
        
        If (LEqual(Arg0, 6)) {
          Store("Secure Boot", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DAD7)  
          }
          Return(EAD7)
        }    
        
        If (LEqual(Arg0, 7)) {
          Store("Boot Mode", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DADA)  
          }
          Return(EADA)
        }   

        If (LEqual(Arg0, 8)) {
          Store("Fast Boot", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DADB)  
          }
          Return(EADB)
        }        

        If (LEqual(Arg0, 9)) {
          Store("USB Boot", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(EADC)  
          }
          Return(DADC)
        } 

        If (LEqual(Arg0, 10)) {
          Store("PXE Boot to LAN", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(DADD)  
          }
          Return(EADD)
        }       
        
        If (LEqual(Arg0, 11)) {
          Store("OS Optimized Default", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          If (LEqual(SCUC, 0)) {
            Return(EADE)  
          }
          Return(DADE)
        }  

        If (LEqual(Arg0, 12)) {
          Store("BootOrder", SCUI)
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING) // Get current BIOS setting 
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          Store ("BootOrder,", Local0)
          Store (0, Local1)
          Store (BCNT, Local2) 
          While (Local2) {
            Store (And(ShiftRight(BORD, Local1), 0x0F), Local3)
            Switch (ToInteger(Local3)) {
              case (EFI_SATA) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "EFI_SATA", Local0)
                } Else {
                  Concatenate(Local0, "EFI_SATA:", Local0)
                }
              }
              case (EFI_USB) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "EFI_USB", Local0)
                } Else {
                  Concatenate(Local0, "EFI_USB:", Local0)
                }
              }
              case (EFI_DVD) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "EFI_DVD", Local0)
                } Else {
                  Concatenate(Local0, "EFI_DVD:", Local0)
                }
              }
              case (EFI_PXE) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "EFI_PXE", Local0)
                } Else {
                  Concatenate(Local0, "EFI_PXE:", Local0)
                }
              }   
              case (LEGACY_SATA) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "SATA", Local0)
                } Else {
                  Concatenate(Local0, "SATA:", Local0)
                }
              }
              case (LEGACY_USB) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "USB", Local0)
                } Else {
                  Concatenate(Local0, "USB:", Local0)
                }
              }
              case (LEGACY_DVD) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "DVD", Local0)
                } Else {
                  Concatenate(Local0, "DVD:", Local0)
                }
              }
              case (EFI_IPV4) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "EFI_IPV4", Local0)
                } Else {
                  Concatenate(Local0, "EFI_IPV4:", Local0)
                }
              } 
              case (EFI_IPV6) {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "EFI_IPV6", Local0)
                } Else {
                  Concatenate(Local0, "EFI_IPV6:", Local0)
                }
              }
              Default {
                if (LEqual(Local2, 1)) {
                  Concatenate(Local0, "Unknown", Local0)
                } Else {
                  Concatenate(Local0, "Unknown:", Local0)
                }
              }
            }
            Decrement (Local2)
            Add (Local1, 4, Local1)
          }
          Return (Local0)
        }          
        Return(INVD)
      }

      //
      // String Query data block
      // Arg0 has the instance being queried
      Method(WQA1, 1) {
        Store(WMI_PMOD_PHRASE, Index(PSET, 0x00)) // PasswordMode
        TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
        Store(PSWI, Index(PSET, 0x04)) // PasswordState 
        Store(1, Index(PSET, 0x08)) // MinLength, 1
        Store(16, Index(PSET, 0x0C)) // MaxLength, 16
        Store(3, Index(PSET, 0x10)) // SupportedEncodings, WMI_ENCCAP_ASCII | WMI_ENCCAP_SCANCODE
        Store(7, Index(PSET, 0x14)) // SupportedKeyboard, WMI_KBDCAP_US | WMI_KBDCAP_FR | WMI_KBDCAP_GR
        Return(PSET)
      } 

      //
      // Check if input parameter valid
      //
      Name (STRG, Buffer(100) {})
      Method(CPAR, 1) {
        Store(0, Local0)
        Store(SizeOf(Arg0), Local0)
        If (LEqual(Local0, 0)) {               // Check if null input parameter
          Return (WMI_INVALID_PARAMETER)
        }

        If (LNotEqual(ObjectType(Arg0), 2)) {  // Check if String object
          Return (WMI_INVALID_PARAMETER)
        }
        
        If (LGreaterEqual(Local0, 99)) {      //  Check if buffer overflow
          Return (WMI_INVALID_PARAMETER)
        }
        Store (Arg0, STRG)
        Decrement(Local0)
        Store(DerefOf(Index(STRG, Local0)), Local1)
        If (LNotEqual(Local1, 0x3B)) {
          Return (WMI_INVALID_PARAMETER)
        }
        Return (WMI_SUCCESS)
      }

      //
      // Check if input SCU item valid
      //
      // Arg0: Buffer to parameter
      // Arg1: Item package
      //
      
      Method(CITM, 2) {
        Name (STR0, Buffer(30) {})       // Buffer for Arg0
        Name (STR1, Buffer(30) {})       // Buffer for item index string
        Store (Arg0, STR0)
        Store (0, Local0)
        Store (SizeOf(Arg1), Local1)
        While(LLess(Local0, Local1)) {
          Store(DerefOf(Index(Arg1, Local0)), STR1)
          If (LEqual(ToString(STR0), ToString(STR1))) {
            Return (WMI_SUCCESS)
          }
          Increment(Local0)
        }
        Return (WMI_INVALID_PARAMETER)  // Not found
      }
      //
      // String Method data block
      // Arg0 has the instance being queried
      // Arg1 has the method id
      // Arg2 has the data passed
      
      Method(WMAB, 3) {   
        Name (ISTR, Buffer (100) {0})//"SCU Item, Enable(Disable)" + ',' + "Password" + ',' + "ascii" + ',' + "us"
        Name (SITE, Buffer (30) {0})//"SCU Item"
        Name (CVAL, Buffer (60) {0})//"Enable","Disable", "EFI_SATA:EFI_USB:EFI_PXE:EFI_DVD:EFI_IPV4:EFI_IPV6"
        Name (PSWD, Buffer (34) {0})//"Input Password"
        Name (ENCD, Buffer (9) {0})//"ascii","scancode"
        Name (KBLG, Buffer (3) {0})//"us","fr","gr"
        Name (BNME, Buffer (12) {0})//"EFI_SATA"
        
        Store (Arg2, ISTR)
        If (LNotEqual(CPAR(Arg2), WMI_SUCCESS)){
          Return ("Invalid Parameter") 
        }
        Store (0x01, SETF)  // Set flag	for setting item
        
        //
        // Parse Input Parameter
        //
        Store (0x00, RTNC)
        Store (1, Local1) //Cycle flag
        Store (0, Local0) //Total input item index
        Store (0, Local3) //Each item index
        While (Local1) {
          Store (DerefOf(Index(ISTR, Local0)), Local2)
          If (LEqual(DerefOf(Index(ISTR, Local0)), 0x3B)) { //If ';' break
            Return ("Invalid Parameter")
          }
          If (LEqual(DerefOf(Index(ISTR, Local0)), 0x2C)) { //If ',' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(SITE, Local3))
          Increment (Local3)
          Increment (Local0)
        }
       
        //
        // Check if input SCU item valid
        //
        If (LNotEqual(CITM(SITE, SITM), WMI_SUCCESS)){
          Return ("Invalid Parameter") 
        }
          
        If (LEqual(ToString(SITE), "BootOrder")) {
          Store (0, Local4) // Total boot order item index
          Store (1, Local5) // Cycle flag
          Store (1, Local6) // Cycle flag
          Store (0, Local7) // Shift number
          Store (0, Local3) // Boot order type
          Store (0, Local1) // Temp varible
          Store (0, Local2) // Each boot order item index
          Store (0, BORD)   // Initialize to 0
          
          Store (ToString(SITE), SCUI) // Pass SCU item string
          TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING)// Get boot order count
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Not Supported")
          }
          
          Store (Local0, Local4)
          Increment (Local4)// Pass ','
          While (Local6) {
            Store (0, Local2)
            Store (1, Local5)
            Store ("", BNME)
            While (Local5) {
              Store (DerefOf(Index(ISTR, Local4)), Local1)
              If (LEqual(DerefOf(Index(ISTR, Local4)), 0x3A)) { //If ':' break
                Store (0, Local5)
                Break
              }       
              If (LOr(LEqual(DerefOf(Index(ISTR, Local4)), 0x2C), LEqual(DerefOf(Index(ISTR, Local4)), 0x3B))) { //If ',' or ';' break
                Store (0, Local5)
                Store (0, Local6)
                Break
              }
              Store (Local1, Index(BNME, Local2))
              Increment (Local2)
              Increment (Local4)
            }
            Increment (Local4) // Pass ':' or ',' or ';'
            If (LEqual(ToString(BNME), "EFI_SATA")) {
              Store (EFI_SATA, Local3)
              Or (BORD, ShiftLeft(Local3, Local7), BORD)
            } ElseIf (LEqual(ToString(BNME), "EFI_USB")) {
              Store (EFI_USB, Local3)
              Or (BORD, ShiftLeft(Local3, Local7), BORD)
            } ElseIf (LEqual(ToString(BNME), "EFI_DVD")) {
              Store (EFI_DVD, Local3)
              Or (BORD, ShiftLeft(Local3, Local7), BORD)
            } ElseIf (LEqual(ToString(BNME), "EFI_PXE")) {
              Store (EFI_PXE, Local3)
              Or (BORD, ShiftLeft(Local3, Local7), BORD)
            } ElseIf (LEqual(ToString(BNME), "EFI_IPV4")) {
              Store (EFI_PXE, Local3)
              Or (BORD, ShiftLeft(Local3, Local7), BORD) 
            } ElseIf (LEqual(ToString(BNME), "EFI_IPV6")) {
              Store (EFI_PXE, Local3)
              Or (BORD, ShiftLeft(Local3, Local7), BORD) 
            } Else {
              Return ("Invalid Parameter")
            }
            Add (Local7, 4, Local7)
          }
          If (LNotEqual(Local7, Multiply(BCNT,4))) {
            Return ("Invalid Parameter")
          }
        }

        Increment (Local0) //Pass ','
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(ISTR, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(ISTR, Local0)), 0x2C), LEqual(DerefOf(Index(ISTR, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(CVAL, Local3))
          Increment (Local3)
          Increment (Local0)
        }
        
        If (LNotEqual(DerefOf(Index(ISTR, Local0)), 0x3B)) { //If ";", no need pass
          Increment (Local0) //Pass ','
        }
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(ISTR, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(ISTR, Local0)), 0x2C), LEqual(DerefOf(Index(ISTR, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(PSWD, Local3))
          Increment (Local3)
          Increment (Local0)
        }   

        If (LNotEqual(DerefOf(Index(ISTR, Local0)), 0x3B)) { //If ";", no need pass
          Increment (Local0) //Pass ','
        }
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(ISTR, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(ISTR, Local0)), 0x2C), LEqual(DerefOf(Index(ISTR, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(ENCD, Local3))
          Increment (Local3)
          Increment (Local0)
        }   
        If (LEqual(DerefOf(Index(ISTR, Local0)), 0x2C)) { //If "," 
          If (LEqual(ToString(ENCD), "ascii")) {
            Increment (Local0) //Pass ','
            Store (1, Local1)
            Store (0, Local3)
            While (Local1) {
              Store (DerefOf(Index(ISTR, Local0)), Local2)
              If (LEqual(DerefOf(Index(ISTR, Local0)), 0x3B)) { //If ';' break
                Store (0, Local1)
                Break
              }
              Store (Local2, Index(KBLG, Local3))
              Increment (Local3)
              Increment (Local0)
            } 
          }
        } ElseIf (LEqual(DerefOf(Index(ISTR, Local0)), 0x3B)) {
        } Else {
          Return ("Invalid Parameter")
        }

        //
        // Password Authentication
        // 
        TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
        If (LEqual(And (PSWI,1), 1)) {
          If (LEqual(ToString(ENCD), "ascii")) {
            Store (0, ENC1)
            If (LEqual(ToString(KBLG), "us")) {
              Store (0, LANG)
            } ElseIf (LEqual(ToString(KBLG), "fr")) {
              Store (1, LANG)
            } ElseIf (LEqual(ToString(KBLG), "gr")) {
              Store (2, LANG)
            } Else {
              Return ("Invalid Parameter")
            }   
          } ElseIf (LEqual(ToString(ENCD), "scancode")) {
            Store (1, ENC1)
          } Else {
            Return ("Invalid Parameter")
          }
          Store (ToString(PSWD), IPDB) // Copy input password into NVS for SMM validation
          TSMI (SMM_SYS_PASSWORD_VALIDATE_SUB_CALL)// Triggle SMI for password validate
          Store (0, IPDB) // Clear password buffer
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Access Denied")
          }
        }
        //
        // Clear load default flag
        //
        Store (0, LDDF)
        
        //
        // Pass SCU control value and save the previous setting
        //
        If (LEqual(ToString(CVAL), "Enable")) {
          Store (1, SCUC)
        } ElseIf (LEqual(ToString(CVAL), "Disable")) {
          Store (0, SCUC)
        } ElseIf (LEqual(ToString(CVAL), "Enter")) {
          Store (2, SCUC)
        } ElseIf (LEqual(ToString(CVAL), "UEFI")) {
          Store (2, SCUC)
        } ElseIf (LEqual(ToString(CVAL), "Legacy Support")) {
          Store (0, SCUC)
        } Else {
          If (LNotEqual(ToString(SITE), "BootOrder")) {
            Return ("Invalid Parameter")
          }
        }
        
        //
        // Special case handler for "USB Boot", "OS Optimized Default"
        //
        
        If (LEqual(ToString(SITE), "USB Boot")) {
          If (LEqual(ToString(CVAL), "Enable")) {
            Store (0, SCUC)
          } ElseIf (LEqual(ToString(CVAL), "Disable")) {
            Store (1, SCUC)
          } Else {
            Return ("Invalid Parameter")
          }
        }
        
        If (LEqual(ToString(SITE), "OS Optimized Default")) {
          If (LEqual(ToString(CVAL), "Enable")) {
            Store (0, SCUC)
          } ElseIf (LEqual(ToString(CVAL), "Disable")) {
            Store (1, SCUC)
          } Else {
            Return ("Invalid Parameter")
          }
        }    

        // Save the previous setting
        Store (ToString(SITE), SCUI) // Pass SCU item string
        TSMI (SMM_SCU_GET_BIOS_CURRENT_SETTING)// Get current BIOS setting
        If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
          Return ("Not Supported")
        }
        If (LEqual(ToString(SITE), "BootOrder")) {
          Store (TBRD, PBRD)
        } Else {
          Store (TEMP, BSCU)
        }    
    
        Return ("Success")
      }  

      Method(PSWV, 1) 
      {
        Name (PSWM, Buffer (30) {0})//"Input Password" + "Encode" + "Language"
        Name (ENCD, Buffer (9) {0})//"ascii","scancode"
        Name (KBLG, Buffer (3) {0})//"us","fr","gr"
        Name (PSWD, Buffer(17) {0}) //Input Password 
        Store (Arg0, PSWM)
        Store (0x00, RTNC)
        // Parse Input Parameter
        //
        Store (1, Local1) //Cycle flag
        Store (0, Local0) //Total input item index
        Store (0, Local3) //Each item index
        While (Local1) {
          Store (DerefOf(Index(PSWM, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(PSWM, Local0)), 0x2C), LEqual(DerefOf(Index(PSWM, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(PSWD, Local3))
          Increment (Local3)
          Increment (Local0)
        }
        
        If (LNotEqual(DerefOf(Index(PSWM, Local0)), 0x2C)) { //If not ",", return error
          Return (WMI_INVALID_PARAMETER)
        }
        
        Increment (Local0) //Pass ','
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(PSWM, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(PSWM, Local0)), 0x2C), LEqual(DerefOf(Index(PSWM, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(ENCD, Local3))
          Increment (Local3)
          Increment (Local0)
        }

        If (LEqual(DerefOf(Index(PSWM, Local0)), 0x2C)) { //If ",", check if "ascii"
          If (LEqual(ToString(ENCD), "ascii")) {
            Increment (Local0) //Pass ','
            Store (1, Local1)
            Store (0, Local3)
            While (Local1) {
              Store (DerefOf(Index(PSWM, Local0)), Local2)
              If (LOr(LEqual(DerefOf(Index(PSWM, Local0)), 0x3B), LEqual(Local3, 2))) { //If ',' or count = 2 break
                Store (0, Local1)
                Break
              }
              Store (Local2, Index(KBLG, Local3))
              Increment (Local3)
              Increment (Local0)
            }   
          } Else {
            Return (WMI_INVALID_PARAMETER)
          }
        } ElseIf (LEqual(DerefOf(Index(PSWM, Local0)), 0x3B)) { //If ";", check if "scancode"
          If (LNotEqual(ToString(ENCD), "scancode")) { 
            Return (WMI_INVALID_PARAMETER)
          }
        }
        
        //
        // Password Authentication
        // 
        TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
        If (LEqual(And (PSWI, 1), 1)) {
          If (LEqual(ToString(ENCD), "ascii")) {
            Store (0, ENC1)
            If (LEqual(ToString(KBLG), "us")) {
              Store (0, LANG)
            } ElseIf (LEqual(ToString(KBLG), "fr")) {
              Store (1, LANG)
            } ElseIf (LEqual(ToString(KBLG), "gr")) {
              Store (2, LANG)
            } Else {
              Return (WMI_INVALID_PARAMETER)
            }   
          } ElseIf (LEqual(ToString(ENCD), "scancode")) {
            Store (1, ENC1)
          } Else {
            Return (WMI_INVALID_PARAMETER)
          }
          Store (ToString(PSWD), IPDB) // Copy input password into NVS for SMM validation
          TSMI (SMM_SYS_PASSWORD_VALIDATE_SUB_CALL)// Triggle SMI for password validate
          Store (0, IPDB) // Clear Password buffer
          Return (0)
        }
        Return (0) 
      }
      //
      // String Method data block
      // Arg0 has the instance being queried
      // Arg1 has the method id
      // Arg2 has the data passed 
      Name (ENFG, 0)
      Method(WMAC, 3) 
      {
         If (LEqual(ENFG, 1)) { //0: Success
           Store (0, ENFG)
           Return ("Success")
         }
        //
        // Password Authentication
        //
        TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
        If (LEqual(And (PSWI, 1), 1)) {
          If (LNotEqual(CPAR (Arg2), WMI_SUCCESS)){
            Return ("Invalid Parameter") 
          }
          If (LNotEqual(PSWV (Arg2), WMI_SUCCESS)) {
            Return ("Not Supported")
          }
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Access Denied")
          }
        }
        TSMI (SMM_SCU_SAVE_BIOS_SETTING_SUB_CALL)// Save current BIOS setting
        Store (0, SCUI)
        
        If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
          Return ("Not Supported")
        }
        Store (1, ENFG)
        Return ("Success")
      }
      
      //
      // String Method data block
      // Arg0 has the instance being queried
      // Arg1 has the method id
      // Arg2 has the data passed
      Method(WMAD, 3) { 
        //
        // Password Authentication
        //
        TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
        If (LEqual(And (PSWI, 1), 1)) {
          If (LNotEqual(CPAR (Arg2), WMI_SUCCESS)){
            Return ("Invalid Parameter") 
          }
          If (LNotEqual(PSWV (Arg2), WMI_SUCCESS)) {
            Return ("Not Supported")
          }
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Access Denied")
          }
        }
        Store (BSCU, SCUC)  
        Store (PBRD, BORD)
        Store (0, LDDF)
        Return ("Success")
      }      

      //
      // String Method data block
      // Arg0 has the instance being queried
      // Arg1 has the method id
      // Arg2 has the data passed
      Method(WMAE, 3) { 
        //
        // Password Authentication
        //
        TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
        If (LEqual(And (PSWI, 1), 1)) {  
          If (LNotEqual(CPAR (Arg2), WMI_SUCCESS)){
            Return ("Invalid Parameter") 
          }
          If (LNotEqual(PSWV (Arg2), WMI_SUCCESS)) {
            Return ("Not Supported")
          }
          If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
            Return ("Access Denied")
          }
        }
        Store (1, LDDF)
        Return ("Success")
      }     

      //
      // String Method data block
      // Arg0 has the instance being queried
      // Arg1 has the method id
      // Arg2 has the data passed
      Method(WMAF, 3) { 
        Name (PSWC, Buffer (55) {0}) //"Password Type" + "Current Password" + "New Password" + "Encode" + "Lang"
        Name (PSWT, Buffer (6)  {0}) // Password type
        Name (CPSW, Buffer (17) {0}) // Current Password
        Name (NPSW, Buffer (17) {0}) // New Password
        Name (ENCD, Buffer (9) {0})  //"ascii","scancode"
        Name (KBLG, Buffer (3) {0})  //"us","fr","gr" 

        Store (Arg2, PSWC)
        If (LNotEqual(CPAR (Arg2), WMI_SUCCESS)){
          Return ("Invalid Parameter") 
        }
        Store (0x00, RTNC)
        
        //
        // Parse Input Parameter
        //
        Store (1, Local1) //Cycle flag
        Store (0, Local0) //Total input item index
        Store (0, Local3) //Each item index
        While (Local1) {
          Store (DerefOf(Index(PSWC, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(PSWC, Local0)), 0x2C), LEqual(DerefOf(Index(PSWC, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(PSWT, Local3))
          Increment (Local3)
          Increment (Local0)
        }

        // Check if exist current password, if no password, return
        If (LEqual(ToString(PSWT), "pap")) { //Supervisor password
          TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
          If (LEqual(And (PSWI,1), 0)) {
            Return ("Access Denied")  
          }
          Store (WMI_PSWD_SVP, PWDT) 
          
        } ElseIf (LEqual(ToString(PSWT), "POP")) { //Power-ON
          TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
          If (LEqual(And (PSWI,2), 0)) {
            Return ("Access Denied")  
          } 
          Store (WMI_PSWD_POP, PWDT)
        } ElseIf (LEqual(ToString(PSWT), "mhdp1")) { //Master hdd password
          TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
          If (LEqual(And (PSWI,4), 0)) {
            Return ("Access Denied")  
          }
          Store (WMI_PSWD_UHDP1, PWDT)
        } ElseIf (LEqual(ToString(PSWT), "uhdp1")) { //User hdd password
          TSMI (SMM_SCU_GET_PASSWORD_STATE) // Get current BIOS password setting 
          If (LEqual(And (PSWI,8), 0)) {
            Return ("Access Denied")  
          } 
          Store (WMI_PSWD_MHDP1, PWDT)
        } Else {
          Return ("Invalid Parameter")
        }
        
        If (LNotEqual(DerefOf(Index(PSWC, Local0)), 0x2C)) { //If not ",", return error
          Return ("Invalid Parameter")
        }
        
        Increment (Local0) //Pass ','
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(PSWC, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(PSWC, Local0)), 0x2C), LEqual(DerefOf(Index(PSWC, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(CPSW, Local3))
          Increment (Local3)
          Increment (Local0)
        }

        If (LNotEqual(DerefOf(Index(PSWC, Local0)), 0x2C)) { //If not ",", return error
          Return ("Invalid Parameter")
        }
        
        Increment (Local0) //Pass ','
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(PSWC, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(PSWC, Local0)), 0x2C), LEqual(DerefOf(Index(PSWC, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(NPSW, Local3))
          Increment (Local3)
          Increment (Local0)
        }  
        
        If (LNotEqual(DerefOf(Index(PSWC, Local0)), 0x2C)) { //If not ",", return error
          Return ("Invalid Parameter")
        }
        
        Increment (Local0) //Pass ','
        Store (1, Local1)
        Store (0, Local3)
        While (Local1) {
          Store (DerefOf(Index(PSWC, Local0)), Local2)
          If (LOr(LEqual(DerefOf(Index(PSWC, Local0)), 0x2C), LEqual(DerefOf(Index(PSWC, Local0)), 0x3B))) { //If ',' or ';' break
            Store (0, Local1)
            Break
          }
          Store (Local2, Index(ENCD, Local3))
          Increment (Local3)
          Increment (Local0)
        }
        
        If (LEqual(DerefOf(Index(PSWC, Local0)), 0x2C)) {
          If (LEqual(ToString(ENCD), "ascii")) {
            Store (0, ENCO)
            Increment (Local0) //Pass ','
            Store (1, Local1)
            Store (0, Local3)
            While (Local1) {
              Store (DerefOf(Index(PSWC, Local0)), Local2)
              If (LOr(LEqual(DerefOf(Index(PSWC, Local0)), 0x3B), LEqual(Local3, 2))) { //If ',' or number = 2
                Store (0, Local1)
                Break
              }
              Store (Local2, Index(KBLG, Local3))
              Increment (Local3)
              Increment (Local0)
            } 
            If (LEqual(ToString(KBLG), "us")) {
              Store (0, LANG)
            } ElseIf (LEqual(ToString(KBLG), "fr")) {
              Store (1, LANG)
            } ElseIf (LEqual(ToString(KBLG), "gr")) {
              Store (2, LANG)
            } Else {
              Return ("Invalid Parameter")
            }   
            
          } Else {
            Return ("Invalid Parameter")
          }
        } ElseIf (LEqual(DerefOf(Index(PSWC, Local0)), 0x3B)){
          If (LEqual(ToString(ENCD), "scancode")) {
            Store (1, ENCO)
          } Else {
            Return ("Invalid Parameter")
          }
        } Else {
          Return ("Invalid Parameter")
        }
        
        Store (ToString(CPSW), ICHP) // Input current password
        Store (ToString(NPSW), INHP) // Input new password
        Store("Password", SCUI)
        TSMI (SMM_SCU_CHANGE_BIOS_PASSWORD_SETTING)// Update password sub SMI
        If (LNotEqual(RTNC, WMI_SUCCESS)) { //0: Success
          Return ("Access Denied")
        }
        Return ("Success")
      } 
      
      // "Lenovo_GetBiosSelections"
      // String Method data block
      // Arg0 has the instance being queried
      // Arg1 has the method id
      // Arg2 has the data passed
      Method(WMA2, 3) 
      { 
        //
        // Check if input parameter valid
        //
        If (LNotEqual(CPAR(Arg2), WMI_SUCCESS)){
          Return ("Invalid Parameter") 
        }
        
        Store (SizeOf(SITV), Local0)
        Store (0, Local1)
        While (Local0) {
          Store(DerefOf(Index(SITV, Local1)), Local2)
          Store(DerefOf(Index(Local2, 0)), Local3)
          If (LEqual(Local3, Arg2)) {
            Store(DerefOf(Index(Local2, 1)), Local4)
            Return (Local4) 
          }
          Decrement (Local0)
          Increment (Local1)
        }
        Return ("Invalid Parameter")
      }
      
      Name(WQBD, Buffer(2196) {
        0x46, 0x4f, 0x4d, 0x42, 0x01, 0x00, 0x00, 0x00, 0x84, 0x08, 0x00, 0x00, 0x5e, 0x37, 0x00, 0x00,
        0x44, 0x53, 0x00, 0x01, 0x1a, 0x7d, 0xda, 0x54, 0x98, 0xc4, 0x9a, 0x00, 0x01, 0x06, 0x18, 0x42,
        0x10, 0x13, 0x10, 0x0a, 0x0d, 0x21, 0x02, 0x0b, 0x83, 0x50, 0x4c, 0x18, 0x14, 0xa0, 0x45, 0x41,
        0xc8, 0x05, 0x14, 0x95, 0x02, 0x21, 0xc3, 0x02, 0x14, 0x0b, 0x70, 0x2e, 0x40, 0xba, 0x00, 0xe5,
        0x28, 0x72, 0x0c, 0x22, 0x02, 0xf7, 0xef, 0x0f, 0x31, 0x0e, 0x88, 0x14, 0x40, 0x48, 0x26, 0x84,
        0x44, 0x00, 0x53, 0x21, 0x70, 0x84, 0xa0, 0x5f, 0x01, 0x08, 0x1d, 0xa2, 0xc9, 0xa0, 0x00, 0xa7,
        0x08, 0x82, 0xb4, 0x65, 0x01, 0xba, 0x05, 0xf8, 0x16, 0xa0, 0x1d, 0x42, 0x68, 0x15, 0x0a, 0x30,
        0x29, 0xc0, 0x27, 0x98, 0x2c, 0x0a, 0x90, 0x0d, 0x26, 0xdb, 0x70, 0x64, 0x18, 0x4c, 0xe4, 0x18,
        0x50, 0x62, 0xc6, 0x80, 0xd2, 0x39, 0x05, 0xd9, 0x04, 0x16, 0x74, 0xa1, 0x28, 0x9a, 0x46, 0x94,
        0x04, 0x07, 0x75, 0x0c, 0x11, 0x82, 0x97, 0x2b, 0x40, 0xf2, 0x04, 0xa4, 0x79, 0x5e, 0xb2, 0x3e,
        0x08, 0x0d, 0x81, 0x8d, 0x80, 0x47, 0x93, 0x00, 0xc2, 0xe2, 0x2c, 0x53, 0x61, 0x60, 0x50, 0x1e,
        0x40, 0x24, 0x67, 0xa8, 0x28, 0x60, 0x7b, 0x9d, 0x88, 0x86, 0x75, 0x9c, 0x4c, 0x12, 0x1c, 0x6a,
        0x94, 0x96, 0x28, 0xc0, 0xfc, 0xc8, 0x34, 0x91, 0x63, 0x6b, 0x7a, 0xc4, 0x82, 0x64, 0xd2, 0x86,
        0x82, 0x1a, 0xba, 0xa7, 0x75, 0x52, 0x9e, 0x68, 0xc4, 0x83, 0x32, 0x4c, 0x02, 0x8f, 0x82, 0xa1,
        0x71, 0x82, 0xb2, 0x20, 0xe4, 0x60, 0xa0, 0x28, 0xc0, 0x93, 0xf0, 0x1c, 0x8b, 0x17, 0x20, 0x7c,
        0xc6, 0xe4, 0x28, 0x10, 0x23, 0x81, 0x8f, 0x04, 0x96, 0x3a, 0x66, 0xf4, 0x88, 0xc2, 0x05, 0x3c,
        0x9f, 0x63, 0x88, 0x1c, 0xf7, 0x50, 0x63, 0x1c, 0x45, 0xe4, 0x04, 0xef, 0x00, 0x51, 0x8c, 0x56,
        0xd0, 0xb0, 0x35, 0xe1, 0x78, 0x11, 0x1e, 0x02, 0x18, 0xd0, 0xb9, 0x47, 0x09, 0xf2, 0xff, 0x8f,
        0x6a, 0xa0, 0x9e, 0x87, 0x40, 0x06, 0x7e, 0x14, 0x18, 0x19, 0x10, 0xf2, 0x28, 0xc0, 0xea, 0xc0,
        0x34, 0x8d, 0x04, 0x76, 0x3f, 0x00, 0x69, 0x9b, 0x83, 0x70, 0x7a, 0x13, 0x60, 0x0e, 0x46, 0x9f,
        0x87, 0xb6, 0xa6, 0xa5, 0x99, 0x06, 0x6a, 0x0c, 0x45, 0x20, 0x61, 0x6a, 0x84, 0x22, 0xe2, 0xf0,
        0x04, 0x11, 0x26, 0xca, 0xcb, 0x43, 0x8c, 0x78, 0x71, 0xc2, 0x05, 0x0b, 0x1d, 0x21, 0x4c, 0x90,
        0x30, 0x41, 0xda, 0x1f, 0x04, 0x19, 0x38, 0xee, 0x04, 0xe0, 0x13, 0xc3, 0x99, 0x9d, 0xdf, 0x13,
        0xc0, 0xd3, 0x82, 0x09, 0x3c, 0xed, 0x03, 0x7b, 0x3e, 0x38, 0x82, 0xe3, 0x8d, 0x1a, 0xe3, 0x0c,
        0x12, 0x38, 0xfe, 0x23, 0x42, 0x1a, 0x40, 0x14, 0x09, 0x1e, 0x75, 0x62, 0xf0, 0xd9, 0xc0, 0x43,
        0x3b, 0x70, 0xcf, 0xf1, 0x04, 0x82, 0x1c, 0xc2, 0x11, 0x3c, 0x35, 0x3c, 0x14, 0x78, 0x0c, 0xec,
        0xb6, 0xe0, 0x63, 0x80, 0x4f, 0x09, 0x78, 0xd7, 0x80, 0xba, 0x1e, 0x3c, 0x1c, 0xb0, 0x41, 0x87,
        0xc3, 0x8c, 0xd7, 0x43, 0xf7, 0x04, 0x9e, 0x0d, 0x1e, 0x32, 0xf8, 0x81, 0xc3, 0x83, 0xc3, 0x4d,
        0xf8, 0x64, 0x8e, 0xec, 0x8d, 0xa0, 0xd9, 0x03, 0x82, 0x0e, 0x13, 0x3e, 0x71, 0xb0, 0x33, 0x00,
        0x46, 0xfe, 0x20, 0x50, 0x23, 0x33, 0xb4, 0xcf, 0x15, 0x2f, 0x1e, 0x86, 0x7c, 0x56, 0x38, 0x2c,
        0x26, 0xf6, 0xe4, 0x41, 0xc7, 0x03, 0xfe, 0x0b, 0xc9, 0xb3, 0x86, 0xa7, 0xef, 0xf9, 0xfa, 0x80,
        0xc2, 0x06, 0x8e, 0x1b, 0x88, 0xef, 0x0b, 0xd8, 0xf3, 0x02, 0xe6, 0xff, 0x3f, 0x38, 0x1e, 0x6d,
        0xe4, 0xf4, 0xa0, 0xe2, 0x13, 0x05, 0x3f, 0x5e, 0xf8, 0x44, 0xc1, 0xce, 0x07, 0xa7, 0xf1, 0x1c,
        0xe0, 0xe1, 0x9c, 0x95, 0x0f, 0x14, 0x60, 0xbb, 0xa3, 0xb0, 0x03, 0x05, 0x38, 0x4e, 0x02, 0xe0,
        0x3b, 0xc7, 0x80, 0xf3, 0xce, 0xc1, 0xc6, 0x03, 0xe3, 0x1c, 0x03, 0x3c, 0x4e, 0x09, 0x1e, 0x02,
        0x3f, 0x4c, 0x78, 0x08, 0x7c, 0x00, 0xcf, 0x20, 0x67, 0x68, 0xa5, 0xf3, 0x42, 0x0e, 0x0c, 0xde,
        0x79, 0x07, 0xc6, 0x28, 0x78, 0x9e, 0x07, 0x87, 0x09, 0x14, 0xe4, 0x45, 0xa0, 0x50, 0x4f, 0x03,
        0x0a, 0xe3, 0xd3, 0x0d, 0x78, 0xff, 0xff, 0xa7, 0x1b, 0xe0, 0x7a, 0x38, 0xc0, 0x9d, 0x1e, 0xe0,
        0xde, 0x0b, 0xd8, 0xc5, 0xe1, 0xf9, 0x06, 0x73, 0x38, 0x80, 0x21, 0xfa, 0x6c, 0x03, 0xf5, 0x24,
        0xf3, 0x54, 0xf3, 0x22, 0x10, 0xe2, 0x20, 0x63, 0x44, 0x0a, 0xf1, 0x4c, 0x73, 0x0a, 0x95, 0xe1,
        0xe8, 0x80, 0xf3, 0x6a, 0x63, 0x38, 0x03, 0xbd, 0xda, 0xb0, 0xa1, 0x3d, 0xdd, 0x18, 0xec, 0xa9,
        0x20, 0x52, 0x34, 0xe3, 0x3e, 0x51, 0xbc, 0xe9, 0x18, 0x25, 0x44, 0x67, 0x47, 0x1b, 0x10, 0x8b,
        0x79, 0xa5, 0xc8, 0xc2, 0xd1, 0x06, 0xd0, 0xf4, 0xff, 0x3f, 0xda, 0x00, 0x77, 0xac, 0x07, 0x14,
        0xf0, 0x1d, 0x19, 0xd8, 0x0d, 0x05, 0xde, 0x09, 0x05, 0xf0, 0x93, 0xf8, 0x05, 0xa0, 0x23, 0x87,
        0xd3, 0x82, 0xc8, 0xc6, 0x1b, 0xc0, 0xa7, 0x00, 0xaa, 0x06, 0x48, 0xd3, 0x84, 0x4d, 0x30, 0x3d,
        0xb9, 0xe0, 0x7d, 0x24, 0x70, 0x6e, 0x12, 0x25, 0x1f, 0x16, 0x85, 0x73, 0xd6, 0x83, 0x08, 0x05,
        0x31, 0xa0, 0x83, 0x1c, 0x27, 0xd0, 0x67, 0x14, 0x1f, 0x44, 0x4e, 0xf4, 0xc9, 0xd0, 0x83, 0xf2,
        0x30, 0xde, 0x51, 0xd8, 0x09, 0xc4, 0xff, 0xff, 0xc3, 0x84, 0xc7, 0xee, 0x63, 0x02, 0xff, 0xc7,
        0x78, 0x36, 0x46, 0xb7, 0x9a, 0x73, 0x0a, 0x0a, 0xcc, 0x07, 0x11, 0x4e, 0x50, 0xd7, 0x4d, 0x02,
        0x64, 0xfa, 0x8e, 0x0a, 0xa0, 0x00, 0xf2, 0xbd, 0xc0, 0xe7, 0x80, 0x67, 0x03, 0x36, 0x86, 0x47,
        0x01, 0xa3, 0x19, 0x9d, 0x87, 0x9f, 0x2c, 0x2a, 0xee, 0x64, 0x29, 0x88, 0x27, 0xeb, 0x28, 0x93,
        0x45, 0xcf, 0xc4, 0xf7, 0x0a, 0xcf, 0xe8, 0x69, 0xca, 0x73, 0xf4, 0x84, 0x7d, 0x0f, 0x81, 0x75,
        0x18, 0x08, 0xf9, 0x60, 0xe0, 0x69, 0x18, 0xce, 0x93, 0xe5, 0x70, 0x9e, 0x2c, 0x1f, 0x8b, 0xaf,
        0x21, 0xe0, 0x13, 0x38, 0x5b, 0x90, 0xc3, 0x63, 0x62, 0x8d, 0x8a, 0x5e, 0x85, 0x3c, 0x3c, 0x7e,
        0x39, 0xf0, 0x7c, 0x9e, 0x11, 0x8e, 0xf2, 0x49, 0x02, 0x87, 0xf1, 0x4c, 0xe2, 0x21, 0xfa, 0x56,
        0x04, 0x6b, 0x22, 0x27, 0xed, 0xdb, 0xc2, 0xc9, 0x06, 0x7d, 0x45, 0xe3, 0xff, 0xff, 0x5b, 0x11,
        0x38, 0x2f, 0x69, 0xfc, 0x9c, 0xe4, 0x9b, 0x05, 0x36, 0xd3, 0xbd, 0x0a, 0x15, 0xeb, 0x5e, 0x05,
        0x88, 0x1d, 0xd6, 0x6b, 0x8f, 0x0f, 0x56, 0x70, 0xef, 0x55, 0x70, 0x8f, 0x51, 0xa7, 0xf8, 0x4e,
        0x15, 0xe1, 0x91, 0xe1, 0x9d, 0xd3, 0x50, 0xef, 0x55, 0x86, 0x7a, 0x9b, 0x7a, 0x90, 0x78, 0xaf,
        0x32, 0x48, 0xb4, 0x97, 0x50, 0xdf, 0xab, 0x7c, 0xa4, 0x32, 0x9e, 0x51, 0xc3, 0xc4, 0x7a, 0x10,
        0x35, 0x64, 0x98, 0xb0, 0xb1, 0xc3, 0xbc, 0x4c, 0xf8, 0x5e, 0x05, 0x56, 0x39, 0xf7, 0x2a, 0xe4,
        0xff, 0xff, 0x5e, 0x05, 0x30, 0xe3, 0xa2, 0x89, 0xbb, 0x57, 0x81, 0xe7, 0xda, 0xc0, 0x6e, 0x48,
        0xb0, 0x80, 0x5e, 0xac, 0x00, 0x27, 0xff, 0xff, 0x8b, 0x15, 0x36, 0xf3, 0xc5, 0x8a, 0xe6, 0xbd,
        0x58, 0xa1, 0xce, 0x25, 0xd6, 0x73, 0xb1, 0x22, 0xf3, 0x84, 0x7f, 0xb3, 0x02, 0x38, 0xfb, 0xff,
        0xbf, 0x59, 0x01, 0x46, 0xef, 0x45, 0x98, 0xa3, 0x15, 0xbc, 0x94, 0x37, 0x2b, 0x1a, 0x67, 0x19,
        0x0a, 0xb8, 0x20, 0x0a, 0xe3, 0x9b, 0x15, 0xe0, 0xe8, 0xba, 0x82, 0x1b, 0x32, 0x5c, 0xb0, 0x28,
        0xc1, 0x7c, 0x08, 0xf1, 0x74, 0xde, 0xc7, 0x7d, 0x09, 0x78, 0xcc, 0xf1, 0x80, 0x1f, 0x08, 0x7c,
        0xb9, 0xc2, 0xfc, 0xff, 0x2f, 0x57, 0xe0, 0x38, 0x3e, 0xbd, 0x4c, 0x19, 0xe2, 0x09, 0xd8, 0x30,
        0x8f, 0x56, 0xbe, 0x57, 0x05, 0x7a, 0xae, 0x7a, 0xfe, 0xf5, 0x49, 0xd8, 0x53, 0x0b, 0xf4, 0x68,
        0xe5, 0xf3, 0x95, 0x81, 0xa2, 0xbc, 0x03, 0x1b, 0x2e, 0x42, 0xfc, 0x67, 0xab, 0xb8, 0xa1, 0x0d,
        0xf8, 0x2a, 0xec, 0xcb, 0x15, 0x78, 0x25, 0x5d, 0xae, 0x00, 0x9a, 0x0c, 0xff, 0xe5, 0x0a, 0x7c,
        0x17, 0x83, 0xff, 0xff, 0x9b, 0x03, 0xee, 0x9a, 0x04, 0xf7, 0x7a, 0x05, 0xf8, 0xc9, 0x7e, 0xbd,
        0xa2, 0xb9, 0xaf, 0x57, 0x28, 0x51, 0x10, 0x52, 0x04, 0xa4, 0x99, 0x82, 0xe7, 0x7e, 0x05, 0x38,
        0xff, 0xff, 0xdf, 0xaf, 0x00, 0x16, 0x84, 0xbb, 0x5f, 0x01, 0xbd, 0xdb, 0x11, 0x78, 0x51, 0xde,
        0x8e, 0xb0, 0x57, 0x2c, 0x7c, 0xc6, 0x2b, 0x16, 0x0d, 0xb3, 0x12, 0xc5, 0x5b, 0x13, 0x85, 0xf1,
        0x15, 0x0b, 0x4c, 0xff, 0xff, 0x2b, 0x16, 0x30, 0x3f, 0x88, 0xe2, 0x46, 0x0d, 0xf7, 0xe2, 0xe4,
        0x5b, 0x8f, 0xe7, 0x1b, 0xd1, 0x77, 0x18, 0xcc, 0x09, 0x0b, 0xc6, 0x0d, 0x0b, 0xf6, 0xe9, 0xca,
        0x53, 0x7a, 0x89, 0x3a, 0xcc, 0x17, 0x60, 0x9f, 0xaf, 0x7c, 0xb7, 0x7a, 0x69, 0x08, 0x12, 0xeb,
        0x0d, 0xcb, 0x77, 0x61, 0x03, 0xbd, 0x61, 0xf9, 0x98, 0x15, 0xd5, 0x28, 0x91, 0x1e, 0xb2, 0x22,
        0xf8, 0x0c, 0x11, 0x29, 0x68, 0xc4, 0x33, 0x78, 0xd9, 0x32, 0xdc, 0x1b, 0x16, 0x60, 0xf3, 0xff,
        0x7f, 0xc3, 0x02, 0x1c, 0x5e, 0x1c, 0xf8, 0xc1, 0x01, 0x7b, 0xc3, 0x02, 0x7c, 0xce, 0x14, 0x3c,
        0x37, 0x2c, 0xb0, 0xfd, 0xff, 0x6f, 0x58, 0x00, 0xff, 0xff, 0xff, 0x37, 0x2c, 0xe0, 0x78, 0xbb,
        0xc2, 0xde, 0xb2, 0xb0, 0xb7, 0x17, 0xb2, 0x0a, 0x1a, 0x6a, 0x25, 0x0a, 0xfe, 0xbd, 0x32, 0x0c,
        0xbd, 0x61, 0x01, 0x8e, 0xc6, 0xed, 0x51, 0x83, 0xe3, 0x6c, 0xf0, 0x24, 0x81, 0x39, 0x57, 0xe3,
        0x66, 0x73, 0x70, 0x81, 0x23, 0xf4, 0x0e, 0xaa, 0xb9, 0xb3, 0x4b, 0x0a, 0x8c, 0x5b, 0x16, 0xdc,
        0x63, 0x95, 0x0f, 0x13, 0x21, 0xde, 0x1a, 0x5e, 0xab, 0x22, 0xbc, 0x4a, 0xbd, 0x65, 0xf9, 0x4a,
        0x6c, 0xa4, 0xb7, 0x2c, 0x5f, 0xb4, 0x7c, 0xcb, 0x32, 0x58, 0x44, 0x5f, 0xb5, 0xd8, 0xb5, 0xd8,
        0x80, 0xaf, 0x5c, 0x3e, 0x67, 0xf9, 0xff, 0x7f, 0xe5, 0x7a, 0xe6, 0xf2, 0xa5, 0xeb, 0x2d, 0x0b,
        0xbc, 0x22, 0xff, 0x2a, 0x3a, 0x11, 0xf8, 0x96, 0x05, 0xf0, 0x23, 0x18, 0x84, 0x9c, 0x0c, 0x1c,
        0x7d, 0x5e, 0xc0, 0x0f, 0xe0, 0x61, 0xc4, 0x43, 0x62, 0x01, 0x06, 0x4e, 0xef, 0x4b, 0x3e, 0x53,
        0xf0, 0x03, 0x86, 0xcf, 0x14, 0xec, 0x8e, 0x10, 0xfd, 0x31, 0xc0, 0xe7, 0x12, 0xcc, 0xe0, 0xc0,
        0xfb, 0xff, 0x1f, 0x1c, 0xbf, 0x56, 0x1c, 0xba, 0x47, 0xf1, 0xca, 0xf0, 0xf0, 0x90, 0xc0, 0x83,
        0x03, 0xc7, 0x9d, 0xe1, 0x88, 0x7c, 0x87, 0xf1, 0xe0, 0xc0, 0x76, 0x10, 0x00, 0x0e, 0x83, 0xc3,
        0x87, 0x1a, 0x1c, 0x7a, 0x3c, 0x06, 0x0e, 0xe9, 0xf3, 0x18, 0xbb, 0x30, 0x3c, 0xe4, 0x78, 0x68,
        0x60, 0x03, 0xf4, 0xd0, 0x80, 0xe5, 0x24, 0x9e, 0x42, 0x0e, 0x0d, 0xcc, 0x12, 0x86, 0x06, 0xea,
        0xff, 0xff, 0xd0, 0xe0, 0xc5, 0x1b, 0x1a, 0x7a, 0x48, 0x3e, 0x2c, 0xc4, 0x7c, 0xca, 0x89, 0xf0,
        0xbe, 0xe0, 0x13, 0x14, 0x43, 0x7f, 0x4d, 0xf1, 0xed, 0xc2, 0x37, 0x41, 0xdc, 0xf8, 0xc0, 0x75,
        0xe9, 0xf4, 0xf8, 0x80, 0xcf, 0x91, 0xcb, 0xe3, 0xc3, 0x1d, 0x9d, 0x3c, 0x3e, 0x18, 0x2e, 0xaf,
        0x32, 0xe4, 0x68, 0xe2, 0xa3, 0x15, 0x1b, 0x1e, 0xd8, 0x6e, 0x7a, 0x1e, 0x1e, 0xf0, 0xb9, 0x6e,
        0x82, 0xf1, 0x0a, 0xc9, 0xaf, 0x25, 0x30, 0x4e, 0x50, 0xf8, 0xeb, 0x26, 0xbc, 0xff, 0xff, 0x75,
        0x13, 0xf0, 0x30, 0x0a, 0x9e, 0xe8, 0x92, 0x4d, 0x83, 0xbc, 0x72, 0x14, 0xeb, 0x45, 0x40, 0x61,
        0x7c, 0x66, 0x03, 0x1c, 0x8d, 0xf5, 0x99, 0x0d, 0x2c, 0xf7, 0x02, 0x76, 0x63, 0x83, 0x7b, 0x5a,
        0x83, 0x7d, 0x8c, 0x78, 0x35, 0x30, 0x84, 0x81, 0x5e, 0xac, 0x7d, 0x50, 0xf3, 0x09, 0x22, 0xcc,
        0x53, 0xb6, 0x47, 0x7a, 0x16, 0xc1, 0x42, 0xf9, 0xd2, 0xe6, 0x73, 0x5a, 0x8c, 0x28, 0xc1, 0xde,
        0xd7, 0x0c, 0x6c, 0x94, 0x27, 0x6c, 0xc3, 0x3e, 0x2a, 0x18, 0x22, 0x81, 0xff, 0xff, 0xa7, 0x35,
        0x70, 0xca, 0xb9, 0x62, 0x03, 0x34, 0xb9, 0xe8, 0xe3, 0x0e, 0x29, 0x60, 0xb9, 0x36, 0xb0, 0x5b,
        0x0a, 0xbc, 0x0b, 0x36, 0x70, 0xfa, 0xff, 0x1f, 0x51, 0x80, 0x4f, 0xe2, 0x0b, 0x36, 0x4d, 0x7b,
        0xc1, 0x46, 0x71, 0x58, 0x0d, 0x90, 0xe6, 0x09, 0xff, 0x7a, 0x0d, 0xfe, 0x49, 0xf8, 0x7a, 0x0d,
        0xd8, 0xbe, 0xc5, 0xe2, 0xae, 0xd7, 0x60, 0xfd, 0xff, 0x5f, 0xaf, 0x01, 0x0b, 0xb1, 0xae, 0xd7,
        0x40, 0xef, 0x5a, 0x04, 0xe6, 0x3b, 0xbf, 0x6f, 0x16, 0x58, 0xc1, 0x17, 0x2b, 0x1a, 0x65, 0x19,
        0x8a, 0xb6, 0x20, 0x0a, 0xe3, 0x8b, 0x15, 0xe0, 0x08, 0xfb, 0x90, 0xf1, 0xff, 0xff, 0x21, 0xc3,
        0xba, 0x4b, 0x1d, 0x32, 0x7e, 0x06, 0x3e, 0x12, 0xf8, 0xfa, 0xc3, 0x30, 0x7d, 0xad, 0x86, 0x7f,
        0x95, 0x7a, 0x87, 0x78, 0x96, 0x32, 0xc2, 0xe3, 0xf4, 0x39, 0x3d, 0x57, 0x19, 0xe3, 0x20, 0x9e,
        0xaa, 0xde, 0xac, 0xd8, 0xb5, 0xea, 0x69, 0xef, 0xa1, 0xda, 0xb7, 0x6a, 0x1f, 0xaa, 0x7d, 0xc2,
        0xf2, 0xed, 0x21, 0x44, 0xb4, 0x48, 0x21, 0x82, 0x84, 0x8a, 0xff, 0x5e, 0xed, 0x9b, 0x15, 0x98,
        0x05, 0xdd, 0xac, 0x00, 0x92, 0xfc, 0xff, 0xaf, 0x4c, 0xd8, 0x9b, 0x15, 0x58, 0xee, 0x0d, 0xec,
        0x88, 0xc4, 0x4f, 0x1f, 0xbe, 0x36, 0xc0, 0xb8, 0x5a, 0x01, 0x86, 0xc2, 0x5c, 0xad, 0x68, 0x90,
        0xab, 0x15, 0xea, 0x64, 0xe2, 0x7b, 0x9e, 0x27, 0x0a, 0x8e, 0xf0, 0x77, 0x2b, 0x54, 0x6c, 0x12,
        0x85, 0x3e, 0x1f, 0xa1, 0x62, 0x1e, 0x46, 0x28, 0x88, 0x01, 0x9d, 0xe1, 0x44, 0x81, 0x56, 0x71,
        0xa2, 0x20, 0x77, 0x1c, 0x4f, 0xea, 0xc9, 0x0a, 0xd8, 0xff, 0xff, 0x4f, 0x56, 0x60, 0x5c, 0xa6,
        0x87, 0xca, 0x23, 0x0f, 0x95, 0x82, 0x78, 0xa8, 0x8e, 0x33, 0x54, 0xf4, 0x24, 0x7d, 0x70, 0x80,
        0x77, 0xb6, 0x02, 0xbc, 0x09, 0x1e, 0x2b, 0x3d, 0x50, 0x81, 0xeb, 0x98, 0x85, 0x07, 0x7d, 0xcc,
        0x82, 0xa9, 0xd0, 0xa6, 0x4f, 0x8d, 0x46, 0xad, 0x1a, 0x94, 0xa9, 0x51, 0xa6, 0x41, 0xad, 0x3e,
        0x95, 0x1a, 0x33, 0x76, 0xe2, 0x79, 0xe0, 0xd1, 0x59, 0x82, 0x8a, 0x5a, 0x8c, 0x46, 0xe3, 0x48,
        0x20, 0x34, 0xc6, 0x77, 0x5d, 0x20, 0x0e, 0xfd, 0xd4, 0x11, 0xa0, 0x83, 0xbe, 0xa8, 0x04, 0x64,
        0x99, 0x6b, 0xd3, 0x39, 0x80, 0x3c, 0x25, 0x64, 0x04, 0x44, 0xda, 0x40, 0x04, 0x64, 0x1d, 0x2b,
        0x12, 0x90, 0x55, 0x81, 0x08, 0xc8, 0x19, 0x64, 0xfc, 0xff, 0xc1, 0x99, 0x6e, 0x1d, 0x40, 0x2c,
        0x35, 0x08, 0x4d, 0xb8, 0x70, 0x81, 0x3a, 0x2e, 0x88, 0x80, 0x1c, 0x8d, 0x46, 0x40, 0x0e, 0x4a,
        0xa1, 0xa3, 0x19, 0xf9, 0x87, 0x0b, 0xdc, 0x82, 0xac, 0x00, 0xb1, 0x38, 0x20, 0x34, 0x80, 0x17,
        0x50, 0x96, 0x06, 0x44, 0x40, 0x56, 0xbe, 0x56, 0x01, 0x39, 0x07, 0x88, 0x80, 0x9c, 0xdd, 0x0c,
        0x38, 0xcb, 0xec, 0x06, 0x88, 0x29, 0x7e, 0x28, 0x08, 0xc4, 0x1a, 0xf5, 0x80, 0x32, 0xc1, 0x20,
        0x02, 0xb2, 0x38, 0x3f, 0x40, 0x4c, 0x2a, 0x88, 0x80, 0x1c, 0xef, 0x60, 0x22, 0x70, 0xc7, 0x53,
        0x04, 0x48, 0x1d, 0x0d, 0x97, 0x82, 0xd0, 0x30, 0xcf, 0x13, 0x81, 0x58, 0xa0, 0x25, 0x30, 0x0a,
        0x42, 0x23, 0x69, 0x02, 0x61, 0x72, 0x3d, 0x81, 0xb0, 0x90, 0xdf, 0x74, 0x0d, 0xc1, 0xc1, 0x54,
        0x0d, 0x91, 0xba, 0x3a, 0x21, 0xd0, 0xff, 0xd4, 0xc1, 0x10, 0x35, 0x20, 0x02, 0x72, 0x72, 0x20,
        0xaa, 0xe3, 0x21, 0x22, 0x20, 0xab, 0xf6, 0x05, 0xce, 0xb4, 0xbe, 0xb2, 0x02, 0x91, 0xa4, 0x20,
        0x34, 0xd2, 0x3b, 0x2b, 0x28, 0xc9, 0x07, 0x22, 0x20, 0xe7, 0x79, 0x5a, 0x09, 0xc8, 0xe9, 0x40,
        0x04, 0xe4, 0xff, 0x3f
        })
    }
  }
}
