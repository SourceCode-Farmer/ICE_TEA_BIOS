
 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2014, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _LFC_VARIABLE_GUID_H_
#define _LFC_VARIABLE_GUID_H_

#define LFC_VARIABLE_GUID  \
  {0x6acce65d, 0xda35, 0x4b39, 0xb6, 0x4b, 0x5e, 0xd9, 0x27, 0xa7, 0xdc, 0x7e}

extern EFI_GUID  gLfcVariableGuid;

#endif //_LFC_VARIABLE_GUID_H_
