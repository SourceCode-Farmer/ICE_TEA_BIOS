
 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2021, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _LFC_Build_As_Secured_Core_Pc_GUID_H_
#define _LFC_Build_As_Secured_Core_Pc_GUID_H_

#define LFC_Build_As_Secured_Core_Pc_GUID  \
  {0x77fa9abd, 0x0359, 0x4d32, 0xbd, 0x60, 0x28, 0xf4, 0xe7, 0x8f, 0x78, 0x4b}
extern EFI_GUID  gBuiltAsSecuredCorePcGuid;

#endif //_LFC_Build_As_Secured_Core_Pc_GUID_H_
