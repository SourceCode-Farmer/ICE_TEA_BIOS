
 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2014, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _LFC_OEM_SECURE_BOOT_ENABLE_VARIABLE_GUID_H_
#define _LFC_OEM_SECURE_BOOT_ENABLE_VARIABLE_GUID_H_

#define LFC_OEM_SECURE_BOOT_ENABLE_VARIABLE_GUID  \
  {0x9f97e427, 0x83ec, 0x430e, 0xaf, 0x77, 0x9a, 0x90, 0x57, 0x10, 0x44, 0x85}

#define LFC_OEM_SECURE_ONE_TIME_VARIABLE_GUID  \
  {0xb8e91d00, 0x6d3c, 0x4730, 0x87, 0x92, 0x93, 0x6d, 0xdb, 0xa7, 0x53, 0x2e}
  
extern EFI_GUID  gOemSecureBootEnableVariableGuid;
extern EFI_GUID  gOemSecureOneTimeVariableGuid;

#endif //_LFC_OEM_SECURE_BOOT_ENABLE_VARIABLE_GUID_H_
  
  
  