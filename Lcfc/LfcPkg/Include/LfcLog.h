 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015                , Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************


#ifndef _LFC_LOG_H_
#define _LFC_LOG_H_

#define LFC_LOG_WAKE_SOURCE_CAN_NOT_GET_PPI         0x10

#define LFC_LOG_ACPI_OVERRIDE_CAN_NOT_GET_PROTOCOL  0x40
#define LFC_LOG_DEBUG_PAGE_CAN_NOT_GET_PROTOCOL     0x41
#define LFC_LOG_LVAR_DXE_CAN_NOT_GET_PROTOCOL       0x42
#define LFC_LOG_LVAR_SMM_CAN_NOT_GET_PROTOCOL       0x43
#define LFC_LOG_LVAR_DXE_ROM_ERROR                  0x44
#define LFC_LOG_LVAR_SMM_ROM_ERROR                  0x45
#define LFC_LOG_LVAR_GET_PLATFROM_ERROR             0x46
#define LFC_LOG_CLEAR_EC_BUFFER_6064_IBE_ERROR      0x47
#define LFC_LOG_CLEAR_EC_BUFFER_6064_OBE_ERROR      0x48
#define LFC_LOG_CLEAR_EC_BUFFER_6266_IBE_ERROR      0x49
#define LFC_LOG_CLEAR_EC_BUFFER_6266_OBE_ERROR      0x4a
#define LFC_LOG_CLEAR_EC_BUFFER_686C_IBE_ERROR      0x4b
#define LFC_LOG_CLEAR_EC_BUFFER_686C_OBE_ERROR      0x4c

#endif
