 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015                , Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************


#ifndef _LFC_CMOS_H_
#define _LFC_CMOS_H_

// Use extend CMOS
#define LFC_CMOS_INDEX                             0x72
#define LFC_CMOS_DATA                              0x73

#define LFC_DEBUG_ERROR_COUNT_INDEX                0x60
#define LFC_DEBUG_ERROR1_INDEX                     0x61
#define LFC_DEBUG_ERROR2_INDEX                     0x62
#define LFC_DEBUG_ERROR3_INDEX                     0x63

#define LFC_FAST_BOOT_PREVIOUS_BOOT_SUCCESS_INDEX  0x70
  #define FAST_BOOT_ONGOING          0x55
  #define FAST_BOOT_BOOT_SUCCESS     0xaa
  #define FAST_BOOT_BOOT_FAIL        0x00

#define LFC_SHOW_DEBUG_PAGE_INDEX1                 0x71
  #define LFC_SHOW_DEBUG_PAGE_DATA1  0x71
#define LFC_SHOW_DEBUG_PAGE_INDEX2                 0x72
  #define LFC_SHOW_DEBUG_PAGE_DATA2  0x72
#define LFC_SHOW_DEBUG_PAGE_INDEX3                 0x73
  #define LFC_SHOW_DEBUG_PAGE_DATA3  0x73

#define LFC_WAKE_SRC_INDEX                         0x74    // Save wake source to CMOS function
#define LFC_LAST_WAKE_SRC_INDEX                    0x75    // Save Last wake source to CMOS function

#define LFC_FLASH_EC_ONCE_H_INDEX                  0x76
#define LFC_FLASH_EC_ONCE_L_INDEX                  0x77

#define LFC_CMOS_CLEARED_INDEX                     0x78
  #define LFC_CMOS_CLEARED_ASSERTED  0x5A
 //[-start-211117-Dongxu0029-add]// 
#define LFC_WAKE_S4_INDEX                          0x52 
 //[-end-211117-Dongxu0029-add]//
//[-start-220323-QINGLIN0167-add]//
#if defined(S370_SUPPORT) 
#define LFC_MEMORY_TYPE_INDEX                      0x53 
#endif
//[-end-220323-QINGLIN0167-add]//
#endif
