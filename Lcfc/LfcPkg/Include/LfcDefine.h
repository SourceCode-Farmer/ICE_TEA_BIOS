 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015                , Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //2020.05.06    Ru Xu    V5.0      DYTC (Dynamic Thermal Control) Method Rev5 for Intelligent Cooling
 //******************************************************************************


#ifndef _LFC_DEFINE_H_
#define _LFC_DEFINE_H_

#include "Project/Include/LfcOemDefine.h"

//ITS Version
#define ITS_VERSION_3_0                          3   //ITS Version 3.0
#define ITS_VERSION_4_0                          4   //ITS Version 4.0
#define ITS_VERSION_5_0                          5   //ITS Version 5.0

//DYTC IC Funciton Bit Map
#define DYTC_ICFUNC_STD                          0   //Standrad                    
#define DYTC_ICFUNC_FBC                          2   //Fan Boost Control 
#define DYTC_ICFUNC_MYH                          3   //Multi Yoga Hinge 
#define DYTC_ICFUNC_STP                          4   //Skin Temperature Protection 
#define DYTC_ICFUNC_APM                          5   //Auto Performance Mode
#define DYTC_ICFUNC_AQM                          6   //Auto Quiet Mode
#define DYTC_ICFUNC_IEPM                         7   //Intelligent Extreme Performance Mode
#define DYTC_ICFUNC_IBSM                         8   //Intelligent Battery Saving Mode
#define DYTC_ICFUNC_AAA                          10  //Ambient Acoustic Adaptive 
#define DYTC_ICFUNC_MMC                          11  //Manual Mode Control 

//IC Mode Bit Map for MYH
#define DYTC_ICMODE_TBL                          0   //Tablet
#define DYTC_ICMODE_TNT                          1   //Tent
#define DYTC_ICMODE_LFT                          2   //Lay Flat 

//IC Mode Bit Map for MMC
#define DYTC_ICMODE_EPM                          2   //Extreme Performance Mode 
#define DYTC_ICMODE_BSM                          3   //Battery Saving Mode
#define DYTC_ICMODE_INT                          0xF //Intelligent Mode

//DYTC IC No Mode
#define DYTC_ICMODE_NOMODE                       0xF //No Mode(when Current Function is not MYH,MMC,RSO) 

//System Performance Mode EC RAM Register
//#define SYSTEM_PERFORMANCE_MODE_EC_RAM_REG       0x1D
#define SYSTEM_PERFORMANCE_MODE_EC_RAM_REG       0x20


#define INTELLIGENT_MODE                         0x0
#define EXTREME_PERFORMANCE_MODE                 0x1
#define BATTERY_SAVING_MODE                      0x2
//[-start-210901-FLINT00016-add]//
#define INTELLIGENT_APM_MODE                     0x3
#define INTELLIGENT_AQM_MODE                     0x4
//[-end-210901-FLINT00016-add]//
//[-start-210909-FLINT00019-add]//
//[-start-210919-QINGLIN0072-modify]//
//[-start-211008-TAMT000021-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211008-TAMT000021-modify]//
//[-end-210919-QINGLIN0072-modify]//
#define INTELLIGENT_EPM_MODE                     0x6
#define INTELLIGENT_BSM_MODE                     0x7
#endif
//[-end-210909-FLINT00019-add]//

#endif

