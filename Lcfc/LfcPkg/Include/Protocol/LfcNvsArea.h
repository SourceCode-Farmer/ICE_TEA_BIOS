 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2013, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************


#ifndef _LFC_NVS_AREA_H_
#define _LFC_NVS_AREA_H_

#define LFC_NVS_AREA_PROTOCOL_GUID \
  { 0x90b10178, 0xf944, 0x469b, 0xbe, 0xd3, 0xe2, 0x73, 0x6b, 0x17, 0xdb, 0x76 };

extern EFI_GUID gLfcNvsAreaProtocolGuid;

#pragma pack (1)
typedef struct {
  ///////////////////////////////////////////////////////////////////////////////
  // Common part, project should not modify it
  // !!! Please sync this layout with LFCN in LfcNvsArea.asl !!!
  UINT8       Ps2TpVendor;
  UINT8       KeyboardId;
  UINT8       MachineSize;
  UINT8       NovoBtStatus;
  UINT8       EdidBuffer[128];
  UINT8       TouchPadType;
  UINT16      TouchPadPID;
  UINT8       TouchPnLType;
  UINT16      TouchPnLPID;
  UINT8       UMABoard;
  UINT8       PerformanceMode;
  UINT32      DytcCommond;               // DYTC Command for debug,LCFCTODO: you need check your project support ITS version and follow ITS to change 
  UINT16      DytcIcFuncCapability;      // Function Capability Status  
  UINT16      DytcIcFuncValidFlag ;      // Function Valid Flag  
  UINT8       DytcMYHCapability;         // MYH Capability Status,Bit0=Tablet supported,Bit1=Tent supported,Bit2=Lay Flat supported
  UINT8       DytcMMCCapability;         // MMC Capability Status,Bit2=Extreme Performance supported, Bit3=Battery Saving supported
  UINT8       DytcSMYH;                  // MYH Current Mode,0=Tablet,1=Tent,2=LayFlat,Fh when clear MYH Valid Flag
  UINT8       DytcSMMC;                  // MMC Current Mode,2=Extreme Performance, 3=Battery Saving,Fh when clear MMC Valid Flag
  UINT8       CurrentIcFuncMode;         // Current IC Function/Mode 
                                         // Bit3:0   - Current IC Function
                                         // Bit7:4   - Current IC Mode
  ///////////////////////////////////////////////////////////////////////////////
  // Project part, please modify it here
  #include <Project/Include/LfcOemNvsArea.h>
  ///////////////////////////////////////////////////////////////////////////////
} LFC_NVS_AREA;
#pragma pack ()

typedef struct _LFC_NVS_AREA_PROTOCOL {
  LFC_NVS_AREA     *Area;
} LFC_NVS_AREA_PROTOCOL;

#endif
