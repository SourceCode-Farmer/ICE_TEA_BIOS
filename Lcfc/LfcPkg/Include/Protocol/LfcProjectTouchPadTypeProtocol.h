 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef __LFC_PROJECT_TOUCH_PAD_TYPE_PROTOCOL_H__
#define __LFC_PROJECT_TOUCH_PAD_TYPE_PROTOCOL_H__

#define LFC_PROJECT_TOUCH_PAD_ELAN_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x12, 0x34, 0x56 } }

extern EFI_GUID gLfcProjectTouchPadElanProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_SYNA_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x65, 0x43, 0x21 } }
extern EFI_GUID gLfcProjectTouchPadSynaProtocolGuid;

//[-start-210930-YUNLEI0141-modify]//
#if defined(C770_SUPPORT) || defined(C970_SUPPORT)
#define LFC_PROJECT_TOUCH_PAD_ELAN1_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x32, 0x36, 0x54 } }

  extern EFI_GUID gLfcProjectTouchPadElan1ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_SYNA1_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x56, 0x33, 0x15 }}

extern EFI_GUID gLfcProjectTouchPadSyna1ProtocolGuid;
#endif
//[-end-210930-YUNLEI0141-modify]//
//[-start-211015-Ching000014-modify]//
#if defined(S77013_SUPPORT)
#define LFC_PROJECT_TOUCH_PAD_ELAN2_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x12, 0x35, 0x57 } }

  extern EFI_GUID gLfcProjectTouchPadElan2ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_SYNA2_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x65, 0x44, 0x22 } }

  extern EFI_GUID gLfcProjectTouchPadSyna2ProtocolGuid;
#endif
//[-start-220127-Dennis0013-add]//
//[-start-220209-OWENWU0037-add]//
//[-start-220224-Dennis0015-add]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT)
#define LFC_PROJECT_TOUCH_PAD_ELAN3_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x12, 0x36, 0x58 } }
  extern EFI_GUID gLfcProjectTouchPadElan3ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_ELAN4_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x12, 0x37, 0x59 } }
  extern EFI_GUID gLfcProjectTouchPadElan4ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_ELAN5_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x12, 0x38, 0x60 } }
  extern EFI_GUID gLfcProjectTouchPadElan5ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_ELAN6_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x12, 0x39, 0x61 } }
  extern EFI_GUID gLfcProjectTouchPadElan6ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_SYNA3_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x65, 0x45, 0x23 } }
  extern EFI_GUID gLfcProjectTouchPadSyna3ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_SYNA4_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x65, 0x46, 0x24 } }
  extern EFI_GUID gLfcProjectTouchPadSyna4ProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_CIRQUE_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x66, 0x46, 0x24 } } 
  extern EFI_GUID gLfcProjectTouchPadCirqueProtocolGuid;

#define LFC_PROJECT_TOUCH_PAD_FOCAL_PROTOCOL_GUID \
  { 0x11f8dcb6, 0x3a1e, 0x4fd5, { 0x91, 0xab, 0xbb, 0xae, 0xf8, 0x67, 0x47, 0x25 } }
  extern EFI_GUID gLfcProjectTouchPadFocalProtocolGuid;
#endif
//[-end-220224-Dennis0015-add]//
//[-end-220209-OWENWU0037-add]//
//[-end-220127-Dennis0013-add]//
//[-end-211015-Ching000014-modify]//
#endif
