 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 
/*
Data          Name          Version    Description
2013.09.26    dahai.zhou    v1.00      Initial release it for EDK2 projects
2014.05.16    dahai.zhou    v1.01      Add LVAR_OS_TYPE_LEGACY_OS_LEGACY_FI for legacy first
2014.06.26    dahai.zhou    v1.02      Add LVAR_LSE_UPLOAD_INFO_FLAG_GUID, LVAR_LSE_ON_OFF_FLAG_GUID and LVAR_OA3_KEY_ID_GUID
2014.08.12    dahai.zhou    v1.10      Support new interface between DXE and SMM modules
2014.09.15    feng.gu       v1.20      Record debug information
                                       Add two debug features,the first one is dumping debug data,and the second one is getting buffer for debug data.
2015.03.31    dahai.zhou    v1.21      Modify the 64 bit variable to meet new lvar spec
2015.06.03    dahai.zhou    v1.22      Lvar tool remove /mfgdown, add /mfgmode
2019.01.04    rita.zhang    v1.23      Add Optane LVAR guid and data structure for flash BIOS to keep optane setting.
2019.07.18    rita.zhang    v1.24      Sync Lenovo Variable (lvar) Specification v1.20 update GUID and remove v1.23 setting.
*/

#ifndef _LENOVO_VARIABLE_PROTOCOL_H_
#define _LENOVO_VARIABLE_PROTOCOL_H_

#pragma warning( disable : 4366 )
//If a structure member could ever be unaligned because of packing, the compiler will warn when that member's address is assigned to an aligned pointer.
//By default, all pointers are aligned.

#define LENOVO_VARIABLE_PROTOCOL_GUID \
   { 0xbfd02359, 0x8dfe, 0x459a, 0x8b, 0x69, 0xa7, 0x3a, 0x6b, 0xaf, 0xad, 0xc0 }

typedef struct _LENOVO_VARIABLE_PROTOCOL LENOVO_VARIABLE_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *LENOVO_GET_VARIABLE) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This,
  IN     EFI_GUID                       *VariableGuidName,
  IN OUT UINT32                         *DataSize,
  OUT    VOID                           *Data
  );

typedef
EFI_STATUS
(EFIAPI *LENOVO_DELETE_VARIABLE) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This,
  IN     EFI_GUID                       *VariableGuidName
  );

typedef
EFI_STATUS
(EFIAPI *LENOVO_SET_VARIABLE) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This,
  IN     EFI_GUID                       *VariableGuidName,
  IN     UINT32                         DataSize,
  IN     VOID                           *Data
  );

typedef
EFI_STATUS
(EFIAPI *LENOVO_LOCK_VARIABLE) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This,
  IN     EFI_GUID                       *VariableGuidName
  );

typedef
EFI_STATUS
(EFIAPI *LENOVO_UNLOCK_VARIABLE) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This,
  IN     EFI_GUID                       *VariableGuidName,
  IN     CHAR16                         *Password
  );

typedef
EFI_STATUS
(EFIAPI *LENOVO_LOCK_VARIABLE_REGION) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This
  );

typedef
EFI_STATUS
(EFIAPI *LENOVO_UNLOCK_VARIABLE_REGION) (
  IN     LENOVO_VARIABLE_PROTOCOL       *This,
  IN     CHAR16                         *Password
  );

typedef struct _LENOVO_VARIABLE_PROTOCOL {
  LENOVO_GET_VARIABLE                   GetVariable;
  LENOVO_SET_VARIABLE                   SetVariable;
  LENOVO_LOCK_VARIABLE                  LockVariable;
  LENOVO_UNLOCK_VARIABLE                UnlockVariable;
} LENOVO_VARIABLE_PROTOCOL, EFI_L05_VARIABLE_PROTOCOL;


/*
#if (FLASH_REGION_LENOVO_VARIABLE_REGION_SUBREGION_1_SIZE != FLASH_REGION_LENOVO_VARIABLE_REGION_SUBREGION_2_SIZE)
  #error "VARIABLE_SUBREGION_1 and 2 must be the same size. Please check your flash map."
#endif
#if (FLASH_REGION_LENOVO_VARIABLE_REGION_SUBREGION_1_SIZE % EFI_PAGE_SIZE != 0)
  #error "FLASH_REGION_LENOVO_VARIABLE_REGION_SUBREGION_1_SIZE must be multiple of 4K. Please check your flash map."
#endif
*/

#define LENOVO_VARIABLE_NO_DATA          0
#define LENOVO_VARIABLE_USING_REGION_1   1
#define LENOVO_VARIABLE_USING_REGION_2   2

#define LENOVO_VARIABLE_REGIONE_NOT_LOCK 0
#define LENOVO_VARIABLE_REGION_LOCKED    1

#define LENOVO_VARIABLE_UNLOCK_PASSWORD  L"UnlockPassword"
#define LENOVO_VARIABLE_UNLOCK_FAIL_MAX  5  //Define max fail count of unlocking

#define LENOVO_VARIABLE_DUMP_PASSWORD    L"DumpPassword"

//If locked, can not delete or modify this variable, just read only
#define LENOVO_VARIABLE_ATTRIBUTE_LOCKED 1  //BIT0

#define LENOVO_VARIABLE_DXE_SMM_DATA_VARIABLE_NAME    L"LvarDxeSmmData"
#define LENOVO_VARIABLE_SMI_READY_FLAG_VARIABLE_NAME  L"LvarSmiReadyFlag"
#define LENOVO_VARIABLE_DXE_TO_SMM_FLAG               0x54
#define MAX_DATA_BUFFER_SIZE                          (1024 * 16)

#pragma pack(push)
#pragma pack(1)
//This struct is NVRAM header, please sync with flashmap.fdf
typedef struct {
  CHAR8                   Signature[4];
  UINT32                  PriorityFlag;
  UINT32                  VariableCount;
  UINT8                   LockFlag;
  UINT8                   RandomNumber;
  UINT16                  Checksum;
} LENOVO_VARIABLE_RIGION_HEADER;

/*
Variable region layout is:
LENOVO_VARIABLE_RIGION_HEADER
Variable1 {
  EFI_GUID                             VariableGuidName1;
  UINT32                               VariableDataSize1;
  UINT32                               VariableAttribute1;
  UINT8                                VariableData[VariableDataSize1];
}
Variable2 {
  EFI_GUID                             VariableGuidName2;
  UINT32                               VariableDataSize2;
  UINT32                               VariableAttribute2;
  UINT8                                VariableData[VariableDataSize2];
}
...

*/

typedef struct {
  EFI_GUID                             VariableGuidName;
  UINT32                               VariableDataSize;  //Only VariableData size
  UINT32                               VariableAttribute;
  UINT8                                VariableData[1];   //The length is not fixed
} LENOVO_VARIABLE_VARIABLE;
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
typedef struct {
  CHAR8                   Signature[4];
  UINT32                  NextUsable;
  UINT8                   Reserved[24];
} LENOVO_DEBUG_RIGION_HEADER;

typedef struct {
  UINT8                   Time[7];
  UINT8                   Action;
  EFI_GUID                Guid;
  UINT32                  DataLength;
  UINT8                   Reserved[4];
} LENOVO_DEBUG_DATA;
#pragma pack(pop)

typedef struct {
  UINT32                                Signature;
  UINT8                                 Region;
  UINT8                                 *VariableBuffer;
  UINT8                                 *LastByte;
  LENOVO_VARIABLE_PROTOCOL              LenovoVariableInstance;
  UINT8                                 *DebugBuffer;
} LENOVO_VARIABLE_PRIVATE_DATA;

#define LENV_SIGNATURE  SIGNATURE_32('L','E','N','V')

#define INSTANCE_FROM_LENV_THIS(a)  CR (a, LENOVO_VARIABLE_PRIVATE_DATA, LenovoVariableInstance, LENV_SIGNATURE)


/////////////////////////////////////////////////////////////////////////////////
////////Please keep these defines sync with the corresponding application////////
/////////////////////////////////////////////////////////////////////////////////
#define LENOVO_VARIABLE_CALLBACK         0x80    //If change this, must change asm file
#define LENOVO_VARIABLE_FLAG             0x5300  //If change this, must change asm file

#define GET_VARIABLE_FUNCTION            1
#define SET_VARIABLE_FUNCTION            2
#define DELETE_VARIABLE_FUNCTION         3
//#define LOCK_REGION_FUNCTION             4
//#define UNLOCK_REGION_FUNCTION           5
#define LOCK_VARIABLE_FUNCTION           6
#define UNLOCK_VARIABLE_FUNCTION         7

#define DUMP_REGION_FUNCTION             8
#define GET_BUFFER_ADDRESS_FUNCTION      9
#define APP_BUFFER_SIZE_IN_PAGE          8

#define LVAR_SUCCESS                     0xCE
#define LVAR_FUNCTION_NUMBER_WRONG       0xB1
#define LVAR_BUFFER_TOO_SMALL            0xB2
#define LVAR_NOT_FOUND                   0xB3
#define LVAR_REGIONE_LOCKED              0xB4
//#define LVAR_ACCESS_DENIED               0xB5  //Not used
#define LVAR_OTHER_ERROR                 0xB6
#define LVAR_VARIABLE_LOCKED             0xB7
#define LVAR_PASSWORD_WRONG              0xB8

//This struct for APP to pass data to SMI handle
#pragma pack(push)
#pragma pack(1)
typedef struct {
   IN OUT UINT16                        FunctionCode;      //Same as BX, not used, just for compatible
   IN     EFI_GUID                      VariableGuidName;  //Variable GUID
   IN OUT UINT16                        Name[5];           //Not used, just for compatible
   IN OUT UINT32                        DataLength;        //Variable data length
   IN OUT UINT8                         Data[1];           //Variable data area
} SMAPI_DATA;
#pragma pack(pop)

#define  LVAR_NULL_GUID                   {0x00000000, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define  LVAR_PRODUCT_NAME_GUID           {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x00}
#define  LVAR_PROJECT_NAME_GUID           {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x01}
#define  LVAR_MTM_NUMBER_GUID             {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x02}
#define  LVAR_SERIAL_NUMBER1_GUID         {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x03}
#define  LVAR_LENOVO_SN_GUID              {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x04}
#define  LVAR_UUID_GUID                   {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x05}
#define  LVAR_BRAND_TYPE_GUID             {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x06}
#define  LVAR_KEYBOARD_ID_GUID            {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x07}
#define  LVAR_EPA_ID_GUID                 {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x08}
#define  LVAR_FUNCTION_FLAG_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x09}
#define  LVAR_CUSTOMER_GUID               {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x0a}
#define  LVAR_FAMILY_NAME_GUID            {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x0b}
#define  LVAR_ASSET_TAG_GUID              {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x0c}
#define  LVAR_SKU_NUMBER_GUID             {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x0d}
#define  LVAR_OS_LICENSE_INFO_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x0e}
#define  LVAR_OS_PN_NUMBER_GUID           {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x0f}
#define  LVAR_OS_DESCRIPTOR_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x00, 0x10}

#define  LVAR_OA3_MSDM_DATA_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x01, 0x00}
#define  LVAR_OA2_AREA_DATA_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x01, 0x01}
#define  LVAR_OA2_SLIC_MARKER_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x01, 0x02}

#define  LVAR_SYSTEM_SUPER_PASSWORD_GUID  {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x02, 0x00}
#define  LVAR_SYSTEM_USER_PASSWORD_GUID   {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x02, 0x01}

#define  LVAR_COMPUTRACE_DATA_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x03, 0x00}

// Not used
#define  LVAR_NEVER_CRASH_CPT_GUID        {0xc2873663, 0xb2cb, 0x4f7a, 0x85, 0x48, 0xa6, 0x04, 0x11, 0xf5, 0xec, 0x86}

#define  LVAR_WIN8_64_DEFAULT_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x04, 0x00}
#define  LVAR_LEGACY_OS_DEFAULT_GUID      {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x04, 0x01}

// This is the 64bit variable defined in minimum spec
#define  LVAR_64BIT_CONFIGURATION_GUID    {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x05, 0x00}
  #define  LVAR_OS_TYPE_MASK                 0x00000000000000ff    // byte 0
    #define  LVAR_OS_TYPE_DO_NOTHING           0
    #define  LVAR_OS_TYPE_WIN8_64BIT           1
    #define  LVAR_OS_TYPE_LEGACY_OS            2
    #define  LVAR_OS_TYPE_LEGACY_OS_LEGACY_FI  4
  #define  LVAR_TPM_MASK                     0x000000000000ff00    // byte 1
    #define  LVAR_TPM_DO_NOTHING               0
    #define  LVAR_TPM_DISPLAY_ENABLE           1
    #define  LVAR_TPM_DISPLAY_DISABLE          2
    #define  LVAR_TPM_HIDE_ENABLE              3
    #define  LVAR_TPM_HIDE_DISABLE             4
  #define  LVAR_OS_OPTIMIZED_MASK            0x0000000000ff0000    // byte 2
    #define  LVAR_OS_DO_NOTHING                0
    #define  LVAR_OS_OPTIMIZED_ENABLE          1
    #define  LVAR_OS_OPTIMIZED_DISABLE         2


#define  LVAR_MFG_FLAG_GUID               {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x06, 0x00}

// Not used
#define  LVAR_LSE_UPLOAD_INFO_FLAG_GUID   {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0a, 0x00}
#define  LVAR_LSE_ON_OFF_FLAG_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0a, 0x01}

#define  LVAR_OA3_KEY_ID_GUID             {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0b, 0x00}

// Not used
#define  LVAR_MFG_DONE_FLAG_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0c, 0x00}

#define  LVAR_MFG_MODE_FLAG_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0d, 0x00}

// It's the flag to control Computrace string show or not, refer to Computrace spec chap. 5
#define  LVAR_COMPUTRACE_STRING_FLAG_GUID {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0e, 0x00}
// For ODM use, to store ODM project name
#define LVAR_ODM_PROJECT_NAME_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0f, 0x00}
// For supporting Mac Address Pass Through feature
#define LVAR_MAC_PASS_THROUGH_GUID        {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x10, 0x00}
// For Modern Preload feature, TPM lock
#define  LVAR_TPM_LOCK_FLAG_GUID          {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x11, 0x00}
// For ISO 3166 country code
#define  LVAR_COUNTRY_CODE_GUID           {0xc20e5755, 0x1169, 0x4c56, 0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x12, 0x00}
/////////////////////////////////////////////////////////////////////////////////
////////Please keep these defines sync with the corresponding application////////
/////////////////////////////////////////////////////////////////////////////////

extern EFI_GUID gLenovoVariableProtocolGuid;
//extern EFI_GUID gEfiL05VariableProtocolGuid;

#endif
