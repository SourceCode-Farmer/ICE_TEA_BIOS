 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _EFI_EC_FLASH_H
#define _EFI_EC_FLASH_H

typedef struct _EFI_ECFLASH_PROTOCOL EFI_ECFLASH_PROTOCOL;

//
// Global Id for EC ROM FLash Interface
//
#define EFI_ECFLASH_PROTOCOL_GUID \
  { \
    0x10c61889, 0x8d72, 0x4704, 0x9e, 0x97, 0x16, 0xcb, 0xc8, 0xe1, 0xe3, 0xd4 \
  }

//
// Upgrade EC ROM
//
typedef
EFI_STATUS
(EFIAPI *EFI_UPGRADE_EC_ROM) (
  IN  EFI_ECFLASH_PROTOCOL           *this
  );

//
// Protocol definition
//
//[-start-210517-KEBIN00001-modify]//
//Follow EDK2 Grammar modify it, because Insyde code base 5.4 build it error, so modify it.
//typedef struct _EFI_ECFLASH_PROTOCOL {
struct _EFI_ECFLASH_PROTOCOL {
  EFI_UPGRADE_EC_ROM UpgradeEcRom;
};
//[-end-210517-KEBIN00001-modify]//

extern EFI_GUID gEfiEcFlashProtocolGuid;

#endif //_EFI_EC_FLASH_H
