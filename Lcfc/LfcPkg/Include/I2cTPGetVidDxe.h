 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2017, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef __LFC_I2C_TP_CAPSULE_H__
#define __LFC_I2C_TP_CAPSULE_H__



#define ZERO_TP_FIRMWARE_GUID \
  { \
    0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} \
  }

extern EFI_GUID gWindowsELANTPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsSYNATPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsALPSTPFirmwareCapsuleGuid;
//[-start-210930-YUNLEI0141-modify]//
#if defined(C770_SUPPORT) || defined(C970_SUPPORT)
extern EFI_GUID gWindowsSYNA1TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsELAN1TPFirmwareCapsuleGuid;
#endif
//[-end-210930-YUNLEI0141-modify]//
//[-start-211015-Ching000014-modify]//
#if defined(S77013_SUPPORT)
extern EFI_GUID gWindowsSYNA2TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsELAN2TPFirmwareCapsuleGuid;
#endif
//[-start-220127-Dennis0013-add]//
//[-start-220209-OWENWU0037-add]//
//[-start-220224-Dennis0015-add]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT)
extern EFI_GUID gWindowsELAN3TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsELAN4TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsELAN5TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsELAN6TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsSYNA3TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsSYNA4TPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsCIRQUETPFirmwareCapsuleGuid;
extern EFI_GUID gWindowsFOCALTPFirmwareCapsuleGuid;
#endif
//[-end-220224-Dennis0015-add]//
//[-end-220209-OWENWU0037-add]//
//[-end-220127-Dennis0013-add]//
//[-end-211015-Ching000014-modify]//
#endif

