 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2013, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef __LFC_SW_SMI_CPU_REGISTER_LIB_H__
#define __LFC_SW_SMI_CPU_REGISTER_LIB_H__


EFI_STATUS
EFIAPI
SmmLfcSwSmiCpuRegisterLibConstructor (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  );


EFI_STATUS
GetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  OUT UINT32                            *RegisterData
  );

EFI_STATUS
SetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  IN  UINT32                            *RegisterData
  );

EFI_STATUS
IdentifyCpuIndexByEax (
  IN  UINT32                            EaxValue,
  IN  UINT32                            EaxValueMask,
  OUT UINTN                             *CpuIndex
  );

EFI_STATUS
GetSwSmiSubFunctionNumber (
  OUT UINT8                             *SwSmiSubFunctionNumber
  );

EFI_STATUS
SetSwSmiSubFunctionNumber (
  IN UINT8                              SwSmiSubFunctionNumber
  );

#endif