 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _EC_LIBRARY_H_
#define _EC_LIBRARY_H_
#include <Uefi.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>

#define ACPI_TIMER_ADDR       (PcdGet16(PcdPerfPkgAcpiIoPortBaseAddress) + 0x08)
#define ACPI_TIMER_MAX_VALUE  0x1000000

#define KEY_CMD_STATE                     0x64
#define KBC_TIME_OUT                      0x10000
#define KEY_OBF                           1
#define KEY_IBF                           2

#define KBC_DATA                          0x60
#define KBC_CMD_STATE                     0x64

#define EC_DATA                           0x62
#define EC_CMD_STATE                      0x66

#define EC_DATA2                           0x68
#define EC_CMD_STATE2                      0x6C

#define EC_READ_ECRAM_CMD                 0x80
#define EC_WRITE_ECRAM_CMD                0x81

#define IRQ_8259_MASK                     0x21

#define KBC_READ_CMD_BYTE                 0x20
#define KBC_WRITE_CMD_BYTE                0x60

#define EC_HOOK_CMD                       0x45

#define EC_USB_CHARGE_EN_DATA              0x8E
#define EC_USB_CHARGE_DIS_DATA             0x8F       

#define EC_DETECT_NOVO                    0xAE
#define EC_MAINBOARDID                    0xAB

#define BEEP_ENABLE_DATA                  0xE9
#define BEEP_DISABLE_DATA                 0xEA

#define ENABLE_POWER_BUTTON               0x20
#define DISABLE_POWER_BUTTON              0x21

#define GET_EC_REVISION_CMD               0x51
#define EC_TOOl_CMD                       0xEF
#define EC_ENABLE_SHIPMODE_DATA           0x20
#define EC_ENTER_CONVERSATIONMODE_DATA    0x21
#define EC_EXIT_CONVERSATIONMODE_DATA     0x22
#define EC_CHECK_CONVERSATIONMODE_DATA    0x23
#define EC_CLEAR_FIRSTUSEDATA_DATA        0x24
#define EC_CHECK_FIRSTUSEDATA_DATA        0x25
#define EC_CHECK_ACDC_MODE                0x26
#define EC_DISABLE_AC                     0x27
#define EC_ENABLE_AC                      0x28
#define EC_CHARGE_CAP                     0x29
#define EC_CHARGE_DISCHARGE_CAP           0x30
#define EC_OSCONTROL_POWERLEDON           0x40
#define EC_OSCONTROL_POWERLEDOFF          0x41
#define EC_OSCONTROL_CHARGELEDON          0x42
#define EC_OSCONTROL_CHARGELEDOFF         0x43
#define EC_OSCONTROL_DISCHARGELEDON       0x44
#define EC_OSCONTROL_DISCHARGELEDOFF      0x45
#define EC_OSCONTROL_NUMLOCKLEDON         0x46
#define EC_OSCONTROL_NUMLOCKLEDOFF        0x47
#define EC_OSCONTROL_CPSLOCKLEDON         0x48
#define EC_OSCONTROL_CPSLOCKLEDOFF        0x49
#define EC_OSCONTROL_KBBACKLIGHTLEDON     0x50
#define EC_OSCONTROL_KBBACKLIGHTLEDOFF    0x51
#define EC_OSCONTROL_FPLED1ON             0x52
#define EC_OSCONTROL_FPLED1OFF            0x53
#define EC_OSCONTROL_FPLED2ON             0x54
#define EC_OSCONTROL_FPLED2OFF            0x55
#define EC_OSCONTROL_FNLOCKLED1ON         0x56
#define EC_OSCONTROL_FNLOCKLED1OFF        0x57
#define EC_OSCONTROL_ALLLEDON             0x58
#define EC_OSCONTROL_ALLLEDOFF            0x59
#define EC_ALLLEDCONTROL_BACKTOEC         0x5A

#define EC_GETCPUGPU_TEMP                 0x60
#define EC_GETCPU_TEMP_EXVALUE            0x01
#define EC_GETGPU_TEMP_EXVALUE            0x02
#define EC_NEARCPU_TEMP_EXVALUE           0x03
#define EC_NEARGPU_TEMP_EXVALUE           0x04
#define EC_CHARGE_TEMP_EXVALUE            0x05
#define EC_ENVIRONMENT_TEMP_EXVALUE       0x06
#define EC_GETSSD_TEMP_EXVALUE            0x07
#define EC_GETRAM_TEMP_EXVALUE            0x08

#define EC_SETFAN1_SPEED                  0x61
#define EC_SETFAN2_SPEED                  0x62
#define EC_GETFAN_SPEED                   0x63
#define EC_GETFAN1_SPEED_EXVALUE          0x01
#define EC_GETFAN2_SPEED_EXVALUE          0x02
#define EC_FANCONTROL_TOEC                0x64
#define EC_GET_FANNUM                     0x65

#define EC_AUTOPOWERON                    0xA0
#define EC_LID_DISABLE                    0xA1
#define EC_LID_ENABLE                     0xA2
#define EC_LID_STATUS                     0xA3
#define EC_BKL_DISABLE                    0xA4
#define EC_BKL_ENABLE                     0xA5

#define EC_EN_SCAN_PWRBUTTON_TEST         0xA6
#define EC_DIS_SCAN_PWRBUTTON_TEST        0xA7
#define EC_GET_PD_VERSION                 0xA8
#define EC_WATCHDOG_DIS                   0xA9
#define EC_WATCHDOG_EN                    0xAA
#define EC_GETLIDOPEN_STATUS              0xAB
typedef enum {
 KEY_NONE,
 KEY_F2,                            
 KEY_F12                           
}HOTKEY_TYPE;

//EC CMD 0x52
#define EC_PLATFORM_CMD                   0x52
#define EC_PRJECT_NAME                    0xA0
#define EC_PROJECT_ID                     0xA2
#define EC_BOOT_CODE_SIZE                 0xA3
#define EC_CRITICAL_STATUS                0xA4
  #define EC_CRITICAL_STATUS_CRISIS_MODE  0x80
  #define EC_CRITICAL_STATUS_NORMAL_MODE  0x80
#define EC_RELEASE_VER                    0xA5
#define EC_DET_ADAPTER_STATUS             0xA6
//EC CMD 0x59
#define EC_SYSTEM_NOTIFI_CMD              0x59
//[-start-201125-WITAID0021-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77014IAH_SUPPORT)
#define EC_SYSTEM_NOTIFI_DARFON_CMD       0x5A
#endif
//[-end-201125-WITAID0021-add]//

//#define EC_SYSTEM_NOTIFI_TP_ENABLED        0x7A    // Enabled Touch Pad
//#define EC_SYSTEM_NOTIFI_TP_DISABLED       0x7B    // Disabled Touch Pad

#define EC_SYSTEM_NOTIFI_CMD_SHUTDOWN      0xA2    // System Shutdown
#define EC_SYSTEM_NOTIFI_CMD_ALERT_BEEP    0xD2    // Beep Alerm 200ms
  
#define EC_SYSTEM_NOTIFI_MACHINE_SIZE_CMD    0x71

//[-start-210929-SHAONN0010-add]//
//#define EC_SYSTEM_NOTIFI_PROJECT_TYPE_CMD    0x73
//[-end-210929-SHAONN0010-add]//

//[-start-211109-BAIN000054-add]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
#define EC_SET_PD_MODE_CMD                0x1D
#define EC_GET_PD_MODE_CMD                0x1E
//[-start-211227-BAIN000077-add]//
#define EC_SET_COMPL_MODE_CMD             0x1F
//[-end-211227-BAIN000077-add]//
#endif
//[-end-211109-BAIN000054-add]//

//[-start-211220-BAIN000070-add]//
#ifdef LCFC_SUPPORT
#define EC_SET_WDG_REST_CMD               0x8E
#endif
//[-end-211220-BAIN000070-add]//

//[-start-211223-BAIN000072-add]//
#ifdef LCFC_SUPPORT
#define EC_SET_CLEAN_CMOS                 0x48
#endif
//[-end-211223-BAIN000072-add]//

//[-start-220104-Dongxu0040-add]// 
#define EC_CPU_TYPE_CMD                   0x45
//[-end-220104-Dongxu0040-add]// 

#define EC_ACPI_MODE_EN_DATA              0xE8
#define EC_ACPI_MODE_DIS_DATA             0xE9
#define EC_ENTER_IDLE_MODE                0xF1
//[-start-201125-WITAID0021-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77014IAH_SUPPORT)
#define EC_ENG_KEYBOARD_MATRIX             0x00
#define EC_GRE_KEYBOARD_MATRIX             0x01
#define EC_EUR_KEYBOARD_MATRIX             0x02
#define EC_KOP_KEYBOARD_MATRIX             0x03
#define EC_TCH_KEYBOARD_MATRIX             0x04
#define EC_RUS_KEYBOARD_MATRIX             0x05
#define EC_ARA_KEYBOARD_MATRIX             0x06
#define EC_HBW_KEYBOARD_MATRIX             0x07
#define EC_THA_KEYBOARD_MATRIX             0x08
#define EC_HIN_KEYBOARD_MATRIX             0x09
#define EC_UKE_KEYBOARD_MATRIX             0x10
#define EC_ITA_KEYBOARD_MATRIX             0x11
#define EC_SPA_KEYBOARD_MATRIX             0x12
#define EC_TUR_KEYBOARD_MATRIX             0x13
#define EC_POR_KEYBOARD_MATRIX             0x14
#define EC_LAS_KEYBOARD_MATRIX             0x15
#define EC_FRE_KEYBOARD_MATRIX             0x16
#define EC_NOR_KEYBOARD_MATRIX             0x17
#define EC_FRA_KEYBOARD_MATRIX             0x18
#define EC_GER_KEYBOARD_MATRIX             0x19
#define EC_BRL_KEYBOARD_MATRIX             0x20
#define EC_HUN_KEYBOARD_MATRIX             0x21
#define EC_BEL_KEYBOARD_MATRIX             0x22
#define EC_ILD_KEYBOARD_MATRIX             0x23
#define EC_SLV_KEYBOARD_MATRIX             0x24
#define EC_SWS_KEYBOARD_MATRIX             0x25
#define EC_BUL_KEYBOARD_MATRIX             0x26
#define EC_CZE_KEYBOARD_MATRIX             0x27
#define EC_ARF_KEYBOARD_MATRIX             0x28
#define EC_JPN_KEYBOARD_MATRIX             0x29
#define EC_UKR_KEYBOARD_MATRIX             0x30
#define EC_USA_KEYBOARD_MATRIX             0x31
//[-start-220824-Skywang0824-add]//    
#define EC_BRL_Alexa_KEYBOARD_MATRIX       0xA0
//[-start-220824-Skywang0824-add]//    

#else
#define EC_FR_KEYBOARD_MATRIX             0xE3
#define EC_EX_KEYBOARD_MATRIX             0xE4
#define EC_US_KEYBOARD_MATRIX             0xE5
#define EC_JP_KEYBOARD_MATRIX             0xE6
#define EC_UK_KEYBOARD_MATRIX             0xE7
#endif
//[-end-201125-WITAID0021-modify]//

#define GET_EC_DEBUGFLAG_CMD              0xB1
#define GET_EC_DEBUGFLAG_DATA             0xB1

#define EC_DEBUGFLAG_TRUE                 0x01

#define EC_BATTERY_FIRST_USE_DATE_STATUS_CMD 0x7A
#define EC_BATTERY_FIRST_USE_GET_FLAG        0x11
#define EC_BATTERY_FIRST_USE_SET_READY       0x12
#define EC_BATTERY_FIRST_USE_GET_TIME        0x7B
#define EC_BATTERY_FIRST_USE_GET_YEAR        0x20
#define EC_BATTERY_FIRST_USE_GET_MONTH       0x21
#define EC_BATTERY_FIRST_USE_GET_DATE        0x22

#define GET_EC_POSTCODE_CMD               0xB2
#define GET_EC_POSTCODE_DATA              0xB2

#define EC_POSTCODE_MAX_LENGTH            0x08

//EC name space define
#define VPC_COMMAND                       0x00  // 0x00       VPC command
#define VPC_DATA                          0x01  // 0x01       VPC data
#define VPC_STATUS                        0x02  // 0x02       VPC status
#ifdef SUPPORT_2ND_BATTERY
//0x03~0x07 Customer EC name space
//0x08~0x1F Project EC name space
#define BATTERY_STATUS_0x20               0x20  // 0x20       Battery status
                                                // 0x20:0     Battery type(0:NiMh,1:LiIon)
                                                // 0x20:1     Battery mode(For AP only);
                                                // 0x20:2     Low battery
                                                // 0x20:3     Reserved
                                                // 0x20[6:4]  Manufacture(001:SANYO,010:Sony,100:Panasonic,101:CPT)
                                                // 0x20:7     Reserved
#define BATTERY_STATUS_0x21               0x21  // 0x21       Battery status
                                                // 0x21:0     Discharging
                                                // 0x21:1     Charging
                                                // 0x21:2     Discharging and Now is critical low
                                                // 0x21[7:3]  Reserved
#define REMAINING2_CAPACITY_0             0x22  // 0x22       Remaining Capacity offset 0
#define REMAINING2_CAPACITY_1             0x23  // 0x23       Remaining Capacity offset 1
#define SERIAL2_NUMBER_0                  0x24  // 0x24       Serial Number offset 0
#define SERIAL2_NUMBER_1                  0x25  // 0x25       Serial Number offset 1
#define PRESENT2_VOLTAGE_0                0x26  // 0x26       Present Voltage offset 0
#define PRESENT2_VOLTAGE_1                0x27  // 0x27       Present Voltage offset 1
#define DESIGN2_VOLTAGE_0                 0x28  // 0x28       Design Voltage offset 0
#define DESIGN2_VOLTAGE_1                 0x29  // 0x29       Design Voltage offset 1
#define DESIGN2_CAPACITY_0                0x2A  // 0x2A       Design Capacity offset 0
#define DESIGN2_CAPACITY_1                0x2B  // 0x2B       Design Capacity offset 1
#define FULL2_CHARGE_CAPACITY_0           0x2C  // 0x2C       Full charge capacity offset 0
#define FULL2_CHARGE_CAPACITY_1           0x2D  // 0x2D       Full charge capacity offset 1
#define GAS2_GAUGE                        0x2E  // 0x2E       Gas gauge
#define BATTERY2_CYCLE_COUNTER            0x2F  // 0x2F       Battery cycle counter
#define BATTERY2_CURRENT_0                0x30  // 0x30       Battery current offset 0
#define BATTERY2_CURRENT_1                0x31  // 0x31       Battery current offset 1
#define BATTERY2_AVERAGE_CURRENT_0        0x32  // 0x32       Battery average current offset 0
#define BATTERY2_AVERAGE_CURRENT_1        0x33  // 0x33       Battery average current offset 1
#define SYSTEM2_POWER_CONSUMPTION         0x34  // 0x34       System power consumption
#define BATTERY2_VOLT                     0x35  // 0x35       Battery Volt
#define BATTERY2_TEMP                     0x36  // 0x36       Battery Temp
#define BATTERY2_AVERAGE_TEMP             0x37  // 0x37       Battery Average Temp
#define BATTERY2_CHARGE_CURRENT_0         0x38  // 0x38       Battery charge current offset 0
#define BATTERY2_CHARGE_CURRENT_1         0x39  // 0x39       Battery charge current offset 1
#define BATTERY2_CURRENT_TSC              0x3A  // 0x3A       Battery current Temp sample counter
#define BATTERY2_COMMAND_INDEX            0x3B  // 0x3B       Battery Command index for read battery through SMBus
#define BATTERY2_COMMUNICATE_COUNTUP      0x3C  // 0x3C       Count up to communicate battery
#define BATTERY2_OVER_TEMP_COUNTUP        0x3D  // 0x3D       Count up if battery is arrived over Temp
#define BATTERY2_SMBUS_STATUS_0           0x3E  // 0x3E       Battery SMBus status offset 0
#define BATTERY2_SMBUS_STATUS_1           0x3F  // 0x3F       Battery SMBus status offset 1
#define BATTERY2_OVER_VOLTAGE_COUNTER     0x40  // 0x40       Battery over voltage counter
#define COMMUNICATION2_FAIL_COUNTER       0x41  // 0x41       Communication fail counter
#define BATTERY2_VOLTAGE_ADC              0x42  // 0x42       Battery voltage of ADC
#define CELL1_VOLTAGE2_0                  0x43  // 0x43       Cell1 voltage offset 0
#define CELL1_VOLTAGE2_1                  0x44  // 0x44       Cell1 voltage offset 1
#define CELL2_VOLTAGE2_0                  0x45  // 0x45       Cell2 voltage offset 0
#define CELL2_VOLTAGE2_1                  0x46  // 0x46       Cell2 voltage offset 1
#define CELL3_VOLTAGE2_0                  0x47  // 0x47       Cell3 voltage offset 0
#define CELL3_VOLTAGE2_1                  0x48  // 0x48       Cell3 voltage offset 1
#define CELL4_VOLTAGE2_0                  0x49  // 0x49       Cell4 voltage offset 0
#define CELL4_VOLTAGE2_1                  0x4A  // 0x4A       Cell4 voltage offset 1
//0x4B~0x4F Reserved
#define BATTERY2_STATUS1                  0x50  // 0x50       Battery staus1
                                                // 0x50:0     Battery exist
                                                // 0x50:1     Battery full
                                                // 0x50:2     Battery empty
                                                // 0x50:3     Battery first in
                                                // 0x50[5:4]  Reserved
                                                // 0x50:6     Battery read complete
                                                // 0x50:7     Use LI_ION or NI_MH#
#define BATTERY2_STATUS2                  0x51  // 0x51       Battery staus2
                                                // 0x51:0     Low low battery
                                                // 0x51:1     Check EDV1
                                                // 0x51:2     Set EDV1
                                                // 0x51:3     Battery shutdown 5%
                                                // 0x51:4     Start to read battery
                                                // 0x51:5     Start to count communication counter
#define BATTERY2_STOP_CHARGE_STATUS_L     0x52  // 0x52       Battery stop charge status(low byte)
                                                // 0x52:0     Battery fast charging timeout
                                                // 0x52:1     Battery bad cell
                                                // 0x52:2     Battery communication fail
                                                // 0x52:3     Use for detect battery charging suspend
                                                // 0x52:4     Battery command stop charge
                                                // 0x52:5     Battery stop charge for Customer use
                                                // 0x52:6     Battery over voltage
                                                // 0x52:7     Battery over temperature
#define BATTERY2_STOP_CHARGE_STATUS_H     0x53  // 0x53       Battery stop charge status(high byte)
                                                // 0x53:0     Battery trickle charging timeout
                                                // 0x53:1     Stop charge of Adapter throttling
                                                // 0x53:2     Stop charge of Sony battery
                                                // 0x53:4     Stop charge of flash
                                                // 0x53:5     Wrong Battery name
#define MANUFACTURE2_DATE_0               0x54  // 0x54       Manufacture Date offset 0
#define MANUFACTURE2_DATE_1               0x55  // 0x55       Manufacture Date offset 1
//0x56~0x57 Reserved
#define BATTERY2_CELL_NUMBER              0x58  // 0x58       Battery Cell number
                                                // 0x58:0     Set Battery is 3 cells
                                                // 0x58:1     Set Battery is 4 cells
                                                // 0x58:2     Set Battery is 6 cells
                                                // 0x58:3     Set Battery is 8 cells
                                                // 0x58:4     Set Battery is 9 cells
                                                // 0x58:5     Set Battery is 12 cells
                                                // 0x58:6     Set battery is 3 series
                                                // 0x58:7     Set battery is 4 series
//0x5A Reserved for ISCT
//0x59~0x5C Reserved
#else
//0x00~0x05 Customer EC name space
#define FAN_SPEED                         0x06  // 0x06       Fan speed. For Lenovo ATM
#define EC_SPACE_0x07                     0x07  // 0x07:0     Battery usage (1: Battery has been running over 72hrs, EM should pop out a message)
                                                // 0x07:1     Battery legal(1: Battery is legal; 0: Battery is illegal)
                                                // 0x07:2     Battery performance(1: Battery performance is on the normal status
                                                // 0x07:3     Notify storage mode (1: Indicates close the storage mode)
                                                // 0x07:4     Battery Testing Mode (1:Start Battery Testing Mode: turn off all LED lights except the Power Button LED,
                                                //            Make the step1 fan on temperature increased by 10 degrees in thermal table.)
                                                // 0x07:5     Bad battery(1: indicates the main battery is bad.)
                                                // 0x07:6     Auto mode(1: Start Auto mode; 0: Stop Auto mode)
                                                // 0x07:7     Fast charge mode(1: Fast Charge is running; 0: Fast Charge is stopping.)
#define AC_BRIGHTNESS_TABLE_LIMIT         0x08  // 0x08       AC Brightness Table Limit
#define DC_BRIGHTNESS_TABLE_LIMIT         0x09  // 0x09       DC Brightness Table Limit
#define EC_SPACE_0x0A                     0x0A  // 0x0A:0     End the calibration cycle
                                                // 0x0A:1     1=Ambient light sensor on;0=off
                                                // 0x0A:2     calibration cycle for the main battery
                                                // 0x0A:3     calibration cycle for the second battery
                                                // 0x0A:4     Battery storage mode
                                                // 0x0A:5     Battery Cycle mode
                                                // 0x0A:6     Battery longest mode
                                                // 0x0A:7     Clear bit of advise user to select battery mode
#define EC_SPACE_0x0B                     0x0B  // 0x0B:0     Switchable status(1:Discrete,0:UMA)
                                                // 0x0B:1     HDMI exist(1=HDMI plug in;0=No HDMI)
                                                // 0x0B:2     Hybrid Graphics(1:Hybrid Graphics 0:External Graphic Only)
                                                // 0x0B:3     Switch state(1:Switch started 0:Switch complete)
                                                // 0x0B:4     EVENT_NOTIFY(1:Need to notify EM)
                                                // 0x0B:5     DCR function(1:Enable 0:Disable)
                                                // 0x0B:6     Color Engine(1:Enable 0:Disable)
                                                // 0x0B:7     SG function control right(1:AP control;0:EC control)
#define EC_SPACE_0x0C                     0x0C  // 0x0C:0     ODD power off(1:Power off)
                                                // 0x0C:1     ODD power on by button(1:Power on by button)
                                                // 0x0C:2     ODD power on by key(1:Power on by key)
                                                // 0x0C:3     1:The camera is exist;0:The camera isn't exist;
                                                // 0x0C:4     1:The camera power is on;0: Off
                                                // 0x0C:5     Reserved for BIOS: Save ODD state when into S3 for BIOS used
                                                // 0x0C:6     ODD power on(1: ODD power on when system resume from S3/S4 or AC plug in.)
                                                // 0x0C:7     ODD power status(1: ODD have Power 0:ODD no power)
#define RAM_TEMPERATUTE                   0x0D  // 0x0D       Ram temperature
#define VRAM_TEMPERATUTE                  0x0E  // 0x0E       V Ram temperature
#define ATM_FAN_CONTROL_COMMANDS          0x0F  // 0x0F       ATM Fan control commands
#define EC_SPACE_0x10                     0x10  // 0x10[2:0]  Platform information is writed by BIOS(000->SV+UMA 001->ULV+UMA 011->SV+NV 010->SV+ATI 101->ULV+NV 100->ULV+ATI)
                                                // 0x10:3     1-Super performance running/0-Super performance stopped.
                                                // 0x10:4     1-indicate the temperature of ATI graphic platform CPU or GPU is too high
                                                //            0-indicate the temperature of ATI graphic platform CPU or GPU is too low
                                                // 0x10:5     1-start quite mode; 0-stop quite mode.
                                                // 0x10:6     1-Enable Lenovo S3.5;0-Disable Lenovo S3.5
                                                // 0x10:7     Reserved
#define EC_SPACE_0x11                     0x11  // 0x11:0     1-Disable reading information from primary battery for update battery firmware.
                                                // 0x11:1     1-Disable reading information from second battery for update battery firmware.
                                                // 0x11[3:2]  VGA type-00:UMA,01:GL1,10:GE,11:GT
#define FIRMWARE_UPDATE_STATUS_L          0x12  // 0x12       Firmware update status low byte
#define FIRMWARE_UPDATE_STATUS_H          0x13  // 0x13       Firmware update status high byte
#define BATTERY_MARKER_ID_L               0x14  // 0x14       Battery marker ID low byte
#define BATTERY_MARKER_ID_H               0x15  // 0x15       Battery marker ID high byte
#define HARDWARE_ID_L                     0x16  // 0x16       Hardware ID low byte
#define HARDWARE_ID_H                     0x17  // 0x17       Hardware ID high byte
#define FIRMWARE_VERSION_L                0x18  // 0x18       Firmware version low byte
#define FIRMWARE_VERSION_H                0x19  // 0x19       Firmware version high byte
#define DATA_VERSION_L                    0x1A  // 0x1A       Data version low byte
#define DATA_VERSION_H                    0x1B  // 0x1B       Data version high byte
//0x1C~0x2F Project EC name space
//[-start-200327-CLYDE00001-add]//
#define BATTERY_TIMER_COUNT_L             0x24  // 0x24       Battery timer count low byte
#define BATTERY_TIMER_COUNT_H             0x25  // 0x25       Battery timer count high byte
//[-end-200327-CLYDE00001-add]//
//0x30~0x47 Panel Information is write by BIOS
//0x48~0x5C Project EC name space
#endif
//External name space definition
#define EXT_NAMESPACE_INDEX               0x5D  // 0x5D       EXT_NAMESPACE_INDEX
#define EXT_NAMESPACE_BANK                0x5E  // 0x5E       EXT_NAMESPACE_BANK
#define EXT_NAMESPACE_DATA                0x5F  // 0x5F       EXT_NAMESPACE_DATA
//SMBus EC interface ACPI RAM definition
#define SMBUS_PROTOCOL                    0x60  // 0x60       SMBus protocol
#define EC_SPACE_0x61                     0x61  // 0x61       bit:0~4 SMBus Status;5 Reserved;6 ALARM;7 DONE
#define SMBUS_ADDRESS                     0x62  // 0x62       SMBus Address
#define SMBUS_COMMAND                     0x63  // 0x63       SMBus Command
//0x64~0x83 SMBus Data
#define SMBUS_DATA_0                      0x64  // 0x64       SMBus Data offset 0
#define SMBUS_DATA_0x1F                   0x83  // 0x83       SMBus Data offset 0x1F
#define SMBUS_BCNT                        0x84  // 0x84       SMBus BCNT
#define SMBUS_ALARM_ADDRESS               0x85  // 0x85       SMBus alarm address
#define SMBUS_ALARM_DATA_0                0x86  // 0x86       SMBus alarm data 0
#define SMBUS_ALARM_DATA_1                0x87  // 0x87       SMBus alarm data 1
//Battery information reading
//0x90~0x98 Battery Manufacture Name
#define BATTERY_MANUFACTURE_NAME_0        0x90  // 0x90       Battery Manufacture Name offset 0
#define BATTERY_MANUFACTURE_NAME_8        0x98  // 0x98       Battery Manufacture Name offset 8
//0x99~0x9F Battery Device Name
#define BATTERY_DEVICE_NAME_0             0x99  // 0x99       Battery Device Name offset 0
#define BATTERY_DEVICE_NAME_6             0x9F  // 0x9F       Battery Device Name offset 6
//EC interface OEM common RAM definition
#define EC_SPACE_0xA0                     0xA0  // 0xA0:0     ISCT battery temperature limitation
                                                // 0xA0:1     ISCT battery capacity limitation
                                                // 0xA0:2     ISCT S0_ISCT status
                                                // 0xA0:3     ISCT RTC wake status
                                                // 0xA0:4     ISCT support enable
                                                // 0xA0:5     ISCT EC wake up for ISCT2.0
                                                // 0xA0:6     ISCT Wake Up Reason
                                                // 0xA0:7     Reserved
#define EC_SPACE_0xA1                     0xA1  // 0xA1       Key And Button Function
                                                // 0xA1:0     Reserved
                                                // 0xA1:1     Video Output Key(1: Video output key pressed);
                                                // 0xA1:2     Touch pad button (1:enable)
                                                // 0xA1:3     Hotkey function disable in setup(0 :enable 1:disable)
                                                // 0xA1[7:4]  Reserved
#define EC_SPACE_0xA2                     0xA2  // 0xA2       OS Shutdown Temp. For system read setting (DTS)
#define EC_SPACE_0xA3                     0xA3  // 0xA3       SYS_STATUS: System indicator
                                                // 0xA3[2:0]  OS type(000:Windows XP,001:Windows Vista,010:Linux,011:Windows 7)
                                                // 0xA3[6:3]  Reserved
                                                // 0xA3:7     AC adaptor(0: Offline,1:Online)
#define WAKEUP_ENABLE                     0xA4  // 0xA4       WAKEUP_ENABLE: Enable wake up function
                                                // 0xA4:0     PMEWAKE(PME Wk Enable:0=Disable,1=Enable)
                                                // 0xA4:1     MDMWAKE(Modem Wk Enable:0=Disable,1=Enable)
                                                // 0xA4:2     LANWAKE(LAN wakeup enable:0=Disable,1=Enable)
                                                // 0xA4:3     RTCWAKE(RTC wakeup Enable:0=DIsable,1=Enable)
                                                // 0xA4:4     WLANWAKE(Wireless LAN wakeup Enable:0=Disable,1=Enable)
                                                // 0xA4:5     USBWAKE(USB wakeup enable: 0=Disable,1=Enable)
                                                // 0xA4:6     KEYWAKE(Keyboard wakeup enable:0=Disable,1=Enable)
                                                // 0xA4:7     TPWAKE(Touchpad wakeup enable:0=Disable,1=Enable)
#define ACOFF_SOURCE                      0xA5  // 0xA5       ACOFF_SOURCE: Enable AC_OFF signal
                                                // 0xA5:0     CHARGECURRENT(1:AC_OFF for when stop charge but have charge current)
                                                // 0xA5:1     ADPOVP(1: AC_OFF for adaptor improper)
                                                // 0xA5:2     BATTLEARN(1:AC_OFF for battery at learning mode)
                                                // 0xA5:3     CMD(1:AC_OFF for Command)
                                                // 0xA5:4     BATTOVP(1:AC_OFF for battery OVP)
                                                // 0xA5:5     LEAKCURRENT(1: AC_OFF for Leakage current)
                                                // 0xA5:6     AIRADAPTOR(1:AC_OFF for air-adaptor)
                                                // 0xA5:7     ACOFF(Control AC_OFF active,0:enable,1:disable)
#define ENTER_RESUME_STAT                 0xA6  // 0xA6       ENTER_RESUME_STAT
                                                // 0xA6:0     Entry S3 State flag
                                                // 0xA6:1     Resume S3 State flag
                                                // 0xA6:2     Entry S4 State flag
                                                // 0xA6:3     Resume S4 State flag
                                                // 0xA6:4     Entry S5 State flag
                                                // 0xA6:5     Resume S5 State flag
                                                // 0xA6[7:6]  Reserved
#define OS_THROTTLING_TEMP                0xA7  // 0xA7       OS Throttling Temp. For system read setting
#define OS_SHUTDOWN_TEMP                  0xA8  // 0xA8       OS Shutdown Temp. For system read setting
#define THROTTLING_TEMP                   0xA9  // 0xA9       Throttling Temp put a temp in for system throttling
#define TCONTROL_TEMP                     0xAA  // 0xAA       For Tcontrol temp, SysBIOS will put Tcontrol temp
#define THERMAL_STATUS1                   0xAB  // 0xAB       THERMAL_STATUS1
                                                // 0xAB:0     MODE(0:Local mode,1:Remote mode)
                                                // 0xAB[2:1]  Reserved
                                                // 0xAB:3     INITOK(0:Control by OS,1:Control by EC)
                                                // 0xAB:4     FAN1 Active
                                                // 0xAB:5     FAN2 Active
                                                // 0xAB:6     FAN speed timer init OK
                                                // 0xAB:7     SKINMODE(0:skin address 90,1:skin address 92)
#define SHUTDOWNTEMP                      0xAC  // 0xAC       SHUTDOWNTEMP: Shundown temperature
#define FAN_STATUS1                       0xAD  // 0xAD       FAN_STATUS1: Just for utility
                                                // 0xAD[3:0]  FAN's speed step number
                                                // 0xAD[7:4]  FAN's number
#define THROTTLING_INDEX                  0xAE  // 0xAE       THROTTLING_INDEX
                                                // 0xAE[3:0]  Throttling level Number of P-State level
                                                // 0xAE:6     THRM_SW(1:SW Throttling active)
                                                // 0xAE:7     THRM_PIN(Throttling status:1=HW Throttling Pin active)
#define THROTTLING_SOURCE                 0xAF  // 0xAF       THROTTLING_SOURCE
                                                // 0xAF:0     TS_THERMAL(1:Throttling for thermal)
                                                // 0xAF:1     TS_BATTCAP(1:Throttling for capacity)
                                                // 0xAF:2     Reserved for Cedar trail platform sync bit used
                                                // 0xAF:3     Reserved
                                                // 0xAF[7:4]  Number of P-State level
#define CPU_TEMP                          0xB0  // 0xB0       CPU_TEMP: CPU current temperature
#define CPU_TEMP_LOCAL                    0xB1  // 0xB1       CPU_TEMP_LOCAL: CPU local temperature
//0xB2~0xB6 This is not common area , please define for project use
#define ACPI_FLAG2                        0xB7  // 0xB7       ACPI_FLAG2
                                                // 0xB7:0     Reserved
                                                // 0xB7:1     Password button(1:enable)
                                                // 0xB7:2     Digital mode selected,set 1
                                                // 0xB7:3     CD Lock mode enable,set 1
                                                // 0xB7[7:4]  Reserved
#define SWI_EVENT_INDICATORS              0xB8  // 0xB8       SWI Event indicators
                                                // 0xB8:0     Reserved
                                                // 0xB0:1     Lid open event(0=off,1=on)
                                                // 0xB8:2     PME event(0=off,1=on)
                                                // 0xB8:3     Power button event(0=off,1=on)
                                                // 0xB8:4     Ring in event(0=off,1=on)
                                                // 0xB8:5     Blue tooth wake up event(0=off,1=on)
                                                // 0xB8[7:6]  Reserved
#define LCD_BRIGHTNESS_VALUE              0xB9  // 0xB9       LCD brightness value (0x00-0x07)
#define EC_SPACE_0xBA                     0xBA  // 0xBA:0     1: Enable AOAC function
                                                // 0xBA:1     S3.5(fast boot) function-Status flag
                                                // 0xBA:2     IFFS status
                                                // 0xBA:3     1:When system in S0-ISCT mode
                                                // 0xBA:4     1:Notify BIOS when battery low capacity in ISCT
                                                // 0xBA:5     1:Notify BIOS when battery over temp in ISCT
                                                // 0xBA:6     Reserved
                                                // 0xBA:7     1:BIOS RTC wake and ready to S0-ISCT from S3
#define DEVICE_MOUDLE_STATUS              0xBB  // 0xBB       Device module status (TBD)
                                                // 0xBB:0     1:Wirless LAN active,0:Wirless LAN no active
                                                // 0xBB:1     1:BlueTooth active,0:BlueTooth no active
                                                // 0xBB:2     1:Wirless LAN exist,0:Wirless LAN no exist
                                                // 0xBB:3     1:BlueTooth exist,0:BlueTooth no exist
                                                // 0xBB:4     1:Kill switch on,0:Kill switch off
                                                // 0xBB:5     1:WireLess LAN initial OK
                                                // 0xBB:6     1:3G active,0:3G no active
                                                // 0xBB:7     1:3G exist,0:3G no exist
#define KBPROJECTID                       0xBC  // 0xBC       KbProjectID: Project ID
#define EC_SPACE_0xBD                     0xBD  // 0xBD[2:0]  CPU_TYPE: CPU type(00:Tj85,01:Tj90,02:Tj100,03:Tj105,04-07:Reverve)
                                                // 0xBD[5:3]  CPU Core Number(00:One Core,01:Dual Core,02:Quad Core,03-07:Reverve)
                                                // 0xBD[7:6]  VGA Type(00:UMA,01:NV VGA,02:AMD VGA,03:Reserved)
//Word registers to Emulate smart selector RAM definition
#define SEL_STATE0                        0xBE  // 0xBE       SEL_STATE0
                                                // 0xBE:0     PRESENT_A(Set if 1st battery present)
                                                // 0xBE:1     PRESENT_B(Set if 2nd battery present)
                                                // 0xBE[3:2]  Reserved
                                                // 0xBE:4     CHARGE_A(Set if 1st battery be charging)
                                                // 0xBE:5     CHARGE_B(Set if 2nd battery be charging)
                                                // 0xBE[7:6]  Reserved
#define SEL_STATE1                        0xBF  // 0xBF       SEL_STATE1
                                                // 0xBF:0     PWR_BY_A(Set if system power up by 1st)
                                                // 0xBF:1     PWR_BY_B(Set if system power up by 2nd)
                                                // 0xBF[3:2]  Reserved
                                                // 0xBF:4     SMB_A(Set if 1st battery on SMBus)
                                                // 0xBF:5     SMB_B(Set if 2nd battery on SMBus)
                                                // 0xBF[7:6]  Reserved
//Control method for 1st battery pack RAM definition
#define BATTERY_STATUS_0xC0               0xC0  // 0xC0       Battery status
                                                // 0xC0:0     Battery type(0:NiMh,1:LiIon)
                                                // 0xC0:1     Battery mode(For AP read only)
                                                // 0xC0:2     Low battery
                                                // 0xC0:3     Reserved
                                                // 0xC0[6:4]  Battery Manufacture(001:SANYO,010:Sony,100:Panasonic,101:Samsung,011:LG,110:Celxpert,111:Simplo)
                                                // 0xC0:7     Reserved
#define BATTERY_STATUS_0xC1               0xC1  // 0xC1       Battery Status
                                                // 0xC1:0     Discharging
                                                // 0xC1:1     Charging
                                                // 0xC1:2     Discharging and Now is critical low
                                                // 0xC1[7:3]  Reserved
#define REMAINING_CAPACITY_0              0xC2  // 0xC2       Remaining Capacity offset 0
#define REMAINING_CAPACITY_1              0xC3  // 0xC3       Remaining Capacity offset 1
#define SERIAL_NUMBER_0                   0xC4  // 0xC4       Serial Number offset 0
#define SERIAL_NUMBER_1                   0xC5  // 0xC5       Serial Number offset 1
#define PRESENT_VOLTAGE_0                 0xC6  // 0xC6       Present Voltage offset 0
#define PRESENT_VOLTAGE_1                 0xC7  // 0xC7       Present Voltage offset 1
#define DESIGN_VOLTAGE_0                  0xC8  // 0xC8       Design Voltage offset 0
#define DESIGN_VOLTAGE_1                  0xC9  // 0xC9       Design Voltage offset 1
#define DESIGN_CAPACITY_0                 0xCA  // 0xCA       Design Capacity offset 0
#define DESIGN_CAPACITY_1                 0xCB  // 0xCB       Design Capacity offset 1
#define FULL_CHARGE_CAPACITY_0            0xCC  // 0xCC       Full charge capacity offset 0
#define FULL_CHARGE_CAPACITY_1            0xCD  // 0xCD       Full charge capacity offset 1
#define GAS_GAUGE                         0xCE  // 0xCE       Gas gauge
#define BATTERY_CYCLE_COUNTER             0xCF  // 0xCF       Battery cycle counter
#define BATTERY_CURRENT_0                 0xD0  // 0xD0       Battery current offset 0
#define BATTERY_CURRENT_1                 0xD1  // 0xD1       Battery current offset 1
#define BATTERY_AVERAGE_CURRENT_0         0xD2  // 0xD2       Battery average current offset 0
#define BATTERY_AVERAGE_CURRENT_1         0xD3  // 0xD3       Battery average current offset 1
#define BATTERY_POWER_CONSUMPTION         0xD4  // 0xD4       Battery power consumption
#define BATTERY_VOLT                      0xD5  // 0xD5       Battery Volt
#define BATTERY_TEMP                      0xD6  // 0xD6       Battery Temp
#define BATTERY_AVERAGE_TEMP              0xD7  // 0xD7       Battery Average Temp
#define BATTERY_CHARGE_CURRENT_0          0xD8  // 0xD8       Battery charge current offset 0
#define BATTERY_CHARGE_CURRENT_1          0xD9  // 0xD9       Battery charge current offset 1
#define BATTERY_CURRENT_TSC               0xDA  // 0xDA       Battery current Temp sample counter
#define BATTERY_COMMAND_INDEX             0xDB  // 0xDB       Battery Command index for read battery through SMBus
#define BATTERY_COMMUNICATE_COUNTUP       0xDC  // 0xDC       Count up to communicate battery
#define BATTERY_OVER_TEMP_COUNTUP         0xDD  // 0xDD       Count up if battery is arrived over Temp
#define BATTERY_SMBUS_STATUS_0            0xDE  // 0xDE       Battery SMBus status offset 0
#define BATTERY_SMBUS_STATUS_1            0xDF  // 0xDF       Battery SMBus status offset 1
#define BATTERY_OVER_VOLTAGE_COUNTER      0xE0  // 0xE0       Battery over voltage counter
#define COMMUNICATION_FAIL_COUNTER        0xE1  // 0xE1       Communication fail counter
#define BATTERY_VOLTAGE_ADC               0xE2  // 0xE2       Battery voltage of ADC
#define CELL1_VOLTAGE_0                   0xE3  // 0xE3       Cell1 voltage offset 0
#define CELL1_VOLTAGE_1                   0xE4  // 0xE4       Cell1 voltage offset 1
#define CELL2_VOLTAGE_0                   0xE5  // 0xE5       Cell2 voltage offset 0
#define CELL2_VOLTAGE_1                   0xE6  // 0xE6       Cell2 voltage offset 1
#define CELL3_VOLTAGE_0                   0xE7  // 0xE7       Cell3 voltage offset 0
#define CELL3_VOLTAGE_1                   0xE8  // 0xE8       Cell3 voltage offset 1
#define CELL4_VOLTAGE_0                   0xE9  // 0xE9       Cell4 voltage offset 0
#define CELL4_VOLTAGE_1                   0xEA  // 0xEA       Cell4 voltage offset 1
//0xEB~0xEF Reserved
#define BATTERY_STATUS1                   0xF0  // 0xF0       Battery staus1
                                                // 0xF0:0     Battery exist
                                                // 0xF0:1     Battery full
                                                // 0xF0:2     Battery empty
                                                // 0xF0:3     Battery first in
                                                // 0xF0[5:4]  Reserved
                                                // 0xF0:6     Battery read complete
                                                // 0xF0:7     Reserved
#define BATTERY_STATUS2                   0xF1  // 0xF1       Battery staus2
                                                // 0xF1:0     Low low battery
                                                // 0xF1:1     Check EDV1
                                                // 0xF1:2     Set EDV1
                                                // 0xF1:3     Battery shutdown 5%
                                                // 0xF1:4     Start to read battery
                                                // 0xF1:5     Start to count communication counter
                                                // 0xF1[7:6]  Reserved
#define BATTERY_STOP_CHARGE_STATUS_L      0xF2  // 0xF2       Battery stop charge status(low byte)
                                                // 0xF2:0     Battery fast charging timeout
                                                // 0xF2:1     Battery bad cell
                                                // 0xF2:2     Battery communication fail
                                                // 0xF2:3     Use for detect battery charging suspend;4 Battery command stop charge
                                                // 0xF2:5     Battery stop charge for Customer use
                                                // 0xF2:6     Battery over voltage
                                                // 0xF2:7     Battery over temperature
#define BATTERY_STOP_CHARGE_STATUS_H      0xF3  // 0xF3       Battery stop charge status(high byte)
                                                // 0xF3:0     Battery trickle charging timeout
                                                // 0xF3:1     Stop charge of Adapter throttling
                                                // 0xF3:2     Stop charge of Sony battery
                                                // 0xF3:4     Stop charge of flash
                                                // 0xF3:5     Wrong Battery name
                                                // 0xF3[7:6]  Reserved
#define MANUFACTURE_DATE_0                0xF4  // 0xF4       Manufacture Date offset 0
#define MANUFACTURE_DATE_1                0xF5  // 0xF5       Manufacture Date offset 1
//0xF6~0xF7 Reserved (For EC or AP to get 1st Batt Charging Voltage)
#define BATTERY_CELL_NUMBER               0xF8  // 0xF8       Battery Cell number
                                                // 0xF8:0     Set Battery is 3 cells
                                                // 0xF8:1     Set Battery is 4 cells
                                                // 0xF8:2     Set Battery is 6 cells
                                                // 0xF8:3     Set Battery is 8 cells
                                                // 0xF8:4     Set Battery is 9 cells
                                                // 0xF8:5     Set Battery is 12 cells
                                                // 0xF8:6     Set battery is 3 series
                                                // 0xF8:7     Set battery is 4 series
#define POST_HOTKEY                       0xFC  // 0xFC       Post Hotkey status
                                                // 0xFC:0     F2 status (0= unpressed,1=pressed)
                                                // 0xFC:1     F12 status (0= unpressed,1=pressed)

//[-start-210731-BAIN000026-add]//
#define EC_SMI_DATA                       0xFD  // 0xFD       EC SMI Data
                                                // 0xFD:0     1: Hang check
                                                // 0xFD:1     1: FnLock LED light, 0: FnLock LED not light
                                                // 0xFD:2     1: Num Lock LED light, 0: Num Lock LED not light
                                                // 0xFD:3     1: Caps Lock LED light, 0: Caps Lock LED not light
//[-end-210731-BAIN000026-add]//

#define BALANCE_MODE                      0x01
#define QUIET_MODE                        0x02
#define HIGH_PERFORMANCE_MODE             0x03



//Function prototype

EFI_STATUS
WaitIBE (
  IN UINT16                         CommandPort
  );

EFI_STATUS
WaitOBF (
  IN UINT16                         CommandPort
  );

EFI_STATUS
WaitOBE (
  IN UINT16                         CommandPort
);

EFI_STATUS
EcCommand (
  IN UINT16                         CommandPort,
  IN UINT16                         DataPort,
  IN UINTN                        NumOfReturnData,
  IN UINT8                          *ReturnData,
  IN UINT8                          Command,
  IN UINTN                          NumOfArgs,
  ...
  );

EFI_STATUS
SendEcCMD (
  IN UINT16                         CommandPort,
  IN UINT16                         DataPort,
  IN UINT8                          Command,
  IN BOOLEAN                        ReturnFlag,
  IN UINT8                         *ReturnData
  );

EFI_STATUS
SendEcCMDData (
  IN UINT16                         CommandPort,
  IN UINT16                         DataPort,
  IN UINT8                          Command,
  IN UINT8                          Data,
  IN BOOLEAN                        ReturnFlag,
  IN UINT8                          *ReturnData
  );

EFI_STATUS
LfcEcLibEnableEcPower (
  VOID
  );

EFI_STATUS 
LfcEcLibDisableEcPower (
  VOID
  );

EFI_STATUS
LfcEcLibPowerStateIsAc (
  OUT BOOLEAN                        *PowerStateIsAc
  );

EFI_STATUS
LfcEcLibEcRamRead(
  IN  UINT8                         Index,
  OUT UINT8                         *Data
  );

EFI_STATUS
LfcEcLibEcRamWrite(
  IN UINT8                          Index,
  IN UINT8                          Value
  );

EFI_STATUS
LfcEcLibSaveRestoreKbc (
  IN  BOOLEAN                       SaveRestoreFlag
  );

EFI_STATUS
LfcEcLibGetTouchpadID (
  IN OUT UINT8                      *TouchpadID
  );

EFI_STATUS
LfcEcLibGetBatterySerial (
  IN OUT  UINT8                     *DataLength,
  IN OUT  UINT8                     *DataBuffer
  );

EFI_STATUS
LfcEcLibGetBatteryPercentage (
  IN OUT  UINT8                     *BatteryPercentage
  );

EFI_STATUS
LfcEcLibNotifyEcKeyboardId (
  IN   UINT8                        KeyboardID
  );

EFI_STATUS
LfcEcLibNotifyEcMachineSize (
  IN   UINT8                        MachineSize
  );

EFI_STATUS
LfcEcLibEcReboot (
  IN UINT16                         CommandPort,
  IN UINT16                         DataPort
  );

EFI_STATUS
LfcEcLibEcReset (
  VOID
  );

EFI_STATUS
LfcEcLibEcShutdown (
  VOID
  );

EFI_STATUS
LfcEcLibReadEcVersion (
  IN OUT UINT8                      *EcMajorVersion,
  IN OUT UINT8                      *EcMinorVersion,
  IN OUT UINT8                      *EcTestVersion
  );

EFI_STATUS
LfcEcLibAcInOutBeepControl (
  IN  BOOLEAN                        EnableEcBeep
  );

EFI_STATUS
LfcEcLibSetHotKeyMode (
  IN  UINT8                          HotKeyMode
  );

EFI_STATUS
LfcEcLibSetAlwaysOnUsbMode (
  IN  UINT8                          AlwaysOnUsbMode
  );

EFI_STATUS
LfcEcLibSetChargeInBatteryMode (
  IN  UINT8                          ChargeInBatteryMode
  );

EFI_STATUS
LfcEcLibSetOneKayBattery (
  IN  UINT8                          OneKeyBattery
  );

EFI_STATUS
LfcEcLibCheckEcDebugFlag(
  IN UINT16                          CommandPort,
  IN UINT16                          DataPort,
  IN BOOLEAN                         *EcDebugFlag
  );



EFI_STATUS
LfcEcLibDumpEcPostCode (
  IN UINT16                          CommandPort,
  IN UINT16                          DataPort,
  IN UINT8                           *OemPostCode
  );

EFI_STATUS
LfcEcLibTellEcDisOrUma (
  IN UINT8                           Uma0Nv1Amd2
  );

EFI_STATUS
LfcEcLibEcAcpiMode (
  IN  BOOLEAN                        EnableEcMode
  );

EFI_STATUS
EcAcpiStall (
  IN UINTN                           Microseconds
  );

EFI_STATUS
LfcEcLibGetRecoverMode (
  OUT BOOLEAN                        *RecoverMode
  );

EFI_STATUS
LfcEcLibGetNovoStatus (
  OUT BOOLEAN                        *NovoKeyStatus
  );

EFI_STATUS
LfcEcLibGetBatteryFirstUsedDate (
  VOID
  );

EFI_STATUS
LfcEcLibEcInit (
  VOID
  );

EFI_STATUS
LfcEcLibEcWait (
  IN  BOOLEAN                        EnableWrites
  );

EFI_STATUS
LfcEcLibEcIdle (
  IN  BOOLEAN                        EnableWrites
  );

EFI_STATUS
LfcEcLibDisableKeyBoard (
  VOID
);

EFI_STATUS
LfcEcLibPowerButtonControl (
  IN  BOOLEAN                           EnablePowerButton
  );

EFI_STATUS
LfcEcLibStartLight(
  );

EFI_STATUS
LfcEcLibStopLight(
  );

EFI_STATUS
LfcCleanUpKbcAndEcBuffer (
  );

EFI_STATUS
LfcEcLibGetHotkeyFromEc (
  IN OUT HOTKEY_TYPE    *Selection
  );

EFI_STATUS
LfcEcLibSetPerformanceMode(
  );

EFI_STATUS
LfcEcLibSetFlipToBootMode (
  IN  UINT8 L05FlipToBoot
);

EFI_STATUS
LfcEcLibSwapFnCtrlMode (
  IN  UINT8 		SwapMode
  );

//[-start-211222-JOYID00009-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
EFI_STATUS
LfcEcLibEnableWatchDogForMemoryTrainning (
  VOID
  );

EFI_STATUS
LfcEcLibDisableWatchDogForMemoryTrainning (
  VOID
  );
#endif
//[-end-211222-JOYID00009-add]//

//[-start-211231-JOYID00010-add]
//[-start-220117-QINGLIN0146-modify]//
//[-start-220119-OWENWU0036-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
EFI_STATUS
LfcEcLibStopWDT (
  );

EFI_STATUS
LfcEcLibStartWDT (
  );
#endif
//[-end-220119-OWENWU0036-modify]//
//[-end-220117-QINGLIN0146-modify]//
//[-end-211231-JOYID00010-add]

//[-start-200327-CLYDE00001-add]//
EFI_STATUS
LfcEcLibGetBatteryTimerCount (
  IN OUT  UINT16                             *BatteryTimerCount
  );
//[-end-200327-CLYDE00001-add]//

//[-start-220210-Dongxu0040-Modify]//
EFI_STATUS
LfcCheckValidTool(
OUT UINT8                      *Data
);
//[-end-220210-Dongxu0040-Modify]//

EFI_STATUS
LNVEcBatterySetting (
  IN UINT8                           Function,
  IN UINT8                           Action,
  IN UINT8                           ExValue
);
//[-start-200821-BAIN000035-add]//
//[-start-210903-YUNLEI0129-modify]//
//[-start-210906-SHAONN0005-modify]//
//[-start-211110-GEORGE0021-modify]//
//#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT)
#ifdef LCFC_SUPPORT
//[-end-211110-GEORGE0021-modify]//
EFI_STATUS
LfcEcLibNotifyThermalMax();

EFI_STATUS
LfcEcLibNotifyThermalMaxStop();
#endif
//[-end-210906-SHAONN0005-modify]//
//[-end-210903-YUNLEI0129-modify]//
//[-end-200821-BAIN000035-add]//

//[-start-210929-SHAONN0010-add]//
#if defined(S370_SUPPORT) 
EFI_STATUS
LfcEcLibNotifyEcProjectType (
  IN   UINT8                             ProjectType
  );
#endif
//[-start-211104-YUNLEI0152-modify]//
#if defined(C770_SUPPORT) 
EFI_STATUS
OemSvcLfcGetCpuIdentifier(
  OUT  UINT8    *LfcCpuId
);
#endif
//[-end-211104-YUNLEI0152-modify]//

//[-end-210929-SHAONN0010-add]//
//[-start-211014-BAIN000051-add]//
//#ifdef LCFC_SUPPORT
//#if defined(C970_BSH_SUPPORT)
//[-start-21119-TAMT000032-modify]//
//[-start-211203-QINGLIN0125-modify]//
//[-start-211206-OWENWU0029-modify]//
//[-start-211214-Ching000017-modify]//
EFI_STATUS
LfcEcLibSetWDTFlagForSelfHealing (
  );

EFI_STATUS
LfcEcLibDisableCurrentWDTForSelfHealing (
  );

EFI_STATUS
LfcEcLibClearWDTFlagForSelfHealing (
  );

EFI_STATUS
LfcEcLibDisableAndClearTopSwapForSelfHealing (
  );

EFI_STATUS
LfcEcLibClearCmosForSelfHealing(
  );
//[-end-211214-Ching000017-modify]//
//[-end-211206-OWENWU0029-modify]//
//[-end-211203-QINGLIN0125-modify]//
//[-end-21119-TAMT000032-modify]//
//[-end-211014-BAIN000051-add]//

//[-start-211109-BAIN000054-modify]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT)||defined(S77014IAH_SUPPORT)
EFI_STATUS
LfcEcLibSetPdControllerMode(
  IN  UINT8            PdControllerMode
);

EFI_STATUS
LfcEcLibGetPdControllerMode(
  OUT UINT8            *PdControllerMode
);

//[-start-211227-BAIN000077-add]//
EFI_STATUS
LfcEcLibSetCOMPLMode(
);
//[-end-211227-BAIN000077-add]//

#endif
//[-end-211109-BAIN000054-modify]//

//[-start-211220-BAIN000070-add]//
#ifdef LCFC_SUPPORT
EFI_STATUS
LfcEcLibSetRestWDGCMD(
);
#endif
//[-end-211220-BAIN000070-add]//
//[-start-220104-Dongxu0040-add]// 
EFI_STATUS
LfcEcLibi7i5(
  IN  UINT8            CpuType
);
//[-end-220104-Dongxu0040-add]// 
//[-start-220120-QINGLIN0151-add]//
#if defined(S370_SUPPORT)
EFI_STATUS
LfcEcLibNotifyEcCpuType (
  IN  UINT8             CpuType
);

//[-start-220224-QINGLIN0163-add]//
EFI_STATUS
LfcEcLibSetUltraQuietMode (
  IN UINT8    UltraQuietMode
  );

EFI_STATUS
LfcEcLibSwitchFoolProofFnCtrl (
  IN UINT8    FoolProofFnCtrl
);
//[-end-220224-QINGLIN0163-add]//
#endif
//[-end-220120-QINGLIN0151-add]//
#endif

//[-start-220407-JEPLIUT219-add]//
#if defined(S570_SUPPORT)
EFI_STATUS
LfcEcLibGetAdapter65WAcOnlyMode (
   OUT  UINT8                         * Adapter65WAc
  );
#endif
//[-end-220407-JEPLIUT219-add]//
