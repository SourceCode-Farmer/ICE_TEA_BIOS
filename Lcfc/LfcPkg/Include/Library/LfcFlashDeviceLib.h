 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2013, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef __LFC_FLASH_DEVICE_LIB_H__
#define __LFC_FLASH_DEVICE_LIB_H__

/*
Read NumBytes from PAddress (flash device) to Buffer
*/
EFI_STATUS
EFIAPI
LfcFdLibRead (
  IN     UINTN                           PAddress,
  IN OUT UINTN                           *NumBytes,
     OUT UINT8                           *Buffer
  );

/*
Write NumBytes of Butter to PAddress (flash device)
*/
EFI_STATUS 
EFIAPI
LfcFdLibWrite (
  IN      UINTN                          PAddress,
  IN OUT  UINTN                          *NumBytes,
	IN      UINT8                          *Buffer
  );

/*
Erase LbaLength from PAddress (flash device)
*/
EFI_STATUS 
EFIAPI
LfcFdLibBlockErase (
  IN      UINTN                          PAddress,
  IN      UINTN                          LbaLength      
  );
#endif

