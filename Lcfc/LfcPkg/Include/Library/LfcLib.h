 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************

#ifndef _LFC_LIB_H_
#define _LFC_LIB_H_

//[-start-210705-CISSIE0651-add]//Lfc UUID
#define EC_SRAM_BASE        0xFE0B0000
#define EC_MBV_UUID_REG     0xE00
#define EC_MBV_UUID_FLAG    0xE20
//[-end-210705-CISSIE0651-add]//
EFI_STATUS
EFIAPI
LfcLibLogError (
  IN  UINT8                        Data
  );

EFI_STATUS
LfcProjectLibStall (
  IN UINTN              Microseconds
  );
  
EFI_STATUS
EFIAPI
LfcProjectLibSetDGPUPowerEN (
  UINT8 Level
  );
  
EFI_STATUS
EFIAPI
LfcProjectLibSetDGPUPowerRST (
  UINT8 Level
  );

EFI_STATUS
EFIAPI
LfcProjectLibGetDGPUPowerGood (
  UINT8 *PowerGood
  );
#endif

