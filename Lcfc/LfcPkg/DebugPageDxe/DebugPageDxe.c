 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/
/*
Data          Name          Version    Description
2014.06.06    Jansen.Jia    v1.00      Modify the code sequence for post code variable always can be used.
*/
/*
Data          Name          Version    Description
2015.12.16    Ken.Zhang     v1.01      Add show Shutdown ID on debug page function.
*/

#include "DebugPageDxe.h"
//#include "GetEcShutdownID.h"
#include <Library/OemSvcLfcPeiGetBoardID.h>

UINTN    mMaxColumn;
UINTN    mMaxRow;

BOOLEAN  gEcDebugFlag = FALSE;
BOOLEAN  gMfgModeFlag = FALSE;
UINT8    gLfcPostCode[EC_POSTCODE_MAX_LENGTH];
UINT8    gWakeSource = 0;
//UINT8    gLfcShutdownId = 0;

//
// Length of temp string buffer to store value string.
//
#define CHARACTER_NUMBER_FOR_VALUE  30

CHAR16  *mOptionStringList[] = { \
          LFC_DEBUG_PAGE_BLANK_STRING, LFC_DEBUG_PAGE_PLATFORM_TYPE_STRING, LFC_DEBUG_PAGE_BLANK_STRING,\
          LFC_DEBUG_PAGE_POST_CODE_STRING, LFC_DEBUG_PAGE_BLANK_STRING, LFC_DEBUG_PAGE_WAKE_SOURCE_STRING,\
          LFC_DEBUG_PAGE_BLANK_STRING, LFC_DEBUG_PAGE_SHUTDOWN_ID_STRING, LFC_DEBUG_PAGE_BLANK_STRING,\
          LFC_DEBUG_PAGE_MFG_MODE_STRING, LFC_DEBUG_PAGE_BLANK_STRING,\
         };

CHAR16  *mWakeSourceStringList[] = { \
          LFC_WAKE_BY_UNKNOWN_STRING,\
          LFC_WAKE_BY_POWER_BTN_STRING, LFC_WAKE_BY_NOVO_BTN_STRING, LFC_WAKE_BY_POWER_BTN_S4S5_STRING,\
          LFC_WAKE_BY_RTC_S4S5_STRING, LFC_WAKE_BY_POWER_BTN_S3_STRING, LFC_WAKE_BY_RTC_S3_STRING,\
          LFC_WAKE_BY_PCIE_S3_STRING, LFC_WAKE_BY_PME_S3_STRING, LFC_WAKE_BY_EC_WAKE_LID_STRING,\
         };

//SHUTDOWNID_DESC ShutdownIdMapArryStd[] = { 
////ShutdownId, ShutdownIdString
//  { 0x01,     LFC_SHUTDOWN_ID_S5_STRING},
//  { 0x04,     LFC_SHUTDOWN_ID_COMMAND_STRING},
//  { 0x05,     LFC_SHUTDOWN_ID_FLASH_STRING},
//  { 0x06,     LFC_SHUTDOWN_ID_COLDBOOT_STRING},
//  { 0x08,     LFC_SHUTDOWN_ID_BOOTFAIL_STRING},
//  { 0x0A,     LFC_SHUTDOWN_ID_BATTOVERTEMP_STRING},
//  { 0x0B,     LFC_SHUTDOWN_ID_THERMALCOMMFAIL_STRING},
//  { 0x0C,     LFC_SHUTDOWN_ID_BATTEMPTY_STRING},
//  { 0x0E,     LFC_SHUTDOWN_ID_BATTCOMMFAIL_STRING},
//  { 0x0F,     LFC_SHUTDOWN_ID_BATTABNORMAL_STRING},
//  { 0x11,     LFC_SHUTDOWN_ID_DTSOVERTEMP_STRING},
//  { 0x12,     LFC_SHUTDOWN_ID_VGAOVERTEMP_STRING},
//  { 0x13,     LFC_SHUTDOWN_ID_BATTLOWVOLT_STRING},
//  { 0x1C,     LFC_SHUTDOWN_ID_EXTVGAOVERTEMP_STRING},
//  { 0x20,     LFC_SHUTDOWN_ID_VGACOMMFAIL_STRING},
//  { 0x21,     LFC_SHUTDOWN_ID_EXTVGACOMMFAIL_STRING},
//  { 0x22,     LFC_SHUTDOWN_ID_CPUCOMMFAIL_STRING},
//  { 0x30,     LFC_SHUTDOWN_ID_TICLOCALOVERTEPM_STRING},
//  { 0x31,     LFC_SHUTDOWN_ID_TICREMOTOVERTEPM_STRING},
//  { 0x32,     LFC_SHUTDOWN_ID_TNTC_V_OVER_TEPM_STRING},
//  { 0x33,     LFC_SHUTDOWN_ID_CMISTOR_OVERTEMP_STRING},
//  { 0x34,     LFC_SHUTDOWN_ID_GMISTOR_OVERTEMP_STRING},
//  { 0x40,     LFC_SHUTDOWN_ID_RSOC_1PtO0P_STRING},
//  { 0xA6,     LFC_SHUTDOWN_ID_SHIP_MODE_STRING},
//  { 0xA7,     LFC_SHUTDOWN_ID_SHIP_MODE_STRING},
//  { 0xA8,     LFC_SHUTDOWN_ID_SHIP_MODE_STRING},
//  { 0xEC,     LFC_SHUTDOWN_ID_EC_RESET0_INFO_STRING},
//  { 0xFC,     LFC_SHUTDOWN_ID_EC_RESET1_INFO_STRING},
//  { 0xFD,     LFC_SHUTDOWN_ID_EC_RESET2_INFO_STRING},
//  { 0xFE,     LFC_SHUTDOWN_ID_EC_RESET3_INFO_STRING},
//  { 0xFF,     LFC_SHUTDOWN_ID_EC_RESET4_INFO_STRING},
//};

static CHAR16 mHexStr[] = { L'0', L'1', L'2', L'3', L'4', L'5', L'6', L'7',
                            L'8', L'9', L'A', L'B', L'C', L'D', L'E', L'F' };

//VOID
//GetAndSaveShutdownID (
//  VOID
//  )
//{
//  EFI_STATUS              Status   = EFI_SUCCESS;
//  UINT8                   *FileBuf = NULL;
//  UINTN                   BufSize  = 0;
//  BOOLEAN                 EcAd     = FALSE;
//  UINTN                   Index;
//  UINT32                  ShutdownIdAddr = 0;
//
//  //
//  // Allocating the file buffer
//  //
//  BufSize = 1 * BLOCK_SIZE_1K;
//  Status  = gBS->AllocatePages (
//                   AllocateAnyPages,
//                   EfiBootServicesData,
//                   EFI_SIZE_TO_PAGES(BufSize),
//                   (EFI_PHYSICAL_ADDRESS *) (&FileBuf)
//                   );
//  if (EFI_ERROR (Status)) {
//    goto Exit;
//  }
//
//  //
//  // Get Shutdown Id address, this can't be execute after enter into falsh mode, it will hang up.
//  // EC will not accept any command in the flash mode.
//  //
//  ShutdownIdAddr = GetShutdownIdAddr();
//
//  //
//  // Disable Keyboard  
//  //
//  SendKbCmd (0xAD);
//  EcAd = TRUE;
//
//  //
//  // Send command to EC, and prepare to enter into the flash mode.
//  //
//  SendEcCmd (0xDC);
//  
//  //
//  // Checking EC status
//  //
//  if (ReadEcData () != 0x33) {
//    Status = EFI_DEVICE_ERROR;
//    goto Exit;
//  }
//
//  //
//  // Dump EC ROM about Shutdown ID region
//  //
//  Status = ReadShutdownId1K (ShutdownIdAddr, FileBuf);
//  if (EFI_ERROR (Status)) {
//    goto Exit;
//  }
//
//  //
//  // To do: progress shutdown id
//  //
//  for (Index = 4; Index < BLOCK_SIZE_1K; Index += 16){
//    if (FileBuf[Index] == 0xFF) {
//      gLfcShutdownId = FileBuf[Index-16];
//      break;
//    }
//  }
//
//  //
//  // Save Shutdown ID data into variable.  
//  //
//   Status = gRT->SetVariable (
//                   LFC_SHUTDOWN_ID_DATA,
//                   &gLfcVariableGuid,
//                   EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
//                   sizeof (gLfcShutdownId),
//                   (VOID *)&gLfcShutdownId
//                   );
//
//   if (EFI_ERROR (Status)) {
//     goto Exit;
//   }
//
//   if (FileBuf != NULL) {
//     gBS->FreePages ((EFI_PHYSICAL_ADDRESS)(UINTN)FileBuf, EFI_SIZE_TO_PAGES (BufSize));
//   }
//
//Exit:
//
//   if (EcAd) {
//     SendKbCmd (0xAE);  // Enable Keyboard.
//     SendEcCmd (0xFE);  // EC will force to Reset the System.
//   } 
//
//   if (FileBuf != NULL) {
//     gBS->FreePages ((EFI_PHYSICAL_ADDRESS)(UINTN)FileBuf, EFI_SIZE_TO_PAGES (BufSize));
//   }
//}

UINTN
PrintAt (
  IN UINTN     Column,
  IN UINTN     Row,
  IN CHAR16    *Fmt,
  ...
  )
{
  CHAR16      *Buffer;
  UINTN       StrLen;
  VA_LIST     Marker;

  Buffer = AllocateZeroPool (0x10000);
  ASSERT(Buffer);
  if (Column != (UINTN) -1) {
    gST->ConOut->SetCursorPosition (gST->ConOut, Column, Row);
  }

  VA_START (Marker, Fmt);
  StrLen = UnicodeVSPrint (Buffer, 0x10000 , Fmt, Marker);
  VA_END (Marker);

  if (gST->ConOut != NULL) {
    gST->ConOut->OutputString (gST->ConOut, Buffer);
  }
  gBS->FreePool (Buffer);
  return StrLen;
}

UINTN
EfiValueToHexStr (
  IN  OUT CHAR16  *Buffer, 
  IN  UINT64      Value, 
  IN  UINTN       Flags, 
  IN  UINTN       Width
  )
/*++

Routine Description:

  VSPrint worker function that prints a Value as a hex number in Buffer

Arguments:

  Buffer - Location to place ascii hex string of Value.

  Value  - Hex value to convert to a string in Buffer.

  Flags  - Flags to use in printing Hex string, see file header for details.

  Width  - Width of hex value.

Returns: 

  Number of characters printed.  

--*/
{
  CHAR16  TempBuffer[CHARACTER_NUMBER_FOR_VALUE];
  CHAR16  *TempStr;
  CHAR16  Prefix;
  CHAR16  *BufferPtr;
  UINTN   Count;
  UINTN   Index;
  
  TempStr   = AllocateZeroPool (100);
  BufferPtr = AllocateZeroPool (100);

  TempStr   = TempBuffer;
  BufferPtr = Buffer;

  //
  // Count starts at one since we will null terminate. Each iteration of the
  // loop picks off one nibble. Oh yea TempStr ends up backwards
  //
  Count = 0;
  
  if (Width > CHARACTER_NUMBER_FOR_VALUE - 1) {
    Width = CHARACTER_NUMBER_FOR_VALUE - 1;
  }

  do {
    Index = ((UINTN)Value & 0xf);
    *(TempStr++) = mHexStr[Index];
    Value = RShiftU64 (Value, 4);
    Count++;
  } while (Value != 0);

  if (Flags & PREFIX_ZERO) {
    Prefix = '0';
  } else { 
    Prefix = ' ';
  }

  Index = Count;
  if (!(Flags & LEFT_JUSTIFY)) {
    for (; Index < Width; Index++) {
      *(TempStr++) = Prefix;
    }
  }

  //
  // Reverse temp string into Buffer.
  //
  if (Width > 0 && (UINTN) (TempStr - TempBuffer) > Width) {
    TempStr = TempBuffer + Width;
  }
  Index = 0;
  while (TempStr != TempBuffer) {
    *(BufferPtr++) = *(--TempStr);
    Index++;
  }
    
  *BufferPtr = 0;
  
  gBS->FreePool(TempStr);
  gBS->FreePool(BufferPtr);
  return Index;
}

VOID
DrawLfcDebugPageDialog (
  IN  UINTN     DialogColumn,
  IN  UINTN     DialogRow
  )
{
  UINTN                         Index;
  UINTN                         Color;
  UINTN                         LeftCol, RightCol;
  UINTN                         UpRow, DownRow;
  CHAR16                        CleanLine[MAX_STRING_LENGTH];

  LeftCol = (mMaxColumn - DialogColumn) / 2;
  RightCol = LeftCol + DialogColumn + 1;
  UpRow = (mMaxRow - DialogRow) /2 ;
  DownRow = UpRow + DialogRow + 1;

  Color = EFI_TEXT_ATTR (EFI_LIGHTGRAY, EFI_BLUE);

  gST->ConOut->SetAttribute (gST->ConOut, Color);

  for (Index = 0; Index <= DialogColumn; Index++) {
    CleanLine[Index] = 0x20;
  }
  CleanLine[Index] = 0;
  for (Index = UpRow; Index <= DownRow; Index++) {
    PrintAt (LeftCol, Index, L"%s", CleanLine);
  }

  for (Index = LeftCol; Index < RightCol; Index++) {
    PrintAt (Index, UpRow, L"%c", BOXDRAW_HORIZONTAL);
    PrintAt (Index, UpRow + 2, L"%c", BOXDRAW_HORIZONTAL);
    PrintAt (Index, DownRow, L"%c", BOXDRAW_HORIZONTAL);
  }
  for (Index = UpRow; Index < DownRow; Index++) {
    PrintAt (LeftCol,  Index, L"%c", BOXDRAW_VERTICAL);
    PrintAt (RightCol, Index, L"%c", BOXDRAW_VERTICAL);
  }

  PrintAt (RightCol, DownRow, L"%c", BOXDRAW_UP_LEFT);
  PrintAt (RightCol, UpRow, L"%c", BOXDRAW_DOWN_LEFT);
  PrintAt (LeftCol,  DownRow, L"%c", BOXDRAW_UP_RIGHT);
  PrintAt (LeftCol,  UpRow, L"%c", BOXDRAW_DOWN_RIGHT);

  PrintAt (LeftCol,  UpRow + 2, L"%c", BOXDRAW_VERTICAL_RIGHT);
  PrintAt (RightCol, UpRow + 2, L"%c", BOXDRAW_VERTICAL_LEFT);

  PrintAt ((mMaxColumn - (StrSize (LFC_DEBUG_PAGE_TITLE_STRING) / sizeof (CHAR16))) / 2, UpRow + 1, L"%s", LFC_DEBUG_PAGE_TITLE_STRING);

  Color = EFI_TEXT_ATTR (EFI_LIGHTGRAY, EFI_BLACK);
  gST->ConOut->SetAttribute (gST->ConOut, Color);
  if (!PcdGetBool (PcdLcfcGoldenBiosEnable)) {
    PrintAt ((mMaxColumn - (StrSize (LFC_DEBUG_PAGE_HELP_STRING) / sizeof (CHAR16))) / 2, mMaxRow - 1, L"%s", LFC_DEBUG_PAGE_HELP_STRING);
  }
}

VOID
DrawLfcDebugPageItmeArea (
  IN  UINTN     DialogColumn,
  IN  UINTN     DialogRow,
  IN  UINTN     MaxSelection
  )
{
  UINTN                         Index;
//UINTN                         ShutdownIdIndex;
  UINTN                         NormalColor;
  UINTN                         HighLightColor;
  UINTN                         LeftCol;
  UINTN                         UpRow;
  EFI_STATUS                    Status = EFI_SUCCESS;
  CHAR16                        *PlatformTypeString = NULL;
  CHAR16                        *TempBuffer = NULL;
  CHAR16                        *EcPostString = NULL;
  UINTN                         IndexPostCode;
  UINT8                         CheckUmaOrDis;

  LeftCol = (mMaxColumn - DialogColumn) / 2;
  UpRow = (mMaxRow - DialogRow) /2 ;
  NormalColor = EFI_TEXT_ATTR (EFI_LIGHTGRAY, EFI_BLUE);
  HighLightColor = EFI_TEXT_ATTR (EFI_LIGHTGRAY, EFI_CYAN);

  gST->ConOut->SetAttribute (gST->ConOut, NormalColor);

  for (Index = 0; Index < MaxSelection; Index++) {
    switch(Index) {
      case DebugPagePlatformType:
        //
        //  Get Platform Type
        //
        PlatformTypeString = AllocateZeroPool (100);
        {
          OemSvcLfcGetBoardID(GPU_ID, &CheckUmaOrDis);
        }

        if (!EFI_ERROR (Status)) {
          if (CheckUmaOrDis == GPU_ID_UMA_ONLY) {
            PlatformTypeString = L"UMA";
          }
          else if (CheckUmaOrDis == GPU_ID_DIS_NVIDIA) {
            PlatformTypeString = L"Discrete Nvidia";
          }
          else if (CheckUmaOrDis == GPU_ID_DIS_AMD) {
            PlatformTypeString = L"Discrete AMD";
          }
          else if (CheckUmaOrDis == GPU_ID_DIS_INTEL) {
            PlatformTypeString = L"Discrete Intel";
        }
        }
        else {
          PlatformTypeString = L"Error";
        }

        PrintAt (LeftCol + 1, UpRow + 3 + Index, L"%s%s", mOptionStringList[Index], PlatformTypeString);
        
        gBS->FreePool(PlatformTypeString);
        break;

      case DebugPagePostCode:
        //
        //  Show Dumped Debug Code.
        //
        EcPostString = AllocateZeroPool (100);
        TempBuffer = AllocateZeroPool (100);

        for(IndexPostCode = 0; IndexPostCode < EC_POSTCODE_MAX_LENGTH; IndexPostCode ++) {    
          EfiValueToHexStr (TempBuffer, gLfcPostCode[IndexPostCode], PREFIX_ZERO, 2);
//[-start-210517-KEBIN00001-modify]//
          StrCatS (EcPostString, 100/sizeof(CHAR16), TempBuffer);
          StrCatS (EcPostString, 100/sizeof(CHAR16), L"h ");
//[-end-210517-KEBIN00001-modify]//
        }
        
        //
        // Display EC post code
        //
        PrintAt (LeftCol + 1, UpRow + 3 + Index, L"%s%s", mOptionStringList[Index], EcPostString);

        gBS->FreePool(EcPostString);
        gBS->FreePool(TempBuffer);
        break;

      case DebugPageWakeSource:
        //
        //  Show Wake Source.
        //
        PrintAt (LeftCol + 1, UpRow + 3 + Index, L"%s%02Xh%s", mOptionStringList[Index], gWakeSource, mWakeSourceStringList[gWakeSource]);
        break;

      case DebugPageShutDownId:
//        //
//        //  Show ShutdownId.
//        //
//        TempBuffer = LFC_SHUTDOWN_ID_UNKNOW_STRING;
//        for (ShutdownIdIndex = 0; ShutdownIdIndex < sizeof (ShutdownIdMapArryStd) / sizeof (ShutdownIdMapArryStd[0]); ShutdownIdIndex++) {
//          if (ShutdownIdMapArryStd[ShutdownIdIndex].ShutdownId == gLfcShutdownId) {
//            TempBuffer = ShutdownIdMapArryStd[ShutdownIdIndex].ShutdownIdString;
//            break;
//          }
//        }
//
//        PrintAt (LeftCol + 1, UpRow + 3 + Index, L"%s%02Xh%s", mOptionStringList[Index], gLfcShutdownId, TempBuffer);
//
//        gBS->FreePool(TempBuffer);
        break;

      case DebugPageMfgMode:
        if (gMfgModeFlag) {
          PrintAt (LeftCol + 1, UpRow + 3 + Index, L"%s%s", mOptionStringList[Index], L"Enabled ");
          break;
        }

      default:
        PrintAt (LeftCol + 1, UpRow + 3 + Index, L"%s", mOptionStringList[Index]);
        break;
    }
  }
}


//
// due to h2offt loaded at hte end of bds, so add a flag change here to indicate develop engineer need skip flash EC this time
// 1st time boot here, change to another value
// 2nd time boot here, change to a invalid value
//

VOID
HandleFlashEcOneTime (
  )
{

  UINT8              DataH, DataL;
  UINT8              InputData=0x55;
  UINT8              SecondData=0xAA;
  UINT8              DefaultData=0;

  IoWrite8(LFC_CMOS_INDEX, LFC_FLASH_EC_ONCE_H_INDEX);
  DataH = IoRead8(LFC_CMOS_DATA);
  IoWrite8(LFC_CMOS_INDEX, LFC_FLASH_EC_ONCE_L_INDEX);
  DataL = IoRead8(LFC_CMOS_DATA);

  if((InputData == DataH) && (InputData == DataH) ){
     IoWrite8 (LFC_CMOS_INDEX, LFC_FLASH_EC_ONCE_H_INDEX);
     IoWrite8 (LFC_CMOS_DATA, SecondData);
     IoWrite8 (LFC_CMOS_INDEX, LFC_FLASH_EC_ONCE_L_INDEX);
     IoWrite8 (LFC_CMOS_DATA, SecondData);
  } else  if((SecondData == DataH) && (SecondData == DataH) ){
     IoWrite8 (LFC_CMOS_INDEX, LFC_FLASH_EC_ONCE_H_INDEX);
     IoWrite8 (LFC_CMOS_DATA, DefaultData);
     IoWrite8 (LFC_CMOS_INDEX, LFC_FLASH_EC_ONCE_L_INDEX);
     IoWrite8 (LFC_CMOS_DATA, DefaultData);
  }

  return;
}


EFI_STATUS
ShowLfcDebugPage (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  EFI_STATUS              Status = EFI_UNSUPPORTED;
  UINT8                   Flag =  0;
  UINT32                  FlagDataSize = 1;
  LENOVO_VARIABLE_PROTOCOL*LenovoVariable;
  EFI_GUID                gLvarMfgModeFlagGuid = LVAR_MFG_MODE_FLAG_GUID;
  EFI_INPUT_KEY           Key;
  UINTN                   OldAttribute, OldColumn, OldRow;
  UINTN                   DialogColumn;
  UINTN                   DialogRow;
  UINTN                   MaxSelection;
  UINTN                   Size;
  BOOLEAN                 mShowHiddenPage;
  BOOLEAN                 mShowHiddenPageFlag;
  BOOLEAN                 mFndPressedFlag;

  HandleFlashEcOneTime();

  //
  // If L"cE!" had been created by Fn + D last time. Remove L"cE!" and hidden the page.
  //
  //
  // Get Fn + D Pressed Flag.  
  //
  Size = sizeof (mFndPressedFlag);
  Status = gRT->GetVariable (
                  LFC_FND_PRESSED_FLAG,
                  &gLfcVariableGuid,
                  NULL,
                  &Size,
                  (VOID *)&mFndPressedFlag
                  );

  if (!EFI_ERROR (Status)) {
    gEcDebugFlag = TRUE;

    Status = gRT->SetVariable (
                    LFC_FND_PRESSED_FLAG,
                    &gLfcVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    0,
                    (VOID *)&mFndPressedFlag
                    );

    Size = sizeof (gLfcPostCode);

    Status = gRT->GetVariable (
                    LFC_POST_CODE_DATA,
                    &gLfcVariableGuid,
                    NULL,
                    &Size,
                    (VOID *)&gLfcPostCode
                    );

    Size = sizeof (gWakeSource);

    Status = gRT->GetVariable (
                    LFC_WAKE_SOURCE_DATA,
                    &gLfcVariableGuid,
                    NULL,
                    &Size,
                    (VOID *)&gWakeSource
                    );

//    Status = gRT->GetVariable (
//                    LFC_SHUTDOWN_ID_DATA,
//                    &gLfcVariableGuid,
//                    NULL,
//                    &Size,
//                    (VOID *)&gLfcShutdownId
//                    );
  }

  if (!gEcDebugFlag) {
    Size = sizeof (mShowHiddenPageFlag);
    mShowHiddenPageFlag = FALSE;
    Status = gRT->GetVariable (
                    LFC_HIDDEN_PAGE_FLAG,
                    &gLfcVariableGuid,
                    NULL,
                    &Size,
                    (VOID *)&mShowHiddenPageFlag
                    );
    //
    // If the Hidden page is opened by debug page last time, close it.
    //
    if (!EFI_ERROR (Status)) {
      Status = gRT->SetVariable (
                      LFC_HIDDEN_PAGE_FLAG,
                      &gLfcVariableGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                      0,
                      (VOID *)&mShowHiddenPageFlag
                      );

      mShowHiddenPage = FALSE;
  
      Status = gRT->SetVariable (
                      L"cE!",
                      &gLfcVariableGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                      0,
                      (VOID *)&mShowHiddenPage
                      );
    }

    return EFI_SUCCESS;
  }
	
  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable) ;
  if (EFI_ERROR (Status) ) {
    return Status;
  }
  Status = LenovoVariable->GetVariable (LenovoVariable, &gLvarMfgModeFlagGuid, &FlagDataSize, (VOID*) (&Flag)) ;
  if (EFI_ERROR (Status) && (Status != EFI_NOT_FOUND)){
    return Status;
  }
  
  if (Flag == 'Y') {
    gMfgModeFlag = TRUE;
  }

  //
  //  Init environment variable
  //
  gST->ConOut->QueryMode (
                gST->ConOut,
                gST->ConOut->Mode->Mode,
                &mMaxColumn,
                &mMaxRow
                );

  //
  //  Show Lfc Debug Page if pressed Fn+D during last Boot.
  //
  MaxSelection = 9;

  if (gMfgModeFlag) {
    MaxSelection = 11;
  }

  DialogColumn = LFC_DEBUG_PAGE_STRING_MAX_LENGTH;
  DialogRow = MaxSelection + 2;

  gST->ConOut->EnableCursor (gST->ConOut, FALSE);
  gST->ConOut->ClearScreen(gST->ConOut);
  OldAttribute = gST->ConOut->Mode->Attribute;
  OldColumn = gST->ConOut->Mode->CursorColumn;
  OldRow = gST->ConOut->Mode->CursorRow;

  //
  // Draw Lfc Debug Page dialog
  //
  DrawLfcDebugPageDialog (DialogColumn, DialogRow);
  DrawLfcDebugPageItmeArea (DialogColumn, DialogRow, MaxSelection);

  //
  // Process input
  //
  do {
    Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
    if (!EFI_ERROR (Status)) {
      if (Key.ScanCode == SCAN_F2) {
        if (!PcdGetBool (PcdLcfcGoldenBiosEnable)) {
          mShowHiddenPage = TRUE;
          
          Status = gRT->SetVariable (
                          L"cE!",
                          &gLfcVariableGuid,
                          EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                          sizeof (mShowHiddenPage),
                          (VOID *)&mShowHiddenPage
                          );
          //
          // Create the flag to hide the Hidden Page for normal boot next time, and keep it will not conflict with other method.  
          // 
          mShowHiddenPageFlag = TRUE;
          Status = gRT->SetVariable (
                          LFC_HIDDEN_PAGE_FLAG,
                          &gLfcVariableGuid,
                          EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                          sizeof (mShowHiddenPageFlag),
                          (VOID *)&mShowHiddenPageFlag
                          );
        }
        break;
      }
      else {
        if(Key.ScanCode == SCAN_ESC) {
          break;
        }
      }
    }
  } while (TRUE);

  gST->ConOut->SetAttribute (gST->ConOut, OldAttribute);
  gST->ConOut->SetCursorPosition (gST->ConOut, OldColumn, OldRow);

  Status = EFI_SUCCESS;

  gST->ConOut->ClearScreen(gST->ConOut);

  return Status;
}

EFI_STATUS
InitialLfcDebugPage (
  VOID
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  BOOLEAN                 DisableFastBootFlag = 1;
  BOOLEAN                 mFndPressedFlag = TRUE;
  
  Status = LfcEcLibCheckEcDebugFlag (KBC_CMD_STATE, KBC_DATA, &gEcDebugFlag);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (gEcDebugFlag) {
    //
    // Dump EC post code
    //
    Status = LfcEcLibDumpEcPostCode (KBC_CMD_STATE, KBC_DATA, gLfcPostCode);
    
    if (EFI_ERROR (Status)) {
      return Status;
    }

    //
    // Get the wake source data
    //
    IoWrite8(LFC_CMOS_INDEX, LFC_LAST_WAKE_SRC_INDEX);
    gWakeSource = IoRead8(LFC_CMOS_DATA);
    //
    // Disable Fast boot by one time flag.  
    //       
    Status = gRT->SetVariable (
                    L"OneTimeDisableFastBoot",
                    &gLfcVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    sizeof (DisableFastBootFlag),
                    &DisableFastBootFlag
                    );
    //
    // Save Fn + D Pressed Flag into variable.  
    //    
    Status = gRT->SetVariable (
                    LFC_FND_PRESSED_FLAG,
                    &gLfcVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    sizeof (mFndPressedFlag),
                    (VOID *)&mFndPressedFlag
                    );

    //
    // Save Post code data into variable.  
    //  
    Status = gRT->SetVariable (
                    LFC_POST_CODE_DATA,
                    &gLfcVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    sizeof (gLfcPostCode),
                    (VOID *)&gLfcPostCode
                    );

    //
    // Save wake source data into variable.  
    //  
    Status = gRT->SetVariable (
                    LFC_WAKE_SOURCE_DATA,
                    &gLfcVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    sizeof (gWakeSource),
                    (VOID *)&gWakeSource
                    );

//    //
//    // Get and Save Shutdown ID, the EC will force to reset the system.
//    //
//    GetAndSaveShutdownID ();

    //
    //Reset start system for Hot key can't work after fast boot and S4 Resume.
    //
    gST->RuntimeServices->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
  }
  
  return Status;
}

EFI_STATUS
DebugPageDxeEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
)
/*++
Routine Description:

Arguments:

  None

Returns:

  None
--*/
{
  EFI_STATUS              Status = EFI_SUCCESS;
//#[-start-220414-dennis0017-Modify]//
#if defined(C770_SUPPORT) || defined(C970_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT)

#else

  VOID                    *StartOfBdsDiagnosticsEventRegistration = NULL;
  EFI_EVENT               StartOfBdsDiagnosticsEvent = NULL;

  InitialLfcDebugPage ();
  
  Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    ShowLfcDebugPage,
                    NULL,
                    &StartOfBdsDiagnosticsEvent
                    );
  if (EFI_ERROR (Status)) {
    return EFI_ABORTED;
  }

  Status = gBS->RegisterProtocolNotify (
                  &gEfiStartOfBdsDiagnosticsProtocolGuid,
                  StartOfBdsDiagnosticsEvent,
                  &StartOfBdsDiagnosticsEventRegistration
                  );
  if (EFI_ERROR (Status)) {
    return EFI_ABORTED;
  }
#endif

//#[-end-220414-dennis0017-Modify]//

  return Status;
}
