 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/

#include "GetEcShutdownID.h"

//
// wait EC output buffer full
//
VOID 
WaitEcObf (
  VOID
  )
{
  while (!(IoRead8(EC_STATUS_PORT) & EC_OBF));
}

//
// wait EC input buffer empty
//
VOID 
WaitEcIbe (
  VOID
  )
{
  while (IoRead8(EC_STATUS_PORT) & EC_IBF);
}


VOID 
SendEcCmd (
  UINT8 EcCmd
  )
{
  WaitEcIbe ();

  IoWrite8 (EC_CMD_PORT, EcCmd);

  WaitEcIbe ();
}

UINT8 
ReadEcData (
  VOID
  )
{
  WaitEcObf ();
  return IoRead8(EC_DATA_PORT);
}

VOID 
WaitKbObe (
  VOID
  )
{
  while (IoRead8 (KB_STATUS_PORT) & KB_OBF) {
    IoRead8 (KB_DATA_PORT);
  }
}

//
// wait KB input buffer empty
//
VOID 
WaitKbIbe (
  VOID
  )
{
  while (IoRead8(KB_STATUS_PORT) & KB_IBF);
}


VOID 
SendKbCmd (
  UINT8 KBCmd
  )
{
  WaitKbObe ();
  WaitKbIbe ();
  IoWrite8 (KB_CMD_PORT, KBCmd);
  WaitKbIbe ();
}

VOID 
SendEcData (
  UINT8 EcData
  )
{
  WaitEcIbe ();	
  IoWrite8 (EC_DATA_PORT, EcData);	
  WaitEcIbe ();
}

VOID 
EnterFollowMode (
  VOID
  )
{
  SendEcCmd (0x01);
}

VOID 
ExitFollowMode (
  VOID
  )
{
  SendEcCmd (0x05);
}

VOID 
SendSPICommand ( 
  UINT8 Cmd
  )
{
  SendEcCmd (0x02);
  SendEcCmd (Cmd);
}

VOID 
SendSPIByte (
  UINT8 Index
  )
{
  SendEcCmd (0x03);
  SendEcCmd (Index);
}

UINT8 
BReadByteFromSPI (
  VOID
  )
{
  SendEcCmd (0x04);
  return ReadEcData();
}

VOID 
WaitSPIFree (
  VOID
  )
{
  EnterFollowMode ();
  SendSPICommand (SPICMD_READ_STATUS);

  while (1) {
    if((BReadByteFromSPI ()&0x01) == 0x00) {
      break;
    } 
  }
  ExitFollowMode();
}


VOID 
SpiWriteDisable (
  VOID
  )
{
  WaitSPIFree ();
  EnterFollowMode ();
  SendSPICommand (SPICMD_WRDI);

  EnterFollowMode ();
  SendSPICommand (SPICMD_READ_STATUS);

  while(1) {
    if((BReadByteFromSPI()&0x02 )== 0x00){
      break;
    } 
  }
  ExitFollowMode ();
}

VOID 
SpiRead1KByte (
  UINT8  BlockNum,
  UINT8* ReadBuf
  )
{
  UINT32  AddrInBlock = 0;
  UINT32  BlockAddr = 0;

  BlockAddr = BlockNum * BLOCK_SIZE_1K;

  SpiWriteDisable ();

  WaitSPIFree ();
  EnterFollowMode ();

  SendSPICommand (SPICMD_HIGH_SPEED_READ);
  SendSPIByte ((UINT8)(((BlockAddr>>16)&0xFF)));  // Addr2
  SendSPIByte ((UINT8)(((BlockAddr>>8)&0xFF)));   // Addr1
  SendSPIByte ((UINT8)(((BlockAddr)&0xFF)));      // fast read dummy byte
  SendSPIByte (0x00);

  for (AddrInBlock = 0x00; AddrInBlock < BLOCK_SIZE_1K; AddrInBlock++) {
    ReadBuf[AddrInBlock] = BReadByteFromSPI ();
  }


  ExitFollowMode ();
  WaitSPIFree ();
}

EFI_STATUS 
ReadShutdownId1K (
  UINT32  ShutdownIdAddr,
  UINT8*  ReadBuf
  )
{
  UINT8  BlockNum = 0;

  BlockNum = (UINT8) (ShutdownIdAddr / 1024);

  SpiRead1KByte (BlockNum, ReadBuf);

  return EFI_SUCCESS;
}

UINT32 GetShutdownIdAddr(
  VOID
  )
{
  UINT32  ShutdownIdAddr = 0;

  //
  // Shutdown Id address format: 0xAABB00
  //
 
  //
  // Get shutdown Id address high 8 bit (AA).
  //
  SendEcCmd (0x52);
  SendEcData (0xA6);
  ShutdownIdAddr = ReadEcData () <<16;

  //
  // Get shutdown Id address low 8 bit (BB).
  //
  SendEcCmd (0x52);
  SendEcData (0xA7);
  ShutdownIdAddr = ShutdownIdAddr | (ReadEcData () <<8);
  
  return ShutdownIdAddr;
}
