 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/

#ifndef _LFC_DEBUG_PAGE_DXE_H_
#define _LFC_DEBUG_PAGE_DXE_H_

#include <Protocol/StartOfBdsDiagnostics.h>
#include <Library/IoLib.h>
#include <Library/BaseLib.h>
#include <Library/LfcEcLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/OemGraphicsLib.h>
#include <Library/BadgingSupportLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Lfc.h>
#include <LfcCmos.h>
#include <Guid/LfcVariableGuid.h>
#include <Protocol/LfcNvsArea.h>
#include <Protocol/LenovoVariable.h> 

typedef enum {
  DebugPagePlatformType = 1,
  DebugPagePostCode     = 3,
  DebugPageWakeSource   = 5,
  DebugPageShutDownId   = 7,
  DebugPageMfgMode      = 9,
} DEBUG_PAGE_OPTION;

typedef enum {
  DebugPageEnterSetupKey,
  DebugPageAnyOtherKey,
} DEBUG_PAGE_KEY_OPTION;

//typedef struct {
//  UINT8         ShutdownId;   
//  CHAR16*       ShutdownIdString;
//} SHUTDOWNID_DESC;

#define MAX_STRING_LENGTH                       100
#define LFC_DEBUG_PAGE_STRING_MAX_LENGTH        58

#define LFC_DEBUG_PAGE_TITLE_STRING             L"LCFC Debug Page Information"
#define LFC_DEBUG_PAGE_PLATFORM_TYPE_STRING     L"  Platform Type : "
#define LFC_DEBUG_PAGE_BLANK_STRING             L"                  "
#define LFC_DEBUG_PAGE_POST_CODE_STRING         L"  Post Code     : "
#define LFC_DEBUG_PAGE_WAKE_SOURCE_STRING       L"  Wake Source   : "
#define LFC_DEBUG_PAGE_SHUTDOWN_ID_STRING       L"  Shutdwon ID   : "
#define LFC_DEBUG_PAGE_MFG_MODE_STRING          L"  MFG Mode      : "
#define LFC_DEBUG_PAGE_HELP_STRING              L"  Press F2 key, it will Enter into BIOS Setup and show Hidden Page. Press ESC key, it will normal boot.  "

#define LFC_WAKE_BY_UNKNOWN_STRING              L" (WAKE_BY_UNKNOWN) "
#define LFC_WAKE_BY_POWER_BTN_STRING            L" (WAKE_BY_POWER_BTN) "
#define LFC_WAKE_BY_NOVO_BTN_STRING             L" (WAKE_BY_NOVO_BTN) "
#define LFC_WAKE_BY_POWER_BTN_S4S5_STRING       L" (WAKE_BY_POWER_BTN_S4S5) "
#define LFC_WAKE_BY_RTC_S4S5_STRING             L" (WAKE_BY_RTC_S4S5) "
#define LFC_WAKE_BY_POWER_BTN_S3_STRING         L" (WAKE_BY_POWER_BTN_S3) "
#define LFC_WAKE_BY_RTC_S3_STRING               L" (WAKE_BY_RTC_S3) "
#define LFC_WAKE_BY_PCIE_S3_STRING              L" (WAKE_BY_PCIE_S3) "
#define LFC_WAKE_BY_PME_S3_STRING               L" (WAKE_BY_PME_S3) "
#define LFC_WAKE_BY_EC_WAKE_LID_STRING          L" (WAKE_BY_EC_WAKE_LID) "

//#define LFC_SHUTDOWN_ID_UNKNOW_STRING           L" (Unknow) "
//#define LFC_SHUTDOWN_ID_S5_STRING               L" (SB SLP_S5) "                                      // 0x01
//#define LFC_SHUTDOWN_ID_COMMAND_STRING          L" (Command C:0x59 D:0xA2) "                          // 0x04
//#define LFC_SHUTDOWN_ID_FLASH_STRING            L" (Flash) "                                          // 0x05
//#define LFC_SHUTDOWN_ID_COLDBOOT_STRING         L" (EC power reset) "                                 // 0x06
//#define LFC_SHUTDOWN_ID_BOOTFAIL_STRING         L" (Boot fail restart by command C:0x40 D:0xXX) "     // 0x08
//#define LFC_SHUTDOWN_ID_BATTOVERTEMP_STRING     L" (Battery over temp) "                              // 0x0A
//#define LFC_SHUTDOWN_ID_THERMALCOMMFAIL_STRING  L" (Thermal sensor communication fail) "              // 0x0B
//#define LFC_SHUTDOWN_ID_BATTEMPTY_STRING        L" (Battery empty) "                                  // 0x0C
//#define LFC_SHUTDOWN_ID_BATTCOMMFAIL_STRING     L" (Battery communication fail) "                     // 0x0E
//#define LFC_SHUTDOWN_ID_BATTABNORMAL_STRING     L" (Battery abnormal device ID) "                     // 0x0F
//#define LFC_SHUTDOWN_ID_DTSOVERTEMP_STRING      L" (DTS over temp) "                                  // 0x11
//#define LFC_SHUTDOWN_ID_VGAOVERTEMP_STRING      L" (VGA thermal sensor over temp) "                   // 0x12
//#define LFC_SHUTDOWN_ID_BATTLOWVOLT_STRING      L" (Battery low voltage) "                            // 0x13
//#define LFC_SHUTDOWN_ID_EXTVGAOVERTEMP_STRING   L" (External VGA thermal sensor over temp) "          // 0x1C
//#define LFC_SHUTDOWN_ID_VGACOMMFAIL_STRING      L" (VGA thermal sensor communication fail) "          // 0x20
//#define LFC_SHUTDOWN_ID_EXTVGACOMMFAIL_STRING   L" (External VGA thermal sensor communication fail) " // 0x21
//#define LFC_SHUTDOWN_ID_CPUCOMMFAIL_STRING      L" (CPU thermal sensor communication fail) "          // 0x22
//#define LFC_SHUTDOWN_ID_TICLOCALOVERTEPM_STRING L" (Thermal sensor local over temp) "                 // 0x30
//#define LFC_SHUTDOWN_ID_TICREMOTOVERTEPM_STRING L" (Thermal sensor remote over temp) "                // 0x31
//#define LFC_SHUTDOWN_ID_TNTC_V_OVER_TEPM_STRING L" (Thermal sensor over temp) "                       // 0x32
//#define LFC_SHUTDOWN_ID_CMISTOR_OVERTEMP_STRING L" (CPU thermistor over Temp) "                       // 0x33
//#define LFC_SHUTDOWN_ID_GMISTOR_OVERTEMP_STRING L" (GPU thermistor over Temp) "                       // 0x34
//#define LFC_SHUTDOWN_ID_RSOC_1PtO0P_STRING      L" (Battery is 0p in DC mode) "                       // 0x40
//#define LFC_SHUTDOWN_ID_SHIP_MODE_STRING        L" (Ship Mode) "                                      // 0xA6, 0xA7, 0xA8
//#define LFC_SHUTDOWN_ID_EC_RESET0_INFO_STRING   L" (EC cold Reset) "                                  // 0xEC
//#define LFC_SHUTDOWN_ID_EC_RESET1_INFO_STRING   L" (EC VSTBY or WRST reset) "                         // 0xFC
//#define LFC_SHUTDOWN_ID_EC_RESET2_INFO_STRING   L" (8S Reset) "                                       // 0xFD
//#define LFC_SHUTDOWN_ID_EC_RESET3_INFO_STRING   L" (Internal watch-dog) "                             // 0xFE
//#define LFC_SHUTDOWN_ID_EC_RESET4_INFO_STRING   L" (External watch-dog) "                             // 0xFF

#define LFC_HIDDEN_PAGE_FLAG  L"!Fn+D"
#define LFC_FND_PRESSED_FLAG  L"!&Fn+D"
#define LFC_POST_CODE_DATA    L"!&PostCode"
#define LFC_WAKE_SOURCE_DATA  L"!&WakeSource"
//#define LFC_SHUTDOWN_ID_DATA  L"!&ShutdownId"

#endif
