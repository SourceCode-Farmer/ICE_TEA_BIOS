 /*****************************************************************************
 *
 *
 * Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
 * And/or its affiliates. All rights reserved. 
 * Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 * Use is subject to license terms.
 * 
 *****************************************************************************/

#ifndef _EFI_SHELL_EC_READ_H_
#define _EFI_SHELL_EC_READ_H_

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>

#define BLOCK_SIZE_1K           (1 * 1024)

#define EC_STATUS_PORT          0x6C
#define EC_CMD_PORT             0x6C
#define EC_DATA_PORT            0x68
#define EC_OBF                  0x01
#define EC_IBF                  0x02

#define KB_STATUS_PORT          0x64
#define KB_CMD_PORT             0x64
#define KB_DATA_PORT            0x60
#define KB_OBF                  0x01
#define KB_IBF                  0x02

#define SPICMD_WRSR             0x01 // Write Status Register
#define SPICMD_WRDI             0x04 // Write disable
#define SPICMD_READ_STATUS      0x05 // Read Status Register
#define SPICMD_HIGH_SPEED_READ  0x0B // High-Speed Read


VOID 
WaitEcObf (
  VOID
  );

  
 VOID 
WaitEcIbe (
  VOID
  );

VOID 
SendEcCmd (
  UINT8 EcCmd
  );

VOID 
SendEcData (
  UINT8 EcData
  );

UINT8 
ReadEcData (
  VOID
  );
  
VOID 
WaitKbObe (
  VOID
  );

VOID 
WaitKbIbe (
  VOID
    );
    
VOID 
SendKbCmd (
  UINT8 KBCmd
  );
  
VOID 
EnterFollowMode (
  VOID
  );
  
VOID 
ExitFollowMode (
  VOID
  );
  
VOID 
SendSPICommand ( 
  UINT8 Cmd
  );
  
VOID 
SendSPIByte (
  UINT8 Index
  );
  
UINT8 
BReadByteFromSPI (
  VOID
  );
  
VOID 
WaitSPIFree (
  VOID
  );
  
VOID 
SpiWriteDisable (
  VOID
  );
  
VOID 
SpiRead1KByte (
  UINT8  BlockNum,
  UINT8* ReadBuf
  );
  
EFI_STATUS 
ReadShutdownId1K (
  UINT32  ShutdownIdAddr,
  UINT8*  ReadBuf
  );

UINT32 GetShutdownIdAddr(
  VOID
  );
 
#endif //_EFI_SHELL_EC_READ_H_
