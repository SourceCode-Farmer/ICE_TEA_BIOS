 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2021, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 /*++
Abstract:
  This driver is used LFC WMI Service, it will public BIOS WMI SSDT table, register
software SMI handler for dealing with the request by APP.
 
History:
  Date          Name          Version    Change Notes
  2021.02.23    Steven Wang   v1.00      Initial release

Module Name:
  LfcWmiServiceSmm.c
 --*/

#ifndef __LFC_WMI_SERVICE_SMM_H__
#define __LFC_WMI_SERVICE_SMM_H__

//---------------------------------------------------------------------------
// Header Files 
//---------------------------------------------------------------------------
#include <PiDxe.h>
#include <IndustryStandard/Acpi.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/AcpiTable.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Uefi/UefiSpec.h>
#include <Library/LfcEcLib.h>
//---------------------------------------------------------------------------
// Structure Definition 
//---------------------------------------------------------------------------
#pragma pack(1)
typedef struct {
  UINT32 Signature;
  UINT32 Cmd;
  UINT32 SubCmd;
  UINT32 ReturnCode;
  UINT32 Data1;
  UINT32 Data2;
  UINT32 Data3;
  UINT32 Data4;
  UINT8  BlockData[0x100];
} LFC_WMI_NVS;

typedef
EFI_STATUS
( EFIAPI *LFC_WMI_FUNCTION ) (
  IN OUT LFC_WMI_NVS *LfcWmiNvs
  );

typedef struct {
  LFC_WMI_FUNCTION Function;
} LFC_WMI_SMI_HANDLER;

typedef struct {
  UINT8                  OpRegionOp;
  UINT32                 NameString;
  UINT8                  RegionSpace;
  UINT8                  DWordPrefix;
  UINT32                 RegionOffset;
  UINT8                  BytePrefix;
  UINT8                  RegionLen;
} AML_OP_REGION_32_8;

typedef struct {
  UINT8                  OpRegionOp;
  UINT32                 NameString;
  UINT8                  RegionSpace;
  UINT8                  DWordPrefix;
  UINT32                 RegionOffset;
  UINT8                  BytePrefix;
  UINT16                 RegionLen;
} AML_OP_REGION_32_16;
#pragma pack()

//---------------------------------------------------------------------------
// Marco Definition 
//---------------------------------------------------------------------------
// WMI status code
#define WMI_SUCCESS                                  0
#define WMI_UNSUPPORTED                              0x80000001
#define WMI_INVALID_PARAMETER                        0x80000002
#define WMI_ACCESS_DENIED                            0x80000003
#define WMI_SYSTEM_BUSY                              0x80000004
#define WMI_DATA_SUCCESS                             0x55AA55AA

// SMI Command
//[-start-211223-kebin000079-modify]//
//#define SMM_LFC_WMI_SERVICE_SMI                      0xCC
#define SMM_LFC_WMI_SERVICE_SMI                      0xF0
//[-end-211223-kebin000079-modify]//


//---------------------------------------------------------------------------
// Functions
//---------------------------------------------------------------------------
EFI_STATUS
LfcWmiEcServiceHandler (
  IN OUT LFC_WMI_NVS *LfcWmiNvs
  );

#endif  // __LFC_WMI_SERVICE_SMM_H__
