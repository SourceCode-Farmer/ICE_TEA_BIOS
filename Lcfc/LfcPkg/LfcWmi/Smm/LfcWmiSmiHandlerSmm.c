 //*****************************************************************************
 //
 //
 // Copyright (c) 2012 - 2021, Hefei LCFC Information Technology Co.Ltd. 
 // And/or its affiliates. All rights reserved. 
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
 // Use is subject to license terms.
 // 
 //******************************************************************************
 /*++
Abstract:
  SMI handler service for EC access and BIOS services.
 
History:
  Date          Name          Version    Change Notes
  2021.02.23    Steven Wang   v1.00      Initial release

Module Name:
  LfcWmiSmiHandlerSmm.c
 --*/

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Header Files 
//---------------------------------------------------------------------------
#include "LfcWmiServiceSmm.h"
#include <Library/BaseMemoryLib.h> //CopyMem, ZeroMem


//---------------------------------------------------------------------------
// Functions
//---------------------------------------------------------------------------
EFI_STATUS
EcBatteryCtrl (
  IN OUT LFC_WMI_NVS *Nvs
  )
{
  EFI_STATUS                       Status = EFI_SUCCESS;

  //Data1 = function; Data2 = enable/disable; Data3 = EC command extend value 
  Status = LNVEcBatterySetting((UINT8)Nvs->Data1,(UINT8)Nvs->Data2,(UINT8)Nvs->Data3);
  
  Nvs->Data1      = (UINT8)(Status);
  Nvs->Cmd        = Nvs->Cmd;
  Nvs->SubCmd     = Nvs->SubCmd;
  Nvs->ReturnCode = WMI_SUCCESS;
  return Status;
}

//
// LFC Common WMI SMI interface table
//
static LFC_WMI_SMI_HANDLER mLfcWmiEcToolsSmiHandlers[] = {
  EcBatteryCtrl         // 00: Battery Control
  
};

EFI_STATUS
LfcWmiEcServiceHandler (
  IN OUT LFC_WMI_NVS *LfcWmiNvs
  )
{
  if (LfcWmiNvs->SubCmd < sizeof(mLfcWmiEcToolsSmiHandlers)/sizeof(LFC_WMI_SMI_HANDLER)) {
    return mLfcWmiEcToolsSmiHandlers[LfcWmiNvs->SubCmd].Function (LfcWmiNvs);
  }

  LfcWmiNvs->ReturnCode = WMI_UNSUPPORTED;
  return EFI_SUCCESS;
}
