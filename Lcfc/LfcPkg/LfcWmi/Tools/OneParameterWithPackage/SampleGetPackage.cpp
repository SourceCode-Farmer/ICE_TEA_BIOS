//*****************************************************************************
// Copyright (c) 2012 - 2018, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.         
//******************************************************************************

/*++
Abstract:
This sample application is used for execute WMI method Lfc_Tools_Wmi_Service->GetDataUlongAndPackage
with one output parameter.

History:
Date          Name          Version    Change Notes
2018.11.16    Steven Wang   V1.00      Initial release.

Module Name:
SampleGetPackage.cpp
*/
#include "stdafx.h"
//#define _WIN32_DCOM
#include <iostream>
using namespace std;
#include <comdef.h>
#include <Wbemidl.h>
#include <stdlib.h>  // for wcstombs
#include <wctype.h>  // for towlower
#include <wchar.h>   // for wchar_t

#pragma comment(lib, "wbemuuid.lib")

int main()
{
	HRESULT hres;
	bool STATUS = FALSE;
	
	// Step 1: --------------------------------------------------
	// Initialize COM. ------------------------------------------

	hres = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hres))
	{
		printf("Failed to initialize COM library. Error code = 0x%x\n", hres);
		return FALSE;                  // Program has failed.
	}

	// Step 2: --------------------------------------------------
	// Set general COM security levels --------------------------

	hres = CoInitializeSecurity(
		NULL,
		-1,                          // COM authentication
		NULL,                        // Authentication services
		NULL,                        // Reserved
		RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication 
		RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation  
		NULL,                        // Authentication info
		EOAC_NONE,                   // Additional capabilities 
		NULL                         // Reserved
	);


	if (FAILED(hres))
	{
		printf("Failed to initialize security. Error code = 0x%x\n",hres);
		CoUninitialize();
		return FALSE;                    // Program has failed.
	}

	// Step 3: ---------------------------------------------------
	// Obtain the initial locator to WMI -------------------------

	IWbemLocator *pLoc = NULL;

	hres = CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID *)&pLoc);

	if (FAILED(hres))
	{
		printf("Failed to create IWbemLocator object.Err code = 0x%x\n", hres);
		CoUninitialize();
		return FALSE;                 // Program has failed.
	}

	// Step 4: -----------------------------------------------------
	// Connect to WMI through the IWbemLocator::ConnectServer method

	IWbemServices *pSvc = NULL;

	// Connect to the root\cimv2 namespace with
	// the current user and obtain pointer pSvc
	// to make IWbemServices calls.
	hres = pLoc->ConnectServer(
		_bstr_t(L"ROOT\\WMI"), // Object path of WMI namespace
		NULL,                    // User name. NULL = current user
		NULL,                    // User password. NULL = current
		0,                       // Locale. NULL indicates current
		NULL,                    // Security flags.
		0,                       // Authority (for example, Kerberos)
		0,                       // Context object 
		&pSvc                    // pointer to IWbemServices proxy
	);

	if (FAILED(hres))
	{
		printf("Could not connect. Error code = 0x%x\n",hres);
		pLoc->Release();
		CoUninitialize();
		return FALSE;                // Program has failed.
	}

	// Step 5: --------------------------------------------------
	// Set security levels on the proxy -------------------------

	hres = CoSetProxyBlanket(
		pSvc,                        // Indicates the proxy to set
		RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
		RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
		NULL,                        // Server principal name 
		RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
		RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
		NULL,                        // client identity
		EOAC_NONE                    // proxy capabilities 
	);

	if (FAILED(hres))
	{
		printf("Could not set proxy blanket. Error code = 0x%x\n",hres);
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return FALSE;               // Program has failed.
	}

  CComPtr<IWbemClassObject>	pClassObj = NULL;
	CComPtr<IWbemClassObject>	pInClass = NULL;
	CComPtr<IWbemClassObject>	pInParam = NULL;
  CComPtr<IWbemClassObject>	pOutClass = NULL;
	CComPtr<IWbemClassObject>	pOutParam = NULL;
	CComPtr<IWbemClassObject> PackageObj = NULL;
	
	hres = pSvc->GetObject(L"Lfc_Tools_Wmi_Service", 0, NULL, &pClassObj, NULL);
	if (FAILED(hres)) {
	  printf("Failed to get Lfc_Tools_Wmi_Service object error: %x\n", hres);
	  goto ErrorExit;
  }

  hres = pClassObj->GetMethod(L"GetDataUlongAndPackage", 0, &pInClass, NULL);
  if (WBEM_S_NO_ERROR == hres)
  {
    printf("WMI:Get GetDataUlongAndPackage method successfully\n");
    if(pInClass != NULL)
		{
		#if 0
	  {
	    //TODO: Check if have input paramter, if yes, need put parameter here
	    //	create instance copy
		   if(WBEM_S_NO_ERROR == (hres=pInClass->SpawnInstance(0, &pInParamA)) )
		   {
		   	// set each property
		     hres = pInParamA->Put("parameter", 0, &SafeArrayData, 0);
		     if (WBEM_S_NO_ERROR != hres)
		     {
		     	DBG_PRINTF_FL((L"WMI:ExecMethodWMI put var failed! error=0x%x\n", hres));
		       //	Put failed, check the properties and their types
		     }
		   }
		   else
		   {
		   	DBG_PRINTF_FL((L"WMI:ExecMethodWMI SpawnInstance failed! error=0x%x\n", hres));
	    }
	  }
	  #endif
	  }
		CComPtr<IWbemClassObject>	ppiWmiOut;
		hres = pSvc->ExecMethod(L"Lfc_Tools_Wmi_Service.InstanceName='ACPI\\PNP0C14\\WM00_0'",L"GetDataUlongAndPackage",0, NULL, pInParam, &ppiWmiOut, NULL);
		if (WBEM_S_NO_ERROR == hres) 
		{
		  printf("WMI:ExecMethodWMI GetDataUlongAndPackage Success! error=0x%x\n", hres);
		}
    CComVariant varData;
    hres = ppiWmiOut->Get(L"return", 0, &varData, NULL,0);
		if (WBEM_S_NO_ERROR != hres) 
		{
		  printf("WMI:ExecMethodWMI Get Data of GetDataUlongAndPackage failed! error=0x%x\n", hres);
		}
		
    if (varData.vt != VT_UNKNOWN) 
    {
		  goto ErrorExit;
    }
    
    CComVariant vVarOCData;
    IUnknown *Package = NULL;
		Package = (IUnknown*)varData.punkVal;
		
		hres = Package->QueryInterface(IID_IWbemClassObject, reinterpret_cast<void**>(&PackageObj));
		
		if (FAILED(hres)) 
		{
		  goto ErrorExit;
		}

		hres = PackageObj->Get(L"Bytes", 0, &vVarOCData, NULL, 0);
		if (FAILED(hres)) 
		{
		  goto ErrorExit;
		}
		
		BYTE * Buffer = NULL;
		BYTE *ocdata = NULL;
		LONG cElements = 0;
		LONG lUBound = 0;
		LONG lLBound = 0;
		SafeArrayGetLBound(vVarOCData.parray, 1, &lLBound);
		SafeArrayGetUBound(vVarOCData.parray, 1, &lUBound);
		cElements = lUBound - lLBound + 1;
		printf("Get Package data size(%d)\n", cElements);
		
		//must call unaccess data
		SafeArrayAccessData(vVarOCData.parray, (void **)&ocdata);
		if (Buffer == NULL) 
		{
		  Buffer = (BYTE * )malloc(cElements);
		}
	  CopyMemory(Buffer, ocdata, cElements);
	  SafeArrayUnaccessData(vVarOCData.parray);
   
	  UINT8 Count = 0;
	  
	  for(Count =0; Count <=cElements; Count++) 
	  {
      printf("Buffer[%x] = 0x%x \n",Count,*(Buffer + Count));
    }
    if (Buffer) {
		  free(Buffer);
  	}
  }
  // ========
	// Cleanup
	// ========
ErrorExit:
	pSvc->Release();
	pLoc->Release();
  return FALSE;
}

