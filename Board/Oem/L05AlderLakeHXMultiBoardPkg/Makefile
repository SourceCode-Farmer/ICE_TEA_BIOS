## @file
#  Project Makefile
#
#******************************************************************************
#* Copyright (c) 2018 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

#
# Build mode customization
# -q: Quiet mode   - disable all messages except fatal errors, module based messages.
#                    this is default build mode if BUILD_MODE is not specified
# -s: Silent mode  - file based messages
# -v: Verbose mode - Turn on verbose output with informational messages printed
#
#BUILD_MODE     = -s

#
# Build report customization
# Below is the default build report setting if BUILD_REPORT is not specified

SYMBOLIC_BUILD = -D SYMBOLIC_DEBUG=YES
HG_BUILD       = -D HYBRID_GRAPHICS_SUPPORT=YES -D NVIDIA_OPTIMUS_SUPPORT=YES -D AMD_POWERXPRESS_SUPPORT=YES -D HG_ASLCODE_FOR_WPT_LYNXPOINTLP=YES
VPRO_BUILD     = -D TXT_SUPPORT=YES -D PRODUCTION_SIGNED_ACM=YES
OC_BUILD       = -D OVERCLOCK_ENABLE=YES
SW_GUARD_BUILD = -D SOFTWARE_GUARD_ENABLE=YES
PEI_CRISIS_RECOVERY_BUILD = -D USE_FAST_CRISIS_RECOVERY=NO
NO_CRB_EC_BUILD = -D USE_INTEL_CRB_H8_EC=NO

#####################################################################
# Support Build Options function in InQuire tool for H2OABT.
#
BOOT_GUARD_BUILD = -D BOOT_GUARD_SUPPORT=YES -D BOOT_GUARD_SINGLE_KEY=YES
TEXT_BUILD       = --pcd gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported=TRUE --pcd gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported=FALSE
BIOS_GUARD_BUILD = -D BIOS_GUARD_SUPPORT=YES -D SECURE_FLASH_SUPPORT=YES
AMT_BUILD        = -D AMT_ENABLE=YES

!ifndef PROJECT_BUILD_MODE
PROJECT_BUILD_MODE =

#@Prompt TextMode:description about what the TextMode is.
!ifdef TextMode
PROJECT_BUILD_MODE = $(PROJECT_BUILD_MODE) $(TEXT_BUILD)
!endif

#@Prompt BootGuard:description about what the BootGuard is.
!ifdef BootGuard
PROJECT_BUILD_MODE = $(PROJECT_BUILD_MODE) $(BOOT_GUARD_BUILD)
!endif

#@Prompt BiosGuard:description about what the BootGuard is.
!ifdef BiosGuard
PROJECT_BUILD_MODE = $(PROJECT_BUILD_MODE) $(BIOS_GUARD_BUILD)
!endif

#@Prompt Sg:description about what the Sg is.
!ifdef Sg
PROJECT_BUILD_MODE = $(PROJECT_BUILD_MODE) $(HG_BUILD)
!endif

#@Prompt Amt:description about what the Amt is.
!ifdef Amt
PROJECT_BUILD_MODE = $(PROJECT_BUILD_MODE) $(AMT_BUILD)
!endif
!endif
#####################################################################
!include $(WORKSPACE)\BaseTools\Conf\Makefile

symbolicdebug:
  @$(MAKE) debug BUILD_TARGET=$@ PROJECT_BUILD_OPTIONS="$(SYMBOLIC_BUILD)"
uefi64Sg:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(HG_BUILD)"
uefi64vPro:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(VPRO_BUILD)"
Uefi64oc:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(OC_BUILD)"
Uefi64SwGuard:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(SW_GUARD_BUILD)"
Uefi64PeiCrisisRecovery:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(PEI_CRISIS_RECOVERY_BUILD)"

## @BuildTarget "IA32 X64", RELEASE, Generate FSP release build binary
Uefi64FspR: Uefi64FspClean
  buildfsp /r

## @BuildTarget "IA32 X64", DEBUG, Generate FSP efidebug build binary
Uefi64FspD: Uefi64FspClean
  buildfsp /d

## @BuildTarget "IA32 X64", DEBUG, Generate FSP DDT build binary
Uefi64FspDdt: Uefi64FspClean
  buildfsp /ddt

## @BuildTarget "IA32 X64", RELEASE, Generate FSP Performance dump build binary
Uefi64FspPerf: Uefi64FspClean
  buildfsp /perf

## @BuildTarget "IA32 X64", Clean FSP build folder
Uefi64FspClean:
  buildfsp /clean

Uefi64NotPrePcd:
  $(MAKE) release BUILD_TARGET=$@ BUILD_MODE="$(BUILD_MODE)"

#####################################################################
# Support Build Options function in InQuire tool for H2OABT.
#
RVS_STANDARD_BUILD = -D BOOT_GUARD_SUPPORT=YES -D USE_FAST_CRISIS_RECOVERY=YES -D PTT_SUPPORT=YES

Uefi64BootGuard:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(BOOT_GUARD_BUILD)"
Uefi64Text:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(TEXT_BUILD)"
uefi64NoCrbEc:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(NO_CRB_EC_BUILD)"
Uefi64RvsStandard:
  @$(MAKE) release BUILD_TARGET=$@ BUILD_TARGET_OPTIONS="$(RVS_STANDARD_BUILD)"
  
#####################################################################  