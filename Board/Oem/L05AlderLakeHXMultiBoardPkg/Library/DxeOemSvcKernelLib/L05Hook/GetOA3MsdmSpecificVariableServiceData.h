/** @file
  Function for Get OA3 MSDM Specific Variable Service Data

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GET_OA3_MSDM_SPECIFIC_VARIABLE_SERVICE_DATA_H_
#define _GET_OA3_MSDM_SPECIFIC_VARIABLE_SERVICE_DATA_H_

EFI_STATUS
GetOA3MsdmSpecificVariableServiceData (
  IN OUT EFI_ACPI_MSDM_DATA_STRUCTURE   *MsdmData
  );
#endif
