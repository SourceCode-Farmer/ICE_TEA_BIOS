/** @file
  Provide OEM to customize the Hardware Signature.

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcKernelLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
//_Start_L05_ACPI_HARDWARE_SIGNATURE_
#include "L05Hook/UpdateHardwareSignature.h"
//_End_L05_ACPI_HARDWARE_SIGNATURE_

/**
  To determine Hardware Signature by oem.

  @param[in,out]  HardwareSignature  Value of Hardware Signature.

  @retval      EFI_SUCCESS           The hardware signature has already been updated by this 
                                     function. The Kernel should not update the hardware 
                                     signature.
  @retval      EFI_MEDIA_CHANGED     Hardware Signature was updated by this function. Then 
                                     Kernel will update the FACS with the returned Hardware 
                                     Signature.
  @retval      EFI_UNSUPPORTED       The hardware signature was not updated by this function.
**/
EFI_STATUS
OemSvcUpdateAcpiFacsHardwareSignature (
  IN OUT  UINT32                  *HardwareSignature
)
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  EFI_STATUS                       Status;
  UINTN                            DataSize = 0;
  UINT8                            *Data;
  UINT8                            Value = 0;

//_Start_L05_ACPI_HARDWARE_SIGNATURE_
#ifdef L05_ALL_FEATURE_ENABLE
  UpdateHardwareSignature (HardwareSignature);
#endif
//_End_L05_ACPI_HARDWARE_SIGNATURE_

  Status = gRT->GetVariable (
                  L"FullReset",
                  &gEfiGenericVariableGuid,
                  NULL,
                  &DataSize,
                  NULL
                  );

  if (Status == EFI_BUFFER_TOO_SMALL) {
    Data = AllocateZeroPool (DataSize);
    ASSERT (Data != NULL);
        
    if (Data == NULL) {
      return EFI_OUT_OF_RESOURCES;
    } else {
      Status = gRT->GetVariable (
                      L"FullReset",
                      &gEfiGenericVariableGuid,
                      NULL,
                      &DataSize,
                      Data
                      );

      if (!EFI_ERROR(Status)) {
        if (*Data != 0) {
          *HardwareSignature += (UINT32)*Data;
          return EFI_MEDIA_CHANGED;
        } 
      }
    }
  } else if (Status == EFI_NOT_FOUND) {
    //
    // set FullReset variable
    //
    Status = gRT->SetVariable (
                    L"FullReset",
                    &gEfiGenericVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    sizeof (UINT8),
                    &Value
                    );
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  return EFI_UNSUPPORTED;
}
