/** @file
  Provide OEM to modifying each MISC BIOS record.

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcKernelLib.h>
#include <OemSmbiosDefineType.h>

#include <Library/BaseLib.h>
#include <Library/CommonPciLib.h>
#include <Library/PciExpressLib.h>
#include <Protocol/Smbios.h>
#include <IndustryStandard/SmBios.h>
#include <Guid/DataHubRecords.h>
//#include <Guid/SetupVariableHob.h>
#include <Library/HobLib.h>
#include <MeBiosPayloadHob.h>
#include <MeBiosPayloadData.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MeTypeLib.h>
#include <KernelSetupConfig.h>
#include <Register/Msr.h>
//[-start-180502-IB11270199-add]//
#include <Protocol/AmtWrapperProtocol.h>
//[-end-180502-IB11270199-add]//
#include <Register/PchRegs.h>
#include <Register/PchRegsLpc.h>
#include <Register/GbeRegs.h>
#include <Register/PchPcieRpRegs.h>
//[-start-190613-IB16990059-add]//
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
//[-end-190613-IB16990059-add]//
// gino: for PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORTS define >>
//#include <PchPcieRpInfo.h>
// gino: for PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORTS define <<
//[-start-191225-IB16740000-add]// for PCI_FUNCTION_NUMBER_PCH_LPC define
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
#include <TrustedDeviceSetup.h>
//[-start-210901-IB118770038-add]//
// RC 2347 removed TrustedDeviceSetupMainProtocol.h from TrustedDeviceSetup.h
#include <Protocol/TrustedDeviceSetupMainProtocol.h>
//[-start-210901-IB118770038-add]//

STATIC
VOID
SetSlotStatus (
  IN OUT  SMBIOS_TABLE_TYPE9    *Type9
  )
{
  UINT8          Bus;
  UINT8          Device;
  UINT8          Function;
  UINT32         PcieCapIdAddress;
  UINT8          PcieCapIdOffset;
  UINT16         PcieCapCapReg;
  UINT16         SlotStatus;
  EFI_STATUS     Status;

  Bus       = Type9->BusNum;
  Device    = Type9->DevFuncNum >> 3;
  Function  = Type9->DevFuncNum & 0x07;

  Type9->CurrentUsage = SlotUsageAvailable;
  Status = PciFindCapId (Bus, Device, Function, EFI_PCI_CAPABILITY_ID_PCIEXP, &PcieCapIdOffset);

  if (!EFI_ERROR(Status)) {
    PcieCapIdAddress = PCI_EXPRESS_LIB_ADDRESS (Bus, Device, Function, PcieCapIdOffset);
    PcieCapCapReg  = PciExpressRead16 (PcieCapIdAddress + 0x2);
    PciExpressWrite16 (PcieCapIdAddress + 0x2, PcieCapCapReg | BIT8);
    SlotStatus = PciExpressRead16 (PcieCapIdAddress + 0x1A);

    if (((SlotStatus >> 6 ) & BIT0) == 1) {
      Type9->CurrentUsage = SlotUsageInUse;
    }

    PciExpressWrite16 (PcieCapIdAddress + 0x2, PcieCapCapReg);
  }

  return;
}
VOID
EFIAPI
MebxVersionReadyToBootEvent (
  IN  EFI_EVENT    Event,
  IN  VOID         *Context
  )
{
  EFI_STATUS                        Status;
  MEBX_PROTOCOL                     *MebxInfo;


  EFI_SMBIOS_HANDLE                 SmbiosHandle;
  EFI_SMBIOS_PROTOCOL               *Smbios;
  EFI_SMBIOS_TABLE_HEADER           *Record;
  EFI_OEM_AMT_VPRO_TABLE            *GetType131;
  EFI_OEM_AMT_VPRO_TABLE            *SetType131 = NULL;


  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;

  Status = gBS->LocateProtocol (
                        &gEfiSmbiosProtocolGuid,
                        NULL,
                        (VOID **) &Smbios
                        );
  ASSERT_EFI_ERROR (Status);


  if (!EFI_ERROR (Status)) {

    //
    // DO WHILE: Search Vpro Type 131 handle
    //
    do {
      Status = Smbios->GetNext (Smbios, &SmbiosHandle, NULL, &Record, NULL);
      if (EFI_ERROR (Status)) {
        break;
      }

      if (Record->Type == EFI_SMBIOS_OEM_DEFINE_TYPE_AMT_VPRO) {

        //
        // Locate Mebx protocol to get MebxVersion
        //
        Status = gBS->LocateProtocol (&gMebxProtocolGuid, NULL, (VOID **) &MebxInfo);
        if (!EFI_ERROR (Status)) {

          //
          // Procedure:
          //   1. Once get the Mebx Info, create new SMBIOS structure
          //   2. Copy every record from original SMBIOS structure into new SMBIOS.
          //   3. Remove original SMBIOS structure by using Smbios->Remove().
          //   4. Append new SMBIOS structure by using Smbios->Add()
          //   ** (Add and remove) is standard procedure, because it will trigger smbios to re-construct whole table.
          //
          SetType131 = (EFI_OEM_AMT_VPRO_TABLE *) AllocateZeroPool (sizeof (EFI_OEM_AMT_VPRO_TABLE) + 2);
          if (SetType131 == NULL) {
            Status = EFI_OUT_OF_RESOURCES;
            ASSERT_EFI_ERROR (Status);
            return;
          }          
          GetType131 = (EFI_OEM_AMT_VPRO_TABLE *) Record;

          SetType131->Hdr.Type           = GetType131->Hdr.Type;
          SetType131->Hdr.Length         = GetType131->Hdr.Length;
          SetType131->Hdr.Handle         = GetType131->Hdr.Handle;

          SetType131->AmtCpuCapability   = GetType131->AmtCpuCapability;

          //
          // Update Mebx version number from MebxInfo.
          //
          SetType131->AmtMebxVersion[0]  = (UINT16)(UINTN) MebxInfo->MebxVersion.Major;
          SetType131->AmtMebxVersion[1]  = (UINT16)(UINTN) MebxInfo->MebxVersion.Minor;
          SetType131->AmtMebxVersion[2]  = (UINT16)(UINTN) MebxInfo->MebxVersion.Hotfix;
          SetType131->AmtMebxVersion[3]  = (UINT16)(UINTN) MebxInfo->MebxVersion.Build;

          SetType131->AmtPchCapability   = GetType131->AmtPchCapability;

          SetType131->AmtMeCapability[0] = GetType131->AmtMeCapability[0];
          SetType131->AmtMeCapability[1] = GetType131->AmtMeCapability[1];
          SetType131->AmtMeCapability[2] = GetType131->AmtMeCapability[2];

          SetType131->ATConfig           = GetType131->ATConfig;
          SetType131->Reserved2          = GetType131->Reserved2;
          SetType131->Reserved4          = GetType131->Reserved4;

          SetType131->AmtNetworkDevice[0] = GetType131->AmtNetworkDevice[0];
          SetType131->AmtNetworkDevice[1] = GetType131->AmtNetworkDevice[1];
          SetType131->AmtNetworkDevice[2] = GetType131->AmtNetworkDevice[2];

          SetType131->AmtBIOSCapability  = GetType131->AmtBIOSCapability;
          SetType131->AmtVproSignature   = GetType131->AmtVproSignature;
          SetType131->Reserved3          = GetType131->Reserved3;

          Status = Smbios->Remove (Smbios, SmbiosHandle);
          if (!EFI_ERROR (Status)) {
            Status = Smbios->Add (Smbios, NULL, &SmbiosHandle, (EFI_SMBIOS_TABLE_HEADER *) SetType131);
          }
          FreePool (SetType131);
        }
        break;
      }
    } while (1);
  }
}


VOID
UpdateAmtCpuCapability (
  UINT32 *CpuCapabilities
  )
{
  UINT32                            Ecx;
  MSR_IA32_FEATURE_CONTROL_REGISTER Msr;

  *CpuCapabilities = 0;
  Msr.Uint64 = AsmReadMsr64 (MSR_IA32_FEATURE_CONTROL);
  AsmCpuid (
          1,
          NULL,
          NULL,
          &Ecx,
          NULL
          );

  ((CPU_CAP*)CpuCapabilities)->VMXState = (UINT32)(RShiftU64(Msr.Uint64, 2));
  ((CPU_CAP*)CpuCapabilities)->SMXState = (UINT32)(RShiftU64(Msr.Uint64, 1));

  if (Ecx & BIT6) {
    ((CPU_CAP*)CpuCapabilities)->LtTxtCap = 1;
  } else {
    ((CPU_CAP*)CpuCapabilities)->LtTxtCap = 0;
  }

  if ((Msr.Uint64 & TXT_OPT_IN_VMX_AND_SMX_MSR_VALUE) == TXT_OPT_IN_VMX_AND_SMX_MSR_VALUE) {
    ((CPU_CAP*)CpuCapabilities)->LtTxtEnabled = 1;
  } else {
    ((CPU_CAP*)CpuCapabilities)->LtTxtEnabled = 0;
  }

  if (Ecx & BIT5) {
    ((CPU_CAP*)CpuCapabilities)->VTxCap = 1;
  } else {
    ((CPU_CAP*)CpuCapabilities)->VTxCap = 0;
  }
  ((CPU_CAP*)CpuCapabilities)->VTxEnabled = (UINT32)(RShiftU64(Msr.Uint64, 2));
}

VOID
UpdateAmtPchCapability (
  UINT64 *PchCapability
  )
{
  //
  // AmtIchCapability
  // 0:2 bits - Function Number of PCI Device
  // 3:7 bits - Device Number of PCI Device
  // 8:15 bits - Bus Number of PCI Device
  // 16:31 bits - Device Identification Number (DID)
  //              DID will set to 0xff if not found
  // 36:63 bits Reserved and must be set to "0xFF"
  //
  UINT32                  DeviceID;
//[-start-190613-IB16990059-add]//
  UINT64                  BaseAddress;
  *PchCapability = 0;
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                  DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                  DEFAULT_PCI_BUS_NUMBER_PCH,
                  PCI_DEVICE_NUMBER_PCH_LPC,
                  PCI_FUNCTION_NUMBER_PCH_LPC,
                  0
                  );
  DeviceID = PciSegmentRead16 (BaseAddress + PCI_DEVICE_ID_OFFSET);
  *PchCapability  |= (UINT64)((UINTN)(DEFAULT_PCI_BUS_NUMBER_PCH << 8)|(UINTN)(PCI_DEVICE_NUMBER_PCH_LPC << 3)|(UINTN)PCI_FUNCTION_NUMBER_PCH_LPC);
  *PchCapability  |= LShiftU64 (DeviceID, 16);
//[-end-190613-IB16990059-add]//
}

EFI_STATUS
UpdateAmtMeVersionNumber (
  UINT32 *AmtMeVersion,
  UINT32 *AmtMeNumber
  )
{
// 32:47 bits- ME FW Minor Version
// 48:63 bits- ME FW Major Version
// 64:79 bits- ME FW Build Number -- 2 bytes
// 80:95 bits- ME FW Hotfix Number -- 2 bytes

//  EFI_STATUS                        Status;
  UINT16                            Index;
  ME_BIOS_PAYLOAD_HOB               *MbpHob;

  MbpHob                            = NULL;

  *AmtMeVersion = 0;
  *AmtMeNumber = 0;
  Index = 0;
  //
  // Get the MBP Data.
  //
  MbpHob = GetFirstGuidHob (&gMeBiosPayloadHobGuid);
  if (MbpHob == NULL) {
    if (!MeTypeIsSps ()) {
#ifndef POWER_ON_FLAG
      ASSERT (MbpHob != NULL);
#endif
    }
    return EFI_NOT_FOUND;
  }
  *AmtMeVersion = ( ( ( UINT32 )MbpHob->MeBiosPayload.FwVersionName.MajorVersion ) << 16 ) | MbpHob->MeBiosPayload.FwVersionName.MinorVersion;
  *AmtMeNumber  = ( ( ( UINT32 )MbpHob->MeBiosPayload.FwVersionName.HotfixVersion << 16 ) ) | MbpHob->MeBiosPayload.FwVersionName.BuildVersion;

  return EFI_SUCCESS;
}

VOID
UpdateAmtWireDevice (
  UINT32 *AmtWireDevice
  )
{
// Wired NIC
// 0:2 bits - Function Number of PCI Device
// 3:7 bits - Device Number of PCI Device
// 8:15 bits - Bus Number of PCI Device
// 16:31 bits - Device Identification Number (DID)
//              DID will set to 0xff if not found
  UINT16                  DeviceID;
//[-start-190613-IB16990059-add]//
  UINT64                  BaseAddress;
  *AmtWireDevice = 0;
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
				 DEFAULT_PCI_SEGMENT_NUMBER_PCH,
				 DEFAULT_PCI_BUS_NUMBER_PCH,
				 PCI_DEVICE_NUMBER_GBE,
				 PCI_FUNCTION_NUMBER_GBE,
				 0
				 );
  DeviceID = PciSegmentRead16 (BaseAddress + PCI_DEVICE_ID_OFFSET);
  *AmtWireDevice  |= (UINT32)((DEFAULT_PCI_BUS_NUMBER_PCH << 8)|(PCI_DEVICE_NUMBER_GBE << 3)|PCI_FUNCTION_NUMBER_GBE);
  *AmtWireDevice  |= (UINT32)(DeviceID << 16);
//[-end-190613-IB16990059-add]//
}

VOID
UpdateAmtWirelessDevice (
  UINT32 *AmtWirelessDevice
  )
{
// 32:47 bits Reserved
// Wireless NIC
// 48:50 bits - Function Number of PCI Device
// 51:55 bits - Device Number of PCI Device
// 56:63 bits - Bus Number of PCI Device
// 64:70 bits - Device Identification Number (DID)
  UINT16                  DeviceID;
  UINT8                   Index;
  UINT16                  BusTemp;
//[-start-190613-IB16990059-add]//
  UINT64 				  PciRootPortBaseAddress;
  UINT64 				  BaseAddressBus5;
  *AmtWirelessDevice = 0;

  for (Index = 0; Index < GetPchMaxPciePortNum(); Index++) {
    PciRootPortBaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                               DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                               DEFAULT_PCI_BUS_NUMBER_PCH,
                               PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_1,
                               Index,
                               0
                               );
    BusTemp = PciSegmentRead16(PciRootPortBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET);
    PciSegmentWrite16 (PciRootPortBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, 0x0505);

    BaseAddressBus5 = PCI_SEGMENT_LIB_ADDRESS (0, 5, 0, 0, 0);
    if (PciSegmentRead16 (BaseAddressBus5 + PCI_VENDOR_ID_OFFSET) == V_PCH_INTEL_VENDOR_ID) {
      if ((PciSegmentRead8 (BaseAddressBus5 + (PCI_CLASSCODE_OFFSET + 2)) == PCI_CLASS_NETWORK) &&
          (PciSegmentRead8 (BaseAddressBus5 + (PCI_CLASSCODE_OFFSET + 1)) == PCI_CLASS_NETWORK_OTHER)) {
        DeviceID = PciSegmentRead16 (BaseAddressBus5 + PCI_DEVICE_ID_OFFSET);
        *AmtWirelessDevice  |= (UINT32)(5 << 8);
        *AmtWirelessDevice  |= (UINT32)(DeviceID << 16);
        PciSegmentWrite16 (PciRootPortBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, BusTemp);
        break;
      }
    }
    PciSegmentWrite16 (PciRootPortBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, BusTemp);
  }
//[-end-190613-IB16990059-add]//

  if (*AmtWirelessDevice == 0) {
    *AmtWirelessDevice = 0xff000000;
  }
}

EFI_STATUS
UpdateEcRevision (
  SMBIOS_TABLE_TYPE0    *Type0
) {
  if (FeaturePcdGet (PcdUseCrbEcFlag)) {
    if (PcdGetBool (PcdEcPresent)) {
        Type0->EmbeddedControllerFirmwareMajorRelease = PcdGet8(PcdEcMajorRevision);
        Type0->EmbeddedControllerFirmwareMinorRelease = PcdGet8(PcdEcMinorRevision);
        return EFI_MEDIA_CHANGED;
    }
  }
  return EFI_UNSUPPORTED;
}

STATIC
VOID
SetAmtVProTable (
  IN OUT EFI_OEM_AMT_VPRO_TABLE            *Type131
  )
{
  EFI_STATUS                                Status;
  UINT32                                    CpuCapability;
  UINT64                                    PchCapability;
  UINT32                                    AmtWireDevice;
  UINT32                                    AmtWirelessDevice;
  UINT32                                    AmtMeVersion;
  UINT32                                    AmtMeNumber;
  MEBX_PROTOCOL                             *MebxInfo;
  MEFWCAPS_SKU                              MeFwCapsSku;
//[-start-180502-IB11270199-remove]//
//  UINT32                                    MebxSetupVariableAttributes;
//  UINTN                                     MebxSetupVariableDataSize;
//  MEBX_DATA_HOB                             MeBiosExtensionSetup;
//[-end-180502-IB11270199-remove]//
  EFI_EVENT                                 Event;
  ME_BIOS_PAYLOAD_HOB                       *MbpHob;
//[-start-180502-IB11270199-add]//
  AMT_WRAPPER_PROTOCOL                      *AmtWrapper;
//[-end-180502-IB11270199-add]//
  TRUSTED_DEVICE_SETUP_PROTOCOL             *TrustedDeviceSetupProtocol;

  MbpHob              = NULL;
  //
  // Is this the first time through this function?
  //
  Status = gBS->LocateProtocol(&gMebxProtocolGuid, NULL, (VOID **)&MebxInfo);
  if (!EFI_ERROR (Status)) {
    Type131->AmtMebxVersion[0] = (UINT16)(UINTN)MebxInfo->MebxVersion.Major;
    Type131->AmtMebxVersion[1] = (UINT16)(UINTN)MebxInfo->MebxVersion.Minor;
    Type131->AmtMebxVersion[2] = (UINT16)(UINTN)MebxInfo->MebxVersion.Hotfix;
    Type131->AmtMebxVersion[3] = (UINT16)(UINTN)MebxInfo->MebxVersion.Build;
  } else {

    Status = EfiCreateEventReadyToBootEx (
               TPL_NOTIFY,
               MebxVersionReadyToBootEvent,
               NULL,
               &Event
               );
    ASSERT_EFI_ERROR (Status);
  }

  //
  // AmtCpuCapability
  // 6:31 bits Reserved and must be set to "0"
  //
  UpdateAmtCpuCapability (&CpuCapability);
  Type131->AmtCpuCapability = CpuCapability;
  //
  // AmtMchCapability
  //
  UpdateAmtPchCapability (&PchCapability);
  Type131->AmtPchCapability  = PchCapability;
  //
  // AmtMeCapability[3]
  // Byte 0-3 SKU/Capabilities
  // 4:31 bits Reserved and must be set to "0"
  // Byte 4-11 ME FW Version
  // 32:47 bits- ME FW Minor Version
  // 48:63 bits- ME FW Major Version
  // 64:79 bits- ME FW Build Number
  // 80:95 bits- ME FW Hotfix Number
  // If ME is disabled then other data cannot be retrieved
  //
  Type131->AmtMeCapability[0] = (UINT32)(UINTN)0;
  //
  // Get Mbp Data HOB
  //
  MbpHob = GetFirstGuidHob (&gMeBiosPayloadHobGuid);
//[-start-190611-IB16990047-add]//
  if (MbpHob == NULL) {
    DEBUG ((DEBUG_INFO, "No MBP Data Protocol available\n"));
    ASSERT_EFI_ERROR (MbpHob == NULL);
    return;
  }
//[-end-190611-IB16990047-add]//

  Type131->AmtMeCapability[0] |= EFI_AMT_ME_ENABLE;

//
// Set TDS Capability.
//
  Status = gBS->LocateProtocol (&gTrustedDeviceSetupMainProtocolGuid, NULL, (VOID **)&TrustedDeviceSetupProtocol);
  if (!EFI_ERROR (Status)) {
    TrustedDeviceSetupProtocol->SmbiosTrustedDeviceSetupCapabilities ((VOID *)&Type131->AmtBIOSCapability);
    Type131->AmtBIOSCapability = Type131->AmtBIOSCapability | TRUSTED_DEVICE_SETUP_CAPABILITY;
  }
  
  switch (MbpHob->MeBiosPayload.FwPlatType.RuleData.Fields.PlatformBrand) {
    case NoBrand:
      //
      // No Platform Brand, Do Nothing.
      //
      break;

    case IntelAmtBrand:
      //
      // Base on ME BWG 7.2.1.8, the platform is Intel AMT brand,it has the Local Wakeup Timer support in default.
      //
      Type131->AmtMeCapability[0] |= (UINT32)(UINTN)(EFI_AMT_ME_AMT_SUPPORT | EFI_AMT_ME_LWT_SUPPORT);
      break;

    case IntelStandardManageabilityBrand:
      Type131->AmtMeCapability[0] |= (UINT32)(UINTN)(EFI_AMT_ME_STD_SUPPORT);
      break;
//
//    case INTEL_LEVEL_III_MANAGEABILITY_UPGRADE_BRAND:
//      Type131->AmtMeCapability[0] |= (UINT32)(UINTN)(EFI_AMT_ME_UPD_SUPPORT);
//      break;
//  0116 Sam
//    case IntelSmallBusinessTechnologyBrand:
//      Type131->AmtMeCapability[0] |= (UINT32)(UINTN)(EFI_AMT_ME_SBA_SUPPORT);
//      break;

    default:
      DEBUG ((DEBUG_ERROR, "Non-Supported PlatformBrand: %d \n", MbpHob->MeBiosPayload.FwPlatType.RuleData.Fields.PlatformBrand));
      ASSERT(TRUE);
      break;
  }

  MeFwCapsSku.Data = MbpHob->MeBiosPayload.FwCapsSku.FwCapabilities.Data;
//[-start-180502-IB11270199-modify]//
  if (MeFwCapsSku.Fields.KVM == ENABLE) {
    //
    // Check ME-BIOS Sync Data for KVM
    //
    Status = gBS->LocateProtocol(&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);
    if (!EFI_ERROR(Status)) {
      if (AmtWrapper->IsKvmEnabled() == ENABLE) {
        Type131->AmtMeCapability[0]  |= (UINT32)(UINTN)(EFI_AMT_ME_KVM_SUPPORT);
      }
    }
  }
//[-end-180502-IB11270199-modify]//
  Status = UpdateAmtMeVersionNumber (&AmtMeVersion,&AmtMeNumber);
  Type131->AmtMeCapability[1] = AmtMeVersion;
  Type131->AmtMeCapability[2] = AmtMeNumber;

  //
  // AmtNetworkDevice[3]
  // Wired NIC
  // 0:2 bits - Function Number of PCI Device
  // 3:7 bits - Device Number of PCI Device
  // 8:15 bits - Bus Number of PCI Device
  // 16:31 bits - Device Identification Number (DID)
  //              DID will set to 0xff if not found
  // 32:47 bits Reserved
  // Wireless NIC
  // 48:50 bits - Function Number of PCI Device
  // 51:55 bits - Device Number of PCI Device
  // 56:63 bits - Bus Number of PCI Device
  // 64:70 bits - Device Identification Number (DID)
  //              DID will set to 0xff if not found
  // 71:95 bits Reserved
  //
  UpdateAmtWireDevice(&AmtWireDevice);
  UpdateAmtWirelessDevice(&AmtWirelessDevice);
  Type131->AmtNetworkDevice[0]  = AmtWireDevice;
  Type131->AmtNetworkDevice[1]  = ((AmtWirelessDevice & 0x0000FFFF) << 16);
  Type131->AmtNetworkDevice[2]  = ((AmtWirelessDevice & 0xFFFF0000) >> 16);

  return ;
}


/**
  This service will be call by each time add SMBIOS record.
  OEM can modify SMBIOS record in run time.

  Notice the SMBIOS protocol is unusable when call this service.

  @param[in, out]  *RecordBuffer         Each SMBIOS record data.
                                         The max length of this buffer is SMBIOS_TABLE_MAX_LENGTH.

  @retval    EFI_UNSUPPORTED       Returns unsupported by default.
  @retval    EFI_SUCCESS           Don't add this SMBIOS record to system.
  @retval    EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed.
**/
EFI_STATUS
OemSvcDxeUpdateSmbiosRecord (
  IN OUT EFI_SMBIOS_TABLE_HEADER *RecordBuffer
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

EFI_STATUS Status;
  if (RecordBuffer == NULL) {
    return EFI_UNSUPPORTED;
  }

  Status = EFI_UNSUPPORTED;
  switch (RecordBuffer->Type) {
  case EFI_SMBIOS_TYPE_BIOS_INFORMATION:
    Status = UpdateEcRevision((SMBIOS_TABLE_TYPE0 *)RecordBuffer);
    break;
    
  case EFI_SMBIOS_TYPE_SYSTEM_SLOTS:
    SetSlotStatus((SMBIOS_TABLE_TYPE9 *)RecordBuffer);
    Status = EFI_MEDIA_CHANGED;
    break;

  case EFI_SMBIOS_OEM_DEFINE_TYPE_AMT_VPRO:
    SetAmtVProTable ((EFI_OEM_AMT_VPRO_TABLE *)RecordBuffer);
    Status = EFI_MEDIA_CHANGED;
    break;

  default:
    break;
  }

  return Status;
}
