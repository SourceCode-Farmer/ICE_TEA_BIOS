/** @file
  
;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>
#include <FspEas.h>
#include <FspsUpd.h>

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
#include <L05Config.h>
#include <L05BiosSelfHealingConfig.h>
#include <Library/PeiServicesLib.h>  // PeiServicesGetBootMode
#include <Library/BiosSelfHealingLib.h>  // IsManufacturingMode
#endif

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
/**
  BIOS Self-healing update FSPS UPD.

  @param  FspsUpd                       A pointer to the FSPS_UPD structure.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Update FSPS UPD completed.
*/
EFI_STATUS
BiosSelfHealingUpdateFspsUpd (
  IN OUT FSPS_UPD                       *FspsUpd
  )
{
  EFI_STATUS                            Status;
  EFI_BOOT_MODE                         BootMode;
  EFI_L05_EEPROM_MAP_120                *EepromBuffer;
  UINT8                                 BiosSelfHealingFlag;
  BOOLEAN                               DisableLockDown;

  Status              = EFI_SUCCESS;
  EepromBuffer        = NULL;
  BiosSelfHealingFlag = 0;
  DisableLockDown     = FALSE;

  Status = PeiServicesGetBootMode (&BootMode);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Fail to get the current boot mode.\n", __FUNCTION__));
    return EFI_UNSUPPORTED;
  }

  if (BootMode == BOOT_IN_RECOVERY_MODE) {
    DisableLockDown = TRUE;
  }

  EepromBuffer = (EFI_L05_EEPROM_MAP_120 *)(UINTN) PcdGet32 (PcdFlashFvEepromBase);

  if (FspsUpd == NULL || EepromBuffer == NULL) {
    ASSERT (FspsUpd != NULL);
    ASSERT (EepromBuffer != NULL);
    return EFI_UNSUPPORTED;
  }

  BiosSelfHealingFlag = EepromBuffer->BiosSelfHealingFlag[0];

  if (IsManufacturingMode () &&
      ((BiosSelfHealingFlag & L05_BIOS_SELF_HEALING_FLAG_DESTROY_BOOT_BLOCK) == L05_BIOS_SELF_HEALING_FLAG_DESTROY_BOOT_BLOCK ||
      (BiosSelfHealingFlag & L05_BIOS_SELF_HEALING_FLAG_DESTROY_FVMAIN) == L05_BIOS_SELF_HEALING_FLAG_DESTROY_FVMAIN)) {
    DisableLockDown = TRUE;
  }

  if (DisableLockDown) {
    //
    // Only triggered when in recovery mode or manufacturing mode
    //
    DEBUG ((DEBUG_INFO, "Disable InSMM.STS (EISS) and BIOS Lock Enable (BLE) since BIOS Self-Healing related flags are set.\n"));
    FspsUpd->FspsConfig.PchLockDownBiosLock      = FALSE;
    FspsUpd->FspsConfig.PchLockDownBiosInterface = FALSE;
    FspsUpd->FspsConfig.RtcBiosInterfaceLock     = FALSE;
    return EFI_MEDIA_CHANGED;
  }

  return EFI_UNSUPPORTED;
}
#endif

/**
 This function offers an interface to modify FSPS_UPD data before the FSP-S API be called.

 @param[in,out]     FspUpdDataPtr                 A pointer to the UPD data region data strcture address.
                                                  It must convert to FSPS_UPD structure for update FSPS UPD.

                                                  If you set this pointer to NULL, FSP API will use the
                                                  default FSP-S UPD settings in the binary.

 @retval            EFI_UNSUPPORTED               Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED             Alter the Configuration Parameter.
 @retval            EFI_SUCCESS                   The function performs the same operation as caller.
                                                  The caller will skip the specified behavior and assuming
                                                  that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateFspsUpd (
  IN OUT VOID                                     **FspUpdDataPtr
  )
{
  FSPS_UPD                                        *FspsUpd;
//_Start_L05_FEATURE_
  EFI_STATUS                                      Status;
  EFI_STATUS                                      TempStatus;
//_End_L05_FEATURE_

  FspsUpd = (FSPS_UPD *)(*FspUpdDataPtr);
//_Start_L05_FEATURE_
  Status     = EFI_UNSUPPORTED;
  TempStatus = EFI_UNSUPPORTED;
//_End_L05_FEATURE_

  //
  // Set this pointer to NULL, use the default FSP-S UPD settings in the binary.
  // BUGBUG: When set the FspUpdDataPtr to NULL, will cause the system to hang in FSP binary,
  //         still can't use the default UPD data in binary to boot successfully.
  //
//  *FspUpdDataPtr = NULL;

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
  TempStatus = BiosSelfHealingUpdateFspsUpd (FspsUpd);

  if (Status != EFI_MEDIA_CHANGED) {
    Status = TempStatus;
  }
#endif

//_Start_L05_FEATURE_
//  return EFI_UNSUPPORTED;
  return Status;
//_End_L05_FEATURE_
}
