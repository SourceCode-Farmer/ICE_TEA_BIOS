/** @file
  Source file for EFI OEM badging support driver.

;******************************************************************************
;* Copyright (c) 2013 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_LOGO_ENABLE_H
#define _L05_LOGO_ENABLE_H

VOID
L05BadgingdataOverridebyPCD (
  VOID
  );

EFI_STATUS
LogoRegisterReadyToBootBeforeCp (
  VOID
  );

BOOLEAN
EfiL05BgrtLogoCheck (
  VOID
  );

BOOLEAN
L05PostLogoIgnoreCheck (
  VOID
  );
#endif
