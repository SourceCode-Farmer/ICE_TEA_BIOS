/** @file
  Determine if "Boot with no change" is true according to project characteristic.

;******************************************************************************
;* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DebugLib.h>
#include <Library/PeiOemSvcKernelLib.h>
#include <Library/IoLib.h>
#include <Register/PmcRegs.h>
//[-start-210322-IB16740136-add]// for build error, some definition are defined in TcoRegs.h
#include <Register/TcoRegs.h>
//[-end-210322-IB16740136-add]//
//#include <Library/PchPlatformLib.h>
#include <Library/PchCycleDecodingLib.h>

/**
  Determine if "Boot with no change" is true according to project characteristic.

  @param[in, out]  *IsNoChange        If IsNoChange == TRUE, then boot mode will be set to
                                      BOOT_ASSUMING_NO_CONFIGURATION_CHANGES which might reduce the POST time.

  @retval          EFI_UNSUPPORTED    Returns unsupported by default.
  @retval          EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed.
**/
EFI_STATUS
OemSvcIsBootWithNoChange (
  IN OUT BOOLEAN                        *IsNoChange
  )
{
  UINT16                                TcoBase;
  UINT16                                Tco2Status;
  BOOLEAN                               BoxOpen;
  EFI_STATUS                            Status;

  //
  // Read the ACPI registers
  //
//  TcoBase     = (UINT16) (PcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress) + (UINT16) PCH_TCO_BASE);
  Status = PchTcoBaseGet (&TcoBase);
  if (EFI_ERROR(Status)) {
    return EFI_UNSUPPORTED;
  }
  Tco2Status  = IoRead16 (TcoBase + R_TCO_IO_TCO2_STS);

  //
  // This is the state of the hardware
  //
  BoxOpen = (BOOLEAN) ((Tco2Status & B_TCO_IO_TCO2_STS_INTRD_DET) == 1);
  if (BoxOpen) {
    //
    // Clear the bit for next boot. (reset B_INTRD_DET by writing high)
    //
    Tco2Status |= B_TCO_IO_TCO2_STS_INTRD_DET;
    IoWrite16 (TcoBase + R_TCO_IO_TCO2_STS, Tco2Status);

    //
    // Since it was OPEN, return that cannot be in "no config. change boot"
    //
    DEBUG ((DEBUG_INFO, "Boot with Full configuration\n"));
    *IsNoChange = FALSE;
    return EFI_MEDIA_CHANGED;
  }
  return EFI_UNSUPPORTED;
}
