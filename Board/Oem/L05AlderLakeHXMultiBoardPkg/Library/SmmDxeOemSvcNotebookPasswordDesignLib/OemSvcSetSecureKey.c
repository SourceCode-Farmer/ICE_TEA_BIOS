/** @file
  Provides an interface to Set Secure Key to EC.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

/**
  Provides an interface to Set Secure Key to EC.

  @param  SecureKey                     A pointer to the secure key for UHDP encryption.

  @retval EFI_SUCCESS                   Set Secure Key success.
  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSetSecureKey (
  UINT8                                 *SecureKey
  )
{
  //
  // [Natural File Guard Design Guide V1.03]
  //   2.2.1 Secure Key Generation
  //     Procedure:
  //       1. When enable Natural File Guard or retrieve Secure Key fail, BIOS inform the EC RNG module to
  //          generate a 32 byte secure key or generate by itself. Then store this key in EC space.
  //     Note:
  //       3. EC should keep the Secure Key region during end user EC FW update.
  //

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
