/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

/**
  Provides an opportunity for
  (Lenovo Sepcification: Lenovo China Minimum BIOS Spec 139.pdf - CH.3.4.1 Rules for BIOS CMOS password)


  @param  SystemPasswordType            System Supervisor password or System User password
  @param  DataLength                    Password length
  @param  SystemPasswordBuffer          Password pointer

  @retval EFI_UNSUPPORTED               Returns unsupported for to do default setting
  @retval EFI_MEDIA_CHANGED             change load password buffer data
  @retval EFI_SCUESS                    skip default behavior
**/
EFI_STATUS
OemSvcSetSystemPasswords (
  IN     PASSWORD_TYPE                  SystemPasswordType,
  IN     UINTN                          DataLength,
  IN OUT UINT8                          *SystemPasswordBuffer
  )
{
  EFI_STATUS                            Status;
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  EFI_GUID                              PasswordGuid = {0};
  EFI_L05_VARIABLE_PROTOCOL             *L05VariablePtr;
#else
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;
  UINT8                                 *L05SystemPasswordArea;
  UINTN                                 L05SystemPasswordSize;
#endif

  Status = EFI_SUCCESS;

  //
  // If L05SystemSupervisorPasswordRegionBase or  L05SystemUserPasswordRegionBase != 0,
  // normal behavior is set password to Supervisor/User Password region.
  // Otherwise it will set Password to L05 specific varaible region.
  // It relative with L05_VARIABLE_SERVICE_ENABLE and L05_SPECIFIC_VARIABLE_SERVICE_ENABLE switch
  //
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  L05VariablePtr = NULL;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gEfiL05VariableProtocolGuid, NULL, &L05VariablePtr);

  } else {
    Status = mSmst->SmmLocateProtocol (&gEfiL05VariableProtocolGuid, NULL, &L05VariablePtr);
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  switch (SystemPasswordType) {

  case SystemSupervisor:
    CopyMem (&PasswordGuid, &gL05SystemSuperPasswordGuid, sizeof (EFI_GUID));
    Status = EFI_MEDIA_CHANGED;
    break;

  case SystemUser:
    CopyMem (&PasswordGuid, &gL05SystemUserPasswordGuid, sizeof (EFI_GUID));
    Status = EFI_MEDIA_CHANGED;
    break;

  default:
    return EFI_UNSUPPORTED;
    break;
  }

  if (Status == EFI_MEDIA_CHANGED) {
    Status = L05VariablePtr->SetVariable (
                               L05VariablePtr,
                               &PasswordGuid,
                               (UINT32) DataLength,
                               SystemPasswordBuffer
                               );

    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

#else
  SmmFwb                = NULL;
  L05SystemPasswordArea = NULL;
  L05SystemPasswordSize = 0;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, (VOID **) &SmmFwb);

  } else {
    Status = mSmst->SmmLocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, (VOID **) &SmmFwb);
  }

  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  //
  // Get system password region.
  //
  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get allocated system password area.
  //
  Status = GetSystemPasswordArea (
             &L05SystemPasswordArea,
             &L05SystemPasswordSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Check random number is exist.
  //
  if (!IsBiosPasswordRandomNumberExist ()) {
    return EFI_UNSUPPORTED;
  }

  switch (SystemPasswordType) {

  case SystemSupervisor:
    CopyMem (
      (VOID *) L05SystemPasswordArea,
      (VOID *) SystemPasswordBuffer,
      ((DataLength < L05SystemSupervisorPasswordRegionSize) ? DataLength : L05SystemSupervisorPasswordRegionSize)
      );
    break;

  case SystemUser:
    CopyMem (
      (VOID *) (L05SystemPasswordArea + L05SystemSupervisorPasswordRegionSize),
      (VOID *) (SystemPasswordBuffer),
      ((DataLength < L05SystemUserPasswordRegionSize) ? DataLength : L05SystemUserPasswordRegionSize)
      );
    break;

  default:
    return EFI_UNSUPPORTED;
    break;
  }

  Status = FlashBiosRegion (
             SmmFwb,
             L05SystemSupervisorPasswordRegionBase,
             L05SystemPasswordSize,
             L05SystemPasswordArea
             );

  if (L05SystemPasswordArea != NULL) {
    FreePool (L05SystemPasswordArea);
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  return EFI_MEDIA_CHANGED;
}

