/** @file
  Provide OEM to add some tasks, before entering S5.

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/SmmOemSvcKernelLib.h>
#include <Library/BaseOemSvcKernelLib.h>
#include <Library/DebugLib.h>
//_Start_L05_FLIP_TO_BOOT_
#include <Library/FeatureLib/OemSvcSwitchFlipToBootS4S5.h>
//_End_L05_FLIP_TO_BOOT_

/**
  This service provides OEM to add some tasks, before entering S5.

  @param  Base on OEM design.
  
  @retval EFI_UNSUPPORTED       Returns unsupported by default.
  @retval EFI_SUCCESS           The service is customized in the project.
  @retval EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed. 
  @retval Others                Depends on customization.
**/
EFI_STATUS
OemSvcS5Callback (
  VOID
  )
{
  EFI_STATUS     OemSvcStatus;

//_Start_L05_FLIP_TO_BOOT_
  OemSvcSwitchFlipToBootS4S5 ();
//_End_L05_FLIP_TO_BOOT_

  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcEcAcpiMode \n"));
  OemSvcStatus = OemSvcEcAcpiMode (FALSE);// follow Intel code
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcEcAcpiMode Status: %r\n", OemSvcStatus));

  return EFI_MEDIA_CHANGED;
}
