/** @file
  Feature Hook to execute hard disk spin down

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Protocol/L05HddSpindown.h>
#include <Library/SmmServicesTableLib.h>

/**
  Execute STANDBY_IMMEDIATE command by AtaPassThru protocol

  @param  None

  @retval EFI_SUCCESS                   Success to finish hard disk spin down function
  @retval Others                        Fail to execute hard disk spin down function
**/
EFI_STATUS
EFIAPI
HddSpinDownFunc (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_L05_HDD_SPINDOWN_PROTOCOL         *L05HddSpindownProtocol;

  L05HddSpindownProtocol = NULL;

  Status = gSmst->SmmLocateProtocol (
                    &gEfiL05HddSpindownProtocolGuid,
                    NULL,
                    (VOID **) &L05HddSpindownProtocol
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = L05HddSpindownProtocol->HddSpinDownAllPort ();

  return Status;
}

