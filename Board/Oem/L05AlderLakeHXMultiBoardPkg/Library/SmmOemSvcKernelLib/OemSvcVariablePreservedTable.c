/** @file
  To return the preserved list.

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/SmmOemSvcKernelLib.h>
#include <Guid/H2OSeamlessRecovery.h>
//#include <SgxSetupData.h>
//_Start_L05_BIOS_OPTANE_SUPPORT_
#include <SetupConfig.h>
#include <Guid/L05BackupSetupItemVariable.h>  // EFI_L05_BACKUP_SETUP_ITEM_VARIABLE_GUID
#include <Library/BaseMemoryLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmVariable.h>
//_End_L05_BIOS_OPTANE_SUPPORT_
//_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#include <Guid/L05EspCustomizePostLogoInfoVariable.h>  // EFI_L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_GUID
#include <Guid/L05EspCustomizePostLogoVcmVariable.h>   // EFI_L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_GUID
//_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_

#define PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_GUID      {0xffffffff, 0xffff, 0xffff, { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,0xff, 0xff}}
#define PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_NAME      L"PreservedTable2OtherAllName"
//
// The followings are the sample codes. Please customize here.
//

//
// Don't keep the variable "MeSetupStorage". If ME FW be flashed and it was not, system will confusion
//
PRESERVED_VARIABLE_TABLE_2 mPreservedVariableTable2[] = {
//  VariableGuid    DeleteVariable    VariableName
//  { {0x523260e2, 0x7237, 0x46aa, { 0xbe, 0x3a, 0x20, 0xfb, 0x49, 0xa, 0x4f, 0x3a}}, FALSE, BIOS_GUARD_IOTRAP_DATA_NAME},   // gBiosGuardIotrapDataGuid
  { {0x1DA748E5, 0x3C39, 0x43E6, {0xB7, 0xAB, 0x55, 0xBA, 0xC7, 0x1A, 0xC7, 0xD2}}, FALSE, L"SetupChange"},  // gH2OSetupChangeVariableGuid
//  { {0x45b5acb9, 0x0359, 0x49be, {0xad, 0xb1, 0x49, 0x37, 0x7b, 0xd6, 0x07, 0xf7}}, FALSE, SGX_SETUP_VARIABLE_NAME},  
//  { {0xd69a279b, 0x58eb, 0x45d1, {0xa1, 0x48, 0x77, 0x1b, 0xb9, 0xeb, 0x52, 0x51}}, FALSE, L"EPCSW"},  // gEpcOsDataGuid   
  { {0x7f9102df, 0xe999, 0x4740, {0x80, 0xa6, 0xb2, 0x03, 0x85, 0x12, 0x21, 0x7b}}, FALSE, L"SystemSupervisorPw"}, // gEfiSupervisorPwGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean password when password in stored in variable.
  { {0x8cf3cfd3, 0xd8e2, 0x4e30, {0x83, 0xff, 0xb8, 0x6f, 0x0c, 0x52, 0x2a, 0x5e}}, FALSE, L"SystemUserPw"}, // gEfiUserPwGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean password when password in stored in variable.
  { {0xe83f7603, 0xa8ed, 0x4b9a, {0x8c, 0x16, 0xb0, 0x15, 0x29, 0xf6, 0x4b, 0x4f}}, FALSE, UPDATE_PROGRESS_NAME}, // gH2OSeamlessRecoveryGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean the update progress stored in variable.
  { {0xfa07a1ea, 0x486d, 0x4b5e, {0xb9, 0x4d, 0x8e, 0x06, 0xe6, 0xfa, 0xd9, 0xdb}}, FALSE, BACKUP_SBB_FILE_DIGEST_SHA256_NAME}, // gH2OSeamlessRecoveryDigestGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean the digest stored in variable.
  { {0x5060073d, 0x88c3, 0x421f, {0x82, 0xf8, 0x28, 0x3f, 0x0d, 0x29, 0xed, 0x90}}, FALSE, L"MeCapDigest"}, // gSysFwUpdateDigiestGuid, DeleteVariable = FALSE to keep behavior that flash BIOS doesn't clean the digest stored in variable.
  { {0x5060073d, 0x88c3, 0x421f, {0x82, 0xf8, 0x28, 0x3f, 0x0d, 0x29, 0xed, 0x90}}, FALSE, L"MeCapDigestTemp"}, // gSysFwUpdateDigiestGuid, DeleteVariable = FALSE to keep behavior that flash Me doesn't clean the digest stored in variable.
  { {0xf92b8157, 0xc647, 0x44d7, {0x8d, 0x94, 0x81, 0x7d, 0x18, 0xa2, 0x76, 0xdc}}, FALSE, L"FwUpdateInfo"}, //gSysFwUpdateProgressGuid, DeleteVariable = FALSE to keep behavior that flash Me doesn't clean the digest stored in variable.
//_Start_L05_BIOS_OPTANE_SUPPORT_
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  { EFI_L05_BACKUP_SETUP_ITEM_VARIABLE_GUID, FALSE, L05_BACKUP_SETUP_ITEM_VARIABLE_NAME},          // L05BackupSetupItem
#endif
//_End_L05_BIOS_OPTANE_SUPPORT_
//_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
  { EFI_L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_GUID, FALSE, L05_BIOS_LOGO_DIY_ESP_VARIABLE_NAME},                  // LBLDESP
  { EFI_L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_GUID,  FALSE, L05_BIOS_LOGO_DIY_VERSION_CRC_CONTROL_VARIABLE_NAME},  // LBLDVCC
#endif
//_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_
//_Start_L05_PROJECT_SHIP_SUPPORT_
#ifndef L05_PROJECT_SHIP_SUPPORT_ENABLE
  { PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_GUID, TRUE, PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_NAME},   // Put at here, Other all variable GUID and Other all variable name
#else
  //
  // [ODM request]
  //   After project ship support, keep all Setup item settings (all variable)
  //   which defined in Setup UI while BIOS update.
  //
  // Do not delete any variables.
  //
  { PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_GUID, FALSE, PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_NAME},   // Put at here, Other all variable GUID and Other all variable name
#endif
//_End_L05_PROJECT_SHIP_SUPPORT_
  { {0}, FALSE, NULL}   // End of table
};

//_Start_L05_BIOS_OPTANE_SUPPORT_
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
/**
  This function offers an interface to backup Setup Item.

  @param  None

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
*/
EFI_STATUS
L05BackupSetupItem (
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_VARIABLE_PROTOCOL             *SmmVariable;
  L05_BACKUP_SETUP_ITEM                 L05BackupSetupItem;
  UINTN                                 BufferSize;
  PCH_SETUP                             PchSetup;
  EFI_GUID                              PchSetupVariableGuid = PCH_SETUP_GUID;

  Status      = EFI_SUCCESS;
  SmmVariable = NULL;
  BufferSize  = 0;
  ZeroMem (&L05BackupSetupItem, sizeof (L05_BACKUP_SETUP_ITEM));

  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmVariableProtocolGuid,
                    NULL,
                    &SmmVariable
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  BufferSize = sizeof (PCH_SETUP);
  Status = SmmVariable->SmmGetVariable (
                          PCH_SETUP_VARIABLE_NAME,
                          &PchSetupVariableGuid,
                          NULL,
                          &BufferSize,
                          &PchSetup
                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  L05BackupSetupItem.SataInterfaceMode   = PchSetup.SataInterfaceMode;
  CopyMem (L05BackupSetupItem.RstPcieRemapEnabled, PchSetup.RstPcieRemapEnabled, PCH_MAX_PCIE_ROOT_PORTS);
  L05BackupSetupItem.SataRstOptaneMemory = PchSetup.SataRstOptaneMemory;

  Status = SmmVariable->SmmSetVariable (
                          L05_BACKUP_SETUP_ITEM_VARIABLE_NAME,
                          &gEfiL05BackupSetupItemVariableGuid,
                          (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE),
                          sizeof (L05_BACKUP_SETUP_ITEM),
                          &L05BackupSetupItem
                          );

  return Status;
}
#endif
//_End_L05_BIOS_OPTANE_SUPPORT_

/**
  To return the preserved list.

  @param[in, out]  VariablePreservedTable    Pointer to the table of preserved variables.
  @param[in, out]  IsKeepVariableInList      True: The variables in the table should be preserved.
                                             FALSE: The variables in the table should be deleted.

  @retval          EFI_UNSUPPORTED           Returns unsupported by default.
  @retval          EFI_SUCCESS               The work to delete the variables is completed.
  @retval          EFI_MEDIA_CHANGED         The table of preserved variables is updated.
**/
EFI_STATUS
OemSvcVariablePreservedTable2 (
  IN OUT PRESERVED_VARIABLE_TABLE_2               **VariablePreservedTable2
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  //
  // The followings are the sample codes. Please customize here.
  //

//_Start_L05_BIOS_OPTANE_SUPPORT_
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  L05BackupSetupItem ();
#endif
//_End_L05_BIOS_OPTANE_SUPPORT_

  *VariablePreservedTable2 = mPreservedVariableTable2;
  return EFI_MEDIA_CHANGED;

  //return EFI_UNSUPPORTED;
}

/**
  Old function OemSvcVariablePreservedTable to return the old sturcture (PRESERVED_VARIABLE_TABLE) preserved list.
  The process will launch new function OemSvcVariablePreservedTable2 first,
  and then launch old function OemSvcVariablePreservedTable only when new function OemSvcVariablePreservedTable2
  return EFI_UNSUPPORTED.
  According to logo requirement, should preserve all UEFI variables with VendorGuid
  {77fa9abd-0359-4d32-bd60-28f4e78f784b}, so the those UEFI variables with this VendorGuidVendorGuid
  will not be limited in this table when if table is filled with those special variables to remove in table,
  this table parse process will skip those.

  @param[in, out]  VariablePreservedTable    Pointer to the table of preserved variables.
                                             (old structure PRESERVED_VARIABLE_TABLE)
  @param[in, out]  IsKeepVariableInList      True: The variables in the table should be preserved.
                                             FALSE: The variables in the table should be deleted.

  @retval          EFI_UNSUPPORTED           Returns unsupported by default.
  @retval          EFI_SUCCESS               The work to delete the variables is completed.
  @retval          EFI_MEDIA_CHANGED         The table of preserved variables is updated.
**/
EFI_STATUS
OemSvcVariablePreservedTable (
  IN OUT PRESERVED_VARIABLE_TABLE              **VariablePreservedTable,
  IN OUT BOOLEAN                               *IsKeepVariableInList
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  //
  // The followings are the sample codes. Please customize here.
  //
  //*IsKeepVariableInList = TRUE;
  //*VariablePreservedTable = mPreservedVariableTable;
  //return EFI_MEDIA_CHANGED;

  return EFI_UNSUPPORTED;
}



