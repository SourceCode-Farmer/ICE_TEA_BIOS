/** @file
  Provide an opportunity to get MFG Mode status.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/VariableLib.h>

#ifdef L05_MFG_MODE_SECURE_BOOT_TEST_ON_CRB
// {E1881D74-9239-439f-9F29-800E120A9E11}
#define MFG_MODE_GUID {0xe1881d74, 0x9239, 0x439f, 0x9f, 0x29, 0x80, 0xe, 0x12, 0xa, 0x9e, 0x11}
#endif

/**
  Provide an opportunity to get MFG Mode status.

  @param  IsMfgMode                     TRUE:  Platform is in MFG Mode.
                                        FALSE: Platform is not in MFG Mode.

  @retval EFI_UNSUPPORTED               Feature will use the internal function by default.
  @retval EFI_MEDIA_CHANGED             Feature will refer IsMfgMode flag to update MFG Mode status.
**/
EFI_STATUS
OemSvcGetMfgMode (
  IN OUT BOOLEAN                        *IsMfgMode
  )
{

  //
  // Todo:
  //   Add project specific code in here.
  //
#ifdef L05_MFG_MODE_SECURE_BOOT_TEST_ON_CRB
  EFI_STATUS                            Status;
  EFI_GUID                              MfgModeGuid = MFG_MODE_GUID;
  UINT8                                 MfgMode;
  UINTN                                 BufferSize;

  *IsMfgMode = FALSE;

  BufferSize = sizeof (UINT8);

  Status = CommonGetVariable (
             L"MfgMode",
             &MfgModeGuid,
             &BufferSize,
             &MfgMode
             );

  if (!EFI_ERROR (Status)) {
    *IsMfgMode = (MfgMode == 0x01) ? TRUE : FALSE;
    return EFI_SUCCESS;
  }
#endif

  return EFI_UNSUPPORTED;
}
