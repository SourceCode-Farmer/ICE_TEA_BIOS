## @file
#  Platform Package Description file
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

!import MdePkg/Package.dsc
!import MdeModulePkg/Package.dsc
!import UefiCpuPkg/Package.dsc
!import PerformancePkg/Package.dsc
!import SecurityPkg/Package.dsc
!import PcAtChipsetPkg/Package.dsc
!import UnitTestFrameworkPkg/Package.dsc
!import InsydeOemServicesPkg/Package.dsc
!import InsydeModulePkg/Package.dsc
!import InsydeSetupPkg/Package.dsc
!import InsydeNetworkPkg/Package.dsc
!import FatPkg/Package.dsc
!import ShellPkg/Package.dsc
#[start-130916-IB05670200-add]#
!import InsydeFlashDevicePkg/Package.dsc
#[end-130916-IB05670200-add]#
!import InsydeCrPkg/Package.dsc
!import SioDummyPkg/Package.dsc
!import IntelSiliconPkg/Package.dsc
#[-start-200721-IB17040134-add]#
!import BeepDebugFeaturePkg/Package.dsc
!import PostCodeDebugFeaturePkg/Package.dsc
!import SndwFeaturePkg/Package.dsc
!import MebxFeaturePkg/Package.dsc
#[-end-200721-IB17040134-add]#
!import BluetoothPkg/Package.dsc
!import ClientOneSiliconPkg/Package.dsc
!import MinPlatformPkg/Package.dsc
!import AlderLakePlatSamplePkg/Package.dsc
!import AlderLakeChipsetPkg/Package.dsc
!import AlderLakeBoardPkg/Package.dsc
#_Start_L05_FEATURE_
!import InsydeL05ModulePkg/Package.dsc
!import InsydeL05PlatformPkg/Package.dsc
#_End_L05_FEATURE_

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  PLATFORM_NAME                  = AlderLakeM
  PLATFORM_GUID                  = C197CED3-B91A-4544-8F97-A458CA0AAFDC
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  OUTPUT_DIRECTORY               = Build/$(PROJECT_PKG)
  SUPPORTED_ARCHITECTURES        = IA32|X64
  BUILD_TARGETS                  = DEBUG|RELEASE
  SKUID_IDENTIFIER               = DEFAULT|SkuIdAdlPLp4Rvp|SkuIdAdlPSimics|SkuIdAdlPDdr5Dg384Aep|SkuIdAdlPLp5Rvp|SkuIdAdlPLp4Bep|SkuIdAdlPLp5Aep|SkuIdAdlPDdr5Rvp|SkuIdAdlPDdr4Rvp|SkuIdAdlMLp4Rvp|SkuIdAdlMLp5Rvp|SkuIdAdlMLp5PmicRvp|SkuIdAdlPLp5Dg128Aep|SkuIdAdlPLp5Gcs|SkuIdAdlMLp5Aep|SkuIdAdlPDdr5MRRvp|SkuIdAdlPLp5MbAep|SkuIdAdlPMMAep|SkuIdAdlPT3Lp5Rvp|SkuIdAdlPLp5PpvRVP|SkuIdAdlMLp5Rvp2a|SkuIdAdlMLp5Rvp2aPpv
  FLASH_DEFINITION               = Build/$(PROJECT_PKG)/Project.fdf
  VPD_TOOL_GUID                  = 8C3D856A-9BE6-468E-850A-24F7A8D38E08

  !include $(PROJECT_PKG)/Project.env

#[-start-200917-IB06462159-add]#
DEFINE    PLATFORM_FSP_BIN_PACKAGE        = AlderLakePFspBinPkg
#[-end-200917-IB06462159-add]#

#
# FV & Driver Version
#
DEFINE    WLAN_FW_VERSION                 = 1.2.10.21463
DEFINE    WLAN_DRIVER_VERSION             = 1.2.10.21463
DEFINE    BT_FW_VERSION                   = 3.4.3
DEFINE    BT_DRIVER_VERSION               = 3.4.3
DEFINE    NIFTYROCK_PPAM_VERSION          = 11.22a.5
DEFINE    VMD_UEFI_DRIVER_VERSION         = 19.1.0.1001.5
DEFINE    ACM_ALIGNMENT_ON_FV_BASE        = 256K
DEFINE    SLOT_SIZE                       = 0x3B000
!if $(PRODUCTION_SIGNED_ACM) == YES
DEFINE    CBNT_BIOSAC_ACM_VERSION         = 1.18.07
DEFINE    CBNT_SINIT_ACM_VERSION          = 1.18.04
!else
DEFINE    CBNT_BIOSAC_ACM_VERSION         = 1.18.07
DEFINE    CBNT_SINIT_ACM_VERSION          = 1.18.04
!endif

#_Start_L05_FEATURE_
  !include $(PROJECT_PKG)/OemConfig.env
  !include InsydeL05ModulePkg/Package.env
  !include $(OEM_FEATURE_OVERRIDE_CORE_CODE)/Package.env
#_End_L05_FEATURE_

#================================================================================
#                         L05 Setting Start
#================================================================================
[Defines]
#_Start_L05_SETUP_MENU_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  RFC_LANGUAGES                  = "en-US;zh-CN;"
!endif
#_End_L05_SETUP_MENU_

[PcdsFixedAtBuild]
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  #
  # [Lenovo Notebook Password Design Spec V1.0]
  #   User Input (64chars)
  #
  gInsydeTokenSpaceGuid.PcdDefaultSysPasswordMaxLength|64
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordMaxLength|64
!else
  gInsydeTokenSpaceGuid.PcdDefaultSysPasswordMaxLength|16
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordMaxLength|16
!endif
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor|L"LENOVO"
  gL05ServicesTokenSpaceGuid.PcdL05SwitchableGraphicsSupported | $(HYBRID_GRAPHICS_SUPPORT)

!if $(L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE) == YES
  gPlatformModuleTokenSpaceGuid.PcdNetworkEnable|TRUE
!endif

[PcdsFeatureFlag]
  gInsydeTokenSpaceGuid.PcdH2OBdsOemBadgingSupported|TRUE

!if $(L05_MFG_MODE_SECURE_BOOT_ENABLE) == YES
  gInsydeTokenSpaceGuid.PcdBuildActivatesSecureBoot|TRUE
!endif

!if $(L05_NOTEBOOK_CLOUD_BOOT_ENABLE) == YES
  gInsydeTokenSpaceGuid.PcdH2ONetworkHttpSupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2ONetworkTlsSupported|TRUE
!endif

!if $(L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE) == YES
  gChipsetPkgTokenSpaceGuid.PcdUefiWirelessCnvtEnable|TRUE
!endif

[PcdsDynamicExDefault]
!if $(L05_CUSTOMER_BGRT_LOGO_SUPPORT) == YES
  gL05ServicesTokenSpaceGuid.PcdL05CustomerBgrtLogoEnable |TRUE
  gL05ServicesTokenSpaceGuid.PcdL05CustomerBgrtLogoGuid   |{ \
  0x78, 0x56, 0x34, 0x12, 0x6C, 0x52, 0x6F, 0x43, 0xAE, 0x1A, 0x93, 0xAE, 0x75, 0x7F, 0x3E, 0x36}
#             0x12345678,     0x526C,     0x436F, 0xAE, 0x1A, 0x93, 0xAE, 0x75, 0x7F, 0x3E, 0x36
  gL05ServicesTokenSpaceGuid.PcdL05CustomerBgrtLogoFormat |0x05  # EfiBadgingSupportFormatJPEG
!endif

##================================SMBIOS PCD Setting================================##
  # Please modify this according to actual status
  #      Follow L05 Spec V1.15 3.4.6 SMBIOS,
  #      System BIOS Major Version is always 0 prior to shiping the product.
  #      Always "1" after a products ships unless a major change occurs which then development can increment this number
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type00BIOSMajorRelease | 0
##==================================================================================##

!if $(L05_ACPI_TABLE_ID_ENABLE) == YES
  #
  # OEM Revision setting for ACPI Tables, Must be incremented every time BIOS is released
  #
  gL05ServicesTokenSpaceGuid.PcdL05AcpiTableOemRevision |0x00000001
!endif

  gInsydeTokenSpaceGuid.PcdH2OSataFreezeLockSupported|$(L05_HDD_PASSWORD_ENABLE)

[LibraryClasses]
  BaseOemSvcFeatureLib|$(PROJECT_PKG)/Library/BaseOemSvcFeatureLib/BaseOemSvcFeatureLib.inf

#_Start_L05_SETUP_MENU_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  SetupUtilityLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Library/SetupUtilityLib/SetupUtilityLib.inf
!else
  SetupUtilityLib|InsydeModulePkg/Library/SetupUtilityLib/SetupUtilityLib.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/InsydeModulePkg/Library/SetupUtilityLib
  }
!endif
#_End_L05_SETUP_MENU_

!if $(L05_ALL_FEATURE_ENABLE) == YES
  GenericBdsLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Library/GenericBdsLib/GenericBdsLib.inf
!else
  GenericBdsLib|InsydeModulePkg/Library/GenericBdsLib/GenericBdsLib.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeModulePkg/Library/GenericBdsLib
  }
!endif

#_Start_L05_LOGO_
  OemGraphicsLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Library/OemGraphicsLib/OemGraphicsLib.inf
#_End_L05_LOGO_

[LibraryClasses.common.PEI_CORE]
  PeiOemSvcFeatureLib       |$(PROJECT_PKG)/Library/PeiOemSvcFeatureLib/PeiOemSvcFeatureLib.inf

[LibraryClasses.common.PEIM]
  PeiOemSvcFeatureLib       |$(PROJECT_PKG)/Library/PeiOemSvcFeatureLib/PeiOemSvcFeatureLib.inf

[LibraryClasses.common.DXE_CORE]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcNotebookPasswordDesignLib/SmmDxeOemSvcNotebookPasswordDesignLib.inf
!else
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!endif

[LibraryClasses.common.UEFI_DRIVER]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcNotebookPasswordDesignLib/SmmDxeOemSvcNotebookPasswordDesignLib.inf
!else
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!endif

[LibraryClasses.common.DXE_DRIVER]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcNotebookPasswordDesignLib/SmmDxeOemSvcNotebookPasswordDesignLib.inf
!else
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!endif

[LibraryClasses.common.DXE_SMM_DRIVER]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  SmmOemSvcFeatureLib|$(PROJECT_PKG)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcNotebookPasswordDesignLib/SmmDxeOemSvcNotebookPasswordDesignLib.inf
!else
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!endif

[LibraryClasses.common.COMBINED_SMM_DXE]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  SmmOemSvcFeatureLib|$(PROJECT_PKG)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcNotebookPasswordDesignLib/SmmDxeOemSvcNotebookPasswordDesignLib.inf
!else
  OemSvcSecurityPasswordLib|$(PROJECT_PKG)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!endif

[LibraryClasses.common.SMM_CORE]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  SmmOemSvcFeatureLib|$(PROJECT_PKG)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  DxeOemSvcFeatureLib|$(PROJECT_PKG)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf

[BuildOptions.common.EDKII]
!if $(L05_ALL_FEATURE_ENABLE) == YES
  MSFT:*_*_*_CC_FLAGS     = $(L05_CC_FLAGS)
  MSFT:*_*_*_VFRPP_FLAGS  = $(L05_CC_FLAGS)
  MSFT:*_*_*_ASLPP_FLAGS  = $(L05_CC_FLAGS)
  MSFT:*_*_*_ASLCC_FLAGS  = $(L05_CC_FLAGS)
  MSFT:*_*_*_VFCFPP_FLAGS = $(L05_CC_FLAGS)
!endif

#================================================================================
#                         L05 Setting End
#================================================================================

################################################################################
#
# SKU Identification section - list of all SKU IDs supported by this Platform.
# If cpu dead loop in post code 0x8E (PEI_BOARD_ID_SETUP_FAILED), it should check
# the BoardId value is whether in both [SkuIds] and SKUID_IDENTIFIER of Project.dsc.
#
################################################################################
[SkuIds]
  0|DEFAULT      #@todo: SKU ID needs to be implemented right way after the code merge. use BoardId temporarily.
  # VPD
  0x10|SkuIdAdlPLp4Rvp
  0x3F|SkuIdAdlPSimics
  0x1E|SkuIdAdlPDdr5Dg384Aep
  0x1F|SkuIdAdlPLp5Dg128Aep | SkuIdAdlPLp5Aep
  0x13|SkuIdAdlPLp5Rvp
  0x10013|SkuIdAdlPLp5PpvRVP | SkuIdAdlPLp5Rvp
  0x15|SkuIdAdlPT3Lp5Rvp | SkuIdAdlPLp5Rvp
  0x16|SkuIdAdlPDdr5MRRvp | SkuIdAdlPLp4Rvp
  0x1D|SkuIdAdlPLp5MbAep | SkuIdAdlPLp5Rvp
  0x19|SkuIdAdlPLp4Bep
  0x1A|SkuIdAdlPLp5Aep
  0x12|SkuIdAdlPDdr5Rvp
  0x14|SkuIdAdlPDdr4Rvp
  0x1B|SkuIdAdlPLp5Gcs
  0x1C|SkuIdAdlPMMAep | SkuIdAdlPDdr5Dg384Aep
  0x01|SkuIdAdlMLp4Rvp                       # Sku Id for ADL-M Lp4 Memory SD PCB
  0x02|SkuIdAdlMLp5Rvp | SkuIdAdlMLp4Rvp     # Sku Id for ADL-M Lp5 Memory SD PCB
  0x03|SkuIdAdlMLp5PmicRvp | SkuIdAdlMLp5Rvp # Sku Id for ADL-M Lp5 PMIC Memory SD PCB
  0x04|SkuIdAdlMLp5Rvp2a | SkuIdAdlMLp5Rvp        # Sku Id for ADL-M Lp5 RVP2a Memory Skt PCB
  0x20004|SkuIdAdlMLp5Rvp2aPpv  | SkuIdAdlMLp5Rvp2a     # Sku Id for ADL-M Lp5 RVP2a Memory SD PCB
  0x0F|SkuIdAdlMLp5Aep | SkuIdAdlMLp5Rvp     # Sku Id for ADL-M Lp5 Memory SD AEP


###############################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform.
#
################################################################################

[PcdsFeatureFlag]
#[-start-190612-IB16990049-add]#
  gInsydeTokenSpaceGuid.PcdH2OFdmChainOfTrustSupported|$(BOOT_GUARD_SUPPORT)
#[-end-190612-IB16990049-add]#
  gInsydeTokenSpaceGuid.PcdH2ODdtSupported|$(INSYDE_DEBUGGER)
!if $(EFI_DEBUG) == YES
  gInsydeTokenSpaceGuid.PcdStatusCodeUseDdt|$(INSYDE_DEBUGGER)
  gInsydeTokenSpaceGuid.PcdStatusCodeUseUsb|$(USB_DEBUG_SUPPORT)
!if $(USB_DEBUG_SUPPORT) == NO
#
# Running crisis will be fail if the CRB bios image is "nmake uefi64" and the USB dongle BIOS image is " uefi64 efidebug"
#
  gEfiTraceHubTokenSpaceGuid.PcdStatusCodeUseTraceHub|FALSE
!endif

!endif

#
# FRONTPAGE_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdFrontPageSupported|$(FRONTPAGE_SUPPORT)

#
# CRISIS RECOVERY
#
  gInsydeTokenSpaceGuid.PcdCrisisRecoverySupported|$(CRISIS_RECOVERY_SUPPORT)

#
# USE_FAST_CRISIS_RECOVERY
#
  gInsydeTokenSpaceGuid.PcdUseFastCrisisRecovery|$(USE_FAST_CRISIS_RECOVERY)

#
# SECURE_FLASH_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdSecureFlashSupported|$(SECURE_FLASH_SUPPORT)
  gInsydeTokenSpaceGuid.PcdBackupSecureBootSettingsSupported|$(BACKUP_SECURE_BOOT_SETTINGS_SUPPORT)

#
# UNSIGNED_FV_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdUnsignedFvSupported|$(UNSIGNED_FV_SUPPORT)

#
# CONSOLE_REDIRECTION_SUPPORT
#
 gInsydeCrTokenSpaceGuid.PcdH2OConsoleRedirectionSupported|FALSE

#
# UEFI_NETWORK_SUPPORTED
#
  gInsydeTokenSpaceGuid.PcdH2ONetworkSupported|TRUE

#
# DUAL_NETWORK_ENABLE
#
  gInsydeTokenSpaceGuid.PcdH2ONetworkIpv6Supported|TRUE

#
# SYS_PASSWORD_IN_CMOS
#
  gInsydeTokenSpaceGuid.PcdSysPasswordInCmos|$(SYS_PASSWORD_IN_CMOS)

#
# SUPPORT_USER_PASSWORD
#
  gInsydeTokenSpaceGuid.PcdSysPasswordSupportUserPswd|$(SUPPORT_USER_PASSWORD)

#
# SUPPORT_HDD_PASSWORD
#
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordSupported|TRUE

#
# SNAP_SCREEN switch
#
  gInsydeTokenSpaceGuid.PcdSnapScreenSupported|$(SNAPSCREEN_SUPPORT)
  gInsydeTokenSpaceGuid.PcdBvdtGenBiosBuildTimeSupported|FALSE

#
# MEMORY_SPD_PROTECTION
#
  gChipsetPkgTokenSpaceGuid.PcdMemSpdProtectionSupported|$(MEMORY_SPD_PROTECTION)

#
# TXT_SUPPORT
#
  gChipsetPkgTokenSpaceGuid.PcdTXTSupported|$(TXT_SUPPORT)

#
# TPM_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdH2OTpmSupported|TRUE
!if gChipsetPkgTokenSpaceGuid.PcdTXTSupported
  gInsydeTokenSpaceGuid.PcdH2OTpmSupported|TRUE
!endif

#
# TPM2_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdH2OTpm2Supported|TRUE

#
# HYBRID_GRAPHICS_SUPPORT
#
  gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported|$(HYBRID_GRAPHICS_SUPPORT)

#
# NVIDIA_OPTIMUS_SUPPORT
#
  gChipsetPkgTokenSpaceGuid.PcdNvidiaOptimusSupported|$(NVIDIA_OPTIMUS_SUPPORT)

#
# AMD_POWERXPRESS_SUPPORT
#
  gChipsetPkgTokenSpaceGuid.PcdAmdPowerXpressSupported|$(AMD_POWERXPRESS_SUPPORT)
  gChipsetPkgTokenSpaceGuid.PcdHgAslCodeForWptLynxPointLp|$(HG_ASLCODE_FOR_WPT_LYNXPOINTLP)
#
# SMM_INT10_ENABLE
#
  gChipsetPkgTokenSpaceGuid.PcdSmmInt10Enable|$(SMM_INT10_ENABLE)
#
# AHCI_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdH2OAhciSupported|TRUE

#
# EC_SHARED_FLASH_SUPPORT
#
  gChipsetPkgTokenSpaceGuid.PcdEcSharedFlashSupported|$(EC_SHARED_FLASH_SUPPORT)

#
# EC_IDLE_PER_WRITE_BLOCK
#
  gChipsetPkgTokenSpaceGuid.PcdEcIdlePerWriteBlockSupported|$(EC_IDLE_PER_WRITE_BLOCK)

#
# BIOS_GUARD_SUPPORT
#
  gBoardModuleTokenSpaceGuid.PcdEcBiosGuardEnable|gChipsetPkgTokenSpaceGuid.PcdBiosGuardEcSupport

  !if $(BIOS_GUARD_SUPPORT) == YES
    gInsydeTokenSpaceGuid.PcdSecureFlashSupported|TRUE
    gInsydeTokenSpaceGuid.PcdRuntimeReclaimSupported|FALSE
  #
  # Please set PcdBiosGuardEcSupport as TURE if Project use Non-Shared rom EC which support Bios Guard requested Command.
  # For Bios Guard requested EC Command, please reference Intel Platform Protection Technology with BIOS Guard Mobile Embedded Controller Design Guide
  #
  # gChipsetPkgTokenSpaceGuid.PcdBiosGuardEcSupport|TRUE
  #
  !if gChipsetPkgTokenSpaceGuid.PcdBiosGuardEcSupport
    gChipsetPkgTokenSpaceGuid.PcdH2OPeiCpBiosGuardEcSupported|TRUE
  !else
    gChipsetPkgTokenSpaceGuid.PcdH2OPeiCpBiosGuardUpdateBgpdt|TRUE
  !endif
  !endif

#
# Q2LSERVICE_SUPPORT
#
  gInsydeTokenSpaceGuid.PcdH2OQ2LServiceSupported|$(Q2LSERVICE_SUPPORT)

#
# PTT_SUPPORT
#
  gChipsetPkgTokenSpaceGuid.PcdPttSupported|$(PTT_SUPPORT)

#
# CFL/CNL Platform selector
#
  gInsydeTokenSpaceGuid.PcdH2OUsbSupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OSdhcSupported|FALSE
  gInsydeTokenSpaceGuid.PcdH2ONetworkIscsiSupported|FALSE
  gInsydeTokenSpaceGuid.PcdH2OIdeSupported|FALSE
  gInsydeTokenSpaceGuid.PcdUefiPauseKeyFunctionSupport|$(UEFI_PAUSE_KEY_FUNCTION_SUPPORT)
  gInsydeTokenSpaceGuid.PcdTextModeFullScreenSupport|$(TEXT_MODE_FULL_SCREEN_SUPPORT)
  gInsydeTokenSpaceGuid.PcdOnlyUsePrimaryMonitorToDisplay|TRUE

#_Start_L05_GRAPHIC_UI_ENABLE
!if $(L05_GRAPHIC_UI_ENABLE) == NO
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported|FALSE
!else
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported|FALSE
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported|TRUE
!endif
#_End_L05_GRAPHIC_UI_ENABLE

!if gSiPkgTokenSpaceGuid.PcdAmtEnable
  gInsydeCrTokenSpaceGuid.PcdH2OConsoleRedirectionSupported|TRUE
  gInsydeCrTokenSpaceGuid.PcdH2OCrPciSerialSupported|FALSE
  gInsydeTokenSpaceGuid.PcdDisplayOemHotkeyString|TRUE
!endif

#
# CrConfigUtilVfr.vfr was included at Advance.hfr only in H2O Form Browser.
#
# ConsoleRedirection only support below 2 display method.
# [1] Controller Tool and CRB Display all show Text mode SCU.
#  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported|TRUE
#  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported|FALSE
# [2] Controller Tool show Text Mode, CRB Display Graphic mode.
#  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported|TRUE
#  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported|TRUE
#
!if gInsydeCrTokenSpaceGuid.PcdH2OConsoleRedirectionSupported
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported|TRUE
!endif

!errif (gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported == FALSE) and (gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported == FALSE), "Must have at least one display engine enabled in Project.dsc"

#_Start_L05_GAMING_UI_ENABLE_
!errif (gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported == FALSE) and (gL05ServicesTokenSpaceGuid.PcdL05GamingUiSupported == TRUE), "Gaming Ui must disable if PcdH2OFormBrowserLocalMetroDESupported == FALSE in Project.dsc"
!if gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported == FALSE
  gL05ServicesTokenSpaceGuid.PcdL05GamingUiSupported|FALSE
!endif
#_End_L05_GAMING_UI_ENABLE_

  gChipsetPkgTokenSpaceGuid.PcdRestoreCmosfromVariableFlag|FALSE

  gChipsetPkgTokenSpaceGuid.PcdMeUnconfigOnRtcSupported|FALSE

  gInsydeTokenSpaceGuid.PcdMultiConfigSupported|$(MULTI_CONFIG_SUPPORT)

  gInsydeTokenSpaceGuid.PcdDynamicHotKeySupported|$(DYNAMIC_HOTKEY_SUPPORT)
#
# PcdS3SaveGpioTableFlag
# TRUE  | Save GPIO status when system into S3, if GPIO was changed.
# FALSE | GPIO status will return to default when S3 resume.
#
  gInsydeTokenSpaceGuid.PcdH2OI2cSupported|TRUE
#_Start_L05_FEATURE_
#  gInsydeTokenSpaceGuid.PcdShellBinSupported|TRUE
  gInsydeTokenSpaceGuid.PcdShellBinSupported|FALSE
#_End_L05_FEATURE_
  gInsydeTokenSpaceGuid.PcdShellBuildSupported|FALSE
  gChipsetPkgTokenSpaceGuid.PcdDisableScuAggressiveLpmSupportForPchH|TRUE
  gSioGuid.PcdSioDummySupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdDebugUsePchComPort|$(DEBUG_USE_PCH_COMPORT)
  gChipsetPkgTokenSpaceGuid.PcdH2OIDEGPIOEditor|$(USE_H2O_IDE_GPIO_EDITOR)

!if $(INSYDE_DEBUGGER) == YES
  !if $(H2O_DDT_DEBUG_IO) == Com
    gChipsetPkgTokenSpaceGuid.PcdComPortDdt|TRUE
  !endif
!endif

#
# Enable/Disable Insyde ChasmFalls
#
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 0
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled|FALSE
!endif 

!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 1
  # BIOS
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled|TRUE
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled|FALSE
  gChipsetPkgTokenSpaceGuid.PcdSpiReadByMemoryMapped|FALSE
  # Monolithic
  gChipsetPkgTokenSpaceGuid.PcdMonolithicCapsuleUpdateSupported|TRUE
  # ME
  gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported|TRUE
!elseif gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
  # BIOS
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled|TRUE
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled|TRUE
  gChipsetPkgTokenSpaceGuid.PcdSpiReadByMemoryMapped|FALSE
  gInsydeTokenSpaceGuid.PcdH2OBaseCpVerifyFvSupported|TRUE
  # Microcode
  gChipsetPkgTokenSpaceGuid.PcdUcodeCapsuleUpdateSupported|TRUE
  # Monolithic
  gChipsetPkgTokenSpaceGuid.PcdMonolithicCapsuleUpdateSupported|TRUE
  # ME
  gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported|TRUE
!endif

  #
  # To reduice post time.
  # System will not get Tbt retimer version during boot.
  # For A0-stepping cpu, it must set ped to FALSE, otherwise it will hang reset.
  #
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerGetVerDuringBoot|FALSE
!if gPlatformModuleTokenSpaceGuid.PcdTdsEnable
  gInsydeTokenSpaceGuid.PcdH2OBdsCpBootDeviceEnumCheckBootOptionSupported|TRUE
!endif

  gInsydeTokenSpaceGuid.PcdH2OBaseCpVerifyFvSupported|TRUE
  
[PcdsFixedAtBuild]
  !include $(PROJECT_PKG)/PlatformPkgConfigOverride.dsc

#[-start-210506-IB16740139-add]#
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  gSiPkgTokenSpaceGuid.PcdITbtEnable|TRUE
!endif
#[-end-210506-IB16740139-add]#
#
# BIOS_GUARD_SUPPORT
#
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable|$(BIOS_GUARD_SUPPORT)
#
# BOOT_GUARD_SUPPORT_FLAG
#
!if $(BOOT_GUARD_SUPPORT) == YES
  gSiPkgTokenSpaceGuid.PcdBootGuardEnable|TRUE
!else
  gSiPkgTokenSpaceGuid.PcdBootGuardEnable|FALSE
!endif

#
# Intel Firmware Support Package (FSP)
#
gSiPkgTokenSpaceGuid.PcdFspWrapperEnable|$(FSP_WRAPPER_SUPPORT)

#
# OVERCLOCK_ENABLE
#
!if $(OVERCLOCK_ENABLE) == YES
  gSiPkgTokenSpaceGuid.PcdOverclockEnable|TRUE
!else
  gSiPkgTokenSpaceGuid.PcdOverclockEnable|FALSE
!endif

#
# THUNDERBOLT_SUPPORT
#
##  set PcdITbtEnable to True, because Adl will support iTBT.
 #gSiPkgTokenSpaceGuid.PcdITbtEnable|TRUE
 #gEfiCpuTokenSpaceGuid.PcdCpuIEDEnabled|TRUE

!if $(FIRMWARE_PERFORMANCE) == YES
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|1
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxPeiPerformanceLogEntries|150
!else
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|0
!endif
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x0F

!if $(H2O_PORT_80_DEBUG) == YES
  gChipsetPkgTokenSpaceGuid.PcdH2OPort80DebugEnable|1
!else
  gChipsetPkgTokenSpaceGuid.PcdH2OPort80DebugEnable|0
!endif

!if $(EFI_PORT_80_DEBUG) == YES
  gChipsetPkgTokenSpaceGuid.PcdEfiPort80DebugEnable|1
!else
  gChipsetPkgTokenSpaceGuid.PcdEfiPort80DebugEnable|0
!endif

!if $(SUPPORT_64BITS_AML) == YES
  gChipsetPkgTokenSpaceGuid.PcdDsdtRevision|0x02
!else
  gChipsetPkgTokenSpaceGuid.PcdDsdtRevision|0x01
!endif

  gPerformancePkgTokenSpaceGuid.PcdPerfPkgAcpiIoPortBaseAddress|0x1800

#  WARNING:
#   This Pcd is the memory size for storing CAR data, it has to be always larger or equal to PcdTemporaryRamSize.
#   S3 resume may fail if PcdTemporaryRamSize > PcdS3AcpiReservedMemorySize.
  gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdS3AcpiReservedMemorySize|0x2600000
  #
  # Hot key Configuration
  # Platform Hot key Define
  # ScanCode, ShiftKey, AltKey, CtrlKey
  # ex:
  #    0x54, 0x0, 0x1, 0x0      F1(Combination Key ScanCode) + ShiftKey
  #    0x68, 0x0, 0x2, 0x0      F1(Combination Key ScanCode) + AltKey
  #    0x5f, 0x0, 0x4, 0x0      F1(Combination Key ScanCode) + CtrlKey
  #
  gInsydeTokenSpaceGuid.PcdPlatformKeyList|{ \
    0x3b, 0x0, 0x0, 0x0,                     \ # F1_KEY
    0x3c, 0x0, 0x0, 0x0,                     \ # F2_KEY
    0x53, 0x0, 0x0, 0x0,                     \ # DEL_KEY
    0x44, 0x0, 0x0, 0x0,                     \ # F10_KEY
    0x86, 0x0, 0x0, 0x0,                     \ # F12_KEY
    0x01, 0x0, 0x0, 0x0,                     \ # ESC_KEY
    0x40, 0x0, 0x0, 0x0,                     \ # UP_ARROW_KEY_BIT
    0x3d, 0x0, 0x0, 0x0,                     \ # F3_KEY
    0x43, 0x0, 0x0, 0x0,                     \ # F9_KEY
#_Start_L05_SMB_BIOS_ENABLE_
    0x85, 0x0, 0x0, 0x0,                     \ # F11_KEY
#_Start_L05_INTERRUPT_MENU_
    0x1C, 0x0, 0x0, 0x0,                     \ # ENTER_KEY
#_End_L05_INTERRUPT_MENU_
#_End_L05_SMB_BIOS_ENABLE_
    0x00, 0x0, 0x0, 0x0}                       # EndEntry
  gChipsetPkgTokenSpaceGuid.PcdSmbiosType20PartitionRowPosition|0xFF
# Power On Demo bios Tseg is 0x1000000
# Demo BIOS Tseg size changes to 0x400000
# According to EDS, BIOS must program TSEGMB to a 8MB naturally aligned boundary, so change PcdSaTsegSize from 4MB to 8MB
  gChipsetPkgTokenSpaceGuid.PcdSaTsegSize|0x1000000
#
# You should take care those PCDs listed as below
#   - gEfiTraceHubTokenSpaceGuid.PcdStatusCodeUseTraceHub
#   - gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial
#   - gEfiTraceHubTokenSpaceGuid.PcdTraceHubStatusCodeMaster
#   - gEfiTraceHubTokenSpaceGuid.PcdTraceHubStatusCodeChannel
#

  gEfiTraceHubTokenSpaceGuid.PcdTraceHubStatusCodeMaster|0x48
  gEfiTraceHubTokenSpaceGuid.PcdTraceHubStatusCodeChannel|0x0E

!if $(L05_GAMING_UI_ENABLE) == YES
  gInsydeTokenSpaceGuid.PcdScuFormsetGuidList|{ \
#_Start_L05_SETUP_MENU_
    GUID("4c622579-b559-4602-93e0-4473793ea200"), \ # Gaming Home
    GUID("1D09183D-66A4-489D-9FCA-CA8E6FEFF971"), \ # Information Main
    GUID("F500784D-75B5-41FA-B7D5-D4137DAEEDB8"), \ # Configuration
#_End_L05_SETUP_MENU_
    GUID("C1E0B01A-607E-4B75-B8BB-0631ECFAACF2"), \ # Main
    GUID("C6D4769E-7F48-4D2A-98E9-87ADCCF35CCC"), \ # Avance
    GUID("5204F764-DF25-48A2-B337-9EC122B85E0D"), \ # Security
    GUID("A6712873-925F-46C6-90B4-A40F86A0917B"), \ # Power
    GUID("2D068309-12AC-45AB-9600-9187513CCDD8"), \ # Boot
    GUID("B6936426-FB04-4A7B-AA51-FD49397CDC01"), \ # Exit
    GUID("00000000-0000-0000-0000-000000000000")}

  gInsydeTokenSpaceGuid.PcdScuFormsetFlagList|{ \
#_Start_L05_SETUP_MENU_
    UINT8(0), \ # Gaming Home
    UINT8(0), \ # Information Main
    UINT8(0), \ # Configuration
#_End_L05_SETUP_MENU_
    UINT8(0), \ # Main
    UINT8(0), \ # Avance
    UINT8(0), \ # Security
    UINT8(0), \ # Power
    UINT8(0), \ # Boot
    UINT8(0), \ # Exit
    UINT8(0xFF)}
!else
  gInsydeTokenSpaceGuid.PcdScuFormsetGuidList|{ \
#_Start_L05_SETUP_MENU_
    GUID("1D09183D-66A4-489D-9FCA-CA8E6FEFF971"), \ # Information Main
    GUID("F500784D-75B5-41FA-B7D5-D4137DAEEDB8"), \ # Configuration
#_End_L05_SETUP_MENU_
    GUID("C1E0B01A-607E-4B75-B8BB-0631ECFAACF2"), \ # Main
    GUID("C6D4769E-7F48-4D2A-98E9-87ADCCF35CCC"), \ # Avance
    GUID("5204F764-DF25-48A2-B337-9EC122B85E0D"), \ # Security
    GUID("A6712873-925F-46C6-90B4-A40F86A0917B"), \ # Power
    GUID("2D068309-12AC-45AB-9600-9187513CCDD8"), \ # Boot
    GUID("B6936426-FB04-4A7B-AA51-FD49397CDC01"), \ # Exit
    GUID("00000000-0000-0000-0000-000000000000")}

  gInsydeTokenSpaceGuid.PcdScuFormsetFlagList|{ \
#_Start_L05_SETUP_MENU_
    UINT8(0), \ # Information Main
    UINT8(0), \ # Configuration
#_End_L05_SETUP_MENU_
    UINT8(0), \ # Main
    UINT8(0), \ # Avance
    UINT8(0), \ # Security
    UINT8(0), \ # Power
    UINT8(0), \ # Boot
    UINT8(0), \ # Exit
    UINT8(0xFF)}
!endif

#
#Register Ihisi sub function table list.
#Table struct define {CmdNumber, FuncSignature, Priority}
# UINT8(CmdNumber), Char8[20](FuncSignature), UINT8(Priority)
##================  ========================  ===============
gInsydeTokenSpaceGuid.PcdIhisiRegisterTable|{ \
  # Register IHISI AH=00h (VATSRead)
  UINT8(0x00),      "S00Kn_VatsRead00000",    UINT8(0x80), \

  # Register IHISI AH=01h (VATSWrite)
  UINT8(0x01),      "S01Kn_VatsWrite0000",    UINT8(0x80), \

  # Register IHISI AH=05h (VATSNext)
  UINT8(0x05),      "S05Kn_VatsGetNext00",    UINT8(0x80), \

  # Register IHISI AH=10h (FBTSGetSupportVersion)
  UINT8(0x10),      "S10Cs_GetPermission",    UINT8(0xE0), \
  UINT8(0x10),      "S10OemGetPermission",    UINT8(0xC0), \
  UINT8(0x10),      "S10OemGetAcStatus00",    UINT8(0xBB), \
  UINT8(0x10),      "S10OemBatterylife00",    UINT8(0xB6), \
  UINT8(0x10),      "S10Kn_GetVersion000",    UINT8(0x80), \
  UINT8(0x10),      "S10Kn_InitOemHelp00",    UINT8(0x7F), \
  UINT8(0x10),      "S10Kn_GetVendorID00",    UINT8(0x7E), \
  UINT8(0x10),      "S10Kn_GetBatteryLow",    UINT8(0x7D), \

  # Register IHISI AH=11h (FBTSGetPlatformInfo)
  UINT8(0x11),      "S11Kn_GetModelName0",    UINT8(0x80), \
  UINT8(0x11),      "S11Kn_GModelVersion",    UINT8(0x7F), \
  UINT8(0x11),      "S11OemFbtsApCheck00",    UINT8(0x40), \
  UINT8(0x11),      "S11Kn_UpExtPlatform",    UINT8(0x20), \

  # Register IHISI AH=12h (FBTSGetPlatformRomMap)
  UINT8(0x12),      "S12Kn_ProtectRomMap",    UINT8(0x80), \
  UINT8(0x12),      "S12Kn_PrivateRomMap",    UINT8(0x7F), \
  UINT8(0x12),      "S12Cs_PlatformRomMp",    UINT8(0x40), \
  UINT8(0x12),      "S12OemPlatformRomMp",    UINT8(0x20), \

  # Register IHISI AH=13h (FBTSGetFlashPartInfo)
  UINT8(0x13),      "S13Kn_FlashPartInfo",    UINT8(0x80), \

  # Register IHISI AH=14h (FBTSRead)
  UINT8(0x14),      "S14Cs_DoBeforeRead0",    UINT8(0xE0), \
  UINT8(0x14),      "S14Kn_FbtsReadProce",    UINT8(0x80), \
  UINT8(0x14),      "S14Cs_DoAfterRead00",    UINT8(0x20), \

  # Register IHISI AH=15h (FBTSWrite)
  UINT8(0x15),      "S15Cs_DoBeforeWrite",    UINT8(0xE0), \
  UINT8(0x15),      "S15Kn_FbtsWriteProc",    UINT8(0x80), \
  UINT8(0x15),      "S15Cs_DoAfterWrite0",    UINT8(0x40), \

  # Register IHISI AH=16h (FBTSComplete)
  UINT8(0x16),      "S16Cs_CApTerminalte",    UINT8(0xE0), \
  UINT8(0x16),      "S16Cs_CNormalFlash0",    UINT8(0xDF), \
  UINT8(0x16),      "S16Cs_CPartialFlash",    UINT8(0xDE), \
  UINT8(0x16),      "S16Kn_PurifyVariabl",    UINT8(0x80), \
  UINT8(0x16),      "S16Cs_FbtsComplete0",    UINT8(0x20), \
  UINT8(0x16),      "S16Cs_FbtsReboot000",    UINT8(0x1F), \
  UINT8(0x16),      "S16Cs_FbtsShutDown0",    UINT8(0x1E), \
  UINT8(0x16),      "S16Cs_FbtsDoNothing",    UINT8(0x1D), \

  # Register IHISI AH=17h (FBTSGetRomFileAndPlatformTable)
  UINT8(0x17),      "S17Cs_GetPlatformTb",    UINT8(0x80), \

  # Register IHISI AH=1Bh (FBTSSkipMcCheckAndBinaryTrans)
  UINT8(0x1B),      "S1BKn_SkipMcCheck00",    UINT8(0x80), \

  # Register IHISI AH=1Ch (FBTSGetATpInformation)
  UINT8(0x1C),      "S1CCs_GetATpInfo000",    UINT8(0x80), \

  # Register IHISI AH=1Eh (FBTSGetWholeBiosRomMap)
  UINT8(0x1E),      "S1EKn_WholeBiosRomp",    UINT8(0x80), \
  UINT8(0x1E),      "S1ECs_WholeBiosRomp",    UINT8(0x60), \
  UINT8(0x1E),      "S1EOemWholeBiosRomp",    UINT8(0x40), \

  # Register IHISI AH=1Fh (FBTSApHookPoint)
  UINT8(0x1F),      "S1FKn_ApHookforBios",    UINT8(0x80), \
  UINT8(0x1F),      "S1FCs_ApHookForBios",    UINT8(0x40), \
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  UINT8(0x1F),      "S1FL05ApHookForBios",    UINT8(0x20), \
#_End_L05_BIOS_SELF_HEALING_SUPPORT_

  # Register IHISI AH=20h (FETSWrite)
  UINT8(0x20),      "S20OemDoBeforeWrite",    UINT8(0xE0), \
  UINT8(0x20),      "S20OemEcIdleTrue000",    UINT8(0xC0), \
  UINT8(0x20),      "S20OemFetsWrite0000",    UINT8(0x80), \
  UINT8(0x20),      "S20OemEcIdleFalse00",    UINT8(0x40), \
  UINT8(0x20),      "S20OemDoAfterWrite0",    UINT8(0x20), \
  UINT8(0x20),      "S20Cs_ShutdownMode0",    UINT8(0x1B), \

  # Register IHISI AH=21h (FETSGetEcPartInfo)
  UINT8(0x21),      "S21OemGetEcPartInfo",    UINT8(0x80), \

  # Register IHISI AH=41h (OEMSFOEMExCommunication)
  UINT8(0x41),      "S41Kn_CommuSaveRegs",    UINT8(0xFF), \
  UINT8(0x41),      "S41Cs_ExtDataCommun",    UINT8(0xE0), \
  UINT8(0x41),      "S41OemT01Vbios00000",    UINT8(0xC0), \
  UINT8(0x41),      "S41OemT54LogoUpdate",    UINT8(0xBB), \
  UINT8(0x41),      "S41OemT55CheckSignB",    UINT8(0xB6), \
  UINT8(0x41),      "S41OemReservedFun00",    UINT8(0xB1), \
  UINT8(0x41),      "S41Kn_T51EcIdelTrue",    UINT8(0x85), \
  UINT8(0x41),      "S41Kn_ExtDataCommun",    UINT8(0x80), \
  UINT8(0x41),      "S41Kn_T51EcIdelFals",    UINT8(0x7B), \
  UINT8(0x41),      "S41OemT50Oa30RWFun0",    UINT8(0x40), \
#_Start_L05_CUSTOMIZE_MULTI_LOGO_
  UINT8(0x41),      "S41L05T04CustMultLg",    UINT8(0xE2), \
#_End_L05_CUSTOMIZE_MULTI_LOGO_

  # Register IHISI AH=42h (OEMSFOEMExDataWrite)
  UINT8(0x42),      "S42Cs_ExtDataWrite0",    UINT8(0xE0), \
  UINT8(0x42),      "S42Kn_T50EcIdelTrue",    UINT8(0x85), \
  UINT8(0x42),      "S42Kn_ExtDataWrite0",    UINT8(0x80), \
  UINT8(0x42),      "S42Kn_T50EcIdelFals",    UINT8(0x7B), \
  UINT8(0x42),      "S42Cs_DShutdownMode",    UINT8(0x20), \
#_Start_L05_CUSTOMIZE_MULTI_LOGO_
  UINT8(0x42),      "S42L05T04WrCustomLg",    UINT8(0xE2), \
#_End_L05_CUSTOMIZE_MULTI_LOGO_

  # Register IHISI AH=47h (OEMSFOEMExDataRead)
  UINT8(0x47),      "S47Cs_ExtDataRead00",    UINT8(0xE0), \
  UINT8(0x47),      "S47Kn_ExtDataRead00",    UINT8(0x80), \

  # Register IHISI AH=48h (FBTSOEMCapsuleSecureFlash)

  UINT8(0x48),      "S48Kn_CpSecureFlash",    UINT8(0x80), \

  # Register IHISI AH=49h (FBTSCommonCommunication)
  UINT8(0x49),      "S49Kn_ComDataCommun",    UINT8(0x80), \

  # Register IHISI AH=4Bh (FBTSCommonRead)
  UINT8(0x4B),      "S4BKn_ComDataRead00",    UINT8(0x80), \

  # Register IHISI AH=4Dh (FBTSPassImageFromTool)
  UINT8(0x4D),      "S4DCs_ImageCheck000",    UINT8(0xC0), \
  # Register IHISI AH=80h (IhisiAuthStatus)
  UINT8(0x80),      "S80Kn_AuthStatus000",    UINT8(0x80), \
  # Register IHISI AH=81h (IhisiAuthLock)
  UINT8(0x81),      "S81Kn_AuthLock00000",    UINT8(0x80), \
  # Register IHISI AH=82h (IhisiAuthUnlock)
  UINT8(0x82),      "S82Kn_AuthUnlock000",    UINT8(0x80), \
  # Register IHISI AH=83h (IhisiGetCmdBuffer)
  UINT8(0x83),      "S83Kn_GetCmdBuf0000",    UINT8(0x80), \
  UINT8(0x83),      "S83Kn_GetImageBuf00",    UINT8(0x79), \
  # Register IHISI AH=84h (IhisiAuth)
  UINT8(0x84),      "S84Kn_Auth000000000",    UINT8(0x80) }

#
#  Specifies timeout value in microseconds for the BSP to detect all APs for the first time.
#  Prompt Timeout for the BSP to detect all APs for the first time.
#  Reduce S3 resume time by reduce CPU AP initial timeout.
#
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApInitTimeOutInMicroSeconds|10000

!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
#
# This PCD is automatically updated by FspBinFvsBaseAddress.exe. Please do NOT modify !!
#
  gIntelFsp2WrapperTokenSpaceGuid.PcdFsptBaseAddress|0xFFFC2000
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspmBaseAddress|0xFFE72000
#[-start-181031-IB10182002-add]#
#[-start-200420-IB17800056-remove]#
# ADL could remove PcdFixedFspmBaseAddress.
#  gPlatformModuleTokenSpaceGuid.PcdFixedFspmBaseAddress|gIntelFsp2WrapperTokenSpaceGuid.PcdFspmBaseAddress
#[-end-200420-IB17800056-remove]#
#[-end-181031-IB10182002-add]#
!endif

#
# Physical address bits supported,
# CPUID.80000008H (Linear/Physical Address size): Bits 7-0: #Physical Address Bits, Bits 15-8: #Linear Address Bits
#
  gInsydeTokenSpaceGuid.PcdMemorySpaceSize|38

!if $(EFI_DEBUG) == YES
!if $(USB_DEBUG_SUPPORT) == NO
  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|TRUE
#
# Running crisis will be fail if the CRB bios image is "nmake uefi64" and the USB dongle BIOS image is " uefi64 efidebug"
#
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|FALSE
!endif

!if $(INSYDE_DEBUGGER) == YES and $(H2O_DDT_DEBUG_IO) == Com
  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|FALSE
!endif
!endif
#
# Unmark pcd to enable status code to memory
#
#  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory|TRUE

#[-start-201217-IB16560234-add]#
!if $(INSYDE_DEBUGGER) == YES and $(H2O_DDT_DEBUG_IO) == Com
  ##
  ## Com port DDT will disconnect if output debug message
  ##
  !if gChipsetPkgTokenSpaceGuid.PcdDebugUsePchComPort == TRUE
    # PCH Uart
    gPlatformModuleTokenSpaceGuid.PcdStatusCodeUseSerialIoUart|FALSE
  !else
    # EC
    gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|FALSE
  !endif
!endif
#[-end-201217-IB16560234-add]#

#
# BiosGuard
#
!if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable
#_Start_L05_FLASH_UPDATE_
#  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPublicKeySlot0|{0x4F, 0xF7, 0x7D, 0x32, 0x56, 0x6C, 0x4C, 0x70, 0x67, 0x44, 0x5B, 0xF3, 0xCA, 0xF7, 0x26, 0x5A, 0x15, 0xD8, 0xF4, 0x3E, 0xAF, 0x5F, 0x97, 0xD6, 0xB8, 0xC0, 0x47, 0x45, 0xDE, 0x72, 0x9E, 0xD5}
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPublicKeySlot0|{0xFE, 0x77, 0x70, 0x99, 0xEB, 0x10, 0xA9, 0x76, 0x49, 0x62, 0xA4, 0x04, 0x92, 0xA2, 0xCA, 0x8D, 0xE4, 0xDD, 0x99, 0x30, 0x13, 0x87, 0xA7, 0x93, 0xA8, 0x6C, 0x76, 0xC5, 0xAF, 0xC2, 0xE4, 0x6F}
#_End_L05_FLASH_UPDATE_
  gEfiMdeModulePkgTokenSpaceGuid.PcdReclaimVariableSpaceAtEndOfDxe|TRUE
!if gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigEcCmdDiscovery|0xB0
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigEcCmdProvisionEav|0xB1
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigEcCmdLock|0xB2
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdGetSvn|0xB3
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdOpen|0xB4
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdClose|0xB5
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdPortTest|0xB6
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgupHeaderEcSvn|0x00001000
!endif
!endif

#[-start-190613-IB16990062-add]#
#
# Provide OemHook to sync HW I2C SCL SDA signal, default value refers to CoffeeLake-H platform
#
  gChipsetPkgTokenSpaceGuid.OemHookI2cSclSdaEnable|FALSE
  gChipsetPkgTokenSpaceGuid.I2cIcSsSclHcnt|0x01F4
  gChipsetPkgTokenSpaceGuid.I2cIcSsSclLcnt|0x024C
  gChipsetPkgTokenSpaceGuid.I2cIcSsSdaHold|0x1C
  gChipsetPkgTokenSpaceGuid.I2cIcFsSclHcnt|0x0101
  gChipsetPkgTokenSpaceGuid.I2cIcFsSclLcnt|0x0101
  gChipsetPkgTokenSpaceGuid.I2cIcFsSdaHold|0x1C
  gChipsetPkgTokenSpaceGuid.I2cIcHsSclHcnt|0x0008
  gChipsetPkgTokenSpaceGuid.I2cIcHsSclLcnt|0x0014
  gChipsetPkgTokenSpaceGuid.I2cIcHsSdaHold|0x1C
#[-end-190613-IB16990062-add]#

#
# Please uncomment PcdDebugPrintErrorLevel setting if you want to print MTRR settings.
#
#  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x8020004F

#
# Provide PcdHgNvidiaDgpuCheckTable to switch NVIDIA DGPU N17 and N18 dynamically.
# Use N18 DGPU device ID as sample check method in the PcdHgNvidiaDgpuCheckTable.
# If you want to use other check method to switch DGPU dynamically, you can fill other values in the PcdHgNvidiaDgpuCheckTable. (ex. SKUID)
#
  gChipsetPkgTokenSpaceGuid.PcdHgNvidiaDgpuCheckTable|{ \
    UINT16(0x1E90), \
    UINT16(0x1F10), \
    UINT16(0x1F11), \
    UINT16(0xFFFF)  \
  }

#
# Provide PcdHgNvidiaDgpuCheckTable2 to switch NVIDIA DGPU N17, N18, N20 or further generation dynamically.
#
#  gChipsetPkgTokenSpaceGuid.PcdHgNvidiaDgpuCheckTable2|{ \
    # DeviceID      # Generation Info
                    # Ex: N17 = 0x11, N18 = 0x12, N20 = 0x14, ...
#    UINT16(0x1E90), UINT8(0x11), \
#    UINT16(0x1F10), UINT8(0x12), \
#    UINT16(0x1F11), UINT8(0x14)  \
#  }

# Defaule layout not support Chasm Falls
#_Start_L05_FEATURE_
!if $(L05_BIOS_SELF_HEALING_SUPPORT) == NO
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport|0
!else
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport|2
!endif
#_End_L05_FEATURE_

#
# Enable/Disable Insyde/Intel ChasmFalls
#
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 1 || gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
  # Enable Intel ChasmFalls (Default chasmfalls feature will not enable Intel chasmfalls)
  #gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable|TRUE
  # ME
  gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable|TRUE
  # Monolithic
!if gChipsetPkgTokenSpaceGuid.PcdMonolithicCapsuleUpdateSupported == TRUE 
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 1
  # Default support case is BIOS(Gen1 12M)+ ME(Max size: Corporate 9.8M) total 23M.
  # Since the flash layout of Gen 1 is the same as Gen 2, the setting was changed to the same as Gen 2
  gChipsetPkgTokenSpaceGuid.PcdMeFirmwareUpdateReservedMemorySize|0x1A00000
!else
  # Default support case is BIOS(Max size: Gen2 16M)+ ME(Max size: Corporate 9.8M) total 26M.
  gChipsetPkgTokenSpaceGuid.PcdMeFirmwareUpdateReservedMemorySize|0x1A00000
!endif 
!endif
!endif

#
# Bus[0~7] Device[8~15] Function[16~23] Port [24~31]
#
gChipsetPkgTokenSpaceGuid.PcdTbtRetimerAddress|0x00020D00

#
# Default onboard retimer version is 207.
#
gChipsetPkgTokenSpaceGuid.PcdTbtRetimerDefaultVersion|0x00CF


 #  Insyde PCD controls Native FSP build,
 #  It means your FSP binary builds by Insyde or Intel 
 #  TRUE  - Use Intel Native FSP.
 #  FALSE - Insyde BIOS should build FSP.
gChipsetPkgTokenSpaceGuid.PcdNativeFspBuild|FALSE
gChipsetPkgTokenSpaceGuid.PcdNativeFspVersion|"2452_00_226"

#
# TXT SINIT SUPPORT
#
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE
!if gChipsetPkgTokenSpaceGuid.PcdTXTSupported == FALSE
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE
!endif

################################################################################
#
# Pcd Dynamic Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################

#  gChipsetPkgTokenSpaceGuid.PcdNvmeMemBaseAddress|0xD9000000

  #
  # PcdNvmeRootPortAddress[23:16]: Secondary Bus Number and Subordinate Bus Number of P2P Bridge.
  # PcdNvmeRootPortAddress[15:8] : Device number of root port.
  # PcdNvmeRootPortAddress[7:0]  : Function number of root port.
  #
# gChipsetPkgTokenSpaceGuid.PcdNvmeRootPortAddress|0x00021B00

#
# Intel VPD Gpio table
#
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/AllStructPCD.dsc

[PcdsDynamicExDefault]
  ##                                                   Bus          Device        Function      Vendor ID       Device ID       PortMap
  ##                                                 ========      =========     ==========    ===========     ===========
  gInsydeTokenSpaceGuid.PcdH2OSataIgnoredDeviceList|{UINT32(0x00), UINT32(0x0E), UINT32(0x00), UINT32(0x8086), UINT32(0x467F), UINT32(0x00000000), \ # VMD device, Intel Kits #618427
                                                     UINT32(0xFF), UINT32(0xFF), UINT32(0xFF), UINT32(0xFFFF), UINT32(0xFFFF), UINT32(0xFFFFFFFF)} # All 0xFF indicates end of list.

  gUefiCpuPkgTokenSpaceGuid.PcdCpuMaxLogicalProcessorNumber|16
  #
  # Crisis File name definition
  #
  # New File Path Definition : //Volume_Label\\File_Path\\File_Name
  # Notice : "//" is signature that volume label start definition.
  #
  # Example path : //RECOVERY\\BIOS\\Current\\AlderLakeM.fd
  gInsydeTokenSpaceGuid.PcdPeiRecoveryFile|L"AlderLakeM.fd"|VOID*|0x100

#
# This file of included the PCD of gChipsetPkgTokenSpaceGuid.PcdCrbBoard to control it's CRB or OEM Board.
# OEM Project should not be modify this file.
#
!include $(PROJECT_PKG)/CrbExtConfig.dsc

#   UINT16 Address  UINT16 Length
  gChipsetPkgTokenSpaceGuid.PcdPchLpcGenIoDecodeTable|{ \   # LPC offset 84h-93h
    UINT16(0x0000), UINT16(0x0000), \
    UINT16(0x0000), UINT16(0x0000), \
    UINT16(0x0000), UINT16(0x0000), \
    UINT16(0x0000), UINT16(0x0000)}

  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPlatformId|"AlderLakeM"

################################################################################
#
# SMBIOS Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType000|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType001|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType002|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType003|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType008|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType009|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType011|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType012|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType013|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType015|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType021|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType022|TRUE
#_Start_L05_SMBIOS_ENABLE
#  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType024|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType024|FALSE
#_End_L05_SMBIOS_ENABLE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType026|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType027|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType028|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType032|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType039|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType040|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType041|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType128|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType129|TRUE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType130|FALSE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType131|FALSE
  gSmbiosTokenSpaceGuid.PcdActiveSmbiosType136|TRUE
  gSmbiosTokenSpaceGuid.PcdSmbiosMaxMultiRecords |32
  gSmbiosTokenSpaceGuid.PcdSmbiosMultiRecordsType|{2, 3, 4, 7, 8, 9, 17, 21, 22, 26, 27, 28, 29, 39, 41}
  gSmbiosTokenSpaceGuid.PcdType000Record | { \
      0x00,                       \ # Type
      0x00,                       \ # Length
      UINT16(0x0000),             \ # Handle
      0xFF,                       \ # Vendor
      0xFF,                       \ # BIOS Version
      UINT16(0xE000),             \ # BIOS Starting Address Segment
      0xFF,                       \ # BIOS Release Date
      0xFF,                       \ # BIOS ROM Size
      UINT64(0x000000004BF99880), \ # BIOS Characteristics
      UINT16(0x0D03),             \ # BIOS Characteristics Extension Bytes
      0xFF,                       \ # System BIOS Major Release
      0xFF,                       \ # System BIOS Minor Release
      0xFF,                       \ # Embedded Controller Firmware Major Release
      0xFF,                       \ # Embedded Controller Firmware Minor Release
      0x00,                       \ # Extended BIOS ROM Size
      0x00                        \ # Extended BIOS ROM Size
  }
  gSmbiosTokenSpaceGuid.PcdType000Strings|"Insyde;05.44.02.0035;01/13/2022;"
  gSmbiosTokenSpaceGuid.PcdType001Record |{0x01, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x78, 0x56, 0x34, 0x12, 0x34, 0x12, 0x78, 0x56, 0x90, 0xab, 0xcd, 0xde, 0xef, 0xaa, 0xbb, 0xcc, 0x06, 0x05, 0x06}
  gSmbiosTokenSpaceGuid.PcdType001Strings|"Insyde;AlderLake;TBD by OEM;123456789;Type1Sku0;Type1Family;"
  gSmbiosTokenSpaceGuid.PcdType002Record000 |{0x02, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x09, 0x06, 0xFF, 0xFF, 0x0A, 0x00}
  gSmbiosTokenSpaceGuid.PcdType002Strings000|"Type2 - Board Vendor Name1;Type2 - Board Product Name1;Type2 - Board Version;Type2 - Board Serial Number;Type2 - Board Asset Tag;Type2 - Board Chassis Location;"
  gSmbiosTokenSpaceGuid.PcdType003Record000 |{0x03, 0x00, 0x00, 0x00, 0x01, 0x0A, 0x02, 0x03, 0x04, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x05}
  gSmbiosTokenSpaceGuid.PcdType003Strings000|"Chassis Manufacturer;Chassis Version;Chassis Serial Number;Chassis Asset Tag;SKU Number;"
  gSmbiosTokenSpaceGuid.PcdType008Record000|{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x0F, 0x0D}
  gSmbiosTokenSpaceGuid.PcdType008Strings000|"J1A1;Keyboard;"
  gSmbiosTokenSpaceGuid.PcdType008Record001 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x0F, 0x0E}
  gSmbiosTokenSpaceGuid.PcdType008Strings001|"J1A1;Mouse;"
  gSmbiosTokenSpaceGuid.PcdType008Record002 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x0D, 0x1C}
  gSmbiosTokenSpaceGuid.PcdType008Strings002|"J2A1;TV OUT;"
  gSmbiosTokenSpaceGuid.PcdType008Record003 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x07, 0x1C}
  gSmbiosTokenSpaceGuid.PcdType008Strings003|"J2A2;CRT;"
  gSmbiosTokenSpaceGuid.PcdType008Record004 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x08, 0x09}
  gSmbiosTokenSpaceGuid.PcdType008Strings004|"J2A2;COM 1;"
  gSmbiosTokenSpaceGuid.PcdType008Record005 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x12, 0x10}
  gSmbiosTokenSpaceGuid.PcdType008Strings005|"J3A1;USB;"
  gSmbiosTokenSpaceGuid.PcdType008Record006 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x12, 0x10}
  gSmbiosTokenSpaceGuid.PcdType008Strings006|"J3A1;USB;"
  gSmbiosTokenSpaceGuid.PcdType008Record007 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x12, 0x10}
  gSmbiosTokenSpaceGuid.PcdType008Strings007|"J3A1;USB;"
  gSmbiosTokenSpaceGuid.PcdType008Record008 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x12, 0x10}
  gSmbiosTokenSpaceGuid.PcdType008Strings008|"J5A1;USB;"
  gSmbiosTokenSpaceGuid.PcdType008Record009 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x12, 0x10}
  gSmbiosTokenSpaceGuid.PcdType008Strings009|"J5A1;USB;"
  gSmbiosTokenSpaceGuid.PcdType008Record010 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x12, 0x10}
  gSmbiosTokenSpaceGuid.PcdType008Strings010|"J5A2;USB;"
  gSmbiosTokenSpaceGuid.PcdType008Record011 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x0B, 0x1F}
  gSmbiosTokenSpaceGuid.PcdType008Strings011|"J5A1;Network;"
  gSmbiosTokenSpaceGuid.PcdType008Record012 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x17, 0x02, 0x00, 0xFF}
  gSmbiosTokenSpaceGuid.PcdType008Strings012|"J9G2;OnBoard Floppy Type;"
  gSmbiosTokenSpaceGuid.PcdType008Record013 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x16, 0x02, 0x00, 0xFF}
  gSmbiosTokenSpaceGuid.PcdType008Strings013|"J7J1;OnBoard Primary IDE;"
  gSmbiosTokenSpaceGuid.PcdType008Record014 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x1F, 0x1D}
  gSmbiosTokenSpaceGuid.PcdType008Strings014|"J30;Microphone In;"
  gSmbiosTokenSpaceGuid.PcdType008Record015 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x1F, 0x1D}
  gSmbiosTokenSpaceGuid.PcdType008Strings015|"J30;Line In;"
  gSmbiosTokenSpaceGuid.PcdType008Record016 |{0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x1F, 0x1D}
  gSmbiosTokenSpaceGuid.PcdType008Strings016|"J30;Speaker Out;"
  gSmbiosTokenSpaceGuid.PcdType009Record000 |{0x09, 0x00, 0x00, 0x00, 0x01, 0xA6, 0x08, 0x00, 0x01, 0x01, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE0, 0x08, 0x00}
  gSmbiosTokenSpaceGuid.PcdType009Strings000|"J6C1;"
  gSmbiosTokenSpaceGuid.PcdType009Record001 |{0x09, 0x00, 0x00, 0x00, 0x01, 0xA6, 0x08, 0x00, 0x01, 0x02, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE1, 0x08, 0x00}
  gSmbiosTokenSpaceGuid.PcdType009Strings001|"J6D2;"
  gSmbiosTokenSpaceGuid.PcdType009Record002 |{0x09, 0x00, 0x00, 0x00, 0x01, 0xA6, 0x08, 0x00, 0x01, 0x03, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE2, 0x08, 0x00}
  gSmbiosTokenSpaceGuid.PcdType009Strings002|"J7C1;"
  gSmbiosTokenSpaceGuid.PcdType009Record003 |{0x09, 0x00, 0x00, 0x00, 0x01, 0xA6, 0x08, 0x00, 0x01, 0x04, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE3, 0x08, 0x00}
  gSmbiosTokenSpaceGuid.PcdType009Strings003|"J7D1;"
  gSmbiosTokenSpaceGuid.PcdType009Record004 |{0x09, 0x00, 0x00, 0x00, 0x01, 0xA8, 0x0A, 0x00, 0x01, 0x05, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE4, 0x0A, 0x00}
  gSmbiosTokenSpaceGuid.PcdType009Strings004|"J8C1;"
#_Start_L05_SMBIOS_ENABLE
  #
  # [Lenovo China Minimum BIOS Spec V1.40]
  #   3.3.5.2 Type 11 OEM string
  #     Lenovo defined country code string in SMBIOS type 11. All products which support
  #     country code string must obey following rules:
  #     1 Country code string use format "Country - XX", XX is ISO 3166 alpha-2 code
  #     2 there are spaces (0x20) before and after "-"
  #     3 there is only one SMBIOS type 11 structure in the system
  #     4 if XX = "MO", "HK" and "TW", BIOS must change the country code string to "Region - XX".
  #     5 if country code was not injected during manufacture process, BIOS must not build country code string in Type 11.
  #
  # [BIOS and Tool Requirements for Modern Preload Support Version 1.10]
  # SMBIOS Type 11 (OEM String)
  # BIOS are required to add a SMBIOS Type 11 (OEM String), to indicate the current system support modern preload.
  #
#  gSmbiosTokenSpaceGuid.PcdType011Record |{0x0B, 0x00, 0x00, 0x00, 0x03}
#  gSmbiosTokenSpaceGuid.PcdType011Strings|"OemString1;OemString2;OemString3;"
  gSmbiosTokenSpaceGuid.PcdType011Record |{0x0B, 0x00, 0x00, 0x00, 0x02}
  gSmbiosTokenSpaceGuid.PcdType011Strings|"Country - XX;Modern Preload;"
#_End_L05_SMBIOS_ENABLE
  gSmbiosTokenSpaceGuid.PcdType012Record |{0x0C, 0x00, 0x00, 0x00, 0x03}
  gSmbiosTokenSpaceGuid.PcdType012Strings|"ConfigOptions1;ConfigOptions2;ConfigOptions3;"
  gSmbiosTokenSpaceGuid.PcdType013Record |{0x0D, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}
  gSmbiosTokenSpaceGuid.PcdType013Strings|"en|US|iso8859-1,0;fr|FR|iso8859-1,0;zh|TW|unicode,0;ja|JP|unicode,0;it|IT|iso8859-1,0;es|ES|iso8859-1,0;de|DE|iso8859-1,0;pt|PT|iso8859-1,0;"
  #gSmbiosTokenSpaceGuid.PcdType013Record |{0x0D, 0x00, 0x00, 0x00, 0x08, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}
  #gSmbiosTokenSpaceGuid.PcdType013Strings|"enUS,0;frFR,0;zhTW,0;jaJP,0;itIT,0;esES,0;deDE,0;ptPT,0;"
  gSmbiosTokenSpaceGuid.PcdType015Record |{0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x04, 0x01, 0x78, 0x56, 0x34, 0x12, 0x00, 0x00, 0x00, 0x00, 0x80, 0x03, 0x02, 0x07, 0x00, 0x08, 0x04, 0x16, 0x00}
  gSmbiosTokenSpaceGuid.PcdType021Record000 |{0x15, 0x00, 0x00, 0x00, 0x07, 0x04, 0x04}
  gSmbiosTokenSpaceGuid.PcdType022Record000 |{0x16, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00}
  gSmbiosTokenSpaceGuid.PcdType022Strings000|"Fake;-Virtual Battery 0-;08/08/2010;Battery 0;CRB Battery 0;;LithiumPolymer;"
  gSmbiosTokenSpaceGuid.PcdType024Record |{0x18, 0x00, 0x00, 0x00, 0x00}
  gSmbiosTokenSpaceGuid.PcdType026Record000 |{0x1A, 0x00, 0x00, 0x00, 0x01, 0x42, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80}
  gSmbiosTokenSpaceGuid.PcdType026Strings000|"Voltage Probe Description;"
  gSmbiosTokenSpaceGuid.PcdType027Record000 |{0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x63, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x01}
  gSmbiosTokenSpaceGuid.PcdType027Strings000|"Cooling Device Description;"
  gSmbiosTokenSpaceGuid.PcdType028Record000 |{0x1C, 0x00, 0x00, 0x00, 0x01, 0x42, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80}
  gSmbiosTokenSpaceGuid.PcdType028Strings000|"Temperature Probe Description;"
  gSmbiosTokenSpaceGuid.PcdType032Record |{0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
  gSmbiosTokenSpaceGuid.PcdType039Record000 |{0x27, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x4B, 0x00, 0xA4, 0x21, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
  gSmbiosTokenSpaceGuid.PcdType039Strings000|"OEM Define 0;OEM Define 1;OEM Define 2;OEM Define 3;OEM Define 4;OEM Define 5;OEM Define 6;"
  gSmbiosTokenSpaceGuid.PcdType040Record |{0x28, 0x00, 0x00, 0x00, 0x02, 0x06, 0x09, 0x00, 0x05, 0x01, 0xAA, 0x06, 0x00, 0x00, 0x05, 0x02, 0x00}
  gSmbiosTokenSpaceGuid.PcdType040Strings|"PCIExpressx16;Compiler Version: VC 9.0;"
  gSmbiosTokenSpaceGuid.PcdType041Record000 |{0x29, 0x00, 0x00, 0x00, 0x01, 0x03, 0x01, 0x00, 0x00, 0x00, 0x10}
  gSmbiosTokenSpaceGuid.PcdType041Strings000|"IGD;"
  gSmbiosTokenSpaceGuid.PcdType128Record |{0x80, 0x00, 0x00, 0x00, 0x55, 0xAA, 0x55, 0xAA}
  gSmbiosTokenSpaceGuid.PcdType128Strings|"Oem Type 128 Test 1;Oem Type 128 Test 2;"
  gSmbiosTokenSpaceGuid.PcdType129Record |{0x81, 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x01}
  gSmbiosTokenSpaceGuid.PcdType129Strings|"Insyde_ASF_001;Insyde_ASF_002;"
  gSmbiosTokenSpaceGuid.PcdType130Record |{0x82, 0x00, 0x00, 0x00, 0x24, 0x41, 0x4D, 0x54, 0x01, 0x01, 0x01, 0x01, 0x01, 0xA5, 0xBF, 0x02, 0xC0, 0x00, 0x01, 0x00}
  gSmbiosTokenSpaceGuid.PcdType131Record | {0x83, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00, 0x00, 0x76, 0x50, 0x72, 0x6F, 0x00, 0x00, 0x00, 0x00}
  gSmbiosTokenSpaceGuid.PcdType136Record |{0x88, 0x00, 0x00, 0x00, 0xFF, 0xFF}
#
#  GPIO Controller Collection
#  --Pch L--
#  Ref: Intel\AlderLake\ClientOneSiliconPkg\Include\Pins\GpioPinsVer2Lp.h
#  LP_GROUP_GPP_B    0x00
#  LP_GROUP_GPP_T    0x01
#  LP_GROUP_GPP_A    0x02
#  LP_GROUP_GPP_R    0x03
#  LP_GROUP_SPI      0x04
#  LP_GROUP_GPD      0x05
#  LP_GROUP_GPP_S    0x06
#  LP_GROUP_GPP_H    0x07
#  LP_GROUP_GPP_D    0x08
#  LP_GROUP_GPP_U    0x09
#  LP_GROUP_VGPIO    0x0A
#  LP_GROUP_GPP_C    0x0B
#  LP_GROUP_GPP_F    0x0C
#  LP_GROUP_HVCMOS   0x0D
#  LP_GROUP_GPP_E    0x0E
#  LP_GROUP_JTAG     0x0F
#  LP_GROUP_CPU      0x10
#  LP_GROUP_VGPIO_3  0x11

  gI2cDeviceTokenSpaceGuid.PcdI2cTouchpad|{                                                 \  # The definition of I2C mouse PCD
    GUID({  0x234124E9,0x40B7,0x43EF,{0x9B,0x5E,0x97,0x47,0x08,0x08,0xD4,0x40}}),           \  # The unique GUID specific for this device, it will be part of device path node
    UINT32(0x0000002C),                                                                     \  # Slave address
    UINT32(0x00000001),                                                                     \  # Hardware revision
    0x00,                                                                                   \  # Interrupt GPIO pin active level, 0 = low active, 1 = high active
    0x0F,                                                                                   \  # Interrupt GPIO pin number
    UINT16(0x0020),                                                                         \  # HID descriptor register number
    UINT16(0x0102),                                                                         \  # HID device type, 0x0000 = Non-HID device, 0x0d00 = Touch panel, 0x0102 = Mouse, 0x0106 = Keyboard
    0x00,                                                                                   \  # Host controller number, 0 based
    0x01,                                                                                   \  # Bus configuration, 0x00 = V_SPEED_STANDARD, 0x01 = V_SPEED_FAST, 0x02 = V_SPEED_HIGH
    0x02                                                                                    \  # GPIO controller 0 based,
  }

#[-start-180828-IB11270210-add]#
#
# This PCD is automatically updated by FspBinFvsBaseAddress.exe. Please do NOT modify !!
#
!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspsBaseAddress|0xFFDC3000
!endif
#[-end-180828-IB11270210-add]#

#
# PCDs to control chipset policy.
#
# 0: Disable port
# 1: Enable port
# 2: AUTO means follow reference code implementation (PCIe)
#
# For USB 2.x, USB 3.x and SATA ports, bitmask specifies the configuration. For example: BIT0 means port 0 is enabled.
# For PCIe port, bitmask specifies the configuration and each port configuration use 2-bit. For example: 0x7 means port 0 is auto, port 1 is enabled, other ports are disabled.
# ADL-P: 12 PCH ports, 3 CPU port
# So the Bit[0:55] are used for PCH PCIe, and Bit[56:61] are used for CPU PCIe, 2 bits for one port.
# Example for enabled PCH PCIe port 0~3, CPU PCIe port 0:
# gInsydeTokenSpaceGuid.PcdH2OChipsetPciePortEnable|0x100000000000055

[Libraries]

[LibraryClasses]
  HidDescriptorLib|InsydeModulePkg/Library/HidDescriptorLib/HidDescriptorLib.inf
  BaseOemSvcKernelLib|$(PROJECT_PKG)/Library/BaseOemSvcKernelLib/BaseOemSvcKernelLib.inf
  BaseOemSvcChipsetLib|$(PROJECT_PKG)/Library/BaseOemSvcChipsetLib/BaseOemSvcChipsetLib.inf
  StdLib|InsydeModulePkg/Library/StdLib/StdLib.inf
  MultiBoardSupportLib|$(PROJECT_PKG)/Library/BoardInitLib/Pei/PeiMultiBoardSupportLib.inf
  FwUpdateLib|$(PROJECT_PKG)/Binary/FWUpdateLib/FWUpdateLib.inf

[LibraryClasses.common.SEC]

[LibraryClasses.common.PEI_CORE]
  PeiOemSvcKernelLib|$(PROJECT_PKG)/Library/PeiOemSvcKernelLib/PeiOemSvcKernelLib.inf

[LibraryClasses.common.PEIM]
  PeiOemSvcKernelLib|$(PROJECT_PKG)/Library/PeiOemSvcKernelLib/PeiOemSvcKernelLib.inf
  GpioCfgLib|$(PROJECT_PKG)/Library/GpioCfgLib/GpioCfgLib.inf
#_Start_L05_FEATURE_
  PeiOemSvcChipsetLib|$(PROJECT_PKG)/Library/PeiOemSvcChipsetLib/PeiOemSvcChipsetLib.inf
#_End_L05_FEATURE_

[LibraryClasses.common.DXE_CORE]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif
#_Start_L05_FEATURE_
  DxeOemSvcChipsetLib|$(PROJECT_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLib.inf
#_End_L05_FEATURE_

[LibraryClasses.common.UEFI_DRIVER]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif
#_Start_L05_FEATURE_
  DxeOemSvcChipsetLib|$(PROJECT_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLib.inf
#_End_L05_FEATURE_

[LibraryClasses.common.DXE_DRIVER]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif
#_Start_L05_FEATURE_
  DxeOemSvcChipsetLib|$(PROJECT_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLib.inf
#_End_L05_FEATURE_

[LibraryClasses.common.DXE_SMM_DRIVER]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif
  SmmOemSvcKernelLib|$(PROJECT_PKG)/Library/SmmOemSvcKernelLib/SmmOemSvcKernelLib.inf

[LibraryClasses.common.COMBINED_SMM_DXE]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif
  SmmOemSvcKernelLib|$(PROJECT_PKG)/Library/SmmOemSvcKernelLib/SmmOemSvcKernelLib.inf

[LibraryClasses.common.SMM_CORE]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif
  SmmOemSvcKernelLib|$(PROJECT_PKG)/Library/SmmOemSvcKernelLib/SmmOemSvcKernelLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  DxeOemSvcKernelLib|$(PROJECT_PKG)/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  DxeOemSvcKernelLibDefault|InsydeOemServicesPkg/Library/DxeOemSvcKernelLib/DxeOemSvcKernelLibDefault.inf {
    <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/InsydeOemServicesPkg/Library/DxeOemSvcKernelLib
  }
!endif

################################################################################
#
# Platform related components
#
################################################################################
[Components.$(PEI_ARCH)]
!if gSioGuid.PcdSioDummySupported
  !disable SioDummyPkg/SioDummyPei/SioDummyPei.inf
!endif

#[-start-201217-IB16560232-add]#
!disable MdeModulePkg/Universal/StatusCodeHandler/Pei/StatusCodeHandlerPei.inf
MdeModulePkg/Universal/StatusCodeHandler/Pei/StatusCodeHandlerPei.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
      DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!else
      # Use BaseDebugLibNull for save the binary size.
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable == TRUE
      NULL|BeepDebugFeaturePkg/Library/BeepStatusCodeHandlerLib/PeiBeepStatusCodeHandlerLib.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable == TRUE
      NULL|PostCodeDebugFeaturePkg/Library/PostCodeStatusCodeHandlerLib/PeiPostCodeStatusCodeHandlerLib.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable == TRUE
      SerialPortLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiPlatformSerialPortLib/PeiPlatformSerialPortLib.inf
!else
      SerialPortLib|MdePkg/Library/BaseSerialPortLibNull/BaseSerialPortLibNull.inf
!endif
      NULL|$(PLATFORM_SI_PACKAGE)/Library/TraceHubStatusCodeHandlerLib/PeiTraceHubStatusCodeHandlerLib.inf
  }
#[-end-201217-IB16560232-add]#

!if gInsydeTokenSpaceGuid.PcdCrisisRecoverySupported
!if gSiPkgTokenSpaceGuid.PcdVmdEnable == TRUE
  $(PROJECT_PKG)/Binary/Vmd/$(VMD_UEFI_DRIVER_VERSION)/Pei/Release/RstVmdPeim.inf
!endif
!endif

[Components.$(DXE_ARCH)]
# [-start-190902-IB16740052-add]
#
# Intel UEFI VMD
#
  $(PROJECT_PKG)/Binary/Vmd/$(VMD_UEFI_DRIVER_VERSION)/RstVmdDriver.inf
# [-end-190902-IB16740052-add]


#
# Microcode Updates
#
!if $(TARGET) == DEBUG && gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
  # For ChasmFalls 2 bios, Fv only have space for one microcode for debug build
  # Project need to check OneMicrocodeUpdates.inf, make sure it's right.
$(PROJECT_PKG)/Binary/MicrocodeUpdates/OneMicrocodeUpdates.inf {
  !if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
    <BuildOptions>
      *_*_*_GENFW_FLAGS = -a $(SLOT_SIZE) -p 0xFF
  !endif
}
!else
$(PROJECT_PKG)/Binary/MicrocodeUpdates/MicrocodeUpdates.inf {
  !if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
    <BuildOptions>
      *_*_*_GENFW_FLAGS = -a $(SLOT_SIZE) -p 0xFF
  !endif
}
!endif

#
# Intel UEFI GOP
#
!if $(TARGET) == DEBUG
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
    $(PROJECT_PKG)/Binary/UefiGop/$(VIDEO_UEFI_DRIVER_VERSION)/IntelGopDriver.inf
  !else
    $(PROJECT_PKG)/Binary/UefiGop/$(VIDEO_UEFI_DRIVER_VERSION)/IntelGopDriverAdlP.inf
  !endif
!else
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
    $(PROJECT_PKG)/Binary/UefiGop/$(VIDEO_UEFI_DRIVER_VERSION)/IntelGopDriver.inf
  !else
    $(PROJECT_PKG)/Binary/UefiGop/$(VIDEO_UEFI_DRIVER_VERSION)/IntelGopDriverAdlP.inf
  !endif
!endif

#
# Intel UEFI LAN
#
  $(PROJECT_PKG)/Binary/UefiLan/$(LAN_UEFI_DRIVER_VERSION)/UefiLan.inf


#[-start-201217-IB16560232-add]#
!disable MdeModulePkg/Universal/StatusCodeHandler/RuntimeDxe/StatusCodeHandlerRuntimeDxe.inf
MdeModulePkg/Universal/StatusCodeHandler/RuntimeDxe/StatusCodeHandlerRuntimeDxe.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
      DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!else
      # Use BaseDebugLibNull for save the binary size.
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable == TRUE
      NULL|BeepDebugFeaturePkg/Library/BeepStatusCodeHandlerLib/RuntimeDxeBeepStatusCodeHandlerLib.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable == TRUE
      NULL|PostCodeDebugFeaturePkg/Library/PostCodeStatusCodeHandlerLib/RuntimeDxePostCodeStatusCodeHandlerLib.inf
!endif
      NULL|$(PLATFORM_SI_PACKAGE)/Library/TraceHubStatusCodeHandlerLib/RuntimeDxeTraceHubStatusCodeHandlerLib.inf
  }

!disable MdeModulePkg/Universal/StatusCodeHandler/Smm/StatusCodeHandlerSmm.inf
MdeModulePkg/Universal/StatusCodeHandler/Smm/StatusCodeHandlerSmm.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
      DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!else
      # Use BaseDebugLibNull for save the binary size.
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable == TRUE
      NULL|BeepDebugFeaturePkg/Library/BeepStatusCodeHandlerLib/SmmBeepStatusCodeHandlerLib.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable == TRUE
      NULL|PostCodeDebugFeaturePkg/Library/PostCodeStatusCodeHandlerLib/SmmPostCodeStatusCodeHandlerLib.inf
!endif
      NULL|$(PLATFORM_SI_PACKAGE)/Library/TraceHubStatusCodeHandlerLib/SmmTraceHubStatusCodeHandlerLib.inf
  }
#[-end-201217-IB16560232-add]#

!if gChipsetPkgTokenSpaceGuid.PcdRetimerCapsuleUpdateSupported == TRUE
  #!disable $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer1Dxe/TbtRetimerCapsule1Dxe.inf
  #!disable $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer2Dxe/TbtRetimerCapsule2Dxe.inf
  !disable $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer3Dxe/TbtRetimerCapsule3Dxe.inf
!endif

#_Start_L05_RTK_USB_LAN_DRIVER_
!if $(L05_RTK_USB_LAN_DRIVER_SUPPORT) == YES
  $(PROJECT_PKG)\Binary\L05UefiLan\RtkUsbUndiDxe\RtkUsbUndiDxe.inf
  $(PROJECT_PKG)\Binary\L05UefiLan\DlUefiUndi\DlUefiUndi.inf  
!endif
#_End_L05_RTK_USB_LAN_DRIVER_

###################################################################################################
#
# BuildOptions Section - Define the module specific tool chain flags that should be used as
#                        the default flags for a module. These flags are appended to any
#                        standard flags that are defined by the build process. They can be
#                        applied for any modules or only those modules with the specific
#                        module style (EDK or EDKII) specified in [Components] section.
#
###################################################################################################
[BuildOptions.common.EDKII]
DEFINE RC_FLAGS = $(DSC_SIPKG_FEATURE_BUILD_OPTIONS) $(CC_FLAGS)

  GCC:*_*_IA32_CC_FLAGS          = -Wno-error -Wno-unused-local-typedefs -Wno-pointer-to-int-cast -Wno-unused-function -Wno-parentheses -DMDEPKG_NDEBUG $(RC_FLAGS)
  GCC:*_*_X64_CC_FLAGS           = -Wno-error -DMDEPKG_NDEBUG $(RC_FLAGS)
  GCC:*_*_IA32_JWASM_FLAGS       =
  GCC:*_*_X64_JWASM_FLAGS        =
  INTEL:*_*_*_CC_FLAGS           = /D MDEPKG_NDEBUG $(RC_FLAGS)
  MSFT:*_*_IA32_CC_FLAGS         = $(RC_FLAGS) -DASF_PEI
  MSFT:RELEASE_*_*_CC_FLAGS      = /D MDEPKG_NDEBUG $(RC_FLAGS)
!if $(EFI_DEBUG) == NO
  MSFT:DEBUG_*_*_CC_FLAGS        = /D MDEPKG_NDEBUG $(RC_FLAGS)
!else
  MSFT:DEBUG_*_*_CC_FLAGS        = $(RC_FLAGS)
!endif
!if $(POWER_ON_FLAG) == YES
  MSFT:*_*_*_CC_FLAGS            = /D POWER_ON_FLAG $(RC_FLAGS)
!endif
#[-start-190606-IB16990030-add]#
  MSFT:*_*_*_CC_FLAGS       = /Gw
#[-end-190606-IB16990030-add]#
  *_*_*_VFRPP_FLAGS         = $(CC_FLAGS) $(RC_FLAGS)
  *_*_X64_ASLPP_FLAGS  = $(RC_FLAGS)
  *_*_X64_ASLCC_FLAGS  = $(RC_FLAGS)
