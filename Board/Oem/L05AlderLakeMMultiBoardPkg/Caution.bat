@echo off
mode con cols=70 lines=15
color 0C
title  Caution! OpenSSL is missing!
@echo.
@echo  Caution! OpenSSL is missing!
@echo.
@echo    OpenSSL DOES NOT exist in the current source code.
@echo    Please follow instructions in
@echo    ChipsetPkg\Tools\OpenSSL\Readme.txt 
@echo    or 
@echo    InsydeH2O Power On Technical Reference Guide
@echo    to get/install OpenSSL for avoiding compiler errors.
@echo.
@echo.
@echo.
pause
exit 1
