/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*--
Module Name:

  OemCmos.h

Abstract:



--*/
#ifndef _OEM_CMOS_H_
#define _OEM_CMOS_H_

//_Start_L05_FEATURE_
//
// OEM Table 1 0x48 ~ 0x4F
//
#define EFI_L05_BIOS_SELF_HEALING_MODE_SWITCH                          0x4E
#define   V_EFI_L05_BIOS_SELF_HEALING_MODE_NORMAL                        0x00  // Default value
#define   V_EFI_L05_BIOS_SELF_HEALING_MODE_CRISIS_RECOVERY               0x01
#define   V_EFI_L05_BIOS_SELF_HEALING_MODE_CRISIS_RECOVERY_COMPLETED     0x02
#define   V_EFI_L05_BIOS_SELF_HEALING_MODE_ESP_BACKUP_IS_UNAVAILABLE     0x03
#define   V_EFI_L05_BIOS_SELF_HEALING_MODE_FORCE_ENTER_BSH               0x04
#define EFI_L05_NOVO_BUTTON_CRISIS_TEST                                0x4F

//
// OEM Table 1 0x6C ~ 0x7F
//
#define EFI_L05_COMPUTRACE_CMOS_ADDRESS_LOW                            0x7E
#define EFI_L05_COMPUTRACE_CMOS_ADDRESS_HIGH                           0x7F

//
// OEM Table 2 0x00 ~ 0x0F
//
// #TBD

//
// OEM Table 2  0x40 ~ 0x57
//
// #TBD

//
// OEM Table 2  0x60 ~ 0x7F
//
// #TBD
typedef enum {
//  OemTable1Reserve48 = 0x48,        // 0x48 ~ 0x4F : OEM Reserve
  EfiL05BiosSelfHealingModeSwitch     = EFI_L05_BIOS_SELF_HEALING_MODE_SWITCH, // 0x4E        : L05 BIOS Self Healing Mode Switch
  EfiL05NovoButtonCrisisRecoveryTest  = EFI_L05_NOVO_BUTTON_CRISIS_TEST,       // 0x4F        : L05 NovoButton Crisis RecoveryTest
//  OemTable1Reserve6C = 0x6C         // 0x6C ~ 0x7F : OEM Reserve
  EfiL05ComputraceCMOSAddressLow      = EFI_L05_COMPUTRACE_CMOS_ADDRESS_LOW,   // 0x7E        : L05 Computrace CMOS Address Low
  EfiL05ComputraceCMOSAddressHigh     = EFI_L05_COMPUTRACE_CMOS_ADDRESS_HIGH,  // 0x7F        : L05 Computrace CMOS Address High
} OEM_CMOS_TABLE1;
//_End_L05_FEATURE_

typedef enum {
  OemTable2Reserve00 = 0x00,        // 0x00 ~ 0x0F : OEM Reserve
  OemTable2Reserve40 = 0x40,        // 0x40 ~ 0x57 : OEM Reserve
  OemTable2Reserve60 = 0x60         // 0x60 ~ 0x7F : OEM Reserve
} OEM_CMOS_TABLE2;
#endif
