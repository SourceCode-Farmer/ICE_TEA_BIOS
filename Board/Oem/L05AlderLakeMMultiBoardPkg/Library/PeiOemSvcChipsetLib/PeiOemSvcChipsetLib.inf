## @file
#  Component description file for PEI OEM Services Chipset Lib instance.
#
#******************************************************************************
#* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiOemSvcChipsetLib
  FILE_GUID                      = A79B5B30-8CF0-4dc5-A526-E99BCD91BDC6
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = PeiOemSvcChipsetLib|PEI_CORE PEIM

[Sources]
  OemSvcChipsetDetectRecoveryRequest.c
#_Start_L05_FEATURE_
  OemSvcUpdateFspsUpd.c
  OemSvcUpdatePeiPolicyInitPostMem.c
  OemSvcUpdatePeiPolicyInitPreMem.c
#_End_L05_L05_FEATURE_

[Packages]
#_Start_L05_FEATURE_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
#_End_L05_L05_FEATURE_
  MdePkg/MdePkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  IntelFsp2Pkg/IntelFsp2Pkg.dec
  AlderLakeFspBinPkg/AlderLakeFspBinPkg.dec
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
#_Start_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_
  AlderLakeBoardPkg/BoardPkg.dec
#_End_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_
  InsydeModulePkg/InsydeModulePkg.dec
  SioDummyPkg/SioDummyPkg.dec

[LibraryClasses]
  PeiOemSvcChipsetLib
  BaseMemoryLib
  PeiInsydeChipsetLib
  DebugLib
  ConfigBlockLib
  PcdLib
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT
  VariableLib
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  PeiServicesLib  # PeiServicesGetBootMode
  BiosSelfHealingLib  # IsManufacturingMode
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
#_Start_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_
  PeiGetFvInfoLib  # PeiGetSectionFromFv
#_End_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_

[Guids]
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT
  gGraphicsPeiConfigGuid
  gEfiL05EspCustomizePostLogoInfoVariableGuid
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  gLockDownConfigGuid
  gRtcConfigGuid
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
#_Start_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_
  gVbtRvpFileGuid
#_End_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_

[Ppis]
#_Start_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_
  gReadyForGopConfigPpiGuid
#_End_L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE_

[Pcd]
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  gL05ServicesTokenSpaceGuid.PcdFlashFvEepromBase
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
