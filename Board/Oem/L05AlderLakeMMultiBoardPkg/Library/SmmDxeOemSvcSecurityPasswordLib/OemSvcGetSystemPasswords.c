/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcSecurityPasswordLib.h>

/**
  Provides an opportunity for
  (Lenovo Sepcification: Lenovo China Minimum BIOS Spec 139.pdf - CH.3.4.1 Rules for BIOS CMOS password)


  @param  SystemPasswordType            System Supervisor password or System User password
  @param  DataLength                    Password length
  @param  SystemPasswordBuffer          Password pointer

  @retval EFI_UNSUPPORTED               Returns unsupported for to do default setting
  @retval EFI_MEDIA_CHANGED             change load password buffer data
  @retval EFI_SCUESS                    skip default behavior
**/
EFI_STATUS
OemSvcGetSystemPasswords (
  IN     PASSWORD_TYPE                  SystemPasswordType,
  IN     UINTN                          DataLength,
  IN OUT UINT8                          *SystemPasswordBuffer
  )
{
  EFI_STATUS                            Status;
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  EFI_L05_VARIABLE_PROTOCOL             *L05VariablePtr;
  EFI_GUID                              PasswordGuid = {0};
#else
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;
#endif

  Status = EFI_UNSUPPORTED;

#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  L05VariablePtr = NULL;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gEfiL05VariableProtocolGuid, NULL, &L05VariablePtr);

  } else {
    Status = mSmst->SmmLocateProtocol (&gEfiL05VariableProtocolGuid, NULL, &L05VariablePtr);
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  switch (SystemPasswordType) {
    case SystemSupervisor:
      CopyMem (&PasswordGuid, &gL05SystemSuperPasswordGuid, sizeof(EFI_GUID));
      Status = EFI_MEDIA_CHANGED;
      break;

    case SystemUser:
      CopyMem (&PasswordGuid, &gL05SystemUserPasswordGuid, sizeof(EFI_GUID));
      Status = EFI_MEDIA_CHANGED;
      break;

    default:
      Status = EFI_UNSUPPORTED;
      break;
  }

  if (Status == EFI_MEDIA_CHANGED) {
    L05VariablePtr->GetVariable(
                      L05VariablePtr,
                      &PasswordGuid,
                      (UINT32 *)&DataLength,
                      SystemPasswordBuffer
                      );
  }

#else

  //
  // Get system password region.
  //
  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Check random number is valid.
  //
  if (!IsRandomNumberValid ()) {
    return EFI_UNSUPPORTED;
  }

  if (L05SystemSupervisorPasswordRegionBase != 0 && L05SystemUserPasswordRegionBase != 0) {
    switch (SystemPasswordType) {
      case SystemSupervisor:
        CopyMem ((VOID*)SystemPasswordBuffer, (VOID*)L05SystemSupervisorPasswordRegionBase, DataLength);
        Status = EFI_MEDIA_CHANGED;
        break;

      case SystemUser:
        CopyMem ((VOID*)SystemPasswordBuffer, (VOID*)L05SystemUserPasswordRegionBase, DataLength);
        Status = EFI_MEDIA_CHANGED;
        break;

      default:
        Status = EFI_UNSUPPORTED;
        break;
    }
  }
#endif

  //
  // Security password encode.
  //
  SecurityPasswordEncode (SystemPasswordBuffer, DataLength);

  return Status;
}

