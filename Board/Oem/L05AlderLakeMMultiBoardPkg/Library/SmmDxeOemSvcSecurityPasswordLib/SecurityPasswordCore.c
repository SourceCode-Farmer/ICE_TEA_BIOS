/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcSecurityPasswordLib.h>

/**
  Get L05 security password encode header pointer form BIOS region.

  @param  L05SecurityPasswordEncodeHeader  A pointer to the L05 security password encode header.

  @retval EFI_SUCCESS                      The operation completed successfully.
  @retval Others                           An unexpected error occurred.
**/
EFI_STATUS
GetL05SecurityPasswordEncodeHeader (
  L05_SECURITY_PASSWORD_ENCODE_HEADER   **L05SecurityPasswordEncodeHeader
  )
{
  EFI_STATUS                            Status;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;

  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get security password encode header form password region.
  //
  *L05SecurityPasswordEncodeHeader = (L05_SECURITY_PASSWORD_ENCODE_HEADER*)((UINTN) L05SystemSupervisorPasswordRegionBase + L05SystemSupervisorPasswordRegionSize + L05SystemUserPasswordRegionSize - sizeof (L05_SECURITY_PASSWORD_ENCODE_HEADER));

  return Status;
}

/**
  Calculate checksum for security password.

  @param  Buffer                        The pointer to the buffer to carry out the sum operation.
  @param  Length                        The size, in bytes, of Buffer.

  @return Checksum                      The checksum of Buffer.
**/
UINT16
CalculateChecksum (
  IN      UINT8                         *Buffer,
  IN      UINTN                         Length
  )
{
  UINT16                                Checksum;
  UINTN                                 Count;

  ASSERT (Buffer != NULL);
  ASSERT (Length <= (MAX_ADDRESS - ((UINTN) Buffer) + 1));

  Checksum = 0;
  for (Count = 0; Count < Length; Count++) {
    Checksum += *(Buffer + Count);
  }
  
  return Checksum;
}

/**
  Check checksum for security password encode header is vaild.

  @param  L05SecurityPasswordEncodeHeader  A pointer to the L05_SECURITY_PASSWORD_ENCODE_HEADER.

  @retval TRUE                             Checksum for security password encode header is vaild.
  @retval FALSE                            Checksum for security password encode header is not valid.
**/
BOOLEAN
IsSecurityPasswordEncodeHeaderChecksumValid (
  L05_SECURITY_PASSWORD_ENCODE_HEADER   *L05SecurityPasswordEncodeHeader
  )
{
  UINT16                                Checksum;

  Checksum = CalculateChecksum (((UINT8 *) L05SecurityPasswordEncodeHeader), (UINTN)(sizeof (L05_SECURITY_PASSWORD_ENCODE_HEADER) - sizeof (L05SecurityPasswordEncodeHeader->Checksum)));

  if (Checksum != L05SecurityPasswordEncodeHeader->Checksum) {
    return FALSE;
  }

  return TRUE;
}

/**
  Check random number for security password is vaild.

  @retval TRUE                          Random number for security password is vaild.
  @retval FALSE                         Random number for security password is not valid.
**/
BOOLEAN
IsRandomNumberValid (
  VOID
  )
{
  EFI_STATUS                            Status;
  L05_SECURITY_PASSWORD_ENCODE_HEADER   *L05SecurityPasswordEncodeHeader;

  Status = GetL05SecurityPasswordEncodeHeader (&L05SecurityPasswordEncodeHeader);

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  //
  // Check signature is vaild.
  //
  if (L05SecurityPasswordEncodeHeader->Signature != L05_SECURITY_PASSWORD_ENCODE_SIGNATURE) {
    return FALSE;
  }

  //
  // Check security password encode header checksum is vaild.
  //
  if (!IsSecurityPasswordEncodeHeaderChecksumValid (L05SecurityPasswordEncodeHeader)) {
    return FALSE;
  }

  //
  // Random number is vaild.
  //
  return TRUE;
}

/**
  Init random number for security password.
  1. Clear system password area.
  2. Init security password encode header.

  @param  SmmFwb                        A pointer to the EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL.
  @param  RandomNumber                  Random number for security password.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
InitRandomNumber (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb,
  UINT8                                 RandomNumber
  )
{
  EFI_STATUS                            Status;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;
  UINT8                                 *L05SystemPasswordArea;
  UINTN                                 L05SystemPasswordSize;
  L05_SECURITY_PASSWORD_ENCODE_HEADER   *L05SecurityPasswordEncodeHeader;

  //
  // Get system password region.
  //
  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get allocated system password area.
  //
  Status = GetSystemPasswordArea (
             &L05SystemPasswordArea,
             &L05SystemPasswordSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Clear system password area to 0xFF.
  //
  SetMem ((VOID *) L05SystemPasswordArea, L05SystemPasswordSize, 0xFF);

  //
  // Get security password encode header from system password area.
  //
  L05SecurityPasswordEncodeHeader = (L05_SECURITY_PASSWORD_ENCODE_HEADER *)((UINTN) L05SystemPasswordArea + L05SystemPasswordSize - sizeof (L05_SECURITY_PASSWORD_ENCODE_HEADER));

  //
  // Init security password encode header.
  //
  L05SecurityPasswordEncodeHeader->Signature    = L05_SECURITY_PASSWORD_ENCODE_SIGNATURE;
  L05SecurityPasswordEncodeHeader->RandomNumber = RandomNumber;
  L05SecurityPasswordEncodeHeader->Checksum     = CalculateChecksum (((UINT8 *) L05SecurityPasswordEncodeHeader), (sizeof (L05_SECURITY_PASSWORD_ENCODE_HEADER) - sizeof (L05SecurityPasswordEncodeHeader->Checksum)));

  Status = FlashBiosRegion (
             SmmFwb,
             L05SystemSupervisorPasswordRegionBase,
             L05SystemPasswordSize,
             L05SystemPasswordArea
             );

  if (L05SystemPasswordArea != NULL) {
    FreePool (L05SystemPasswordArea);
  }

  return Status;
}

/**
  Generate random number for security password.

  @retval Rand8                         Random number.
**/
UINT8
GenerateRandomNumber (
  VOID
  )
{
  BOOLEAN                               RngSuccess;
  UINTN                                 Index;
  UINT16                                Rand16;
  UINT8                                 Rand8;

  RngSuccess = FALSE;
  Rand8      = 0;
  
  for (Index = 0; Index < GET_RANDOM_NUMBER_RETRY_LIMIT; Index++) {
    RngSuccess = GetRandomNumber16 (&Rand16);
    Rand8      = (UINT8) Rand16;

    if (RngSuccess && (Rand8 != 0)) {
      break;
    }
  }

  return Rand8;
}

/**
  Get random number for security password.

  @param  RandomNumber                  A pointer to the random number for security password.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetSecurityPasswordRandomNumber (
  UINT8                                 *RandomNumber
  )
{
  EFI_STATUS                            Status;
  L05_SECURITY_PASSWORD_ENCODE_HEADER   *L05SecurityPasswordEncodeHeader;

  Status = GetL05SecurityPasswordEncodeHeader (&L05SecurityPasswordEncodeHeader);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  *RandomNumber = L05SecurityPasswordEncodeHeader->RandomNumber;

  return Status;
}

/**
  Encode security password by random number.

  @param  BufferPtr                     A pointer to the buffer for security password.
  @param  BufferSize                    The buffer size for security password.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SecurityPasswordEncode (
  IN  UINT8                             *BufferPtr,
  IN  UINTN                             BufferSize
  )
{
  EFI_STATUS                            Status;
  UINT8                                 RandomNumber;
  UINTN                                 Index;

  RandomNumber = 0;
  
  Status = GetSecurityPasswordRandomNumber (&RandomNumber);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (Index = 0; Index < BufferSize; Index++) {
    *(BufferPtr + Index) ^= RandomNumber;
  }

  return Status;
}

/**
  Initialize security password encode.

  @param  SmmFwb                        A pointer to the EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SecurityPasswordInit (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb
  )
{
  EFI_STATUS                            Status;
  UINT8                                 RandomNumber;

  RandomNumber = 0;

  //
  // Check random number for security password is vaild.
  //
  if (IsRandomNumberValid ()) {
    return EFI_SUCCESS;
  }
  //
  // Generate random number for security password.
  //
  RandomNumber = GenerateRandomNumber ();

  //
  // Init random number for security password.
  //
  Status = InitRandomNumber (SmmFwb, RandomNumber);

  return Status;
}

