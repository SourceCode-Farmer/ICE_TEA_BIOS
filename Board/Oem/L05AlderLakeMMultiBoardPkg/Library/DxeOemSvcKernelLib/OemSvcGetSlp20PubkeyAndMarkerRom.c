/** @file
  Provides an opportunity for OEM to update the Public Key ROM and SLP.

;******************************************************************************
;* Copyright (c) 2012, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcKernelLib.h>
//_Start_L05_ACPI_SLP_20_
#include <PiDxe.h>
#include <IndustryStandard/SLP2_0.h>
#include <IndustryStandard/Oa3_0.h>
#include <FlashRegionLayout.h>

#include <L05Config.h>
#include <L05Slp20Config.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesLib.h>  // GetSectionFromAnyFv
#include <Library/BaseMemoryLib.h>  // CopyMem
#include <Library/FlashRegionLib.h>
//_End_L05_ACPI_SLP_20_

#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
#include <Protocol/L05Variable.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/MemoryAllocationLib.h>
#include <Guid/L05VariableTool.h>
#endif

/**
  This OemService provides OEM to update the Public Key ROM and SLP (System-Locked Preinstallation) Marker ROM. 
  The update info will return to ACPI SLIC table. The service is following the SLP specification 2.0. 

  @param[in, out]  *PublicKey            If OEM updates the Public Key ROM, this parameter will return the address of ROM.
  @param[in, out]  *UpdatedPublicKey     If OEM updates the Public Key ROM, this parameter will return true.
  @param[in, out]  *SlpMarker            If OEM updates the SLP Marker ROM, this parameter will return the address of ROM.
  @param[in, out]  *UpdatedMarker   	 If OEM updates the SLP Marker ROM, this parameter will return true.

  @retval          EFI_UNSUPPORTED       Returns unsupported by default.
  @retval          EFI_SUCCESS           Get Slp2.0 information success.
  @retval          EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed. 
  @retval          Others                Base on OEM design.
**/
EFI_STATUS
OemSvcGetSlp20PubkeyAndMarkerRom (
  IN OUT EFI_ACPI_OEM_PUBLIC_KEY_STRUCTURE     *PublicKey,
  IN OUT BOOLEAN                               *UpdatedPublickey,
  IN OUT EFI_ACPI_SLP_MARKER_STRUCTURE         *SlpMarker,
  IN OUT BOOLEAN                               *UpdatedMarker
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//_Start_L05_ACPI_SLP_20_
  EFI_STATUS                                 Status = EFI_SUCCESS;
  EFI_L05_SLP20_AREA                         L05Slp20Area = {0};
  UINT8                                      *BufferPtr = NULL;
  UINT8                                      *BufferPtr2 = NULL;
  UINTN                                      Index = 0;
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  UINT32                                     DataLength;
  EFI_ACPI_SLP_MARKER_STRUCTURE              Buffer;
  EFI_L05_SLP20_AREA                         Data;
  EFI_L05_VARIABLE_PROTOCOL                  *EfiL05VariablePtr;
#else
//_Start_L05_VARIABLE_SERVICE_
  EFI_GUID                                   L05Slp20PubkeyGUID = EFI_L05_SLP20_PUB_KEY;
  UINTN                                      L05SLP20RegionBase = 0;
  UINTN                                      L05SLP20RegionSize = 0;
  VOID                                       *PubkeyRomImage = NULL;
  UINTN                                      PubkeyRomSize = 0;
//_End_L05_VARIABLE_SERVICE_
#endif

#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  DataLength         = 0;
  EfiL05VariablePtr  = NULL;

  Status = gBS->LocateProtocol (&gEfiL05VariableProtocolGuid, NULL, (VOID**)&EfiL05VariablePtr);
  if (!EFI_ERROR(Status)) {
    return Status;
  }

  DataLength = sizeof (EFI_L05_SLP20_AREA);
  //
  // Update L05 SLP20 area data
  //
  Status = EfiL05VariablePtr->GetVariable(
                                EfiL05VariablePtr,
                                &gL05OA2AreaDataGuid,
                                &DataLength,
                                &Data
                                );
  if (!EFI_ERROR(Status)) {
    CopyMem (&L05Slp20Area, &Data, sizeof (EFI_L05_SLP20_AREA));
  }

  //
  // Update L05 Marker key.
  //
  DataLength = sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE);
  Status = EfiL05VariablePtr->GetVariable(
                                EfiL05VariablePtr,
                                &gL05OA2SlicMakerGuid,
                                &DataLength,
                                &Buffer
                                );
  if (!EFI_ERROR(Status)) {
    CopyMem (&(L05Slp20Area.MarkerInfo), &Buffer, sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE));
  }
#else
//_Start_L05_VARIABLE_SERVICE_
  L05SLP20RegionBase = (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionSlp20Guid, 1);
  L05SLP20RegionSize = (UINTN) FdmGetNAtSize (&gL05H2OFlashMapRegionSlp20Guid, 1);

  //
  // Update L05 Pubkey
  //
  Status = GetSectionFromAnyFv (&L05Slp20PubkeyGUID, EFI_SECTION_RAW, 0, &PubkeyRomImage, &PubkeyRomSize);
  if (EFI_ERROR (Status) || (PubkeyRomSize != sizeof (EFI_ACPI_OEM_PUBLIC_KEY_STRUCTURE))) {
    return EFI_UNSUPPORTED;
  }

  CopyMem (PublicKey, PubkeyRomImage, PubkeyRomSize);
  *UpdatedPublickey = TRUE;

  //
  // Update L05 Marker key.
  //
  if ((L05SLP20RegionBase == (UINTN)L05_INVALID_VALUE) || (L05SLP20RegionSize == (UINTN)L05_INVALID_VALUE)) {
    return EFI_UNSUPPORTED;
  }

  //
  // Get L05 Marker Key from L05 SLP 2.0 FV Region
  //
  CopyMem (&L05Slp20Area, (VOID*)L05SLP20RegionBase, sizeof (EFI_L05_SLP20_AREA));
//_End_L05_VARIABLE_SERVICE_
#endif

  if ((L05Slp20Area.MarkerInfo.Type       == EFI_L05_SLP_MARKER_INVALID) ||
      (L05Slp20Area.RandomInfo.RandomNum0 == 0x00) ||
      (L05Slp20Area.RandomInfo.RandomNum0 == 0xFF) ) {
    return EFI_UNSUPPORTED;
  }

  //
  // Check L05 Marker Key is valid
  //
  Status = gBS->AllocatePool (EfiBootServicesData, sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE), &BufferPtr);
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  BufferPtr2 = (UINT8*)(&L05Slp20Area.MarkerInfo);
  for (Index = 0 ; Index < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE) ; Index++) {
    BufferPtr[Index] = BufferPtr2[Index] ^ L05Slp20Area.RandomInfo.RandomNum0;
  }

  if ((((EFI_ACPI_SLP_MARKER_STRUCTURE *) BufferPtr)->Type   != EFI_L05_SLP20_MARKER_TYPE)              || // SLP 20 marker type
      (((EFI_ACPI_SLP_MARKER_STRUCTURE *) BufferPtr)->Length != sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE))) {
    return EFI_UNSUPPORTED;
  }

  for (Index = 0 ; Index < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE) ; Index++) {
    if (BufferPtr[Index] != 0x00) {
      break;
    }
  }
  if (Index == sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE)) {
    return EFI_UNSUPPORTED;
  }

  CopyMem (SlpMarker, BufferPtr, sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE));
  *UpdatedMarker = TRUE;
  
//  return EFI_UNSUPPORTED;
  return EFI_MEDIA_CHANGED;
//_End_L05_ACPI_SLP_20
}
