/** @file
  Function for Get OA3 MSDM Specific Variable Service Data

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <IndustryStandard/Oa3_0.h>
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
#include <Library/DxeOemSvcKernelLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/L05Variable.h>
#include <Guid/L05VariableTool.h>
#endif

/**
  Get OA3 MSDM Data for Lenovo Specific Variable Service.

  @param[in, out]  MsdmData             The MSDM Data will be copied to this address if OEM updates MSDM Data here.

  @retval          EFI_SUCCESS          The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
  @retval          EFI_MEDIA_CHANGED    Alter the Configuration Parameter or hook code.
  @retval          Others               Base on OEM design.
**/
EFI_STATUS
GetOA3MsdmSpecificVariableServiceData (
  IN OUT EFI_ACPI_MSDM_DATA_STRUCTURE   *MsdmData
  )
{
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  EFI_STATUS                            Status;
  UINT32                                DataLength;
  VOID                                  *Data;
  EFI_L05_VARIABLE_PROTOCOL             *EfiL05VariablePtr;

  Status = gBS->LocateProtocol (&gEfiL05VariableProtocolGuid, NULL, (VOID **) &EfiL05VariablePtr);

  if (!EFI_ERROR (Status)) {
    DataLength = 0;
    Data       = NULL;
    Status = EfiL05VariablePtr->GetVariable (
                                  EfiL05VariablePtr,
                                  &gL05OA3MsdmDataGuid,
                                  &DataLength,
                                  Data);

    if (Status == EFI_BUFFER_TOO_SMALL) {
      Data   = AllocateZeroPool (DataLength);
      Status = EfiL05VariablePtr->GetVariable (
                                    EfiL05VariablePtr,
                                    &gL05OA3MsdmDataGuid,
                                    &DataLength,
                                    Data);

      if (!EFI_ERROR (Status)) {
        gBS->CopyMem (MsdmData, Data, sizeof (EFI_ACPI_MSDM_DATA_STRUCTURE));
      }

      FreePool (Data);
    }
  }

  return Status;
#else
  return EFI_SUCCESS;
#endif
}
