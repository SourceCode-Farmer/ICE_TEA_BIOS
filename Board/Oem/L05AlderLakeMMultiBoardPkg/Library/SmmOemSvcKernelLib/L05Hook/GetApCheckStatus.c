/** @file
  Feature Hook to get/modify ApCheck status

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Library/BaseMemoryLib.h>
#include <SetupConfig.h>
#include <Protocol/SmmVariable.h>
#include <Library/SmmServicesTableLib.h>
#include <H2OIhisi.h>

/**
 This function offers an interface to modify IHISI Sub function AH=11h,function "FbtsAPCheck" by feature

 @param[in, out]    ApStatus            On entry, pointer to ApStatus Address.
                                        On exit, points to updated ApStatus Address.

 @retval            EFI_SUCCESS         Feature alter the Configuration Parameter or hook code successfully.
 @retval            Others              Failure to modify Ap status by feature
*/
EFI_STATUS
GetApCheckStatus (
  IN OUT UINT8                          *ApStatus
  )
{
  EFI_SMM_VARIABLE_PROTOCOL             *SmmVariable;
  SYSTEM_CONFIGURATION                  SetupVariable;
  UINTN                                 DataSize;
  EFI_STATUS                            Status;

  SmmVariable = NULL;
  DataSize = 0;

  //
  // BIOS must prevent BIOS rolling back and provide an option
  // in setup to allow end user to disable this mechanism.
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmVariableProtocolGuid,
                    NULL,
                    &SmmVariable
                    );

  if (!EFI_ERROR (Status)) {
    DataSize = sizeof (SYSTEM_CONFIGURATION);
    Status = SmmVariable->SmmGetVariable (
                            L"Setup",
                            &gSystemConfigurationGuid,
                            NULL,
                            &DataSize,
                            &SetupVariable
                            );

    *ApStatus = MODEL_VERSION_CHECK;

    if (!EFI_ERROR (Status))  {
      if (SetupVariable.L05BiosBackFlash == 1) { //  1:Enable
        *ApStatus = AP_DO_NOTHING;
      }
    }
  }

  return Status;
}
