/** @file
 This function offers an interface to Hook IHISI Sub function AH=11h,function "FbtsAPCheck"

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/SmmOemSvcKernelLib.h>
//_Start_L05_BIOS_FLASH_UPDATE_
#include "L05Hook/GetApCheckStatus.h"
//_End_L05_BIOS_FLASH_UPDATE_

/**
 This function offers an interface to Hook IHISI Sub function AH=11h,function "FbtsAPCheck"

 @param[in, out]    ApStatus            On entry, pointer to ApStatus Address.
                                        On exit, points to updated ApStatus Address.


 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter or hook code.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcIhisiS11HookFbtsApCheck (
  IN OUT UINT8      *ApStatus
  )
{
/*++
  Todo:
  Add project specific code in here.
--*/
//_Start_L05_BIOS_FLASH_UPDATE_
  EFI_STATUS                            Status;

  Status = GetApCheckStatus (ApStatus);

  if (!EFI_ERROR (Status)) {
    return EFI_MEDIA_CHANGED;
  }
//_End_L05_BIOS_FLASH_UPDATE_

  return EFI_UNSUPPORTED;
}
