/** @file
  Provide an interface for Project to switch feature version.

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provide an interface for Project to switch feature version.

  @param  FeatureVersion                The point to the FeatureVersion for switch feature Version (The default version is 130)
                                        (Ex: If SPEC Version is 1.29, please set FeatureVersion to 129,
                                             If SPEC Version is 1.30, please set FeatureVersion to 130)

  @retval EFI_UNSUPPORTED               Returns unsupported for run default verion (The default version is latest version to following SPEC)
  @retval EFI_MEDIA_CHANGED             Returns changed version by user.
**/
EFI_STATUS
OemSvcSwitchFeatureVersion (
  IN OUT  UINT32                        *FeatureVersion
  )
{
  EFI_STATUS                            Status;
  
  Status = EFI_UNSUPPORTED;

  /*++
    Todo:
      Add project specific code in here.

  --*/

  return Status;
}
