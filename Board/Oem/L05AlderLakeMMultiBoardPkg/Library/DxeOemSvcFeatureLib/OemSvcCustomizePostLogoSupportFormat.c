/** @file
  Provide an interface for customize POST LOGO support format.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Guid/L05EspCustomizePostLogoInfoVariable.h>

/**
  Provide an interface for customize POST LOGO support format.

  @param  LOGO_SUPPORT_FORMAT           LOGO support format.

  @retval EFI_UNSUPPORTED               Feature will use LOGO support format by default.
  @retval EFI_MEDIA_CHANGED             Feature will refer Oem Svc to set LOGO support format.
**/
EFI_STATUS
OemSvcCustomizePostLogoSupportFormat (
  IN OUT  LOGO_SUPPORT_FORMAT           *LogoSupportFormat
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

