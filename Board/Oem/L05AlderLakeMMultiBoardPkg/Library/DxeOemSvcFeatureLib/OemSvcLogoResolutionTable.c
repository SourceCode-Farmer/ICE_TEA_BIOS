/** @file
  Provide an interface for logo resolution table

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>

#if 0 // Sample Implementation
L05_LOGO_RESOLUTION_TABLE               mOemSvcLogoResolutionTable[] =
{

#if (L05_LOGO_VERSION == 003)
  //   X,    Y,    GUID
  { 1024,  600, EFI_OEM_BADGING_FILENAME_BGRT_L05_1024x600},
  { 1280,  800, EFI_OEM_BADGING_FILENAME_BGRT_L05_1280x800},
  { 1366,  768, EFI_OEM_BADGING_FILENAME_BGRT_L05_1366x768},
  { 1600,  900, EFI_OEM_BADGING_FILENAME_BGRT_L05_1600x900},
  { 1920, 1080, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1080},
  { 1920, 1200, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1200},
  { 2160, 1440, EFI_OEM_BADGING_FILENAME_BGRT_L05_2160x1440},
  { 3200, 1800, EFI_OEM_BADGING_FILENAME_BGRT_L05_3200x1800},
  { 3840, 2160, EFI_OEM_BADGING_FILENAME_BGRT_L05_3840x2160},
#endif

};
#endif

/**
  Provide an interface for logo resolution table.

  @param  LogoResolutionTable           The point to the logo resolution table.
  @param  LogoResolutionCount           Logo resolution table count.

  @retval EFI_UNSUPPORTED               Feature will use LogoResolutionTable only by default.
  @retval EFI_MEDIA_CHANGED             Feature will refer Oem Svc to get LogoResolutionTable.
**/
EFI_STATUS
OemSvcLogoResolutionTable (
  IN OUT  L05_LOGO_RESOLUTION_TABLE     **LogoResolutionTable,
  IN OUT  UINTN                         *LogoResolutionCount
  )
{

  /*++
    Todo:
      Add project specific code in here.

  --*/
#if 0 // Sample Implementation

  *LogoResolutionTable = mOemSvcLogoResolutionTable;
  *LogoResolutionCount = sizeof (mOemSvcLogoResolutionTable) / sizeof (L05_LOGO_RESOLUTION_TABLE);
  
  return EFI_MEDIA_CHANGED;
  
#endif

  return EFI_UNSUPPORTED;
}

