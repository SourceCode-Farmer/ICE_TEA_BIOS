/** @file
  Provides an opportunity for ODM to get battery serial.

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcDxeComputrace.h>

/**
  Provides an opportunity for computrace to get battery serial.

  @param  DataLength                    Points to system serial length
  @param  DataBuffer                    Points to systme serail buffer

  @retval EFI_UNSUPPORTED               Returns unsupported.
  @retval EFI_MEDIA_CHANGED             Set system serial length and buffer success.
**/
EFI_STATUS
OemSvcGetBatterySerial (
  IN OUT  UINT8                         *DataLength,
  IN OUT  UINT8                         *DataBuffer
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}
