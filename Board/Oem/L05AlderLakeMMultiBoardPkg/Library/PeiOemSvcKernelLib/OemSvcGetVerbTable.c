/** @file
  This OemService is part of setting Verb Table. The function is created for setting verb table 
  to support Multi-Sku and return the table to common code to program.

;******************************************************************************
;* Copyright (c) 2014 - 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <MultiSkuInfoCommonDef.h>
#include <Ppi/VerbTable.h>
#include <Library/PeiOemSvcKernelLib.h>
#include <Library/PeiInsydeChipsetLib.h>
#include <Library/DebugLib.h>

//
// data type definitions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sample :                                                                                                      //
//                                                                                                               //
//   #define OEM_VERB_TABLE_ID_(n)         n                                                                     //
//                                                                                                               //
//   #define OEM_VERB_TABLE_(n)_HEADER(a)  0x10EC0880, // Vendor ID / Device ID                                  //
//                                         0x00000000, // SubSystem ID                                           //
//                                         0x02,       // RevisionId                                             //
//                                         0x01,       // Front panel support ( 1 = Yes, 2 = No )                //
//                                         0x000A,     // Number of Rear Jacks = 10                              //
//                                         0x0002      // Number of Front Jacks = 2                              //
//                                                                                                               //
//   #define OEM_VERB_TABLE_(n)_DATA(a)    0x01171CF0,0x01171D11,0x01171E11,0x01171F41, // NID(0x11): 0x411111F0 //
//                                         0x01271C40,0x01271D09,0x01271EA3,0x01271F99, // NID(0x12): 0x99A30940 //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// To define Verb Table ID.
//
#if 0
  // Sample code for VerbTable

  // HdaVerbTableAlc700
#define OEM_VERB_TABLE_ID_1          1
  //
  //  VerbTable: (Realtek ALC700) CNL RVP
  //  Revision ID = 0xff
  //  Codec Verb Table for CNL PCH boards
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0700
  //
#define OEM_VERB_TABLE_1_HEADER1     0x10EC0700, \
                                     0x00000000, \
                                     0xFF,       \
                                     0x01,       \
                                     161,        \
                                     0x0
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.0
  //Realtek HD Audio Codec : ALC700
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  //HDA Codec PnP ID : HDAUDIO\FUNC_01&VEN_10EC&DEV_0700&SUBSYS_10EC10F2
  //The number of verb command block : 17

  //    NID 0x12 : 0x411111F0
  //    NID 0x13 : 0x40000000
  //    NID 0x14 : 0x411111F0
  //    NID 0x15 : 0x411111F0
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x90170110
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11030
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40622005
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0
  //    NID 0x21 : 0x04211020
  //    NID 0x29 : 0x411111F0


  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC10F2
#define OEM_VERB_TABLE_1_DATA1  0x001720F2, \
  0x00172110, \
  0x001722EC, \
  0x00172310, \
  0x0017FF00, \
  0x0017FF00, \
  0x0017FF00, \
  0x0017FF00, \
  0x01271C00, \
  0x01271D00, \
  0x01271E00, \
  0x01271F40, \
  0x01371C00, \
  0x01371D00, \
  0x01371E00, \
  0x01371F40, \
  0x01471CF0, \
  0x01471D11, \
  0x01471E11, \
  0x01471F41, \
  0x01571CF0, \
  0x01571D11, \
  0x01571E11, \
  0x01571F41, \
  0x01671CF0, \
  0x01671D11, \
  0x01671E11, \
  0x01671F41, \
  0x01771C10, \
  0x01771D01, \
  0x01771E17, \
  0x01771F90, \
  0x01871CF0, \
  0x01871D11, \
  0x01871E11, \
  0x01871F41, \
  0x01971C30, \
  0x01971D10, \
  0x01971EA1, \
  0x01971F04, \
  0x01A71CF0, \
  0x01A71D11, \
  0x01A71E11, \
  0x01A71F41, \
  0x01B71CF0, \
  0x01B71D11, \
  0x01B71E11, \
  0x01B71F41, \
  0x01D71C05, \
  0x01D71D20, \
  0x01D71E62, \
  0x01D71F40, \
  0x01E71CF0, \
  0x01E71D11, \
  0x01E71E11, \
  0x01E71F41, \
  0x01F71CF0, \
  0x01F71D11, \
  0x01F71E11, \
  0x01F71F41, \
  0x02171C20, \
  0x02171D10, \
  0x02171E21, \
  0x02171F04, \
  0x02971CF0, \
  0x02971D11, \
  0x02971E11, \
  0x02971F41, \
  0x02050045, \
  0x02045289, \
  0x0205004A, \
  0x0204201B, \
  0x05850000, \
  0x05843888, \
  0x0205006F, \
  0x02042C0B, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040000, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040004, \
  0x02050028, \
  0x02040600, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204003C, \
  0x02050028, \
  0x0204FFD0, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040080, \
  0x02050028, \
  0x02040080, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040080, \
  0x02050028, \
  0x02040880, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204003A, \
  0x02050028, \
  0x02040DFE, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006A, \
  0x02050028, \
  0x0204005D, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006C, \
  0x02050028, \
  0x02040442, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040005, \
  0x02050028, \
  0x02040880, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040006, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040008, \
  0x02050028, \
  0x0204B000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204002E, \
  0x02050028, \
  0x02040800, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006A, \
  0x02050028, \
  0x020400C3, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006C, \
  0x02050028, \
  0x0204D4A0, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006A, \
  0x02050028, \
  0x020400CC, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006C, \
  0x02050028, \
  0x0204400A, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006A, \
  0x02050028, \
  0x020400C1, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006C, \
  0x02050028, \
  0x02040320, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040039, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204003B, \
  0x02050028, \
  0x0204FFFF, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204003C, \
  0x02050028, \
  0x0204FC20, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204003A, \
  0x02050028, \
  0x02041DFE, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C0, \
  0x02050028, \
  0x020401FA, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C1, \
  0x02050028, \
  0x0204DE23, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C2, \
  0x02050028, \
  0x02041C00, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C3, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C4, \
  0x02050028, \
  0x02040200, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C5, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C6, \
  0x02050028, \
  0x020403F5, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C7, \
  0x02050028, \
  0x0204AF1B, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C8, \
  0x02050028, \
  0x02041E0A, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400C9, \
  0x02050028, \
  0x0204368E, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400CA, \
  0x02050028, \
  0x020401FA, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400CB, \
  0x02050028, \
  0x0204DE23, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400CC, \
  0x02050028, \
  0x02041C00, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400CD, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400CE, \
  0x02050028, \
  0x02040200, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400CF, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400D0, \
  0x02050028, \
  0x020403F5, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400D1, \
  0x02050028, \
  0x0204AF1B, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400D2, \
  0x02050028, \
  0x02041E0A, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x020400D3, \
  0x02050028, \
  0x0204368E, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040040, \
  0x02050028, \
  0x0204800F, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040062, \
  0x02050028, \
  0x02048000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040063, \
  0x02050028, \
  0x02044848, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040064, \
  0x02050028, \
  0x02040800, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040065, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040066, \
  0x02050028, \
  0x02044004, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040067, \
  0x02050028, \
  0x02040802, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040068, \
  0x02050028, \
  0x0204890F, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040069, \
  0x02050028, \
  0x0204E021, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040070, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040071, \
  0x02050000, \
  0x02043330, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040072, \
  0x02050000, \
  0x02043333, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040073, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040074, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040075, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040076, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040050, \
  0x02050028, \
  0x020402EC, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040051, \
  0x02050028, \
  0x02044909, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040052, \
  0x02050028, \
  0x020440B0, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040046, \
  0x02050028, \
  0x0204C22E, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040047, \
  0x02050028, \
  0x02040C00, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040048, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040049, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204004A, \
  0x02050028, \
  0x02040000, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204004B, \
  0x02050028, \
  0x02041C00, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006A, \
  0x02050028, \
  0x02040090, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204006C, \
  0x02050028, \
  0x0204721F, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x0204009E, \
  0x02050028, \
  0x02040001, \
  0x02050029, \
  0x0204B024, \
  0x02050024, \
  0x02040010, \
  0x02050026, \
  0x02040004, \
  0x02050028, \
  0x02040500, \
  0x02050029, \
  0x0204B024

//
// To include verb table 1 with 1 codec header/data
// To congregate the verb table header/data.
//
#ifdef OEM_VERB_TABLE_ID_1
DEFINE_VERB_TABLE_LOCAL_HEADER_DATA_1(OEM_VERB_TABLE_ID_1);
COLLECT_DEFINE_VERB_TABLE_LOCAL_HEADER_DATA_1(OEM_VERB_TABLE_ID_1);
#endif
#endif

/**
  This OemService is part of setting Verb Table. The function is created for setting verb table 
  to support Multi-Sku and return the table to common code to program.

  @param  *VerbTableHeaderDataAddress    A pointer to VerbTable data/header

  @retval EFI_MEDIA_CHANGED              Get verb table data/header success.
  @retval Others                         Base on OEM design.
**/
EFI_STATUS
OemSvcGetVerbTable (
  OUT COMMON_CHIPSET_AZALIA_VERB_TABLE      **VerbTableHeaderDataAddress
  )
{
#if 0
  // Sample code for VerbTable  
  EFI_STATUS                      Status;
  PCH_SETUP                       PchSetup;

  Status = GetChipsetPchSetupVariablePei (&PchSetup);
  ASSERT_EFI_ERROR (Status);

  if (PchSetup.PchHdAudioCodecSelect == 0) {
    // BoardIdCannonlakeUDdr4Rvp
    *VerbTableHeaderDataAddress = VERB_TABLE_HEADER_DATA_BUFFER_ADDRESS(OEM_VERB_TABLE_ID_1);
  } else {
    *VerbTableHeaderDataAddress = VERB_TABLE_HEADER_DATA_BUFFER_ADDRESS(OEM_VERB_TABLE_ID_1);
  }
#endif

  return EFI_UNSUPPORTED;
}
