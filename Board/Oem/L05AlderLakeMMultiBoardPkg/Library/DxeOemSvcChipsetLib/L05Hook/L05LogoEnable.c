/** @file
  Source file for EFI OEM badging support driver.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "L05LogoEnable.h"
#include <SetupConfig.h>
#include <L05Project.h>
#include <L05Config.h>

//_Start_L05_CUSTOMIZE_MULTI_LOGO_
#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
#include <Guid/L05CustomizeMultiLogo.h>
#endif
//_End_L05_CUSTOMIZE_MULTI_LOGO_

#include <Guid/H2OBdsCheckPoint.h>
#include <Library/BadgingSupportLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/OemGraphicsLib.h>
#include <Library/FeatureLib/OemSvcLogoResolutionTable.h>
#include <Library/VariableLib.h>
#include <Library/H2OCpLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/SetupUtilityLib.h>

#include <Protocol/EdidDiscovered.h>
//_Start_L05_FEATURE_
#include <Protocol/L05EndOfBdsConnect.h>
//_End_L05_FEATURE_

EFI_OEM_BADGING_LOGO_DATA               mL05BadgingData[] =
{
  //
  // BGRT Logo
  //
#ifdef L05_MEDIA_LOGO_SUPPORT
  {
    EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1080,
    EfiBadgingSupportFormatGIF,
    EfiBadgingSupportDisplayAttributeCenter,
    0,
    0,
    EfiL05BgrtLogoCheck,
    EfiBadgingSupportImageBoot
  },
#else

#if (L05_LOGO_VERSION == 003)
  {
    EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1200,
    EfiBadgingSupportFormatTGA,
    EfiBadgingSupportDisplayAttributeCenter,
    0,
    0,
    EfiL05BgrtLogoCheck,
    EfiBadgingSupportImageBoot
  },
#else
  {
    EFI_OEM_BADGING_FILENAME_BGRT_L05_1366x768,
    EfiBadgingSupportFormatTGA,
    EfiBadgingSupportDisplayAttributeCenter,
    0,
    0,
    EfiL05BgrtLogoCheck,
    EfiBadgingSupportImageBoot
  },
#endif

#endif
};

UINTN  mL05BadgingDataSize = sizeof (mL05BadgingData) / sizeof (EFI_OEM_BADGING_LOGO_DATA);

L05_LOGO_RESOLUTION_TABLE               mLogoResolutionTable[] =
{
#ifdef L05_MEDIA_LOGO_SUPPORT
  //   X,    Y,    GUID
  { 1280,  768, EFI_OEM_BADGING_FILENAME_BGRT_L05_1280x768},
  { 1920, 1080, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1080},
#else

#if (L05_LOGO_VERSION == 003)
  //   X,    Y,    GUID
  { 1920, 1200, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1200},
#endif
#if (L05_LOGO_VERSION == 002)
  //   X,    Y,    GUID
  {  800, 1280, EFI_OEM_BADGING_FILENAME_BGRT_L05_800x1280},
  { 1366,  768, EFI_OEM_BADGING_FILENAME_BGRT_L05_1366x768},
  { 1600,  900, EFI_OEM_BADGING_FILENAME_BGRT_L05_1600x900},
  { 1920, 1080, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1080},
  { 1920, 1200, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1200},
  { 2160, 1440, EFI_OEM_BADGING_FILENAME_BGRT_L05_2160x1440},
  { 3200, 1800, EFI_OEM_BADGING_FILENAME_BGRT_L05_3200x1800},
  { 3480, 2160, EFI_OEM_BADGING_FILENAME_BGRT_L05_3480x2160},
#endif
#if (L05_LOGO_VERSION == 001)
  //   X,    Y,    GUID
  {  768, 1024, EFI_OEM_BADGING_FILENAME_BGRT_L05_768x1024},
  { 1024,  600, EFI_OEM_BADGING_FILENAME_BGRT_L05_1024x600},
  { 1366,  768, EFI_OEM_BADGING_FILENAME_BGRT_L05_1366x768},
  { 1600,  900, EFI_OEM_BADGING_FILENAME_BGRT_L05_1600x900},
  { 1920, 1080, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1080},
  { 1920, 1200, EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1200},
  { 3480, 2160, EFI_OEM_BADGING_FILENAME_BGRT_L05_3480x2160},
#endif

#endif
};

UINTN  mLogoResolutionCount = sizeof (mLogoResolutionTable) / sizeof (L05_LOGO_RESOLUTION_TABLE);

BOOLEAN                                 mIsLogoReadyToBootBeforeCp = FALSE;

VOID
L05BadgingdataOverridebyPCD (
  VOID
  )
{
  if (PcdGetBool (PcdL05CustomerBgrtLogoEnable)) {

    //
    // Change Customer BGRT Logo
    //
    gBS->CopyMem (&mL05BadgingData[L05BgrtLogo].FileName, PcdGetPtr (PcdL05CustomerBgrtLogoGuid), sizeof (EFI_GUID) / sizeof (UINT8));
    mL05BadgingData[L05BgrtLogo].Format = PcdGet8 (PcdL05CustomerBgrtLogoFormat);
  }
}

/**
 LOGO ready to boot before check point.

 @param[in] Event                       The Event this notify function registered to.
 @param[in] Handle                      The handle associated with a previously registered checkpoint handler.
**/
STATIC
VOID
EFIAPI
LogoReadyToBootBeforeCp (
  IN EFI_EVENT                          Event,
  IN H2O_CP_HANDLE                      Handle
  )
{
  mIsLogoReadyToBootBeforeCp = TRUE;
}

/**
 LOGO register ReadyToBootBeforeCp.

 @retval EFI_SUCCESS                    The function completed successfully.
 @retval Other                          An unexpected error occurred.
**/
EFI_STATUS
LogoRegisterReadyToBootBeforeCp (
  VOID
  )
{
  EFI_STATUS                           Status;
  H2O_CP_HANDLE                        CpHandle;
  
  //
  // Register notification on TPL_CALLBACK - 1 of BdsCpReadyToBootBefore event.
  //
  Status = H2OCpRegisterHandler (
             &gH2OBdsCpReadyToBootBeforeGuid,
             LogoReadyToBootBeforeCp,
             H2O_CP_MEDIUM,
             &CpHandle
             );
  
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBdsCpDisplayBeforeGuid, Status));
    return Status;
  }
  
  DEBUG ((EFI_D_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OBdsCpDisplayBeforeGuid, Status));
    
  return Status;
}

BOOLEAN
EfiL05BgrtLogoCheck (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  EFI_EDID_DISCOVERED_PROTOCOL          *EdidDiscovered;
  UINT32                                NativeResolutionX;
  UINT32                                NativeResolutionY;
  UINTN                                 LogoIndex;
  L05_LOGO_RESOLUTION_TABLE             *LogoResolutionTable;
  UINTN                                 LogoResolutionCount;
//_Start_L05_CUSTOMIZE_MULTI_LOGO_
#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
  L05_LOGO_RESOLUTION_INFO              CustomizeLogoInfo = {0};
#endif
//_End_L05_CUSTOMIZE_MULTI_LOGO_

  LogoResolutionTable = NULL;
  LogoResolutionCount = 0;
  NativeResolutionX   = 0;
  NativeResolutionY   = 0;
  LogoIndex           = (UINTN) L05_INVALID_VALUE;

  if (L05PostLogoIgnoreCheck ()) {
    return FALSE;
  }

  Status = gBS->LocateProtocol (
                  &gEfiEdidDiscoveredProtocolGuid,
                  NULL,
                  (VOID **) &EdidDiscovered
                  );

  if (!EFI_ERROR (Status)) {

    Status = GetResolutionByEdid (EdidDiscovered, &NativeResolutionX, &NativeResolutionY);

//_Start_L05_CUSTOMIZE_MULTI_LOGO_
#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT

    if (NativeResolutionX != 0 && NativeResolutionY != 0) {

      CustomizeLogoInfo.ResolutionX = NativeResolutionX;
      CustomizeLogoInfo.ResolutionY = NativeResolutionY;

      Status = gRT->SetVariable (
                      L05_CUSTOM_MULTI_LOGO_VARIABLE_NAME,
                      &gL05CustomizeMultiLogoGuid,
                      EFI_VARIABLE_BOOTSERVICE_ACCESS,
                      sizeof (L05_LOGO_RESOLUTION_INFO),
                      &CustomizeLogoInfo
                      );
    }

#endif
//_End_L05_CUSTOMIZE_MULTI_LOGO_

    if (!PcdGetBool (PcdL05CustomerBgrtLogoEnable)) {

      Status = OemSvcLogoResolutionTable (&LogoResolutionTable, &LogoResolutionCount);

      if (Status != EFI_MEDIA_CHANGED) {
        LogoResolutionTable = mLogoResolutionTable;
        LogoResolutionCount = mLogoResolutionCount;
      }

      for (Index = 0; Index < LogoResolutionCount; Index++) {
        if ((LogoResolutionTable[Index].X == NativeResolutionX) && (LogoResolutionTable[Index].Y == NativeResolutionY)) {
          LogoIndex = Index;
          break;
        }
      }

      if (LogoIndex != (UINTN) L05_INVALID_VALUE) {

        gBS->CopyMem ((VOID *) &mL05BadgingData[L05BgrtLogo].FileName, (VOID *) &(LogoResolutionTable[LogoIndex].Guid), sizeof (EFI_GUID) / sizeof (UINT8));
      }
    }
  }

  return TRUE;
}

/**
  POST logo ignore check.
  If POST is FastBoot or LegacyToEfi, Only need to show BGRT logo.


  @param  None

  @retval TRUE                          Cannot show POST logo.
  @retval FALSE                         Can show POST logo.
**/
BOOLEAN
L05PostLogoIgnoreCheck (
  VOID
  )
{
  EFI_STATUS                            Status;
  BOOLEAN                               IsPostLogoIgnore;
  VOID                                  *Interface;
  SYSTEM_CONFIGURATION                  *SystemConfiguration;
  UINT8                                 BootType;
  UINT16                                BootPriority;
  UINT8                                 LegacyToEfi;
  UINTN                                 BufferSize;

  Status              = EFI_SUCCESS;
  IsPostLogoIgnore    = FALSE;
  Interface           = NULL;
  SystemConfiguration = NULL;
  BootType            = EFI_BOOT_TYPE;
  BootPriority        = EFI_FIRST;
  LegacyToEfi         = 0;
  BufferSize          = 0;

#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
  if ((PcdGetBool (PcdH2OHddPasswordSupported) && PcdGetBool (PcdH2OHddPasswordUefiOsFastBootSupport)) || !PcdGetBool (PcdH2OHddPasswordSupported)) {
    //
    // Check gL05EndOfBdsConnectProtocolGuid to prevert ESP logo not ready.
    //
    Status = gBS->LocateProtocol (
                    &gL05EndOfBdsConnectProtocolGuid,
                    NULL,
                    (VOID **) &Interface
                    );
  
    if (EFI_ERROR (Status)) {
      return TRUE;
    }
  }
#endif

//_Start_L05_LEGACY_TO_UEFI_
  SystemConfiguration = CommonGetVariableData (SETUP_VARIABLE_NAME, &gSystemConfigurationGuid);
  
  if (SystemConfiguration != NULL) {
    BootType = SystemConfiguration->BootType;
    BootPriority = SystemConfiguration->BootNormalPriority;
  }

  FreePool (SystemConfiguration);

  //
  //To check BIOS is legacy to EFI behavior
  //If varaible is TRUE, it will follow EFI policy only show BGRT Logo
  //
  BufferSize = sizeof (LegacyToEfi);
  Status = gRT->GetVariable (
                  L"IsLegacyToEfi",
                  &gEfiGenericVariableGuid,
                  NULL,
                  &BufferSize,
                  &LegacyToEfi
                  );

  if ((BootType == DUAL_BOOT_TYPE) &&
      (BootPriority == LEGACY_FIRST) &&
      (EFI_ERROR (Status) || (LegacyToEfi == 0))) {
    IsPostLogoIgnore = mIsLogoReadyToBootBeforeCp ? FALSE : TRUE;
  }
//_End_L05_LEGACY_TO_UEFI_

  return IsPostLogoIgnore;
}

