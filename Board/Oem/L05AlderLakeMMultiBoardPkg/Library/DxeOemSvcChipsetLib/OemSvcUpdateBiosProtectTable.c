/** @file
   Provide OEM to set BIOS region protection before boot to OS.
   
;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcChipsetLib.h> 
//_Start_L05_FEATURE_
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
//_End_L05_FEATURE_
/**
  Customize BIOS protect region before boot to OS. 

  @param[out]        *BiosRegionTable    Pointer to BiosRegion Table.
  @param[out]        ProtectRegionNum    The number of Bios protect region instances.
  
  @retval            EFI_UNSUPPORTED     Returns unsupported by default, OEM did not implement this function.
  @retval            EFI_MEDIA_CHANGED   Provide table for kernel to set protect region and lock flash program registers.
                                         This table will be freed by kernel.
**/
EFI_STATUS
OemSvcUpdateBiosProtectTable (
  OUT BIOS_PROTECT_REGION           **BiosRegionTable,
  OUT UINT8                         *ProtectRegionNum
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
#if 0 // Sample Implementation
  //
  // When OEM needs to add protect region, this map provides the protected BIOS region.
  // Other, OEM just redefine these PCDs in your projectpkg .dsc file.
  // These protected regions will be locked by BiosProtect Driver and only can be updated by Secure Flash mechanism 
  //
  UINT8         BiosProtectRegionNumber;

  FreePool (*BiosRegionTable);
  *BiosRegionTable = NULL;
  BiosProtectRegionNumber = 3;
  
  if (BiosRegionTable == NULL ||  ProtectRegionNum == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  
  *BiosRegionTable = AllocateZeroPool (sizeof (BIOS_PROTECT_REGION) * (BiosProtectRegionNumber));
  if (*BiosRegionTable == NULL) {
    DEBUG ((DEBUG_INFO, "BiosRegionTable allocate memory resource failed.\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  (*BiosRegionTable)[0].Base = PcdGet32 (PcdFlashFvMainBase);
  (*BiosRegionTable)[0].Size = PcdGet32 (PcdFlashFvMainSize);
  (*BiosRegionTable)[1].Base = PcdGet32 (PcdFlashFvRecoveryBase);
  (*BiosRegionTable)[1].Size = PcdGet32 (PcdFlashFvRecoverySize);
  (*BiosRegionTable)[2].Base = PcdGet32 (PcdFlashNvStorageMicrocodeBase);
  (*BiosRegionTable)[2].Size = PcdGet32 (PcdFlashNvStorageMicrocodeSize);

  *ProtectRegionNum = BiosProtectRegionNumber;
  
  return EFI_MEDIA_CHANGED;
#endif

#ifndef L05_ALL_FEATURE_ENABLE
  return EFI_UNSUPPORTED;

#else
//
// L05 region is in flash protection range, that will make L05 region can't flash.
// Independent VARIABLE_DEFAULTS and FVMAIN region of protected region table,
// avert L05 region in flash protection range.
//
  UINT8                                 BiosProtectRegionNumber;

  FreePool (*BiosRegionTable);
  *BiosRegionTable        = NULL;
  *ProtectRegionNum       = 0;
  BiosProtectRegionNumber = 4;
  
  if (BiosRegionTable == NULL ||  ProtectRegionNum == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  
  *BiosRegionTable = AllocateZeroPool (sizeof (BIOS_PROTECT_REGION) * (BiosProtectRegionNumber));
  if (*BiosRegionTable == NULL) {
    DEBUG ((EFI_D_INFO, "BiosRegionTable allocate memory resource failed.\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  // Set Protected Region in Protected Region Table [0]: NV_FACTORY_COPY
  (*BiosRegionTable)[0].Base = PcdGet32 (PcdFlashNvStorageFactoryCopyBase);
  (*BiosRegionTable)[0].Size = PcdGet32 (PcdFlashNvStorageFactoryCopySize);
  // Set Protected Region in Protected Region Table [1]: VARIABLE_DEFAULTS
  (*BiosRegionTable)[1].Base = PcdGet32 (PcdFlashNvStorageVariableDefaultsBase);
  (*BiosRegionTable)[1].Size = PcdGet32 (PcdFlashNvStorageVariableDefaultsSize);
  // Set Protected Region in Protected Region Table [2]: FVMAIN
  (*BiosRegionTable)[2].Base = PcdGet32 (PcdFlashFvMainBase);
  (*BiosRegionTable)[2].Size = PcdGet32 (PcdFlashFvMainSize);
#ifndef L05_BIOS_SELF_HEALING_SUPPORT
  // Set Protected Region in Protected Region Table [3]: FW_BINARIES ~ FV_RECOVERY0 
  (*BiosRegionTable)[3].Base = PcdGet32 (PcdFlashFirmwareBinariesFvBase);
  (*BiosRegionTable)[3].Size = PcdGet32 (PcdFlashFirmwareBinariesFvSize) +
                               PcdGet32 (PcdFlashNvStorageMicrocodeSize) +
                               PcdGet32 (PcdFlashFvRecovery2Size) +
                               PcdGet32 (PcdFlashFvRecoverySize) +
                               PcdGet32 (PcdFlashFvFspSSize) +
                               PcdGet32 (PcdFlashFvFspMSize) +
                               PcdGet32 (PcdFlashFvFspTSize) +
                               (UINT32)(PcdGet64 (PcdH2OFlashDeviceMapSize)) +
                               PcdGet32 (PcdFlashFvRecovery0Size);
#else
  // Set Protected Region in Protected Region Table [3]: FW_RESILIENCY_RESERVED ~ FV_RECOVERY0
  (*BiosRegionTable)[3].Base = PcdGet32 (PcdFwResiliencyReservedBase);
  (*BiosRegionTable)[3].Size = PcdGet32 (PcdFwResiliencyReservedSize) * 2;
#endif

  *ProtectRegionNum = BiosProtectRegionNumber;

  return EFI_MEDIA_CHANGED;
#endif

}

