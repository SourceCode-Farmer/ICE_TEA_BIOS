/** @file
  Provide hook function for OEM to set DSW mode to EC.

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/CommonEcLib.h>
#include <PchPolicyCommon.h>
#include <Library/EcMiscLib.h>

/**
 Set DSW mode to EC. 
 
 @param[out]        SetDswModeStatus    The status of set DSW mode.
 @param[in]         DswMode             DSW mode.
                                          0x0 Disable DSW mode
                                          0x1 Enable DSW in S5 (DC)
                                          0x2 Enable DSW in S5 (Always)
                                          0x3 Enable DSW in S4-S5 (DC)
                                          0x4 Enable DSW in S4-S5 (Always)

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEcSetDswMode (
  OUT EFI_STATUS   *SetDswModeStatus,
  IN  UINT8        DswMode
  )
{
  *SetDswModeStatus = SendEcCommand (SMC_DEEPSX_CMD);
  if (*SetDswModeStatus == EFI_SUCCESS) {
    switch (DswMode) {
      case PchDpS5BatteryEn:
        *SetDswModeStatus = SendEcData (0x01);
        break;
      case PchDpS5AlwaysEn:
        *SetDswModeStatus = SendEcData (0x02);
        break;
      case PchDpS4S5BatteryEn:
        *SetDswModeStatus = SendEcData (0x03);
        break;
      case PchDpS4S5AlwaysEn:
        *SetDswModeStatus = SendEcData (0x04);
        break;        
      case PchDeepSxPolDisable:
      default:
        *SetDswModeStatus = SendEcData (0x00);
        break;
    }
  }

  return EFI_MEDIA_CHANGED;
}
