@REM 
@REM Handle PostBuildProcess in BIOS Guard Support.
@REM (Usage1) Update Offset information to BiosGuardSetting.ini
@REM (Usage2) ChasmeFall Gen2, create BIOSGuard Header and Certificate for PBB/PBBR/SBB and injected back to BIOS image.
@REM 



@REM _Start_L05_FEATURE_
@REM @set BIOSGUARD_SETTING_PATH=%WORKSPACE%\Board\Intel\%PROJECT_PKG%\PlatformConfig\BiosGuard
@set BIOSGUARD_SETTING_PATH=%WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\PlatformConfig\BiosGuard
@REM _End_L05_FEATURE_
@set BUILD_PATH=%WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%

@for /f "tokens=4" %%a in ('find "BIOS_GUARD_SUPPORT" Project.env') do set BIOSGUARD=%%a
@if %BIOSGUARD% EQU NO goto EndBiosGuardSetting

@
@REM (Usage1) Update Offset information to BiosGuardSetting.ini
@

@REM Tool Usage : BiosGuardSettingUpdate.exe {PcdList_Path} {BiosGuardSetting.ini_Path}
@%CHIPSET_PKG_TOOLS_PATH%\BiosGuardSettingUpdate.exe %BUILD_PATH%\X64\PcdList.txt %BIOSGUARD_SETTING_PATH%\BiosGuardSetting.ini ReservedList.txt

@
@REM (Usage2) ChasmeFall Gen2, create BIOSGuard Header and Certificate for PBB/PBBR/SBB and injected back to BIOS image.
@

@if %RESILIENCY_BUILD% EQU FALSE goto EndBiosGuardResiliency

@REM 
@REM (Step1) Into Temp Folder to get SBB part (SBB.fd).
@REM 
@cd %TEMP_FOLDER_PATH%

@REM 
@REM (Step2) Create BiosGuard Header for PBB/PBBR/SBB (PBB_BGHdr.bin/PBBR_BGHdr.bin/SBB_BGHdr.bin)
@REM 
@%CHIPSET_PKG_TOOLS_PATH%\GenBiosGuardHdr.exe -i %BIOSGUARD_SETTING_PATH%\BiosGuardSetting.ini -p %BIOSGUARD_SETTING_PATH%\BGSL\InsydeBiosGuardRomTransferScript.bin -m 4 -s %PBB_OFFSET% -d %PBBR_OFFSET% -l %PBB_SIZE% -o PBB_BGHdr.bin
@%CHIPSET_PKG_TOOLS_PATH%\GenBiosGuardHdr.exe -i %BIOSGUARD_SETTING_PATH%\BiosGuardSetting.ini -p %BIOSGUARD_SETTING_PATH%\BGSL\InsydeBiosGuardRomTransferScript.bin -m 4 -s %PBBR_OFFSET% -d %PBB_OFFSET% -l %PBB_SIZE% -o PBBR_BGHdr.bin
@%CHIPSET_PKG_TOOLS_PATH%\GenBiosGuardHdr.exe -i %BIOSGUARD_SETTING_PATH%\BiosGuardSetting.ini -p %BIOSGUARD_SETTING_PATH%\BGSL\InsydeBiosGuardSbbRevertScript.bin -f SBB.fd -m 5 -d %SBB_OFFSET% -l %SBB_SIZE% -o SBB_BGHdr.bin

@REM 
@REM (Step3) Get BiosGuard Certificate (PBB_Sign.cer\PBBR_Sign.cer\SBB_Sign.cer)
@REM 
@Copy /y /b *_BGHdr.bin *_Sign.bin
@Copy /y /b SBB_BGHdr.bin+SBB.fd SBB_Sign.bin
@%CHIPSET_PKG_TOOLS_PATH%\Signer\iEFIFlashSigner.exe engineer -signbiosguard %TEMP_FOLDER_PATH%\PBBR_Sign.bin -n "QA Certificate."
@%CHIPSET_PKG_TOOLS_PATH%\Signer\iEFIFlashSigner.exe engineer -signbiosguard %TEMP_FOLDER_PATH%\PBB_Sign.bin -n "QA Certificate."
@%CHIPSET_PKG_TOOLS_PATH%\Signer\iEFIFlashSigner.exe engineer -signbiosguard %TEMP_FOLDER_PATH%\SBB_Sign.bin -n "QA Certificate."
@Copy /y /b %CHIPSET_PKG_TOOLS_PATH%\Signer\*_Sign.bin.p7.cer %TEMP_FOLDER_PATH%\*_Sign.cer
@del %CHIPSET_PKG_TOOLS_PATH%\Signer\*_Sign.bin*

@REM 
@REM (Step4) Combine Header and Certificate (BiosGuard_PBB.bin\BiosGuard_PBBR.bin\BiosGuard_SBB.bin)
@REM 
@set PBB_BGSL_FILE_GUID=DD1481F4-12F9-4D69-AC11-855FFAC32816
@set PBBR_BGSL_FILE_GUID=9370D781-EC98-4D3C-B88B-F8676A54BDEA
@set SBB_BGSL_FILE_GUID=36914867-7EBB-4C12-A7BA-BA51191B0424

@copy /y /b PBB_BGHdr.bin + PBB_Sign.cer BiosGuard_PBB.bin
@copy /y /b PBBR_BGHdr.bin + PBBR_Sign.cer BiosGuard_PBBr.bin
@copy /y /b SBB_BGHdr.bin + SBB_Sign.cer BiosGuard_SBB.bin

@REM 
@REM (Step5) injected back to BIOS image.
@REM 
@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "BiosGuard_PBB" -o BiosGuard_PBB.ui
@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW BiosGuard_PBB.bin -o BiosGuard_PBB.raw
@call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %PBB_BGSL_FILE_GUID% -o BiosGuard_PBB.ffs -i BiosGuard_PBB.raw -i BiosGuard_PBB.ui

@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "BiosGuard_PBBr" -o BiosGuard_PBBr.ui
@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW BiosGuard_PBBr.bin -o BiosGuard_PBBr.raw
@call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %PBBR_BGSL_FILE_GUID% -o BiosGuard_PBBr.ffs -i BiosGuard_PBBr.raw -i BiosGuard_PBBr.ui

@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "BiosGuard_SBB" -o BiosGuard_SBB.ui
@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW BiosGuard_SBB.bin -o BiosGuard_SBB.raw
@call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %SBB_BGSL_FILE_GUID% -o BiosGuard_SBB.ffs -i BiosGuard_SBB.raw -i BiosGuard_SBB.ui

@call %EDK_TOOLS_BIN%\FMMT.exe -r SbbDigest_temp.fd %FV_GUID% BiosGuard_PBB BiosGuard_PBB.ffs BIOS_temp.fd
@copy /y /b Bios_temp.fd temp.fd
@call %EDK_TOOLS_BIN%\FMMT.exe -r temp.fd %FV_GUID% BiosGuard_PBBr BiosGuard_PBBr.ffs BIOS_temp.fd
@copy /y /b BIOS_temp.fd temp.fd
@call %EDK_TOOLS_BIN%\FMMT.exe -r temp.fd %FV_GUID% BiosGuard_SBB BiosGuard_SBB.ffs BIOS_temp.fd
@copy /y /b %TEMP_FOLDER_PATH%\BIOS_temp.fd %BUILD_PATH%\FV\%PLATFORM_TYPE%.fd
@cd ..
:EndBiosGuardResiliency
:EndBiosGuardSetting

@REM Continue PostBuildProcess.
