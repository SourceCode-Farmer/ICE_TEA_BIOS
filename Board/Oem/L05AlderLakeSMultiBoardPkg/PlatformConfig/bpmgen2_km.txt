BpmGen Tool Version 2.7.5
Command Line: C:\ADL_branch35\Intel\AlderLake\AlderLakePlatSamplePkg\Tools\BpmGen2\BpmGen2.exe KMGEN -KEY pubkey.pem BPM -KM C:\ADL_branch35\Build\AlderLakeSMultiBoardPkg\RELEASE_DEVTLSxVC16\FV\KeyManifest.bin -SIGNKEY keyprivkey.pem -SIGHASHALG SHA384 -SCHEME RSAPSS -KMKHASH SHA384 -KMID 0x01 -SVN 1 -d:2 

Start KM Gen function

 Adding Public Key pubkey.pem, 
 Producing KM binary output file C:\ADL_branch35\Build\AlderLakeSMultiBoardPkg\RELEASE_DEVTLSxVC16\FV\KeyManifest.bin 
 KM Signing Key keyprivkey.pem,  Sign Hash: 12,  Signing Scheme: 22,  KM Key Hash: 12,  Key Manifest ID: 1,  KMSVN: 1, 
pKmOutFileName C:\ADL_branch35\Build\AlderLakeSMultiBoardPkg\RELEASE_DEVTLSxVC16\FV\KeyManifest.bin || KeyCount 1 || SigSource 1 


Generating KM 
 - Get Public Keys (1)
Usage: 1, Alg: 0xB 
##  DEBUG (ReadKey) - 0  pubkey.pem - looking for Public Key 
##  DEBUG (ReadInputFile) start - Filename: pubkey.pem
@@ malloc 0139ac08
##  DEBUG (ReadInputFile) End - Filename: pubkey.pem
##  Opened Key file
##    FileData = 2d 2d 2d 2d 
##  DEBUG (GetDerFromPEM) 
-- Found 0x0A at offset 27 (0x1b)
-- pPemHeader: 0x139ac08
-- pPemHeader: -----BEGIN PUBLIC KEY-----
##  processing header at 139ac08 
## Base64 data starting address: 139ac24 
## Found '-' at offset 434 (0x1b2) - start of trailer 
## Base64 data length: 0x196  (0x196)
@@ malloc 0137ca58
## Placing result in DER Buffer at address: 137ca58 (size=304 0x130)

## DEBUG (Base64Decode) pB64String: 139ac24, B64Length: 0x196 pDecodeLocation: 137ca58
## PEM Type = 9
##  DEBUG (ReadKey) - Processing DER
##  Debug (GetKeyFromDER) - 0  PEM Type = 9
## DEBUG (ParseASN1) ASN.1 Size: 294 Starting Index: 0
##  DEBUG Index: 0, Offset: 0x0 :: 30 82 1 22
##  [0]  Adjust (2), Type: 0x30, Length: 290, Value[0] = 30
##  Processing embedded Sequence
## DEBUG (ParseASN1) ASN.1 Size: 290 Starting Index: 1
##  DEBUG Index: 1, Offset: 0x0 :: 30 d 6 9
##  [1]  Adjust (0), Type: 0x30, Length: 13, Value[0] = 6
##  Processing embedded Sequence
## DEBUG (ParseASN1) ASN.1 Size: 13 Starting Index: 2
##  DEBUG Index: 2, Offset: 0x0 :: 6 9 2a 86
##  [2]  Adjust (0), Type: 0x6, Length: 9, Value[0] = 2a
##  DEBUG Index: 3, Offset: 0xb :: 5 0 3 82
##  [3]  Adjust (0), Type: 0x5, Length: 0, Value[0] = 3
##  DEBUG Index: 4, Offset: 0xf :: 3 82 1 f
##  [4]  Adjust (2), Type: 0x3, Length: 271, Value[0] = 0
##  Processing embedded Sequence in BitString
## DEBUG (ParseASN1) ASN.1 Size: 270 Starting Index: 5
##  DEBUG Index: 5, Offset: 0x0 :: 30 82 1 a
##  [5]  Adjust (2), Type: 0x30, Length: 266, Value[0] = 2
##  Processing embedded Sequence
## DEBUG (ParseASN1) ASN.1 Size: 266 Starting Index: 6
##  DEBUG Index: 6, Offset: 0x0 :: 2 82 1 1
##  [6]  Adjust (2), Type: 0x2, Length: 257, Value[0] = 0
##  DEBUG Index: 7, Offset: 0x105 :: 2 3 1 0
##  [7]  Adjust (0), Type: 0x2, Length: 3, Value[0] = 1

  DER ASN.1 Decode (8 elements)
  [0] Type: 0x30, Length:  290, Value[0] =  30 0d 06 09 ...
  [1] Type: 0x30, Length:   13, Value[0] =  06 09 2a 86 ...
  [2] Type: 0x06, Length:    9, Value[0] =  2a 86 48 86 ...
  [3] Type: 0x05, Length:    0
  [4] Type: 0x03, Length:  271, Value[0] =  00 30 82 01 ...
  [5] Type: 0x30, Length:  266, Value[0] =  02 82 01 01 ...
  [6] Type: 0x02, Length:  256, Value[0] =  ca fe 82 0f ...
  [7] Type: 0x02, Length:    3, Value[0] =  01 00 01
##  DEBUG - Extracting RSA key
##  DEBUG (GetKeyFromDER) - end /key RSA Key Size: 256
## Result of GetKeyFromDER(Public): KeySizeBytes = 0x100
@@ -- Free 0x0139ac08
@@ -- Free 0x0137ca58
##  DEBUG (ReadKey) end 
 - Calcualting Public Keyhash
 -- an RSA key 256
##  DEBUG (GetIppHashAlgID)- Alg: 0xB
##  --SHA256 is 2
##  DEBUG (CreateHash) size=32
      0000: 68 83 7d d0 9e 6d b4 ba 7f 08 d3 85 a0 2f 5a 82 
      0010: 3d b8 ba 7c 03 e4 1a e5 b0 06 af cb c2 2a 16 cf 
 - Creating KM signature
##  DEBUG (KmGenMain) Addr KM: 113f6e8,  pSignature: 113f72c
 - RSA signature
##  DEBUG (ReadKey) - 0  keyprivkey.pem - looking for Public Key 
##  DEBUG (ReadInputFile) start - Filename: keyprivkey.pem
@@ malloc 0139ac08
##  DEBUG (ReadInputFile) End - Filename: keyprivkey.pem
##  Opened Key file
##    FileData = 2d 2d 2d 2d 
##  DEBUG (GetDerFromPEM) 
-- Found 0x0A at offset 32 (0x20)
-- pPemHeader: 0x139ac08
-- pPemHeader: -----BEGIN RSA PRIVATE KEY-----
##  processing header at 139ac08 
## Base64 data starting address: 139ac29 
## Found '-' at offset 2467 (0x9a3) - start of trailer 
## Base64 data length: 0x982  (0x982)
@@ malloc 013969b0
## Placing result in DER Buffer at address: 13969b0 (size=1825 0x721)

## DEBUG (Base64Decode) pB64String: 139ac29, B64Length: 0x982 pDecodeLocation: 13969b0
## PEM Type = 4
##  DEBUG (ReadKey) - Processing DER
##  Debug (GetKeyFromDER) - 0  PEM Type = 4
## DEBUG (ParseASN1) ASN.1 Size: 1768 Starting Index: 0
##  DEBUG Index: 0, Offset: 0x0 :: 30 82 6 e4
##  [0]  Adjust (2), Type: 0x30, Length: 1764, Value[0] = 2
##  Processing embedded Sequence
## DEBUG (ParseASN1) ASN.1 Size: 1764 Starting Index: 1
##  DEBUG Index: 1, Offset: 0x0 :: 2 1 0 2
##  [1]  Adjust (0), Type: 0x2, Length: 1, Value[0] = 0
##  DEBUG Index: 2, Offset: 0x3 :: 2 82 1 81
##  [2]  Adjust (2), Type: 0x2, Length: 385, Value[0] = 0
##  DEBUG Index: 3, Offset: 0x188 :: 2 3 1 0
##  [3]  Adjust (0), Type: 0x2, Length: 3, Value[0] = 1
##  DEBUG Index: 4, Offset: 0x18d :: 2 82 1 80
##  [4]  Adjust (2), Type: 0x2, Length: 384, Value[0] = 6e
##  DEBUG Index: 5, Offset: 0x311 :: 2 81 c1 0
##  [5]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 6, Offset: 0x3d5 :: 2 81 c1 0
##  [6]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 7, Offset: 0x499 :: 2 81 c0 5b
##  [7]  Adjust (1), Type: 0x2, Length: 192, Value[0] = 5b
##  DEBUG Index: 8, Offset: 0x55c :: 2 81 c1 0
##  [8]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 9, Offset: 0x620 :: 2 81 c1 0
##  [9]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0

  DER ASN.1 Decode (10 elements)
  [0] Type: 0x30, Length: 1764, Value[0] =  02 01 00 02 ...
  [1] Type: 0x02, Length:    1, Value[0] =  00
  [2] Type: 0x02, Length:  384, Value[0] =  d8 76 4a c6 ...
  [3] Type: 0x02, Length:    3, Value[0] =  01 00 01
  [4] Type: 0x02, Length:  384, Value[0] =  6e 3e 81 03 ...
  [5] Type: 0x02, Length:  192, Value[0] =  f2 fd 19 db ...
  [6] Type: 0x02, Length:  192, Value[0] =  e4 0d 90 39 ...
  [7] Type: 0x02, Length:  192, Value[0] =  5b 6f 3c 4c ...
  [8] Type: 0x02, Length:  192, Value[0] =  bb f1 19 9c ...
  [9] Type: 0x02, Length:  192, Value[0] =  e4 3e e6 0f ...
##  DEBUG - Extracting RSA key
##  DEBUG (GetKeyFromDER) - end /key RSA Key Size: 384
## Result of GetKeyFromDER(Public): KeySizeBytes = 0x180
@@ -- Free 0x0139ac08
@@ -- Free 0x013969b0
##  DEBUG (ReadKey) end 
##  DEBUG (CreateRsaSignatureIpp) - Start (PrivateKey: keyprivkey.pem
##  DEBUG (ReadKey) - 0  keyprivkey.pem - looking for Public Key 
##  DEBUG (ReadInputFile) start - Filename: keyprivkey.pem
@@ malloc 0139ac08
##  DEBUG (ReadInputFile) End - Filename: keyprivkey.pem
##  Opened Key file
##    FileData = 2d 2d 2d 2d 
##  DEBUG (GetDerFromPEM) 
-- Found 0x0A at offset 32 (0x20)
-- pPemHeader: 0x139ac08
-- pPemHeader: -----BEGIN RSA PRIVATE KEY-----
##  processing header at 139ac08 
## Base64 data starting address: 139ac29 
## Found '-' at offset 2467 (0x9a3) - start of trailer 
## Base64 data length: 0x982  (0x982)
@@ malloc 013969b0
## Placing result in DER Buffer at address: 13969b0 (size=1825 0x721)

## DEBUG (Base64Decode) pB64String: 139ac29, B64Length: 0x982 pDecodeLocation: 13969b0
## PEM Type = 4
##  DEBUG (ReadKey) - Processing DER
##  Debug (GetKeyFromDER) - 0  PEM Type = 4
## DEBUG (ParseASN1) ASN.1 Size: 1768 Starting Index: 0
##  DEBUG Index: 0, Offset: 0x0 :: 30 82 6 e4
##  [0]  Adjust (2), Type: 0x30, Length: 1764, Value[0] = 2
##  Processing embedded Sequence
## DEBUG (ParseASN1) ASN.1 Size: 1764 Starting Index: 1
##  DEBUG Index: 1, Offset: 0x0 :: 2 1 0 2
##  [1]  Adjust (0), Type: 0x2, Length: 1, Value[0] = 0
##  DEBUG Index: 2, Offset: 0x3 :: 2 82 1 81
##  [2]  Adjust (2), Type: 0x2, Length: 385, Value[0] = 0
##  DEBUG Index: 3, Offset: 0x188 :: 2 3 1 0
##  [3]  Adjust (0), Type: 0x2, Length: 3, Value[0] = 1
##  DEBUG Index: 4, Offset: 0x18d :: 2 82 1 80
##  [4]  Adjust (2), Type: 0x2, Length: 384, Value[0] = 6e
##  DEBUG Index: 5, Offset: 0x311 :: 2 81 c1 0
##  [5]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 6, Offset: 0x3d5 :: 2 81 c1 0
##  [6]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 7, Offset: 0x499 :: 2 81 c0 5b
##  [7]  Adjust (1), Type: 0x2, Length: 192, Value[0] = 5b
##  DEBUG Index: 8, Offset: 0x55c :: 2 81 c1 0
##  [8]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 9, Offset: 0x620 :: 2 81 c1 0
##  [9]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0

  DER ASN.1 Decode (10 elements)
  [0] Type: 0x30, Length: 1764, Value[0] =  02 01 00 02 ...
  [1] Type: 0x02, Length:    1, Value[0] =  00
  [2] Type: 0x02, Length:  384, Value[0] =  d8 76 4a c6 ...
  [3] Type: 0x02, Length:    3, Value[0] =  01 00 01
  [4] Type: 0x02, Length:  384, Value[0] =  6e 3e 81 03 ...
  [5] Type: 0x02, Length:  192, Value[0] =  f2 fd 19 db ...
  [6] Type: 0x02, Length:  192, Value[0] =  e4 0d 90 39 ...
  [7] Type: 0x02, Length:  192, Value[0] =  5b 6f 3c 4c ...
  [8] Type: 0x02, Length:  192, Value[0] =  bb f1 19 9c ...
  [9] Type: 0x02, Length:  192, Value[0] =  e4 3e e6 0f ...
##  DEBUG - Extracting RSA key
##  DEBUG (GetKeyFromDER) - end /key RSA Key Size: 384
## Result of GetKeyFromDER(Public): KeySizeBytes = 0x180
@@ -- Free 0x0139ac08
@@ -- Free 0x013969b0
##  DEBUG (ReadKey) end 
##  DEBUG (ReadKey) - 0  keyprivkey.pem - looking for Private Key 
##  DEBUG (ReadInputFile) start - Filename: keyprivkey.pem
@@ malloc 0139ac08
##  DEBUG (ReadInputFile) End - Filename: keyprivkey.pem
##  Opened Key file
##    FileData = 2d 2d 2d 2d 
##  DEBUG (GetDerFromPEM) 
-- Found 0x0A at offset 32 (0x20)
-- pPemHeader: 0x139ac08
-- pPemHeader: -----BEGIN RSA PRIVATE KEY-----
##  processing header at 139ac08 
## Base64 data starting address: 139ac29 
## Found '-' at offset 2467 (0x9a3) - start of trailer 
## Base64 data length: 0x982  (0x982)
@@ malloc 013969b0
## Placing result in DER Buffer at address: 13969b0 (size=1825 0x721)

## DEBUG (Base64Decode) pB64String: 139ac29, B64Length: 0x982 pDecodeLocation: 13969b0
## PEM Type = 4
##  DEBUG (ReadKey) - Processing DER
##  Debug (GetKeyFromDER) - 0  PEM Type = 4
## DEBUG (ParseASN1) ASN.1 Size: 1768 Starting Index: 0
##  DEBUG Index: 0, Offset: 0x0 :: 30 82 6 e4
##  [0]  Adjust (2), Type: 0x30, Length: 1764, Value[0] = 2
##  Processing embedded Sequence
## DEBUG (ParseASN1) ASN.1 Size: 1764 Starting Index: 1
##  DEBUG Index: 1, Offset: 0x0 :: 2 1 0 2
##  [1]  Adjust (0), Type: 0x2, Length: 1, Value[0] = 0
##  DEBUG Index: 2, Offset: 0x3 :: 2 82 1 81
##  [2]  Adjust (2), Type: 0x2, Length: 385, Value[0] = 0
##  DEBUG Index: 3, Offset: 0x188 :: 2 3 1 0
##  [3]  Adjust (0), Type: 0x2, Length: 3, Value[0] = 1
##  DEBUG Index: 4, Offset: 0x18d :: 2 82 1 80
##  [4]  Adjust (2), Type: 0x2, Length: 384, Value[0] = 6e
##  DEBUG Index: 5, Offset: 0x311 :: 2 81 c1 0
##  [5]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 6, Offset: 0x3d5 :: 2 81 c1 0
##  [6]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 7, Offset: 0x499 :: 2 81 c0 5b
##  [7]  Adjust (1), Type: 0x2, Length: 192, Value[0] = 5b
##  DEBUG Index: 8, Offset: 0x55c :: 2 81 c1 0
##  [8]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0
##  DEBUG Index: 9, Offset: 0x620 :: 2 81 c1 0
##  [9]  Adjust (1), Type: 0x2, Length: 193, Value[0] = 0

  DER ASN.1 Decode (10 elements)
  [0] Type: 0x30, Length: 1764, Value[0] =  02 01 00 02 ...
  [1] Type: 0x02, Length:    1, Value[0] =  00
  [2] Type: 0x02, Length:  384, Value[0] =  d8 76 4a c6 ...
  [3] Type: 0x02, Length:    3, Value[0] =  01 00 01
  [4] Type: 0x02, Length:  384, Value[0] =  6e 3e 81 03 ...
  [5] Type: 0x02, Length:  192, Value[0] =  f2 fd 19 db ...
  [6] Type: 0x02, Length:  192, Value[0] =  e4 0d 90 39 ...
  [7] Type: 0x02, Length:  192, Value[0] =  5b 6f 3c 4c ...
  [8] Type: 0x02, Length:  192, Value[0] =  bb f1 19 9c ...
  [9] Type: 0x02, Length:  192, Value[0] =  e4 3e e6 0f ...
##  DEBUG - Extracting RSA key
##  DEBUG (GetKeyFromDER) - end /key RSA Key Size: 384
## Result of GetKeyFromDER(Private): KeySizeBytes = 0x180
@@ -- Free 0x0139ac08
@@ -- Free 0x013969b0
##  DEBUG (ReadKey) end 
## Creating RSA PubKey Context - KeyLength: 3072 (exp: 0x00010001)
@@ malloc 0139ac08
##-- Setting Modulus and Exponent (0x00010001)
      0000: d9 03 fc 44 eb ad 15 79 
## Creating BigNumber Context - NumBytes: 384
@@ malloc 013969b0
##-- BigNumInit 
##-- Created BigNumber Context @0x 13969B0 size: 803
##-- pData: 113f494
## Creating BigNumber Context - NumBytes: 4
@@ malloc 01393978
##-- BigNumInit 
##-- Created BigNumber Context @0x 1393978 size: 43
##-- pData: 113f2e4
@@ -- Free 0x013969b0
@@ -- Free 0x01393978
##-- Created RSA PubKey Context @0x 139AC08 size: 2026
## Creating RSA PrivateKey Context - KeyLength: 3072
@@ malloc 013969b0
##-- Setting Modulus: 
      0000: d9 03 fc 44 eb ad 15 79 
##-- Setting PrivateExp: 
      0000: a1 35 36 62 5a 4f 27 7b 
## Creating BigNumber Context - NumBytes: 384
@@ malloc 01397340
##-- BigNumInit 
##-- Created BigNumber Context @0x 1397340 size: 803
##-- pData: 113f494
## Creating BigNumber Context - NumBytes: 384
@@ malloc 01397670
##-- BigNumInit 
##-- Created BigNumber Context @0x 1397670 size: 803
##-- pData: 113f314
@@ -- Free 0x01397340
@@ -- Free 0x01397670
##-- Created RSA PrivateKey Context @0x 13969B0 size: 2434
@@ malloc 0139bee0
Random Number (384 bits):  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
## Creating PRNG Context
@@ malloc 0137ca58
## -- Randomize the seed
## -- seed: 18084540
## Creating BigNumber Context - NumBytes: 4
@@ malloc 01393b38
##-- BigNumInit 
##-- Created BigNumber Context @0x 1393B38 size: 43
##-- pData: 113f2bc
@@ -- Free 0x01393b38
@@ -- Free 0x0137ca58
Random Number (384 bits): e0 6b aa de 12 7d 24 68 72 7c 4b 62 43 b9 8e da 64 b7 97 b4 79 3d 97 c3 a8 9b da 76 db 7a 83 ca 2a e2 67 70 ee b9 bc  1 29  f 7e  5  1 98 19 d5 
##  DEBUG (GetIppHashAlgID)- Alg: 0xC
##  -- SHA384 is 5
@@ -- Free 0x0139ac08
@@ -- Free 0x013969b0
@@ -- Free 0x0139bee0
      0000: 5f 5f 4b 45 59 4d 5f 5f 21 00 00 00 44 00 00 00 
      0010: 00 01 01 01 0c 00 01 00 01 00 00 00 00 00 00 00 
      0020: 0b 00 20 00 68 83 7d d0 9e 6d b4 ba 7f 08 d3 85 
      0030: a0 2f 5a 82 3d b8 ba 7c 03 e4 1a e5 b0 06 af cb 
      0040: c2 2a 16 cf 10 01 00 10 00 0c 01 00 01 00 d9 03 
      0050: fc 44 eb ad 15 79 bf b1 a5 45 22 f2 af a8 6e bd 
      0060: b8 62 f5 4b 59 fe 6b 97 a6 9a f0 74 59 89 e2 35 
      0070: 27 00 74 7e 8b 10 df ef 15 84 d0 d9 a7 77 e8 af 
      0080: f3 7e de 4a 2a 1a 18 5f 50 ed 01 b7 4d a4 bd b4 
      0090: 65 a5 74 88 10 8a 22 f6 b0 c6 e6 a1 ba 64 5e d8 
      00a0: 5e 8f fc 91 37 ef fa 88 66 56 40 1d 9a 60 43 91 
      00b0: dc 0b 6d 8b 01 28 4a 5b 4d b7 1f fc 0f 79 8e 92 
      00c0: b4 03 0b 02 b8 3b 16 ba d3 a7 f4 70 72 d8 4e e7 
      00d0: c0 0c 52 57 b1 05 74 c7 24 d2 6b c6 b7 5a ba 35 
      00e0: 6e 81 0f ca 0c 46 cf 8f bb f4 8d fc 5b 3d 85 59 
      00f0: b0 35 7b 30 c2 10 4e 93 3c 6e cc 66 cc 2d d1 4f 
      0100: 5a 5e ce 73 4c 25 78 f1 73 4c e2 25 33 25 18 9c 
      0110: 63 9b 21 58 1f c5 6f aa 40 36 58 25 78 a4 a8 6d 
      0120: c5 ca 5f 95 11 d2 03 6e 00 fa 74 61 9d 2b 18 16 
      0130: 41 0a 3c cf 84 e8 bb b4 a7 76 d3 d9 86 2b 42 68 
      0140: f3 1e 31 31 4e ad 28 ec f6 66 53 e5 26 9f c6 fa 
      0150: 39 6d 17 04 bf 5b d3 3e 55 24 0e b1 1f 20 90 60 
      0160: 8d 97 c5 b3 b7 ed dc 9f 46 9f 2f 62 5d 10 e9 80 
      0170: e8 4c cc 0d 64 e0 1a b2 11 d6 03 44 24 aa 41 14 
      0180: 07 28 0d e3 a8 a6 e7 27 17 23 65 88 46 ed 9b de 
      0190: 9a c2 37 e2 2f 4f 14 3d 32 2a e2 0e 2c 41 36 7a 
      01a0: d6 9b f1 a4 ea 8d 26 a9 a8 85 f0 56 69 00 65 88 
      01b0: 47 ca 3b 7d 17 f6 7b e9 cb 5a 49 39 8b 41 f0 f6 
      01c0: 5b d0 11 30 c8 92 c8 98 51 af c6 4a 76 d8 16 00 
      01d0: 10 00 0c 0c 00 9b 0f f7 a6 41 b7 f7 44 c4 df 7b 
      01e0: 25 b1 58 37 5f 66 74 00 fa 64 dd 73 ba cb 45 72 
      01f0: 7a c1 f0 d1 90 d8 fe 80 b3 93 67 d3 ed 5e 87 7b 
      0200: f9 a5 99 15 af 1b 59 f8 4d e5 74 ab 03 c2 cc b7 
      0210: f0 89 da 27 f7 9d 5d fe 55 3c df 24 6d 2d 5d 72 
      0220: a7 ea d0 d8 e4 38 02 6a 45 d5 23 1c 98 56 82 61 
      0230: de a1 d0 97 b6 c6 03 74 71 48 d2 4b f7 cb 17 c2 
      0240: 62 12 1d 9a be f7 79 d4 55 69 1a b0 2d 05 cc 2d 
      0250: c6 d0 d0 99 bd 9c 89 9e 2c a4 64 ab 6f 4d 7f 69 
      0260: 7b 00 dc 84 63 2c d8 4f 24 91 f9 e3 97 b0 c5 17 
      0270: 58 f3 1c 7f ed 27 6e b9 ff af 6d 32 58 8c 69 7e 
      0280: 03 36 81 52 d4 2f d5 cf 69 84 11 8f bb 9f 30 08 
      0290: 4d b3 06 a6 fb 11 3c 8c ed dc ad 69 ff 53 16 ff 
      02a0: ac 66 75 71 5e cc e5 ca 48 04 e0 41 d5 70 b7 0c 
      02b0: 1e cf 1a f7 85 2b 5f d2 c0 9c c2 05 66 59 a9 e5 
      02c0: 6d 77 b7 85 79 bf 3b 11 cc 71 2b 63 d9 34 12 63 
      02d0: f4 45 1b 0f 56 51 1d ff f9 ef 0c e6 be 54 02 30 
      02e0: 60 53 65 54 ff fe 18 62 b7 0b 41 9f 5f 79 14 c7 
      02f0: 04 d0 dd 59 2d ce c4 bb 37 50 79 c3 aa 99 66 95 
      0300: ec cc f6 87 42 48 41 6a 5f aa 48 b1 ac 8f ac e6 
      0310: 53 07 76 ae fd fc eb 5e 5a 13 a2 e2 e8 aa 1d b8 
      0320: d4 be e7 04 f0 bd fc ca e8 18 b0 17 1c 05 ef b5 
      0330: ce 99 19 03 5d 3c c8 70 2f bb e3 b7 4b 4a a0 65 
      0340: b4 cc db 15 44 06 5b 47 a5 dc d0 92 6f 69 cf cc 
      0350: 3f df a1 f7 8b 

################
# Key Manifest #
################
  StructureID:    __KEYM__
  StructVersion:  0x21
  Reserved:       0x00 00 00
  KeySigOffset:   0x0044
  Reserved:       0x00 00 00
  KeyManifestVer: 0x01
  KMSVN:          0x01
  KeyManifestID:  0x01
  KmPubKey Alg:   0x000c - 0x0C:SHA384
  Number of Manifest Key Digests:   1 
  KeyHashes:
  [1] Usage:      0x0000000000000001 For: Boot Policy Manifest, 
      HashAlg:    0x000b - 0x0B:SHA256
      Size:       0x0020
      HashBuffer: 68837dd09e6db4ba7f08d385a02f5a823db8ba7c03e41ae5b006afcbc22a16cf
  Signature Structure:
    Version:      0x10
    KeyAlg:       0x0001 0x01:RSA
    RsaPublicKeyStructure:
       Version:   0x10
       KeySize:   0x0c00
      Exponent:   0x00010001
       Modulus:
      0000: d9 03 fc 44 eb ad 15 79 bf b1 a5 45 22 f2 af a8 
      0010: 6e bd b8 62 f5 4b 59 fe 6b 97 a6 9a f0 74 59 89 
      0020: e2 35 27 00 74 7e 8b 10 df ef 15 84 d0 d9 a7 77 
      0030: e8 af f3 7e de 4a 2a 1a 18 5f 50 ed 01 b7 4d a4 
      0040: bd b4 65 a5 74 88 10 8a 22 f6 b0 c6 e6 a1 ba 64 
      0050: 5e d8 5e 8f fc 91 37 ef fa 88 66 56 40 1d 9a 60 
      0060: 43 91 dc 0b 6d 8b 01 28 4a 5b 4d b7 1f fc 0f 79 
      0070: 8e 92 b4 03 0b 02 b8 3b 16 ba d3 a7 f4 70 72 d8 
      0080: 4e e7 c0 0c 52 57 b1 05 74 c7 24 d2 6b c6 b7 5a 
      0090: ba 35 6e 81 0f ca 0c 46 cf 8f bb f4 8d fc 5b 3d 
      00a0: 85 59 b0 35 7b 30 c2 10 4e 93 3c 6e cc 66 cc 2d 
      00b0: d1 4f 5a 5e ce 73 4c 25 78 f1 73 4c e2 25 33 25 
      00c0: 18 9c 63 9b 21 58 1f c5 6f aa 40 36 58 25 78 a4 
      00d0: a8 6d c5 ca 5f 95 11 d2 03 6e 00 fa 74 61 9d 2b 
      00e0: 18 16 41 0a 3c cf 84 e8 bb b4 a7 76 d3 d9 86 2b 
      00f0: 42 68 f3 1e 31 31 4e ad 28 ec f6 66 53 e5 26 9f 
      0100: c6 fa 39 6d 17 04 bf 5b d3 3e 55 24 0e b1 1f 20 
      0110: 90 60 8d 97 c5 b3 b7 ed dc 9f 46 9f 2f 62 5d 10 
      0120: e9 80 e8 4c cc 0d 64 e0 1a b2 11 d6 03 44 24 aa 
      0130: 41 14 07 28 0d e3 a8 a6 e7 27 17 23 65 88 46 ed 
      0140: 9b de 9a c2 37 e2 2f 4f 14 3d 32 2a e2 0e 2c 41 
      0150: 36 7a d6 9b f1 a4 ea 8d 26 a9 a8 85 f0 56 69 00 
      0160: 65 88 47 ca 3b 7d 17 f6 7b e9 cb 5a 49 39 8b 41 
      0170: f0 f6 5b d0 11 30 c8 92 c8 98 51 af c6 4a 76 d8 

    SigScheme:    0x0016 0x16:RSAPSS
    RsaSsaSigStructure:
      Version:    0x10
      KeySize:    0x0c00
      HashAlg:    0x000c 0x0C:SHA384
      Signature:
      0000: 9b 0f f7 a6 41 b7 f7 44 c4 df 7b 25 b1 58 37 5f 
      0010: 66 74 00 fa 64 dd 73 ba cb 45 72 7a c1 f0 d1 90 
      0020: d8 fe 80 b3 93 67 d3 ed 5e 87 7b f9 a5 99 15 af 
      0030: 1b 59 f8 4d e5 74 ab 03 c2 cc b7 f0 89 da 27 f7 
      0040: 9d 5d fe 55 3c df 24 6d 2d 5d 72 a7 ea d0 d8 e4 
      0050: 38 02 6a 45 d5 23 1c 98 56 82 61 de a1 d0 97 b6 
      0060: c6 03 74 71 48 d2 4b f7 cb 17 c2 62 12 1d 9a be 
      0070: f7 79 d4 55 69 1a b0 2d 05 cc 2d c6 d0 d0 99 bd 
      0080: 9c 89 9e 2c a4 64 ab 6f 4d 7f 69 7b 00 dc 84 63 
      0090: 2c d8 4f 24 91 f9 e3 97 b0 c5 17 58 f3 1c 7f ed 
      00a0: 27 6e b9 ff af 6d 32 58 8c 69 7e 03 36 81 52 d4 
      00b0: 2f d5 cf 69 84 11 8f bb 9f 30 08 4d b3 06 a6 fb 
      00c0: 11 3c 8c ed dc ad 69 ff 53 16 ff ac 66 75 71 5e 
      00d0: cc e5 ca 48 04 e0 41 d5 70 b7 0c 1e cf 1a f7 85 
      00e0: 2b 5f d2 c0 9c c2 05 66 59 a9 e5 6d 77 b7 85 79 
      00f0: bf 3b 11 cc 71 2b 63 d9 34 12 63 f4 45 1b 0f 56 
      0100: 51 1d ff f9 ef 0c e6 be 54 02 30 60 53 65 54 ff 
      0110: fe 18 62 b7 0b 41 9f 5f 79 14 c7 04 d0 dd 59 2d 
      0120: ce c4 bb 37 50 79 c3 aa 99 66 95 ec cc f6 87 42 
      0130: 48 41 6a 5f aa 48 b1 ac 8f ac e6 53 07 76 ae fd 
      0140: fc eb 5e 5a 13 a2 e2 e8 aa 1d b8 d4 be e7 04 f0 
      0150: bd fc ca e8 18 b0 17 1c 05 ef b5 ce 99 19 03 5d 
      0160: 3c c8 70 2f bb e3 b7 4b 4a a0 65 b4 cc db 15 44 
      0170: 06 5b 47 a5 dc d0 92 6f 69 cf cc 3f df a1 f7 8b 

Key Manifest Size: 0x355 

##  DEBUG (PrintKmKeyHash) 
## Hash Ctx Size: 232
##  DEBUG (GetIppHashAlgID)- Alg: 0xC
##  -- SHA384 is 5
# FYI: KM Public Key Hash Digest (Modulus+Exponent)#
  78 9a ca fe 0b b6 aa 6e b4 ef 7d 90 15 f6 19 16 
  8e a4 24 fc c1 3f ea ae 2d 57 7f 2c 1c b0 d3 a1 
  6f cf 12 68 31 db 67 99 5b 7c 36 45 f2 a7 4b a1 
  
# FYI: KM Public Key Hash Digest (Modulus Only)#
  ##  DEBUG (GetIppHashAlgID)- Alg: 0xC
##  -- SHA384 is 5
##  DEBUG (CreateHash) size=48
      0000: 15 44 bc c0 d3 aa 55 a5 66 c6 13 b5 6f a0 a8 dc 
      0010: 54 72 4d 24 76 7d 35 2b 49 68 a0 12 07 7f d8 a8 
      0020: e5 96 32 cc 4f b5 c5 55 e4 1d 4f b8 3b 88 ad dc 
15 44 bc c0 d3 aa 55 a5 66 c6 13 b5 6f a0 a8 dc 
  54 72 4d 24 76 7d 35 2b 49 68 a0 12 07 7f d8 a8 
  e5 96 32 cc 4f b5 c5 55 e4 1d 4f b8 3b 88 ad dc 
  

Writing KM  to file (C:\ADL_branch35\Build\AlderLakeSMultiBoardPkg\RELEASE_DEVTLSxVC16\FV\KeyManifest.bin)
##  DEBUG (WriteOutputFile) start Filename: C:\ADL_branch35\Build\AlderLakeSMultiBoardPkg\RELEASE_DEVTLSxVC16\FV\KeyManifest.bin, BufAddress: 113f6e8, Size 0x355
##  DEBUG (WriteOutputFile) opened
##  DEBUG (WriteOutputFile) written
##  DEBUG (WriteOutputFile) end
