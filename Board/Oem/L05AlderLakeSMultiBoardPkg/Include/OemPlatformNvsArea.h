/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_PLATFORM_NVS_AREA_H_
#define _OEM_PLATFORM_NVS_AREA_H_

//
// OEM Platform NVS Area definition
//
#pragma pack (1)
typedef struct {

  //
  // The definitions below need to be matched OGNS definitions in OemPlatformNvs.ASL
  // and can be modified by OEM
  //
  UINT8       OemPlatformNvsData00;    // 00
  UINT8       OemPlatformNvsData01;    // 01
  UINT8       OemPlatformNvsData02;    // 02
  UINT8       OemPlatformNvsData03;    // 03
  UINT8       OemPlatformNvsData04;    // 04
  UINT8       OemPlatformNvsData05;    // 05
  UINT8       OemPlatformNvsData06;    // 06
  UINT8       OemPlatformNvsData07;    // 07
  UINT8       OemPlatformNvsData08;    // 08
  UINT8       OemPlatformNvsData09;    // 09
  UINT8       OemPlatformNvsData10;    // 10
  UINT8       OemEmbeddedControllerFlag;
} OEM_PLATFORM_NVS_AREA;
#pragma pack ()

#endif
