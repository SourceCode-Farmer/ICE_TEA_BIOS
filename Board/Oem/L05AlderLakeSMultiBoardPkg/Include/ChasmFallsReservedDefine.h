/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHASM_FALLS_RESERVED_DEFINITION_
#define _CHASM_FALLS_RESERVED_DEFINITION_

UINT32 mSkipListCount;

typedef struct {
  UINT32    SkipAddress;
  UINT32    SkipSize;
} FAULTTOLERANCE_SKIP_TABLE;

//
// Skip table
// !!NOTICE!! When BIOS Guard Enabled, only skipped FIRST area which described in mSkipHashInSbbTable
// Detail usage, please reference OEM Customization Guide
//
FAULTTOLERANCE_SKIP_TABLE mSkipHashInSbbTable[] = {
//
//Example : 
//_Start_L05_FEATURE_
  {FixedPcdGet32 (PcdFlashNvStorageMsdmDataBase), FixedPcdGet32 (PcdFlashNvStorageMsdmDataSize)},
  {FixedPcdGet32 (PcdFlashFvL05FeatureUsedBase), FixedPcdGet32 (PcdFlashFvL05FeatureUsedSize)},
//_End_L05_FEATURE_
//
  {0x0,0x0}
};

#endif
