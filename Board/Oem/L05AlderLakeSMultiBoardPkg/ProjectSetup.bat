@REM
@REM Project setup batch file
@REM
@REM ******************************************************************************
@REM * Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************

@echo off

REM ---------------------------------------------------------------------------------------------
REM Auto setting of WORKSPACE environment variable
REM ---------------------------------------------------------------------------------------------
pushd \ && set ROOT_DIR=%cd% && popd && pushd .
:SetWorkSpace
if "%cd%" == "%ROOT_DIR%" goto Next
cd ..
if not exist %cd%\BaseTools goto SetWorkSpace
set WORKSPACE=%cd%
:Next
set ROOT_DIR= && popd
%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv WORKSPACE > NUL && for /f %%i in ('%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv WORKSPACE') do set %%i
REM ---------------------------------------------------------------------------------------------
REM Set PACKAGES_PATH here to specify multiple workspaces.
REM ---------------------------------------------------------------------------------------------
REM _Start_L05_FEATURE_
REM set PACKAGES_PATH=^
REM %WORKSPACE%\Board\Intel;^
REM %WORKSPACE%\Intel\AlderLake;^
REM %WORKSPACE%\Intel\EDK2Platforms;^
REM %WORKSPACE%\Intel\EDK2Platforms\Platform\Intel;^
REM %WORKSPACE%\Intel\EDK2Platforms\Silicon\Intel;^
REM %WORKSPACE%\Insyde;^
REM %WORKSPACE%\EDK2
set PACKAGES_PATH=^
%WORKSPACE%\Board\Oem;^
%WORKSPACE%\Oem\L05\AlderLake;^
%WORKSPACE%\Oem\L05\FeatureCommon;^
%WORKSPACE%\Board\Intel;^
%WORKSPACE%\Intel\AlderLake;^
%WORKSPACE%\Intel\AlderLake\Features;^
%WORKSPACE%\Intel\AlderLake\Features\Audio;^
%WORKSPACE%\Intel\AlderLake\Features\Storage;^
%WORKSPACE%\Intel\AlderLake\Features\Manageability;^
%WORKSPACE%\Intel\Edk2Platforms;^
%WORKSPACE%\Intel\Edk2Platforms\Platform\Intel;^
%WORKSPACE%\Intel\Edk2Platforms\Silicon\Intel;^
%WORKSPACE%\Intel\Edk2Platforms\Features\Intel\Debugging;^
%WORKSPACE%\Intel\Edk2Platforms\Features\Intel\UserInterface;^
%WORKSPACE%\Intel\Edk2Platforms\Features\Intel;^
%WORKSPACE%\Insyde;^
%WORKSPACE%\EDK2
REM _End_L05_FEATURE_


@REM ---------------------------------------------------------------------------------------------
@REM Support Module build
@REM Build command sample:
@REM                      build -q -p ..\Build\AlderLakeSMultiBoardPkg\Project.dsc -m InsydeModulePkg/Universal/MemoryTest/GenericMemoryTestDxe/GenericMemoryTestDxe.inf
@REM ---------------------------------------------------------------------------------------------
%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv PROJECT_PKG > NUL && for /f %%i in ('%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv PROJECT_PKG') do set %%i

REM ---------------------------------------------------------------------------------------------
REM Project dependent parameters
REM ---------------------------------------------------------------------------------------------
%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv CHIPSET_PKG > NUL && for /f %%a in ('%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv CHIPSET_PKG') do set %%a
%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv PROJECT_REL_PATH > NUL && for /f %%a in ('%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv PROJECT_REL_PATH') do set %%a
%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv CHIPSET_REL_PATH > NUL && for /f %%a in ('%WORKSPACE%\BaseTools\Bin\Win32\GetProjectEnv CHIPSET_REL_PATH') do set %%a
REM set TOOL_CHAIN=CLANGPDB
set ASL_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32
set ARCH=IA32 X64
set EFI_SOURCE_DIR=InsydeModulePkg
set PYTHON_DIR=%LOCALAPPDATA%\Programs\Python\Python38-32
set PATH=%PYTHON_DIR%;%PATH%

set PLATFORM_TYPE=AlderLakeS
set EXT_BUILD_FLAGS=-D PROJECT_PKG_ROOT=%PROJECT_PKG%
set CrbBuild=YES
@REM[-start-200917-IB06462159-add]REM
set PLATFORM_FSP_BIN_PACKAGE=AlderLakeFspBinPkg
@REM[-end-200917-IB06462159-add]REM


@REM !!! Don't set the NativeFsp and NativeFspVersion flag directly, 
@REM !!! You should control the PCD of PcdNativeFspBuild instead of NativeFsp flag and the PCD of PcdNativeFspVersion is the same.
set NativeFsp=NO
set NativeFspVersion=

@rem for /f "tokens=2 delims=|" %%a in ('find "gChipsetPkgTokenSpaceGuid.PcdNativeFspVersion" Project.dsc') do set NativeFspVersion=%%a
@rem echo test2
@rem echo %NativeFspVersion%
for /f tokens^=2^ delims^=^" %%a in ('find "gChipsetPkgTokenSpaceGuid.PcdNativeFspVersion" Project.dsc') do set NativeFspVersion=%%a

findstr /C:"gChipsetPkgTokenSpaceGuid.PcdNativeFspBuild|TRUE" %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\Project.dsc >nul
if not errorlevel 1 set NativeFsp=YES
if %NativeFsp% equ NO goto NoCheckNativeFspBin

if not exist "%WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORM_FSP_BIN_PACKAGE%\NativeFspRelease\%NativeFspVersion%\Fsp.fd" (
  echo !!! ERROR !!! NativeFsp = YES !!!
  echo !!! ERROR !!! NativeFsp version %NativeFspVersion% Binary not exist!!!
  pause
) 
:NoCheckNativeFspBin

REM ---------------------------------------------------------------------------------------------
REM Check tool chain
REM ---------------------------------------------------------------------------------------------
:Check_signtool
set BIOS_GUARD=NO
set CHASMFALLGEN2=NO
for /f "tokens=4" %%a in ('find "BIOS_GUARD_SUPPORT" Project.env') do if %%a==YES set BIOS_GUARD=YES

findstr /C:"gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport|2" %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\Project.dsc >nul
if not errorlevel 1 set CHASMFALLGEN2=YES

if %BIOS_GUARD% equ NO goto End_SignTool_Check
if %CHASMFALLGEN2% equ NO goto End_SignTool_Check
set SIGNTOOL_FOUND=NO
set INSYDE_SIGNER_FOUND=NO
set SIGNER_CHIPSET_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32\Signer
set SIGNER_PATH=DEVTLS\SignTool
set SIGNER_PRESET_PATH=C:\%SIGNER_PATH%;D:\%SIGNER_PATH%;E:\%SIGNER_PATH%;F:\%SIGNER_PATH%;G:\%SIGNER_PATH%;H:\%SIGNER_PATH%;%SIGNER_CHIPSET_PATH%
set PATH=%SIGNER_PRESET_PATH%;%PATH%
signtool /? 2>nul
if not errorlevel 1 goto SignTool_Found

:SignTool_NotFound
start /WAIT CautionSiger.bat
echo.
call :ColorText 0c "Build code process has been terminated due to SignTool is missing!"
echo.
echo.
exit /b

:SignTool_Found
echo.
call :ColorText 0b "SignTool Found!"
echo.
echo.
:End_SignTool_Check

:Check_OpenSSL_tool
set CHASMFALLGEN0=NO
findstr /C:"gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport|0" %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\Project.dsc >nul
if not errorlevel 1 set CHASMFALLGEN0=YES
for /f "tokens=4" %%a in ('find "BOOT_GUARD_SUPPORT" Project.env') do if %%a==NO (if %CHASMFALLGEN0% equ YES goto Check_OpenSSL_End)
@REM Check 1. Does OpenSSL already exist and tool PATH is ready
echo Check OpenSSL:
@openssl version  2>nul
@if not errorlevel 1 goto OpenSSL_Found

@REM Check 2. Set the preset PATH to environment and try
set OPENSSL_PRESET_PATH=C:\OpenSSL-Win32\bin;C:\Program Files (x86)\OpenSSL-Win32\bin;
set OPENSSL_PRESET_PATH=%OPENSSL_PRESET_PATH%;C:\OpenSSL-Win64\bin;C:\Program Files\OpenSSL-Win64\bin;
set OPENSSL_PRESET_PATH=%OPENSSL_PRESET_PATH%;C:\OpenSSL\bin;%WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\OpenSSl;
set SSL_DIR=DEVTLS\OpenSSL
set OPENSSL_PRESET_PATH=%OPENSSL_PRESET_PATH%;C:\%SSL_DIR%;D:\%SSL_DIR%;E:\%SSL_DIR%;F:\%SSL_DIR%;G:\%SSL_DIR%;H:\%SSL_DIR%;
set PATH=%OPENSSL_PRESET_PATH%;%PATH%
@openssl version  2>nul
@if not errorlevel 1 goto OpenSSL_Found

:OpenSSL_Not_Found
start /WAIT Caution.bat
echo.
call :ColorText 0c "Build code process has been terminated due to OpenSSL is missing!"
echo.
echo.
exit /b

:ColorText
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & for %%b in (1) do rem"') do (
  set "TempToDel=%%a"
)
<nul set /p ".=%TempToDel%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1
goto :eof

:OpenSSL_Found
echo.
call :ColorText 0b "OpenSSL Found!"
echo.
echo.
:Check_OpenSSL_End

:CBnT_Check
@REM Check Intel Converged Boot Guard and Trusted Execution Technologies
set TXT_SUPPORT=NO
set BOOT_GUARD_SUPPORT=NO

for /f "tokens=4" %%a in ('find "TXT_SUPPORT" Project.env') do if %%a==YES set TXT_SUPPORT=YES
for /f "tokens=4" %%a in ('find "BOOT_GUARD_SUPPORT" Project.env') do if %%a==YES set BOOT_GUARD_SUPPORT=YES

@REM For CBnT, the Boot Policy Manifest is updated to add a new TXT Element (_TXTE_), which is used to provide additional information needed for TXT functionality
@REM The TXT Element is optional, and if not defined, all of its fields are assumed to have default values.
@if /I %BOOT_GUARD_SUPPORT%==NO (
  @if /I %TXT_SUPPORT%==YES (
     echo.
     echo.
     call :ColorText 0c "  Intel Converged Boot Guard and Trusted Execution Technologie"
     echo.
     call :ColorText 0c "  TXT need fill in _TXTE_ information in BIOS"
     echo.
     call :ColorText 0c "  Please Enable Boot Guard in Project.env"
     echo.
     echo.
     echo.
     exit /b
  )
)

@if not defined TOOL_CHAIN (
  REM ---------------------------------------------------------------------------------------------
  REM @echo Check the DEVTLS environment.
  REM ---------------------------------------------------------------------------------------------
  set TOOL_DIR=DEVTLS\MSVC16
  call :CheckDevtlsPath C:
  call :CheckDevtlsPath D:
  call :CheckDevtlsPath E:
  call :CheckDevtlsPath F:
  call :CheckDevtlsPath G:
  call :CheckDevtlsPath H:
)

if defined TOOL_CHAIN_DRV (
  set TOOL_CHAIN=DEVTLS_VC16
  goto KernelEnvSetting
)

@if defined TOOL_CHAIN goto KernelEnvSetting

@if not defined TOOL_CHAIN (
  REM ---------------------------------------------------------------------------------------------
  REM @echo check VS2017~ environment from vswhere
  REM ---------------------------------------------------------------------------------------------  
  IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe" (
    call "%ProgramFiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe" > Output
    FOR /f "usebackq tokens=1* delims=: " %%i IN (Output) do (
      IF /i "%%i"=="installationVersion" (
        IF %%j GTR 16 (
          REM ---------------------------------------------------------------------------------------------
          REM @echo Select the VS2019 environment.
          REM ---------------------------------------------------------------------------------------------
          set TOOL_CHAIN=VS2019
          goto KernelEnvSetting
          ) ELSE IF %%j GTR 15 (
          REM ---------------------------------------------------------------------------------------------
          REM @echo Select the VS2017 environment.
          REM ---------------------------------------------------------------------------------------------
          set TOOL_CHAIN=VS2017x86
          goto KernelEnvSetting
          )
      )
    )
  ) ELSE IF exist "%ProgramFiles%\Microsoft Visual Studio\Installer\vswhere.exe" (
    call "%ProgramFiles%\Microsoft Visual Studio\Installer\vswhere.exe" > Output
    FOR /f "usebackq tokens=1* delims=: " %%i IN (Output) do (
      IF /i "%%i"=="installationVersion" (
        IF %%j GTR 16 (
          REM ---------------------------------------------------------------------------------------------
          REM @echo Select the VS2019 environment.
          REM ---------------------------------------------------------------------------------------------
          set TOOL_CHAIN=VS2019
          goto KernelEnvSetting
          ) ELSE IF %%j GTR 15 (
          REM ---------------------------------------------------------------------------------------------
          REM @echo Select the VS2017 environment.
          REM ---------------------------------------------------------------------------------------------
          set TOOL_CHAIN=VS2017x86
          goto KernelEnvSetting
          )
      )
    )
  ) 
  if defined VS140COMNTOOLS (
    REM ---------------------------------------------------------------------------------------------
    REM @echo Select the VS2015 environment.
    REM ---------------------------------------------------------------------------------------------
    set TOOL_CHAIN=VS2015x86
    goto KernelEnvSetting
  ) 
)

:ErrorHandle
REM If Visual Studio 2019 was not detected, return an error.
@if not defined TOOL_CHAIN (
  echo.
  echo !!! ERROR !!! Visual Studio 2019 not installed correctly!!!
  echo.
  pause
  goto :EOF
)

:CheckDevtlsPath
if not DEFINED TOOL_CHAIN_DRV (
  if exist "%1\%TOOL_DIR%" set TOOL_CHAIN_DRV=%1
)
goto :EOF

:KernelEnvSetting
call %WORKSPACE%\BaseTools\H2ORev50.bat

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
)
%PYTHON_COMMAND% --version
if not %ERRORLEVEL% == 0  (
  @echo ERROR - Python is not found at %PYTHON_COMMAND% and py -3 is invalid
  @echo.
  @echo !! The EDKII BIOS build has failed in ProjectSetup!
  @echo.
  @exit /b 1
)