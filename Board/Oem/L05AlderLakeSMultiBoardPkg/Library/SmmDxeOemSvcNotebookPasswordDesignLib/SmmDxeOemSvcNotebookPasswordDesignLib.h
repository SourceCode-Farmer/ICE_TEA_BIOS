/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _SMM_DXE_OEM_SVC_NOTEBOOK_PASSWORD_DESIGN_LIB_H_
#define _SMM_DXE_OEM_SVC_NOTEBOOK_PASSWORD_DESIGN_LIB_H_

#ifdef L05_NATURAL_FILE_GUARD_ENABLE
#include <NaturalFileGuardCore.h>
#endif

#include <FlashRegionLayout.h>
#include <SetupConfig.h>
#include <Library/FeatureLib/OemSvcSecurityPassword.h>
#include <Library/UefiLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/RngLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseCryptLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/VariableLib.h>
#include <Protocol/L05Variable.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/SmmBase2.h>
#include <Protocol/Hash.h>
#include <Protocol/CryptoServices.h>
#include <Guid/L05VariableTool.h>

//
// [Lenovo Notebook Password Design Spec V1.0]
//   1 BIOS Password
//
#define L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE  32

BOOLEAN
L05IsInSmm (
  VOID
  );

EFI_STATUS
FlashBiosRegion (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb,
  UINTN                                 Offset,
  UINTN                                 NumBytes,
  UINT8                                 *Buffer
  );

EFI_STATUS
L05Sha256Hash (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  OUT UINT8                             *HashData
  );

EFI_STATUS
GetSystemPasswordRegion (
  UINTN                                 *L05SystemSupervisorPasswordRegionBase,
  UINTN                                 *L05SystemSupervisorPasswordRegionSize,
  UINTN                                 *L05SystemUserPasswordRegionBase,
  UINTN                                 *L05SystemUserPasswordRegionSize
  );

EFI_STATUS
GetSystemPasswordArea (
  UINT8                                 **L05SystemPasswordArea,
  UINTN                                 *L05SystemPasswordSize
  );

EFI_STATUS
GetBiosPasswordRandomNumber (
  UINT8                                 **RandomNumber
  );

BOOLEAN
IsBiosPasswordRandomNumberExist (
  VOID
  );

EFI_STATUS
SecurityPasswordEncodePhase1 (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  );

EFI_STATUS
SecurityPasswordEncodePhase2 (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  IN  UINT8                             *ConcatenateData,
  IN  UINTN                             ConcatenateDataSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  );

EFI_STATUS
BiosPasswordInit (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb
  );

extern EFI_SMM_SYSTEM_TABLE2            *mSmst;

#endif