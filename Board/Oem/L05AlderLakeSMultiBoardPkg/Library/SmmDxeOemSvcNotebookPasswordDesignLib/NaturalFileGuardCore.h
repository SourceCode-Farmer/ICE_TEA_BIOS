/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _NATURAL_FILE_GUARD_CORE_H_
#define _NATURAL_FILE_GUARD_CORE_H_

#include <Guid/L05NaturalFileGuardVariable.h>

//
// [Natural File Guard Design Guide V1.01]
//   2.2 User Hard Disk Password Safe Store
//     2.2.1 Secure Key Generation
//     2.2.2 UHDP Encryption
//
#define L05_HDD_PASSWORD_SECURITY_KEY_SIZE           32
#define BITS_PER_BYTE                                8
#define PKCS5_PADDING_BLOCK_SIZE                     8

EFI_STATUS
OemSvcGenerateSecureKey (
  UINT8                                 **SecureKey
  );

EFI_STATUS
OemSvcGetSecureKey (
  UINT8                                 **SecureKey
  );

EFI_STATUS
OemSvcSetSecureKey (
  UINT8                                 *SecureKey
  );

EFI_STATUS
SetSecureKey (
  UINT8                                 *SecureKey
  );

EFI_STATUS
ClearSecureKey (
  VOID
  );

#endif
