/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

#ifdef L05_NATURAL_FILE_GUARD_ENABLE
/**
  Check natural file guard is enable.

  @retval TRUE                          Natural file guard is enable.
  @retval FALSE                         Natural file guard is disable.
**/
BOOLEAN
IsNaturalFileGuardEnable (
  VOID
  )
{
  EFI_STATUS                            Status;
  BOOLEAN                               NaturalFileGuardEnable;
  UINTN                                 BufferSize;
  SYSTEM_CONFIGURATION                  *SetupNvData;

  NaturalFileGuardEnable = FALSE;
  SetupNvData            = NULL;

  BufferSize  = sizeof (SYSTEM_CONFIGURATION);
  SetupNvData = AllocateZeroPool (BufferSize);

  if (SetupNvData == NULL) {
    return FALSE;
  }

  Status = CommonGetVariable (
             L"Setup",
             &gSystemConfigurationGuid,
             &BufferSize,
             SetupNvData
             );

  if (!EFI_ERROR (Status) && (SetupNvData->L05NaturalFileGuard == 1)) {
    NaturalFileGuardEnable = TRUE;
  }

  FreePool (SetupNvData);

  return NaturalFileGuardEnable;
}

/**
  Check stored UHDP is found.

  @retval TRUE                          Stored UHDP is found.
  @retval FALSE                         Stored UHDP is not found.
**/
BOOLEAN
IsFoundStoredUhdp (
  VOID
  )
{
  EFI_STATUS                            Status;
  LENOVO_UHDP_STORE                     *L05UhdpStore;
  UINTN                                 BufferSize;

  L05UhdpStore = NULL;
  BufferSize   = 0;

  Status = CommonGetVariable (
             L05_NATURAL_FILE_GUARD_VARIABLE_NAME,
             &gEfiL05NaturalFileGuardVariableGuid,
             &BufferSize,
             L05UhdpStore
             );

  if (Status == EFI_BUFFER_TOO_SMALL) {
    return TRUE;
  }

  return FALSE;
}

/**
  Clear stored UHDP.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ClearStoredUhdp (
  VOID
  )
{
  EFI_STATUS                            Status;

  Status = CommonSetVariable (
             L05_NATURAL_FILE_GUARD_VARIABLE_NAME,
             &gEfiL05NaturalFileGuardVariableGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             0,
             NULL
             );

  ClearSecureKey ();

  return Status;
}

/**
  Disable natural file guard.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
DisableNaturalFileGuard (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINTN                                 BufferSize;
  SYSTEM_CONFIGURATION                  *SetupNvData;

  SetupNvData = NULL;
  BufferSize  = sizeof (SYSTEM_CONFIGURATION);
  SetupNvData = AllocateZeroPool (BufferSize);

  if (SetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = CommonGetVariable (
             L"Setup",
             &gSystemConfigurationGuid,
             &BufferSize,
             SetupNvData
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  SetupNvData->L05NaturalFileGuard = 0;  // 0:Disable, 1:Enable

  Status = CommonSetVariable (
             L"Setup",
             &gSystemConfigurationGuid,
             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
             sizeof (SYSTEM_CONFIGURATION),
             SetupNvData
             );

  FreePool (SetupNvData);

  return Status;
}

/**
  Set secure key to BIOS Region.

  @param  RandomNumber                  A pointer to the secure key.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SetSecureKeyToBiosRegion (
  UINT8                                 *SecureKey
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;
  UINT8                                 *L05SystemPasswordArea;
  UINTN                                 L05SystemPasswordSize;
  UINT8                                 *L05NaturalFileGuardSecureKey;

  SmmFwb                       = NULL;
  L05SystemPasswordArea        = NULL;
  L05NaturalFileGuardSecureKey = NULL;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, (VOID **) &SmmFwb);

  } else {
    Status = mSmst->SmmLocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, (VOID **) &SmmFwb);
  }

  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  //
  // Get system password region.
  //
  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get allocated system password area.
  //
  Status = GetSystemPasswordArea (
             &L05SystemPasswordArea,
             &L05SystemPasswordSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get secure key from system password area.
  //
  L05NaturalFileGuardSecureKey = (UINT8 *) ((UINTN) L05SystemPasswordArea + L05SystemPasswordSize - L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE - L05_HDD_PASSWORD_SECURITY_KEY_SIZE);

  //
  // Set random number.
  //
  CopyMem (L05NaturalFileGuardSecureKey, SecureKey, L05_HDD_PASSWORD_SECURITY_KEY_SIZE);

  Status = FlashBiosRegion (
             SmmFwb,
             L05SystemSupervisorPasswordRegionBase,
             L05SystemPasswordSize,
             L05SystemPasswordArea
             );

  if (L05SystemPasswordArea != NULL) {
    FreePool (L05SystemPasswordArea);
  }

  return Status;
}
/**
  Set secure key.

  @param  RandomNumber                  A pointer to the secure key.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SetSecureKey (
  UINT8                                 *SecureKey
  )
{
  EFI_STATUS                            Status;

  Status = OemSvcSetSecureKey (SecureKey);

  if (!EFI_ERROR (Status)) {
    return Status;
  }

#ifdef L05_NATURAL_FILE_GUARD_TEST_ON_CRB
  Status = SetSecureKeyToBiosRegion (SecureKey);
#endif

  return Status;

}

/**
  Clear Secure Key.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ClearSecureKey (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *SecureKey;
  UINTN                                 SecureKeySize;

  Status        = EFI_SUCCESS;
  SecureKey     = NULL;
  SecureKeySize = L05_HDD_PASSWORD_SECURITY_KEY_SIZE;

  SecureKey = AllocateZeroPool (SecureKeySize);

  if (SecureKey == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Set secure key to BIOS Region.
  //
  Status = SetSecureKey (SecureKey);

  return Status;
}

/**
  Get L05 Secure Key from BIOS Region.

  @param  SecureKey                     A pointer to the Secure Key.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetSecureKeyFromBiosRegion (
  UINT8                                 **SecureKey
  )
{
  EFI_STATUS                            Status;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;
  UINTN                                 L05HddPasswordSecureKeyRegion;

  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get HDD password secure key form password region.
  //
  L05HddPasswordSecureKeyRegion = L05SystemSupervisorPasswordRegionBase + L05SystemSupervisorPasswordRegionSize + L05SystemUserPasswordRegionSize - L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE - L05_HDD_PASSWORD_SECURITY_KEY_SIZE;
  *SecureKey = AllocateCopyPool (L05_HDD_PASSWORD_SECURITY_KEY_SIZE, (VOID *) L05HddPasswordSecureKeyRegion);

  return Status;
}
/**
  Get L05 Secure Key.

  @param  SecureKey                     A pointer to the Secure Key.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
L05GetSecureKey (
  UINT8                                 **SecureKey
  )
{
  EFI_STATUS                            Status;

  Status = OemSvcGetSecureKey (SecureKey);

  if (!EFI_ERROR (Status)) {
    return Status;
  }

#ifdef L05_NATURAL_FILE_GUARD_TEST_ON_CRB
  Status = GetSecureKeyFromBiosRegion (SecureKey);
#endif

  return Status;
}

/**
  Check secure key for natural file guard is exist.

  @retval TRUE                          Secure key for natural file guard is exist.
  @retval FALSE                         Secure key for natural file guard is not exist.
**/
BOOLEAN
IsSecureKeyExist (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *SecureKey;
  UINT8                                 *TempBuffer;
  BOOLEAN                               SecureKeyExist;

  SecureKey      = NULL;
  TempBuffer     = NULL;
  SecureKeyExist = TRUE;

  Status = L05GetSecureKey (&SecureKey);

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  TempBuffer = AllocateZeroPool (L05_HDD_PASSWORD_SECURITY_KEY_SIZE);

  if (TempBuffer == NULL) {
    return FALSE;
  }

  if (CompareMem (SecureKey, TempBuffer, L05_HDD_PASSWORD_SECURITY_KEY_SIZE) == 0) {
    SecureKeyExist = FALSE;
  }

  SetMem (TempBuffer, L05_HDD_PASSWORD_SECURITY_KEY_SIZE, 0xFF);

  if (CompareMem (SecureKey, TempBuffer, L05_HDD_PASSWORD_SECURITY_KEY_SIZE) == 0) {
    SecureKeyExist = FALSE;
  }

  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.1 Secure Key Generation
  //     BIOS should clear the key buffer after the encryption/decryption.
  //
  SetMem (SecureKey, L05_HDD_PASSWORD_SECURITY_KEY_SIZE, 0);
  FreePool (SecureKey);

  return SecureKeyExist;
}

/**
  Generate Secure Key.

  Secure Key is 32 bytes (256 bits).
  A 256 bits AES key is needed for UHDP encryption.

  @retval UINT8 *                       A pointer to Secure Key.
**/
UINT8 *
GenerateSecureKey (
  VOID
  )
{
  EFI_STATUS                            Status;
  CRYPTO_SERVICES_PROTOCOL              *CryptoService;
  EFI_TIME                              Time;
  UINT8                                 *SecureKey;
  UINTN                                 SecureKeySize;

  Status        = EFI_SUCCESS;
  CryptoService = NULL;
  SecureKey     = NULL;
  SecureKeySize = L05_HDD_PASSWORD_SECURITY_KEY_SIZE;

  Status = OemSvcGenerateSecureKey (&SecureKey);

  if (!EFI_ERROR (Status)) {
    return SecureKey;
  }

  Status = gBS->LocateProtocol (
                  &gCryptoServicesProtocolGuid,
                  NULL,
                  (VOID **) &CryptoService
                  );

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  ZeroMem (&Time, sizeof (Time));

  SecureKey = AllocateZeroPool (SecureKeySize);

  if (SecureKey == NULL) {
    return NULL;
  }

  gRT->GetTime (&Time, NULL);

  Status = CryptoService->RandomSeed ((UINT8 *) &Time, sizeof (Time));

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  Status = CryptoService->RandomBytes (SecureKey, SecureKeySize);

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  //
  // Set secure key to BIOS Region.
  //
  Status = SetSecureKey (SecureKey);

  return SecureKey;
}


/**
  Initialize secure key for natural file guard.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardInitSecureKey (
  VOID
  )
{
  UINT8                                 *SecureKey;

  SecureKey = NULL;

  //
  // Generate secure key.
  //
  SecureKey = GenerateSecureKey ();

  if (SecureKey == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.1 Secure Key Generation
  //     BIOS should clear the key buffer after the encryption/decryption.
  //
  SetMem (SecureKey, L05_HDD_PASSWORD_SECURITY_KEY_SIZE, 0);
  FreePool (SecureKey);

  return EFI_SUCCESS;
}

/**
  Generate random IV.

  IV is 16 bytes (128 bits).
  A new random IV is created EVERY time the UHDP is ENCRYPTED for store.

  @retval UINT8 *                       A pointer to Random IV.
**/
UINT8 *
GenerateRandomIv (
  VOID
  )
{
  EFI_STATUS                            Status;
  CRYPTO_SERVICES_PROTOCOL              *CryptoService;
  EFI_TIME                              Time;
  UINTN                                 RandomIvSize;
  UINT8                                 *RandomIv;

  Status        = EFI_SUCCESS;
  CryptoService = NULL;
  RandomIvSize  = L05_HDD_PASSWORD_IV_SIZE;
  RandomIv      = NULL;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gCryptoServicesProtocolGuid, NULL, (VOID **) &CryptoService);

  } else {
    Status = mSmst->SmmLocateProtocol (&gCryptoServicesProtocolGuid, NULL, (VOID **) &CryptoService);
  }

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  ZeroMem (&Time, sizeof (Time));

  RandomIv = AllocateZeroPool (RandomIvSize);

  if (RandomIv == NULL) {
    return NULL;
  }

  gRT->GetTime (&Time, NULL);

  Status = CryptoService->RandomSeed ((UINT8 *) &Time, sizeof (Time));

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  Status = CryptoService->RandomBytes (RandomIv, RandomIvSize);

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  return RandomIv;
}

/**
  PKCS5 padding.

  The block length B = 8.
  If the block length is B then add N padding bytes of value N to make the input length up to the next exact multiple of B.
  If the input length is already an exact multiple of B then add B bytes of value B.
  Thus padding of length N between one and B bytes is always added in an unambiguous manner.

  @param  PaddingPtr                    A pointer to the Padding buffer.
  @param  PaddingSize                   Padding buffer size.
  @param  DataSize                      Padding data size.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
Pkcs5Padding (
  IN OUT UINT8                          *PaddingPtr,
  IN  UINTN                             PaddingSize,
  IN  UINTN                             DataSize
  )
{
  UINT8                                 *TempPaddingPtr;
  UINTN                                 NumPaddingBlock;
  UINTN                                 Index;
  UINTN                                 Index2;
  UINT8                                 PaddingNum;
  BOOLEAN                               IsNonBlockPadding;

  if ((PaddingPtr == NULL) || (PaddingSize < DataSize) || (DataSize == 0)) {
    return EFI_INVALID_PARAMETER;
  }

  TempPaddingPtr = PaddingPtr + DataSize;
  NumPaddingBlock = ((PaddingSize - DataSize) % PKCS5_PADDING_BLOCK_SIZE == 0) ? (PaddingSize - DataSize) / PKCS5_PADDING_BLOCK_SIZE : (PaddingSize - DataSize) / PKCS5_PADDING_BLOCK_SIZE + 1;
  IsNonBlockPadding = ((PaddingSize - DataSize) % PKCS5_PADDING_BLOCK_SIZE == 0) ? FALSE : TRUE;

  for (Index = 0; Index < NumPaddingBlock; Index++) {
    if (IsNonBlockPadding) {
      PaddingNum = PKCS5_PADDING_BLOCK_SIZE - ((PaddingSize - DataSize) % PKCS5_PADDING_BLOCK_SIZE);
      IsNonBlockPadding = FALSE;

    } else {
      PaddingNum = PKCS5_PADDING_BLOCK_SIZE;
    }

    for (Index2 = 0; Index2 < PaddingNum; Index2++) {
      *TempPaddingPtr = PaddingNum;
      TempPaddingPtr++;
    }
  }

  return EFI_SUCCESS;
}

/**
  PKCS5 unpadding.

  The block length B = 8.
  Check that the last N bytes of the decrypted data all have value N with 1 < N = B.
  If so, strip N bytes, otherwise throw a decryption error.

  @param  PaddingPtr                    A pointer to the Padding buffer.
  @param  PaddingSize                   Padding buffer size.
  @param  DataSize                      Padding data size.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
Pkcs5Unpadding (
  IN  UINT8                              *PaddingPtr,
  IN  UINTN                             PaddingSize,
  OUT UINTN                             *DataSize
  )
{
  UINTN                                 Index;
  UINT8                                 *EndPaddingPtr;
  UINT8                                 PaddingNum;
  BOOLEAN                               IsEndPadding;

  if ((PaddingPtr == NULL) || (PaddingSize < PKCS5_PADDING_BLOCK_SIZE)) {
    return EFI_INVALID_PARAMETER;
  }

  IsEndPadding = FALSE;
  EndPaddingPtr = PaddingPtr + PaddingSize - 1;
  *DataSize = PaddingSize;

  while (*DataSize >= PKCS5_PADDING_BLOCK_SIZE) {
    PaddingNum = *EndPaddingPtr;

    if (PaddingNum > PKCS5_PADDING_BLOCK_SIZE) {
      break;
    }

    for (Index = 0; Index < PaddingNum; Index++) {
      if (*EndPaddingPtr != PaddingNum) {
        IsEndPadding = TRUE;
        break;
      }

      EndPaddingPtr--;
    }

    if (IsEndPadding) {
      break;
    }

    *DataSize = *DataSize - PaddingNum;
  }

  return EFI_SUCCESS;
}

/**
  UHDP encryption.

  The following design assumptions apply:
   - AES 256 CBC PKCS5Padding mode is used
   - IV is 16 bytes (128 bits) A new random IV is created EVERY time the UHDP is ENCRYPTED for store
   - SHA 256 of user input UHDP

  Clear text data to encrypt is 64 bytes; Ciphertext (encrypted data) will be 64 bytes.

  Input data = [16 byte IV | SHA 256 of user input UHDP | Padding]
   - 16 byte IV: Random 16 byte string used for IV for encryption; this will need to be stored in the clear in the SPI with the ciphertext
   - SHA 256 of user input UHDP: 32 bytes
   - Padding: PKCS5Padding

  Encrypted UHDP store to SPI
    UEFI variable contents:
     - 16 byte IV
     - 64 byte ciphertext
     - 16 byte HDD ID (Reserved for future version, to support different HDP for each HDD)

  @param  DataPtr                       A pointer to the encryption buffer.
  @param  DataSize                      Padding encryption buffer size.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
UhdpEncryption (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize
  )
{
  EFI_STATUS                            Status;
  CRYPTO_SERVICES_PROTOCOL              *CryptoService;
  BOOLEAN                               CryptoSuccess;
  UINT8                                 *HashData;
  UINT8                                 *SecureKey;
  UINT8                                 *RandomIv;
  UINT8                                 *InputData;
  UINT8                                 *EncryptData;
  VOID                                  *AesCtx;
  LENOVO_UHDP_STORE                     *L05UhdpStore;

  CryptoService = NULL;
  HashData      = NULL;
  SecureKey     = NULL;
  RandomIv      = NULL;
  InputData     = NULL;
  EncryptData   = NULL;
  AesCtx        = NULL;
  L05UhdpStore  = NULL;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gCryptoServicesProtocolGuid, NULL, (VOID **) &CryptoService);

  } else {
    Status = mSmst->SmmLocateProtocol (&gCryptoServicesProtocolGuid, NULL, (VOID **) &CryptoService);
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  HashData = AllocateZeroPool (SHA256_DIGEST_SIZE);

  if (HashData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // SHA 256 of user input UHDP
  //
  Status = L05Sha256Hash (DataPtr, DataSize, HashData);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // A 256 bits AES key is needed for UHDP encryption.
  //
  Status = NaturalFileGuardInitSecureKey ();

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = L05GetSecureKey (&SecureKey);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (!IsSecureKeyExist ()) {
    return EFI_SECURITY_VIOLATION;
  }

  //
  // IV is 16 bytes (128 bits) A new random IV is created EVERY time the UHDP is ENCRYPTED for store.
  //
  RandomIv = GenerateRandomIv ();

  if (RandomIv == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  InputData = AllocateZeroPool (L05_HDD_PASSWORD_ENCRYPT_SIZE);

  if (InputData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  EncryptData = AllocateZeroPool (L05_HDD_PASSWORD_ENCRYPT_SIZE);

  if (EncryptData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Input data = [16 byte IV | SHA 256 of user input UHDP | Padding]
  //
  CopyMem (InputData, RandomIv, L05_HDD_PASSWORD_IV_SIZE);
  CopyMem ((InputData + L05_HDD_PASSWORD_IV_SIZE), HashData, SHA256_DIGEST_SIZE);

  //
  // Padding: PKCS5Padding
  //
  Status = Pkcs5Padding (InputData, L05_HDD_PASSWORD_ENCRYPT_SIZE, (L05_HDD_PASSWORD_IV_SIZE + SHA256_DIGEST_SIZE));

  if (EFI_ERROR (Status)) {
    return Status;
  }

  AesCtx = AllocatePool (CryptoService->AesGetContextSize ());

  if (AesCtx == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CryptoSuccess = CryptoService->AesInit (AesCtx, SecureKey, (L05_HDD_PASSWORD_SECURITY_KEY_SIZE * BITS_PER_BYTE));

  if (!CryptoSuccess) {
    return EFI_SECURITY_VIOLATION;
  }

  //
  // AES 256 CBC PKCS5Padding mode is used
  //
  CryptoSuccess = CryptoService->AesCbcEncrypt (
                                   AesCtx,
                                   InputData,
                                   L05_HDD_PASSWORD_ENCRYPT_SIZE,
                                   RandomIv,
                                   EncryptData
                                   );

  if (!CryptoSuccess) {
    return EFI_SECURITY_VIOLATION;
  }

  //
  // UEFI variable contents:
  //  - 16 byte IV
  //  - 64 byte ciphertext
  //  - 16 byte HDD ID (Reserved for future version, to support different HDP for each HDD)
  //
  L05UhdpStore = AllocateZeroPool (sizeof (LENOVO_UHDP_STORE));

  CopyMem (L05UhdpStore->Iv, RandomIv, L05_HDD_PASSWORD_IV_SIZE);
  CopyMem (L05UhdpStore->Ciphertext, EncryptData, L05_HDD_PASSWORD_ENCRYPT_SIZE);

  Status = CommonSetVariable (
             L05_NATURAL_FILE_GUARD_VARIABLE_NAME,
             &gEfiL05NaturalFileGuardVariableGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             sizeof (LENOVO_UHDP_STORE),
             L05UhdpStore
             );

  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.1 Secure Key Generation
  //     BIOS should clear the key buffer after the encryption/decryption.
  //
  SetMem (SecureKey, L05_HDD_PASSWORD_SECURITY_KEY_SIZE, 0);

  FreePool (HashData);
  FreePool (SecureKey);
  FreePool (RandomIv);
  FreePool (InputData);
  FreePool (EncryptData);
  FreePool (AesCtx);
  FreePool (L05UhdpStore);

  return Status;
}

/**
  UHDP decryption.

  The following design assumptions apply:
   - AES 256 CBC PKCS5Padding mode is used
   - IV is 16 bytes (128 bits) A new random IV is created EVERY time the UHDP is ENCRYPTED for store
   - SHA 256 of user input UHDP

  Clear text data to encrypt is 64 bytes; Ciphertext (encrypted data) will be 64 bytes.

  Input data = [16 byte IV | SHA 256 of user input UHDP | Padding]
   - 16 byte IV: Random 16 byte string used for IV for encryption; this will need to be stored in the clear in the SPI with the ciphertext
   - SHA 256 of user input UHDP: 32 bytes
   - Padding: PKCS5Padding

  @param  DataPtr                       A pointer to the encryption buffer.
  @param  DataSize                      Padding encryption buffer size.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/EFI_STATUS
UhdpDecryption (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             *DataSize
  )
{
  EFI_STATUS                            Status;
  LENOVO_UHDP_STORE                     *L05UhdpStore;
  UINTN                                 BufferSize;
  UINT8                                 *DecryptData;
  CRYPTO_SERVICES_PROTOCOL              *CryptoService;
  BOOLEAN                               CryptoSuccess;
  VOID                                  *AesCtx;
  UINT8                                 *SecureKey;
  UINTN                                 UnpaddingSize;

  if (!IsSecureKeyExist ()) {
    return EFI_SECURITY_VIOLATION;
  }

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gCryptoServicesProtocolGuid, NULL, (VOID **) &CryptoService);

  } else {
    Status = mSmst->SmmLocateProtocol (&gCryptoServicesProtocolGuid, NULL, (VOID **) &CryptoService);
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  BufferSize = sizeof (LENOVO_UHDP_STORE);
  L05UhdpStore = AllocateZeroPool (sizeof (LENOVO_UHDP_STORE));

  Status = CommonGetVariable (
             L05_NATURAL_FILE_GUARD_VARIABLE_NAME,
             &gEfiL05NaturalFileGuardVariableGuid,
             &BufferSize,
             L05UhdpStore
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  DecryptData = AllocateZeroPool (L05_HDD_PASSWORD_ENCRYPT_SIZE);

  if (DecryptData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  AesCtx = AllocatePool (CryptoService->AesGetContextSize ());

  if (AesCtx == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = L05GetSecureKey (&SecureKey);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  CryptoSuccess = CryptoService->AesInit (AesCtx, SecureKey, (L05_HDD_PASSWORD_SECURITY_KEY_SIZE * BITS_PER_BYTE));

  if (!CryptoSuccess) {
    return EFI_SECURITY_VIOLATION;
  }

  //
  // AES 256 CBC PKCS5Padding mode is used
  //
  CryptoSuccess = CryptoService->AesCbcDecrypt (
                                   AesCtx,
                                   L05UhdpStore->Ciphertext,
                                   L05_HDD_PASSWORD_ENCRYPT_SIZE,
                                   L05UhdpStore->Iv,
                                   DecryptData
                                   );

  if (!CryptoSuccess) {
    return EFI_SECURITY_VIOLATION;
  }

  //
  // Padding: PKCS5Padding
  //
  Status = Pkcs5Unpadding (DecryptData, L05_HDD_PASSWORD_ENCRYPT_SIZE, &UnpaddingSize);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (*DataSize < UnpaddingSize - L05_HDD_PASSWORD_IV_SIZE) {
    return EFI_OUT_OF_RESOURCES;
  }

  *DataSize = UnpaddingSize - L05_HDD_PASSWORD_IV_SIZE;
  CopyMem (DataPtr, (DecryptData + L05_HDD_PASSWORD_IV_SIZE), *DataSize);


  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.1 Secure Key Generation
  //     BIOS should clear the key buffer after the encryption/decryption.
  //
  SetMem (SecureKey, L05_HDD_PASSWORD_SECURITY_KEY_SIZE, 0);

  FreePool (L05UhdpStore);
  FreePool (DecryptData);
  FreePool (AesCtx);
  FreePool (SecureKey);

  return Status;
}
#endif

