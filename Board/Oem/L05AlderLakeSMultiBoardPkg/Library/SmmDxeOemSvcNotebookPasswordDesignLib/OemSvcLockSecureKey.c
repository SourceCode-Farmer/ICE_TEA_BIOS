/** @file
  Provides an interface to Lock Secure Key by EC.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

/**
  Provides an interface to Lock Secure Key by EC.

  @retval EFI_SUCCESS                   Lock Secure Key success.
  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcLockSecureKey (
  VOID
  )
{
  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.1 Secure Key Generation
  //     Procedure:
  //       2. On the Ready to boot event, BIOS should issue command to EC to lock the secure key data area in
  //          the EC and prevent Read/Write access.
  //

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
