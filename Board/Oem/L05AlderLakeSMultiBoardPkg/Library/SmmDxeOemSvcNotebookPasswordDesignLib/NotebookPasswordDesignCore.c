/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

/**
  SHA256 Hash function.

  @param  DataPtr                       A pointer to the Data.
  @param  DataLength                    A pointer to the Data Size.
  @param  HashData                      A pointer to the Hash data.

  @retval EFI_SUCCESS                   Hash returned successfully.
  @retval EFI_OUT_OF_RESOURCES          Allocate Memory is Failed.
  @retval EFI_INVALID_PARAMETER         (1)Message or Hash is NULL. (2) DataPtr or HashData is NULL.
  @retval EFI_UNSUPPORTED               The algorithm specified by HashAlgorithm is not supported by this
                                        driver. Or extend is TRUE and the algorithm doesn't support extending the hash.
**/
EFI_STATUS
L05Sha256Hash (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  OUT UINT8                             *HashData
  )
{
  EFI_STATUS                            Status;
  EFI_HASH_PROTOCOL                     *EfiHash;
  EFI_HASH_OUTPUT                       HashOutput;

  Status                = EFI_SUCCESS;
  EfiHash               = NULL;
  HashOutput.Sha256Hash = NULL;

  if ((DataPtr == NULL) || (DataSize == 0) || (HashData == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gEfiHashProtocolGuid, NULL, (VOID **) &EfiHash);

  } else {
    Status = mSmst->SmmLocateProtocol (&gEfiHashProtocolGuid, NULL, (VOID **) &EfiHash);
  }

  //
  // Hash Data for SHA-256 type
  //
  HashOutput.Sha256Hash = AllocateZeroPool (SHA256_DIGEST_SIZE);

  if (HashOutput.Sha256Hash == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = EfiHash->Hash (
                      EfiHash,
                      &gEfiHashAlgorithmSha256Guid,
                      FALSE,
                      (UINT8 *) DataPtr,
                      DataSize,
                      (EFI_HASH_OUTPUT *) &HashOutput
                      );

  if (!EFI_ERROR (Status)) {
    CopyMem (HashData, HashOutput.Sha256Hash, SHA256_DIGEST_SIZE);
  }

  FreePool (HashOutput.Sha256Hash);

  return Status;
}

/**
  Generate random number by Length.

  @param  Length                        Length for generat random number.

  @retval UINT8 *                       A pointer to the random number.
**/
UINT8 *
GenerateRandomNumber (
  UINTN                                 Length
  )
{
  EFI_STATUS                            Status;
  CRYPTO_SERVICES_PROTOCOL              *CryptoService;
  EFI_TIME                              Time;
  UINT8                                 *RandomNumber;

  Status        = EFI_SUCCESS;
  RandomNumber  = NULL;

  if (!L05IsInSmm ()) {
    Status = gBS->LocateProtocol (&gCryptoServicesProtocolGuid, NULL, &CryptoService);

  } else {
    Status = mSmst->SmmLocateProtocol (&gCryptoServicesProtocolGuid, NULL, &CryptoService);
  }

  if (EFI_ERROR (Status)) {
    return NULL;
  }

  ZeroMem (&Time, sizeof (Time));

  RandomNumber = AllocateZeroPool (Length);

  if (RandomNumber == NULL) {
    return NULL;
  }

  gRT->GetTime (&Time, NULL);
  CryptoService->RandomSeed ((UINT8 *) &Time, sizeof (Time));
  CryptoService->RandomBytes (RandomNumber, Length);

  return RandomNumber;
}

/**
  Get random number for BIOS password.

  @param  RandomNumber                  A pointer to the random number for BIOS password.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetBiosPasswordRandomNumber (
  UINT8                                 **RandomNumber
  )
{
  EFI_STATUS                            Status;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;

  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get BIOS password random number form password region.
  //
  *RandomNumber = (UINT8 *) ((UINTN) L05SystemSupervisorPasswordRegionBase + L05SystemSupervisorPasswordRegionSize + L05SystemUserPasswordRegionSize - L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE);

  return Status;
}

/**
  Check random number for BIOS password is exist.

  @retval TRUE                          Random number for BIOS password is exist.
  @retval FALSE                         Random number for BIOS password is not exist.
**/
BOOLEAN
IsBiosPasswordRandomNumberExist (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *RandomNumber;
  UINT8                                 *TempBuffer;
  BOOLEAN                               RandomNumberExist;

  RandomNumber      = NULL;
  TempBuffer        = NULL;
  RandomNumberExist = TRUE;

  Status = GetBiosPasswordRandomNumber (&RandomNumber);

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  TempBuffer = AllocateZeroPool (L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE);

  if (TempBuffer == NULL) {
    return FALSE;
  }

  if (CompareMem (RandomNumber, TempBuffer, L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE) == 0) {
    RandomNumberExist = FALSE;
  }

  SetMem (TempBuffer, L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE, 0xFF);

  if (CompareMem (RandomNumber, TempBuffer, L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE) == 0) {
    RandomNumberExist = FALSE;
  }

  //
  // Random number is exist.
  //
  return RandomNumberExist;
}

/**
  Init random number for BIOS password.

  @param  SmmFwb                        A pointer to the EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL.
  @param  RandomNumber                  A pointer to random number for BIOS password.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
InitBiosPassworRandomNumber (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb,
  UINT8                                 *RandomNumber
  )
{
  EFI_STATUS                            Status;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;
  UINT8                                 *L05SystemPasswordArea;
  UINTN                                 L05SystemPasswordSize;
  UINT8                                 *L05BiosPasswordRandomNumber;

  //
  // Get system password region.
  //
  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get allocated system password area.
  //
  Status = GetSystemPasswordArea (
             &L05SystemPasswordArea,
             &L05SystemPasswordSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Clear system password area to 0xFF.
  //
#ifndef L05_NATURAL_FILE_GUARD_ENABLE
  SetMem ((VOID *) L05SystemPasswordArea, (L05SystemPasswordSize - L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE), 0xFF);
#else
  SetMem ((VOID *) L05SystemPasswordArea, (L05SystemPasswordSize - L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE - L05_HDD_PASSWORD_SECURITY_KEY_SIZE), 0xFF);
#endif

  //
  // Get BIOS password encode header from system password area.
  //
  L05BiosPasswordRandomNumber = (UINT8 *) ((UINTN) L05SystemPasswordArea + L05SystemPasswordSize - L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE);

  //
  // Set random number.
  //
  CopyMem (L05BiosPasswordRandomNumber, RandomNumber, L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE);

  Status = FlashBiosRegion (
             SmmFwb,
             L05SystemSupervisorPasswordRegionBase,
             L05SystemPasswordSize,
             L05SystemPasswordArea
             );

  if (L05SystemPasswordArea != NULL) {
    FreePool (L05SystemPasswordArea);
  }

  return Status;
}

/**
  Initialize BIOS password encode.

  @param  SmmFwb                        A pointer to the EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
BiosPasswordInit (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *RandomNumber;

  RandomNumber = NULL;

  //
  // Check random number for BIOS password is exist.
  //
  if (IsBiosPasswordRandomNumberExist ()) {
    return EFI_SUCCESS;
  }

  //
  // Generate random number for BIOS password.
  //
  RandomNumber = GenerateRandomNumber (L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE);

  if (RandomNumber == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Init random number for BIOS password.
  //
  Status = InitBiosPassworRandomNumber (SmmFwb, RandomNumber);

  return Status;
}

/**
  Security password encode phase 1.

  User Input (64chars) -> SHA 256 (32bytes)

  @param  DataPtr                       A pointer to the buffer for password.
  @param  DataSize                      The buffer size for password.
  @param  EncodeData                    A pointer to the buffer for encode data.
  @param  EncodeDataSize                The buffer size for encode data.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SecurityPasswordEncodePhase1 (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *HashData;

  if ((DataPtr == NULL) || (DataSize == 0) ||
      (EncodeData == NULL) || (EncodeDataSize < SHA256_DIGEST_SIZE)) {
    return EFI_INVALID_PARAMETER;
  }

  HashData = NULL;
  HashData = AllocateZeroPool (SHA256_DIGEST_SIZE);

  if (HashData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = L05Sha256Hash (DataPtr, DataSize, HashData);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  CopyMem (EncodeData, HashData, SHA256_DIGEST_SIZE);

  FreePool (HashData);

  return Status;
}

/**
  Security password encode phase 2.

  Concatenate -> SHA 256 (32bytes)

  @param  DataPtr                       A pointer to the buffer for password.
  @param  DataSize                      The buffer size for password.
  @param  ConcatenateData               A pointer to the buffer for concatenate data.
  @param  ConcatenateDataSize           The buffer size for concatenate data.
  @param  EncodeData                    A pointer to the buffer for encode data.
  @param  EncodeDataSize                The buffer size for encode data.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SecurityPasswordEncodePhase2 (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  IN  UINT8                             *ConcatenateData,
  IN  UINTN                             ConcatenateDataSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *HashData;
  UINT8                                 *Concatenate;
  UINTN                                 ConcatenateSize;

  if ((DataPtr == NULL) || (DataSize == 0) ||
      (ConcatenateData == NULL) || (ConcatenateData == 0) ||
      (EncodeData == NULL) || (EncodeDataSize < SHA256_DIGEST_SIZE)) {
    return EFI_INVALID_PARAMETER;
  }

  HashData    = NULL;
  Concatenate = NULL;

  HashData = AllocateZeroPool (SHA256_DIGEST_SIZE);

  if (HashData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  ConcatenateSize = SHA256_DIGEST_SIZE + ConcatenateDataSize;
  Concatenate = AllocateZeroPool (ConcatenateSize);

  if (Concatenate == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (Concatenate, DataPtr, DataSize);
  CopyMem ((UINT8 *) ((UINTN) Concatenate + SHA256_DIGEST_SIZE), ConcatenateData, ConcatenateDataSize);
  Status = L05Sha256Hash (Concatenate, ConcatenateSize, HashData);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  CopyMem (EncodeData, HashData, SHA256_DIGEST_SIZE);

  FreePool (HashData);
  FreePool (Concatenate);

  return Status;
}

