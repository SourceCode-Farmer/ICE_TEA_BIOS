#******************************************************************************
#* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  BASE_NAME                      = SmmDxeOemSvcNotebookPasswordDesignLib
  FILE_GUID                      = F4236BDE-EC83-4775-BDCF-77EF377AAD31
  MODULE_TYPE                    = COMBINED_SMM_DXE
  VERSION_STRING                 = 1.00
  LIBRARY_CLASS                  = OemSvcSecurityPasswordLib|COMBINED_SMM_DXE DXE_DRIVER DXE_SMM_DRIVER UEFI_DRIVER DXE_RUNTIME_DRIVER
  CONSTRUCTOR                    = OemSvcSecurityPasswordLibConstructor
  INF_VERSION                    = 0x00010017

[Sources]
  SmmDxeOemSvcNotebookPasswordDesignLib.c
  SmmDxeOemSvcNotebookPasswordDesignLib.h
  OemSvcGetSystemPasswords.c
  OemSvcSetSystemPasswords.c
  OemSvcGenerateSecureKey.c
  OemSvcGetSecureKey.c
  OemSvcSetSecureKey.c
  OemSvcLockSecureKey.c
  NotebookPasswordDesignCore.c
  NaturalFileGuardCore.c
  NaturalFileGuardCore.h

[Packages]
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  InsydeModulePkg/InsydeModulePkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec  ## ChipsetSetupConfig.h
!if $(L05_CHIPSET_VENDOR) == INTEL
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec  ## PchLimits.h
!endif
  
[LibraryClasses]
  UefiLib
  BaseLib
  BaseMemoryLib
  MemoryAllocationLib
  RngLib
  DebugLib
  UefiRuntimeServicesTableLib
  VariableLib

[Protocols]
  gEfiL05VariableProtocolGuid
  gEfiSmmFwBlockServiceProtocolGuid
  gEfiHashProtocolGuid
  gCryptoServicesProtocolGuid

[Guids]
  gL05SystemSuperPasswordGuid
  gL05SystemUserPasswordGuid
  gL05H2OFlashMapRegionEepromGuid
  gL05H2OFlashMapRegionSupervisorPasswordGuid
  gL05H2OFlashMapRegionUserPasswordGuid
  gSystemConfigurationGuid
  gEfiL05NaturalFileGuardVariableGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdDefaultSysPasswordMaxLength
  gL05ServicesTokenSpaceGuid.PcdL05NaturalFileGuardUnlockStoredUhdpFlag
