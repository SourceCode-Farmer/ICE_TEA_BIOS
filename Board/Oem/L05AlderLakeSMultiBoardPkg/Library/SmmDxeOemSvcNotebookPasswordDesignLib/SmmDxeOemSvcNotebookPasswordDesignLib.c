/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

EFI_SMM_SYSTEM_TABLE2                   *mSmst = NULL;

/**
  Transfer each other while being front and back.

  @param[in] Data                       The address of data.
  @param[in] Size                       Size of data.

**/
VOID
L05SwapEntries (
  IN CHAR8                              *Data,
  IN UINT16                             Size
  )
{
  UINT16                                Index;
  CHAR8                                 Temp8;

  for (Index = 0; (Index + 1) < Size; Index += 2) {
    Temp8           = Data[Index];
    Data[Index]     = Data[Index + 1];
    Data[Index + 1] = Temp8;
  }
}

/**
  System Password Encode.

  @param  DataPtr                       A pointer to the buffer for system password.
  @param  DataSize                      The buffer size for system password.
  @param  EncodeData                    A pointer to the buffer for encode data.
  @param  EncodeDataSize                The buffer size for encode data.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SystemPasswordEncode (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *RandomNumber;

  if ((DataPtr == NULL) || (DataSize == 0) ||
      (EncodeData == NULL) || (EncodeDataSize < SHA256_DIGEST_SIZE)) {
    return EFI_INVALID_PARAMETER;
  }

  Status = GetBiosPasswordRandomNumber (&RandomNumber);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SecurityPasswordEncodePhase1 (
             DataPtr,
             DataSize,
             EncodeData,
             EncodeDataSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SecurityPasswordEncodePhase2 (
             EncodeData,
             EncodeDataSize,
             RandomNumber,
             L05_BIOS_PASSWORD_RANDOM_NUMBER_REGION_SIZE,
             EncodeData,
             EncodeDataSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return Status;
}

/**
  HDD Password Encode.

  @param  DataPtr                       A pointer to the buffer for system password.
  @param  DataSize                      The buffer size for system password.
  @param  ModelNumberPtr                A pointer to the buffer for model number.
  @param  ModelNumberSize               The buffer size for model number.
  @param  SerialNumberPtr               A pointer to the buffer for serial number.
  @param  SerialNumberSize              The buffer size for serial number.
  @param  EncodeData                    A pointer to the buffer for encode data.
  @param  EncodeDataSize                The buffer size for encode data.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
HddPasswordEncode (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  IN  UINT8                             *ModelNumberPtr,
  IN  UINTN                             ModelNumberSize,
  IN  UINT8                             *SerialNumberPtr,
  IN  UINTN                             SerialNumberSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *ConcatenateData;
  UINTN                                 ConcatenateDataSize;

  if ((DataPtr == NULL) || (DataSize == 0) ||
      (ModelNumberPtr == NULL) || (ModelNumberSize == 0) ||
      (SerialNumberPtr == NULL) || (SerialNumberSize == 0) ||
      (EncodeData == NULL) || (EncodeDataSize < SHA256_DIGEST_SIZE)) {
    return EFI_INVALID_PARAMETER;
  }

  ConcatenateDataSize = ModelNumberSize + SerialNumberSize;
  ConcatenateData = AllocateZeroPool (ConcatenateDataSize);

  if (ConcatenateData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (ConcatenateData, ModelNumberPtr, ModelNumberSize);
  CopyMem ((UINT8 *) ((UINTN) ConcatenateData + ModelNumberSize), SerialNumberPtr, SerialNumberSize);

  Status = SecurityPasswordEncodePhase1 (
             DataPtr,
             DataSize,
             EncodeData,
             EncodeDataSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

#ifdef L05_NATURAL_FILE_GUARD_ENABLE
  if (PcdGetBool (PcdL05NaturalFileGuardUnlockStoredUhdpFlag)) {
    CopyMem (EncodeData, DataPtr, EncodeDataSize);
  }
#endif

  Status = SecurityPasswordEncodePhase2 (
             EncodeData,
             EncodeDataSize,
             ConcatenateData,
             ConcatenateDataSize,
             EncodeData,
             EncodeDataSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  FreePool (ConcatenateData);

  return Status;
}

/**
  Detect whether the system is at SMM mode.

  @retval TRUE                          System is at SMM mode.
  @retval FALSE                         System is not at SMM mode.
**/
BOOLEAN
L05IsInSmm (
  VOID
  )
{
  if (mSmst != NULL) {
    return TRUE;

  } else {
    return FALSE;
  }
}

/**
  Check system has system password or not.

  @param  PasswordPtr                   A pointer to the system password.

  @retval TRUE                          Checksum for system password is vaild.
  @retval FALSE                         Checksum for system password is not valid.
**/
BOOLEAN
L05HaveSystemPassword (
  IN UINT8                              *PasswordPtr
  )
{
  UINT8                                 *TempBuffer;
  BOOLEAN                               HaveSystemPassword;

  TempBuffer         = NULL;
  HaveSystemPassword = TRUE;

  TempBuffer = AllocateZeroPool (SHA256_DIGEST_SIZE);

  if (TempBuffer == NULL) {
    return FALSE;
  }

  if (CompareMem (PasswordPtr, TempBuffer, SHA256_DIGEST_SIZE) == 0) {
    HaveSystemPassword = FALSE;
  }

  SetMem (TempBuffer, SHA256_DIGEST_SIZE, 0xFF);

  if (CompareMem (PasswordPtr, TempBuffer, SHA256_DIGEST_SIZE) == 0) {
    HaveSystemPassword = FALSE;
  }

  return HaveSystemPassword;
}

/**
  Refoer from Unicode2Ascii() at SysPasswordDxe.c

  System Password Packet.

  @param  PasswordPtr                   A pointer to the Password string.
  @param  PasswordLength                Password string length.

  @retval EncodePassword                Encode password string.
**/
UINT8 *
L05SysPasswordPacket (
  IN  UINT8                             *PasswordPtr,
  IN  UINTN                             PasswordLength
  )
{
  UINT8                                 *EncodePassword;
  UINT8                                 CheckSum;
  UINTN                                 Index;

  //
  // Initialization
  //
  EncodePassword = NULL;
  CheckSum       = 0;
  Index          = 0;

  //
  // Allocate resources
  //
#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
  EncodePassword = AllocateZeroPool (PasswordLength + 2);
#else
  EncodePassword = AllocateZeroPool (L05_SECURITY_SYSTEM_PASSWORD_LENGTH);
#endif

  if (EncodePassword == NULL) {
    return EncodePassword;
  }

#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
  //
  // Put password length to encode password
  //
  EncodePassword[0] = (UINT8) PasswordLength;

  //
  // Count check sum & Put password to encode password
  //
  CheckSum = EncodePassword[0];

  for (Index = 1; Index < PasswordLength + 1; Index++) {
    EncodePassword[Index] = (UINT8)(PasswordPtr[Index - 1]);
    CheckSum = CheckSum + EncodePassword[Index];
  }

  CheckSum = ~CheckSum + 0x01;

  //
  //put Checksum at the end of password
  //
  EncodePassword[Index] = CheckSum;
#else
  for (Index = 0; Index < PasswordLength; Index++) {
    EncodePassword[Index] = (UINT8)(PasswordPtr[Index]);
  }
#endif

  return EncodePassword;
}

/**
  L05 System Password Encode.

  @param  PasswordPtr                   A pointer to the Password string.
  @param  PasswordLength                Password string length.

  @retval EncodeData                    Encode Data.
**/
UINT8 *
L05SysPasswordEncode (
  IN  UINT8                             *PasswordPtr,
  IN  UINTN                             PasswordLength
  )
{
#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
  return PasswordPtr;

#else
  //
  // [Lenovo Notebook Password Design Spec V1.0]
  //   1 BIOS Password
  //
  EFI_STATUS                            Status;
  UINT8                                 *EncodeData;
  UINTN                                 EncodeDataSize;

  EncodeDataSize = SHA256_DIGEST_SIZE;
  EncodeData = AllocateZeroPool (EncodeDataSize);

  if (EncodeData == NULL) {
    return EncodeData;
  }

  PasswordLength = L05_SECURITY_SYSTEM_PASSWORD_LENGTH;
  Status = SystemPasswordEncode (PasswordPtr, PasswordLength, EncodeData, EncodeDataSize);

  FreePool (PasswordPtr);

  return EncodeData;
#endif
}

/**
  Compare two password.

  @param  Password1                     A pointer to password 1.
  @param  Password2                     A pointer to password 2.

  @retval TRUE                          They are the same password.
  @retval FALSE                         They are different password.

**/
BOOLEAN
L05PasswordCmp (
  UINT8                                 *Password1,
  UINT8                                 *Password2
  )
{
  UINT8                                 Index;
  UINTN                                 PasswordLength;

  if (Password1[0] != Password2[0]) {
    return FALSE;
  }

#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
  PasswordLength = Password1[0] + 2;

#else
  PasswordLength = SHA256_DIGEST_SIZE;
#endif

  for (Index = 0; Index < PasswordLength; Index++) {
    if (Password1[Index] != Password2[Index]) {
      return FALSE;
    }
  }

  return TRUE;
}

/**
  Get system password region from BIOS region.

  @param  L05SystemSupervisorPasswordRegionBase  A pointer to the L05 system supervisor password region base.
  @param  L05SystemSupervisorPasswordRegionSize  A pointer to the L05 system supervisor password region size.
  @param  L05SystemUserPasswordRegionBase        A pointer to the L05 system user password region base.
  @param  L05SystemUserPasswordRegionSize        A pointer to the L05 system user password region size.

  @retval EFI_SUCCESS                            The operation completed successfully.
  @retval EFI_INVALID_PARAMETER                  The input parameters is NULL.
**/
EFI_STATUS
GetSystemPasswordRegion (
  UINTN                                 *L05SystemSupervisorPasswordRegionBase,
  UINTN                                 *L05SystemSupervisorPasswordRegionSize,
  UINTN                                 *L05SystemUserPasswordRegionBase,
  UINTN                                 *L05SystemUserPasswordRegionSize
  )
{

  //
  // Check for invalid input parameters
  //
  if (L05SystemSupervisorPasswordRegionBase == NULL ||
      L05SystemSupervisorPasswordRegionSize == NULL ||
      L05SystemUserPasswordRegionBase       == NULL ||
      L05SystemUserPasswordRegionSize       == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *L05SystemSupervisorPasswordRegionBase = FdmGetNAtAddr (&gL05H2OFlashMapRegionSupervisorPasswordGuid, 1);
  *L05SystemSupervisorPasswordRegionSize = FdmGetNAtSize (&gL05H2OFlashMapRegionSupervisorPasswordGuid, 1);
  *L05SystemUserPasswordRegionBase       = FdmGetNAtAddr (&gL05H2OFlashMapRegionUserPasswordGuid, 1);
  *L05SystemUserPasswordRegionSize       = FdmGetNAtSize (&gL05H2OFlashMapRegionUserPasswordGuid, 1);

  return EFI_SUCCESS;
}

/**
  Get system password area.
  This function will allocate memory for system password area and copy system password from BIOS region.

  @param  L05SystemPasswordArea         A pointer to the L05 system password area.
  @param  L05SystemPasswordSize         A pointer to the L05 system password size.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         The input parameters is NULL.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetSystemPasswordArea (
  UINT8                                 **L05SystemPasswordArea,
  UINTN                                 *L05SystemPasswordSize
  )
{
  EFI_STATUS                            Status;
  UINTN                                 L05SystemSupervisorPasswordRegionBase;
  UINTN                                 L05SystemSupervisorPasswordRegionSize;
  UINTN                                 L05SystemUserPasswordRegionBase;
  UINTN                                 L05SystemUserPasswordRegionSize;

  //
  // Check for invalid input parameters
  //
  if (L05SystemPasswordArea == NULL || L05SystemPasswordSize == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = GetSystemPasswordRegion (
             &L05SystemSupervisorPasswordRegionBase,
             &L05SystemSupervisorPasswordRegionSize,
             &L05SystemUserPasswordRegionBase,
             &L05SystemUserPasswordRegionSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Total size of System Password.
  //
  *L05SystemPasswordSize = L05SystemSupervisorPasswordRegionSize + L05SystemUserPasswordRegionSize;

  *L05SystemPasswordArea = AllocateZeroPool (*L05SystemPasswordSize);

  if (*L05SystemPasswordArea == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem ((VOID *) *L05SystemPasswordArea, (VOID *) L05SystemSupervisorPasswordRegionBase, *L05SystemPasswordSize);

  return EFI_SUCCESS;
}

/**
 Flash BIOS region.
 1. erases one or more blocks as denoted by the variable argument list.
 2. Writes data beginning at Lba:Offset from FV. The write terminates either when *NumBytes of data have been written,
    or when a block boundary is reached.  *NumBytes is updated to reflect the actual number of bytes written.

  @param  SmmFwb                        A pointer to the EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL.
  @param  Offset                        Start address to be written.
  @param  NumBytes                      IN:  The requested write size.
                                        OUT: The data size by bytes has been written.
  @param  Buffer                        Data buffer want to write.

  @retval EFI_SUCCESS                   Write data successful.
  @retval EFI_INVALID_PARAMETER         Input function parameters are invalid.
  @retval EFI_UNSUPPORTED               The flash device is not supported
  @retval EFI_DEVICE_ERROR              Write data failed caused by device error.
**/
EFI_STATUS
FlashBiosRegion (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb,
  UINTN                                 Offset,
  UINTN                                 NumBytes,
  UINT8                                 *Buffer
  )
{
  EFI_STATUS                            Status;

  //
  // Make sure Password Region be cleared for writing data.
  //
  Status = SmmFwb->EraseBlocks (SmmFwb, Offset, &NumBytes);

  if (!EFI_ERROR (Status)) {
    Status = SmmFwb->Write (
                       SmmFwb,
                       Offset,
                       &NumBytes,
                       (UINT8 *) Buffer
                       );
  }

  return Status;
}

/**
  Initialize BIOS password encode Notify.

  @param[in] Event                      Event whose notification function is being invoked.
  @param[in] Context                    Pointer to the notification function's context.
**/
VOID
EFIAPI
BiosPasswordInitNotify (
  IN  EFI_EVENT                         Event,
  IN  VOID                              *Context
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb;

  SmmFwb = NULL;
  Status = gBS->LocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, (VOID **) &SmmFwb);

  if (EFI_ERROR (Status)) {
    return;
  }

  gBS->CloseEvent (Event);


#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  //
  // System passwords already encode in L05 Specific variable.
  //
  return;

#else
  Status = BiosPasswordInit (SmmFwb);

  return;
#endif
}

/**
  The constructor function

  @param[in]  ImageHandle               The firmware allocated handle for the EFI image.
  @param[in]  SystemTable               A pointer to the EFI System Table.

  @retval     EFI_SUCCESS               The constructor completed successfully.
**/
EFI_STATUS
EFIAPI
OemSvcSecurityPasswordLibConstructor (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_BASE2_PROTOCOL                *SmmBase;
  BOOLEAN                               InSmmMode;
  VOID                                  *Registration;

  mSmst        = NULL;
  InSmmMode    = FALSE;
  Registration = NULL;

  //
  // SMM check
  //
  Status = gBS->LocateProtocol (
                  &gEfiSmmBase2ProtocolGuid,
                  NULL,
                  (VOID **) &SmmBase
                  );

  if (!EFI_ERROR (Status)) {
    Status = SmmBase->InSmm (SmmBase, &InSmmMode);
  }

  if (!InSmmMode) {
    EfiCreateProtocolNotifyEvent (
      &gEfiSmmFwBlockServiceProtocolGuid,
      TPL_NOTIFY,
      BiosPasswordInitNotify,
      NULL,
      &Registration
      );

    //
    // We are not in SMM, so SMST is not needed
    //
    return EFI_SUCCESS;
  }

  //
  // Get Smm Syatem Table
  //
  Status = SmmBase->GetSmstLocation (SmmBase, &mSmst);

  return Status;
}

