/** @file
  Provide OEM to define SLP string which is based on OEM specification.

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseMemoryLib.h>
#include <Library/DxeOemSvcKernelLib.h>
//_Start_L05_SLP_STRING_
#include "L05Config.h"
//_End_L05_SLP_STRING_

//
// Maximum length of OemSLPString is 16 bytes.
//
#define OEM_SLP_STRING_MAX_LENGTH 16

//
// module variables
//
//_Start_L05_SLP_STRING_
//CHAR8        OemSLPString[OEM_SLP_STRING_MAX_LENGTH] = {'P','L','A','T','F','O','R','M','-','C','R','B','0','0','0','0'};
CHAR8        OemSLPString[]       = L05_SLP10_STRING;
CHAR8        OemSLPString2[]      = L05_SLP10_STRING2;
UINTN        SlpDedicatedAddress  = 0xFE810;
UINTN        SlpDedicatedAddress2 = 0xFE800;
//_End_L05_SLP_STRING_

/**
  This OemService provides OEM to define SLP string which based on OEM specification. 
  This service only supports the legacy OS (XP system).

  @param[out]  *SlpStringLength      The length of SLP string.
  @param[out]  *SlpStringAddress     A pointer to the address of SLP string. 

  @retval      EFI_SUCCESS           Set SLP string to specific memory address successfully.
  @retval      Others                Base on OEM design.
**/
EFI_STATUS
OemSvcInstallLegacyBiosOemSlp (
  OUT UINTN                                 *SlpLength,
  OUT UINTN                                 *SlpAddress
  )
{
  *SlpLength = OEM_SLP_STRING_MAX_LENGTH;  
  *SlpAddress = SlpDedicatedAddress;
  CopyMem  ((VOID *)SlpDedicatedAddress, (CHAR8 *)OemSLPString, *SlpLength);
 
//_Start_L05_SLP_STRING_
  CopyMem ((VOID *)SlpDedicatedAddress2, (CHAR8 *)OemSLPString2, *SlpLength);
  return EFI_MEDIA_CHANGED;
//  return EFI_SUCCESS;
//_End_L05_SLP_STRING_
}
