/** @file
  Calculate Hardware signature

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _UPDATE_HARDWARE_SIGNATURE_H_
#define _UPDATE_HARDWARE_SIGNATURE_H_

EFI_STATUS
EFIAPI
UpdateHardwareSignature (
  IN OUT  UINT32                        *HardwareSignature
  );

#endif
