/** @file
  Calculate Hardware signature

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "SetupConfig.h"
#include <L05ChipsetNameList.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
#include <Dxe/AmdPbsSetupDxe/AmdPbsConfig.h>  // AMD_PBS_SETUP_OPTION
#endif
#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
#include <Guid/L05EspCustomizePostLogoInfoVariable.h>
#include <Guid/L05EspCustomizePostLogoVcmVariable.h>
#endif

/**
  To determine Hardware Signature by feature.

  @param[in,out]  HardwareSignature     Value of Hardware Signature.

  @retval      EFI_SUCCESS              Hardware Signature was updated by this function. Then
                                        Feature will update the FACS with the returned Hardware
                                        Signature.
  @retval      Others                   The hardware signature was not updated by this function.
**/
EFI_STATUS
EFIAPI
UpdateHardwareSignature (
  IN OUT  UINT32                        *HardwareSignature
  )
{
  EFI_STATUS                            Status;
  UINT32                                L05Crc32;
  UINTN                                 VariableDataSize;
  UINT8                                 *CrcDataBuffer;
  UINTN                                 CrcDataBufferSize;
  UINTN                                 OldCrcDataBufferSize;
  EFI_GUID                              SystemConfigurationGuid = SYSTEM_CONFIGURATION_GUID;
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_KABYLAKE   || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  EFI_GUID                              PchSetupVariableGuid = PCH_SETUP_GUID;
  EFI_GUID                              CpuSetupVariableGuid = CPU_SETUP_GUID;
  EFI_GUID                              SaSetupVariableGuid  = SA_SETUP_GUID;
#endif
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
  EFI_GUID                              AmdPbsSetupVariableGuid = AMD_PBS_SETUP_GUID;
#endif

  L05Crc32             = 0;
  CrcDataBuffer        = NULL;
  CrcDataBufferSize    = 0;
  OldCrcDataBufferSize = 0;

  //
  // Get Setup Variable
  //
  VariableDataSize = sizeof (SYSTEM_CONFIGURATION);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = AllocateZeroPool (CrcDataBufferSize);

  Status = gRT->GetVariable (
                  L"Setup",
                  &SystemConfigurationGuid,
                  NULL,
                  &VariableDataSize,
                  CrcDataBuffer
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }


#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_KABYLAKE   || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  //
  // Get PchSetup Variable
  //
  OldCrcDataBufferSize = CrcDataBufferSize;
  VariableDataSize = sizeof (PCH_SETUP);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = ReallocatePool (OldCrcDataBufferSize, CrcDataBufferSize, CrcDataBuffer);

  Status = gRT->GetVariable (
                  PCH_SETUP_VARIABLE_NAME,
                  &PchSetupVariableGuid,
                  NULL,
                  &VariableDataSize,
                  (CrcDataBuffer + OldCrcDataBufferSize)
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get CpuSetup Variable
  //
  OldCrcDataBufferSize = CrcDataBufferSize;
  VariableDataSize = sizeof (CPU_SETUP);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = ReallocatePool (OldCrcDataBufferSize, CrcDataBufferSize, CrcDataBuffer);


  Status = gRT->GetVariable (
                  CPU_SETUP_VARIABLE_NAME,
                  &CpuSetupVariableGuid,
                  NULL,
                  &VariableDataSize,
                  (CrcDataBuffer + OldCrcDataBufferSize)
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get SaSetup Variable
  //
  OldCrcDataBufferSize = CrcDataBufferSize;
  VariableDataSize = sizeof (SA_SETUP);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = ReallocatePool (OldCrcDataBufferSize, CrcDataBufferSize, CrcDataBuffer);

  Status = gRT->GetVariable (
                  SA_SETUP_VARIABLE_NAME,
                  &SaSetupVariableGuid,
                  NULL,
                  &VariableDataSize,
                  (CrcDataBuffer + OldCrcDataBufferSize)
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
  //
  // Get AMD_PBS_SETUP Variable
  //
  OldCrcDataBufferSize = CrcDataBufferSize;
  VariableDataSize = sizeof (AMD_PBS_SETUP_OPTION);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = ReallocatePool (OldCrcDataBufferSize, CrcDataBufferSize, CrcDataBuffer);

  Status = gRT->GetVariable (
                  AMD_PBS_SETUP_VARIABLE_NAME,
                  &AmdPbsSetupVariableGuid,
                  NULL,
                  &VariableDataSize,
                  (CrcDataBuffer + OldCrcDataBufferSize)
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
  //
  // Get L05_BIOS_LOGO_DIY_ESP_VARIABLE_NAME Variable
  //
  OldCrcDataBufferSize = CrcDataBufferSize;
  VariableDataSize = sizeof (LENOVO_ESP_CUSTOMIZE_POST_LOGO_INFO);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = ReallocatePool (OldCrcDataBufferSize, CrcDataBufferSize, CrcDataBuffer);

  Status = gRT->GetVariable (
                  L05_BIOS_LOGO_DIY_ESP_VARIABLE_NAME,
                  &gEfiL05EspCustomizePostLogoInfoVariableGuid,
                  NULL,
                  &VariableDataSize,
                  (CrcDataBuffer + OldCrcDataBufferSize)
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get L05_BIOS_LOGO_DIY_VERSION_CRC_CONTROL_VARIABLE_NAME Variable
  //
  OldCrcDataBufferSize = CrcDataBufferSize;
  VariableDataSize = sizeof (LENOVO_ESP_CUSTOMIZE_POST_LOGO_VCM);
  CrcDataBufferSize += VariableDataSize;
  CrcDataBuffer = ReallocatePool (OldCrcDataBufferSize, CrcDataBufferSize, CrcDataBuffer);

  Status = gRT->GetVariable (
                  L05_BIOS_LOGO_DIY_VERSION_CRC_CONTROL_VARIABLE_NAME,
                  &gEfiL05EspCustomizePostLogoVcmVariableGuid,
                  NULL,
                  &VariableDataSize,
                  (CrcDataBuffer + OldCrcDataBufferSize)
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  //
  //  Calculate the 32-bit CRC of all Setup Variable and treat as Hardware Signature
  //
  Status = gBS->CalculateCrc32 (CrcDataBuffer, CrcDataBufferSize, &L05Crc32);

  *HardwareSignature += L05Crc32;

  FreePool (CrcDataBuffer);

  return Status;
}

