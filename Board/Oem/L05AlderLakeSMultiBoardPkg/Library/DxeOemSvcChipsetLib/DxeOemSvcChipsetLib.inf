## @file
#  Component description file for DXE OEM Services Chipset Lib instance.
#
#******************************************************************************
#* Copyright (c) 2014 - 2019, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = DxeOemSvcChipsetLib
  FILE_GUID                      = 2113FC40-040C-4351-8CE7-E36BBAEEC228
  MODULE_TYPE                    = UEFI_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = DxeOemSvcChipsetLib|DXE_CORE DXE_DRIVER DXE_RUNTIME_DRIVER DXE_SAL_DRIVER DXE_SMM_DRIVER UEFI_APPLICATION UEFI_DRIVER SMM_CORE

[Sources]
#_Start_L05_SETUP_MENU_
#  OemSvcHookPlatformDxe.c
#  OemSvcHookPlatformReset.c
  OemSvcHookRouteConfig.c
#  OemSvcInitTbtFunc.c
#  OemSvcSetBacklightControl.c
#  OemSvcSetBootDisplayDevice.c
#  OemSvcSetIgdOpRegion.c
#  OemSvcSetUsbLegacyPlatformOptions.c
#  OemSvcUpdateAmtPlatformPolicy.c
  OemSvcUpdateBiosProtectTable.c
#  OemSvcUpdateDsdtAcpiTable.c
#  OemSvcUpdateDxeMePolicy.c
#  OemSvcUpdateDxePlatformSaPolicy.c
  OemSvcUpdateOemBadgingLogoData.c
#  OemSvcUpdatePlatformNvs.c
#_End_L05_SETUP_MENU_
#_Start_L05_LOGO_
   L05Hook/L05LogoEnable.c
   L05Hook/L05LogoEnable.h
#_End_L05_LOGO_

[Packages]
#_Start_L05_SETUP_MENU_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  MinPlatformPkg/MinPlatformPkg.dec
#_End_L05_SETUP_MENU_
  MdePkg/MdePkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(PROJECT_PKG)/Project.dec
  InsydeModulePkg/InsydeModulePkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec
  #ClientSiliconPkg/ClientSiliconPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
#_Start_L05_LEGACY_TO_UEFI_
  MdeModulePkg/MdeModulePkg.dec
#_End_L05_LEGACY_TO_UEFI_

[Protocols]
  gEfiLegacyBiosProtocolGuid
#_Start_L05_LOGO_
  gEfiEdidDiscoveredProtocolGuid
#_End_L05_LOGO_
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
gL05EndOfBdsConnectProtocolGuid
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  gEfiL05BiosSelfHealingProtocolGuid
#_End_L05_BIOS_SELF_HEALING_SUPPORT_

#_Start_L05_FEATURE_
[Guids]
#_Start_L05_LOGO_
  gH2OBdsCpReadyToBootBeforeGuid
#_End_L05_LOGO_
#_Start_L05_FLIP_TO_BOOT_
  gEfiL05FliptoBootVariableGuid
#_End_L05_FLIP_TO_BOOT_
#_End_L05_FEATURE_

[LibraryClasses]
  UefiBootServicesTableLib
  H2ODebugTraceErrorLevelLib
#_Start_L05_SETUP_MENU_
  DxeOemSvcFeatureLibDefault
  UefiRuntimeServicesTableLib
#_End_L05_SETUP_MENU_
#_Start_L05_LOGO_
  OemGraphicsLib
  DebugLib
  BaseMemoryLib
  H2OCpLib
#_End_L05_LOGO_
#_Start_L05_FEATURE_
  MemoryAllocationLib
#_End_L05_FEATURE_

#_Start_L05_FEATURE_
[Pcd]
#_Start_L05_LOGO_
  gL05ServicesTokenSpaceGuid.PcdL05CustomerBgrtLogoEnable
  gL05ServicesTokenSpaceGuid.PcdL05CustomerBgrtLogoGuid
  gL05ServicesTokenSpaceGuid.PcdL05CustomerBgrtLogoFormat
#_End_L05_LOGO_
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
  gL05ServicesTokenSpaceGuid.PcdL05CustomizeLogoFromEspFlag
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordSupported
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordUefiOsFastBootSupport
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_
  gInsydeTokenSpaceGuid.PcdFlashNvStorageFactoryCopyBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageFactoryCopySize
  gInsydeTokenSpaceGuid.PcdFlashNvStorageVariableDefaultsBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageVariableDefaultsSize
  gInsydeTokenSpaceGuid.PcdFlashFvMainBase
  gInsydeTokenSpaceGuid.PcdFlashFvMainSize
  gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesFvBase
  gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesFvSize
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSBase
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSSize
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMicrocodeBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMicrocodeSize
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery2Base
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery2Size
  gInsydeTokenSpaceGuid.PcdFlashFvRecoveryBase
  gInsydeTokenSpaceGuid.PcdFlashFvRecoverySize
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMSize
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTSize
  gInsydeTokenSpaceGuid.PcdH2OFlashDeviceMapStart
  gInsydeTokenSpaceGuid.PcdH2OFlashDeviceMapSize
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery0Size
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  gChipsetPkgTokenSpaceGuid.PcdFwResiliencyReservedBase
  gChipsetPkgTokenSpaceGuid.PcdFwResiliencyReservedSize
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
#_End_L05_FEATURE_
  
[FixedPcd]
  gPlatformModuleTokenSpaceGuid.PcdUpServerEnable
#_Start_L05_SETUP_MENU_
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName
#_End_L05_SETUP_MENU_
