/** @file
  This function provides an interface to hook GenericRouteConfig.
   
;******************************************************************************
;* Copyright (c) 2015-2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcChipsetLib.h>
#include <Guid/RcSetupUtility.h>
#include <SetupVariable.h>
#include <MeSetup.h>
//_Start_L05_SETUP_MENU_
#include <SetupConfig.h>  // SYSTEM_CONFIGURATION
#include <L05ChipsetNameList.h>
#include <Library/FeatureLib/OemSvcOverrideSetupSettingInSetupMenu.h>
#include <Library/FeatureLib/OemSvcSecurityPassword.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO     || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
#include <Dxe/AmdPbsSetupDxe/AmdPbsConfig.h>
#endif
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_WAKE_ON_LAN_
#include <KernelSetupConfig.h>
#include <Library/FeatureLib/OemSvcEcKeepWolPowerForAcOnly.h>
#include <Library/FeatureLib/OemSvcNotifyEcEnableWolFromDock.h>
//_End_L05_WAKE_ON_LAN_
#endif
//_End_L05_SETUP_MENU_
//_Start_L05_BIOS_SELF_HEALING_SUPPORT_
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/L05BiosSelfHealing.h>
//_End_L05_BIOS_SELF_HEALING_SUPPORT_
//_Start_L05_FLIP_TO_BOOT_
#include <Guid/L05SwitchFlipToBoot.h>
//_End_L05_FLIP_TO_BOOT_

/**
  This function provides an interface to hook GenericRouteConfig.

  @param[in,out]     ScBuffer            A pointer to CHIPSET_CONFIGURATION struct.
  @param[in]         BufferSize          System configuration size.
  @param[in,out]     RcScBuffer          A pointer to RC relative variables struct.
  @param[in]         ScuRecord           The bit mask of the currently SCU record.
                                           Bit 0 = 1 (SCU_ACTION_LOAD_DEFAULT), It indicates system do load default action.

  @retval            EFI_UNSUPPORTED     This function is a pure hook; Chipset code don't care return status.
  @retval            EFI_SUCCESS         This function is a pure hook; Chipset code don't care return status. 
**/
EFI_STATUS
OemSvcHookRouteConfig (
  IN OUT CHIPSET_CONFIGURATION          *ScBuffer,
  IN     UINT32                         BufferSize,
  IN OUT VOID                           *RcScBuffer,
  IN     UINT32                         ScuRecord
  )
{
  RC_SETUP_UTILITY_BROWSER_DATA         *RcSUBData;
  SA_SETUP                              *SaScBuffer;
  UINTN                                 SaScBufferSize;
  ME_SETUP                              *MeScBuffer;
  UINTN                                 MeScBufferSize;
  CPU_SETUP                             *CpuScBuffer;
  UINTN                                 CpuScBufferSize;
  PCH_SETUP                             *PchScBuffer;
  UINTN                                 PchScBufferSize;
  SI_SETUP                             *SiScBuffer;
  UINTN                                 SiScBufferSize;
  ME_SETUP_STORAGE                      *MeStorageScBuffer;
  UINTN                                 MeStorageScBufferSize;
//_Start_L05_SETUP_MENU_
  SYSTEM_CONFIGURATION                  *SetupVariable;
  EFI_STATUS                            Status;
//_End_L05_SETUP_MENU_

  RcSUBData         = (RC_SETUP_UTILITY_BROWSER_DATA *)RcScBuffer;
  SaScBuffer        = (SA_SETUP *)RcSUBData->SaSUBrowserData;
  MeScBuffer        = (ME_SETUP *)RcSUBData->MeSUBrowserData;
  CpuScBuffer       = (CPU_SETUP *)RcSUBData->CpuSUBrowserData;
  PchScBuffer       = (PCH_SETUP *)RcSUBData->PchSUBrowserData;
  SiScBuffer        = (SI_SETUP *)RcSUBData->SiSUBrowserData;
  MeStorageScBuffer = (ME_SETUP_STORAGE *)RcSUBData->MeStorageSUBrowserData;
//_Start_L05_SETUP_MENU_
  SetupVariable     = (SYSTEM_CONFIGURATION *) ScBuffer;
//_End_L05_SETUP_MENU_

  SaScBufferSize        = RcSUBData->SaSUBDataSize;
  MeScBufferSize        = RcSUBData->MeSUBDataSize;
  CpuScBufferSize       = RcSUBData->CpuSUBDataSize;
  PchScBufferSize       = RcSUBData->PchSUBDataSize;
  SiScBufferSize       = RcSUBData->SiSUBDataSize;
  MeStorageScBufferSize = RcSUBData->MeStorageSUBDataSize;

  /*++
    Todo:
      Add project specific code in here.
  --*/

//_Start_L05_SETUP_MENU_
  SetupVariable = (SYSTEM_CONFIGURATION *) ScBuffer;

  //
  // When L05WlanSyncToChipsetWlan is Enable (1),
  // system will synchronize the L05 WLAN switch (SYSTEM_CONFIGURATION.L05WirelessLan) with Chipset WLAN.
  // Intel: CNVi switch (PCH_SETUP.CnviMode).
  // AMD:   WLAN/WIFI Power Enable (AMD_PBS_SETUP_OPTION.WlanPowerControl)
  //
  if (SetupVariable->L05WlanSyncToChipsetWlan == 1 && SetupVariable->L05OdmWirelessLanImplement == 1) {
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE)
    //
    // "CNVi Mode" item,    0:Disable Integrated, 1:Auto Detection
    // "Wireless LAN" item, 0:Disable,            1:Enable
    //
    PchScBuffer->PchCnviMode = SetupVariable->L05WirelessLan;
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE   || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
    //
    // "CNVi Mode" item,    0:Disable Integrated, 1:Auto Detection
    // "Wireless LAN" item, 0:Disable,            1:Enable
    //
    PchScBuffer->CnviMode = SetupVariable->L05WirelessLan;
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO     || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
{
    AMD_PBS_SETUP_OPTION                AmdPbsSetupConfig;
    UINTN                               AmdPbsSetupBufferSize;

    ZeroMem (&AmdPbsSetupConfig, sizeof (AMD_PBS_SETUP_OPTION));

    AmdPbsSetupBufferSize = sizeof (AMD_PBS_SETUP_OPTION);
    Status = gRT->GetVariable (
              AMD_PBS_SETUP_VARIABLE_NAME,
              &gAmdPbsSystemConfigurationGuid,
              NULL,
              &AmdPbsSetupBufferSize,
              &AmdPbsSetupConfig
              );

    if (!EFI_ERROR (Status)) {
      if (AmdPbsSetupConfig.WlanPowerControl != SetupVariable->L05WirelessLan) {
        //
        // "WLAN/WIFI Power Enable" item, 0:Disable, 1:Enable
        // "Wireless LAN" item,           0:Disable, 1:Enable
        //
        AmdPbsSetupConfig.WlanPowerControl = SetupVariable->L05WirelessLan;
  
        Status = gRT->SetVariable(
                        AMD_PBS_SETUP_VARIABLE_NAME,
                        &gAmdPbsSystemConfigurationGuid,
                        EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                        sizeof (AMD_PBS_SETUP_OPTION),
                        &AmdPbsSetupConfig
                        );
      }
    }
}
#endif
  }

#ifdef L05_NATURAL_FILE_GUARD_ENABLE
  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.3 Encrypted UHDP store to SPI
  //         When to clear: MHDP disabled
  //
  if (SetupVariable->L05NaturalFileGuard == 0) {  // 0:Disable, 1:Enable
    ClearStoredUhdp ();
  }
#endif

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_WAKE_ON_LAN_
{
  BOOLEAN                               WakeOnLanEnable;

  //
  // "Wake On LAN" item, 0:Disable, 1:AC only, 2:AC and Battery
  //
  WakeOnLanEnable = (SetupVariable->L05WakeOnLan != 0) ? TRUE : FALSE;

  //
  // [Lenovo SMB BIOS Special Requirements V1.1]
  //   2.7 Wake On LAN (WOL)
  //     Note: Wake On LAN function does not work when hard disk password is set.
  //
  if (SetupVariable->L05SetHddPasswordFlag == 1) {
    WakeOnLanEnable = FALSE;
  }

  switch (SetupVariable->L05WakeOnLan) {  // 0:Disable, 1:AC only, 2:AC and Battery

  case 1:
    // Wake on LAN for AC only
    OemSvcEcKeepWolPowerForAcOnly (TRUE);
    break;

  case 2:
    // Wake on LAN for AC and Battery
    OemSvcEcKeepWolPowerForAcOnly (FALSE);
    break;

  default:
    OemSvcEcKeepWolPowerForAcOnly (FALSE);
  }

  OemSvcNotifyEcEnableWolFromDock ((WakeOnLanEnable && (SetupVariable->L05WakeOnLanFromDock == 1)) ? TRUE : FALSE);  // 0:Disable, 1:Enable

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  PchScBuffer->PchWakeOnLan = WakeOnLanEnable ? 1 : 0;  // 0:Disable, 1:Enable
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO     || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
{
  AMD_PBS_SETUP_OPTION                  AmdPbsSetupConfig;
  UINTN                                 AmdPbsSetupBufferSize;

  ZeroMem (&AmdPbsSetupConfig, sizeof (AMD_PBS_SETUP_OPTION));

  AmdPbsSetupBufferSize = sizeof (AMD_PBS_SETUP_OPTION);
  Status = gRT->GetVariable (
                  AMD_PBS_SETUP_VARIABLE_NAME,
                  &gAmdPbsSystemConfigurationGuid,
                  NULL,
                  &AmdPbsSetupBufferSize,
                  &AmdPbsSetupConfig
                  );

  if (!EFI_ERROR (Status)) {
    AmdPbsSetupConfig.WakeOnPME = WakeOnLanEnable ? 1 : 0;  // 0:Disable, 1:Enable

    Status = gRT->SetVariable(
                    AMD_PBS_SETUP_VARIABLE_NAME,
                    &gAmdPbsSystemConfigurationGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                    sizeof (AMD_PBS_SETUP_OPTION),
                    &AmdPbsSetupConfig
                    );
  }
}
#endif
}
//_End_L05_WAKE_ON_LAN_
#endif

  //
  // Override setting of variable L"Setup" before store into variable L"Setup" if need
  //
  Status = OemSvcOverrideSetupSettingInSetupMenu ((UINT8 *) (VOID *) SetupVariable);

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
{
  EFI_L05_BIOS_SELF_HEALING_PROTOCOL    *L05BiosSelfHealingPtr;

  Status = gBS->LocateProtocol (
                  &gEfiL05BiosSelfHealingProtocolGuid,
                  NULL,
                  &L05BiosSelfHealingPtr
                  );

  if (!EFI_ERROR (Status)) {
    Status = L05BiosSelfHealingPtr->FunctionSwitch (SetupVariable->L05BiosSelfHealing);
  }
}
#endif

//_Start_L05_FLIP_TO_BOOT_
{
  LENOVO_FLIP_TO_BOOT_SW_INTERFACE      L05FlipToBootVar;
  UINTN                                 L05FlipToBootVarSize;

  L05FlipToBootVarSize = sizeof (LENOVO_FLIP_TO_BOOT_SW_INTERFACE);
  Status = gRT->GetVariable (
                  L05_EFI_FLIP_TO_BOOT_VARIABLE_NAME,
                  &gEfiL05FliptoBootVariableGuid,
                  NULL,
                  &L05FlipToBootVarSize,
                  &L05FlipToBootVar
                  );
  
  if (!EFI_ERROR (Status) && (L05FlipToBootVar.FlipToBootEn != SetupVariable->L05FlipToBoot)){

    L05FlipToBootVar.FlipToBootEn = SetupVariable->L05FlipToBoot;
    Status = gRT->SetVariable (
                    L05_EFI_FLIP_TO_BOOT_VARIABLE_NAME,
                    &gEfiL05FliptoBootVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    sizeof (LENOVO_FLIP_TO_BOOT_SW_INTERFACE),
                    &L05FlipToBootVar
                    );
  }
}
//_End_L05_FLIP_TO_BOOT_

//  return EFI_UNSUPPORTED;
  return EFI_SUCCESS;
//_End_L05_SETUP_MENU_
}
