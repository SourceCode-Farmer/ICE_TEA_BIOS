/** @file

;******************************************************************************
;* Copyright (c) 2015 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>

/**
 This function offers an interface to update SI_PREMEM_POLICY_PPI data before the system
 installs gSiPreMemPolicyReadyPpiGuid.

 @param[in, out]   *SiPreMemPolicyPpi    On entry, points to SI_PREMEM_POLICY_PPI structure.
                                         On exit, points to updated SI_PREMEM_POLICY_PPI structure.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdatePeiPolicyInitPreMem (
  IN OUT SI_PREMEM_POLICY_PPI         *SiPreMemPolicyPpi
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  return EFI_UNSUPPORTED;
}
