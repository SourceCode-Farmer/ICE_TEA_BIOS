/** @file

;******************************************************************************
;* Copyright (c) 2015 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>
//_Start_L05_FEATURE_
#include <Library/PeiServicesLib.h>
//_End_L05_FEATURE_

#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
#include <Guid/L05EspCustomizePostLogoInfoVariable.h>  // LENOVO_ESP_CUSTOMIZE_POST_LOGO_INFO
#include <Library/VariableLib.h>
#endif

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
#include <L05Config.h>
#include <L05BiosSelfHealingConfig.h>
#include <Library/BiosSelfHealingLib.h>  // IsManufacturingMode
#endif

#ifdef L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE
#include <Library/PeiGetFvInfoLib.h>  // PeiGetSectionFromFv
#include <Library/MemoryAllocationLib.h>
#endif

#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
/**
  BIOS POST Logo DIY update SI policy.

  @param  *SiPolicyPpi                  On entry, points to SI_POLICY_PPI structure.
                                        On exit, points to updated SI_POLICY_PPI structure.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Update SI policy completed.
*/
EFI_STATUS
BiosPostLogoDiyUpdateSiPolicy (
  IN OUT SI_POLICY_PPI                  *SiPolicyPpi
  )
{
  EFI_STATUS                            Status;
  GRAPHICS_PEI_CONFIG                   *GtConfig;
  UINTN                                 BufferSize;
  LENOVO_ESP_CUSTOMIZE_POST_LOGO_INFO   *L05EspCustomizePostLogoInfo;

  GtConfig                     = NULL;
  BufferSize                   = 0;
  L05EspCustomizePostLogoInfo  = NULL;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGraphicsPeiConfigGuid, (VOID *) &GtConfig);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return EFI_UNSUPPORTED;
  }

  CommonGetVariableDataAndSize (
    L05_BIOS_LOGO_DIY_ESP_VARIABLE_NAME,
    &gEfiL05EspCustomizePostLogoInfoVariableGuid,
    &BufferSize,
    (VOID **) &L05EspCustomizePostLogoInfo
    );

  if (L05EspCustomizePostLogoInfo == NULL) {
    return EFI_UNSUPPORTED;
  }

  if (L05EspCustomizePostLogoInfo->LogoDiySupport) {
    GtConfig->PeiGraphicsPeimInit = 0;  // 0:DISABLED 1:ENABLED
    return EFI_MEDIA_CHANGED;
  }

  return EFI_UNSUPPORTED;
}
#endif

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
/**
  BIOS Self-healing update SI policy.

  @param  *SiPolicyPpi                  On entry, points to SI_POLICY_PPI structure.
                                        On exit, points to updated SI_POLICY_PPI structure.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Update SI policy completed.
*/
EFI_STATUS
BiosSelfHealingUpdateSiPolicy (
  IN OUT SI_POLICY_PPI                  *SiPolicyPpi
  )
{
  EFI_STATUS                            Status;
  PCH_LOCK_DOWN_CONFIG                  *LockDownConfig;
  RTC_CONFIG                            *RtcConfig;
  EFI_BOOT_MODE                         BootMode;
  EFI_L05_EEPROM_MAP_120                *EepromBuffer;
  UINT8                                 BiosSelfHealingFlag;
  BOOLEAN                               DisableLockDown;

  Status              = EFI_SUCCESS;
  LockDownConfig      = NULL;
  EepromBuffer        = NULL;
  BiosSelfHealingFlag = 0;
  DisableLockDown     = FALSE;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gLockDownConfigGuid, (VOID *) &LockDownConfig);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return EFI_UNSUPPORTED;
  }

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gRtcConfigGuid, (VOID *) &RtcConfig);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return EFI_UNSUPPORTED;
  }

  Status = PeiServicesGetBootMode (&BootMode);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Fail to get the current boot mode.\n", __FUNCTION__));
    return EFI_UNSUPPORTED;
  }

  if (BootMode == BOOT_IN_RECOVERY_MODE) {
    DisableLockDown = TRUE;
  }

  EepromBuffer = (EFI_L05_EEPROM_MAP_120 *)(UINTN) PcdGet32 (PcdFlashFvEepromBase);
  if (EepromBuffer == NULL) {
    ASSERT (EepromBuffer != NULL);
    return EFI_UNSUPPORTED;
  }

  BiosSelfHealingFlag = EepromBuffer->BiosSelfHealingFlag[0];

  if (IsManufacturingMode () &&
      ((BiosSelfHealingFlag & L05_BIOS_SELF_HEALING_FLAG_DESTROY_BOOT_BLOCK) == L05_BIOS_SELF_HEALING_FLAG_DESTROY_BOOT_BLOCK ||
      (BiosSelfHealingFlag & L05_BIOS_SELF_HEALING_FLAG_DESTROY_FVMAIN) == L05_BIOS_SELF_HEALING_FLAG_DESTROY_FVMAIN)) {
    DisableLockDown = TRUE;
  }

//_Start_L05_ALDERLAKE_PLATFORM_
  if (CheckPbbrSyncFlag ()) {
    DisableLockDown = TRUE;
  }
//_End_L05_ALDERLAKE_PLATFORM_

  if (DisableLockDown) {
    //
    // Triggered only in some special conditions
    //
    DEBUG ((DEBUG_INFO, "Disable InSMM.STS (EISS) and BIOS Lock Enable (BLE) since BIOS Self-Healing related flags are set.\n"));
    LockDownConfig->BiosLock      = FALSE;
    LockDownConfig->BiosInterface = FALSE;
    RtcConfig->BiosInterfaceLock  = FALSE;
    return EFI_MEDIA_CHANGED;
  }

  return EFI_UNSUPPORTED;
}
#endif

#ifdef L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE
/**
  Crisis Recovery update SI policy.

  @param  *SiPolicyPpi                  On entry, points to SI_POLICY_PPI structure.
                                        On exit, points to updated SI_POLICY_PPI structure.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Update SI policy completed.
*/
EFI_STATUS
CrisisRecoveryUpdateSiPolicy (
  IN OUT SI_POLICY_PPI                  *SiPolicyPpi
  )
{
  EFI_STATUS                            Status;
  EFI_BOOT_MODE                         BootMode;
  GRAPHICS_PEI_CONFIG                   *GtConfig;
  EFI_GUID                              FileGuid;
  VOID                                  *Buffer;
  UINT32                                Size;
  EFI_PEI_PPI_DESCRIPTOR                *ReadyForGopConfigPpiDesc;

  Status   = EFI_SUCCESS;
  GtConfig = NULL;
  Buffer   = NULL;
  Size     = 0;

  Status = PeiServicesGetBootMode (&BootMode);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Fail to get the current boot mode\n", __FUNCTION__));
    return EFI_UNSUPPORTED;
  }

  if (BootMode != BOOT_IN_RECOVERY_MODE) {
    return EFI_UNSUPPORTED;
  }

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGraphicsPeiConfigGuid, (VOID *) &GtConfig);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return EFI_UNSUPPORTED;
  }

  //
  // Force enable PeiGraphicsPeimInit
  //
  GtConfig->PeiGraphicsPeimInit = 1;  // 0:DISABLED 1:ENABLED

  //
  // Init GraphicsConfigPtr
  //
  FileGuid = gVbtRvpFileGuid;
  PeiGetSectionFromFv (FileGuid, &Buffer, &Size);
  if (Buffer == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: Could not locate VBT\n", __FUNCTION__));
    return EFI_UNSUPPORTED;
  }

  GtConfig->GraphicsConfigPtr = Buffer;

  DEBUG ((DEBUG_INFO, "%a: Vbt Pointer from PeiGetSectionFromFv is 0x%x\n", __FUNCTION__, GtConfig->GraphicsConfigPtr));
  DEBUG ((DEBUG_INFO, "%a: Vbt Size from PeiGetSectionFromFv is 0x%x\n", __FUNCTION__, Size));

  //
  // Install ReadyForGopConfig PPI to trigger PEI phase GopConfig callback
  //
  ReadyForGopConfigPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
  if (ReadyForGopConfigPpiDesc == NULL) {
    ASSERT (FALSE);
    return EFI_UNSUPPORTED;
  }

  ReadyForGopConfigPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  ReadyForGopConfigPpiDesc->Guid  = &gReadyForGopConfigPpiGuid;
  ReadyForGopConfigPpiDesc->Ppi   = GtConfig->GraphicsConfigPtr;
  Status = PeiServicesInstallPpi (ReadyForGopConfigPpiDesc);

  return EFI_MEDIA_CHANGED;
}
#endif

/**
 This function offers an interface to update SI_POLICY_PPI data before the system
 installs gSiPolicyReadyPpiGuid.

 @param[in, out]   *SiPolicyPpi          On entry, points to SI_POLICY_PPI structure.
                                         On exit, points to updated SI_POLICY_PPI structure.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdatePeiPolicyInitPostMem (
  IN OUT SI_POLICY_PPI                   *SiPolicyPpi
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

//_Start_L05_FEATURE_
  EFI_STATUS                            Status;
  EFI_STATUS                            TempStatus;

  Status     = EFI_UNSUPPORTED;
  TempStatus = EFI_UNSUPPORTED;
//_End_L05_FEATURE_

#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
  TempStatus = BiosPostLogoDiyUpdateSiPolicy (SiPolicyPpi);

  if (Status != EFI_MEDIA_CHANGED) {
    Status = TempStatus;
  }
#endif

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
  TempStatus = BiosSelfHealingUpdateSiPolicy (SiPolicyPpi);

  if (Status != EFI_MEDIA_CHANGED) {
    Status = TempStatus;
  }
#endif

#ifdef L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE
  TempStatus = CrisisRecoveryUpdateSiPolicy (SiPolicyPpi);

  if (Status != EFI_MEDIA_CHANGED) {
    Status = TempStatus;
  }
#endif

//_Start_L05_FEATURE_
//  return EFI_UNSUPPORTED;
  return Status;
//_End_L05_FEATURE_
}
