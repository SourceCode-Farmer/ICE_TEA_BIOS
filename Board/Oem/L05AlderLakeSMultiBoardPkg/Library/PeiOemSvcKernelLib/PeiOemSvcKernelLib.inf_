## @file
#  Component description file for PeiOemSvcKernelLib instance.
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiOemSvcKernelLib
  FILE_GUID                      = BFCAACD2-EAC7-479b-800D-850E4D185893
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = PeiOemSvcKernelLib|PEI_CORE PEIM

[Sources]
  OemSvcChipsetModifyClockGenInfo.c
  OemSvcDetectRecoveryRequest.c
  OemSvcGetVerbTable.c
  OemSvcInitPlatformStage2.c
  OemSvcIsBootWithNoChange.c
  OemSvcGetWaitTimerAfterHdaInit.c
  OemSvcPeiCrisisRecoveryReset.c
  OemSvcGetProtectTable.c
  OemSvcGetBoardId.c

[Packages]
  MdePkg/MdePkg.dec
  PerformancePkg/PerformancePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  MdeModulePkg/MdeModulePkg.dec
  $(PROJECT_PKG)/Project.dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec

[LibraryClasses]
  PcdLib
  CmosLib
  PeiServicesLib
  IoLib
  DebugLib
  HobLib
  PeiServicesTablePointerLib
#  PchPlatformLib
   PchCycleDecodingLib
  GpioLib
  PchCycleDecodingLib
  PeiInsydeChipsetLib
  PchPcrLib
  EcMiscLib
  EcLib
  EcMiscLib
  PmcLib

[Guids]
  gEfiGenericVariableGuid
  gChasmfallsCrisisRecoveryGuid

[Ppis]
  gEfiPeiReadOnlyVariable2PpiGuid

[Pcd]
  gChipsetPkgTokenSpaceGuid.PcdFlashFvBackupBase        ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdFlashFvBackupSize        ## CONSUMES

  gPerformancePkgTokenSpaceGuid.PcdPerfPkgAcpiIoPortBaseAddress
  gChipsetPkgTokenSpaceGuid.PcdPchGpioBaseAddress
  gChipsetPkgTokenSpaceGuid.PcdSetupConfigSize
  gInsydeTokenSpaceGuid.PcdH2OBoardId
  gChipsetPkgTokenSpaceGuid.PcdCrbBoard
  gBoardModuleTokenSpaceGuid.PcdBoardId
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport

[FeaturePcd]
#[-start-190606-IB16990031-add]#
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag
#[-end-190606-IB16990031-add]#

[FixedPcd]
  gBoardModuleTokenSpaceGuid.PcdSioBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashAreaBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataSize
