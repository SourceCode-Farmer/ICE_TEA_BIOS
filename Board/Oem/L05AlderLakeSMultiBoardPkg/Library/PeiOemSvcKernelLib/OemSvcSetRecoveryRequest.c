/** @file
  This OemService provides OEM to decide the method of recovery request. 
  When DXE loader found that the DXE-core of firmware volume is corrupt, it will force system to restart. 
  This service will be called to set the recovery requests before system restart. 
  To design the recovery requests according to OEM specification.

;******************************************************************************
;* Copyright (c) 2012, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcKernelLib.h>
#ifdef L05_BIOS_SELF_HEALING_SUPPORT
#include <L05ChipsetNameList.h>
#include <OemCmos.h>
#include <Library/CmosLib.h>
#endif

/**
  This OemService provides OEM to decide the method of recovery request. 
  When DXE loader found that the DXE-core of firmware volume is corrupt, it will force system to restart. 
  This service will be called to set the recovery requests before system restart. 
  To design the recovery requests according to OEM specification.

  @param  Based on OEM design.

  @retval EFI_UNSUPPORTED      Returns unsupported by default.
  @retval EFI_SUCCESS          The service is customized in the project.
  @retval EFI_MEDIA_CHANGED    The value of IN OUT parameter is changed. 
  @retval Others               Depends on customization.
**/
EFI_STATUS
OemSvcSetRecoveryRequest (
  VOID
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE)
  if (PcdGetBool (PcdL05BiosSelfHealingEnable)) {
    //
    // [Lenovo BIOS Self-Healing Design Guidance Specification v1.9]
    //   2.2 Detect
    //     When transfer from PEI to DXE phase, BIOS will check if the reset of bootblock FVMAIN data were damaged,
    //     if data untrust, BIOS will not cancel the WDT, then the WDT timeout will trigger BIOS self-healing.
    //
    CpuDeadLoop ();
  }
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CEZANNE)
  if (PcdGetBool (PcdL05BiosSelfHealingEnable)) {
    //
    // [Workaround for AMD]
    //   Use CMOS flag to force enter BSH to replace of EC function
    //
    WriteCmos8 (EfiL05BiosSelfHealingModeSwitch, V_EFI_L05_BIOS_SELF_HEALING_MODE_FORCE_ENTER_BSH);
  }
#endif
#endif

  return EFI_UNSUPPORTED;
}

