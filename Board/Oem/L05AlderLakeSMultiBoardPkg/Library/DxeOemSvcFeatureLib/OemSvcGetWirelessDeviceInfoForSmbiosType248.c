/** @file
  OEM Service to get Wireless Device Information for SMBIOS Type 248

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05WirelessDeviceSupportConfig.h>

/**
  OEM Service to get Wireless Device Information for SMBIOS Type 248

  @param  SmbiosType248Info             Point to the SMBIOS Type 248 information (refer by L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO)

  @retval EFI_MEDIA_CHANGED             Returns media changed, L05 Feature will use setting of SmbiosType248Info to add SMBIOS Type 248
  @retval EFI_UNSUPPORTED               Returns unsupported by default, L05 Feature will not use setting of SmbiosType248Info to add SMBIOS Type 248
  @retval EFI_SUCCESS                   Returns success, L05 Feature will not add SMBIOS Type 248, Project need porting this feature function by itself.
**/
EFI_STATUS
OemSvcGetWirelessDeviceInfoForSmbiosType248 (
  OUT VOID                              *SmbiosType248Info
  )
{
  L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO  *ProjectSetting;

  ProjectSetting = (L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO *) SmbiosType248Info;

  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}
