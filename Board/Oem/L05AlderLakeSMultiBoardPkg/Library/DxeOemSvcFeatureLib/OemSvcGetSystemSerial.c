/** @file
  Provides an opportunity for ODM to get get system serial.

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcDxeComputrace.h>

/**
  Provides an opportunity for get system serial.

  @param  DataLength                    Points to system serial length
  @param  DataBuffer                    Points to systme serail buffer

  @retval EFI_UNSUPPORTED               Returns unsupported.
  @retval EFI_MEDIA_CHANGED             Set system erial length and buffer success.
**/
EFI_STATUS
OemSvcGetSystemSerial (
  IN OUT  UINT8                         *DataLength,
  IN OUT  UINT8                         *DataBuffer
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}
