## @file
#  File to declare variable default information
#
#******************************************************************************
#* Copyright (c) 2018 - 2021, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
# This file is ini format and Comments are prefixed with a # and continue to the end of the line.
#

!include InsydeModulePkg/Package.var
!include AlderLakeChipsetPkg/Package.var
#_Start_L05_FEATURE_
!include InsydeL05PlatformPkg/Package.var
#_End_L05_FEATURE_

#
# Optional [Guids] section. uses to declare guid which used in this file.
#
[Guids]
  gSetupVariableGuid    = {0xEC87D643, 0xEBA4, 0x4BB5, {0xA1, 0xE5, 0x3F, 0x3E, 0x36, 0xB2, 0x0D, 0xA9}}
  gSaSetupVariableGuid  = {0x72c5e28c, 0x7783, 0x43a1, {0x87, 0x67, 0xfa, 0xd7, 0x3f, 0xcc, 0xaf, 0xa4}}
  gCpuSetupVariableGuid = {0xb08f97ff, 0xe6e8, 0x4193, {0xa9, 0x97, 0x5e, 0x9e, 0x9b, 0xa,  0xdb, 0x32}}
  gPchSetupVariableGuid = {0x4570b7f1, 0xade8, 0x4943, {0x8d, 0xc3, 0x40, 0x64, 0x72, 0x84, 0x23, 0x84}}
  gMeSetupVariableGuid  = {0x5432122d, 0xd034, 0x49d2, {0xa6, 0xde, 0x65, 0xa8, 0x29, 0xeb, 0x4c, 0x74}}
  gSiSetupVariableGuid  = {0xAAF8E719, 0x48F8, 0x4099, {0xA6, 0xF7, 0x64, 0x5F, 0xBD, 0x69, 0x4C, 0x3D}}
  gSystemConfigurationGuid = {0xA04A27f4, 0xDF00, 0x4D42, {0xB5, 0x52, 0x39, 0x51, 0x13, 0x02, 0x11, 0x3D}}

#
# The [Packages] section lists all of the package declaration files that contain
# GUIDs or PCDs that are used by this variable declaration file.
#
[Packages]
  AlderLakeChipsetPkg/AlderLakeChipsetPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec

#
# Below can write Var section to introduce a single UEFI variable default value for the specified
# default stores and specified SKU.
#
# Section format is [Var.VariableGUID.VariableName] for example [Var.gEfiFileInfoGuid.MyTestVar]
#
###########################################################################
# The default setting is come from
# 1. Intel\AlderLake\AlderLakeBoardPkg\AlderLake"X"Boards\Include\AlderLake"X"BoardConfigPatchTable.h
#    Note: "X" means "U", "Y" or "H" SKU
# 2. Command "cond" in Intel RC *.hfr file
# 3. Variable which without default value
# The Variable Offset defined in file AdvanceVfr.lst(build folder)
#--------------------------------------------------------------------------
#  0x3F|BoardIdAdlPSimics
#  0x10|BoardIdAdlPLp4Rvp
#  0x12|BoardIdAdlPDdr5Rvp
#  0x13|BoardIdAdlPLp5Rvp
#  0x19|BoardIdAdlPLp4Bep
#  0x1A|BoardIdAdlPLp5Aep
#  0x1E|BoardIdAdlPDdr5Dg02384Aep
#  0x14|SkuIdAdlPDdr4Rvp
#  0x00|BoardDefault

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0
  Struct = SETUP_DATA
  Data = name=Rtd3Support                                | (1)
         name=StorageRtd3Support                         | (2)

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3F,0x10,0x12,0x13,0x19,0x1A,0x1E,0x14
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (3)
         name=AuxOriOverrideSupport                      | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=AcpiDebug                                  | (1)
         name=ITbtRtd3                                   | (0)
         name=DisableTbtPcieTreeBme                      | (0)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3F,0x10,0x12,0x13,0x19,0x1A,0x1E,0x14
  Struct = SA_SETUP
  Data = name=EnableVtd                         | (0)
         name=PcieRootPortL1SubStates[1]        | (0)
         name=PcieRootPortClockReqMsgEnable[1]  | (0)
         name=SkipCdClockInit                   | (1)
         name=D3ColdEnable                      | (0)

[Var.gCpuSetupVariableGuid.CpuSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3F,0x10,0x12,0x13,0x19,0x1A,0x1E,0x14
  Struct = CPU_SETUP
  Data = name=TmeEnable                    | (0)
         name=EnergyEfficientTurbo         | (1)
         name=EnableGv                     | (0)
         name=PkgCStateLimit               | (3)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3F,0x10,0x12,0x13,0x19,0x1A,0x1E,0x14
  Struct = PCH_SETUP
  Data = name=PchSerialIoSpi[1]            | (1)
         name=PchUartHwFlowCtrl[0]         | (0)
         name=PchSerialIoI2c[0]            | (1)
         name=PchSerialIoI2c[1]            | (1)
         name=PchSerialIoI2c[2]            | (0)
         name=PchSerialIoI2c[3]            | (0)
         name=PchSerialIoI2c[4]            | (0)
         name=PchSerialIoI2c[5]            | (1)
         name=PchIshGpEnable[7]            | (1)

#--------------------------------------------------------------------------
#  0x03|SkuIdAdlS
#  0x21|BoardIdAdlSTgpHDdr4SODimm1DCrb
#  0x27|BoardIdAdlSAdpSDdr4UDimm2DCrb
#  0x2B|BoardIdAdlSAdpSDdr5UDimm1DCrb
#  0x2E|BoardIdAdlSAdpSDdr5UDimm2DCrb
#  0x127|SkuIdAdlSAdpSDdr4UDimm2DCrbFab2
#  0x12B|SkuIdAdlSAdpSDdr5UDimm1DCrbFab2
#  0x30|BoardIdAdlSAdpSDdr5SODimmCrb
#  0x35|BoardIdAdlSAdpSDdr4SODimmCrb
#  0x33|BoardIdAdlSAdpSDdr5UDimm1DAep
#  0x36|BoardIdAdlSAdpSDdr5UDimm1DOc

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x03,0x21,0x27,0x2B,0x2E,0x127,0x12B,0x30,0x35,0x36
  Struct = SETUP_DATA
  Data = name=TbtLegacyModeSupport        | (0)
         name=AuxOriOverrideSupport       | (0)
         name=HebcValueSupport            | (1)
         name=PchI2cSensorDevicePort[0]   | (1)
         name=PchI2cSensorDevicePort[1]   | (2)
         name=PchSpiSensorDevicePort[1]   | (1)
         name=NativeAspmEnable            | (0)
         name=PepGfx                      | (1)
         name=PepPeg0                     | (1)
         name=PepSataEnumeration          | (1)
         name=PepI2c0                     | (1)
         name=PepI2c1                     | (1)
         name=PepI2c2                     | (1)
         name=PepI2c3                     | (1)
         name=PepI2c4                     | (1)
         name=PepI2c5                     | (1)
         name=PepSpi                      | (1)
         name=PepXhci                     | (1)
         name=PepAudio                    | (1)
         name=SysFwUpdateSkipPowerCheck   | (1)
         name=TbtSetupFormSupport         | (1)
         name=PepCsme                     | (1)
         name=PepGbe                      | (1)
         name=PepTcss                     | (1)
         name=MipiCam_ControlLogic0       | (0)
         name=MipiCam_ControlLogic1       | (0)
         name=MipiCam_ControlLogic2       | (0)
         name=MipiCam_ControlLogic3       | (0)
         name=MipiCam_ControlLogic4       | (0)
         name=MipiCam_ControlLogic5       | (0)
         name=MipiCam_Link0               | (0)
         name=MipiCam_Link1               | (0)
         name=MipiCam_Link2               | (0)
         name=MipiCam_Link3               | (0)
         name=MipiCam_Link4               | (0)
         name=MipiCam_Link5               | (0)
         name=PchI2cSensorDevicePort[2]   | (4)
         name=TcssUcmDevice               | (2)
         name=DiscreteTbtPlatformConfigurationSupport    | (1)
         name=EcPeciModeUnsupport         | (1)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x03,0x21,0x27,0x2B,0x2E,0x33,0x127,0x12B,0x30,0x35,0x36
  Struct = SA_SETUP
  Data = name=TcssXhciEn                       | (0)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x03,0x21,0x27,0x2B,0x2E,0x33,0x127,0x12B,0x36
  Struct = PCH_SETUP
  Data = name=PxDevSlp[0]                  | (1)
         name=PxDevSlp[1]                  | (1)
         name=PchSerialIoSpi[0]            | (0)
         name=PchSerialIoSpi[1]            | (1)
         name=PchSerialIoSpi[2]            | (0)
         name=PchSerialIoUart[0]           | (0)
         name=PchSerialIoUart[1]           | (0)
         name=PchUartHwFlowCtrl[2]         | (0)
         name=PchSerialIoI2c[2]            | (1)
         name=PchSerialIoI2c[3]            | (1)
         name=PchSerialIoI2c[4]            | (1)
         name=PchSerialIoI2c[5]            | (1)
         name=PchHdAudioI2sCodecSelect     | (0)

[Var.gSystemConfigurationGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x2B
  Struct = CHIPSET_CONFIGURATION
  Data = name=HgSlot                       | (1)

#--------------------------------------------------------------------------
#  0x33|BoardIdAdlSAdpSDdr5UDimm1DAep

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x33
  Struct = SETUP_DATA
  Data = name=TbtLegacyModeSupport         | (0)
         name=AuxOriOverrideSupport        | (0)
         name=HebcValueSupport             | (1)
         name=PchI2cSensorDevicePort[0]    | (1)
         name=PchI2cSensorDevicePort[1]    | (2)
         name=PchSpiSensorDevicePort[1]    | (1)
         name=NativeAspmEnable             | (0)
         name=PepGfx                       | (1)
         name=PepGna                       | (0)
         name=PepPeg0                      | (1)
         name=PepSataEnumeration           | (1)
         name=PepI2c0                      | (1)
         name=PepI2c1                      | (1)
         name=PepI2c2                      | (1)
         name=PepI2c3                      | (1)
         name=PepI2c4                      | (1)
         name=PepI2c5                      | (1)
         name=PepSpi                       | (1)
         name=PepXhci                      | (1)
         name=PepAudio                     | (1)
         name=SysFwUpdateSkipPowerCheck    | (1)
         name=TbtSetupFormSupport          | (1)
         name=PepCsme                      | (1)
         name=PepGbe                       | (1)
         name=PepTcss                      | (1)
         name=MipiCam_ControlLogic0        | (0)
         name=MipiCam_ControlLogic1        | (0)
         name=MipiCam_ControlLogic2        | (0)
         name=MipiCam_ControlLogic3        | (0)
         name=MipiCam_ControlLogic4        | (0)
         name=MipiCam_ControlLogic5        | (0)
         name=MipiCam_Link0                | (0)
         name=MipiCam_Link1                | (0)
         name=MipiCam_Link2                | (0)
         name=MipiCam_Link3                | (0)
         name=MipiCam_Link4                | (0)
         name=MipiCam_Link5                | (0)
         name=AdlSAepBoard                 | (1)
         name=DiscreteTbtPlatformConfigurationSupport                 | (1)

#--------------------------------------------------------------------------
#  0x30|BoardIdAdlSAdpSDdr5SODimmCrb
#  0x35|BoardIdAdlSAdpSDdr4SODimmCrb

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x30,0x35
  Struct = PCH_SETUP
  Data = name=PxDevSlp[0]                  | (1)
         name=PxDevSlp[1]                  | (1)
         name=PchSerialIoSpi[0]            | (0)
         name=PchSerialIoSpi[1]            | (1)
         name=PchSerialIoSpi[2]            | (0)
         name=PchSerialIoUart[0]           | (0)
         name=PchSerialIoUart[1]           | (0)
         name=PchUartHwFlowCtrl[2]         | (0)
         name=PchSerialIoI2c[2]            | (1)
         name=PchSerialIoI2c[3]            | (1)
         name=PchSerialIoI2c[4]            | (1)
         name=PchSerialIoI2c[5]            | (1)
         name=PchHdAudioI2sCodecSelect     | (0)
         name=PchLan                       | (0)
         name=FoxvilleLanSupport           | (1)
         name=FoxvilleWakeOnLan            | (1)

#--------------------------------------------------------------------------
#  0x36|BoardIdAdlSAdpSDdr5UDimm1DOc

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x36
  Struct = SI_SETUP
  Data = name=PlatformDebugConsent          | (2)

[Var.gSystemConfigurationGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0
  Struct = KERNEL_CONFIGURATION
  Data = name=AcpiVer                      | (7)

#--------------------------------------------------------------------------
#  0x3A|BoardIdAdlSAdpSSbgaDdr5SODimmErb
#  0x3B|BoardIdAdlSAdpSSbgaDdr5SODimmCrb
#  0x2F|BoardIdAdlSAdpSSbgaDdr5SODimmAep

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3A,0x3B
  Struct = SETUP_DATA
  Data = name=TbtLegacyModeSupport        | (0)
         name=AuxOriOverrideSupport       | (0)
         name=HebcValueSupport            | (1)
         name=PchI2cSensorDevicePort[0]   | (1)
         name=PchI2cSensorDevicePort[1]   | (2)
         name=PchSpiSensorDevicePort[1]   | (1)
         name=NativeAspmEnable            | (0)
         name=PepGfx                      | (1)
         name=PepPeg0                     | (1)
         name=PepSataEnumeration          | (1)
         name=PepI2c0                     | (1)
         name=PepI2c1                     | (1)
         name=PepI2c2                     | (1)
         name=PepI2c3                     | (1)
         name=PepI2c4                     | (1)
         name=PepI2c5                     | (1)
         name=PepSpi                      | (1)
         name=PepXhci                     | (1)
         name=PepAudio                    | (1)
         name=SysFwUpdateSkipPowerCheck   | (1)
         name=TbtSetupFormSupport         | (1)
         name=PepCsme                     | (1)
         name=PepGbe                      | (1)
         name=PepTcss                     | (1)
         name=EnableDptf                  | (0)
         name=MipiCam_ControlLogic0       | (0)
         name=MipiCam_ControlLogic1       | (0)
         name=MipiCam_ControlLogic2       | (0)
         name=MipiCam_ControlLogic3       | (0)
         name=MipiCam_ControlLogic4       | (0)
         name=MipiCam_ControlLogic5       | (0)
         name=MipiCam_Link0               | (0)
         name=MipiCam_Link1               | (0)
         name=MipiCam_Link2               | (0)
         name=MipiCam_Link3               | (0)
         name=MipiCam_Link4               | (0)
         name=MipiCam_Link5               | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (1)
         name=DiscreteTbtSupport          | (1)
         name=DTbtRtd3ClkReq              | (1)
         name=DTbtController[0]           | (1)
         name=DTbtController[1]           | (1)
         name=DTbtRtd3                    | (1)
         name=ITbtRootPort[0]             | (0)
         name=ITbtRootPort[1]             | (0)
         name=ITbtRootPort[2]             | (0)
         name=ITbtRootPort[3]             | (0)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3A,0x3B,0x2F
  Struct = SA_SETUP
  Data = name=TcssXhciEn                       | (0)

[Var.gCpuSetupVariableGuid.CpuSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3A,0x3B,0x2F
  Struct = CPU_SETUP
  Data = name=Irms                             | (1)
  Data = name=EnergyEfficientTurbo             | (1)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3A,0x3B
  Struct = PCH_SETUP
  Data = name=PxDevSlp[0]                  | (1)
         name=PxDevSlp[1]                  | (1)
         name=PchSerialIoSpi[0]            | (0)
         name=PchSerialIoSpi[1]            | (1)
         name=PchSerialIoSpi[2]            | (0)
         name=PchSerialIoUart[0]           | (0)
         name=PchSerialIoUart[1]           | (0)
         name=PchUartHwFlowCtrl[2]         | (0)
         name=PchSerialIoI2c[2]            | (1)
         name=PchSerialIoI2c[3]            | (1)
         name=PchSerialIoI2c[4]            | (1)
         name=PchSerialIoI2c[5]            | (1)
         name=PchHdAudioI2sCodecSelect     | (0)
         name=PchLan                       | (0)
         name=PcieRootPortHPE[8]           | (1)
         name=PcieRootPortHPE[24]          | (1)
         name=PsOnEnable                   | (0)
         name=FoxvilleLanSupport           | (1)
         name=FoxvilleWakeOnLan            | (1)

#--------------------------------------------------------------------------
#  0x2F|BoardIdAdlSAdpSSbgaDdr5SODimmAep

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x2F
  Struct = SETUP_DATA
  Data = name=TbtLegacyModeSupport        | (0)
         name=AuxOriOverrideSupport       | (0)
         name=HebcValueSupport            | (1)
         name=PchI2cSensorDevicePort[0]   | (1)
         name=PchI2cSensorDevicePort[1]   | (2)
         name=PchSpiSensorDevicePort[1]   | (1)
         name=NativeAspmEnable            | (0)
         name=PepGfx                      | (1)
         name=PepPeg0                     | (1)
         name=PepSataEnumeration          | (1)
         name=PepI2c0                     | (1)
         name=PepI2c1                     | (1)
         name=PepI2c2                     | (1)
         name=PepI2c3                     | (1)
         name=PepI2c4                     | (1)
         name=PepI2c5                     | (1)
         name=PepSpi                      | (1)
         name=PepXhci                     | (1)
         name=PepAudio                    | (1)
         name=SysFwUpdateSkipPowerCheck   | (1)
         name=TbtSetupFormSupport         | (1)
         name=PepCsme                     | (1)
         name=PepGbe                      | (1)
         name=PepTcss                     | (1)
         name=MipiCam_ControlLogic0       | (0)
         name=MipiCam_ControlLogic1       | (0)
         name=MipiCam_ControlLogic2       | (0)
         name=MipiCam_ControlLogic3       | (0)
         name=MipiCam_ControlLogic4       | (0)
         name=MipiCam_ControlLogic5       | (0)
         name=MipiCam_Link0               | (0)
         name=MipiCam_Link1               | (0)
         name=MipiCam_Link2               | (0)
         name=MipiCam_Link3               | (0)
         name=MipiCam_Link4               | (0)
         name=MipiCam_Link5               | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (1)
         name=DiscreteTbtSupport          | (1)
         name=DTbtRtd3ClkReq              | (1)
         name=DTbtController[0]           | (1)
         name=DTbtController[1]           | (0)
         name=DTbtRtd3                    | (1)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x2F
  Struct = SA_SETUP
  Data = name=PrimaryDisplay                   | (4)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x2F
  Struct = PCH_SETUP
  Data = name=PxDevSlp[0]                  | (1)
         name=PxDevSlp[1]                  | (1)
         name=PchSerialIoSpi[0]            | (0)
         name=PchSerialIoSpi[1]            | (1)
         name=PchSerialIoSpi[2]            | (0)
         name=PchSerialIoUart[0]           | (0)
         name=PchSerialIoUart[1]           | (0)
         name=PchUartHwFlowCtrl[2]         | (0)
         name=PchSerialIoI2c[2]            | (1)
         name=PchSerialIoI2c[3]            | (1)
         name=PchSerialIoI2c[4]            | (1)
         name=PchSerialIoI2c[5]            | (1)
         name=PchHdAudioI2sCodecSelect     | (0)
         name=PchLan                       | (0)
         name=PcieRootPortHPE[8]           | (1)
         name=PcieRootPortHPE[24]          | (0)
         name=PsOnEnable                   | (0)
         name=FoxvilleLanSupport           | (1)
         name=FoxvilleWakeOnLan            | (1)

#--------------------------------------------------------------------------
#  0x3E|BoardIdAdlSAdpSSbgaDdr4SODimmCrb

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3E
  Struct = SETUP_DATA
  Data = name=TbtLegacyModeSupport        | (0)
         name=AuxOriOverrideSupport       | (0)
         name=HebcValueSupport            | (1)
         name=PchI2cSensorDevicePort[0]   | (1)
         name=PchI2cSensorDevicePort[1]   | (2)
         name=PchSpiSensorDevicePort[1]   | (1)
         name=NativeAspmEnable            | (0)
         name=PepGfx                      | (1)
         name=PepPeg0                     | (1)
         name=PepSataEnumeration          | (1)
         name=PepI2c0                     | (1)
         name=PepI2c1                     | (1)
         name=PepI2c2                     | (1)
         name=PepI2c3                     | (1)
         name=PepI2c4                     | (1)
         name=PepI2c5                     | (1)
         name=PepSpi                      | (1)
         name=PepXhci                     | (1)
         name=PepAudio                    | (1)
         name=SysFwUpdateSkipPowerCheck   | (1)
         name=TbtSetupFormSupport         | (1)
         name=PepCsme                     | (1)
         name=PepGbe                      | (1)
         name=PepTcss                     | (1)
         name=MipiCam_ControlLogic0       | (0)
         name=MipiCam_ControlLogic1       | (0)
         name=MipiCam_ControlLogic2       | (0)
         name=MipiCam_ControlLogic3       | (0)
         name=MipiCam_ControlLogic4       | (0)
         name=MipiCam_ControlLogic5       | (0)
         name=MipiCam_Link0               | (0)
         name=MipiCam_Link1               | (0)
         name=MipiCam_Link2               | (0)
         name=MipiCam_Link3               | (0)
         name=MipiCam_Link4               | (0)
         name=MipiCam_Link5               | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (1)
         
[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3E
  Struct = SA_SETUP
  Data = name=TcssXhciEn                       | (0)

[Var.gCpuSetupVariableGuid.CpuSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3E
  Struct = CPU_SETUP
  Data = name=Irms                             | (1)
  Data = name=EnergyEfficientTurbo             | (1)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3E
  Struct = PCH_SETUP
  Data = name=PxDevSlp[0]                  | (1)
         name=PxDevSlp[1]                  | (1)
         name=PchSerialIoSpi[0]            | (0)
         name=PchSerialIoSpi[1]            | (1)
         name=PchSerialIoSpi[2]            | (0)
         name=PchSerialIoUart[0]           | (0)
         name=PchSerialIoUart[1]           | (0)
         name=PchUartHwFlowCtrl[2]         | (0)
         name=PchSerialIoI2c[2]            | (1)
         name=PchSerialIoI2c[3]            | (1)
         name=PchSerialIoI2c[4]            | (1)
         name=PchSerialIoI2c[5]            | (1)
         name=PchHdAudioI2sCodecSelect     | (0)
         name=PchLan                       | (0)
         name=FoxvilleLanSupport           | (1)
         name=FoxvilleWakeOnLan           | (1)