@set path=%CD%\Tool\Python38;%path%
@set path=%CD%\Tool\Python38\Scripts\;%path%

@rem Check ProjectConfig.ini Golden value
@setlocal enabledelayedexpansion
@set /a sRow=12
@set /a eRow=12
@set /a sRow=%sRow%-1 
@if "%sRow%"=="0" (set sRow=) else set sRow=skip=%sRow%
for /f "%sRow% tokens=1,* delims=:" %%a in ('findstr /n ".*" ..\..\..\Board\Oem\L05AlderLakePMultiBoardPkg\LfcBprInput\ProjectConfig.ini') do (
    echo.%%b >> temp.txt
    if %eRow%==%%a goto Golden
)

:Golden
@setlocal enabledelayedexpansion
@for /f "delims=" %%i in (temp.txt) do (
@set GOLDEN_BIOS=%%i
@set GOLDEN_BIOS=!GOLDEN_BIOS:~7,7!
 )
@if not defined GOLDEN_BIOS_EN       set GOLDEN_BIOS_EN=%GOLDEN_BIOS%

if %GOLDEN_BIOS_EN%==1 goto EnGoldenBios else goto DisGoldenBios

:DisGoldenBios
@del temp.txt
cd LfcBpr
@Tool\Python38\python.exe -u LfcBpr.py %1 %2 %3 %4 %5 %6
@REM @Tool\Python38\python.exe LfcBpr.py /p "F:\Project\TGL\Tag0014\Board\Oem\L05TigerLakeMultiBoardPkg" %1 %2 %3 %4 %5 %6

:EnGoldenBios
@del temp.txt
cd LfcBpr
@Tool\Python38\python.exe -u LfcBprGolden.py %1 %2 %3 %4 %5 %6
@REM @Tool\Python38\python.exe LfcBpr.py /p "F:\Project\TGL\Tag0014\Board\Oem\L05TigerLakeMultiBoardPkg" %1 %2 %3 %4 %5 %6