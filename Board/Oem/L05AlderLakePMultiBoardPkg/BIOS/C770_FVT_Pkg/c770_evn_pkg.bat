@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_J1.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=J1CN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=ES_14,ES_16,QS_14,QS_16
set ES_14_FOLDER=J1CN%BIOS_VER_2_CHAR%WW_ES_14

REM Crisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1Crisis_%%a.bin J1CN%BIOS_VER_2_CHAR%WW_%%a\Crisis\J1Crisis.bin
)

REM Mfg
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1CNXXWW_%%a_IA32_SWDL.exe    J1CN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_IA32_SWDL.exe
copy /B ..\J1CNXXWW_%%a.bin              J1CN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT.bin
copy /B ..\J1CNXXWW_%%a1.bin             J1CN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT(part1")".bin
copy ..\J1CNXXWW_%%a2.bin             J1CN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT(part2")".bin
copy ..\J1CNXXWW_%%a_X64_SWDL.exe     J1CN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_X64_SWDL.exe
)

REM Uefi64
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\Mocca_REL_%%a.bin J1CN%BIOS_VER_2_CHAR%WW_%%a\Uefi64\J1CN%BIOS_VER_2_CHAR%WW.fd
echo H2OFFT-Sx64.efi %BIOS_VER%.fd -all > J1CN%BIOS_VER_2_CHAR%WW_%%a\Uefi64\flash.nsh
)

REM Windows
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1CNXXWW_%%a_IA32_REL.exe J1CN%BIOS_VER_2_CHAR%WW_%%a\Windows\J1CN%BIOS_VER_2_CHAR%WW.exe
)


REM checksum update
for %%a in (%BIOS_TYPE_LIST%) do (
cd J1CN%BIOS_VER_2_CHAR%WW_%%a
copy ..\..\..\LfcBprInput\LfcReleaseNote_J1.txt ReleaseNote.txt
echo update checksum
call Checksum.bat
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Crisis\J1Crisis.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_IA32_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_SMT(part1).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_SMT(part2).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_SMT.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_X64_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: PasswordCrisis\J1Crisis.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\H2OFFT-Sx64.efi"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\J1CN%BIOS_VER_2_CHAR%WW.fd"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Windows\J1CN%BIOS_VER_2_CHAR%WW.exe"
cd ..
)

REM pkg each type BIOS
for %%a in (%BIOS_TYPE_LIST%) do (
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a J1CN%BIOS_VER_2_CHAR%WW_%%a.7z J1CN%BIOS_VER_2_CHAR%WW_%%a\* -p123456 -mhe
)

REM move to release pkg
for %%a in (%BIOS_TYPE_LIST%) do (
move J1CN%BIOS_VER_2_CHAR%WW_%%a.7z MEBIOS\
)

REM remove temp files
for %%a in (%BIOS_TYPE_LIST%) do (
rd J1CN%BIOS_VER_2_CHAR%WW_%%a /S /Q
)

REM pkg release pkg
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a J1CN%BIOS_VER_2_CHAR%WW.7z MEBIOS tool unpackage.bat


cd MEBIOS
del *.* /q
cd ..