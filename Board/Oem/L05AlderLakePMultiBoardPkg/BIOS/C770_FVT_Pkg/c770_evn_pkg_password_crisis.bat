@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_J1.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=J1CN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=ES_14,ES_16,QS_14,QS_16
set ES_14_FOLDER=J1CN%BIOS_VER_2_CHAR%WW_ES_14

REM init
for %%a in (%BIOS_TYPE_LIST%) do (
  if exist J1CN%BIOS_VER_2_CHAR%WW_%%a (
  rd J1CN%BIOS_VER_2_CHAR%WW_%%a /S /Q
  )
  xcopy J1CNXXWW J1CN%BIOS_VER_2_CHAR%WW_%%a /E /Y /I
)


REM PasswordCrisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1Crisis_%%a.bin J1CN%BIOS_VER_2_CHAR%WW_%%a\PasswordCrisis\J1Crisis.bin
)

