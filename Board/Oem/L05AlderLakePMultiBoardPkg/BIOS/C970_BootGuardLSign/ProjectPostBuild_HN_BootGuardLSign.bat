@REM ******************************************************************************
@REM * Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************


@REM Run kernel post-build process
@if exist %WORKSPACE%\BaseTools\KernelPostBuild.bat call %WORKSPACE%\BaseTools\KernelPostBuild.bat %1
@REM Patch $BME of Bvdt region of Fd according .FDM after region resize
PatchBvdt %PLATFORM_TYPE%.fd %PLATFORM_TYPE%.fdm
@REM Run Chipset specific post-build process
@GetProjectEnv CHIPSET_PKG > NUL && for /f %%a in ('GetProjectEnv CHIPSET_PKG') do set %%a
@GetProjectEnv CHIPSET_REL_PATH > NUL && for /f %%a in ('GetProjectEnv CHIPSET_REL_PATH') do @set %%a
@if exist %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\ChipsetPostBuild.bat call %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\ChipsetPostBuild.bat %1

@echo off
REM Update hash vale of FDM entries at last step of building
for /f "tokens=3" %%a in ('findstr /R "\<TARGET\>" %WORKSPACE%\Conf\target.txt') do (
  PatchFdmHash %WORKSPACE%\Build\%PROJECT_PKG%\%%a_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%.fd %WORKSPACE%\Build\%PROJECT_PKG%\%%a_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%.fdm
)

REM Call FitGen.exe to fill FIT table and call BpmGen2.exe to generate KeyManifest and Manifest for Boot Guard.
setlocal
set BIOS_INFO_GUID=4A4CA1C6-871C-45bb-8801-6910A7AA5807
set STARTUP_AC_MODULE_GUID=26fdaa3d-b7ed-4714-8509-eecf1593800d
for /f "tokens=3" %%a in ('find "TARGET " %WORKSPACE%\Conf\target.txt') do set TARGET=%%a
set CHIPSET_PKG_TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\AlderLakeChipsetPkg\Tools\Bin\Win32
set PLATFORM_FULL_PACKAGE=%WORKSPACE%\%CHIPSET_REL_PATH%\AlderLakePlatSamplePkg
set BUILD_FV_PATH=%WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV
set EDK_TOOLS_BIN=%WORKSPACE%\BaseTools\Bin\Win32

set RESILIENCY_BUILD=FALSE
set /a CHASMFALLS_TYPE=0
for /f "tokens=2" %%a in ('find "gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport:" %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\X64\PcdList.txt') do set /a CHASMFALLS_TYPE=%%a
if %CHASMFALLS_TYPE% EQU 2 set RESILIENCY_BUILD=TRUE

if %RESILIENCY_BUILD% EQU TRUE (
  set MICROCODE_ARRAY_FFS_GUID=197DB236-F856-4924-90F8-CDF12FB875F3
  set SLOT_SIZE=0x3B000
)

if %CHASMFALLS_TYPE% EQU 0 goto skip_SBB_Digest_Flow
set /a BIOS_SIZE=0x0
set /a PBB_SIZE=0x0
set /a SBB_SIZE=0x0
@for /f "tokens=2" %%a in ('find "gInsydeTokenSpaceGuid.PcdFlashAreaSize:" %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\X64\PcdList.txt') do set /a BIOS_SIZE=%%a
@for /f "tokens=2" %%a in ('find "gInsydeTokenSpaceGuid.PcdFlashPbbSize:" %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\X64\PcdList.txt') do set /a PBB_SIZE=%%a
@for /f "tokens=2" %%a in ('find "gInsydeTokenSpaceGuid.PcdFlashSbbSize:" %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\X64\PcdList.txt') do set /a SBB_SIZE=%%a

set SBB_DIGEST_FILE_GUID=B2DA2CA8-7259-4F79-64A4-F712625ED2C9
set FV_GUID=CF1406C5-3FEC-47EB-A6C3-B71A3EE00B95
@REM
@REM //[-start-220103-BAIN000081-modify]//
@REM BIOS = Var + OBB + (PBBR/empty) + PBB
@REM BIOS_SIZE   - PBB_SIZE = PBB_OFFSET
@REM PBB_OFFSET  - PBB_SIZE = PBBR_OFFSET
@REM PBBR_OFFSET - SBB_SIZE = SBB_OFFSET
@REM
set /a PBB_OFFSET=%BIOS_SIZE%-%PBB_SIZE%
set /a PBBR_OFFSET=%PBB_OFFSET%-%PBB_SIZE%
set /a SBB_OFFSET=%PBBR_OFFSET%-%SBB_SIZE%

if exist Temp rd /s /q Temp
mkdir Temp 
copy /y /b %BUILD_FV_PATH%\%PLATFORM_TYPE%.fd Temp\%PLATFORM_TYPE%_Backup.fd
cd Temp
split -f %PLATFORM_TYPE%_Backup.fd -s %SBB_OFFSET% -o VAR.fd -t PBB_SBB.fd
split -f PBB_SBB.fd -s %SBB_SIZE% -o SBB.fd -t PBB.fd
openssl dgst -binary -sha256 SBB.fd > SbbDigest.bin
@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "SbbDigest" -o SbbDigest.ui
@call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW SbbDigest.bin -o SbbDigest.raw
@call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %SBB_DIGEST_FILE_GUID% -o SbbDigest.ffs -i SbbDigest.raw -i SbbDigest.ui
@call %EDK_TOOLS_BIN%\FMMT.exe -r %PLATFORM_TYPE%_Backup.fd %FV_GUID% SbbDigest SbbDigest.ffs SbbDigest_temp.fd
@if exist %BUILD_FV_PATH%\%PLATFORM_TYPE%.fd del %BUILD_FV_PATH%\%PLATFORM_TYPE%.fd
@copy /y /b SbbDigest_temp.fd %BUILD_FV_PATH%\%PLATFORM_TYPE%.fd
@cd ..
if exist Temp rd /s /q Temp

:SKIP_SBB_Digest_Flow
call BiosGuardPostBuild.bat
@REM //[-end-220103-BAIN000081-modify]//

@REM ******************************************************************************
@REM * FitGen BIOS Input Filename: %PLATFORM_TYPE%_PRE_FIT.fd
@REM * FitGen BIOS Output Filename: %PLATFORM_TYPE%.fd
@REM ******************************************************************************

echo ***** Fill FIT table *****
set TXT_SUPPORT=YES
set BOOT_GUARD_SUPPORT=YES
set PCDLIST_FILE=%WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\X64\PcdList.txt

@REM Intel Converged Boot Guard and Trusted Execution Technologies (CBnT) feature
for /f "tokens=2" %%a in ('find "gChipsetPkgTokenSpaceGuid.PcdTXTSupported" %PCDLIST_FILE%') do if %%a==0 set TXT_SUPPORT=NO
for /f "tokens=2" %%a in ('find "gSiPkgTokenSpaceGuid.PcdTxtEnable" %PCDLIST_FILE%') do if %%a==0 set TXT_SUPPORT=NO
for /f "tokens=2" %%a in ('find "gSiPkgTokenSpaceGuid.PcdBootGuardEnable" %PCDLIST_FILE%') do if %%a==0 set BOOT_GUARD_SUPPORT=NO

@REM Fit Table and Sign process will going if:
@REM 1. CBnT Boot Guard
@REM 2. CBnT Boot Guard with CBnT TXT
@REM Only CBnT TXT is incorrect
@REM For CBnT, the Boot Policy Manifest is updated to add a new TXT Element (_TXTE_), which is used to provide additional information needed for TXT functionality
@REM The TXT Element is optional, and if not defined, all of its fields are assumed to have default values.
@if "%BOOT_GUARD_SUPPORT%"=="NO" (
  goto FIT_BootGuardOff
)

@REM
@REM "%BOOT_GUARD_SUPPORT%"=="YES" or with CBnT "%TXT_SUPPORT%"=="YES"
@REM
pushd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV
if exist %PLATFORM_TYPE%_PRE_FIT.fd del %PLATFORM_TYPE%_PRE_FIT.fd
rename %PLATFORM_TYPE%.fd %PLATFORM_TYPE%_PRE_FIT.fd

@if "%TXT_SUPPORT%"=="YES" (
  if %RESILIENCY_BUILD% EQU TRUE (
    %CHIPSET_PKG_TOOLS_PATH%\FitGen.exe -D %PLATFORM_TYPE%_PRE_FIT.fd %PLATFORM_TYPE%.fd -F 0x40 -NA -L %SLOT_SIZE% %MICROCODE_ARRAY_FFS_GUID% -I %BIOS_INFO_GUID% -S %STARTUP_AC_MODULE_GUID% -O 0x0C RESERVE 0x600 -O 0x0B RESERVE 0x400 -P 0x0A 0x70 0x71 0x01 0x04 0x2A
  ) else (
    %CHIPSET_PKG_TOOLS_PATH%\FitGen.exe -D %PLATFORM_TYPE%_PRE_FIT.fd %PLATFORM_TYPE%.fd -F 0x40 -NA -I %BIOS_INFO_GUID% -S %STARTUP_AC_MODULE_GUID% -O 0x0C RESERVE 0x600 -O 0x0B RESERVE 0x400 -P 0x0A 0x70 0x71 0x01 0x04 0x2A
  )
)

@if "%TXT_SUPPORT%"=="NO" (
  if %RESILIENCY_BUILD% EQU TRUE (
    %CHIPSET_PKG_TOOLS_PATH%\FitGen.exe -D %PLATFORM_TYPE%_PRE_FIT.fd %PLATFORM_TYPE%.fd -F 0x40 -NA -L %SLOT_SIZE% %MICROCODE_ARRAY_FFS_GUID% -I %BIOS_INFO_GUID% -S %STARTUP_AC_MODULE_GUID% -O 0x0C RESERVE 0x600 -O 0x0B RESERVE 0x400
  ) else (
    %CHIPSET_PKG_TOOLS_PATH%\FitGen.exe -D %PLATFORM_TYPE%_PRE_FIT.fd %PLATFORM_TYPE%.fd -F 0x40 -NA -I %BIOS_INFO_GUID% -S %STARTUP_AC_MODULE_GUID% -O 0x0C RESERVE 0x600 -O 0x0B RESERVE 0x400
  )
)
popd
if not errorlevel 0 goto FitError
goto GenForBootGuard

:FIT_BootGuardOff
pushd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV
if exist %PLATFORM_TYPE%_PRE_FIT.fd del %PLATFORM_TYPE%_PRE_FIT.fd
rename %PLATFORM_TYPE%.fd %PLATFORM_TYPE%_PRE_FIT.fd
if %RESILIENCY_BUILD% EQU TRUE (
  %CHIPSET_PKG_TOOLS_PATH%\FitGen.exe -D %PLATFORM_TYPE%_PRE_FIT.fd %PLATFORM_TYPE%.fd -F 0x40 -NA -L %SLOT_SIZE% %MICROCODE_ARRAY_FFS_GUID% -I %BIOS_INFO_GUID%
) else (
  %CHIPSET_PKG_TOOLS_PATH%\FitGen.exe -D %PLATFORM_TYPE%_PRE_FIT.fd %PLATFORM_TYPE%.fd -F 0x40 -NA -I %BIOS_INFO_GUID%
)
popd
if errorlevel 0 goto End_FIT

:FitError
REM Error handling: pause when error occurs
echo ==========================================================================
echo !!!!!                              !!!!!
echo !!!!!     FIT Generation Error     !!!!!
echo !!!!!                              !!!!!
echo ==========================================================================
pause

:End_FIT
@REM ******************************************************************************
@REM * BpmGen2 BIOS Input Filename: %PLATFORM_TYPE%_FIT.fd
@REM * BpmGen2 BIOS Output Filename: %PLATFORM_TYPE%.fd
@REM ******************************************************************************

:GenForBootGuard
@if "%BOOT_GUARD_SUPPORT%"=="NO" (
  goto End_GenForBootGuard
)
echo ***** Generate for Boot Guard when BOOT_GUARD_SUPPORT = YES *****
set KM_REVOCATION=1
set KM_ID=0x01

pushd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV
if exist %PLATFORM_TYPE%_FIT.fd del %PLATFORM_TYPE%_FIT.fd
rename %PLATFORM_TYPE%.fd %PLATFORM_TYPE%_FIT.fd
popd


echo "#### BpmGen2:  Generating KeyManifest.bin ####"
pushd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\PlatformConfig
  @REM Intel Converged Boot Guard and Trusted Execution Technologies (CBnT) feature enabled
  %PLATFORM_FULL_PACKAGE%\Tools\BpmGen2\BpmGen2.exe KMGEN -KEY pubkey.pem BPM -KM %BUILD_FV_PATH%\KeyManifest.bin -SIGNKEY keyprivkey.pem -SIGHASHALG SHA384 -SCHEME RSAPSS -KMKHASH SHA384 -KMID %KM_ID% -SVN %KM_REVOCATION% -d:2 >bpmgen2_km.txt
popd
if %ERRORLEVEL% NEQ 0 (
  echo "#### Error generating KM file #####"
  goto :EOF
)

echo "#### BpmGen2:  Generating Manifest.bin ####"
pushd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\PlatformConfig
  @REM Intel Converged Boot Guard and Trusted Execution Technologies (CBnT) feature enabled
  %PLATFORM_FULL_PACKAGE%\Tools\BpmGen2\BpmGen2.exe GEN %BUILD_FV_PATH%\%PLATFORM_TYPE%_FIT.fd bpmgen2.params -BPM %BUILD_FV_PATH%\Manifest.bin -U %BUILD_FV_PATH%\%PLATFORM_TYPE%.fd -KM %BUILD_FV_PATH%\KeyManifest.bin -d:2 >bpmgen2_bpm.txt
popd

@REM [-start-211230-Dongxu0039-add]
@ECHO Please sign the BIOS for bootguard and replace the BIOS
copy /y /b %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%_FIT.fd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\C970_BootGuardLSign\input\%PLATFORM_TYPE%_FIT.fd  /Y
pushd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\C970_BootGuardLSign
call C970_BootGuardLSign_email.bat
popd
@REM [-end-211230-Dongxu0039-add]

if %ERRORLEVEL% NEQ 0 (
  echo "#### Error generating BPM file #####"
  goto :EOF
)

for /f "tokens=2" %%a in ('find "gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport" %PCDLIST_FILE%') do if %%a==0 goto SkipExtendedRegionPostBuildProcess
@REM
@REM Cutting off unused part of Extended BIOS Region out of the FD image
@REM
@REM @if %EXTENDEDREGION_BUILD% EQU FALSE goto SkipExtendedRegionPostBuildProcess
@set FLASHMAP_FDF=%WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\Project.fdf
@if not defined FLASHMAP_FDF goto SkipExtendedRegionPostBuildProcess
  @set EXTENDED_REGION_SIZE=
  @set EXTENDED_REGION_IN_USE=
  @for /f "tokens=3" %%a in ('find "TARGET " %WORKSPACE%\Conf\target.txt') do set TARGET=%%a
  @for /f "tokens=4" %%i in ('@findstr /c:"DEFINE EXTENDED_REGION_SIZE " %FLASHMAP_FDF%') do @set EXTENDED_REGION_SIZE=%%i
  @for /f "tokens=4" %%j in ('@findstr /c:"DEFINE EXTENDED_REGION_IN_USE" %FLASHMAP_FDF%') do @set EXTENDED_REGION_IN_USE=%%j
  @echo Extended BIOS Region size        : %EXTENDED_REGION_SIZE%
  @echo Extended BIOS Region size in use : %EXTENDED_REGION_IN_USE%
  @set /a CUTOFF_SIZE="%EXTENDED_REGION_SIZE%-%EXTENDED_REGION_IN_USE%"
  @echo Cutting off unused %CUTOFF_SIZE% in size
  @copy /y /b %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%.fd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%_ExtSplitPre.fd
  split -f %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%.fd -s %CUTOFF_SIZE% -t %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%_ExtSplitPost.fd -o %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\Garbage.bin
  @copy /y /b %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%_ExtSplitPost.fd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%.fd
  @del %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\Garbage.bin
:SkipExtendedRegionPostBuildProcess

:End_GenForBootGuard
echo ***** Copy BIOS to BoardPkg\BIOS *****
copy /y %BUILD_FV_PATH%\%PLATFORM_TYPE%.fd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%PLATFORM_TYPE%.fd > NUL

@REM
@REM Uncomment this command to generate "BIOS Guard Update Image" by default for your projects
@REM
@REM copy /b .\BIOS\AlderLakeP.fd + /b .\BIOS\InsydeBiosGuardHeader.bin .\BIOS\AlderLakeP_BiosGuard.fd /y > NUL

@REM ------------ Restore BaseTools\Bin\Win32\GenVarrcBat.bat file -------------#
@if exist %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat_.bat (
  copy %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat_.bat %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat.bat /y
  del  %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat_.bat
)

@REM FvAlignChecker.exe for 4K Align Check
FvAlignChecker.exe --PCD-report-file %WORKSPACE%\Build\%PROJECT_PKG%\BuildReport.txt
@REM cd BIOS
@REM cd AlderLakeLP
@REM BuildRom.bat %WORKSPACE%\%BUILD_DIR%
@REM cd ..
@REM cd ..

@if %RESILIENCY_BUILD% EQU TRUE (
@REM 
@REM variable + OBB(SBB) + IPBBR(PBBR) + IBB(PBB) = bios size
@REM BIOS_SIZE = 0x1000000 (16M)
@REM IBB_SIZE = IBBR SIZE = 0400000 (4M)
@REM

if exist Temp rd /s /q Temp
mkdir Temp
copy /y /b Bios\%PLATFORM_TYPE%.fd Temp\%PLATFORM_TYPE%_Backup.fd

cd Temp
split -f %PLATFORM_TYPE%_Backup.fd -s %PBB_OFFSET% -t PBB.fd -o PBBR_SBB.fd
split -f PBBR_SBB.fd -s %PBBR_OFFSET% -t PBBR.fd -o SBB_VAR.fd
copy /y /b SBB_VAR.fd + PBB.fd + PBB.fd %PLATFORM_TYPE%.fd
cd ..

@if exist Bios\%PLATFORM_TYPE%.fd del Bios\%PLATFORM_TYPE%.fd
copy /y /b Temp\%PLATFORM_TYPE%.fd Bios\%PLATFORM_TYPE%.fd
@if exist Temp rd /s /q Temp
)
endlocal

@echo on

@REM [-start-210519-KEBIN00001-modify]
@if not defined ME_VERSION     set ME_VERSION=16.0.15.1620
@if not defined BIOS_NAME      set BIOS_NAME=HNCNXXWW
@if not defined BUILD_ID       set BUILD_ID=HN
@if not defined PLFM_NAME      set PLFM_NAME=AlderLakeP
@if not defined IFDPACKER_NAME set IFDPACKER_NAME=InsydeH2OFFT_x86_WIN_6.52.00
@if not defined TOOL_FOLDER    set TOOL_FOLDER=tools
@if not defined X86_FOLDER     set X86_FOLDER=x86
@if not defined X64_FOLDER     set X64_FOLDER=x86_64
@if not defined SF_Tool        set SF_Tool=SecurityFlash2.01.07.00
@if not defined SC_Tool        set SC_Tool=SecureCrisis1.00

@REM [-start-211117-Dongxu0030-modify]
@echo ------------Pack ME------------
cd ME\%ME_VERSION%
@mfit.exe --loadconfig %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\ME\%ME_VERSION%\YogaC970.xml --build %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.bin
@mfit.exe --loadconfig %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\ME\%ME_VERSION%\YogaC970.xml --build %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_QS.bin
@mfit.exe --loadconfig %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\ME\%ME_VERSION%\YogaC970SQS.xml --build %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_SQS.bin
@cd..
@cd..


rem For C970 SIT2 loop start

rem init

del /F /Q BIOS\HNCrisis*.bin
del /F /Q BIOS\Mocca*.bin
del /F /Q BIOS\HNCN*.exe

set BIOS_NAME_ORG=%BIOS_NAME%

set BIOS_NAME=%BIOS_NAME_ORG%_QS
goto PkgAndExeMEBIOS
:TypeSQS
set BIOS_NAME=%BIOS_NAME_ORG%_SQS
goto PkgAndExeMEBIOS

:PkgAndExeMEBIOS
echo pkg ME and exe for %BIOS_NAME%

@if exist bios_smt.bin del bios_smt.bin
@if exist bios_smt1.bin del bios_smt1.bin
@if exist bios_smt2.bin del bios_smt2.bin
@if exist crisis.bin del crisis.bin
copy /y /b BIOS\%BIOS_NAME%.bin bios_smt.bin
copy /y /b BIOS\%BIOS_NAME%1.bin bios_smt1.bin
copy /y /b BIOS\%BIOS_NAME%2.bin bios_smt2.bin
copy /y /b BIOS\AlderLakeP.fd crisis.bin
@if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\BiosMe.bin del %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\BiosMe.bin > nul
@if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Crisis.fd del %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BUILD_ID%Crisis.fd > nul

@echo ------------For Auto Release BIOS Package Use------------
@copy /b /y %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\BiosMe.bin > nul

@for /f "tokens=4" %%a in ('find "SECURE_FLASH_SUPPORT" ..\..\..\Build\%PROJECT_PKG%\Project.dsc') do if %%a==YES goto EnSecureFlash

:DisSecureFlash
@echo ------------Disable Secure Flash------------
@echo creating Crisis ROM
@copy /b /y %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%PLFM_NAME%.fd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BUILD_ID%Crisis.bin > nul

@echo ------------Pack Win Flash------------
@copy /b /y %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.fd
@cd FlashTool\%IFDPACKER_NAME%

@echo REL X86 WinFlash.exe
@copy /y Platform_REL.ini %X86_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X86_FOLDER% -winini -b 3264 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_IA32_REL.exe
@cd ..

@echo SMT X86 WinFlash.exe
@copy /y Platform_SMT.ini %X86_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X86_FOLDER% -winini -b 3264 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_IA32_SWDL.exe
@cd ..

@echo SMT X64 WinFlash.exe
@copy /y Platform_SMT.ini %X64_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X64_FOLDER% -winini -b 64 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_X64_SWDL.exe
@cd ..

@echo NAC X64 WinFlash.exe
@copy /y Platform_NAC.ini %X64_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X64_FOLDER% -winini -b 64 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_X64_NAC.exe
@cd ..

@cd ..
@goto GpioWarning


:EnSecureFlash
@echo ------------Enable Secure Flash------------
@cd FlashTool\%SF_Tool%

@echo ------------Sign BIOS------------
@echo Sign REL BIOS ROM
@if exist isflash.bin del isflash.bin /a:r >nul
@iEFIFlashSigner.exe sign -n "Mocca" -bios %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.bin -ini %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\Platform_REL.ini
@copy /b /y isflash.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_REL.bin > nul
@copy /b /y %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_REL.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_REL.fd > nul

@echo Sign SMT BIOS ROM
@if exist isflash.bin del isflash.bin /a:r >nul
@iEFIFlashSigner.exe sign -n "Mocca" -bios %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.bin -ini %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\Platform_SMT.ini
@copy /b /y isflash.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.bin > nul
@copy /b /y %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.fd > nul

@echo Sign NAC BIOS ROM
@if exist isflash.bin del isflash.bin /a:r >nul
@iEFIFlashSigner.exe sign -n "Mocca" -bios %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%.bin -ini %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\Platform_NAC.ini
@copy /b /y isflash.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_NAC.bin > nul
@copy /b /y %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_NAC.bin %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_NAC.fd > nul

@if exist isflash.bin del isflash.bin /a:r >nul

@echo Sign Crisis ROM
@cd ..
@cd %SC_Tool%
@call %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%SC_Tool%\pkcs1sign %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%SF_Tool%\Mocca.pfx %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%PLFM_NAME%.fd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BUILD_ID%Crisis.bin
@cd ..
@cd ..

@echo ------------Pack Win Flash------------
@cd FlashTool\%IFDPACKER_NAME%

@echo REL X86 WinFlash.exe
@copy /y Platform_REL.ini %X86_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X86_FOLDER% -winini -b 3264 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_REL.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_IA32_REL.exe
@cd ..

@echo SMT X86 WinFlash.exe
@copy /y Platform_SMT.ini %X86_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X86_FOLDER% -winini -b 3264 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_IA32_SWDL.exe
@cd ..

@echo SMT X64 WinFlash.exe
@copy /y Platform_SMT.ini %X64_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X64_FOLDER% -winini -b 64 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_X64_SWDL.exe
@cd ..

@echo NAC X64 WinFlash.exe
@copy /y Platform_NAC.ini %X64_FOLDER%\platform.ini
@cd %TOOL_FOLDER%
iFdPacker.exe -winsrc %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\FlashTool\%IFDPACKER_NAME%\%X64_FOLDER% -winini -b 64 -winarg "-b" -fv %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_NAC.fd -output %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\%BIOS_NAME%_X64_NAC.exe
@cd ..

@if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_REL.fd del %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_REL.fd > nul
@if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.fd del %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_SMT.fd > nul
@if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_NAC.fd del %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BIOS\Mocca_NAC.fd > nul

@if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\ME\%ME_VERSION%\fit.log del %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\ME\%ME_VERSION%\fit.log > nul
@time /t
@cd ..
@cd ..

if "%BIOS_NAME%"=="HNCNXXWW_QS" (
rename BIOS\%BUILD_ID%Crisis.bin %BUILD_ID%Crisis_QS.bin
rename BIOS\Mocca_NAC.bin Mocca_NAC_QS.bin
rename BIOS\Mocca_REL.bin Mocca_REL_QS.bin
rename BIOS\Mocca_SMT.bin Mocca_SMT_QS.bin
goto TypeSQS
) 

rename BIOS\%BUILD_ID%Crisis.bin %BUILD_ID%Crisis_SQS.bin
rename BIOS\Mocca_NAC.bin Mocca_NAC_SQS.bin
rename BIOS\Mocca_REL.bin Mocca_REL_SQS.bin
rename BIOS\Mocca_SMT.bin Mocca_SMT_SQS.bin
rem For C770 FVT loop end


@REM [-end-210519-KEBIN00001-modify]
:GpioWarning
@MD GpioWarning
@CD GpioWarning
@echo ==============================================================================
@echo The SoC may be burned out if you don't have correct GPIO settings! > "Warning"
@findstr /a:0c /C:"The" /S "Warning"

@echo Before you using the binary on your project,
@echo Please double check your GPIO settings based on your H/W design!
@echo Except for all pads in GPIO F group and GPD group, all other GPIO pads support perpad
@echo configurable voltage, which allows control selection of 1.8V or 3.3V for each pad.
@echo GPIO pad voltage configuration must be set correctly depending on device connected to it; otherwise, damage to the PCH or the device may occur.
@echo You should check:
@echo   1. ME XML file (Voltage setting for 3.3V/1.8V Pads)
@echo   2. Gpio file (Internal Pull-up/Pull-down, Rx/Tx EnCfg, ...)
@echo Notes: - GPIO F group supports 1.8V only.
@echo        - GPD group supports 3.3V only.
@echo ==============================================================================
@CD ..
@RD /S /Q GpioWarning

@REM @REM[-start-200710-IB10181004-add]REM
@REM @REM POWER_ON_FLAG
@REM @REM NOTE!! calling gensimics to generate simics relative binaries for Intel Simics.
@REM @echo ---Create ROM and Simics images---
@REM @call %WORKSPACE%\Board\Intel\AlderLakePMultiBoardPkg\gensimics.bat
@REM @if %errorlevel% NEQ 0 (
@REM   @echo !!! ERROR !!! Create ROM and Simics images !!!
@REM )
@REM @REM[-end-200710-IB10181004-add]REM

@REM auto build for formal release @ SIT2 phase

cd BIOS\C970_SIT2_Pkg\

for /f %%a in ('get_bios_version_from_uni.exe ../../Project_HN.uni') do set "BIOS_VER_STR=%%a"

for /l %%I in (1,1,9) do (
 echo "HNCN0%%IWW", "%BIOS_VER_STR%"
 if "HNCN0%%IWW"=="%BIOS_VER_STR%" (
   goto package
 )
)

for /l %%I in (10,1,99) do (
 echo "HNCN%%IWW", "%BIOS_VER_STR%"
 if "HNCN%%IWW"=="%BIOS_VER_STR%" (
   goto package
 )
)

echo end
goto end

:package
echo package
call c970_evn_pkg.bat
goto end

:end

cd ..
cd ..
@REM [-end-211117-Dongxu0030-modify]

