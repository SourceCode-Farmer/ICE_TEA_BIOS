@echo off

@if not defined ME_VERSION1     set ME_VERSION1=16.0.15.1620
@if not defined ISH_VERSION1    set ISH_VERSION1=4565
@if not defined PROJECT_NAME    set PROJECT_NAME=C970

copy /y YogaC970_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\YogaC970.xml /Y

copy /y YogaC970SQS_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\YogaC970SQS.xml /Y

copy /y /b ISHC_MEU.bin ..\..\ME\%ME_VERSION1%\ISH\%ISH_VERSION1%\%PROJECT_NAME%\ISHC_MEU.bin /Y

copy /y /b OEMKeyManifest.bin ..\..\ME\%ME_VERSION1%\ISH\%ISH_VERSION1%\%PROJECT_NAME%\OEMKeyManifest.bin /Y

copy /y ProjectPostBuild_HN_BootGuardLSign.bat ..\..\ProjectPostBuild_HN.bat /Y