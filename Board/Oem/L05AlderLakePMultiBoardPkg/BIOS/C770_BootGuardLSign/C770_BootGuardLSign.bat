@echo off

@if not defined ME_VERSION1     set ME_VERSION1=16.0.15.1778
@if not defined ISH_VERSION1    set ISH_VERSION1=4565
@if not defined PROJECT_NAME    set PROJECT_NAME=C770

copy /y YogaC770_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\YogaC770SQS.xml /Y

@REM copy /y YogaC770QS_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\YogaC770QSK0.xml /Y

copy /y /b ISHC_MEU.bin ..\..\ME\%ME_VERSION1%\ISH\%ISH_VERSION1%\%PROJECT_NAME%\ISHC_MEU.bin /Y

copy /y /b OEMKeyManifest.bin ..\..\ME\%ME_VERSION1%\ISH\%ISH_VERSION1%\%PROJECT_NAME%\OEMKeyManifest.bin /Y

copy /y ProjectPostBuild_J1_BootGuardLSign.bat ..\..\ProjectPostBuild_J1.bat /Y