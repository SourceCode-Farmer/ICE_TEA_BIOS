@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_HN.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=HNCN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=QS,SQS
set QS_FOLDER=HNCN%BIOS_VER_2_CHAR%WW_QS

REM init
for %%a in (%BIOS_TYPE_LIST%) do (
  if exist HNCN%BIOS_VER_2_CHAR%WW_%%a (
  rd HNCN%BIOS_VER_2_CHAR%WW_%%a /S /Q
  )
  xcopy HNCNXXWW HNCN%BIOS_VER_2_CHAR%WW_%%a /E /Y /I
)

REM PasswordCrisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\HNCrisis_%%a.bin HNCN%BIOS_VER_2_CHAR%WW_%%a\PasswordCrisis\HNCN.bin
)

