@echo off

@if not defined ME_VERSION1     set ME_VERSION1=16.0.15.1810
@if not defined PROJECT_NAME    set PROJECT_NAME=S370

copy /y S370_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\S370.xml /Y

copy /y /b OEMKeyManifest.bin ..\..\ME\%ME_VERSION1%\Audio\%PROJECT_NAME%\OEMKeyManifest.bin /Y

copy /y ProjectPostBuild_JK_BootGuardLSign.bat ..\..\ProjectPostBuild_JK.bat /Y