@echo off

@if not defined ME_VERSION1     set ME_VERSION1=16.0.15.1810
@if not defined PROJECT_NAME    set PROJECT_NAME=S570

copy /y S570_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\S570_QS.xml /Y

copy /y /b OEMKeyManifest.bin ..\..\ME\%ME_VERSION1%\Audio\%PROJECT_NAME%\OEMKeyManifest.bin /Y

copy /y ProjectPostBuild_JB_BootGuardLSign.bat ..\..\ProjectPostBuild_JB.bat /Y