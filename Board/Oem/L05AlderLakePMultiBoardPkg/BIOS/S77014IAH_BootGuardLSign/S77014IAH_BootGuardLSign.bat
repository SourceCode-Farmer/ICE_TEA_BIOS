@echo off

@if not defined ME_VERSION1     set ME_VERSION1=16.0.15.1662
@if not defined ISH_VERSION1    set ISH_VERSION1=4565
@if not defined PROJECT_NAME    set PROJECT_NAME=S77014IAH

copy /y S77014IAH_BootGuardLSign.xml ..\..\ME\%ME_VERSION1%\S77014IAH.xml /Y

copy /y /b ISHC_MEU.bin ..\..\ME\%ME_VERSION1%\ISH\%ISH_VERSION1%\%PROJECT_NAME%\ISHC_MEU.bin /Y

copy /y /b OEMKeyManifest.bin ..\..\ME\%ME_VERSION1%\ISH\%ISH_VERSION1%\%PROJECT_NAME%\OEMKeyManifest.bin /Y

copy /y ProjectPostBuild_KR_BootGuardLSign.bat ..\..\ProjectPostBuild_KR.bat /Y