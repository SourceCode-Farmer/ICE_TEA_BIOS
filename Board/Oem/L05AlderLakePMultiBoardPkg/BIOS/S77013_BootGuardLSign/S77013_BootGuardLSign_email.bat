@echo on
@if not defined BUILD_ID    set BUILD_ID=K2

..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a .\input.7z .\input\* 

call ..\..\LfcBpr\Tool\Python38\python.exe ..\..\LfcBpr\LfcEmail.py %BUILD_ID% %cd%

echo LfcEmail.py end


@if not exist 05_tool_7z_uncompress2\Unzipped2\AlderLakeP.fd (
  echo LSign error
  pause
)

REM copy 05_tool_7z_uncompress2\Unzipped2\AlderLakeP.fd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\%PLATFORM_TYPE%.fd
copy /y 05_tool_7z_uncompress2\Unzipped2\AlderLakeP.fd ..\..\..\..\..\Build\L05AlderLakePMultiBoardPkg\RELEASE_DEVTLSxVC16\FV\AlderLakeP.fd /Y


REM pause 
REM you can check

@if exist data.json del  data.json

@if exist input.7z del input.7z

@if exist 00_prepare_time_stamp rd /s /q 00_prepare_time_stamp

@if exist 01_get_time_stamp rd /s /q 01_get_time_stamp

@if exist 02_send_email_to_sign rd /s /q 02_send_email_to_sign

@if exist 03_wait_for_sign_back rd /s /q 03_wait_for_sign_back

@if exist 04_tool_7z_uncompress1 rd /s /q 04_tool_7z_uncompress1

@if exist 05_tool_7z_uncompress2 rd /s /q 05_tool_7z_uncompress2
