@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_JB.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=JBCN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=ES,QS
set ES_FOLDER=JBCN%BIOS_VER_2_CHAR%WW_ES

for %%a in (%BIOS_TYPE_LIST%) do (
  if not exist JBCN%BIOS_VER_2_CHAR%WW_%%a (
    xcopy JBCNXXWW JBCN%BIOS_VER_2_CHAR%WW_%%a /E /Y /I
  )
)

REM Crisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JBCrisis_%%a.bin JBCN%BIOS_VER_2_CHAR%WW_%%a\Crisis\JBCN.bin
)

REM Mfg
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JBCNXXWW_%%a_IA32_SWDL.exe    JBCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_IA32_SWDL.exe
copy /B ..\JBCNXXWW_%%a.bin              JBCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT.bin
copy /B ..\JBCNXXWW_%%a1.bin             JBCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT(part1")".bin
copy /B ..\JBCNXXWW_%%a2.bin             JBCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT(part2")".bin
copy /B ..\JBCNXXWW_%%a_X64_SWDL.exe     JBCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_X64_SWDL.exe
)

REM Uefi64
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\Mocca_REL_%%a.bin JBCN%BIOS_VER_2_CHAR%WW_%%a\Uefi64\JBCN%BIOS_VER_2_CHAR%WW.fd
echo H2OFFT-Sx64.efi %BIOS_VER%.fd -all > JBCN%BIOS_VER_2_CHAR%WW_%%a\Uefi64\flash.nsh
)

REM Windows
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JBCNXXWW_%%a_IA32_REL.exe JBCN%BIOS_VER_2_CHAR%WW_%%a\Windows\JBCN%BIOS_VER_2_CHAR%WW.exe
)


REM checksum update
for %%a in (%BIOS_TYPE_LIST%) do (
cd JBCN%BIOS_VER_2_CHAR%WW_%%a
copy ..\..\..\LfcBprInput\LfcReleaseNote_JB.txt ReleaseNote.txt
echo update checksum
call Checksum.bat
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Crisis\JBCN.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JBCN%BIOS_VER_2_CHAR%WW_IA32_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JBCN%BIOS_VER_2_CHAR%WW_SMT(part1).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JBCN%BIOS_VER_2_CHAR%WW_SMT(part2).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JBCN%BIOS_VER_2_CHAR%WW_SMT.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JBCN%BIOS_VER_2_CHAR%WW_X64_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: PasswordCrisis\JBCN.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\H2OFFT-Sx64.efi"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\JBCN%BIOS_VER_2_CHAR%WW.fd"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Windows\JBCN%BIOS_VER_2_CHAR%WW.exe"
cd ..
)

REM pkg each type BIOS
for %%a in (%BIOS_TYPE_LIST%) do (
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a JBCN%BIOS_VER_2_CHAR%WW_%%a.7z JBCN%BIOS_VER_2_CHAR%WW_%%a\* -p123456 -mhe
)

REM move to release pkg
for %%a in (%BIOS_TYPE_LIST%) do (
move JBCN%BIOS_VER_2_CHAR%WW_%%a.7z MEBIOS\
)

REM remove temp files
for %%a in (%BIOS_TYPE_LIST%) do (
rd JBCN%BIOS_VER_2_CHAR%WW_%%a /S /Q
)

REM pkg release pkg
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a JBCN%BIOS_VER_2_CHAR%WW.7z MEBIOS tool unpackage.bat


cd MEBIOS
del *.* /q
cd ..