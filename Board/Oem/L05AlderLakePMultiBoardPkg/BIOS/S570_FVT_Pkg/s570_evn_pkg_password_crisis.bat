@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_JB.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=JBCN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=ES,QS
set ES_FOLDER=JBCN%BIOS_VER_2_CHAR%WW_ES

REM init
for %%a in (%BIOS_TYPE_LIST%) do (
  if exist JBCN%BIOS_VER_2_CHAR%WW_%%a (
  rd JBCN%BIOS_VER_2_CHAR%WW_%%a /S /Q
  )
  xcopy JBCNXXWW JBCN%BIOS_VER_2_CHAR%WW_%%a /E /Y /I
)


REM PasswordCrisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JBCrisis_%%a.bin JBCN%BIOS_VER_2_CHAR%WW_%%a\PasswordCrisis\JBCN.bin
)

