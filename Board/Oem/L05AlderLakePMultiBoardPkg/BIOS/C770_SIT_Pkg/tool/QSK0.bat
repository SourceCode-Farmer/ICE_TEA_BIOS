@echo off

set FILE7Z_POSTFIX=QSK0
set PASSWORD=123456

ReleasePkgBIOSVer.exe ..\MEBIOS\ 1>nul
if not "0"=="%errorlevel%" (
echo error when run exe
goto end_error
)

for /f %%a in ('ReleasePkgBIOSVer.exe ..\MEBIOS\') do set "OUTPUTFOLDER=%%a"
set FILE7Z=%OUTPUTFOLDER%%FILE7Z_POSTFIX%

echo -------------------
echo %FILE7Z%
echo -------------------

cd ..
if exist %OUTPUTFOLDER% (
rd %OUTPUTFOLDER% /S /Q 1>nul
)
cd tool

7-Zip\7z.exe x ..\MEBIOS\%FILE7Z%.7z -o..\%OUTPUTFOLDER% -p%PASSWORD% 1>nul
goto end

:end_error
echo error here

:end