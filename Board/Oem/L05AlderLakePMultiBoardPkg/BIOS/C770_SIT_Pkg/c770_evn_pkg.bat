@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_J1.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=J1CN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=SQS,QSK0

REM Crisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1Crisis%%a.bin J1CN%BIOS_VER_2_CHAR%WW%%a\Crisis\J1CN.bin
)

REM Mfg
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1CNXXWW%%a_IA32_SWDL.exe    J1CN%BIOS_VER_2_CHAR%WW%%a\Mfg\%BIOS_VER%_IA32_SWDL.exe
copy /B ..\J1CNXXWW%%a.bin              J1CN%BIOS_VER_2_CHAR%WW%%a\Mfg\%BIOS_VER%_SMT.bin
copy /B ..\J1CNXXWW%%a1.bin             J1CN%BIOS_VER_2_CHAR%WW%%a\Mfg\%BIOS_VER%_SMT(part1")".bin
copy ..\J1CNXXWW%%a2.bin             J1CN%BIOS_VER_2_CHAR%WW%%a\Mfg\%BIOS_VER%_SMT(part2")".bin
copy ..\J1CNXXWW%%a_X64_SWDL.exe     J1CN%BIOS_VER_2_CHAR%WW%%a\Mfg\%BIOS_VER%_X64_SWDL.exe
)

REM Uefi64
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\Mocca_REL%%a.bin J1CN%BIOS_VER_2_CHAR%WW%%a\Uefi64\J1CN%BIOS_VER_2_CHAR%WW.fd
echo H2OFFT-Sx64.efi %BIOS_VER%.fd -all > J1CN%BIOS_VER_2_CHAR%WW%%a\Uefi64\flash.nsh
)

REM Windows
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1CNXXWW%%a_IA32_REL.exe J1CN%BIOS_VER_2_CHAR%WW%%a\Windows\J1CN%BIOS_VER_2_CHAR%WW.exe
)


REM checksum update
for %%a in (%BIOS_TYPE_LIST%) do (
if exist J1CN%BIOS_VER_2_CHAR%WW%%a (
cd J1CN%BIOS_VER_2_CHAR%WW%%a
copy ..\..\..\LfcBprInput\LfcReleaseNote_J1.txt ReleaseNote.txt
echo update checksum
call Checksum.bat
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Crisis\J1CN.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_IA32_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_SMT(part1).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_SMT(part2).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_SMT.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\J1CN%BIOS_VER_2_CHAR%WW_X64_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: PasswordCrisis\J1CN.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\H2OFFT-Sx64.efi"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\J1CN%BIOS_VER_2_CHAR%WW.fd"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Windows\J1CN%BIOS_VER_2_CHAR%WW.exe"
cd ..
)
)

REM pkg each type BIOS
for %%a in (%BIOS_TYPE_LIST%) do (
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a J1CN%BIOS_VER_2_CHAR%WW%%a.7z J1CN%BIOS_VER_2_CHAR%WW%%a\* -p123456 -mhe
)

REM move to release pkg
for %%a in (%BIOS_TYPE_LIST%) do (
move J1CN%BIOS_VER_2_CHAR%WW%%a.7z MEBIOS\
)

REM remove temp files
for %%a in (%BIOS_TYPE_LIST%) do (
rd J1CN%BIOS_VER_2_CHAR%WW%%a /S /Q
)

REM pkg release pkg
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a J1CN%BIOS_VER_2_CHAR%WW.7z MEBIOS tool unpackage.bat


cd MEBIOS
del *.* /q
cd ..