@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_J1.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=J1CN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=SQS,QSK0,QSR0

del J1CN%BIOS_VER_2_CHAR%WW.7z

REM init
for %%a in (%BIOS_TYPE_LIST%) do (
  if exist J1CN%BIOS_VER_2_CHAR%WW%%a (
  rd J1CN%BIOS_VER_2_CHAR%WW%%a /S /Q
  )
  xcopy J1CNXXWW J1CN%BIOS_VER_2_CHAR%WW%%a /E /Y /I
)


REM PasswordCrisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\J1Crisis%%a.bin J1CN%BIOS_VER_2_CHAR%WW%%a\PasswordCrisis\J1CN.bin
)

