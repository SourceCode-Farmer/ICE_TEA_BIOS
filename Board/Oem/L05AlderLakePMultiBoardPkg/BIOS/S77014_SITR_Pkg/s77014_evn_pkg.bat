@echo off
for /f %%a in ('get_bios_version_from_uni.exe ../../Project_JH.uni') do set "BIOS_VER_STR=%%a"

set BIOS_VER_2_CHAR=%BIOS_VER_STR:~4,2%
set BIOS_VER=JHCN%BIOS_VER_2_CHAR%WW
set BIOS_TYPE_LIST=QS,SQS
set QS_FOLDER=JHCN%BIOS_VER_2_CHAR%WW_QS


REM Crisis
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JHCrisis_%%a.bin JHCN%BIOS_VER_2_CHAR%WW_%%a\Crisis\JHCN.bin
)

REM Mfg
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JHCNXXWW_%%a_IA32_SWDL.exe    JHCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_IA32_SWDL.exe
copy /B ..\JHCNXXWW_%%a.bin              JHCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT.bin
copy /B ..\JHCNXXWW_%%a1.bin             JHCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT(part1")".bin
copy /B ..\JHCNXXWW_%%a2.bin             JHCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_SMT(part2")".bin
copy /B ..\JHCNXXWW_%%a_X64_SWDL.exe     JHCN%BIOS_VER_2_CHAR%WW_%%a\Mfg\%BIOS_VER%_X64_SWDL.exe
)

REM Uefi64
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\Mocca_REL_%%a.bin JHCN%BIOS_VER_2_CHAR%WW_%%a\Uefi64\JHCN%BIOS_VER_2_CHAR%WW.fd
echo H2OFFT-Sx64.efi %BIOS_VER%.fd -all > JHCN%BIOS_VER_2_CHAR%WW_%%a\Uefi64\flash.nsh
)

REM Windows
for %%a in (%BIOS_TYPE_LIST%) do (
copy /B ..\JHCNXXWW_%%a_IA32_REL.exe JHCN%BIOS_VER_2_CHAR%WW_%%a\Windows\JHCN%BIOS_VER_2_CHAR%WW.exe
)


REM checksum update
for %%a in (%BIOS_TYPE_LIST%) do (
cd JHCN%BIOS_VER_2_CHAR%WW_%%a
copy ..\..\..\LfcBprInput\LfcReleaseNote_JH.txt ReleaseNote.txt
echo update checksum
call Checksum.bat
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Crisis\JHCN.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JHCN%BIOS_VER_2_CHAR%WW_IA32_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JHCN%BIOS_VER_2_CHAR%WW_SMT(part1).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JHCN%BIOS_VER_2_CHAR%WW_SMT(part2).bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JHCN%BIOS_VER_2_CHAR%WW_SMT.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Mfg\JHCN%BIOS_VER_2_CHAR%WW_X64_SWDL.exe"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: PasswordCrisis\JHCN.bin"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\H2OFFT-Sx64.efi"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Uefi64\JHCN%BIOS_VER_2_CHAR%WW.fd"
call ..\checksum_merge_to_releasenote.exe checksum.txt ReleaseNote.txt "File: Windows\JHCN%BIOS_VER_2_CHAR%WW.exe"
cd ..
)

REM pkg each type BIOS
for %%a in (%BIOS_TYPE_LIST%) do (
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a JHCN%BIOS_VER_2_CHAR%WW_%%a.7z JHCN%BIOS_VER_2_CHAR%WW_%%a\* -p123456 -mhe
)

REM move to release pkg
for %%a in (%BIOS_TYPE_LIST%) do (
move JHCN%BIOS_VER_2_CHAR%WW_%%a.7z MEBIOS\
)

REM remove temp files
for %%a in (%BIOS_TYPE_LIST%) do (
rd JHCN%BIOS_VER_2_CHAR%WW_%%a /S /Q
)

REM pkg release pkg
..\..\LfcBpr\Tool\ToolCommon\7-Zip\7z.exe a JHCN%BIOS_VER_2_CHAR%WW.7z MEBIOS tool unpackage.bat


cd MEBIOS
REM del *.* /q
cd ..