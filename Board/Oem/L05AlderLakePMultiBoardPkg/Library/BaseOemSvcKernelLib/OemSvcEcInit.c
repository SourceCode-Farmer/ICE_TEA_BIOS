/** @file
  Provide hook function for OEM to initialize EC. 

;******************************************************************************
;* Copyright (c) 2013 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcKernelLib.h>
#include <Library/EcLib.h>

/**
  EC initialize.

  @retval EFI_UNSUPPORTED    Returns unsupported by default.
  @retval EFI_SUCCESS        The service is customized in the project.
  @retval EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed. 
**/
EFI_STATUS
OemSvcEcInit (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
//[-start-210721-QINGLIN0001-add]//
#ifdef LCFC_SUPPORT
  return EFI_UNSUPPORTED;
#else
  EFI_STATUS        Status;

  Status = SendEcCommand (EC_C_LAN_ON);
  Status = SendEcCommand (EC_C_ACPI_DISABLE);
  Status = SendEcCommand (EC_C_SMI_NOTIFY_DISABLE);
	
  return EFI_MEDIA_CHANGED;
#endif
//[-end-210721-QINGLIN0001-add]//
}
