/** @file
  Provide hook function for OEM to implement EC ACPI Mode. 

;******************************************************************************
;* Copyright (c) 2014 - 2017, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcKernelLib.h>
//[-start-210723-QINGLIN0001-modify]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#else
#include <Library/EcMiscLib.h>
#endif
//[-end-210519-KEBIN00001-modify]//

/**
  Platform specific function to enable/disable EC ACPI Mode

  @param[in]      Enable     Boolean to enable/disable ACPI mode.

  @retval EFI_UNSUPPORTED    Returns unsupported by default.
  @retval EFI_SUCCESS        The service is customized in the project.
  @retval EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed. 
**/
EFI_STATUS
OemSvcEcAcpiMode (
  IN  BOOLEAN         Enable
  )
{

//[-start-210723-QINGLIN0001-modify]//
#ifdef LCFC_SUPPORT
  EFI_STATUS Status;

  LfcCleanUpKbcAndEcBuffer();
  Status = LfcEcLibEcAcpiMode(Enable);
  
  return Status;
#else
  EnableEcAcpiMode (Enable);

  return EFI_UNSUPPORTED;
#endif
//[-end-210519-KEBIN00001-modify]//
}
