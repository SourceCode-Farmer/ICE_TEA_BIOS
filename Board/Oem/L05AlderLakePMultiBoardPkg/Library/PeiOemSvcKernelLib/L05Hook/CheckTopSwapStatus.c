/** @file
  Check Top Swap Status.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/FeatureLib/OemSvcNotifyEcToStopWdt.h>

/**
  Check Top Swap status and sync to IsRecovery flag.

  @param  *IsRecovery                   If service detects that the current system is recovery mode.
                                        This parameter will return TRUE, else return FALSE.

  @retval None
**/
VOID
CheckTopSwapStatus (
  IN OUT BOOLEAN                        *IsRecovery
  )
{
  DEBUG ((DEBUG_INFO, "%a Start\n", __FUNCTION__));

  if (!PcdGetBool (PcdL05TopSwapEnable)) {
    return;
  }

  //
  // System is now in Top Swap mode
  //
  OemSvcNotifyEcToStopWdt ();

}