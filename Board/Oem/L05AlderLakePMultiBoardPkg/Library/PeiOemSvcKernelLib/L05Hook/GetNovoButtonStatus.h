/** @file
  Get the Novo Button Status

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GET_NOVO_BUTTON_STATUS_
#define _GET_NOVO_BUTTON_STATUS_

EFI_STATUS
GetNovoButtonStatus (
  VOID
  );

#endif
