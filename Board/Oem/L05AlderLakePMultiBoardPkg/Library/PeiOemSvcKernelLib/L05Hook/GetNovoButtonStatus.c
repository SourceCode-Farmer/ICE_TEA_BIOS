/** @file
  Get the Novo Button Status

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiServicesLib.h>
#include <Library/FeatureLib/OemSvcGetNovoButtonStatus.h>
#include <Library/BaseMemoryLib.h>
#include <Guid/L05NovoKeyInfoHob.h>

/**
  Get Novo buton status and create hob.

  @param[in]  VOID

  @retval     EFI_SUCCESS               Get Novo button status and create hob successfully.
  @retval     Others                    Maybe something wrong or function was not implemented, please check it.
**/
EFI_STATUS
GetNovoButtonStatus (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_HOB_GUID_TYPE                     *NovoKeyInfoHob;

  Status = OemSvcGetNovoButtonStatus ();

  switch (Status) {

  case EFI_UNSUPPORTED:
    break;

  case EFI_MEDIA_CHANGED:
    //
    //  Novo Button is Pressed. Create HOB to pass this infomation to DXE phase.
    //
    Status = PeiServicesCreateHob (
               EFI_HOB_TYPE_GUID_EXTENSION,
               sizeof (EFI_HOB_GUID_TYPE),
               &NovoKeyInfoHob
               );

    if (!EFI_ERROR (Status)) {
      CopyMem (&NovoKeyInfoHob->Name, &gL05NovoKeyInfoHobGuid, sizeof (EFI_GUID));
    }

    break;
  }

  return Status;
}
