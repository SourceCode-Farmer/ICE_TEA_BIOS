/** @file
  Project dependent initial code in PlatformStage1.

;******************************************************************************
;* Copyright (c) 2012, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcKernelLib.h>
//_Start_L05_NOVO_BUTTON_MENU_
#include <L05Hook/GetNovoButtonStatus.h>
//_End_L05_NOVO_BUTTON_MENU_

/**
  Project dependent initial code in PlatformStage1.

  @param  Base on OEM design.

  @retval EFI_UNSUPPORTED    Returns unsupported by default.
  @retval EFI_SUCCESS        The service is customized in the project.
  @retval EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed. 
  @retval Others             Depends on customization.
**/
EFI_STATUS
OemSvcInitPlatformStage1 (
  VOID
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//_Start_L05_NOVO_BUTTON_MENU_
//[-start-220224-TAMT000046-modify]//
#ifndef S77013_SUPPORT
#ifdef L05_ALL_FEATURE_ENABLE
  GetNovoButtonStatus ();
#endif
#endif
//[-end-220224-TAMT000046-modify]//
//_End_L05_NOVO_BUTTON_MENU_

  return EFI_UNSUPPORTED;
}
