/** @file
  Provide OEM to modify the task when the recovery failed.

;******************************************************************************
;* Copyright (c) 2012, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcKernelLib.h>
//[-start-220118-BAIN000089-add]//
#ifdef LCFC_SUPPORT
#include <Ppi/Speaker.h>
#include <Library/PeiServicesLib.h>
#endif
//[-end-220118-BAIN000089-add]//
//[-start-220120-BAIN000091-add]//
#ifdef LCFC_SUPPORT
#include <Library/ResetSystemLib.h>  // ShutDown
#include <Library/LfcEcLib.h>
#endif
//[-end-220120-BAIN000091-add]//

/**
  This OemService call at the end of the Recovery Mode. 
  When flash recovery BIOS failed, this service will be called. 
  It provides OEM to modify the task when the recovery failed.
   
  @param  Based on OEM design.

  @retval EFI_UNSUPPORTED       Returns unsupported by default.
  @retval EFI_SUCCESS           The service is customized in the project.
  @retval Others                Base on OEM design.
**/
EFI_STATUS
OemSvcHookWhenRecoveryFail (
  VOID
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//[-start-220118-BAIN000089-add]//
#ifdef LCFC_SUPPORT
#ifdef L05_BIOS_SELF_HEALING_SUPPORT
  //
  // [Lenovo BIOS Self-Healing Design Guidance Specification v2.2]
  //   2.3 Notification Interface
  //     Crisis (Recovery Fail):
  //     When loading cisis recovery file fails, if cannot display, please make beep sound to remind user to do crisis.
  //     BIOS should shutdown the unit and wait for user to do crisis.
  //

  //
  // Todo:
  //   Add project specific code in here.
  //
  EFI_STATUS              Status = EFI_SUCCESS;
  BOOLEAN                 SpeakerOn = FALSE;
  PEI_SPEAKER_IF_PPI      *SpeakerPPI;

  Status = PeiServicesLocatePpi (
             &gPeiSpeakerInterfacePpiGuid,
             0,
             NULL,
             (VOID **)&SpeakerPPI
             );
  if (!EFI_ERROR (Status)) {
    SpeakerOn = TRUE;
  }
// When FN+R Press don't  generate Beep if can't get the Crisis bin.
  if((SpeakerOn == TRUE)&&(!PcdGetBool(PcdL05BiosRecoveryHotkeyFlag))){
    SpeakerPPI->GenerateBeep (1, 500000, 0);
  }
#endif
#endif
//[-end-220118-BAIN000089-add]//
//[-start-220120-BAIN000091-add]//
#ifdef LCFC_SUPPORT
  LfcEcLibClearCmosForSelfHealing();
  ResetShutdown();
  CpuDeadLoop ();
#endif
//[-end-220120-BAIN000091-add]//

  return EFI_UNSUPPORTED;
}
