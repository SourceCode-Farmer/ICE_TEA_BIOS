/** @file
  Get board ID.

;******************************************************************************
;* Copyright (c) 2015 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcKernelLib.h>
#include <Library/EcMiscLib.h>
//[-start-190606-IB16990031-add]//
#include <Library/TimerLib.h>
#include <Library/PmcLib.h>
//[-end-190606-IB16990031-add]//
#include <Library/DebugLib.h>



/**
  Get board ID.

  @param[in, out]  *BoardId           A pointer to board ID.

  @retval          EFI_UNSUPPORTED    Returns unsupported by default.
  @retval          EFI_SUCCESS        The service is customized in the project.
  @retval          EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed.
  @retval          Others             Depends on customization.
**/
EFI_STATUS
OemSvcGetBoardId (
  IN OUT H2O_BOARD_ID                         *BoardId
  )
{
//[-start-210519-KEBIN00001-modify]
#if 1
  *BoardId = (H2O_BOARD_ID) PcdGet16 (PcdBoardId);
#else
  BOARD_ID_INFO                               EcBoardInfo;
//[-start-190606-IB16990031-add]//
  PMC_SLEEP_STATE                             SleepType;

  if (FeaturePcdGet(PcdUseCrbEcFlag)) {
    SleepType = PmcGetSleepTypeAfterWake ();

    if (SleepType == PmcS3SleepState) {
      MicroSecondDelay (200000);  // Delay 200ms to wait CRB EC ready.
    }
  }
//[-end-190606-IB16990031-add]//


  if (!PcdGetBool(PcdCrbBoard)) {
    //
    // Project customization:
    // Project could return the Board ID here
    // The value will be set to SkuId & PcdH2OBoardId
    // The value should be declared in both [SkuIds] and SKUID_IDENTIFIER of Project.dsc.
    //
    DEBUG ((DEBUG_INFO, "[OemSvcGetBoardId] PcdCrbBoard: %x \n", FeaturePcdGet(PcdCrbBoard)));
    DEBUG ((DEBUG_INFO, "Project could return the Board ID here\n"));
    DEBUG ((DEBUG_INFO, "CrbSkuId: 0x%x\n", PcdGet16 (PcdCrbSkuId)));
    DEBUG ((DEBUG_INFO, "PcdBoardId: 0x%x\n", PcdGet16 (PcdBoardId)));
  } else {
    GetBoardInfo ((UINT16 *)&EcBoardInfo);
//[-start-201222-IB09480120-modify]//
//[-start-200420-IB17800056-modify]//
    //
    // ADL PO modify . ADL use TglRvpFields.
    //
    *BoardId = (H2O_BOARD_ID) EcBoardInfo.TglRvpFields.BoardId;
    *BoardId = (UINTN) (EcBoardInfo.TglRvpFields.BomId << 16) | (EcBoardInfo.TglRvpFields.FabId << 8) | (EcBoardInfo.TglRvpFields.BoardId);
//[-end-200420-IB17800056-modify]//
//[-end-201222-IB09480120-modify]//
    DEBUG ((DEBUG_INFO, "GetBoardInfo BoardId 0x%x\n", *BoardId));
  }
#endif
//[-end-210519-KEBIN00001-modify]//
  return EFI_MEDIA_CHANGED;
}

