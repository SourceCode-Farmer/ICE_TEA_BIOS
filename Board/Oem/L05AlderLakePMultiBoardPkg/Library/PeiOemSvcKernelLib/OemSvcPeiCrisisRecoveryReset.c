/** @file
  Provide OEM to reset system after crisis recovery complete.

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcKernelLib.h>
#include <Library/CmosLib.h>
#include <Library/IoLib.h>
#include <Library/MmPciLib.h>
#include <ChipsetCmos.h>
#include "Register/PchRegsLpc.h"
#include "Register/PmcRegs.h"
#include <Library/PchCycleDecodingLib.h>
#include <Library/PmcLib.h>
//_Start_L05_FEATURE_
#include <Library/CmosLib.h>
#include <OemCmos.h>
//_End_L05_FEATURE_

/**
  Provide OEM to reset system after crisis recovery complete.

  @retval EFI_UNSUPPORTED    Returns unsupported by default.
  @retval EFI_SUCCESS        The service is customized in the project.
  @retval EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed.
  @retval Others             Depends on customization.
**/
EFI_STATUS
OemSvcPeiCrisisRecoveryReset (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
  UINTN     BaseAddress;

  if (RECOVERY_VALUE == ReadExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, ChipsetRecoveryFlag)) {
    WriteExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, ChipsetRecoveryFlag, 0);
  }

//[-start-211117-BAIN000059-remove]//
#ifndef LCFC_SUPPORT
//_Start_L05_FEATURE_
#ifdef L05_CRISIS_ENABLE
  //
  // BUG!! BUG!! BUG!!
  // This is for Lenovo INTERNAL NovoButton and CrisisRecovery test.
  // Project need to remove this code.
  // 0x01 = Crisis Recovery.
  // 0x02 = Novo button pressed.
  //
  if (ReadCmos8 (EFI_L05_NOVO_BUTTON_CRISIS_TEST) == (UINT8)0x01) {
    WriteCmos8 (EFI_L05_NOVO_BUTTON_CRISIS_TEST, (UINT8)0x0);
  }
#endif
//_End_L05_FEATURE_
#endif
//[-end-211117-BAIN000059-remove]//

  ///
  /// PCH BIOS Spec Section 4.6 GPIO Reset Requirement
  ///
  BaseAddress = PmcGetPwrmBase ();

  MmioOr32 (BaseAddress + R_PMC_PWRM_ETR3, (UINT32) (B_PMC_PWRM_ETR3_CF9GR));

  IoWrite8 ((UINTN) R_PCH_IO_RST_CNT, V_PCH_IO_RST_CNT_FULLRESET);
  ///
  /// Waiting for system reset
  ///
  CpuDeadLoop ();

  return EFI_SUCCESS;
}
