/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _SMM_DXE_OEM_SVC_SECURITY_PASSWORD_LIB_H_
#define _SMM_DXE_OEM_SVC_SECURITY_PASSWORD_LIB_H_

#include <FlashRegionLayout.h>
#include <Library/FeatureLib/OemSvcSecurityPassword.h>
#include <Library/UefiLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/RngLib.h>
#include <Library/DebugLib.h>
#include <Protocol/L05Variable.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/SmmBase2.h>
#include <Guid/L05VariableTool.h>

#define L05_SECURITY_PASSWORD_ENCODE_SIGNATURE  SIGNATURE_32('L','S','P','E')
#define GET_RANDOM_NUMBER_RETRY_LIMIT           100

#pragma pack (1)
typedef struct {
  UINT32                                Signature;
  UINT8                                 RandomNumber;
  UINT16                                Checksum;
} L05_SECURITY_PASSWORD_ENCODE_HEADER;
#pragma pack ()

BOOLEAN
L05IsInSmm (
  VOID
  );

EFI_STATUS
FlashBiosRegion (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb,
  UINTN                                 Offset,
  UINTN                                 NumBytes,
  UINT8                                 *Buffer
  );

EFI_STATUS
GetSystemPasswordRegion (
  UINTN                                 *L05SystemSupervisorPasswordRegionBase,
  UINTN                                 *L05SystemSupervisorPasswordRegionSize,
  UINTN                                 *L05SystemUserPasswordRegionBase,
  UINTN                                 *L05SystemUserPasswordRegionSize
  );

EFI_STATUS
GetSystemPasswordArea (
  UINT8                                 **L05SystemPasswordArea,
  UINTN                                 *L05SystemPasswordSize
  );

BOOLEAN
IsRandomNumberValid (
  VOID
  );

EFI_STATUS
SecurityPasswordEncode (
  IN  UINT8                             *BufferPtr,
  IN  UINTN                             BufferSize
  );

EFI_STATUS
SecurityPasswordInit (
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwb
  );

extern EFI_SMM_SYSTEM_TABLE2            *mSmst;

#endif