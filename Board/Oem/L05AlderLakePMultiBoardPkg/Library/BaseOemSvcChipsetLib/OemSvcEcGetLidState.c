/** @file
  Provide hook function for OEM to get lid state from EC.

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/DebugLib.h>
#include <Library/EcMiscLib.h>
#include <PlatformBoardId.h>
#include <Library/CmosLib.h>
#include <ChipsetCmos.h>
//[-start-211020-KEBIN00060-modify]// 
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-211020-KEBIN00060-modify]// 

typedef enum {
  LidClosed,
  LidOpen,
  LidStatusMax
} LID_STATUS;

/**
 Get Lid state from EC.

 @param[out]        EcGetLidState       The status of get Lid.
 @param[out]        LidIsOpen           TRUE: Lid is open; FALSE: Lid is close.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEcGetLidState (
  OUT EFI_STATUS   *EcGetLidState,
  OUT UINT8        *LidIsOpen
  )
{
//[-start-210519-KEBIN00001-modify]//
//[-start-211020-KEBIN00060-modify]//
#ifdef LCFC_SUPPORT
//  EFI_STATUS       Status;
  UINT8            Data;
  *EcGetLidState = EFI_SUCCESS;
//  Status = LfcEcLibEcRamRead (0xB8, &Data);    // get LID status
 Data = MmioRead8 (0xFE0B04B8);  //EC RAM4 0xB8 bit 1

//  if (EFI_ERROR(Status)) {
//  return Status;
//  }
  if(Data&0x02)    //EC RAM4 0xB8 bit 1
  *LidIsOpen = LidOpen;
  else
  *LidIsOpen = LidClosed;

//[-start-220307-QINGLIN0164-add]//
#if defined LCFC_SUPPORT
  *LidIsOpen = LidOpen;
#endif
//[-end-220307-QINGLIN0164-add]//

  return EFI_MEDIA_CHANGED;
//[-end-211020-KEBIN00060-modify]//  
#else
  UINT8                   PortDataOut;
  UINT8                   DataBuffer[1];

  //
  // If the platform does not support a lid, the function must return EFI_UNSUPPORTED
  //
  if (PcdGet8 (PcdPlatformType) == TypeTrad && PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) {
    DEBUG ((DEBUG_INFO, "Returning Lid status as unsupported to GOP for DT/AIO board\n"));
    return EFI_UNSUPPORTED;
  }

  if (PcdGetBool (PcdEcPresent)) {
    if (FeaturePcdGet (PcdUseCrbEcFlag)) {
      DataBuffer[0] = EC_D_LID_STATE;
      *EcGetLidState = ReadEcRam (DataBuffer);
      if (*EcGetLidState == EFI_SUCCESS) {
        PortDataOut = DataBuffer[0];
        if ((PortDataOut & EC_B_LID_STATUS_OPEN) == EC_B_LID_STATUS_OPEN) {
          *LidIsOpen = LidOpen;
        } else {
          *LidIsOpen = LidClosed;
        }
        return EFI_MEDIA_CHANGED;
      }
    }
  }

  return EFI_UNSUPPORTED;
#endif
//[-start-210519-KEBIN00001-modify]//
}
