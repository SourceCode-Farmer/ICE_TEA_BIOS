/** @file
  Provide hook function for OEM to Send Critical thermal to EC. 
  
;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/EcMiscLib.h>

/**
 Send Critical thermal to EC.
 
 @param[in]         CriticalThermal     The temperature of Critical Thermal.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEcSetCriticalThermal (
  IN UINT8  Temperature
  )
{
//[-start-210519-KEBIN00001-modify]//
#ifdef LCFC_SUPPORT
  return EFI_UNSUPPORTED;
#else
  EFI_STATUS Status;

  //
  // Get the Critical shutdown temperature and pass it to the EC so that in ACPI mode the OS will shut the system down.
  //
  Status = SetEcCriticalShutdownTemperature (Temperature);

  return EFI_MEDIA_CHANGED;
#endif
//[-end-210519-KEBIN00001-modify]//
}
