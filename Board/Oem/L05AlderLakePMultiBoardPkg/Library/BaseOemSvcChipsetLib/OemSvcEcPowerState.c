/** @file
  Provide hook function for OEM to implement EC power state detection. 
  
;******************************************************************************
;* Copyright (c) 2014 - 2017, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/EcMiscLib.h>

/**
 Get power state from EC.  If power state cannot be determined,
 battery powered is assumed.
 
 @param[in, out]    PowerState          A boolean pointer. TRUE means AC power; FALSE means Battery power.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEcPowerState (
  IN OUT  BOOLEAN                       *PowerState
  )
{

//[-start-210519-KEBIN00001-modify]//
#ifdef LCFC_SUPPORT
  *PowerState = TRUE;
#else
  *PowerState = PowerStateIsAc ();
#endif
//[-end-210519-KEBIN00001-modify]//
  return EFI_MEDIA_CHANGED;
}
