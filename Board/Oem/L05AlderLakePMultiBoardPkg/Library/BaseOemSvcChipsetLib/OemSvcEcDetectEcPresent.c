/** @file
  Provide hook function for OEM to detect EC Present. 
  
;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/EcLib.h>
#include <Library/EcMiscLib.h>
#include <Library/TimerLib.h>

#define STALL_TIME                  1000000 // 1,000,000 microseconds = 1 second

/**
 Detect EC Present.
 
 @param[out]        *CommandStatus      Command status.
 @param[out]        Present             TRUE: EC is presnet; FALSE: EC is absent.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEcDetectEcPresent (
  OUT EFI_STATUS   *CommandStatus,
  OUT BOOLEAN      *Present
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//[-start-210519-KEBIN00001-modify]//
#ifdef LCFC_SUPPORT
  *CommandStatus = EFI_UNSUPPORTED;

  *Present = FALSE;
  return EFI_UNSUPPORTED;
#else
  RETURN_STATUS   Status;
  UINT8           Retry;
  UINT8           DataBuffer[2];

  //
  // Detect EC Revision
  //
  Retry = 5;
  do {
    Status = DetectEcRevision ((UINT8 *)DataBuffer);
    Retry--;
    if ((RETURN_ERROR (Status)) && (Retry != 0)) {
      MicroSecondDelay (STALL_TIME);
    }
  } while ((RETURN_ERROR (Status)) && (Retry != 0));

  if (RETURN_ERROR (Status)) {
    *Present = FALSE;
  } else {
    *Present = TRUE;
  }

  *CommandStatus = Status;

  return EFI_MEDIA_CHANGED;
#endif
//[-start-210519-KEBIN00001-modify]//
}
