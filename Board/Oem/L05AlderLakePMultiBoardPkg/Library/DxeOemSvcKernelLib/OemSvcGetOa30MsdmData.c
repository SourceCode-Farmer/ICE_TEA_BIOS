/** @file
  Provides an opportunity for OEM to update the MSDM Data.

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcKernelLib.h>
//_Start_L05_SPECIFIC_VARIABLE_SERVICE_
#include "L05Hook/GetOA3MsdmSpecificVariableServiceData.h"
//_End_L05_SPECIFIC_VARIABLE_SERVICE_

//[-start-210623-YUNLEI0104-add]//
#ifdef LCFC_SUPPORT
#include <Protocol/LenovoVariable.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Base.h>
#endif
//[-end-210623-YUNLEI0104-add]//

/**
  This OemService provides an opportunity for OEM to update the MSDM Data.
  The returned data is used to build the ACPI MSDM table.
  The service follows the OA specification 3.0.

  @param[in, out]  MsdmData              The MSDM Data will be copied to this address if OEM updates MSDM Data here.

  @retval          EFI_UNSUPPORTED       Returns unsupported by default.
  @retval          EFI_SUCCESS           Get OA3.0 information failed.
  @retval          EFI_MEDIA_CHANGED     Get OA3.0 information success.
  @retval          Others                Base on OEM design.
**/
EFI_STATUS
OemSvcGetOa30MsdmData (
  IN OUT EFI_ACPI_MSDM_DATA_STRUCTURE          *MsdmData
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//[-start-210623-YUNLEI0104-modify]//
#ifdef LCFC_SUPPORT
  LENOVO_VARIABLE_PROTOCOL                *LenovoVariable = NULL;
  EFI_GUID                                MsdmGuid = LVAR_OA3_MSDM_DATA_GUID;
  EFI_STATUS                              Status;
  UINT32                                  DataSize = sizeof (EFI_ACPI_MSDM_DATA_STRUCTURE);
  EFI_ACPI_DESCRIPTION_HEADER             *MsdmAddress;
  
  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable);
  ASSERT_EFI_ERROR (Status);

  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &MsdmGuid,
                             &DataSize,
                             MsdmData);
  if (!EFI_ERROR (Status)) {
    return EFI_MEDIA_CHANGED;
  } else {
    MsdmAddress = (EFI_ACPI_DESCRIPTION_HEADER *)((UINTN)(MsdmData) -  sizeof (EFI_ACPI_DESCRIPTION_HEADER));
    MsdmAddress->Signature = SIGNATURE_32 ('P','O','A','T');

    //Set these to non-00 and non-ff to make table built
    MsdmData->MsdmVersion = 1;
    MsdmData->MdsmDataType = 1;
    MsdmData->MsdmDataLength = 1;
    return EFI_MEDIA_CHANGED;
  }
#else
//_Start_L05_SPECIFIC_VARIABLE_SERVICE_
#ifndef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE

  return EFI_UNSUPPORTED;

#else

  GetOA3MsdmSpecificVariableServiceData (MsdmData);

  return EFI_MEDIA_CHANGED;
#endif
#endif
//[-end-210623-YUNLEI0104-modify]//
//_End_L05_SPECIFIC_VARIABLE_SERVICE_
}
