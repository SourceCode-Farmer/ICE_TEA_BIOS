/** @file
  Provide OEM to define the post key corresponding to the behavior of utility choosing (SCU or Boot Manager).

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PostKey.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DxeOemSvcKernelLib.h>
//_Start_L05_NOVO_BUTTON_MENU_
#include <PiPei.h>
#include <Library/HobLib.h>
#include <Guid/L05NovoKeyInfoHob.h>
//_End_L05_NOVO_BUTTON_MENU_

//
// These must reflect order in KEY_ELEMENT
//
#define F1_KEY_BIT       0x01
#define F2_KEY_BIT       0x02
#define DEL_KEY_BIT      0x04
#define F10_KEY_BIT      0x08
#define F12_KEY_BIT      0x10
#define ESC_KEY_BIT      0x20
#define UP_ARROW_KEY_BIT 0x40
#define F3_KEY_BIT       0x80
#define F9_KEY_BIT       0x100
#ifdef L05_SMB_BIOS_ENABLE
#define F11_KEY_BIT      0x200
//_Start_L05_INTERRUPT_MENU_
#define ENTER_KEY_BIT    0x400
//_End_L05_INTERRUPT_MENU_
#endif
//
// module variables
//
//_Start_L05_HOTKEY_
//SCAN_TO_OPERATION  *mPostKeyToOperation = NULL;
//UINTN              mPostOperationCount = 0;
//_End_L05_HOTKEY_

//_Start_L05_HOTKEY_
//
//  Add F2, F12 function key back.
//
SCAN_TO_OPERATION                       mPostKeyToOperation[] = {
#ifndef L05_SMB_BIOS_ENABLE
  {F2_KEY_BIT,    SCAN_NULL, L05_SETUP_HOT_KEY              },
#else
  //
  // [Lenovo SMB BIOS Special Requirements V1.1]
  //   2.2 Hotkey Definition
  //     Enter BIOS Setup   - F1
  //     Enter Diagnostical - F10
  //     Enter Recovery     - F11
  //
  {F1_KEY_BIT,    SCAN_NULL,  L05_SETUP_HOT_KEY             },
#ifdef L05_DIAGNOSTICS_SUPPORT
  {F10_KEY_BIT,   SCAN_NULL,  L05_DIAGNOSTICAL_HOT_KEY      },
#endif
  {F11_KEY_BIT,   SCAN_NULL,  L05_RECOVERY_HOT_KEY          },
//_Start_L05_INTERRUPT_MENU_
    //
    // [Lenovo SMB BIOS Special Requirements V1.1]
    //   2.2 Hotkey Definition
    //     Startup Interrupt Menu - Enter
    //
  {ENTER_KEY_BIT, SCAN_NULL, L05_STARTUP_INTERRUPT_MENU_KEY },
//_End_L05_INTERRUPT_MENU_
#endif
//[-start-211213-Dongxu0035-add]//
//[-start-211216-Dennis0012-modify]//
//[-start-211217-OWENWU0031-modify]//
//[-start-211222-DABING0028-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
#ifdef L05_DIAGNOSTICS_SUPPORT
    {F10_KEY_BIT,   SCAN_NULL,  L05_DIAGNOSTICAL_HOT_KEY      },
#endif
#endif
//[-end-211222-DABING0028-modify]//    
//[-end-211217-OWENWU0031-modify]//
//[-end-211216-Dennis0012-modify]//
//[-end-211213-Dongxu0035-add]//
  {F12_KEY_BIT,   SCAN_NULL, L05_BOOT_MANAGER_HOT_KEY       },
  {0,             SCAN_NULL, NO_OPERATION                   }
};

UINTN                                   mPostOperationCount = (sizeof(mPostKeyToOperation) / sizeof(SCAN_TO_OPERATION));
//_End_L05_HOTKEY_

//
// helper function prototypes
//
VOID
InitialPostkeyTable (
  VOID
  );

//_Start_L05_HOTKEY_
///**
//  Initialize module variable mPostKeyToOperation.
//
//  @param                        None
//
//  @retval                       None
//**/
//VOID
//InitialPostkeyTable (
//  VOID
//  )
//{
//  UINTN         Index;
//  UINTN         OldPostOperationCount;
//
//  Index = 0;
//
//  if (mPostKeyToOperation != NULL) {
//    return;
//  }
//
//  //
//  // Add the end of table.
//  //
//  OldPostOperationCount = mPostOperationCount;
//  mPostOperationCount++;
//  mPostKeyToOperation = ReallocatePool (
//                          OldPostOperationCount*sizeof(SCAN_TO_OPERATION),
//                          sizeof(SCAN_TO_OPERATION)*mPostOperationCount,
//                          mPostKeyToOperation
//                          );
//  if (mPostKeyToOperation != NULL) {
//    mPostKeyToOperation[Index].KeyBit = 0;
//    mPostKeyToOperation[Index].ScanCode = SCAN_NULL;
//    mPostKeyToOperation[Index].PostOperation = NO_OPERATION;
//    Index++;
//  }
//}
//_End_L05_HOTKEY_

/**
  This OemService provides OEM to define the post key corresponding to the behavior of utility choosing (SCU or Boot Manager).

  @param[in]   KeyDetected           A bit map of the monitored keys found.
                                     Bit N corresponds to KeyList[N] as provided by the
                                     GetUsbPlatformOptions () API of UsbLegacy protocol.
  @param[in]   ScanCode              The Scan Code.
  @param[out]  *PostOperation        Point to the operation flag which imply the behavior of utility choosing in post time. For example: choose the SCU or Boot Manager.

  @retval      EFI_MEDIA_CHANGED     Get post operation success.
  @retval      EFI_SUCCESS           Get post operation failed.
  @retval      Others                Base on OEM design.
**/
EFI_STATUS
OemSvcInstallPostKeyTable (
  IN  UINTN                          KeyDetected,
  IN  UINT16                         ScanCode,
  OUT UINTN                          *PostOperation
  )
{
  UINTN                                 Index;
  UINTN                                 PostOperationCount;
  SCAN_TO_OPERATION                     *PostKeyToOperation;

//_Start_L05_HOTKEY_
//  InitialPostkeyTable ();
//_End_L05_HOTKEY_

//_Start_L05_NOVO_BUTTON_MENU_
{
  VOID                                  *Hoblist;
  UINT8                                 *Raw;

  //
  // Search the Novo Key Info Hob
  //
  Hoblist = GetHobList ();
  Raw = GetNextGuidHob (&gL05NovoKeyInfoHobGuid, Hoblist);
  if (Raw != NULL) {
    return EFI_NOT_FOUND;
  }
}
//_End_L05_NOVO_BUTTON_MENU_

  PostOperationCount = mPostOperationCount;
  PostKeyToOperation = mPostKeyToOperation;

  for (Index = 0; Index < PostOperationCount; Index++) {
    //
    // Search table
    //
    if ((KeyDetected & PostKeyToOperation[Index].KeyBit) == PostKeyToOperation[Index].KeyBit &&
         ScanCode == PostKeyToOperation[Index].ScanCode){
      *PostOperation = (UINTN)PostKeyToOperation[Index].PostOperation;
      if (PostKeyToOperation[Index].PostOperation == NO_OPERATION) {
        break;
      }

      return EFI_MEDIA_CHANGED;
    } else if (KeyDetected == 0xffff && ScanCode == PostKeyToOperation[Index].ScanCode) {
      //
      // Pure EFI enviorment, no monitor key driver
      //
      *PostOperation = (UINTN)PostKeyToOperation[Index].PostOperation;
      if (PostKeyToOperation[Index].PostOperation == NO_OPERATION) {
        break;
      }

      return EFI_MEDIA_CHANGED;
    }
  }

  return EFI_SUCCESS;
}
