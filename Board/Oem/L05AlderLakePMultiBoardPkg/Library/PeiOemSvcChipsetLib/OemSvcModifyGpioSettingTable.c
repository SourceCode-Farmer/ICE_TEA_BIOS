/** @file
    This function offers an interface to dynamically modify GPIO table.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <PlatformGpioConfig.h>
//[-start-210726-YUNLEI0115-modify]//
#ifdef C770_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-210726-YUNLEI0115-modify]//

//[-start-210519-KEBIN00001-modify]//
//[-start-210527-KEBIN00006-modify]//
#ifdef C970_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC970_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CS#
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BoardID 7
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//SSD_DET_N
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BT ON/OFF
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC1
{GPIO_VER2_LP_GPP_A15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC2
{GPIO_VER2_LP_GPP_A16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//AMP_SPK_ID
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C0_SDA
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C0_SCL
{GPIO_VER2_LP_GPP_B7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SDA
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SCL
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//ISH_EC INT
{GPIO_VER2_LP_GPP_B16, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SDA
{GPIO_VER2_LP_GPP_B17, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SCL
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210702-DABING0001-modify]//
//{GPIO_VER2_LP_GPP_B23, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_ALERT_N
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML1_ALERT_N
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
//{GPIO_VER2_LP_GPP_C0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBDATA#
//{GPIO_VER2_LP_GPP_C2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBDATA#
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_SDA_R
//{GPIO_VER2_LP_GPP_C4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_SDA_R
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_CLK_R
//{GPIO_VER2_LP_GPP_C5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//[-end-210702-DABING0001-modify]//
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_D0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ACCEL1_INT_N
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ACCEL2_INT_N
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ISH_ALS_INT
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TOF_INT_N
{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0CLK(TBTB)
//[-start-211028-KEBIN00066-modify]// 
//{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0DATA(TBTB)
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCIE WLAN CLK req, not used in C970
//[-end-211028-KEBIN00066-modify]//
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_TXD
{GPIO_VER2_LP_GPP_D10, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_RXD
{GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D12, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX3_RXD
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Reset
{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210729-Kebin000040-modify]//
//{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
//{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
//{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210729-Kebin000040-modify]//
{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DEVSLP(Reserved??
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TBT FORCE POWER
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//REESEVERD
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //REESEVERD
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
//[-start-211022-FLINT00025-remove]//
//FOR DCI ENABLE
//{GPIO_VER2_LP_GPP_E18, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_TXD
//{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
//{GPIO_VER2_LP_GPP_E20, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX1_TXD
//{GPIO_VER2_LP_GPP_E21, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX1_RXD
//FOR DCI ENABLE
//[-end-211022-FLINT00025-remove]//
{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

//[-end-201214-MARY000011-modify]//
// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15
//[-start-210729-Kebin000040-modify]//
//{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
//[-start-210729-Kebin000040-modify]//
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//For ITS
{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},// CPU_C10_GATE_N
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210928-BAOBO0003-modify]//
//Note: These GPIO setting have been configured in the PeiInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
//Note: These GPIO setting have been configured in the PeiInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-end-210928-BAOBO0003-modify]//
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4
{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_CLK0
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_DAT0
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW#
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT_R
//Note: D2 Error Native1
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCH_LAN_WAKE#(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT#_R
//Note: D4 Error Gpiomode
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S3#
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S4#
{GPIO_VER2_LP_GPD6,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_A#(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BB_TBT_PERST#(Reserved)
//Note: D8 Error Gpiomode
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PM_SLP_WLAN#(Reserved)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_PM_SLP_S5#(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
};
#endif
//[-end-210519-KEBIN00001-modify]//

//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT
//[-start-210726-YUNLEI0115-modify]//
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_14_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CS#
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BoardID 7 set NC
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BT ON/OFF
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC1
{GPIO_VER2_LP_GPP_A15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC2
{GPIO_VER2_LP_GPP_A16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone,    GpioPadLock}},//CPU HDMI HPD 
{GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_B7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SDA
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SCL
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//ISH_EC INT
{GPIO_VER2_LP_GPP_B16, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SDA
{GPIO_VER2_LP_GPP_B17, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SCL
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210702-DABING0001-modify]//
//{GPIO_VER2_LP_GPP_B23, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_ALERT_N
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML1_ALERT_N
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
//{GPIO_VER2_LP_GPP_C0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBDATA#
//{GPIO_VER2_LP_GPP_C2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBDATA#
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_SDA_R
//{GPIO_VER2_LP_GPP_C4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_SDA_R
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_CLK_R
//{GPIO_VER2_LP_GPP_C5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//[-end-210702-DABING0001-modify]//
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_D0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ACCEL1_INT_N
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ACCEL2_INT_N
//[-start-210924-YUNLEI0139-modify]//
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210924-YUNLEI0139-modify]//
//[-start-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 

{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0CLK(TBTB)
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0DATA(TBTB)
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault, GpioTermDefault,  GpioPadLock}},//CR CLKREQ2N
{GPIO_VER2_LP_GPP_D8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_TXD
{GPIO_VER2_LP_GPP_D10, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_RXD
{GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D12, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX3_RXD
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Reset
{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC modyl
//[-start-210922-YUNLEI0137-modify]//
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
//[-end-210922-YUNLEI0137-modify]//
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DEVSLP(Reserved??
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TBT FORCE POWER
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//REESEVERD
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //REESEVERD
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
//[-start-211027-JAYAN00003-modify]//
//FOR DCI ENABLE
//{GPIO_VER2_LP_GPP_E18, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_TXD{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
//{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
//FOR DCI ENABLE
//[-end-211027-JAYAN00003-modify]//
{GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//NC

{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU SD RSTN (Reserve) set as NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU SD CDN (Reserve) set as NC

//[-end-201214-MARY000011-modify]//
// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14 set NC
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15 set NC
{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//For ITS
{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU SD EN (Reserve) set as NC
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone, GpioPadLock}},//CPU HDMI DDC CLK
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone, GpioPadLock}},//CPU HDMI DDC DAT 
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},// CPU_C10_GATE_N
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[start-210923-STORM1115-modify]
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//[-start-211009-YUNLEI0140-remove]//
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//[-end-211009-YUNLEI0140-remove]//
//[end-210923-STORM1115-modify]
{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_F4_LED_N (Reserve) set as NC
//[start-210923-STORM1115-modify]
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-start-211009-YUNLEI0140-remove]//
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-end-211009-YUNLEI0140-remove]//
//[end-210923-STORM1115-modify]
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//SD WAKE N (Reserve) set as NC
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
//{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1 set NC
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2 set NC
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_CLK0
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_DAT0
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW#
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT_R
//Note: D2 Error Native1
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCH_LAN_WAKE#(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT#_R
//Note: D4 Error Gpiomode
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S3#
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S4#
{GPIO_VER2_LP_GPD6,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_A#(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BB_TBT_PERST#(Reserved)
//Note: D8 Error Gpiomode
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PM_SLP_WLAN#(Reserved)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_PM_SLP_S5#(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
};

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_16_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CS#
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_SAMC_SA_VR_PG(Reserve) set as NC
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BoardID 7 set NC
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BT ON/OFF
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC1
//Note: This GPIO setting has been configured in other file, so remove it.
//{GPIO_VER2_LP_GPP_A15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PEG_RST_N
{GPIO_VER2_LP_GPP_A16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[start-210731-STORM1101-modify]//
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_PWRGD(Reserve) set as NC
//[end-210731-STORM1101-modify]//
{GPIO_VER2_LP_GPP_A18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone,    GpioPadLock}},//CPU HDMI HPD
{GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[start-210731-STORM1101-modify]//
//Note: This GPIO setting has been configured in other file, so remove it.
//{GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPU_PCIE_WAKE_N notdo
//[end-210731-STORM1101-modify]//
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_B7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SDA
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SCL
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//ISH_EC INT
{GPIO_VER2_LP_GPP_B16, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SDA
{GPIO_VER2_LP_GPP_B17, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SCL
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210702-DABING0001-modify]//
//{GPIO_VER2_LP_GPP_B23, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_ALERT_N
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML1_ALERT_N
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
//{GPIO_VER2_LP_GPP_C0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBDATA#
//{GPIO_VER2_LP_GPP_C2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBDATA#
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_SDA_R
//{GPIO_VER2_LP_GPP_C4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_SDA_R
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_CLK_R
//{GPIO_VER2_LP_GPP_C5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//[-end-210702-DABING0001-modify]//
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_D0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ACCEL1_INT_N
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ACCEL2_INT_N
//[-start-210924-YUNLEI0139-modify]//
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210924-YUNLEI0139-modify]//
//[-start-210916-YUNLEI0125-modify]//
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-end-210916-YUNLEI0125-modify]//
//[-start-210923-YUNLEI0138-modify]//
//[-start-211009-YUNLEI0140-remove]//
//{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU NUM_LED N 
//[-end-211009-YUNLEI0140-remove]//
//[-end-210923-YUNLEI0138-modify]//

{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0CLK(TBTB)
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0DATA(TBTB)
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault, GpioTermDefault,  GpioPadLock}},//CR CLKREQ2N
//Note: This GPIO setting has been configured in other file, so remove it.
//{GPIO_VER2_LP_GPP_D8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ3_N
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_TXD
{GPIO_VER2_LP_GPP_D10, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_RXD
{GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D12, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX3_RXD
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Reset
{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_CORE_SAMC_SA_VR_PG (Reserve) set as NC
//[-start-210922-YUNLEI0137-modify]//
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
//[-end-210922-YUNLEI0137-modify]//
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault,  GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DEVSLP(Reserved??
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TBT FORCE POWER
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
//Note: This GPIO setting has been configured in other file, so remove it.
//{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DGPU_PWR_CTRL_EN 
{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//V1P8A_GPU_PWR_EN(Reserve) set as NC
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//REESEVERD
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //REESEVERD
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
//[-start-211027-JAYAN00003-modify]//
//FOR DCI ENABLE
//{GPIO_VER2_LP_GPP_E18, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_TXD{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
//{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
//FOR DCI ENABLE
//[-end-211027-JAYAN00003-modify]//
{GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU SD RSTN (Reserve) set as NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU SD CDN (Reserve) set as NC

//[-end-201214-MARY000011-modify]//
// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14 set NC
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15 set NC
{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//For ITS
{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU SD EN (Reserve) set as NC
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone, GpioPadLock}},//CPU HDMI DDC CLK
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone, GpioPadLock}},//CPU HDMI DDC DAT
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},// CPU_C10_GATE_N
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[start-210923-STORM1115-modify]
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//[-start-211009-YUNLEI0140-remove]//
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//[-end-211009-YUNLEI0140-remove]//
//[end-210923-STORM1115-modify]
{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[start-210923-STORM1115-modify]
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-start-211009-YUNLEI0140-remove]//
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-end-211009-YUNLEI0140-remove]//
//[end-210923-STORM1115-modify]
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//SD WAKE N (Reserve) set as NC
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
//{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1 set NC
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2 set NC
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_CLK0
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_DAT0
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW#
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT_R
//Note: D2 Error Native1
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCH_LAN_WAKE#(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT#_R
//Note: D4 Error Gpiomode
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S3#
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S4#
{GPIO_VER2_LP_GPD6,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_A#(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BB_TBT_PERST#(Reserved)
//Note: D8 Error Gpiomode
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PM_SLP_WLAN#(Reserved)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_PM_SLP_S5#(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
};
//[-end-210726-YUNLEI0115-modify]//
#endif

//[end-210720-STORM1100-modify]//
//[-start-210923-JEPLIUT189-modify]//
//[-start-210721-QINGLIN0001-add]//
#ifdef S570_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S570_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_CS_N
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_RST_N
//[-start-220101-OWENWU0034-remove]//
//{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BoardID 7
//[-end-220101-OWENWU0034-remove]//
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//SSD_SATA_PCIE_DET1_N(Reserved)
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_BT_OFF_N
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC1_N
{GPIO_VER2_LP_GPP_A15, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_HDMI_HPD
{GPIO_VER2_LP_GPP_A16, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC3_N
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-211008-QINGLIN0090-modify]//
{GPIO_VER2_LP_GPP_A19, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DDSP_HPD1
{GPIO_VER2_LP_GPP_A20, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DDSP_HPD2
//[-end-211008-QINGLIN0090-modify]//
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//IR_CAM_DTCT_N(Reserved)
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//IR_FW_FLASH_EN(Reserved)
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//{GPIO_VER2_LP_GPP_B7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PXS_PWREN
//[-start-210917-QINGLIN0068-modify]//
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//FB_GC6_EN_R(Reserved)
//[-end-210917-QINGLIN0068-modify]//
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP STRAP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210917-QINGLIN0068-modify]//
{GPIO_VER2_LP_GPP_B16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NVVDD_PWROK_R(Reserved)
//[-end-210917-QINGLIN0068-modify]//
{GPIO_VER2_LP_GPP_B17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18_NO_REBOOT(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML1_ALERT_N(Reserved)
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK(Reserved)
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBDATA(Reserved)
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SMB_ALERT_N(Reserved)
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML0_CLK(Reserved)
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML0_DATA(Reserved)
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML0_ALERT_N(Reserved)
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_CLK
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_DATA

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
//{GPIO_VER2_LP_GPP_D0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PXS_RST_N_R
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//FPR_RESET_R(Reserved)
{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//UART_WLAN_WAKE_N(Reserved)
{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SSD_CLKREQ0_N
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//WLAN_CLKREQ1_N
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//{GPIO_VER2_LP_GPP_D8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ_N
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_LSX2_TXD(Reserved)
{GPIO_VER2_LP_GPP_D10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_LSX2_RXD(Reserved)
{GPIO_VER2_LP_GPP_D11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_HDMI_DDC_CLK
{GPIO_VER2_LP_GPP_D12, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_HDMI_DDC_DAT
//[-start-211028-QINGLIN0106-modify]//
//{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_RST_N TouchScreen Reset
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_RST_N TouchScreen Reset
//[-end-211028-QINGLIN0106-modify]//
//[-start-210917-QINGLIN0068-modify]//
{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPU_EVENT_N(Reserved)
//[-end-210917-QINGLIN0068-modify]//
//[-start-220128-JEPLIUT213-modify]// 
//{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_STOP TouchScreen Stop
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_STOP TouchScreen Stop
//[-end-220128-JEPLIUT213-modify]// 
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_STOP TouchScreen Stop
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_INT_N TouchScreen Interrupt
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TP_INT_N Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_PERST_N(Reserved)
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},// TO ec ,3.3v  /CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC //  TBT FORCE POWER
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SMI(Reserved)
{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF1_N(Reserved)
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_WAKE_N(Reserved)
{GPIO_VER2_LP_GPP_E18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_LSX0_TXD(Reserved)
{GPIO_VER2_LP_GPP_E19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_LSX0_RXD(Reserved)
{GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_LSX1_TXD(Reserved)
{GPIO_VER2_LP_GPP_E21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_LSX1_RXD(Reserved)
{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

//[-end-201214-MARY000011-modify]//
// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10,{GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we needn't configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8 
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
//{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
//{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15
{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCH_VGA_ALERT_N(Reserved)
//{GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_PWRGD
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},// CPU_C10_GATE_N
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210923-QINGLIN0080-modify]//
//[-start-210929-QINGLIN0087-remove]//
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_FN_LED_N
//{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_NUM_LED_N
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_CAPS_LED_N
//[-end-210929-QINGLIN0087-remove]//
//[-end-210923-QINGLIN0080-modify]//
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4
{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_CLK0
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_DAT0
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW_N
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT
//Note: D2 Error Native1
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_LAN_WAKE_N(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT_N
//Note: D4 Error Gpiomode
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S3_N
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S4_N
{GPIO_VER2_LP_GPD6,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_PM_SLP_A_N(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_PERST_N(Reserved)
//Note: D8 Error Gpiomode
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_PM_SLP_WLAN_N(NC)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S5_N(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TP123 NC
};
//[-end-210923-JEPLIUT189-modify]//
#endif
//[-end-210721-QINGLIN0001-add]//

//[-start-210803-QINGLIN0008-add]//
//[-start-210907-QINGLIN0046-modify]//
#ifdef S370_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S370_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_CS_N
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPU_SELFREFRESH_ENABLE_R(Reserved)
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_CORE_SAMC_SA_VR_PG_R(Reserved)
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ESPI_RST_N
//[-start-211228-QINGLIN0136-remove]//
//{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BoardID 7
//[-end-211228-QINGLIN0136-remove]//
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_BT_OFF_N
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC1_N
//{GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},// PEG_RST_N
{GPIO_VER2_LP_GPP_A16, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC3_N
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_PWRGD(Reserve)
{GPIO_VER2_LP_GPP_A18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone,    GpioPadLock}},//CPU HDMI HPD
{GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DDSP_HPD1(Reserved)
//{GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirInInv,   GpioOutDefault,  GpioIntLevel | GpioIntSci,     GpioHostDeepReset, GpioTermNone ,   GpioPadConfigUnlock }}, //GPU_PCIE_WAKE_N
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP STRAP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18_NO_REBOOT(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_SML1_ALERT_N(Reserved)
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBDATA
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_SMB_ALERT_N(Reserved)
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_SML0_CLK(Reserved)
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_SML0_DATA(Reserved)
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_SML0_ALERT_N(Reserved)
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_CLK
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_DATA

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_D0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210923-QINGLIN0080-modify]//
//[-start-210929-QINGLIN0087-remove]//
//{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,      GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_NUM_LED_N
//[-end-210929-QINGLIN0087-remove]//
//[-end-210923-QINGLIN0080-modify]//
{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SSD_CLKREQ0_N
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//WLAN_CLKREQ1_N
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault, GpioTermDefault,  GpioPadLock}},//CARD_CLKREQ2_N
//{GPIO_VER2_LP_GPP_D8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ_N
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//Note: GPP_D11 GPP_D12  Error Native6
{GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-211028-QINGLIN0106-modify]//
//{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_RST_N TouchScreen Reset
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_RST_N TouchScreen Reset
//[-end-211028-QINGLIN0106-modify]//
//{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_CORE_SAMC_SA_VR_PG_R2
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_STOP TouchScreen Stop
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TS_INT_N TouchScreen Interrupt
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TP_INT_N Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_PERST_N(Reserved)
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
//{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDefault, GpioPlatformReset, GpioTermNone,    GpioPadConfigUnlock }}, // DGPU_PWR_CTRL_EN
{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//V1P8A_GPU_PWR_EN(Reserve) set as NC
//[-start-210916-xinwei0002-add]// 
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
//[-end-210916-xinwei0002-add]//
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FPR_RESET(Reserved)
//Note: GPP_E16 Error GpioPadModeNative6
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF1_N(Reserved)
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_WAKE_N
{GPIO_VER2_LP_GPP_E18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//Note: GPP_E20 GPP_E21 Error Native6
{GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

//[-end-201214-MARY000011-modify]//
// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10,{GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we needn't configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8 
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
//{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
//{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15
{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone,    GpioPadLock}},//CPU_HDMI_DDC_CLK
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioPlatformReset, GpioTermNone,    GpioPadLock}},//CPU_HDMI_DDC_DATA
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_C10_GATE_N
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210923-QINGLIN0080-modify]//
//[-start-210929-QINGLIN0087-remove]//
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//[-end-210929-QINGLIN0087-remove]//
//[-end-210923-QINGLIN0080-modify]//
{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210923-QINGLIN0080-modify]//
//[-start-210929-QINGLIN0087-remove]//
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-end-210929-QINGLIN0087-remove]//
//[-end-210923-QINGLIN0080-modify]//
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4
{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_DMIC_CLK0(Reserved)
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_DMIC_DAT0(Reserved)
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW_N
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_LAN_WAKE_N(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT_N
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S3_N
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S4_N
//Note: GPD6 Error GpioPadModeNative1
{GPIO_VER2_LP_GPD6,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_PM_SLP_A_N(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TBT_PERST_N(Reserved)
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_PM_SLP_WLAN_N(NC)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S5_N(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TP123 NC
};
#endif
//[-end-210907-QINGLIN0046-modify]//
//[-end-210803-QINGLIN0008-add]//

//[-start-210823-DABING0003-modify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77014_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CS#
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BoardID 7
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//SSD_DET_N
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BT ON/OFF
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC1
{GPIO_VER2_LP_GPP_A15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC2
{GPIO_VER2_LP_GPP_A16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C0_SDA
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C0_SCL
{GPIO_VER2_LP_GPP_B7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SDA
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SCL
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//ISH_EC INT
{GPIO_VER2_LP_GPP_B16, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SDA
{GPIO_VER2_LP_GPP_B17, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C2_SCL
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210702-DABING0001-modify]//
//{GPIO_VER2_LP_GPP_B23, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_ALERT_N
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML1_ALERT_N
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
//{GPIO_VER2_LP_GPP_C0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBDATA#
//{GPIO_VER2_LP_GPP_C2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBDATA#
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_SDA_R
//{GPIO_VER2_LP_GPP_C4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_SDA_R
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_CLK_R
//{GPIO_VER2_LP_GPP_C5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//[-end-210702-DABING0001-modify]//
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_D0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ISH_ALS_INT
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TOF_INT_N
{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SSD_CLKREQ0_N
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//WLAN_CLKREQ1_N
//[-start-210831-GEORGE0001-modify]//
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NVVDD_PWROK_R(Reserved)
//[-end-210831-GEORGE0001-modify]//
//{GPIO_VER2_LP_GPP_D8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ_N
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_TXD
{GPIO_VER2_LP_GPP_D10, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_RXD
{GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D12, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX3_RXD
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Reset
{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210729-Kebin000040-modify]//
//{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
//{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
//{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
//[-start-210923-GEORGE0007-modify]//
//{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
//[-end-210923-GEORGE0007-modify]//
//[-end-210729-Kebin000040-modify]//
//{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_PWRGD
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DEVSLP(Reserved??
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,      GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TBT FORCE POWER
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
//[-start-210831-GEORGE0001-modify]//
{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPU_EVENT_N(Reserved)
//[-end-210831-GEORGE0001-modify]//
//{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PXS_PWREN_R
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//REESEVERD
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //REESEVERD
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
{GPIO_VER2_LP_GPP_E18, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_TXD
{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
{GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E21, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX1_RXD
{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15

{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//For ITS
//{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PXS_RST_N DGPU
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},// CPU_C10_GATE_N
//[-start-210831-GEORGE0001-modify]//
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VGA_ALERT_N(Reserved)
//[-end-210831-GEORGE0001-modify]//
//[-start-210918-Ching000002-modify]//
//[-start-211011-TAMT000025-remove]//
//Note: These GPIO setting have been configured in the PeiInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-end-211011-TAMT000025-remove]//
//[-end-210918-Ching000002-modify]//
//[-start-210831-GEORGE0001-modify]//
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//FB_GC6_EN_R(Reserved)
//[-end-210831-GEORGE0001-modify]//
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4
{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_CLK0
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_DAT0
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW#
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT_R
//Note: D2 Error Native1
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCH_LAN_WAKE#(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT#_R
//Note: D4 Error Gpiomode
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S3#
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S4#
{GPIO_VER2_LP_GPD6,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_A#(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BB_TBT_PERST#(Reserved)
//Note: D8 Error Gpiomode
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PM_SLP_WLAN#(Reserved)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_PM_SLP_S5#(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
};
#endif
//[-end-210823-DABING0003-modify]//

//[-start-210914-DABING0006-modify]//
//[-start-210917-DABING0007-modify]//
#ifdef S77013_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77013_GPIO_Table[] = 
{
// Pad                 | PadMode       | HostSoftPadOwn    | Direction     | OutputState   | InterruptConfig  | PowerConfig  | ElectricalConfig | LockConfig
// (0)                 | (1)           | (2)               | (3)           | (4)           | (5)              | (6)          | (7)              | (8)
// ===========================================================================================================================
// GPP_AX
// ===========================================================================================================================
// sample
{GPIO_VER2_LP_GPP_A0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO0
{GPIO_VER2_LP_GPP_A1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO1
{GPIO_VER2_LP_GPP_A2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO2
{GPIO_VER2_LP_GPP_A3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_IO3
{GPIO_VER2_LP_GPP_A4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CS#
{GPIO_VER2_LP_GPP_A5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A10, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ESPI_CLK
{GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//SSD_DET_N
{GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BT ON/OFF
{GPIO_VER2_LP_GPP_A14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC1
{GPIO_VER2_LP_GPP_A15, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USBOC2
{GPIO_VER2_LP_GPP_A16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =============================================================================================================================
// GPP_BX
// =============================================================================================================================
{GPIO_VER2_LP_GPP_B0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID0
{GPIO_VER2_LP_GPP_B1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//VCCIN_AUX_VID1
{GPIO_VER2_LP_GPP_B2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPPC_B2_VRALERT_N
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C0_SDA
{GPIO_VER2_LP_GPP_B6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C0_SCL
{GPIO_VER2_LP_GPP_B7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SDA
{GPIO_VER2_LP_GPP_B8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//ISH_I2C1_SCL
{GPIO_VER2_LP_GPP_B9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PD_I2C_INT_N
{GPIO_VER2_LP_GPP_B12, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PM_SLP_S0_N
{GPIO_VER2_LP_GPP_B13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_PLT_RST_N
{GPIO_VER2_LP_GPP_B14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_BEEP FOR Top Swap
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//ISH_EC INT
{GPIO_VER2_LP_GPP_B16, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//IPCM
{GPIO_VER2_LP_GPP_B17, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//IPCM
{GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPP_B18(STRAP)
{GPIO_VER2_LP_GPP_B19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_B22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210702-DABING0001-modify]//
//{GPIO_VER2_LP_GPP_B23, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_SML1_ALERT_N
{GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//CPU_SML1_ALERT_N
// ==============================================================================================================================
// GPP_CX
// ==============================================================================================================================
//{GPIO_VER2_LP_GPP_C0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBDATA#
//{GPIO_VER2_LP_GPP_C2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C1,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBDATA#
{GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//{GPIO_VER2_LP_GPP_C3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_SDA_R
//{GPIO_VER2_LP_GPP_C4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_SDA_R
{GPIO_VER2_LP_GPP_C4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_I2C2_CLK_R
//{GPIO_VER2_LP_GPP_C5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#
{GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone, GpioLockDefault}},//PCH_SMBCLK#
//[-end-210702-DABING0001-modify]//
{GPIO_VER2_LP_GPP_C6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_I2C2_CLK_R
{GPIO_VER2_LP_GPP_C7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_SMBCLK#

// ==============================================================================================================================
// GPP_DX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_D0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TOF_RESET reserved
{GPIO_VER2_LP_GPP_D1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_TOF_LPN reserved
{GPIO_VER2_LP_GPP_D2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_ISH_ALS_INT
{GPIO_VER2_LP_GPP_D3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TOF_INT_N
{GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

{GPIO_VER2_LP_GPP_D5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0CLK(TBTB)
{GPIO_VER2_LP_GPP_D6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SML0DATA(TBTB)
{GPIO_VER2_LP_GPP_D7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D9,  {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_TXD
{GPIO_VER2_LP_GPP_D10, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX2_RXD
{GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D12, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX3_RXD
{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Reset
{GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210729-Kebin000040-modify]//
//{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
//{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
//{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
{GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Stop
{GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//TouchScreen Interrupt
//[-start-210923-GEORGE0007-modify]//
//{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
//[-end-210923-GEORGE0007-modify]//
//[-end-210729-Kebin000040-modify]//
{GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_D19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_EX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_ECLPM_BREA
{GPIO_VER2_LP_GPP_E2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//EC_SCI_N
{GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//DEVSLP(Reserved??
{GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//TBT FORCE POWER
{GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_WLAN_OFF_N
{GPIO_VER2_LP_GPP_E9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//USB_OC0#
{GPIO_VER2_LP_GPP_E10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_VCC_TS_ON
{GPIO_VER2_LP_GPP_E13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E14, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_EDP_HPD
{GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//REESEVERD
{GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //REESEVERD
{GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},
{GPIO_VER2_LP_GPP_E18, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_TXD
{GPIO_VER2_LP_GPP_E19, {GpioPadModeNative4, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_TBT_LSX0_RXD
{GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC

//[-end-201214-MARY000011-modify]//
// ==============================================================================================================================
// GPP_FX
// ==============================================================================================================================
{GPIO_VER2_LP_GPP_F0, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_DT_R
{GPIO_VER2_LP_GPP_F1, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_BRI_RSP
{GPIO_VER2_LP_GPP_F2, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_DT_R
{GPIO_VER2_LP_GPP_F3, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_RGI_RSP
{GPIO_VER2_LP_GPP_F4, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_CNVI_RF_RESET_N
{GPIO_VER2_LP_GPP_F5, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CNVI_MODEM_CLKREQ_R
{GPIO_VER2_LP_GPP_F6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_F8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC 
{GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
//Note: These GPIO setting have been configured in the BoardSaInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
//{GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
//{GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
//{GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID11
{GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 not used
{GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13 not used
{GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID14 not used
{GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID15 not used
//[-start-210729-Kebin000040-modify]//
//[-start-210923-GEORGE0007-modify]//
{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//{GPIO_VER2_LP_GPP_F19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirInInv,   GpioOutDefault, GpioIntLevel,   GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Interrupt for TouchPad
//[-end-210923-GEORGE0007-modify]//
//[-start-210729-Kebin000040-modify]//
{GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_F23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC


// =================================================================================================================================
// GPP_HX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC STARP
{GPIO_VER2_LP_GPP_H4,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SDA
{GPIO_VER2_LP_GPP_H5,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C0_SCL
{GPIO_VER2_LP_GPP_H6,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SDA
{GPIO_VER2_LP_GPP_H7,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_I2C1_SCL
{GPIO_VER2_LP_GPP_H8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//IPCM
{GPIO_VER2_LP_GPP_H9,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//IPCM
#if EFI_DEBUG
{GPIO_VER2_LP_GPP_H10, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
{GPIO_VER2_LP_GPP_H11, {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//UART for debug
#else
{GPIO_VER2_LP_GPP_H10, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
{GPIO_VER2_LP_GPP_H11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//
#endif
//Note:The GPP_B3/B4/B15/H12 has been mandatory configured by 'ME' when the ISH sensor function enable.
{GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//E3_PWR_EN_3V
{GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H17, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_H18, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},// CPU_C10_GATE_N
{GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
//[-start-210924-TAMT000012-A-modify]//
//[-start-211011-TAMT000024-remove]//
//Note: These GPIO setting have been configured in the PeiInitPreMemLib.c so we need'nt configures these GPIO one more.
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//{GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
{GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_F4_LED_N
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//{GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//[-end-211011-TAMT000024-remove]//
//[-end-210924-TAMT000012-A-modify]//
{GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// =================================================================================================================================
// GPP_RX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_R0,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_BCLK
{GPIO_VER2_LP_GPP_R1,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SYNC
{GPIO_VER2_LP_GPP_R2,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDO
{GPIO_VER2_LP_GPP_R3,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_HDA_SDI
{GPIO_VER2_LP_GPP_R4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID3 not used
{GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID4 not used
{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5 not used
{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6 not used

// =================================================================================================================================
// GPP_SX
// =================================================================================================================================
{GPIO_VER2_LP_GPP_S0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID1 not used
{GPIO_VER2_LP_GPP_S1,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID2 not used
{GPIO_VER2_LP_GPP_S2,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_CLK0
{GPIO_VER2_LP_GPP_S3,  {GpioPadModeNative2, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//CPU_DMIC_DAT0
{GPIO_VER2_LP_GPP_S4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S5,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
{GPIO_VER2_LP_GPP_S7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
// ================================================================================================================================
// GPD_GPDX
// ================================================================================================================================
{GPIO_VER2_LP_GPD0,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//BATLOW#
{GPIO_VER2_LP_GPD1,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//AC_PRESENT_R
//Note: D2 Error Native1
{GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PCH_LAN_WAKE#(Reserved)
{GPIO_VER2_LP_GPD3,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PBTN_OUT#_R
//Note: D4 Error Gpiomode
{GPIO_VER2_LP_GPD4,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S3#
{GPIO_VER2_LP_GPD5,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_S4#
{GPIO_VER2_LP_GPD6,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PM_SLP_A#(NC)
{GPIO_VER2_LP_GPD7,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//BB_TBT_PERST#(Reserved)
//Note: D8 Error Gpiomode
{GPIO_VER2_LP_GPD8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//SUSCLK
//Note: The following GPP_D9/D11 can't change from the actual Native1 state to the except configured GPIO mode state
{GPIO_VER2_LP_GPD9,    {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PM_SLP_WLAN#(Reserved)
{GPIO_VER2_LP_GPD10,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//PCH_PM_SLP_S5#(NC)
{GPIO_VER2_LP_GPD11,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirNone,    GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NC
};
#endif
//[-end-210917-DABING0007-modify]//
//[-end-210914-DABING0006-modify]//

/**
 This function offers an interface to dynamically modify GPIO table.

 @param[in, out]    GpioTable           On entry, points to a structure that specifies the GPIO setting.
                                        On exit, points to the updated structure.
 @param[in, out]    GpioTableCount      On entry, points to a value that the length of GPIO table .
                                        On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcModifyGpioSettingTable (
  IN OUT  GPIO_INIT_CONFIG                 **GpioTable,
  IN OUT  UINT16                           *GpioTableCount
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
// *GpioTable = mGpioTableTglUDdr4Simics;
//
// *GpioTableCount = 38;
//[-start-210519-KEBIN00001-modify]//
#ifdef LCFC_SUPPORT
 #ifdef C970_SUPPORT
  *GpioTable = YogaC970_GPIO_Table;
  *GpioTableCount = sizeof(YogaC970_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
 #endif
//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT
//[-start-210726-YUNLEI0115-modify]//
  UINT8                             Panel_Size;
  OemSvcLfcGetBoardID(PANEL_SIZE, &Panel_Size);
  if(Panel_Size==PANEL_SIZE_16)
  {
    *GpioTable = YogaC770_16_GPIO_Table;
    *GpioTableCount = sizeof(YogaC770_16_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
  }else{
    *GpioTable = YogaC770_14_GPIO_Table;
    *GpioTableCount = sizeof(YogaC770_14_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
  }
//[-end-210726-YUNLEI0115-modify]//
#endif
//[end-210720-STORM1100-modify]//
//[-start-210721-QINGLIN0001-add]//
#ifdef S570_SUPPORT
  *GpioTable = S570_GPIO_Table;
  *GpioTableCount = sizeof(S570_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
#endif
//[-end-210721-QINGLIN0001-add]//

//[-start-210803-QINGLIN0008-add]//
#ifdef S370_SUPPORT
  *GpioTable = S370_GPIO_Table;
  *GpioTableCount = sizeof(S370_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
#endif
//[-end-210803-QINGLIN0008-add]//

//[-start-210817-DABING0002-modify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  *GpioTable = S77014_GPIO_Table;
  *GpioTableCount = sizeof(S77014_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
#endif
//[-start-210817-DABING0002-modify]//

//[-start-210914-DABING0006-modify]//
#ifdef S77013_SUPPORT
  *GpioTable = S77013_GPIO_Table;
  *GpioTableCount = sizeof(S77013_GPIO_Table)/sizeof(GPIO_INIT_CONFIG);
#endif
//[-start-210914-DABING0006-modify]//
  return EFI_MEDIA_CHANGED;
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210519-KEBIN00001-modify]//
}
