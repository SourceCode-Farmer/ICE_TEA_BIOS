/** @file
  Provides an interface to Get Secure Key by EC.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcNotebookPasswordDesignLib.h>

/**
  Provides an interface to Get Secure Key by EC.

  @param  SecureKey                     A pointer to the secure key for UHDP encryption.

  @retval EFI_SUCCESS                   Get Secure Key success.
  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcGetSecureKey (
  UINT8                                 **SecureKey
  )
{
  //
  // [Natural File Guard Design Guide V1.01]
  //   2.2.1 Secure Key Generation
  //     Procedure:
  //       2. During POST, BIOS will retrieve the Secure Key via IO port 0x66/0x62 just at the event of data
  //          encryption/decryption, and BIOS should clear the key buffer after the encryption/decryption.
  //

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
