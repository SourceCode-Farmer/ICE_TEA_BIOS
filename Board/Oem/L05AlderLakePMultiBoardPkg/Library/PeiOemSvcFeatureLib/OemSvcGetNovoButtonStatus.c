/** @file
  Provides an opportunity for Get Novo Button Status

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcGetNovoButtonStatus.h>
//_Start_L05_FEATURE_
#include <Library/CmosLib.h>
#include <OemCmos.h>
//_End_L05_FEATURE_

//[-start-210609-FLINT00004-add]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210609-FLINT00004-add]//
/**
 This function offers an interface to Crisis Led state

 @retval            EFI_UNSUPPORTED     Novo Button is not pressed
 @retval            EFI_MEDIA_CHANGED   Novo Button is pressed
*/
EFI_STATUS
OemSvcGetNovoButtonStatus (
  VOID
  )
{
//[-start-210609-FLINT00004-modify]//
#ifdef LCFC_SUPPORT
  EFI_STATUS                            Status = EFI_UNSUPPORTED;
  BOOLEAN                               NovoKeyStatus = FALSE;

  Status = LfcEcLibGetNovoStatus (&NovoKeyStatus);

  if (!EFI_ERROR(Status) && NovoKeyStatus) {
    Status = EFI_MEDIA_CHANGED;
  }
#else
  EFI_STATUS                            Status = EFI_UNSUPPORTED;

//_Start_L05_FEATURE_
  //
  // BUG!! BUG!! BUG!!   
  // This is for Lenovo INTERNAL NovoButton and CrisisRecovery test.
  // Project need to remove this code.
  // 0x01 = Crisis Recovery.
  // 0x02 = Novo button pressed.
  //
  if (ReadCmos8 (EFI_L05_NOVO_BUTTON_CRISIS_TEST) == (UINT8)0x02) {
    WriteCmos8 (EFI_L05_NOVO_BUTTON_CRISIS_TEST, (UINT8)0x0);
    Status = EFI_MEDIA_CHANGED;
  }
//_End_L05_FEATURE_
#endif
//[-end-210609-FLINT00004-modify]//

  return Status;
}
