/** @file
  OEM Service to get Wireless Device Support Table for Wireless Device Support feature

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05WirelessDeviceSupportConfig.h>

//
// Please implemnt PCI location which do you want to detect
//
EFI_L05_PCI_LOCATION                    mPciLocationLList [] = {
                                          //
                                          //  Bus, Device, Function
                                          //
//[-start-210817-DABING0002-modify]//
#ifdef LCFC_SUPPORT
                                          {  0x00,   0x14,     0x03},
#endif
//[-end-210817-DABING0002-modify]//
//                                        {  0x00,   0x1C,     0x01},
//                                        {  0x00,   0x05,     0x00},
                                          //
                                          // End of List (DO NOT Remove)
                                          //
                                          {0xFF,   0xFF,     0xFF}
                                          };

//
// Please implemnt USB location which do you want to detect
//
EFI_L05_USB_LOCATION                    mUsbLocationList [] = {
                                          //
                                          // Device,  Function,  Port
                                          //
//                                        {    0x00,      0x1C,  0x01},
                                          //
                                          // End of List (DO NOT Remove)
                                          //
                                          {0xFF, 0xFF, 0xFF}
                                          };

////
//// Sample Setting for wireless device
////
//UINT8                                    mSampleSettingValue[] = {
//                                           0x00, 0x01, 0x02, 0x03,
//                                           0x04, 0x05, 0x06, 0x07,
//                                           0x08, 0x09, 0x0A, 0x0B
//                                           };

//
// Please implement Supported wireless device to list.
//
EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  mSupportedDeviceList[] = {
  //
  // The [PCIE] device need to implemnt
  //
//  { // Name of Wireless Device
//    WLAN_DEVICE,                   // (UINT8)  DeviceType
//    PCI_INTERFACE,                 // (UINT8)  Interface
//    0x14E4,                        // (UINT16) VendorId
//    BrandName_Broadcom,            // (UINT8)  VendorNameId
//    sizeof (mSampleSettingValue),  // (UINTN)  SizeofSettingValue
//    (VOID *) mSampleSettingValue   // (VOID *) SettingValue
//  },
  //
  //  The [USB] device need to implement
  //
//  { // Name of Wireless Device
//    WWAN_DEVICE,         // (UINT8)  DeviceType
//    USB_INTERFACE,       // (UINT8)  Interface
//    0x14E4,              // (UINT16) VendorId
//    BrandName_Broadcom,  // (UINT8)  VendorNameId
//    12,                  // (UINTN)  SizeofSettingValue
//    L"123456"            // (VOID *) SettingValue
//  },
  //
  // End of List (DO NOT Remove)
  //
  {0xFF, 0xFF, 0xFFFF, 0xFF, 0, NULL}
};

/**
  OEM Service to get Wireless Device Support Table for Wireless Device Support feature

  @param  PciDeivceLocationList         Return Bus, Device, Function list  (if PciDeivceLocationList == NULL, Feature will not check PCI Interface)
  @param  UsbDeivceLocationList         Return Device, Function, Port list (if UsbDeivceLocationList == NULL, Feature will not check USB Interface)
  @param  SupportedWirelessDeviceList   Return Supported wireless device list (if SupportedWirelessDeviceList == NULL, Feature will not execute Wireless Device Support function)

  @retval EFI_MEDIA_CHANGED             Returns media changed, L05 Feature will check valid Supported wireless device list then execute Wireless Device Support function
  @retval EFI_UNSUPPORTED               Returns unsupported by default, L05 Feature will not execute Wireless Device Support function.
  @retval EFI_SUCCESS                   Returns success, L05 Feature will not execute Wireless Device Support function, Project need porting this feature function by itself.
**/
EFI_STATUS
OemSvcGetWirelessDeviceSupportTable (
  OUT EFI_L05_PCI_LOCATION                    **PciDeivceLocationList,
  OUT EFI_L05_USB_LOCATION                    **UsbDeivceLocationList,
  OUT EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  **SupportedWirelessDeviceList
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  *PciDeivceLocationList       = mPciLocationLList;
  *UsbDeivceLocationList       = mUsbLocationList;
  *SupportedWirelessDeviceList = mSupportedDeviceList;

  return EFI_MEDIA_CHANGED;
}
