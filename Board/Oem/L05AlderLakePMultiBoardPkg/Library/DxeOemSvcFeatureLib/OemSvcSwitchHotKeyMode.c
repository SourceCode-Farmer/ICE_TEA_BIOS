/** @file
  Provides an interface for switch Hotkey Mode

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#ifdef LCFC_SUPPORT
#include <L05Config.h>
#include <Library/LfcLib.h>
#include <Library/LfcEcLib.h>
#include <SetupConfig.h>
#include <Library/UefiRuntimeServicesTableLib.h>
//[-start-210823-Dongxu0011-add]//
//[-start-210825-YUNLEI0124-modify]//
//[-start-210825-QINGLIN0035-modify]//
//[-start-210929-TAMT000019-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/Gpiolib.h>

#define GPIO_VER2_LP_GPP_H20                 0x09070014 //FN_LED
#endif
//[-end-210929-TAMT000019-modify]//
//[-end-210825-QINGLIN0035-modify]//
//[-end-210825-YUNLEI0124-modify]//
//[-end-210823-Dongxu0011-add]//

#define SYSTEM_CONFIGURATION_GUID {0xA04A27f4, 0xDF00, 0x4D42, 0xB5, 0x52, 0x39, 0x51, 0x13, 0x02, 0x11, 0x3D}
#endif


/**
  Provides an interface for switch Hotkey Mode

  @param  L05HotKeyMode                 The switch setting of Hotkey Mode

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchHotKeyMode (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05HotKeyMode
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-210922-Dongxu0020-modify]// 
#ifdef LCFC_SUPPORT  
  EFI_STATUS    Status;
  Status = EFI_SUCCESS;

  if (0 == L05HotKeyMode) {
    LfcEcLibSetHotKeyMode (0);  //Hot key disable
//[-start-210823-Dongxu0011-add]//
//[-start-210825-YUNLEI0124-modify]//
//[-start-210825-QINGLIN0035-modify]//
//[-start-210929-TAMT000019-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT)
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x0);
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x1);
#endif
//[-end-210929-TAMT000019-modify]//
//[-end-210825-QINGLIN0035-modify]//
//[-end-210825-YUNLEI0124-modify]//
//[-end-210823-Dongxu0011-add]//
  } else {
    LfcEcLibSetHotKeyMode (1);  //Hot key enable  
//[-start-210823-Dongxu0011-add]//
//[-start-210825-YUNLEI0124-modify]//
//[-start-210825-QINGLIN0035-modify]//
//[-start-210929-TAMT000019-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT)
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x1);
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_H20,0x0);
#endif
//[-end-210929-TAMT000019-modify]//
//[-end-210825-QINGLIN0035-modify]//
//[-end-210825-YUNLEI0124-modify]//
//[-end-210823-Dongxu0011-add]//
  }
  return EFI_SUCCESS;
#else  
  return EFI_UNSUPPORTED;
#endif
//[-end-210922-Dongxu0020-modify]// 
}

