/** @file
  Provides an interface for switch Ultra Quiet Mode.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>
//[-start-211124-kebin000069-modify]//  
//[-start-220113-YUNLEI0158-modify]//
//[-start-220117-SHAONN0027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT)
#include <Library/LfcEcLib.h>
#include <Library/BaseLib.h>
#endif
//[-end-220117-SHAONN0027-modify]//
//[-end-220113-YUNLEI0158-modify]//
//[-start-211124-kebin000069-modify]//

//[-start-220104-Dongxu0040-add]// 
//[-start-220113-YUNLEI0158-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT)
EFI_STATUS
EFIAPI
OemSvcLfcGetCpuBrand(
  IN OUT  UINT8    *CpuType
)
{
  EFI_STATUS    Status;
  UINT32                               RegEAX;
  UINT32                               RegEBX;
  UINT32                               RegECX;
  UINT32                               RegEDX;

  AsmCpuid (0x80000003, &RegEAX, &RegEBX, &RegECX, &RegEDX);
  *CpuType = ((UINT8 ) RegEDX ) & 0x0f ;
  Status = EFI_SUCCESS;
   return Status;
}
//[-end-220113-YUNLEI0158-modify]//
//[-end-220104-Dongxu0040-add]//

#endif


/**
  Provides an interface for switch Ultra Quiet Mode.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05UltraQuietMode             The switch setting of Ultra Quiet Mode.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchUltraQuietMode (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05UltraQuietMode
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-211124-kebin000069-modify]//  
//[-start-220113-YUNLEI0158-modify]//
//[-start-220117-SHAONN0027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT)
//[-end-220117-SHAONN0027-modify]//
//[-end-220113-YUNLEI0158-modify]//
  EFI_STATUS      Status;
  UINT8           Ec8DData = 0;
//[-start-220120-QINGLIN0151-modify]//
#ifndef S370_SUPPORT
  UINT8           EcData;
  UINT8           CpuType;
#endif
//[-end-220120-QINGLIN0151-modify]//
  
  Status = LfcEcLibEcRamRead(0x8D, &Ec8DData);  //set EC RAM 0X8D bit5 to 1
  if (EFI_ERROR(Status)) {
    return Status;
  }
  Ec8DData &= 0xDF; // Clear bit5
  
  if(L05UltraQuietMode == 1) {
    Ec8DData |= 0x20 ;//  UltraQuietMode set to enable set bit5 =1
  }
  Status = LfcEcLibEcRamWrite (0x8D, Ec8DData);  
  if (EFI_ERROR(Status)) {
    return Status;   
  }
//[-start-220104-Dongxu0040-add]//
//[-start-220120-QINGLIN0151-modify]//
#ifndef S370_SUPPORT
  if(L05UltraQuietMode == 1) {
    OemSvcLfcGetCpuBrand(&CpuType);
    if (CpuType == 7) {
      EcData = 0xC7;    
    } else if(CpuType == 5) {
      EcData = 0xC5; 
    } else {
      EcData = 0xC7; //never go to here
    }
    LfcEcLibi7i5(EcData);
  }
#endif
//[-end-220120-QINGLIN0151-modify]//
//[-end-220104-Dongxu0040-add]//  
  return EFI_SUCCESS;  
#else
  return EFI_UNSUPPORTED;
#endif
//[-start-211124-kebin000069-modify]//

}

