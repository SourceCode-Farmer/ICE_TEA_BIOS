/** @file
  Provides an opportunity for ODM to get One Key Battery status.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/FeatureLib/OemSvcOneKeyBattery.h>
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>

#define EC_DATA                           0x62
#define EC_CMD_STATE                      0x66

#define EC_READ_ECRAM_CMD                 0x80
#define EC_WRITE_ECRAM_CMD                0x81
#define BATT_REMAINING_CAP_LOW            0xC2
#define BATT_REMAINING_CAP_HIGH           0xC3
#define BATT_FULL_CHARGE_CAP_LOW          0xCC
#define BATT_FULL_CHARGE_CAP_HIGH         0xCD

#define ADAPTER_STATE                     0xA3
#define ADAPTER_STATE_ONLINE              0X80

#define EC_BOOTUP_STATE_CMD               0x49
#endif
 
/**
  Provides an opportunity for ODM get battery percentage.

  @param  BatteryPercentage             Battery percentage

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_SUCCESS                   Support Get BatteryPercentage.
**/
EFI_STATUS
OemSvcGetBatteryPercentage (
  IN OUT  UINT8                         *BatteryPercentage
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
#ifdef LCFC_SUPPORT
  UINT16           RemainingCap, FullChargeCap;
  EFI_STATUS       Status;
  UINT8            Data1 = 1, Data2 = 1, Data3 = 1, Data4 = 1;
  
  Status = LfcEcLibEcRamRead (BATT_REMAINING_CAP_HIGH, &Data1);
  if (EFI_ERROR(Status)) {
    return Status;
  }
  
  Status = LfcEcLibEcRamRead (BATT_REMAINING_CAP_LOW, &Data2);
  if (EFI_ERROR(Status)) {
    return Status;
  }
  
  Status = LfcEcLibEcRamRead (BATT_FULL_CHARGE_CAP_HIGH, &Data3);
  if (EFI_ERROR(Status)) {
    return Status;
  }
  
  Status = LfcEcLibEcRamRead (BATT_FULL_CHARGE_CAP_LOW, &Data4);
  if (EFI_ERROR(Status)) {
    return Status;
  }
    
  RemainingCap  = (((UINT16)(Data1))<<8) + (UINT16)Data2;
  FullChargeCap = (((UINT16)(Data3))<<8) + (UINT16)Data4;
  
  //If battery not exist or battery error
  if (FullChargeCap == 0 || 100*RemainingCap/FullChargeCap > 100) {
  //If error
    *BatteryPercentage = 15;
    return EFI_SUCCESS;
  }
  
  *BatteryPercentage = (UINT8)(100*RemainingCap/FullChargeCap);
  return EFI_SUCCESS;
#else
  return EFI_UNSUPPORTED;
#endif 

}

/**
  Provides an opportunity for ODM get AC State.

  @param  AcStatus                      AC status.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_SUCCESS                   Support Get AC Status.
**/
EFI_STATUS
OemSvcGetAcStatus (
  OUT BOOLEAN                           *AcStatus
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
#ifdef LCFC_SUPPORT
  EFI_STATUS                            Status;
  
  UINT8       Data = 0;
  //Get AC Status.
  Status = LfcEcLibEcRamRead (ADAPTER_STATE, &Data );
  if (EFI_ERROR(Status)) {
    return Status;
  }
  if ((Data &ADAPTER_STATE_ONLINE) == 0) {
    *AcStatus = FALSE;
  } else {
    *AcStatus = TRUE;
  }
  return EFI_SUCCESS;
#else
  return EFI_UNSUPPORTED;
#endif
}

/**
  Provides an opportunity for ODM get AOU Status.

  @param  AouStatus                     AOU status.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_SUCCESS                   Support Get AOU status.
**/
EFI_STATUS
OemSvcGetAouStatus (
  OUT BOOLEAN                           *AouStatus
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
  return EFI_UNSUPPORTED;
}

/**
  Provides an opportunity for ODM get bootup state.

  @param  BootupState                       BootUp state.

  @retval EFI_UNSUPPORTED                   Returns unsupported by default.
  @retval EFI_SUCCESS                       Support Get BootUp State.
**/
EFI_STATUS
OemSvcGetBootUpState (
  OUT BOOTUP_STATE_FOR_SHOW_ONEKEYBATTERY   *BootupState
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/ 
//[-start-210928-BAOBO0003-modify]//
#ifdef LCFC_SUPPORT
  EFI_STATUS        Status = EFI_SUCCESS;
  UINT8             BootupStatus;
//[-start-210929-QINGLIN0087-modify]//
//#ifdef C970_SUPPORT
//[-start-211009-YUNLEI0140-modify]//
//[-start-211222-TAMT000025-A-modify]//
#if defined(C970_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211009-YUNLEI0140-modify]//
//[-end-210929-QINGLIN0087-modify]//
//  
//  //
//  // Get BootUp state
//  //
//  Status = EcCommand (
//             EC_CMD_STATE,
//             EC_DATA,
//             1,
//             &BootupStatus,
//             EC_BOOTUP_STATE_CMD,
//             0);
//  if (EFI_ERROR (Status)) {
//    return Status;
//  }
  BootupStatus = PcdGet8(NotifyECBootUp);
#else
  //
  // Get BootUp state
  //
  Status = EcCommand (
             EC_CMD_STATE,
             EC_DATA,
             1,
             &BootupStatus,
             EC_BOOTUP_STATE_CMD,
             0);
  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif
  IoWrite8 (0x70, 0x75);
  IoWrite8 (0x71, BootupStatus+1);  
  // Check if bootup state is for show OneKeyBattery
  if ((BootupStatus  == Normal_Bootup_State) ||
      (BootupStatus  == Keyboard_Bootup_State) ||
      (BootupStatus  == Attach_AC_Bootup_State) ||
      (BootupStatus  == Attach_AOU_Bootup_State)) {
    *BootupState = BootupStatus;
    return Status;
  } else{
    return EFI_NOT_FOUND;
  }
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-211222-TAMT000025-A-modify]//
//[-end-210928-BAOBO0003-modify]//
}
  
VOID
DisableEcPower (
  VOID
  )
{
  UINT8    Data;
  //Disable EC power
  LfcEcLibEcRamRead(0xA1, &Data);
  Data &= (UINT8)~(BIT6|BIT7);  // Clear BIT6 and BIT7
  LfcEcLibEcRamWrite(0xA1,Data);
}

