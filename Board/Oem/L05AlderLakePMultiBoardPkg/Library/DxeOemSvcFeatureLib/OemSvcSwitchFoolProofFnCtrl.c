/** @file
  Provides an interface for switch Fool Proof Fn Ctrl.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>
//[-start-210625-Dongxu0004-add]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210625-Dongxu0004-add]//

/**
  Provides an interface for switch Fool Proof Fn Ctrl.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05FoolProofFnCtrl            The switch setting of Fool Proof Fn Ctrl.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchFoolProofFnCtrl (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05FoolProofFnCtrl
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-210625-Dongxu0004-add]//
#ifdef LCFC_SUPPORT
  EFI_STATUS                            Status;
  UINT8                                 Data;

  Data = 0;

  Status = LfcEcLibEcRamRead (0xA1, &Data );
  if (EFI_ERROR(Status)) {
    return Status;
  }

  if (1 == L05FoolProofFnCtrl) {
    Data |= BIT0;
  }
  if (0 == L05FoolProofFnCtrl) {
    Data &= ~BIT0;
  }

  Status = LfcEcLibEcRamWrite (0xA1, Data);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210625-Dongxu0004-add]// 
}

