/** @file
  Provides an opportunity for ODM to get Country Code.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcSmbiosOverride.h>
#include <Library/BaseLib.h>

/**
  Provides an opportunity for ODM get Country Code.

  @param  CountryCode                   Points to uint8 that specifies to get Country Code

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Get EC Country Code success.
**/
EFI_STATUS
OemSvcGetCountryCode (
  CHAR8                                 *CountryCode,
  UINTN                                 CountryCodeSize
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//
// [Lenovo China Minimum BIOS Spec V1.40]
//   3.3.5.2 Type 11 OEM string
//     1 Country code string use format "Country - XX", XX is ISO 3166 alpha-2 code
//
//  AsciiStrCpyS (CountryCode, CountryCodeSize, "TW");
//
//  return EFI_MEDIA_CHANGED;
//
  return EFI_UNSUPPORTED;
}

