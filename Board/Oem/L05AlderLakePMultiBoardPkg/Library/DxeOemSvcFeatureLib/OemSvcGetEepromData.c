/** @file
  Provides an opportunity for ODM to get Eeprom Data.

;******************************************************************************
;* Copyright (c) 2012 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <L05Config.h>
#include <FlashRegionLayout.h>
#include <Library/FeatureLib/OemSvcSmbiosOverride.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/FlashRegionLib.h>

//[-start-210623-YUNLEI0104-add]//
#ifdef LCFC_SUPPORT
#include <Protocol/LenovoVariable.h>
#include <Library/BaseMemoryLib.h>
#endif
//[-end-210623-YUNLEI0104-add]//

/**
  Provides an opportunity for ODM get Eeprom Data.
  (Lenovo Sepcification: Lenovo China Minimum BIOS Spec 128.doc - CH.3.4.6 System Management BIOS (SMBIOS)
                         LENOVO_CHINA_EEPROMContents_and_Tools_Spsc 1.14 - All Chapter)

  @@@@@@  The MTM Data will effect Type1-Offset 19 Data.  @@@@@@
  if MTM existed (Data Example: 12345678), Type1-Offset 0x19 will be show LENOVO_MT_XXXXX
  if MTM Not exited (Data: 0x0),           Type1-Offset 0x19 will be show LENOVO_BI_IDEAPADXX

  @param  BufferPtr                     Points to VOID that specifies to EEPROM Data (EFI_L05_EEPROM_MAP)
                                        String must be ASCII.
  @param  EepromSize                    Input Eeprom length, define in Sepc

  @retval EFI_UNSUPPORTED               Returns unsupported by uesed default rule (Load eerpom from default data).
  @retval EFI_MEDIA_CHANGED             Get Eeprom Data success.
**/
EFI_STATUS
OemSvcGetEepromData (
  IN OUT  VOID                          *BufferPtr,
  IN      UINT16                        EepromSize
  )
{
  /*++
    Todo:
      Add project specific code in here.
  //
  // ODM could use EC or other method to access EEPROM.
  // If the EEPROM region is put in ROM, you may refer to this sample.
  //
  // Note:
  // The UUID in EEPROM is following SMBIOS SPEC :
  // The UUID {00112233-4455-6677-8899-AABBCCDDEEFF} would thus be represented as:
  // 33 22 11 00 55 44 77 66 88 99 AA BB CC DD EE FF => In EEEPROM Type
  // If your UUID in EEPROM wasn't following, please convert it in this function.
  //
  --*/

////
//// BUGBUG:
////
//// The Memeory structure will follow definition EFI_L05_EEPROM_MAP in L05Config.h
//// !!!!!!  Please follow EFI_L05_EEPROM_MAP structure to set data !!!!!!
//// !!!!!!  Sructure mapping definition in Spec LENOVO_CHINA_EEPROMContents_and_Tools_Spsc 1.14 ch.3.7 !!!!!!
//
//// Sample code :
//  EFI_L05_EEPROM_MAP *L05EerpomBase;
//  L05EerpomBase = (EFI_L05_EEPROM_MAP *) (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionEepromGuid, 1);
////
////  Please set data following EFI_L05_EEPROM_MAP structure
////  Ex: Set ProductName data
//  gBS->CopyMem(L05EerpomBase->ProductName, AnotherEEPROM->ProductName, L05_EEPROM_PRODUCT_NAME_LENGTH);
//  gBS->CopyMem (BufferPtr, (VOID *)L05EerpomBase, EepromSize);
//  return EFI_MEDIA_CHANGED;

//[-start-210623-YUNLEI0104-modify]//
#ifdef LCFC_SUPPORT
  LENOVO_VARIABLE_PROTOCOL                *LenovoVariable = NULL;
  EFI_GUID                                DefaultGuid = LVAR_PRODUCT_NAME_GUID;
  EFI_STATUS                              Status;
  UINT32                                  DataSize;
  UINT8                                   *TempBufferPtr;

  Status = gBS->LocateProtocol (&gLenovoVariableProtocolGuid, NULL, &LenovoVariable);
  if (EFI_ERROR (Status)) {
    return EFI_SUCCESS;
  }

  //Make sure buffer is cleared
  SetMem (BufferPtr, EepromSize, 0);

  //1-5
  //LVAR_MTM_NUMBER_GUID
  DefaultGuid.Data4[7] = 0x02;
  DataSize = L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_MACHINE_TYPE_MODEL_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //1-6, 3-6
  //LVAR_PRODUCT_NAME_GUID
  DefaultGuid.Data4[7] = 0x00;
  DataSize = L05_EEPROM_PRODUCT_NAME_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_PRODUCT_NAME_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //1-8
  //LVAR_UUID_GUID
  DefaultGuid.Data4[7] = 0x05;
  DataSize = L05_EEPROM_UUID_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_UUID_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //1-1a
  //LVAR_FAMILY_NAME_GUID
  DefaultGuid.Data4[7] = 0x0B;
  DataSize = L05_EEPROM_FAMILY_NAME_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_FAMILY_NAME_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //2-5
  //LVAR_PROJECT_NAME_GUID
  DefaultGuid.Data4[7] = 0x01;
  DataSize = L05_EEPROM_PROJECT_NAME_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_PROJECT_NAME_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //2-6
  //LVAR_OS_PN_NUMBER_GUID
  DefaultGuid.Data4[7] = 0x0F;
  DataSize = L05_EEPROM_OS_PN_NUMBER_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_OS_PN_NUMBER_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //2-6
  //LVAR_OS_DESCRIPTOR_GUID
  DefaultGuid.Data4[7] = 0x10;
  DataSize = L05_EEPROM_OS_DESCRIPTOR_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_OS_DESCRIPTOR_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //2-7, 3-7
  //LVAR_LENOVO_SN_GUID
  DefaultGuid.Data4[7] = 0x04;
  DataSize = L05_EEPROM_SERIAL_NUMBER_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_SERIAL_NUMBER_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);

  //2-8, 3-8
  //LVAR_ASSET_TAG_GUID
  DefaultGuid.Data4[7] = 0x0C;
  DataSize = L05_EEPROM_ASSET_TAG_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_ASSET_TAG_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);
  
  DefaultGuid.Data4[6] = 0x0b;
  DefaultGuid.Data4[7] = 0x00;
//DefaultGuid = LVAR_OA3_KEY_ID_GUID;
  DataSize = L05_EEPROM_OA3_KEY_ID_LENGTH;
  TempBufferPtr = (UINT8*)BufferPtr + L05_EEPROM_OA3_KEY_ID_OFFSET;
  Status = LenovoVariable->GetVariable (
                             LenovoVariable,
                             &DefaultGuid,
                             &DataSize,
                             TempBufferPtr);
  return EFI_MEDIA_CHANGED;
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210623-YUNLEI0104-modify]//
}
