/** @file
  Provides an opportunity for ODM to get EC major and minor version.

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcSmbiosOverride.h>
//[-start-210617-FLINT00006-add]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210617-FLINT00006-add]//

/**
  Provides an opportunity for ODM get EC version.

  @param  MajorVer                      Points to uint8 that specifies to get EC major veriosn
  @param  MinorVer                      Points to uint8 that specifies to get EC major veriosn

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Get EC major and minor version success.
**/
EFI_STATUS
OemSvcGetEcVersion (
  OUT UINT8                             *MajorVer,
  OUT UINT8                             *MinorVer
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-210617-FLINT00006-modify]//
#ifdef LCFC_SUPPORT
  UINT8      EcTestVersion;
  EFI_STATUS Status;

  Status = LfcEcLibReadEcVersion(
                             MajorVer,
                             MinorVer,
                             &EcTestVersion
                             );

  return EFI_MEDIA_CHANGED;
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210617-FLINT00006-modify]//
}

