/** @file
  Provides an interface for switch One Key Battery.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>

#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif


/**
  Provides an interface for switch One Key Battery.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05OneKeyBattery              The switch setting of One Key Battery.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchOneKeyBattery (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05OneKeyBattery
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

//[-start-210721-QINGLIN0001-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210817-DABING0002-modify]//
//#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT)
#ifdef LCFC_SUPPORT
  EFI_STATUS Status;
  
  Status = LfcEcLibSetOneKayBattery (L05OneKeyBattery);
  
  return EFI_SUCCESS;
#else  
  return EFI_UNSUPPORTED;
#endif
//[-end-210817-DABING0002-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-modify]//
}

