/** @file
  Provides an interface for Disable Building-in Battery.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#endif

/**
  Provides an interface for Disable Building-in Battery.

  @param  None

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcDisableBuildingInBattery (
  VOID
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
#ifdef LCFC_SUPPORT
  EFI_STATUS           Status;

  Status = EcCommand (
             KBC_CMD_STATE,
             KBC_DATA,
             0,
             NULL,
             0x5F,
             0);
             
  gRT->ResetSystem (EfiResetShutdown, EFI_SUCCESS, 0, NULL); 
  return EFI_SUCCESS;
#else
  return EFI_UNSUPPORTED;
#endif
}

