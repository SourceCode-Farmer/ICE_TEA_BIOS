/** @file
  To return the preserved list.

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/SmmOemSvcKernelLib.h>
#include <Guid/H2OSeamlessRecovery.h>
//#include <SgxSetupData.h>
//_Start_L05_BIOS_OPTANE_SUPPORT_
#include <SetupConfig.h>
#include <Guid/L05BackupSetupItemVariable.h>  // EFI_L05_BACKUP_SETUP_ITEM_VARIABLE_GUID
#include <Library/BaseMemoryLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmVariable.h>
//_End_L05_BIOS_OPTANE_SUPPORT_
//_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#include <Guid/L05EspCustomizePostLogoInfoVariable.h>  // EFI_L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_GUID
#include <Guid/L05EspCustomizePostLogoVcmVariable.h>   // EFI_L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_GUID
//_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_

#define PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_GUID      {0xffffffff, 0xffff, 0xffff, { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,0xff, 0xff}}
#define PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_NAME      L"PreservedTable2OtherAllName"
//
// The followings are the sample codes. Please customize here.
//
//[-start-210918-Dongxu0019-modify]//
//
// Don't keep the variable "MeSetupStorage". If ME FW be flashed and it was not, system will confusion
//
PRESERVED_VARIABLE_TABLE_2 mPreservedVariableTable2[] = {
//  VariableGuid    DeleteVariable    VariableName
//  { {0x523260e2, 0x7237, 0x46aa, { 0xbe, 0x3a, 0x20, 0xfb, 0x49, 0xa, 0x4f, 0x3a}}, FALSE, BIOS_GUARD_IOTRAP_DATA_NAME},   // gBiosGuardIotrapDataGuid
  { {0x8BE4DF61, 0x93CA, 0x11d2, {0xAA, 0x0D, 0x00, 0xE0, 0x98, 0x03, 0x2B, 0x8C}}, FALSE, L"PlatformLang"},  // gEfiGlobalVariableGuid 
  { {0x8BE4DF61, 0x93CA, 0x11d2, {0xAA, 0x0D, 0x00, 0xE0, 0x98, 0x03, 0x2B, 0x8C}}, FALSE, L"SecureBoot"},  // gEfiGlobalVariableGuid 
  { {0x1DA748E5, 0x3C39, 0x43E6, {0xB7, 0xAB, 0x55, 0xBA, 0xC7, 0x1A, 0xC7, 0xD2}}, FALSE, L"SetupChange"},  // gH2OSetupChangeVariableGuid
//  { {0x45b5acb9, 0x0359, 0x49be, {0xad, 0xb1, 0x49, 0x37, 0x7b, 0xd6, 0x07, 0xf7}}, FALSE, SGX_SETUP_VARIABLE_NAME},  
//  { {0xd69a279b, 0x58eb, 0x45d1, {0xa1, 0x48, 0x77, 0x1b, 0xb9, 0xeb, 0x52, 0x51}}, FALSE, L"EPCSW"},  // gEpcOsDataGuid   
  { {0x7f9102df, 0xe999, 0x4740, {0x80, 0xa6, 0xb2, 0x03, 0x85, 0x12, 0x21, 0x7b}}, FALSE, L"SystemSupervisorPw"}, // gEfiSupervisorPwGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean password when password in stored in variable.
  { {0x8cf3cfd3, 0xd8e2, 0x4e30, {0x83, 0xff, 0xb8, 0x6f, 0x0c, 0x52, 0x2a, 0x5e}}, FALSE, L"SystemUserPw"}, // gEfiUserPwGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean password when password in stored in variable.
  { {0xe83f7603, 0xa8ed, 0x4b9a, {0x8c, 0x16, 0xb0, 0x15, 0x29, 0xf6, 0x4b, 0x4f}}, FALSE, UPDATE_PROGRESS_NAME}, // gH2OSeamlessRecoveryGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean the update progress stored in variable.
  { {0xfa07a1ea, 0x486d, 0x4b5e, {0xb9, 0x4d, 0x8e, 0x06, 0xe6, 0xfa, 0xd9, 0xdb}}, FALSE, BACKUP_SBB_FILE_DIGEST_SHA256_NAME}, // gH2OSeamlessRecoveryDigestGuid, DeleteVariable = FALSE to keep kernel behavior that flash BIOS doesn't clean the digest stored in variable.
  { {0x5060073d, 0x88c3, 0x421f, {0x82, 0xf8, 0x28, 0x3f, 0x0d, 0x29, 0xed, 0x90}}, FALSE, L"MeCapDigest"}, // gSysFwUpdateDigiestGuid, DeleteVariable = FALSE to keep behavior that flash BIOS doesn't clean the digest stored in variable.
  { {0x5060073d, 0x88c3, 0x421f, {0x82, 0xf8, 0x28, 0x3f, 0x0d, 0x29, 0xed, 0x90}}, FALSE, L"MeCapDigestTemp"}, // gSysFwUpdateDigiestGuid, DeleteVariable = FALSE to keep behavior that flash Me doesn't clean the digest stored in variable.
  { {0xf92b8157, 0xc647, 0x44d7, {0x8d, 0x94, 0x81, 0x7d, 0x18, 0xa2, 0x76, 0xdc}}, FALSE, L"FwUpdateInfo"}, //gSysFwUpdateProgressGuid, DeleteVariable = FALSE to keep behavior that flash Me doesn't clean the digest stored in variable.
//[-start-211108-Dennis0008-add]//
#if defined(S370_SUPPORT)
  { {0x59d1c24f, 0x50f1, 0x401a, {0xb1, 0x01, 0xf3, 0x3e, 0x0d, 0xae, 0xd4, 0x43 }}, FALSE, L"MachineSize"},
#endif
//[-end-211108-Dennis0008-add]//
//_Start_L05_BIOS_OPTANE_SUPPORT_
#ifdef LCFC_SUPPORT
  { EFI_BIOS_BACKUP_SETUP_KEEP_VARIABLE_GUID, FALSE, BIOS_BACKUP_SETUP_KEEP_VARIABLE_NAME},          // BIOSBackupSetupKeep
#endif
//[-end-210918-Dongxu0019-modify]//

//[-start-211109-BAIN000054-modify]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT)||defined(S77014IAH_SUPPORT)
  { {0x567ce681, 0x487d, 0x42ff, {0xb8, 0x75, 0x38, 0xc9, 0x63, 0xe5, 0xbb, 0x4c}}, FALSE, L"TbtRetimer1Version"},
  { {0x125622a2, 0x2aeb, 0x4930, {0xa5, 0xa4, 0xe2, 0xb9, 0x95, 0x5b, 0x6d, 0x9e}}, FALSE, L"TbtRetimer2Version"},
#endif
//[-end-211109-BAIN000054-modify]//
//[-start-211117-BAIN000058-add]//
#ifdef LCFC_SUPPORT
  { {0xE2C5A81A, 0x4F05, 0x477C, {0xBA, 0x4E, 0x49, 0xBD, 0xD5, 0x75, 0xE9, 0xAE }}, FALSE, L"LNFG"},
  { {0x86bbf7e3, 0xb772, 0x4d22, {0x80, 0xa9, 0xe7, 0xc5, 0x8c, 0x3c, 0x7f, 0xf0 }}, FALSE, L"SaveHddPassword"},
  { {0X5FDA220C, 0X9DDC, 0X4588, {0X9D, 0XA7, 0X34, 0X94, 0X6E, 0X00, 0XBF, 0XAD }}, FALSE, L"LVE"},
#endif
//[-end-211117-BAIN000058-add]//
//[-start-20211213-BAIN000066-add]//
#ifdef LCFC_SUPPORT
  { {0x6acce65d, 0xda35, 0x4b39, {0xb6, 0x4b, 0x5e, 0xd9, 0x27, 0xa7, 0xdc, 0x7e }}, FALSE, L"KeepSecureBootMode"},
#endif
//[-end-20211213-BAIN000066-add]//
//[-start-220216-BAIN000096-add]//
#if defined(S77013_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
  { {0x6acce65d, 0xda35, 0x4b39, {0xb6, 0x4b, 0x5e, 0xd9, 0x27, 0xa7, 0xdc, 0x7e }}, FALSE, L"AfterFlashSBBRecovey"},
#endif
//[-end-220216-BAIN000096-add]//

#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  { EFI_L05_BACKUP_SETUP_ITEM_VARIABLE_GUID, FALSE, L05_BACKUP_SETUP_ITEM_VARIABLE_NAME},          // L05BackupSetupItem
#endif
//_End_L05_BIOS_OPTANE_SUPPORT_
//_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#ifdef L05_BIOS_POST_LOGO_DIY_SUPPORT
  { EFI_L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_GUID, FALSE, L05_BIOS_LOGO_DIY_ESP_VARIABLE_NAME},                  // LBLDESP
  { EFI_L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_GUID,  FALSE, L05_BIOS_LOGO_DIY_VERSION_CRC_CONTROL_VARIABLE_NAME},  // LBLDVCC
#endif
//_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_
//_Start_L05_PROJECT_SHIP_SUPPORT_
#ifndef L05_PROJECT_SHIP_SUPPORT_ENABLE
  { PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_GUID, TRUE, PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_NAME},   // Put at here, Other all variable GUID and Other all variable name
#else
  //
  // [ODM request]
  //   After project ship support, keep all Setup item settings (all variable)
  //   which defined in Setup UI while BIOS update.
  //
  // Do not delete any variables.
  //
  { PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_GUID, FALSE, PRESERVED_VARIABLE_TABLE_2_OTHER_ALL_NAME},   // Put at here, Other all variable GUID and Other all variable name
#endif
//_End_L05_PROJECT_SHIP_SUPPORT_
  { {0}, FALSE, NULL}   // End of table
};

//_Start_L05_BIOS_OPTANE_SUPPORT_
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
/**
  This function offers an interface to backup Setup Item.

  @param  None

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
*/
EFI_STATUS
L05BackupSetupItem (
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_VARIABLE_PROTOCOL             *SmmVariable;
  L05_BACKUP_SETUP_ITEM                 L05BackupSetupItem;
  UINTN                                 BufferSize;
  PCH_SETUP                             PchSetup;
  EFI_GUID                              PchSetupVariableGuid = PCH_SETUP_GUID;

  Status      = EFI_SUCCESS;
  SmmVariable = NULL;
  BufferSize  = 0;
  ZeroMem (&L05BackupSetupItem, sizeof (L05_BACKUP_SETUP_ITEM));

  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmVariableProtocolGuid,
                    NULL,
                    &SmmVariable
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  BufferSize = sizeof (PCH_SETUP);
  Status = SmmVariable->SmmGetVariable (
                          PCH_SETUP_VARIABLE_NAME,
                          &PchSetupVariableGuid,
                          NULL,
                          &BufferSize,
                          &PchSetup
                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  L05BackupSetupItem.SataInterfaceMode   = PchSetup.SataInterfaceMode;
  CopyMem (L05BackupSetupItem.RstPcieRemapEnabled, PchSetup.RstPcieRemapEnabled, PCH_MAX_PCIE_ROOT_PORTS);
  L05BackupSetupItem.SataRstOptaneMemory = PchSetup.SataRstOptaneMemory;

  Status = SmmVariable->SmmSetVariable (
                          L05_BACKUP_SETUP_ITEM_VARIABLE_NAME,
                          &gEfiL05BackupSetupItemVariableGuid,
                          (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE),
                          sizeof (L05_BACKUP_SETUP_ITEM),
                          &L05BackupSetupItem
                          );

  return Status;
}
#endif
//[-start-210918-Dongxu0019-add]//
/*
  Stores the values of the Setup Utility options that the user can manipulate in the Variable "BiosBackupSetUpKeep".
*/
EFI_STATUS
BIOSBackupSetupKeep (
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_VARIABLE_PROTOCOL             *SmmVariable;
  BIOS_BACKUP_SETUP_KEEP                BIOSSetupKeep;
  UINTN                                 BufferSize1;
  UINTN                                 BufferSize2;
  UINTN                                 BufferSize3;
  SYSTEM_CONFIGURATION                  SetupVariable;

  SA_SETUP                              SaSetup;
  EFI_GUID                              SaSetupVariableGuid = SA_SETUP_GUID;

  CPU_SETUP                             CpuSetup;
  EFI_GUID                              CpuSetupVariableGuid = CPU_SETUP_GUID;

//[-start-20211213-BAIN000066-add]//
#ifdef LCFC_SUPPORT
  UINT8                                 KeepSecureBootMode = 0;
  UINTN                                 KeepSecureBootModeSize = sizeof (KeepSecureBootMode);
#endif
//[-end-20211213-BAIN000066-add]//
//[-start-220216-BAIN000096-add]//
#if defined(S77013_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
  UINT8                                 AfterFlashSBBRecovey = 1;
  UINTN                                 AfterFlashSBBRecoveySize = sizeof (AfterFlashSBBRecovey);
#endif
//[-end-220216-BAIN000096-add]//

  Status      = EFI_SUCCESS;
  SmmVariable = NULL;
  BufferSize1  = 0;
  BufferSize2  = 0;
  BufferSize3  = 0;


  ZeroMem (&BIOSSetupKeep, sizeof (BIOS_BACKUP_SETUP_KEEP));

  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmVariableProtocolGuid,
                    NULL,
                    &SmmVariable
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

//Setup
  BufferSize1 = sizeof (SYSTEM_CONFIGURATION);
  Status = SmmVariable->SmmGetVariable (
                          L"Setup",
                          &gSystemConfigurationGuid,
                          NULL,
                          &BufferSize1,
                          &SetupVariable
                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

//SaSetup
  BufferSize2 = sizeof (SA_SETUP);
  Status = SmmVariable->SmmGetVariable (
                          SA_SETUP_VARIABLE_NAME,
                          &SaSetupVariableGuid,
                          NULL,
                          &BufferSize2,
                          &SaSetup
                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

//CpuSetup
  BufferSize3 = sizeof (CPU_SETUP);
  Status = SmmVariable->SmmGetVariable (
                          CPU_SETUP_VARIABLE_NAME,
                          &CpuSetupVariableGuid,
                          NULL,
                          &BufferSize3,
                          &CpuSetup
                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }
  
  //
  //Configuration Setup Items.
  //
  BIOSSetupKeep.L05WirelessLan                       = SetupVariable.L05WirelessLan;
  
  BIOSSetupKeep.VmdEnable                            = SaSetup.VmdEnable;
  
  BIOSSetupKeep.PrimaryDisplay                       = SaSetup.PrimaryDisplay;
  
  BIOSSetupKeep.VT                                   = CpuSetup.VT;
  
  BIOSSetupKeep.EnableVtd                            = SaSetup.EnableVtd;
  
  BIOSSetupKeep.HyperThreading                       = CpuSetup.HyperThreading;
  
  BIOSSetupKeep.L05BiosBackFlash                     = SetupVariable.L05BiosBackFlash;
  
  BIOSSetupKeep.L05HotKeyMode                        = SetupVariable.L05HotKeyMode;
  
  BIOSSetupKeep.L05FoolProofFnCtrl                   = SetupVariable.L05FoolProofFnCtrl;
  
  BIOSSetupKeep.L05AlwaysOnUsb                       = SetupVariable.L05AlwaysOnUsb;
  
  BIOSSetupKeep.L05ChargeInBatteryMode               = SetupVariable.L05ChargeInBatteryMode;
  
  BIOSSetupKeep.L05DisableBuildingInBatterySupport   = SetupVariable.L05DisableBuildingInBatterySupport;

  BIOSSetupKeep.L05SystemPerformanceMode             = SetupVariable.L05SystemPerformanceMode;
  
  BIOSSetupKeep.L05ThermalMode                       = SetupVariable.L05ThermalMode;
  
  BIOSSetupKeep.L05FlipToBoot                        = SetupVariable.L05FlipToBoot;
  
  BIOSSetupKeep.L05OneKeyBattery                     = SetupVariable.L05OneKeyBattery;
//[-start-21119-TAMT000032-modify]//
//[-start-211203-QINGLIN0125-modify]//
//[-start-211206-OWENWU0029-modify]//
//[-start-211214-Ching000017-modify]//
#if defined(C970_BSH_SUPPORT) || defined(C770_BSH_SUPPORT) || defined(S77014_BSH_SUPPORT) || defined(S370_BSH_SUPPORT) || defined(S570_BSH_SUPPORT) || defined(S77013_BSH_SUPPORT) || defined(S77014IAH_BSH_SUPPORT)
  BIOSSetupKeep.L05BiosSelfHealing                   = SetupVariable.L05BiosSelfHealing;
#endif
//[-end-211214-Ching000017-modify]//
//[-end-211206-OWENWU0029-modify]//
//[-end-211203-QINGLIN0125-modify]//
//[-end-21119-TAMT000032-modify]//
//[-start-211124-kebin000069-modify]//
//[-start-220113-YUNLEI0158-modify]//
//[-start-220117-SHAONN0027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT)
 BIOSSetupKeep.L05UltraQuietMode            =       SetupVariable.L05UltraQuietMode;
#endif  
//[-end-220117-SHAONN0027-modify]//
//[-end-220113-YUNLEI0158-modify]//
//[-end-211124-kebin000069-modify]// 

  //
  //Security Setup Items.
  //

  BIOSSetupKeep.L05DeviceGuard                       = SetupVariable.L05DeviceGuard;
  BIOSSetupKeep.L05SecureBoot                        = SetupVariable.L05SecureBoot;
  BIOSSetupKeep.L05PlatformMode                      = SetupVariable.L05PlatformMode;
  BIOSSetupKeep.L05SecureBootMode                    = SetupVariable.L05SecureBootMode;
//[-start-211117-BAIN000058-add]//
//[-start-211207-SHAONN0023-add]//
#ifdef LCFC_SUPPORT
  BIOSSetupKeep.L05PowerOnPassword                   = SetupVariable.PowerOnPassword;
  BIOSSetupKeep.L05NaturalFileGuard                  = SetupVariable.L05NaturalFileGuard;
  BIOSSetupKeep.L05FtpmEnable                        = SetupVariable.L05FtpmEnable;
#endif
//[-end-211207-SHAONN0023-add]//
//[-end-211117-BAIN000058-add]//

  //
  //Boot Setup Items.
  //
  BIOSSetupKeep.L05FastBoot                          = SetupVariable.L05FastBoot;
  BIOSSetupKeep.UsbBoot                              = SetupVariable.UsbBoot;
  BIOSSetupKeep.PxeBootToLan                         = SetupVariable.PxeBootToLan;
  BIOSSetupKeep.L05PxeIpv4First                      = SetupVariable.L05PxeIpv4First;

  Status = SmmVariable->SmmSetVariable (
                          BIOS_BACKUP_SETUP_KEEP_VARIABLE_NAME,
                          &gEfiBIOSBackupSetupKeepVariableGuid,
                          (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE),
                          sizeof (BIOS_BACKUP_SETUP_KEEP),
                          &BIOSSetupKeep
                          );
  if (EFI_ERROR (Status)) {
    return Status;
  }
//[-start-20211213-BAIN000066-add]//
#ifdef LCFC_SUPPORT
  Status = SmmVariable->SmmSetVariable (
                L"KeepSecureBootMode",
                &gLfcVariableGuid,
                ( EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE),
                KeepSecureBootModeSize,
                &KeepSecureBootMode
                );
  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif
//[-end-20211213-BAIN000066-add]//

//[-start-220216-BAIN000096-add]//
#if defined(S77013_SUPPORT)  || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
  Status = SmmVariable->SmmSetVariable (
               L"AfterFlashSBBRecovey",
                &gLfcVariableGuid,
                (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE),
                AfterFlashSBBRecoveySize,
                &AfterFlashSBBRecovey
                );
  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif
//[-end-220216-BAIN000096-add]//

  return Status;
}
//[-end-210918-Dongxu0019-add]//
//_End_L05_BIOS_OPTANE_SUPPORT_

/**
  To return the preserved list.

  @param[in, out]  VariablePreservedTable    Pointer to the table of preserved variables.
  @param[in, out]  IsKeepVariableInList      True: The variables in the table should be preserved.
                                             FALSE: The variables in the table should be deleted.

  @retval          EFI_UNSUPPORTED           Returns unsupported by default.
  @retval          EFI_SUCCESS               The work to delete the variables is completed.
  @retval          EFI_MEDIA_CHANGED         The table of preserved variables is updated.
**/
EFI_STATUS
OemSvcVariablePreservedTable2 (
  IN OUT PRESERVED_VARIABLE_TABLE_2               **VariablePreservedTable2
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  //
  // The followings are the sample codes. Please customize here.
  //

//_Start_L05_BIOS_OPTANE_SUPPORT_
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  L05BackupSetupItem ();
#endif
//_End_L05_BIOS_OPTANE_SUPPORT_
//[-start-210918-Dongxu0019-add]//
#ifdef LCFC_SUPPORT
    BIOSBackupSetupKeep();
#endif
//[-end-210918-Dongxu0019-add]//

  *VariablePreservedTable2 = mPreservedVariableTable2;
  return EFI_MEDIA_CHANGED;

  //return EFI_UNSUPPORTED;
}

/**
  Old function OemSvcVariablePreservedTable to return the old sturcture (PRESERVED_VARIABLE_TABLE) preserved list.
  The process will launch new function OemSvcVariablePreservedTable2 first,
  and then launch old function OemSvcVariablePreservedTable only when new function OemSvcVariablePreservedTable2
  return EFI_UNSUPPORTED.
  According to logo requirement, should preserve all UEFI variables with VendorGuid
  {77fa9abd-0359-4d32-bd60-28f4e78f784b}, so the those UEFI variables with this VendorGuidVendorGuid
  will not be limited in this table when if table is filled with those special variables to remove in table,
  this table parse process will skip those.

  @param[in, out]  VariablePreservedTable    Pointer to the table of preserved variables.
                                             (old structure PRESERVED_VARIABLE_TABLE)
  @param[in, out]  IsKeepVariableInList      True: The variables in the table should be preserved.
                                             FALSE: The variables in the table should be deleted.

  @retval          EFI_UNSUPPORTED           Returns unsupported by default.
  @retval          EFI_SUCCESS               The work to delete the variables is completed.
  @retval          EFI_MEDIA_CHANGED         The table of preserved variables is updated.
**/
EFI_STATUS
OemSvcVariablePreservedTable (
  IN OUT PRESERVED_VARIABLE_TABLE              **VariablePreservedTable,
  IN OUT BOOLEAN                               *IsKeepVariableInList
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  //
  // The followings are the sample codes. Please customize here.
  //
  //*IsKeepVariableInList = TRUE;
  //*VariablePreservedTable = mPreservedVariableTable;
  //return EFI_MEDIA_CHANGED;

  return EFI_UNSUPPORTED;
}



