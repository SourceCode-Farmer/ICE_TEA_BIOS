/** @file
  Feature Hook for getting ApCheck Status

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GET_AP_CHECK_STATUS_H_
#define _GET_AP_CHECK_STATUS_H_

EFI_STATUS
GetApCheckStatus (
  IN OUT UINT8                          *ApStatus
  );


#endif
