@echo off

if "%1"=="J1" (
echo build J1
cd BIOS
cd C770_SIT_Pkg
call c770_evn_pkg_password_crisis.bat
cd ..
cd ..
) else if "%1"=="HN" (
echo build HN
cd BIOS
cd C970_SIT2_Pkg
call c970_evn_pkg_password_crisis.bat
cd ..
cd ..
) else if "%1"=="JH" (
echo build JH
cd BIOS
cd S77014_SITR_Pkg
call s77014_evn_pkg_password_crisis.bat
cd ..
cd ..
) else (
echo buld ID: %1
)
