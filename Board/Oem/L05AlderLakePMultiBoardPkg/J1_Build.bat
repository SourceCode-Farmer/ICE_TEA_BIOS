@REM environment setup
@set FILE_SUFFIX_BUILD_ID=J1
@set IFDPACKER_NAME=InsydeH2OFFT_x86_WIN_6.50-Beta1

@REM [-start-210919-TAMT000008-add]
@del  Project.var
@copy Project_%FILE_SUFFIX_BUILD_ID%.var Project.var /Y
@REM [-end-210919-TAMT000008-add]

@del  Project.dsc
@copy Project_%FILE_SUFFIX_BUILD_ID%.dsc Project.dsc /Y

@del  Project.env
@copy Project_%FILE_SUFFIX_BUILD_ID%.env Project.env /Y

@del  Project.fdf
@copy Project_%FILE_SUFFIX_BUILD_ID%.fdf Project.fdf /Y

@del  Project.uni
@copy Project_%FILE_SUFFIX_BUILD_ID%.uni Project.uni /Y

@del ProjectPostBuild.bat
@copy ProjectPostBuild_%FILE_SUFFIX_BUILD_ID%.bat ProjectPostBuild.bat /Y

@del  FactoryCopyInfo\SecureFlash.cer
@copy FactoryCopyInfo\SecureFlash_%FILE_SUFFIX_BUILD_ID%.cer FactoryCopyInfo\SecureFlash.cer /Y
@del LfcBprInput\ProjectConfig.ini
@copy LfcBprInput\ProjectConfig_%FILE_SUFFIX_BUILD_ID%.ini LfcBprInput\ProjectConfig.ini /Y
@del  FactoryCopyInfo\FactoryDefaultConfig.ini
@copy FactoryCopyInfo\FactoryDefaultConfig_%FILE_SUFFIX_BUILD_ID%.ini FactoryCopyInfo\FactoryDefaultConfig.ini /Y

@REM [-start-211229-YUNLEI0155-add]
@del  ..\..\..\Intel\AlderLake\AlderLakePlatSamplePkg\Setup\ConnectivitySetup.hfr
@copy ..\..\..\Intel\AlderLake\AlderLakePlatSamplePkg\Setup\ConnectivitySetup_C770.hfr ..\..\..\Intel\AlderLake\AlderLakePlatSamplePkg\Setup\ConnectivitySetup.hfr /Y
@REM [-end-211229-YUNLEI0155-add]
@REM for C770_SIT2_Pkg sync with LfcBprInput\Platform_NAC.ini
@copy /y LfcBprInput\Platform_NAC_%FILE_SUFFIX_BUILD_ID%.ini FlashTool\%IFDPACKER_NAME%\Platform_NAC.ini /Y
@copy /y LfcBprInput\Platform_REL_%FILE_SUFFIX_BUILD_ID%.ini FlashTool\%IFDPACKER_NAME%\Platform_REL.ini /Y
@copy /y LfcBprInput\Platform_MFG_%FILE_SUFFIX_BUILD_ID%.ini FlashTool\%IFDPACKER_NAME%\Platform_SMT.ini /Y

@rem re config default path, build ID, BIOS version
@echo off&setlocal EnableDelayedExpansion

cd ..\..\..\
set svn_version=0
for /f "tokens=4* delims= " %%i in ('"svn info | find "Last Changed Rev""') do set svn_version=%%i
echo %svn_version%
cd Board\Oem\L05AlderLakePMultiBoardPkg\

set configuration_build_ID=    "BuildId": "%FILE_SUFFIX_BUILD_ID%",
set configuration_bios_ver=    "BiosVer": "99T%svn_version%_%time:~1,1%%time:~3,2%",
set configuration_cur_path=    "p": "%CD%",
set configuration_cur_path0=    "p0": "%CD%"
set configuration_cur_path2=%configuration_cur_path:\=/%
set configuration_cur_path20=%configuration_cur_path0:\=/%

set config_count=0

if exist LfcBpr\config.json.temp (
del LfcBpr\config.json.temp
)

if exist LfcBpr\config.json (
del LfcBpr\config.json
)

for /f "delims=" %%a in (LfcBpr\config.json.C770) do (
set /a config_count+=1
set configuration_str=%%a
if !config_count!==2 (
echo !configuration_build_ID!>>LfcBpr\config.json.temp
) else if !config_count!==3 (
echo !configuration_bios_ver!>>LfcBpr\config.json.temp
) else if !config_count!==9 (
echo !configuration_cur_path2!>>LfcBpr\config.json.temp
) else if !config_count!==10 (
echo !configuration_cur_path20!>>LfcBpr\config.json.temp
) else (
echo !configuration_str!>>LfcBpr\config.json.temp
)
)


move LfcBpr\config.json.temp LfcBpr\config.json

@REM @del  FactoryCopyInfo\SecureFlash.cer
@REM @copy FactoryCopyInfo\Yoga-C750.cer FactoryCopyInfo\SecureFlash.cer /Y
 
@REM @del  ProjectPostBuild.bat
@REM @copy ProjectPostBuild_%FILE_SUFFIX_BUILD_ID%.bat ProjectPostBuild.bat /Y
 
@REM @del  FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_NAC.ini
@REM @copy FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_NAC_Golden.ini FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_NAC.ini /Y
 
@REM @del  FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_REL.ini
@REM @copy FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_REL_Golden.ini FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_REL.ini /Y
 
@REM @del  FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_SMT.ini
@REM @copy FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_SMT_Golden.ini FlashTool\InsydeH2OFFT_x86_WIN_6.31.00\Platform_SMT.ini /Y
 
@REM @del  FactoryCopyInfo\FactoryDefaultConfig.ini
@REM @copy FactoryCopyInfo\FactoryDefaultConfig_Golden.ini FactoryCopyInfo\FactoryDefaultConfig.ini /Y
call b.bat