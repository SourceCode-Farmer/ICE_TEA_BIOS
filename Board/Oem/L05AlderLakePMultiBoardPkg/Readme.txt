#;******************************************************************************
#;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
#;*
#;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;* transmit, broadcast, present, recite, release, license or otherwise exploit
#;* any part of this publication in any form, by any means, without the prior
#;* written permission of Insyde Software Corporation.
#;*
#;******************************************************************************
#

The ChiefRiver platform package is compatible to HuronRiver platform.
The output firmware can be used on both ChiefRiver or HuronRiver platforms.

Build procedure.
1. Modifiy the platform settings and compiler tool settings in PlatformBuild.bat
   under the platform package folder (ChiefRiverPkg)
2. Executing PlatformBuild.bat by clicking it on the file manager.
3. To build release version, type
     nmake uefi64
   to build DDT debug version, type
     nmake uefi64ddt

