@echo off

if "%1"=="HN" (
echo build HN
cd BIOS
cd C970_BootGuardLSign
call C970_BootGuardLSign.bat
cd ..
cd ..
) else if "%1"=="JH" (
echo build JH
cd BIOS
cd S77014_BootGuardLSign
call S77014_BootGuardLSign.bat
cd ..
cd ..
) else if "%1"=="J1" (
echo build J1
cd BIOS
cd C770_BootGuardLSign
call C770_BootGuardLSign.bat
cd ..
cd ..
) else if "%1"=="JK" (
echo build JK
cd BIOS
cd S370_BootGuardLSign
call S370_BootGuardLSign.bat
cd ..
cd ..
) else if "%1"=="JB" (
echo build JB
cd BIOS
cd S570_BootGuardLSign
call S570_BootGuardLSign.bat
cd ..
cd ..
) else if "%1"=="KR" (
echo build KR
cd BIOS
cd S77014IAH_BootGuardLSign
call S77014IAH_BootGuardLSign.bat
cd ..
cd ..
) else if "%1"=="K2" (
echo build K2
cd BIOS
cd S77013_BootGuardLSign
call S77013_BootGuardLSign.bat
cd ..
cd ..
)else (
echo buld ID: %1
)

