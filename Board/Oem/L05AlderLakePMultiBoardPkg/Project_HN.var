## @file
#  File to declare variable default information
#
#******************************************************************************
#* Copyright (c) 2018 - 2021, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
# This file is ini format and Comments are prefixed with a # and continue to the end of the line.
#

!include InsydeModulePkg/Package.var
!include AlderLakeChipsetPkg/Package.var
#_Start_L05_FEATURE_
!include InsydeL05PlatformPkg/Package.var
#_End_L05_FEATURE_

#
# Optional [Guids] section. uses to declare guid which used in this file.
#
[Guids]
  gSetupVariableGuid    = {0xEC87D643, 0xEBA4, 0x4BB5, {0xA1, 0xE5, 0x3F, 0x3E, 0x36, 0xB2, 0x0D, 0xA9}}
  gSaSetupVariableGuid  = {0x72c5e28c, 0x7783, 0x43a1, {0x87, 0x67, 0xfa, 0xd7, 0x3f, 0xcc, 0xaf, 0xa4}}
  gCpuSetupVariableGuid = {0xb08f97ff, 0xe6e8, 0x4193, {0xa9, 0x97, 0x5e, 0x9e, 0x9b, 0xa,  0xdb, 0x32}}
  gPchSetupVariableGuid = {0x4570b7f1, 0xade8, 0x4943, {0x8d, 0xc3, 0x40, 0x64, 0x72, 0x84, 0x23, 0x84}}
  gMeSetupVariableGuid  = {0x5432122d, 0xd034, 0x49d2, {0xa6, 0xde, 0x65, 0xa8, 0x29, 0xeb, 0x4c, 0x74}}
  gSiSetupVariableGuid  = {0xAAF8E719, 0x48F8, 0x4099, {0xA6, 0xF7, 0x64, 0x5F, 0xBD, 0x69, 0x4C, 0x3D}}
  gSystemConfigurationGuid = {0xA04A27f4, 0xDF00, 0x4D42, {0xB5, 0x52, 0x39, 0x51, 0x13, 0x02, 0x11, 0x3D}}

#
# The [Packages] section lists all of the package declaration files that contain
# GUIDs or PCDs that are used by this variable declaration file.
#
[Packages]
  AlderLakeChipsetPkg/AlderLakeChipsetPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec

#
# Below can write Var section to introduce a single UEFI variable default value for the specified
# default stores and specified SKU.
#
# Section format is [Var.VariableGUID.VariableName] for example [Var.gEfiFileInfoGuid.MyTestVar]
#
###########################################################################
# The default setting is come from
# 1. Intel\AlderLake\AlderLakeBoardPkg\AlderLake"X"Boards\Include\AlderLake"X"BoardConfigPatchTable.h
#    Note: "X" means "U", "Y" or "H" SKU
# 2. Command "cond" in Intel RC *.hfr file
# 3. Variable which without default value
# The Variable Offset defined in file AdvanceVfr.lst(build folder)
#--------------------------------------------------------------------------
#  0x3F|BoardIdAdlPSimics
#  0x10|BoardIdAdlPLp4Rvp
#  0x12|BoardIdAdlPDdr5Rvp
#  0x13|BoardIdAdlPLp5Rvp
#  0x19|BoardIdAdlPLp4Bep
#  0x1A|BoardIdAdlPLp5Aep
#  0x1C|BoardIdAdlPMMAep
#  0x1E|BoardIdAdlPDdr5Dg384Aep
#  0x1F|BoardIdAdlPLp5Dg128Aep
#  0x14|SkuIdAdlPDdr4Rvp
#  0x16|BoardIdAdlPDdr5MRRvp
#  0x00|BoardDefault

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0
  Struct = SETUP_DATA
  Data = name=Rtd3Support                                | (1)
#[-start-211229-GEORGE0040-modify]#
         name=StorageRtd3Support                         | (1)
#[-end-211229-GEORGE0040-modify]#

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x3F,0x10,0x19,0x1A,0x1F
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (3)
         name=AuxOriOverrideSupport                      | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=HebcValueSupport                           | (1)
         name=SensorHubType                              | (1)
         name=Rp08D3ColdSupport                          | (1)

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x16
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (3)
         name=AuxOriOverrideSupport                      | (0)
         name=DisableTbtPcieTreeBme                      | (0)
         name=HebcValueSupport                           | (1)
         name=DiscreteTbtPlatformConfigurationSupport    | (1)
         name=ITbtPcieRootPortSupported[0]               | (0)
         name=ITbtPcieRootPortSupported[1]               | (0)
         name=ITbtPcieRootPortSupported[2]               | (0)
         name=ITbtPcieRootPortSupported[3]               | (0)
         name=ITbtRootPort[0]                            | (0)
         name=ITbtRootPort[1]                            | (0)
         name=ITbtRootPort[2]                            | (0)
         name=ITbtRootPort[3]                            | (0)
         name=IntegratedTbtSupport                       | (0)
         name=Rp08D3ColdSupport                          | (0)

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x12,0x1E
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (3)
         name=AuxOriOverrideSupport                      | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=DisableTbtPcieTreeBme                      | (0)
         name=ControlIommu                               | (1)
         name=CvfSupport                                 | (0)

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x14
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (1)
         name=AuxOriOverrideSupport                      | (1)
         name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=HebcValueSupport                           | (1)
         name=SensorHubType                              | (1)
         name=Rp08D3ColdSupport                          | (1)
         name=CvfSupport                                 | (0)
         name=TcssUcmDevice                              | (0)


[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x13
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (1)
         name=AuxOriOverrideSupport                      | (1)
         name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=HebcValueSupport                           | (1)
         name=SensorHubType                              | (1)
         name=Rp08D3ColdSupport                          | (1)
         name=AuxOriOverride                             | (0)

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x1B
  Struct = SETUP_DATA
  Data = name=PchI2cSensorDevicePort[0]                  | (3)
         name=AuxOriOverrideSupport                      | (0)
         name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=DisableTbtPcieTreeBme                      | (0)
         name=HebcValueSupport                           | (1)
         name=SensorHubType                              | (1)
         name=Rp08D3ColdSupport                          | (1)
         name=PowermeterDeviceEnable                     | (1)
         name=CvfSupport                                 | (0)
         name=PchI2cTouchPadType                         | (7)
         name=MipiCam_ControlLogic0                      | (1)
         name=MipiCam_ControlLogic1                      | (0)
         name=MipiCam_ControlLogic2                      | (0)
         name=MipiCam_ControlLogic3                      | (0)
         name=MipiCam_Link0                              | (1)
         name=MipiCam_Link1                              | (0)
         name=MipiCam_Link1_FlashDriverSelection         | (0)
         name=MipiCam_Link2                              | (0)
         name=MipiCam_Link3                              | (0)
         name=MipiCam_ControlLogic0_Type                 | (2)
         name=MipiCam_ControlLogic0_CrdVersion           | (50)
         name=MipiCam_ControlLogic0_InputClock           | (10)
         name=MipiCam_ControlLogic0_PchClockSource       | (0)
         name=MipiCam_ControlLogic0_Pld                  | (29)
         name=MipiCam_ControlLogic0_I2cChannel           | (1)
         name=MipiCam_Link0_SensorModel                  | (16)
         name=MipiCam_Link0_LanesClkDiv                  | (0)
         name=MipiCam_Link0_DriverData_CrdVersion        | (50)
         name=MipiCam_Link0_DriverData_ControlLogic      | (0)
         name=MipiCam_Link0_CameraPhysicalLocation       | (61)
         name=MipiCam_Link0_DriverData_FlashSupport      | (3)
         name=MipiCam_Link0_DriverData_PmicPosition      | (1)
         name=MipiCam_Link0_ModuleName[0]                | (35)
         name=MipiCam_Link0_ModuleName[1]                | (36)
         name=MipiCam_Link0_ModuleName[2]                | (42)
         name=MipiCam_Link0_ModuleName[3]                | (36)
         name=MipiCam_Link0_DriverData_LinkUsed          | (1)
         name=MipiCam_Link0_DriverData_LaneUsed          | (1)
         name=MipiCam_Link0_DriverData_EepromType        | (0)
         name=MipiCam_Link0_DriverData_VcmType           | (0)
         name=MipiCam_Link0_I2cDevicesEnabled            | (1)
         name=MipiCam_Link0_I2cChannel                   | (1)
         name=MipiCam_Link0_I2cAddress[0]                | (37)
         name=Rtd3Support                                | (0)
         name=StorageRtd3Support                         | (1)

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x12,0x16
  Struct = SETUP_DATA
  Data = name=TcssUcmDevice                              | (0)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3F
  Struct = SA_SETUP
  Data = name=VmdEnable                         | (0)
         name=VmdGlobalMapping                  | (0)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x16
  Struct = SA_SETUP
  Data = name=TcssItbtPcie0En                   | (0)
         name=TcssItbtPcie1En                   | (0)
         name=TcssItbtPcie2En                   | (0)
         name=TcssItbtPcie3En                   | (0)
         name=TcssDma0En                        | (0)
         name=TcssDma1En                        | (0)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x13
  Struct = SA_SETUP
  Data = name=TcssItbtPcie2En                   | (0)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x13
  Struct = SA_SETUP
#[-start-210712-Dongxu0010-modify]#
  Data = name=DdrFreqLimit        | (0)
#[-end-210712-Dongxu0010-modify]#

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x1E,0x1F,0x1C
  Struct = SA_SETUP
  Data = name=PrimaryDisplay      | (4)

[Var.gCpuSetupVariableGuid.CpuSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x10,0x12,0x13,0x19,0x1A,0x1B,0x1E,0x1F,0x14
  Struct = CPU_SETUP
  Data = name=TmeEnable                    | (0)
         name=EnergyEfficientTurbo         | (1)
#[-start-220210-FLINT00038-modify]#
         name=Irms                         | (0)
#[-end-220210-FLINT00038-modify]#

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x3F,0x10,0x12,0x13,0x19,0x1A,0x1B,0x1E,0x1F,0x14
  Struct = PCH_SETUP
  Data = name=PmcLpmS0i2p1En               | (0)
#[-start-210817-YUNLEI0120-modify]
         name=PchSerialIoSpi[1]            | (0)
#[-end-210817-YUNLEI0120-modify]
         name=PchUartHwFlowCtrl[0]         | (0)
         name=PchSerialIoI2c[0]            | (1)
         name=PchSerialIoI2c[1]            | (1)
         name=PchSerialIoI2c[2]            | (0)
         name=PchSerialIoI2c[3]            | (0)
         name=PchSerialIoI2c[4]            | (0)
#[-start-210830-BAIN000034-modify]#
         name=PchSerialIoI2c[5]            | (0)
#[-end-210830-BAIN000034-modify]#
         name=PchIshGpEnable[7]            | (1)
         name=CnviBtAudioOffload           | (1)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x19,0x1A,0x1D
  Struct = PCH_SETUP
  Data = name=PchHdAudioHdaLinkEnable      | (0)
         name=PchHdAudioSndwLinkEnable[0]  | (1)
         name=PchHdAudioSndwLinkEnable[1]  | (1)
         name=PchHdAudioSndwLinkEnable[2]  | (1)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x1B
  Struct = PCH_SETUP
  Data = name=PchLan                       | (0)
         name=ThcPort0Assignment           | (1)
         name=PchHdAudioHdaLinkEnable      | (0)
         name=PchHdAudioSndwLinkEnable[0]  | (1)
         name=PchHdAudioSndwLinkEnable[1]  | (1)
         name=PchHdAudioSndwLinkEnable[2]  | (1)

[Var.gSystemConfigurationGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x10,0x14
  Struct = CHIPSET_CONFIGURATION
  Data = name=HgSlot                       | (1)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x14
  Struct = SA_SETUP
  Data = name=TcssItbtPcie3En              | (0) 

#--------------------------------------------------------------------------
#  0x01|BoardIdAdlMLp4Rvp
#  0x02|BoardIdAdlMLp5Rvp
#  0x03|BoardIdAdlMLp5PmicRvp
#  0x0F|BoardIdAdlMLp5Aep

[Var.gSetupVariableGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0x01,0x02,0x0F,0x03
  Struct = SETUP_DATA
  Data = name=DiscreteTbtPlatformConfigurationSupport    | (0)
         name=PchI2cSensorDevicePort[0]                  | (3)
         name=AuxOriOverrideSupport                      | (0)
         name=HebcValueSupport                           | (1)
         name=SensorHubType                              | (1)

[Var.gPchSetupVariableGuid.PchSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x01,0x02,0x0F,0x03
  Struct = PCH_SETUP
#[-start-210817-YUNLEI0120-modify]
  Data = name=PchSerialIoSpi[1]            | (0)
#[-end-210817-YUNLEI0120-modify]
         name=PchUartHwFlowCtrl[0]         | (0)
         name=PchSerialIoI2c[0]            | (1)
         name=PchSerialIoI2c[1]            | (1)
         name=PchSerialIoI2c[2]            | (0)
         name=PchSerialIoI2c[3]            | (0)
         name=PchSerialIoI2c[4]            | (0)
         name=PchSerialIoI2c[5]            | (1)
         name=PchIshGpEnable[7]            | (1)
         name=PmcLpmS0i2p1En               | (0)
         name=CnviBtAudioOffload           | (1)

[Var.gSaSetupVariableGuid.SaSetup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId =  0x03
  Struct = SA_SETUP
  Data = name=VbtSelect                    | (1)

[Var.gSystemConfigurationGuid.Setup]
  Attribs = BS | RT
  DefaultId = 0
  SkuId = 0
  Struct = KERNEL_CONFIGURATION
  Data = name=AcpiVer                      | (7)
