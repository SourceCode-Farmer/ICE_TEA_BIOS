from Lib.lcfc_lib import *
from Lib.action_build import ActionBuild
from Lib.action_email import ActionEmail
from Lib.action_pack import ActionPack
from Lib.action_tool import ActionTool
from Lib.action_file import ActionFile
from Lib.action_txt import ActionTxt
from Lib.action_docx import ActionDocx
from Lib.action_ftp import ActionFtp
from Lib.action_svn import ActionSvn
from Lib.action_html import ActionHtml
from Lib.action_cmd import ActionCmd
from Lib.action_gui import LfcLabelFrame
from Lib.action_gui import LfcRadiobuttonGroup
from Lib.LcfcGetRawFileLib import lcfc_get_raw_file
import os
import datetime
from functools import reduce
import configparser
import argparse
import sys
import threading
import signal
import tkinter as tk
from tkinter.scrolledtext import ScrolledText
from tkinter import Radiobutton
from tkinter import IntVar
from tkinter import messagebox
from tkinter import filedialog
from tkinter import Listbox
import subprocess
import time

tool_version = 'V2.01'
tool_release_date = '2021/05/28'

fun = {}
project_config = {}
support_ini_version = ['1.05']

BIOS_VER_TEMPLATE = '29'
BIOS_VER_FULL_TEMPLATE = 'A7CN29WW'
BIOS_VER_FULL_TEST_TEMPLATE = 'A7CN29WWT01'
CRISIS_VER_FULL_TEMPLATE = 'A7Crisis'
MARKET_NAME_TEMPLATE = 'S540-13ARE'
RELEASE_DATE_TEMPLATE = '2000-00-00'


def get_action_lenovo_sign(n):
    global fun

    if n == 'prepare_time_stamp':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_create,
                           file= r'time_stamp.txt',
                           content = 'time stamp:'+str(time.time()))
        return fun[n]

    if n == 'get_time_stamp':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_read,
                           file=fun['prepare_time_stamp'].action_output['file'],
                           )
        return fun[n]

    if n == 'send_email_to_sign':
        print(' folder       :', os.path.join(g.working_path_abs,'input.7z'))
        send_para = {
            'email_receiver': 'biosromagent@lenovo.com',
            'email_cc': '',
            'email_bcc': '',
            'email_subject': 'sign BIOS %s %s' % (g.build_id, fun['get_time_stamp'].read_output),
            'email_content': '',
            'email_attachment': [os.path.join(g.working_path_abs,
                                              'input.7z')],
        }
        fun[n] = ActionEmail(sub_function=ActionEmail.email_send,
                             para_dict=send_para)
        return fun[n]



    if n == 'wait_for_sign_back':
        receive_para = {
            'email_sender': 'biosromagent@lenovo.com',
            'email_subject': 'RE: [External] sign BIOS %s %s'% (g.build_id, fun['get_time_stamp'].read_output),
            'email_deadline': datetime.datetime.now()
        }
        fun[n] = ActionEmail(sub_function=ActionEmail.email_receive,
                             para_dict=receive_para)
        return fun[n]

    if n == 'tool_7z_uncompress1':
        para = r'e %s -o"$output$\Unzipped1" -plenovo' % \
               (os.path.join(g.working_path_abs, fun['wait_for_sign_back'].action_output['file']))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='Unzipped1')
        return fun[n]

    if n == 'tool_7z_uncompress2':
        _input = os.path.join(g.working_path_abs, fun['tool_7z_uncompress1'].action_output['file'])
        file_name = [_i for _i in os.listdir(_input) if '.7z' in _i][0]
        para = r'e %s -o"$output$\Unzipped2"' % (os.path.join(_input, file_name))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='Unzipped2')
        return fun[n]

    return None

def get_action(n):
    global fun
    DebugPrint('get_action n = '+ str(n))
    
    if fun.get(n):
        DebugPrint('found %s in fun.get ' % n)
        DebugPrint('fun.get(n) = ' + str(fun.get(n)))
        return fun.get(n)
    #get_action n = build
    #fun_list = [<Lib.action_build.ActionBuild object at 0x000002BA5799AB80>, None, None, None, None]

    # get_action n = pack_lnv_sign
    #fun_list = [None, None, None, None, < Lib.action_pack.ActionPack object at 0x0000021447296040 >]

    #get_action n = txt_modify_cfg
    #fun_list = [None, None, None, None, < Lib.action_txt.ActionTxt object at 0x0000021447296D30 >]

    #fun_list at any time,only has one member.
    fun_list = [
        get_action_lenovo_sign(n),
    ]

    
    DebugPrint('fun_list = '+ str(fun_list))
    for _i in fun_list:
        if _i:
            return _i

def global_run(_action_list):
    if not _action_list:
        error_handle(0, 'Not a correct action list, please check it')
        return STATUS_ERROR
    print('------------------------------------------------------------')
    #_action_list = (lenovo_sign) + action + (formal_pack)/(test pack)
    #flat_action_list = _action_list ? on meaning
    flat_action_list = _action_list if len(_action_list) == 1 else \
        reduce(lambda x, y: (x if isinstance(x, list) else [x]) + (y if isinstance(y, list) else [y]), _action_list)

    DebugPrint('flat_action_list = '+ str(flat_action_list))
    for _count, _action_str in enumerate(flat_action_list):
        _action = get_action(_action_str)
        if get_var_from_disk('%02d_%s_result' % (_count, _action_str)) == STATUS_SUCCESS:
            # Executed and pass, should be previous result, so skip
            print('[Action %02d] %s executed, result success, so skipped' % (_count, _action_str))
            _action.get_saved_output(_count, _action_str)
            continue
        if not _action:
            error_handle(0, '[Action %02d] %s not exist' % (_count, _action_str))
            return STATUS_ERROR
        if not _action.condition:
            print('[Action %02d] %s condition not meet, skipped...' % (_count, _action_str))
            continue
        print('[Action %02d] %s executing...' % (_count, _action_str))
        _action.status = STATUS_ONGOING
        _action.pre_work(_count, _action_str)
        _action.action(_count, _action_str)
        _action.post_work(_count, _action_str)
        if _action.status == STATUS_SUCCESS:
            print('[Action %02d] %s success' % (_count, _action_str))
        elif _action.status == STATUS_PRE_WORK_ERROR:
            error_handle(0, '[Action %02d] %s pre-work failed' % (_count, _action_str))
            return STATUS_ERROR
        else:
            error_handle(0, '[Action %02d] %s failed' % (_count, _action_str))
            return STATUS_ERROR
    return STATUS_SUCCESS

if __name__ == '__main__':
    g.build_id = sys.argv[1]  
    g.working_path_abs = os.getcwd()   
    g.insyde = True
    g.output_path = sys.argv[2]
    
    print('Working folder       :', g.working_path_abs)
    print('Working output path  :', g.output_path)
 
    action_list = [
                    'prepare_time_stamp','get_time_stamp','send_email_to_sign',
                    'wait_for_sign_back','tool_7z_uncompress1','tool_7z_uncompress2'
                  ]
 
    global_run(action_list)

    
