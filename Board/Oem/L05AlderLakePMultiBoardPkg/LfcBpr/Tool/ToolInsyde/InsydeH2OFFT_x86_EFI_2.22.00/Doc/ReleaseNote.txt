H2OFFT (Flash Firmware Tool) for EFI

Version 2.22
------------
1. Correct the UI when doing device firmware update via 23h/24h.
2. Support special update flow of -pbi:TYPE command. [For specific OEM/ODM]
    Support following flow for -pbi:TYPE command update on BIOS Guard enabled platform.
    a. Copy the input file to ESP (or USB) EFI\Insyde\BIOSGUARDLOGO.bin.
    b. Trigger update feature via writing variable.
    c. After restart system BIOS will update the binary directly.

Version 2.21
------------
1. Fix the issue that data in BIOS image BVDT (with multi model name and multi version) will be changed after write to ROM.
    Ex: binary BVDT -> Model_1,Model_2,Model_3[0x00]
        After write to ROM -> Model_1[0x00]Model_2[0x00]Model_3[0x00]
2. Add platform.ini [UI] IndentSpace to control the model name and BIOS version message location.
3. Add platform.ini [UI] IndentSpace2 to control the update XXX message location.
4. Support UTF16 format platform.ini file.
    NOTE: UTF16 format platform.ini only number settings and single-byte character set message strings are valid in shell/secure flash.
          Any character using double-byte character set will be change to space character.
5. Support to update ME sub region (ISH/PDT/IOM/MGPHY/TBT) only capsule.
6. Add -retimer:FILE and -winux:FILE command for copy retimer capsule to ESP and trigger Windows capsule update.
    Tool will copy the retimer file to ESP EFI\UpdateCapsule\CapsuleUpdateFile1000.bin,
     and copy the winux file to ESP EFI\UpdateCapsule\CapsuleUpdateFile0001.bin in default,
    If the target filename is not expected, you can use -norenmae to let tool only copy and keep original filename.
    Usage:
        H2OFFT-Sx64.efi -retimer:Retimer.bin -winux:Winux.bin
    or    
        H2OFFT-Sx64.efi -retimer:CapsuleUpdateFile1000.bin -winux:CapsuleUpdateFile0001.bin -norename

Version 2.20
------------
1. Support new parameter-buffer based SMI function calling (IB02961344).


Version 2.12
------------
1. Support specific OEM vendor ID for non share EC firmware update with version and size check. [For specific OEM/ODM]
2. Support BIOS customize update sequence feature. And the max support update block count is 24.

Version 2.11
------------
1. Support to update ME only capsule.
2. Support EC_Compare, EC_Verify, EC_VerifyErrorRetry in platform.ini [UpdateEC] section.
3. Add [AC_Adapter] Flag=2~4, when Flag >= 2 the BatteryCheck setting will not be referenced.
    Now the power check behavior as below:
    Flag=0: Don't check AC power and battery life percentage.
            Skip power check and continue flash.
    Flag=1: Check AC power not check battery life percentage.
            Only AC power meets the requirement can continue flash.
    Flag=2: Check battery life percentage not check AC power.
            Only DC power meets the requirement can continue flash.
    Flag=3: Check AC power and battery life percentage.
            Both AC and DC power meet the requirement can continue flash.
    Flag=4: Check AC power or battery life percentage.
            One of AC or DC power meets the requirement can continue flash.
4. Update iFdPacker to v2.5.5
    4-1. [Windows] Add OEM/ODM files in packing list.

Version 2.10
------------
1. Add [SecureUpdate] viaESP=3 setting as default and update behavior.
    When viaESP=0, it will pass capsule via memory only. (BIOS need to have enough reserved memory to store whole capsule file.)
    When viaESP=1, it will pass capsule via ESP only. (BIOS need to support via ESP method.)
    Flash process will terminate when BIOS not support via ESP method or copy file to ESP failed.
    When viaESP=2, it will copy capsule to ESP then pass capsule to BIOS via memory.
    It success when both steps are success, and failure when any one failed.
    When viaESP=3, It will try to update capsule via ESP first.
    It success when via ESP method success and will not do via memory method.
    When via ESP method failed it will use pass capsule to BIOS via memory.
2. Support to update the capsule file which contains native ME, BIOS region and external EC.

Version 2.09
------------
1. Fix EC only capsule will fail on boot guard verification issue.

Version 2.08
------------
1. Support specific OEM vendor ID for EC firmware update. [For specific OEM/ODM]
2. Update iFdPacker to v2.5.4
    2-1. Fix Windows command line sometimes can not pack correctly issue.

Version 2.07
------------
1. Support kernel code 5.4 (enable PcdH2OIhisiAuthEnabled and PcdH2OIhisiAuthSupported).
2. Fix ini override not work when multi-FD feature enabled issue.

Version 2.06
------------
1. Support [Bios_Version_Check] Flag=3 and 4 to meet different BIOS version check scenario.
    When need to do BIOS version check by BIOS only, please set Flag=3.
    When need to do BIOS version check by BIOS and application, please set Flag=4. (For the case that BIOS only want to ban specific version.)
2. Remove [Bios_Version_Check] CheckByBios setting, because it is a subset of Flag=3 or 4.
    The behavior of original setting [Bios_Version_Check] Flag=1 CheckByBios=1 in shell flash is the same as new setting [Bios_Version_Check] Flag=4.
    (For backward compatible, this setting can work when Flag=0, 1, 2. When Flag=3 or 4, this setting won't be referenced.)

Version 2.05
------------
1. Support boot guard version verification.

Version 2.04
------------
1. Support specific OEM command override. [For specific OEM/ODM]
2. Update iFdPacker to v2.5.3
    2-1. Fix 64bit Windows single package cannot run correctly on Win10 RS6 issue.
         (32bit package still have this issue in this version.)

Version 2.03
------------
1. Correct [Flash Complete] pause behavior.
    When set [Flash Complete] pause=1, secure flash will show "Press any key to continue...".
    Old (incorrect) behavior: When set [Flash Complete] pause=1, flash secure binary it will pause after passing capsule to BIOS.
    New (correct) behavior: When set [Flash Complete] pause=1, flash secure binary it will do action (reboot) after passing capsule to BIOS.
                            Only doing legacy (normal) flash in EFI shell it will pause after flash complete.

Version 2.02
------------
1. Support [UI] ExtEcUpdateErrorMessage### settings in platform.ini.
    The ### is number in hex, valid range 1~FFFFFFFF.
    It will be shown when EC version check error.
2. Fix shell flash hang when doing ini override issue.

Version 2.01
------------
1. Support \r\n on all message string in platform.ini.
2. Add [Platform_Check] NotTargetPlatformErrorMessage, [Bios_Version_Check] SameVersionErrorMessage and OlderVersionErrorMessage for error message customization.
3. Show warning message when user input a no function command for flash secure BIOS.
    Ex: Using -bios -all for flashing secure capsule will get this warning message.
    To make the flash purpose settings workable, please set it in platform.ini.

Version 2.00
------------
1. Support passing EC data to BIOS for EC version check. [For specific OEM/ODM]
   And add [UI] ExtEcUpdateErrorMessage for EC binary not allow to flash with -extec: command.
2. Support query external EC version with -ecver:EC_FILE command. [For specific OEM/ODM]
    This EC_FILE can be EC binary only file or a secure capsule with EC binary in it.
3. Support Intel Runtime BIOS Resilience (Intel Copper Point).
4. Fix singlepackage cannot run in a sub folder issue on some version of shell (such as v2.2).
5. Update iFdPacker to v2.5.2
   5-1. Support not show extract progress dialog in single package when run with -s command.
6. Show error message "We were unable to upgrade BIOS. Please check your disk." when copy/compare capsule to ESP failed 3 times. (for secure flash via ESP)
7. Add [SecureUpdate] ViaESPCopyFileWaitingTime to wait input seconds after secure capsule file copy.



Version 1.4b
------------
1. Support update EC only capsule to Intel external region.
2. Normal flash supports to update EC to Intel external region with extec:EC_FILE command.
3. Reference platform.ini [FlashComplete] Action setting when update native ME only capsule.
4. Update iFdPacker to v2.5.0
   4-1. Support to pack default argument for windows package.
        Check "Pack Argument" in UI or use -winarg in command line for this feature.

Version 1.4a
------------
1. Add [SecureUpdate] DeviceOrder key in platform.ini to support customizing FAT device detection sequence of secure flash via ESP feature.

Version 1.3z
------------
1. Add [PassToBios] section for passing ini settings to BIOS.
2. Support device firmware update.

Version 1.3y
------------
1. Support 32MB ROM.
2. Support to pass command to secure flash.
3. Update iFdPacker to v2.4.8
    3-1. Fix shell/DOS single package may incorrect and cause flash fail issue.
         (The file size may incorrect in header when the packed file was replaced with same filename but different size file.)

Version 1.3x
------------
1. Support secure flash via ESP with ATAPI device.

Version 1.3w
------------
1. Fix check version by BIOS feature didn't work issue.
2. Update version check behavior to meet OEM spec. [For specific OEM/ODM]

Version 1.3v
------------
1. Support -pbi:TYPE command to flash protected region. [For specific OEM/ODM]
2. Support admin password check (-pwd:PASSWORD) in flash protected region feature (with -pbi:TYPE). [For specific OEM/ODM]

Version 1.3u
------------
1. Fix secure flash via ESP method does not reference BIOS return action code issue.

Version 1.3t
------------
1. Enumerate all FAT partition and get first suitable partition (has EFI folder) for secure flash via ESP method.

Version 1.3s
------------
1. Try to read ROM again when it read failed because ME is locked.
2. Fix version check not work issue.
3. Fix [Log_file] Flag=1 cannot save out log file issue.
4. Fix when flashing ROM it doesn't read block for compare before writing issue. 

Version 1.3r
------------
1. Support doing secure flash via S3 method.
2. Support secure flash via ESP with NVMe device.
3. Update iFdPacker to v2.4.7
    3-1. Fix crash problem of program in console mode after packing.
    3-2. Fix memory leak problem when pack platform.ini in EFI package.

Version 1.3q
------------
1. Fix the passing platform.ini to BIOS feature not terminate with [55AA] issue.
2. Support -p=<password> command. [For specific OEM/ODM]

Version 1.3p
------------
1. Support update Intel native ME with -nativeme and -oemid command. (BIOS version required: 05.05.07.0019)
    Usage: H2OFFT-Sx64.efi -nativeme:ME.bin -oemid:12345678-1234-1234-1234-123456789ABC
2. Passing platform.ini [Region] section to BIOS via IHISI 1Dh.
3. Support multi-SKU package. [For specific OEM/ODM]
4. Update iFdPacker to v2.4.6
    2-1. Fix single package can not pass arguments to flash tool correctly. (For Windows flash only)

Version 1.3o
------------
1. Fix EC cannot update issue.

Version 1.3n
----------------
1. Normal flash support UseBvdtRomMap feature.
2. When update native me only, skip to check model name and version.
3. Fix update via ESP will fail issue.

Version 1.3m
----------------
1. Remove -nativeme and -oemid command.
    For updating Intel native ME only, please using Windows capsule update or Insyde secure flash method.
2. Support BVDT multiple product name and multiple bios version.
3. Upadte iFdPacker to v2.4.2
    3-1. Support packing capsule which without BIOS image (Such as a capsule with Intel native ME only).

Version 1.3l
----------------
1. Fix the isssue that a non-PFAT BIOS can be flashed on a PFAT enabled platform.
2. When input image contains BIOS region only, it don't need to set [Region] bios=1 or use command -bios. AP can auto detect and recognize input image.
3. Support update Intel native ME with -nativeme and -oemid command. (BIOS version required: 05.05.07.0019)
    Usage: H2OFFT-Sx64.efi -nativeme:ME.bin -oemid:12345678-1234-1234-1234-123456789ABC
4. Support customize EC update block size.
5. Add Private/Protection Region Movement feature.
6. Support update PFAT image with secure capsule method.
    It's a secure capsule contains PFAT header and cer data, and will be updated using secure flash process.
    It will doing PFAT update in secure flash (after reboot) not in OS.
    NOTE: The new PFAT update method need BIOS to enable secure flash and PFAT feature at the same time when building.

Version 1.3k
----------------
1. Fix -edt command string type (Unicode string) size incorrect issue.
2. Fix AP will return success to BIOS when flash failed.
3. Update -edt command file type behavior.
    When file size is larger than 256KB, AP will call IHISI 42h multiple times to pass the data to BIOS and each time transfer 64KB data.
4. Fix the issue that when setting [UpdateEC] Flag=1 in platform.ini to do secure flash with EC, AP will flash EC in OS and flash it again in UEFI.

Version 1.3j
----------------
1. Fix the logic error of "ME is locked".
2. Correct write ROM verify behavior. (original code didn't do retry.)
3. Support -priv command to query private region map from IHISI 12h.
4. Fix AP will not reboot or shutdown when using -e: with -r or -s command.

Version 1.3i
----------------
1. Update secure flash via ESP behavior for backward compatible.
2. Update flash sequence let the BVDT block update at the end of flash process.

Version 1.3h
----------------
1. Fix update fail issue when input a BIOS region only FD with EC without command -ecbp.
2. Fix [Others] DisableSecureCapsuleFlash setting not work issue.
3. Supports command -pi.
4. Supports command -eob.
5. Fix it didn't reboot after PFAT update issue.
6. Add PlatformIniEditor tool in tools folder.
    It's an easy to use platform.ini editor with UI.
7. Fix the memory for IHISI may allocated higher than 4G issue. (ITS#33259)
8. Fix it will show size not match error message when flashing a capsule with EC.

Version 1.3g
----------------
1. Fix command -edt fail in 32bit platform.
2. Fix Ihisi 10h error in check support status.

Version 1.3f
----------------
1. Support type MEMORY in [Multi_FD].
2. Update behavior to NOT call Ihisi 48h if enable viaESP of [SecureUpdate].

Version 1.3e
------------
1. Support EC flash flag of Ihisi 19h.
2. Disable command /forcetype. [For specific OEM/ODM]

Version 1.3d
------------
1. Add eMMC for visESP feature. (specific BIOS version required)
2. Support Action_20h in [UpdateEC] for passing action code via Ihisi 20h. [For specific OEM/ODM]
3. Fix NOT protect region in PFAT update.
4. Fix update error if Ihisi 41h implemented but not supports PFAT.
5. Fix update error of alias to type#xx (BB_PEI,DXE,EC) in [ForceFlash]. (#31396)

Version 1.3c
------------
1. Add warning message for Ihisi 15h fail.
2. Add warning message for DC not connected when checking both AC and DC. 

Version 1.3b
------------
1. Fix CheckByBios of [Bios_Version_Check] not works.
2. Fix BIOSVFEnable of [BIOSVersionFormat] not works.
3. Fix update error if alp of [ForceFlash] enabled.
4. Fix DisableVerify of [Others] not works.

Version 1.3a
------------
1. Support command -OemCus for specific string of Ihisi 19h. [For specific OEM/ODM]
2. Fix command -acb NOT check power status.

Version 1.2z
------------
1. Upadte iFdPacker to v2.4.0
    1-a. fix issue of exception occurred in packing EFI flash.
    1-b. disable UAC.

Version 1.2y
------------
1. Add new section [AutoWakeup] with key that can enable auto power on via RTC.
2. Support EC size un-defined in IHISI 21h. (IHISI version required: 2.0.4)
3. Support command "-extrFD" to extract FD in package mode. (IFDPacker version required: 2.3.9)
    Usage: -extrFD [path]
4. Fix issue of command "-n", "-r", "-s" not works if update EC only. 
5. Fix issue of AP hangs if runs couple times without reboot in some platform. (#0030516)
6. Update command -g to check file name valid before IHISI 14h.
7. Upadte iFdPacker to v2.3.9
    7-a. supports command "-extrFD".
    7-b. add option x64/ia32 for EFI. 
8. Update the IHISI version for funcation 10h to 204.

                    Platforms support 0x1F
                   ------------+--------------------
                     Broadwell  05.03.51.0014
                      SharkBay  03.73.06.1067
                   Bay Trail-T  05.03.47.0033
                   Bay Trail-M  05.03.47.0021
                        Kaveri  03.74.03.1015
                       Mullins  03.74.03.1009
                        Kabini  03.74.03.2047
                      Richland  03.74.03.1052

Version 1.2x
------------
1. Add IHISI 1Fh to notice BIOS can enter/leave EC idle mode .
2. Add new key "Elapse" under [UI] which can enable to show Elapse time on screen.
3. Fix cannot flash issue (with update BIOS failed message) on 32bit platform. (#29844, #29840)

Version 1.2w
------------
1. Support command -mfg.
2. Fix issue of update error if alp enabled.
3. Apply new utility naming.
4. Upadte iFdPacker to v2.3.5
    4-1. Update UI
    4-2. Fixs issue to avoid to add duplicate file in H2OFFT folder.
    4-3. Support new utility nameing.
    4-4. Support drag to select file or folder.

Version 1.2v
------------
1. Support viaESP in [SecureUpdate]. [specific BIOS version required]
2. Fix to stop update if ATP Dtimer check (Ihisi 1Ch) fail.
3. Fix issue of command "-n", "-r" not works. (#28762)

Version 1.2u
------------
1. Support EDK2 shell.
2. Fix issue of if ME field omitted in [Multi_FD] enabled.
3. Fix issue of command -ab will check AC plugin.
4. Fix message print cut off when secure boot support but 1Eh not support.
5. Fix to FD filename case insensitive if [Multi_FD] enabled.
6. Fix issue of make log file error when the platform with NOT only one disk.
7. Fix issue of update error if NOT only one NV Storage from Ihisi 1Eh.
8. Support multi region PEI.
9. Upadte iFdPacker to v2.3.4
    9-1. Fix issue of incorrent warning message in shell mode.

Version 1.2t
------------
1. Support -nopause -forceit -forcetype commands. [For specific OEM/ODM]
2. Support [UpdateOEMME].
3. Upadte iFdPacker to v2.2.3
    3-1. Support to pack resource file in shell mode.
4. Support command -edt.
5. Fix issue that cannot update when inputting only BIOS image without ME or others.

Version 1.2s
------------
1. Fix IHISI 10h return permission is denied still can flash issue.
2. Support pause in [FalseComplete].
3. Support EC_DockWarning in [UpdateEC].

Version 1.2r
------------
1. Display EC update process.
2. Fix flash utility won't reboot after EC updating.
3. Fix DC boundary display incorrect issue.
4. Update EC flash behavior.
    When flashing EC, BIOS must provide EC part information by IHISI.
    Otherwize it will get a "Get EC part infromation fail." error.

Version 1.2q
------------
1. Support IHISI 16h after EC updating.
2. Update IHISI 10h behavior to call once.
3. Update IHISI 15h behavior when ME locked.

Version 1.2p
------------
1. Support PFAT update with region protection.

Version 1.2o
------------
1. When flashing an unsigned PFAT image, display meaningful error message instead of "file size not match".
2. Fix error of LauncherAcWarning of [AC_Adapter] for secure update.

Version 1.2n
------------
1. Support alp of [ForceFlash] in command filter feature for security flash.

Version 1.2m
------------
1. Update OEM message when flashing secure image on normal platform and normal image on secure platform. [For specific OEM/ODM]
2. Update [BIOSVersionFormat] to support new type D (Don't care field). Detail 
   Usage please reference the descriptions in platform.ini
3. Support LauncherAcWarning of [AC_Adapter] for AC warning message.

Version 1.2l
------------
1. Passing flash status to BIOS when flash complete by IHISI 16h.
2. Notify BIOS the flash result by IHISI 1Ah.

Version 1.2k
------------
1. Fix -acb no effect issue.

Version 1.2j
------------
1. Support -unvs, -ufc, -skipsbp in command filter feature.
2. Add warning message in save BIOS command (-g) when ME is locked.
3. Upadte iFdPacker to v2.2.8
    3-1. Support packing multi FD for SHELL package.
    3-2. Remove argument filter check box.

Version 1.2i
------------
1. Support PFAT image flash on platform PFAT feature disable. (BIOS version required: SharkBay 03.72.37.0018)
2. Support PFAT checksum verify.
3. Support [Bios_Version_Check] in platform.ini.
4. Support [Platform_Check] in platform.ini.
5. Support [BIOSVersionFormat] in platform.ini.
6. Support command filter feature.
7. Support [PlatformVersion] in platform.ini.
8. Support [FlashSecureBIOSOverride] in platform.ini.
9. Support [Others] in platform.ini.
10. Support [Multi FD] in platform.ini.

Version 1.2h
------------
1. Add "-acb" command.
2. Support ini override for secure capsule update.
3. Upadte iFdPacker to v2.2.7.
    3-1. Support packing multi FD for DOS package.
    3-2. Update the behavior of pack platform.ini for secure image for DOS and Shell package.
         (Don't disable platform.ini pack option when secure image detected for DOS and Shell package.)
4. Support PFAT image update. (BIOS version required: SharkBay 03.72.37.0018)

Version 1.2g
------------
1. Fix model name compare incorrect issue.

Version 1.2f
------------
1. Support OEM secure flash behavior. [For specific OEM/ODM]
2. Add -ec and -ecb command and [UpdateEC] Progress_Bar in platform.ini to support update whole EC image in single IHISI call.
3. Upadte iFdPacker to v2.2.6.
    3-1. Fix message error of Windows single package run in DOS mode.
    3-2. Fix pack error when there is space character in fd path.
    3-3. Correct help message error.

Version 1.2e
------------
1. Add DisableSecureCapsuleFlash flag in [Others] section in platform.ini 
     to enable/disable flash secure BIOS on normal platform.
2. Update AT-p behavior. It does NOT check AT-p signature in plaform supported when flash EC only.
3. Add version check rule for OEM. (it must base on BIOS report.)
4. Add processing input argument for single package.
5. Support OEM message when flashing secure image on normal platform and normal image on secure platform.
6. Upadte iFdPacker to v2.2.5.
    6-1. Add platform.ini pcak option in UI for Windows package.
    6-2. Modify ini pack flag for command line.
    6-3. Disable platform.ini pack option when secure image detected for DOS and Shell package.
    6-4. Fix the parameter checking behavior for command line mode.
7. It can base on BIOS settings from IHISI 11h to do version, model name and same version check.

Version 1.2d
------------
1. Support -option command.
2. platform.ini support case insensitive.
3. Ignore shell flash command, when image is secure capsule.
4. Add shell single package support.
5. Add iFdPacker tool to generate single package file.
6. Support to flash a secure capsule on a normal platform by normal flash method.

Version 1.2c
------------
1. Fix the issue that BIOS return do nothing action in secure flash update but AP do reboot.
2. Support common flash interface for ODM.

Version 1.2b
------------
1. Fix the issue that AP will show not support when flash EC or EC+BIOS with /ecp or /ecbp command.
2. Fix update BIOS address not correct issue.

Version 1.2a
------------
1. Support specified size of flash rom. Such as 5M(1M+4M), 6M(2M+4M) flash rom.
2. Support secure flash.
3. Add platform.ini for security flash. (The platform.ini does not supported in shell flash now.)
4. Add do nothing action for 48h. And add some command for secure flash action override. /r for reboot, /s for shutdown, /n for do nothing.
5. Add EFI driver version check for secure BIOS flash.

Version 1.1o
------------
1.Fix only region with error file name cannot stop to flash. 
2.Add "vrt:" and "rt:" command.
3.Add "ecp" and "ecbp" feature.
4.Get SMI command port via ACPI table.
5.Fix to flash bios with "mc" will show error.
6.Support skip block flash feature.
7.Fix the region may not be protected when start address + size equal to 0x100000000 which overflow of DWORD size.

Version 1.1n
------------
1.Fix more than 4G memory cannot flash.
2.Support 16M platform.
3.Fix "/e" feature.

Version 1.1m
------------
1.Fixed can not run shell flash on other platform which SMI common port is not 0xB2.
2.Fixed can not get rom file when current volume is not fs0.
3.Added tool and BIOS hand shaking feature. 
    Tool send BIOS information that include tool and OS version by functoin 10h. 
    Get the customer vendor ID in function 10h. 
4.Added function 1Ch to check AT-p support by IHISI spec version 1.8.5.
5.Modified the function 10h by IHISI spec version 1.8.5.
6.Fixed when flash only BIOS region with only BIOS binary file will flash wrong address. 

Version 1.1l
------------
1.Modify Me flash feature.
  Support separate ME binary, and two type of Me:
  1.FULL SKU.
  2.Ignition SKU.
  
Version 1.1k
------------
1.Sort the help list.
2.Fixed the tool can't get the file name in a folder.

Version 1.1j
------------
1.Modify x64 to support 8M.

Version 1.1i
------------
1.Modify to support 4M.
2.Add function 17h,12h,19h,1Ah 11h

