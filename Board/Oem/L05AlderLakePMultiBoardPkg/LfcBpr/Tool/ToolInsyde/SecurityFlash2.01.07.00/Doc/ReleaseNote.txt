Insyde Security Flash Release Notes

Version 2.01.07
---------------
1. Support passing "WITHOUTBIOS" signature string via IHISI 19h when input capsule not contains BIOS binary.
2. Allow to flash on low DC for specific OEM. [For specific OEM/ODM]
3. Add return error code.
4. Support to pass FFT error code and error message to BIOS via protocol. [For specific OEM/ODM]
5. Add read compare before write for BIOS guard update method.

Version 2.01.06
---------------
1. Support passing input image BIOS version to onboard BIOS.
2. Support customize update sequence data start address not align to ROM block address case.
    e.g. ROM block size is 64KB, but customize update area start address is FFBFE000.
    Old: Data which want to write to FFBFE000 will write to FFBF0000 and write 0x10000 (64KB) size.
    New: Data which want to write to FFBFE000 will write to FFBFE000 and write 0x2000 (8KB) size.
3. Update iEFIFlashSigner.exe to 1.1.6.3
    3-1. Add to specify GUID (-guid) and capsule flags (-capflag) for capsule header command.
         This two command also valid in capsuleheader method.
         When -guid not used, tool will using the GUID in BIOS ESRT instead.
           Usage:
             Signing Secure Image and also add header
               a. Using the GUID in BIOS ESRT
               iEFIFlashSigner.exe sign -n "QA Certificate." -bios BIOS.fd -ini platform.ini -capsuleheader -capflag PersistAcrossReset -capflag InitiateReset
               b. Input GUID
               iEFIFlashSigner.exe sign -n "QA Certificate." -bios BIOS.fd -ini platform.ini -capsuleheader -guid 12345678-1234-1234-1234-123456789ABC -capflag PersistAcrossReset -capflag InitiateReset

             Add capsule header to binary image file without signing
               a. Using the GUID in BIOS ESRT
               iEFIFlashSigner.exe capsuleheader -bios isflash.bin -capflag PersistAcrossReset -capflag InitiateReset
               b. Input GUID
               iEFIFlashSigner.exe capsuleheader -bios isflash.bin -guid 12345678-1234-1234-1234-123456789ABC -capflag PersistAcrossReset -capflag InitiateReset
    3-2. Fix signing BIOS guard BIOS with device firmware will not pack device firmware in capsule issue.
    3-3. Fix signbin method signing microcode with BIOS Guard header cannot sign correctly issue. (issue occurs on signer v1.1.6.2)

Version 2.01.05
---------------
1. Auto detecting EC need to write to EC part or Intel EC region (via 17h and 21h).
2. Correct the UI when doing device firmware update via 23h/24h.

Version 2.01.04
---------------
1. Fix update Intel external EC may hang issue when EC binary smaller than EC region size.
2. Add platform.ini [UI] IndentSpace2 to control the update XXX message location.

Version 2.01.03
---------------
1. Support BIOS guard block header using BIOS region base offset in write address field.
    When BIOS guard block header offset 0x3C is 0xFFFFFFFF means the write address field (0x4C) is using BIOS region base offset,
    otherwize this field is using ROM base offset.
2. Update iEFIFlashSigner.exe to 1.1.6.2
    2-1. Add signpfat method to sign BIOS Guard binary with non-capsule format.
    2-2. signefi method support to sign all the efi files.
         Previous version only can sign shell flash.
3. Support BIOS region not at the bottom of ROM and BIOS guard ChasmFalls enable case.

Version 2.01.02
------------
1. Add platform.ini [UI] IndentSpace to control the model name and BIOS version message location.
2. Support UTF16 format platform.ini file.
    NOTE: UTF16 format platform.ini only number settings and single-byte character set message strings are valid in shell/secure flash.
          Any character using double-byte character set will be change to space character.

Version 2.01.01
------------
1. Fix the issue that data in BIOS image BVDT (with multi model name and multi version) will be changed after write to ROM.
    Ex: binary BVDT -> Model_1,Model_2,Model_3[0x00]
        After write to ROM -> Model_1[0x00]Model_2[0x00]Model_3[0x00]
2. Update iEFIFlashSigner.exe to 1.1.6.1
    2-1. Fix the signed binary using full path and not put in signer main program folder will sign failed issue.

Version 2.01.00
------------
1. Update the layout of update message and progress.
2. Support new parameter-buffer based SMI function calling (IB02961344).


Version 2.00.22
------------
1. Normalize all firmware update progress as progress bar.

Version 2.00.21
------------
1. Add [FlashComplete] SecureFlashDelayBeforeExit to customize the delay time before secure flash exit.
    When the flash complete action is donothing, secure flash will do delay before terminated and then return to BIOS.
    When the action is reboot or shutdown, secure flash will do reboot or shutdown in IHISI 16h.
2. Update iEFIFlashSigner.exe to 1.1.6.0
    2-1. Support to sign BIOS with BIOS Guard header and EC with BIOS Guard header in the same capsule.
    2-2. Support AMD one bios compress and decompress.
          Compress:
            iEFIFlashSigner onebios -compress isflash.bin [-output compress.fd]
          Decompress:
            iEFIFlashSigner onebios -decompress compress.fd [-output isflash.bin]
          Without the -output option, the output filename will be input filename add "_cmp" or "_decmp" postfix.
          In the example above the default output filename will be isflash_cmp.bin for compress and compress_decmp.fd for decompress.
    2-3. Add signbiosregion method and -bin <FILE> -guid <GUID> command to sign BIOS region FV (such as BTG Acm).
3. Support -secondlogo command for specific OEM. [For specific OEM/ODM]

Version 2.00.20
------------
1. Support specific OEM vendor ID for non share EC firmware update with version and size check. [For specific OEM/ODM]
2. Support BIOS customize update sequence feature on BIOS Guard enable case.

Version 2.00.19
------------
1. Support to update the capsule file which only contains non-share EC.
2. Extend BIOS customize update block count to 24 for BIOS customize update sequence feature.
3. Update iEFIFlashSigner.exe to 1.1.5.9
   3-1. Add command -microcode in signbin method.
   3-2. Support to sign BIOS Guard binary with specifying block offset. (PFAT header version 3)

Version 2.00.18
------------
1. Support BIOS customize update sequence feature.
    BIOS report the update area type, address, size and input image offset table from IHISI, tool will update base on this table.
2. Support partial protect of update block.
    The update block size is reported from IHISI, when the size is not minimum update size (4KB) the protect area may cover on a part of update block.
    Old behavior: The update block which is partial protected will not be updated.
    New behavior: The update block which is partial protected, the unprotected part will be updated with 4KB by 4KB.

Version 2.00.17
------------
1. Support EC_Compare, EC_Verify, EC_VerifyErrorRetry in platform.ini [UpdateEC] section.
2. Add [AC_Adapter] Flag=2~4, when Flag >= 2 the BatteryCheck setting will not be referenced.
    Now the power check behavior as below:
    Flag=0: Don't check AC power and battery life percentage.
            Skip power check and continue flash.
    Flag=1: Check AC power not check battery life percentage.
            Only AC power meets the requirement can continue flash.
    Flag=2: Check battery life percentage not check AC power.
            Only DC power meets the requirement can continue flash.
    Flag=3: Check AC power and battery life percentage.
            Both AC and DC power meet the requirement can continue flash.
    Flag=4: Check AC power or battery life percentage.
            One of AC or DC power meets the requirement can continue flash.
3. Add [UI] PauseWhenUpdateBinaryWithoutBiosFail setting let user can disable the pause when using Windows capsule update.
   The default value set as 0 (disable), the behavior of updating binary without BIOS will be changed as below.
    Old default behavior: When binary update failed, it will show error and pause, after user press a key it will return to BIOS.
    New default behavior: When binary update failed, it will show error and not pause and return to BIOS directly.

Version 2.00.16
------------
1. Support to update full ROM size BIOS Guard (PFAT) format capsule.
2. Support to update the capsule file which contains native ME, BIOS region and external EC.

Version 2.00.15
------------
1. When input image contains BIOS region only, it don't need to set [Region] bios=1 or use command -bios. AP can auto detect and recognize input image.
2. Update iEFIFlashSigner.exe to 1.1.5.8
   2-1. Fix device firmware cannot pack in capsule issue in 1.1.5.7.

Version 2.00.14
------------
1. Support to update BIOS Guard (PFAT) format EC only capsule.
2. Update iEFIFlashSigner.exe to 1.1.5.7
   2-1. Remove output file when the sign process failed.
   2-2. Support to sign EC + BIOS Guard (PFAT) header image. (Using the same command as sign EC only image.)
3. Support to hide model name display via [UI] DisplayID and IHISI 11h.

Version 2.00.13
------------
1. Support specific OEM vendor ID for EC firmware update. [For specific OEM/ODM]
2. Update iEFIFlashSigner.exe to 1.1.5.6
   2-1. Fix merge method will show error in merge stage 2 issue.
        Symptom: Using iEFIFlashSigner.exe merge -in isflash_drv.efi will get "Error: Load iFlashDriver.efi binary failed.".

Version 2.00.12
------------
1. Support kernel code 5.4 (enable PcdH2OIhisiAuthEnabled and PcdH2OIhisiAuthSupported).

Version 2.00.11
------------
1. Support to update new BIOS Guard (PFAT) format which different update block size in single file.
2. Update iEFIFlashSigner.exe to 1.1.5.5
    2-1. Support to sign new BIOS Guard (PFAT) format which different update block size in single file.
3. Fix when BIOS Guard (PFAT) update failed, IHISI 16h flash complete status not AP terminate issue.

Version 2.00.10
------------
1. Support to update Retimer only capsule.
2. Update iEFIFlashSigner.exe to 1.1.5.4
   2-1. Add command -retimer, -ver and -funcnum in sign method, merge method, and signbin method.

Version 2.00.09
------------
1. Support boot guard version verification.
2. Fix CPU exception issue when flash complete do nothing in secure flash.
3. Fix Windows capsule update will display 100% at start then increase from 0% issue.

Version 2.00.08
------------
1. Fix flash utility will return incorrect update result to BIOS when calling UnloadImage function failed.
    Symptom: BIOS update succeess but it popup a blue dialog shows update failed.

Version 2.00.07
------------
1. Support specific OEM command override. [For specific OEM/ODM]

Version 2.00.06.01
------------
1. Update iEFIFlashSigner.exe to 1.1.5.3
   1-1. Fix signing EFI flash file may cause signer program terminate issue.

Version 2.00.06
------------
1. Support -all, -ft:TYPE command override.

Version 2.00.05
------------
1. Support to update IOM, MGPHY and TBT only capsule.
2. Update iEFIFlashSigner.exe to 1.1.5.2
   2-1. Add command -iom, -mgphy and -tbt  in sign method, merge method.
3. Add [UpdateDeviceFirmware] VerifyErrorRetry setting to enable/disable verify and do the retry times on error when updating device firmware.
    VerifyErrorRetry = 0 is default, means disable verify.
    VerifyErrorRetry = 1 means enable verify.
    VerifyErrorRetry = 3 means enable verify and will do the write-read-verify for max 3 times.
4. Remove call 16h after write ROM for OEM. [For specific OEM/ODM]

Version 2.00.04
------------
1. Correct the error message on update binary without BIOS failed case.
    EX: When update failed in EC only case, it will show "Error: Update EC failed!" instead of "Error: Update BIOS failed!".
2. Support [UI] ExtEcUpdateErrorMessage### settings in platform.ini.
    The ### is number in hex, valid range 1~FFFFFFFF.
    It will be shown when EC version check error.
3. Correct [Flash Complete] pause behavior.
    When set [Flash Complete] pause=1, secure flash will show "Press any key to continue...".
    Old (incorrect) behavior: After press a key it still waiting 5 seconds then end program.
    New (correct) behavior: After press a key it will end program.

Version 2.00.03
------------
1. Fix secure flash didn't pass progress to BIOS in BIOS Guard (PFAT) update case.
2. Fix secure flash didn't call IHISI 19h on update EC only case.
3. Support [ParamForBiosReference] section in platform.ini to pass specific parameter string to BIOS via IHISI 19h.
4. Fix IHISI Get Device Firmware Information using wrong register issue.
5. Fix secure flash didn't pass progress to BIOS in external EC only update case.

Version 2.00.02
------------
1. Revise the update behavior of secure capsule with ME.
    Old behavior: When ME update failed, the flash process will be terminated, BIOS will not be updated. (v2.00.00 and v2.00.01)
    New behavior: When ME update failed, it will continue to update BIOS.
2. Fix secure flash will hang when enable debug log issue. This issue occurs on v2.00.00 and v2.00.01.

Version 2.00.01
------------
1. Support [PreFlash] EraseDataType setting to erase specific data type (which reported from IHISI 1Eh or 12h) before flash.

Version 2.00.00
------------
1. Do model name and BIOS version check in secure flash.
2. Support Intel Runtime BIOS Resilience (Intel Copper Point).
3. Update iEFIFlashSigner.exe to 1.1.5.1
   3-1. Support -f [PFX_FILE] and -p [PFX_PASSWORD] command for using key from pfx file instead of the key installed in system key store.
   3-2. Support -sha1 [HASH] command to specify which key will be used when the key store have more than 1 keys have the same subject name.
   3-3. Support -o [FILENAME] command to specify output file name.



Version 1.00.62
------------
1. Add [UI] ShowEcUpdateProgress to hide or show the EC update percentage.

Version 1.00.61
------------
1. Support update EC image to Intel external region.
2. Support to pass capsule update progress to BIOS via protocol. (Kernel code supports in 05.22.49.)
3. Reference platform.ini [FlashComplete] Action setting when update native ME only capsule.
4. Fix the issue that secure flash complete action will reference the Action setting in [FlashSecureBIOSOverride] section when it enabled.

Version 1.00.60
------------
1. Update pass update progress to BIOS via IHISI behavior. When it get any fail in this feature, AP will ignore this IHISI for following process.
2. Update iEFIFlashSigner.exe to 1.1.4.8
   2-1. Command signbin support -ini option.
   2-2. Support to sign ec in signbin method.

Version 1.00.59
------------
1. Support device firmware update.
2. Update iEFIFlashSigner.exe to 1.1.4.7
   2-1. Support nosign method for packing binary only.
   2-2. Add -df## command in sign method and nosign method.

Version 1.00.58
------------
1. Fix tool will hang when the override command too long issue.

Version 1.00.57
------------
1. Support 32MB ROM.
2. Support pause after flash complete when /forceit command used without /nopause to meet OEM spec. [For specific OEM/ODM]

Version 1.00.56
------------
1. Show error message when EC update failed.

Version 1.00.55.01
------------
1. Update iEFIFlashSigner.exe to 1.1.4.6
   1-1 Support to backup/restore unsigned area in signefi method.

Version 1.00.55
------------
1. Fix [Log_file] Flag=1 cannot save out log file issue.
2. Fix when flashing ROM it doesn't read block for compare before writing issue. 

Version 1.00.54
------------
1. Remove [Others] EnableMeLockReadFailErrorRetry key. Always enable this retry feature, but only do once in whole read ROM loop.

Version 1.00.53
------------
1. Add [UI] PassUpdateProgressToBios flag to enable/disable passing flash progress to BIOS.
2. Support to update ish + pdt capsule.
3. Update iEFIFlashSigner.exe to 1.1.4.5
   3-1. Support to sign pdt and ish at the same time in signbin method.
   3-2. Display a warning message when signing with Insyde QA key.

Version 1.00.52
------------
1. Support to update Intel PDT image.
2. Add [Others] EnableMeLockReadFailErrorRetry=1 setting in platform.ini for reading ROM again when it read failed because ME is locked.
3. Update iEFIFlashSigner.exe to 1.1.4.4
   3-1. Add command -pdt in sign and merge method for signing Intel PDT binary.

Version 1.00.51
------------
1. Fix the passing platform.ini to BIOS feature not terminate with [55AA] issue.
2. Support -p=<password> command. [For specific OEM/ODM]

Version 1.00.50
------------
1. Support multi-SKU package. [For specific OEM/ODM]
2. Passing platform.ini [Region] section to BIOS via IHISI 1Dh.

Version 1.00.49
------------
1. Support to update Intel ISH (Integrated Sensor Hub) image.
2. Update iEFIFlashSigner.exe to 1.1.4.3
   2-1. Add command -ish in sign and merge method for signing Intel ISH binary.

Version 1.00.48
------------
1. Fix progress bar flicking issue while flashing BIOS.

Version 1.00.47
------------
1. Fix EC cannot update issue.

Version 1.00.46
------------
1. Support BVDT multiple product name and multiple bios version.

Version 1.00.45
------------
1. Add Private/Protection Region Movement feature.
2. Support update PFAT image with secure capsule method.
    NOTE: The new PFAT update method need BIOS to enable secure flash and PFAT feature at the same time when building.
3. Update iEFIFlashSigner.exe to 1.1.4.2
   3-1. Modify sign PFAT image method.
        Currently the PFAT image is a kind of secure BIOS, it will be signed as secure BIOS method.
        Signing a PFAT image without update engine method is not supported from this version.

Version 1.00.44
------------
1. Use new method for security flash template. (iEFIFlashSigner version required: v1.1.4.1)
2. Support update Intel native ME. (BIOS version required: 05.05.07.0019)
3. Support customize EC update block size.
4. Update iEFIFlashSigner.exe to 1.1.4.1
   4-1. Support -me and -oemid in sign method and merge method.
   4-2. Support -me and -oemid in sign PFAT image method.
   4-3. Support -me and -oemid in signbin method for signing Intel native ME only capsule.

Version 1.00.43
------------
1. UEFI flash for OEM need to call 16h between flash BIOS and EC. [For specific OEM/ODM]
2. Fix AP will return success to BIOS when flash failed.

Version 1.00.42
------------
1. Support BVDT private/protection ROM map override IHISI 12h feature.

Version 1.00.41
------------
1. Update iEFIFlashSigner.exe to 1.1.4.0
   1-1. Support signlogo method.
   1-2. Add -capsuleheader in sign method to generate a EFI capsule header structure in front of signed secure capsule.
2. Update flash sequence let the BVDT block update at the end of flash process.

Version 1.00.40
------------
1. Fix it will not update EC when flashing a capsule with EC.

Version 1.00.39
------------
1. Fix the memory for IHISI may allocated higher than 4G issue. (ITS#33259)

Version 1.00.38
------------
1. Supports feature: skip update region ME if ME locked and [Region] ME=1. 
2. Update iEFIFlashSigner.exe to 1.1.3.0
   2-1.Support merge method.

Version 1.00.37
------------
1. Supports feature: update to NOT call Ihisi 1Bh in secure update.

Version 1.00.36
------------
1. Supports feature: supports EC flash flag of Ihisi 19h.

Version 1.00.35
------------
1. fix bugs        : fix passing incorrect action code via Ihisi 20h in secure update.(For specific OEM/ODM)

Version 1.00.34
------------
1. supports feature: supports Action_20h in [UpdateEC] for passing action code via Ihisi 20h.(For specific OEM/ODM)
2. fix bugs        : fix update error of alias to type#xx (BB_PEI,DXE,EC) in [ForceFlash]. (#31396)

Version 1.00.33
------------
1. fix bugs        : fix update error if alp of [ForceFlash] enabled.
2. fix bugs        : fix DisableVerify of [Others] not works.
3. fix bugs        : add warning message for Ihisi 15h fail.
4. fix bugs        : add warning message for DC not connected when checking both AC and DC. 

Version 1.00.32
------------
1. Fix AC/DC check behavior incorrect issue in secure flash.

Version 1.00.31
------------
1. fix bugs        : fix update error in secure update if [UpdateOEMME] enabled.

Version 1.00.30
------------
1. Update iEFIFlashSigner.exe to 1.1.2.9.
    1-a. disable UAC.

Version 1.00.29
------------
1. fix bugs        : fix the default behavior of updating EC to block by block.

Version 1.00.28
------------
1. supports feature: supports EC size un-defined in IHISI 21h. (IHISI version required: 2.0.4)
2. fix bugs        : update the IHISI version for funcation 10h to 204;

                    Platforms support 0x1F
                   ------------+--------------------
                     Broadwell  05.03.51.0014
                      SharkBay  03.73.06.1067
                   Bay Trail-T  05.03.47.0033
                   Bay Trail-M  05.03.47.0021
                        Kaveri  03.74.03.1015
                       Mullins  03.74.03.1009
                        Kabini  03.74.03.2047
                      Richland  03.74.03.1052

Version 1.00.27
------------
1. Supports feature: Add new section [AutoWakeup] with key that can enable auto power on via RTC.

Version 1.00.26
------------
1. Supports feature: Add IHISI 1Fh to notice BIOS can enter/leave EC idle mode .
2. Supports feature: Add new key "Elapse" under [UI] which can enable to show Elapse time on screen.
3. fix update fail with message "Decompress failled" when enable secure boot. (#0029704)
4. fix cannot flash issue (with update BIOS failed message) on 32bit platform. (#29844, #29840)

Version 1.00.25
------------
1. fix issue of update error if alp enabled.
2. apply new utility naming.

Version 1.00.24
------------
1. fix to stop update if ATP Dtimer check (Ihisi 1Ch) fail.
2. fix issue of update ME region in secure update if [UpdateOEMME] enabled.

Version 1.00.23
------------
1. supports multi NV Storage from Ihisi 1Eh.
2. supports multi region PEI from Ihisi 12h.
3. Update iEFIFlashSigner.exe to 1.1.2.8.
    3-1. Boot Guard command supports to sign PFAT image with header.

Version 1.00.22
------------
1.Use update progressive bar to show update percentage.

Version 1.00.21
------------
1. fix display platform name error if Flag=2 in [Platform_Check].

Version 1.00.20
------------
1. Update EC flash behavior for Ihisi 21h fail. [For specific OEM/ODM]

Version 1.00.19
------------
1. display EC update process.
2. update IHISI 21h behavior.
3. update behavior of flash EC with BIOS. [For specific OEM/ODM]
4. update behavior of AC/DC check. [For specific OEM/ODM]
5. Update iEFIFlashSigner.exe to 1.1.2.7
    5-1. supports input -n2 command. [For specific OEM/ODM].
    5-2. fix signing PFAT error.

Version 1.00.18
------------
1. Supports update OEM ME in OS flash before doing secure flash.

Version 1.00.17
------------
1. Supports IHISI 16h after EC updating.
2. Update IHISI 10h behavior to call once.
3. Update IHISI 15h behavior when ME locked.
4. Remove delay for IHISI 15h.

Version 1.00.16
------------
1. Support alp of [ForceFlash] in command filter feature for security flash.

Version 1.00.15
------------
1. enable AC/DC check in security flash.
2. supports SecurityAcWarning of [AC_Adapter] for AC warning message.

Version 1.00.14
------------
1. Passing flash status to BIOS when flash complete by IHISI 16h.
2. Notify BIOS the flash result by IHISI 1Ah.
3. Support 32bit BIOS.
4. Update iEFIFlashSigner.exe to 1.1.2.5.

Version 1.00.13
------------
1. Support ME lock condition.

Version 1.00.12
------------
1. Support the image with unsigned area run on SAP2 feature applied platform. (BIOS version required: 3.72.45)

Version 1.00.11.01
------------
1. Update iEFIFlashSigner.exe to 1.1.2.3.
    1-1. Support sign secure image for crisis. (BIOS version required: 3.72.36, 5.02.36)
    1-2. Fix signed image can't run on SAP2 feature applied platform.
    1-3. Add file checksum for PFAT image.
    1-4. Support processing unsigned area.
    1-5. Support sign shell flash efi file.

Version 1.00.11.00
------------
1. Support ini override for secure capsule update.
2. Update iEFIFlashSigner.exe to 1.1.2.2.
    2-1. Support sign PFAT image.

Version 1.00.10
------------
1. Fix private map can't be protect issue when platform.ini [ForceFlash] all=1.
2. Flash settings will reference the settings return from IHISI 11h.

Version 1.00.09
------------
1. Support common flash feature in security flash.
2. Support case insensitive in platform.ini.
3. Disable check in secure flash.
4. Support all settings in platform.ini [ForceFlash] section.

Version 1.00.08
------------
1. Update common flash feature flash complete action override.

Version 1.00.07
------------
1. Add force update variable area flag.
2. Update iEFIFlashSigner to v1.1.2.0. Not allow to sign a signed capsule.
3. Update FACTORY_COPY restore interface. (Need to update kernel code.)
4. Update NV storage protect method for secure boot support platform. (BIOS must implement IHISI 1Eh function. Kernel code supports in 03.72.20.)
5. Update QA certificate sample file from RSA 1024 bits to RSA 2048 bits. (Need to update kernel code.)

Version 1.00.06
------------
1. Add FACTORY_COPY restore support.

Version 1.00.05
------------
1. Add update EC support.

Version 1.00.04
------------
1. Variable area will not update by flash utility to meet Windows logo requirement.
2. Support specified size of flash rom. Such as 5M(1M+4M), 6M(2M+4M) flash rom.

Version 1.00.03
------------
1. Support 16M bios.
2. Support no CSM BIOS.
3. Using SHA256.

Version 1.00.02
------------
1. Add secure BIOS identify information.

Version 1.00.01
------------
1. Initial version.