*******************************************************************************************************************
System BIOS Crisis Release Notification


[Procedure]:
1. Shutdown the system and plug in AC.
2. Insert the bootable device.
3. Press Fn + R.
4. Power on the unit.
5. Release Fn + R.
6. The process of Crisis runs automatically.
