@echo OFF

mt.exe -inputresource:"%ProgramFiles(x86)%\Inno Setup 5\Setup.e32" -out:extract.manifest

for /f "delims=" %%i in ('findstr /i /c:"<description>" extract.manifest') do call     :job "%%i"
goto :eof

:job
set line=%1
set line=%line:<=+%
set line=%line:>=+%
set line=%line:*+description+=%
set line=%line:+=&rem.%

set var= ^"%line%^"
<nul set /p=#define PatchString> list.iss
echo %var% >> list.iss

set str=%line:~-5%

set version="?"
if "%str%" == "09/20" ( 
	set version="1"
)

if "%str%" == "09/26" (
        set version="3" )

if "%str%" == "10/25" (
    set version= "2"
 )

<nul set /p=#define Package>> list.iss
echo %version%>>list.iss


for /f "delims=" %%i in ('findstr /i /c:"CIssVersion" Compat.iss') do call     :job1 "%%i"
goto :eof
:job1
set Cpara=%1
set Cpara=%Cpara:#define=%
set Cpara=%Cpara:CIssVersion=%
set Cpara=%Cpara:"=%
set Cpara=%Cpara:'=%
set Cpara=%Cpara: =%

for /f "delims=" %%i in ('findstr /i /c:"LIssVersion" LenovoWizard.iss') do call    :job2 "%%i"
goto :eof
:job2
set Lpara=%1
set Lpara=%Lpara:#define=%
set Lpara=%Lpara:LIssVersion=%
set Lpara=%Lpara:"=%
set Lpara=%Lpara:'=%
set Lpara=%Lpara: =%
set version=^"%Cpara%%Lpara%^"
<nul set /p=#define ScriptVersion >> list.iss
echo %version% >> list.iss

exit


