from Lib.lcfc_lib import *
from Lib.action_base import ActionBase
import shutil


class ActionPack(ActionBase):
    # packed_folder
    pack_create_folder = 1
    pack_copy_tree = 2
    pack_copy_file = 3
    pack_delete_folder = 4
    pack_copy_files =5
    pack_delete_files = 6
    pack_rename_file = 7

    def __init__(self, layout):
        super().__init__()
        self.layout = layout

    def action(self, count, action_str):
        super().action(count, action_str)

        # lnb output
        for _i in self.layout:
            _tmp_action = _i[0]
            src = _i[1]
            dst = _i[2]
            if not _i[3]:
                continue
            if _tmp_action == ActionPack.pack_create_folder:
                os.makedirs(os.path.join(self.output_folder, dst))
            elif _tmp_action == ActionPack.pack_copy_file:
                shutil.copyfile(src, os.path.join(self.output_folder, dst))
            elif _tmp_action == ActionPack.pack_copy_tree:
                shutil.copytree(src, os.path.join(self.output_folder, dst))
            elif  _tmp_action == ActionPack.pack_delete_folder:
                shutil.rmtree (src)
            elif  _tmp_action == ActionPack.pack_copy_files:
                os.system(r'xcopy %s %s /E' % (
                src, os.path.join(self.output_folder, dst)))
            elif  _tmp_action == ActionPack.pack_delete_files:
                if os.path.exists(src):
                    os.remove(src)
            elif _tmp_action == ActionPack.pack_rename_file:
                if os.path.exists(src):
                    os.rename(src, os.path.join(src.rsplit('\\', 1)[0], dst))

        self.action_output['packed_folder'] = os.path.join(self.output_folder, self.layout[0][2])
        return


"""
# tree
from pathlib import Path

tree_str = ''
def generate_tree(pathname, n=0):
    global tree_str
    if pathname.is_file():
        tree_str += '    |' * n + '-' * 4 + pathname.name + '\n'
    elif pathname.is_dir():
        tree_str += '    |' * n + '-' * 4 + \
            str(pathname.relative_to(pathname.parent)) + '\\' + '\n'
        for cp in pathname.iterdir():
            generate_tree(cp, n + 1)

if __name__ == '__main__':
    path = 'D:/xxx/xxx/xxx'
    generate_tree(Path(path), 0)
    print(tree_str)
"""