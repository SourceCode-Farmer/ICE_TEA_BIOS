# coding： UTF-8
# DEVELOP PERSON : Cissie
# DEVELOP TIME   :  14:48
# FILE NAME      : Action_gui.PY
# DEVELOP TOOL   : PyCharm
import tkinter as tk
from tkinter.scrolledtext import ScrolledText
from tkinter import Radiobutton
from tkinter import IntVar
from tkinter import messagebox
from tkinter import filedialog
from tkinter import Listbox



class LfcLabelFrame:
    def  __init__(self):
        pass

class LfcRadiobuttonGroup:
    #container: the LfcLabelFrame which contain the RadiobuttonGroup
    #data_layout :[{'text':'Mocca Sign','value':0,'row':2,'column':1,'associated':{'button_group':object,'value':0, 'state': 'normal'}},
    #              {},..
    #             ]
    #group_label:{'text':'Release Type','row':3,'column':0}
    def  __init__(self,window,container,data_layout,group_label):
        self.window = window
        self.container = container
        self.lfc_radiobutton_group = []
        self.v = IntVar()
        self.v.set(0)

        for index,button_data in enumerate(data_layout):
            lfc_button = LfcRadiobutton(self.window, self.container, button_data, self.v,button_data['associated'])
            self.lfc_radiobutton_group.append(lfc_button)

        self.window.Label(self.container, text=group_label['text']).grid(row=group_label['row'], column=group_label['column'], sticky=self.window.NW)

class LfcRadiobutton:
    #container: the LfcLabelFrame which contain the RadiobuttonGroup
    #radiobutton :{'text':'Mocca Sign','value':0,'row':2,'column':1}
    #associated: {'button_group': object, 'value': 0, 'state': ['normal',]}
    #
    def __init__(self, window, container, radiobutton,variable,associated):
        self.container = container
        self.variable = variable
        self.radiobutton = radiobutton
        self.associated = associated
        if self.associated is not None:
            self.associated_button_group = associated.get('button_group')
            self.associated_value = associated.get('value')
            self.associated_state = associated.get('state')
        self.button = Radiobutton(self.container, text=self.radiobutton['text'], variable=self.variable, value=self.radiobutton['value'],
                    command=self.associated_radiobutton)
        self.button.grid(row=self.radiobutton['row'], column=self.radiobutton['column'], sticky=window.NW)


    def associated_radiobutton(self):

        if self.associated is not None:
            if self.associated_value is not None:
                self.associated_button_group.v.set(self.associated_value)
            if self.associated_state is not None:
                for index,value in enumerate(self.associated_state):
                    self.associated_button_group.lfc_radiobutton_group[index].button['state'] = value
