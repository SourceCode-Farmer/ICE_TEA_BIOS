from Lib.lcfc_lib import *
from Lib.action_base import ActionBase


class ActionTxt(ActionBase):
    txt_create = 1
    txt_read = 2
    txt_modify = 3
    txt_grab = 4
    # file
    txt_insert = 5
    txt_override_line = 6
    txt_modify_line = 7
    txt_mix = 8

    def __init__(self, **kwargs):
        super().__init__()
        self.sub_function = kwargs['sub_function']
        if self.sub_function == ActionTxt.txt_create:
            self.create_file = kwargs['file']
            self.content = kwargs['content']
        if self.sub_function == ActionTxt.txt_read:
            self.read_file = kwargs['file']

        elif self.sub_function == ActionTxt.txt_modify:
            self.modify_file = kwargs['file']
            self.string_map = kwargs['string_map']
            if 'match_count' in  kwargs.keys():
                self.match_count = kwargs['match_count']
        elif self.sub_function == ActionTxt.txt_grab:
            self.grab_file = kwargs['file']
            self.start_str = kwargs['start_str']
            self.end_str = kwargs['end_str']
            self.start_str_include = kwargs['start_str_include']
            self.end_str_include = kwargs['end_str_include']
            self.grab_output_file = kwargs['output_file']

        elif self.sub_function == ActionTxt.txt_insert:
            self.input_data = kwargs.get('input_data')
            self.input_file = kwargs.get('input_file')
            self.dst_file = kwargs['dst_file']
            self.start_str = kwargs.get('start_str')
            self.end_str = kwargs.get('end_str')
            self.insert_count = kwargs['insert_count']
        elif self.sub_function == ActionTxt.txt_modify_line:
            self.modify_line_file =kwargs['file']
            self.string_map =kwargs['string_map']
            self.insert_before = kwargs['insert_before']
            self.modify_count = kwargs['modify_count']
            
        elif self.sub_function == ActionTxt.txt_override_line:
            self.override_line_file = kwargs['file']
            self.override_count = kwargs['override_count']
            self.override_line = kwargs['override_line']
            if 'line_exclude' in  kwargs.keys():
                self.line_exclude =kwargs['line_exclude']
            else:
                self.line_include = kwargs['line_include']
                
        elif self.sub_function == ActionTxt.txt_mix:
            self.layout = kwargs['layout']


    def create(self):
        if r'\\' not in self.create_file:
            self.action_output['file'] = os.path.join(self.output_folder, self.create_file)
            self.create_file = self.action_output['file']
        with open(self.create_file, 'w') as f:
            f.write(self.content)

    def read(self):
        with open(self.read_file, 'r') as f:
            self.read_output = f.read()

    def modify(self):
        with open(self.modify_file, 'r') as f:
            content = f.read()
        for key, value in self.string_map.items():
            if  hasattr(self,'match_count'):
                content = content.replace(key, value, self.match_count)
            else:
                content = content.replace(key, value)
        with open(self.modify_file, 'w') as f:
            f.write(content)

    def grab(self):
        try:
            with open(self.grab_file) as f:
                content = f.read()
        except FileNotFoundError:
            error_handle(1, 'Cannot find ' + self.grab_file)
            self.status = STATUS_ERROR
            return
        if content.find(self.start_str) == -1:
            error_handle(1, 'Cannot find string %s in %s' % (self.start_str, self.grab_file))
            self.status = STATUS_ERROR
            return
        start_find = content.find(self.start_str) + len(self.start_str)
        if content[start_find:].find(self.end_str) == -1:
            error_handle(1, 'Cannot find string %s in %s' % (self.end_str, self.grab_file))
            self.status = STATUS_ERROR
            return
        end_find = content[start_find:].find(self.end_str) + start_find
        find_str = content[start_find - self.start_str_include: end_find + self.end_str_include]
        with open(os.path.join(self.output_folder, self.grab_output_file), 'w') as f:
            f.write(find_str.strip('\n'))
        self.action_output[self.grab_output_file.split('.')[0]] = os.path.join(self.output_folder, self.grab_output_file)

    def insert(self):
        if self.input_data is not None:
            input_data = self.input_data
        elif self.input_file is not None:
            with open(self.input_file) as f:
                input_data = f.read()
        with open(self.dst_file) as f:
            content = f.readlines()
        for _i, _data in enumerate(content):
            if self.start_str is not None and self.end_str is not None:
                if content[_i] == self.start_str and content[_i + 1] == self.end_str and self.insert_count:
                    content.insert(_i + 1, input_data)
                    self.insert_count -= 1
            elif self.start_str is not None:
                if content[_i] == self.start_str and self.insert_count:
                    content.insert(_i + 1, input_data)
                    self.insert_count -= 1
            elif self.end_str is not None:
                if content[_i] == self.end_str and self.insert_count:
                    content.insert(_i, input_data)
                    self.insert_count -= 1
        with open(self.dst_file, 'w') as f:
            f.writelines(content)

#override specific line which doesn't contain self.line_include
#if need delete line, self.override_line = ''
    def override(self):
        match_list = 0
        try:
            with open(self.override_line_file) as f:
                content = f.readlines()
        except FileNotFoundError:
            error_handle(1, 'Cannot find ' + self.file)
            self.status = STATUS_ERROR
            return
        flen = len(content)
        for i in range(flen):
            if hasattr(self,'line_include') :
                if re.search(self.line_include, content[i], re.I):
                    match_list = match_list +1
                    if self.override_count == 0:
                        content[i] = self.override_line
                    else:
                        if match_list <= self.override_count:
                            content[i] = self.override_line
            else:
                if not re.search(self.line_exclude, content[i], re.I):
                    match_list = match_list + 1
                    if self.override_count == 0:
                        content[i] = self.override_line
                    else:
                        if match_list <= self.override_count:
                            content[i] = self.override_line



        with open(self.override_line_file, 'w') as f:
            f.writelines(content)

    def modify_line(self):
        match_list={}
        try:
            with open(self.modify_line_file) as f:
                content = f.readlines()
        except FileNotFoundError:
            error_handle(1, 'Cannot find ' + self.file)
            self.status = STATUS_ERROR
            return
        flen = len(content)
        for key in self.string_map.keys():
            match_list[key] = 0
        for i in range(flen):
            for key, value in self.string_map.items():
                match = re.search(key, content[i], re.I)
                if match:
                    if self.modify_count == 0:
                        head = match.group()
                        if self.insert_before == True:
                            content[i] = value + ' ' + head + '\n'
                        else:
                            content[i] = head + ' ' + value + '\n'
                    else:
                        match_list[key] = match_list[key] + 1
                        if match_list[key] <= self.modify_count:
                            head = match.group()
                            if self.insert_before == True:
                                content[i] = value + ' ' + head + '\n'
                            else:
                                content[i] = head + ' ' + value + '\n'


        with open(self.modify_line_file, 'w') as f:
            f.writelines(content)



    def action(self, count, action_str):
        super().action(count, action_str)

        # main action here
        if self.sub_function == ActionTxt.txt_create:
            self.create()

        if self.sub_function == ActionTxt.txt_read:
            self.read()

        elif self.sub_function == ActionTxt.txt_modify:
            self.modify()

        elif self.sub_function == ActionTxt.txt_grab:
            self.grab()

        elif self.sub_function == ActionTxt.txt_insert:
            self.insert()

        elif self.sub_function == ActionTxt.txt_override_line:
            self.override()

        elif self.sub_function == ActionTxt.txt_modify_line:
            self.modify_line()

        elif self.sub_function == ActionTxt.txt_mix:
            # lnb output
            for _i in self.layout:
                _tmp_action = _i['sub_function']

                if _tmp_action ==  ActionTxt.txt_create:
                    self.create_file = _i['file']
                    self.content = _i['content']
                    self.create()
                elif self.sub_function == ActionTxt.txt_read:
                    self.read_file = _i['file']
                    self.read()
                elif _tmp_action == ActionTxt.txt_modify:
                    self.modify_file = _i['file']
                    self.string_map = _i['string_map']
                    if 'match_count' in _i.keys():
                        self.match_count = _i['match_count']
                    self.modify()
                elif _tmp_action == ActionTxt.txt_grab:
                    self.grab_file = _i['file']
                    self.start_str = _i['start_str']
                    self.end_str = _i['end_str']
                    self.start_str_include = _i['start_str_include']
                    self.end_str_include = _i['end_str_include']
                    self.grab_output_file = _i['output_file']
                    self.grab()

                elif  _tmp_action == ActionTxt.txt_override_line:
                    self.override_line_file = _i['file']
                    self.override_count = _i['override_count']
                    self.override_line = _i['override_line']
                    if 'line_exclude' in _i.keys():
                        self.line_exclude = _i['line_exclude']
                    else:
                        self.line_include = _i['line_include']
                    self.override()
                elif _tmp_action == ActionTxt.txt_insert:
                    self.input_data = _i.get('input_data')
                    self.input_file = _i.get('input_file')
                    self.dst_file = _i['dst_file']
                    self.start_str = _i.get('start_str')
                    self.end_str = _i.get('end_str')
                    self.insert_count = _i['insert_count']
                    self.insert()
                elif _tmp_action == ActionTxt.txt_modify_line:
                    self.modify_line_file = _i['file']
                    self.string_map = _i['string_map']
                    self.insert_before = _i['insert_before']
                    self.modify_count = _i['modify_count']
                    self.modify_line()

        return

