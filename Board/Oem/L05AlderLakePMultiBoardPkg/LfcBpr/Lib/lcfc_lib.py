"""
BSD 2-Clause License

Copyright (c) 2020, Hefei LCFC Information Technology Co.Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
"""

import os
import json
import re
import tkinter as tk
from tkinter import messagebox
import datetime
import calendar

STATUS_SUCCESS = 0
STATUS_NOT_STARTED = 1
STATUS_ONGOING = 2
STATUS_ERROR = 10
STATUS_PRE_WORK_ERROR = 11

DEBUG_MODE = False
DEBUG_DATA = False
DEBUG_MAIL = False

class g:
    build_type = 0

    tool_path_abs = '' #D:\SW\Document\Technical\BuildAndRelease\LfcBpr
    working_path_abs = ''  #D:\Projects\S750ACHT\Phoenix\Projects\CeladonCzn\000
    project_config = {}
    output_path = ''
    workspace = ''
    build_id = ''
    bios_ver = ''
    bios_ver_full = ''
    bios_ver_full_test = ''
    ec_ver_full = ''
    crisis_ver_full = ''
    crisis_zip_name = ''
    release_date = ''
    argv = {}
    b_buildid_bat = False
    clean = True

    pack_crisis = True
    build_psw_crisis = True
    pack_web_release = True
    insyde = False
    phoenix = False
    local_sign = False
    lenovo_sign = False
    test_release = False
    formal_release = False
    send_release_mail = False
    upload_ftp = ''

    gui = False
    gui_b_1 = 0
    gui_t_2 = 0
    gui_b_3 = 0
    build_proc = 0

def DebugPrint (self, *args, sep=' ', end='\n', file=None):
    if DEBUG_DATA == 1:

       print(self, *args, sep=' ', end='\n', file=None)
    else:
        x=1

def error_handle(error_code, error_str):
    if error_code == 0:
        print('ERROR:', error_str)
    elif error_code == 1:
        print(error_str)
    if g.gui:
        tk.messagebox.showinfo('', error_str)


def set_var_to_disk(var, value, dic_in=None, loc=0):
    if loc == 0:
        file = os.path.join(g.output_path, 'data.json')
    else:
        file = os.path.join(g.tool_path_abs, 'config.json')
    try:
        with open(file) as f:
            dic = json.loads(f.read())
    except (FileNotFoundError, json.JSONDecodeError):
        dic = {}

    if isinstance(dic_in, dict):
        for key, value in dic_in.items():
            dic[key] = value
    else:
        dic[var] = value

    with open(file, 'w') as f:
        f.write(json.dumps(dic, indent=4))


def get_var_from_disk(var, loc=0):
    if loc == 0:
        file = os.path.join(g.output_path, 'data.json')
    else:
        file = os.path.join(g.tool_path_abs, 'config.json')
    try:
        with open(file) as f:
            dic = json.loads(f.read())
    except (FileNotFoundError, json.JSONDecodeError):
        return None

    return dic.get(var)


def process_bios_ver(fun=1):
    # fun = 1: get
    # fun = 2: set
    if g.insyde:
        try:
            with open('Project_%s.uni' % g.build_id, encoding='utf_16_le') as f:
                content = f.read()
                string1 = r'\n#string\s+STR_MISC_BIOS_VERSION\s+#language\s+en-US\s+"(.*)"'
                string2 = re.search(string1, content).group(0)
                bios_ver = re.search(r'"(.*)"', string2).group(0)[1:-1]
            if fun == 1:
                return bios_ver
            elif fun == 2:
                content = content.replace(bios_ver,
                                          (g.bios_ver_full if g.formal_release else g.bios_ver_full_test))
                with open('Project_%s.uni' % g.build_id, 'w', encoding='utf_16_le') as f:
                    f.write(content)
                return bios_ver
        except FileNotFoundError:
            return ''
    elif g.phoenix:
        try:
            with open('BIOS_Version_%s.def' % g.build_id) as f:
                content = f.read()
                string1 = r'\nConfig\s+BIOS_VERSION,\s+"\\"(.*)\\""'
                string2 = re.search(string1, content).group(0)
                bios_ver = re.search(r'"\\"(.*)\\""', string2).group(0)[3:-3]
            if fun == 1:
                return bios_ver
            elif fun == 2:
                content = content.replace(bios_ver, g.bios_ver_full if g.formal_release else g.bios_ver_full_test)
                with open('BIOS_Version_%s.def' % g.build_id, 'w') as f:
                    f.write(content)
                return bios_ver
        except FileNotFoundError:
            return ''

def process_ec_ver(fun = 1):
    # fun = 1: get
    # fun = 2: set
    if fun == 1:
        return g.ec_ver_full
    elif fun == 2:
        g.ec_ver_full = g.bios_ver_full[:2] + 'EC' + g.bios_ver_full[4:]
        return g.ec_ver_full

class OutputTkinter(object):
    def __init__(self, widget):
        self.widget = widget

    def write(self, string):
        self.widget.config(state="normal")
        self.widget.insert("end", string)
        self.widget.see('end')
        self.widget.update()
        self.widget.config(state="disabled")

    def flush(self):
        pass


def gather (src_dir,count,pastflag,past_scr_dir):
    global i

    count =  count +1

    if '.svn' in os.listdir(src_dir) :
        flag = 1
    else :
        flag = 0

    if count == 1:
        pastflag = flag

    if pastflag == 1 and flag == 0:

        g.workspace = past_scr_dir
        return

    gather(os.path.dirname(src_dir),count,flag,src_dir)

def findline(list_content,symbol):
    for i in list_content:
        if symbol in i:
            return i
    return ''

#1.choose characters after : 2 remove first and last space
def SplitString (String, Symbol):
    StringList = String.split(Symbol)[1:]
    print('StringList' + str(StringList))
    FinalString = ''.join(StringList)
    print('finalstring = ' +FinalString)
    FinalString=FinalString.strip()
    return FinalString

class detail_time:
    def __init__(self,time):
        self.year = time.strftime('%Y')
        self.month = time.strftime('%B')
        self.day = time.strftime('%d')
        self.day_of_week = time.strftime('%A')
        self.am_or_pm = time.strftime('%p')
        self.hour = time.strftime('%I')
        self.min = time.strftime('%M')

class mail_time:
    def __init__(self,time):
        self.day_of_week = time.split(',')[0]
        month_and_day = time.split(',')[1]
        year_and_time = time.split(',')[2]
        self.month = month_and_day.split(' ')[1]
        self.day = month_and_day.split(' ')[2]
        self.year = year_and_time.split(' ')[1]
        hour_and_min = year_and_time.split(' ')[2]
        self.am_or_pm = year_and_time.split(' ')[3]
        self.hour = hour_and_min.split(':')[0]
        self.min = hour_and_min.split(':')[1]

    def format_time(self):

        time = '{:s}-{:0>2s}-{:0>2s} {:s}'.format(self.year, str(list(calendar.month_name).index(self.month)), self.day, \
        self.format_hour_min_12_to_24())


        return datetime.datetime.strptime(time, "%Y-%m-%d %H:%M")

    def format_hour_min_12_to_24(self):

        hour_and_min = '{:0>2s}:{:0>2s} {:0>2s}'.format(self.hour, self.min, self.am_or_pm)
        if hour_and_min[-2:].upper() == 'AM':
            return hour_and_min[:-3]
        elif hour_and_min[-2:].upper() == 'PM':

            if hour_and_min[:2] == '12':
                return '00' + hour_and_min[2:-3]
            else:
                return str(int(hour_and_min[:2]) + 12) + hour_and_min[2:-3]



def insert_data(dict_data, data,replace_data, key):
    dict_data[key] = dict_data[key].replace(data, ''.join(replace_data))

    return dict_data

#insert new tool information into tool_info_dict, accroding to the tool_template_dict,only modify the name of the tool_template_dict
def insert_tool_info(tool_name,tool_info_dict,tool_template_dict):
    if tool_name not in tool_info_dict.keys():
        for key, value in tool_template_dict.items():
            if tool_name.startswith(key):
                new_tool_info = insert_data(dict_data=value, data=key, replace_data=tool_name, key='location')

        tool_info_dict[tool_name] = new_tool_info




def search(path=".", name=""):
    for item in os.listdir(path):
        item_path = os.path.join(path, item)
        if os.path.isfile(item_path):
            if re.match(name,item):
                yield item_path
                
#execute function if meet some errors, skip it                
#fun:fucntion
#passby_error: tuple
#arg:arg for function
def try_fun_bypass_error(fun,passby_error,arg):
    try:
        fun(arg)
    except passby_error:
        pass
