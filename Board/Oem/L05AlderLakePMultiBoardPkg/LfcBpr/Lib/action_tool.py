"""
BSD 2-Clause License

Copyright (c) 2020, Hefei LCFC Information Technology Co.Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
"""

from Lib.lcfc_lib import *
from Lib.action_base import ActionBase
import subprocess
import sys

def check_tool_return(this_tool_info, return_code, result_txt, output_file_name):
    result_return_code = this_tool_info['success_flag'][0]
    result_pass_string = this_tool_info['success_flag'][1]
    result_output_file_name = this_tool_info['success_flag'][2]
    if result_return_code is not None and result_return_code == return_code:
        return True
    elif result_pass_string:
        with open(result_txt) as f:
            content = f.read()
            if result_pass_string in content:
                return True
    elif result_output_file_name:
        if os.path.exists(output_file_name):
            return True
    return False


class ActionTool(ActionBase):
    tool_run_in_place = 1
    # file
    tool_copy_to_new_then_run = 2
    # file
    tool_specific_place =3

    tool_type_local_sign = 1
    tool_type_pack_exe = 2
    tool_type_svn_log = 3
    tool_type_checksum = 4
    tool_type_md5 = 5

    tool_type_common = 1
    tool_type_insyde = 2
    tool_type_phoenix = 3

    tool_info = {
        '7-Zip': {
            'location': r'Tool\ToolCommon\7-Zip',
            'name': '7z.exe',
            'type': tool_type_common,
            'success_flag': [0, '', False],
        },
        'CheckSumTools': {
            'location': r'Tool\ToolCommon\CheckSumTools',
            'name': 'LfcChecksumV103.exe',
            'type': tool_type_common,
            'success_flag': [None, 'Success!', False],
        },
        'Md5DeepTools': {
            'location': r'Tool\ToolCommon\Md5DeepTools',
            'name': 'md5deep.exe',
            'type': tool_type_common,
            'success_flag': [None, 'Success!', False],  #
        },
        'SVNServer': {
            'location': r'Tool\ToolCommon\SVNServer',
            'name': 'svn.exe',
            'type': tool_type_common,
            'success_flag': [None, 'Success!', False],  #
        },
        'SecurityFlash2.00.16.00': {
            'location': r'Tool\ToolInsyde\SecurityFlash2.00.16.00',
            'name': 'iEFIFlashSigner.exe',
            'type': tool_type_insyde,
            'success_flag': [0, '', False],
        },
        'SecurityFlash2.00.18.00': {
            'location': r'Tool\ToolInsyde\SecurityFlash2.00.18.00',
            'name': 'iEFIFlashSigner.exe',
            'type': tool_type_insyde,
            'success_flag': [0, '', False],
        },
        'SecureCrisis1.00': {
            'location': r'Tool\ToolInsyde\SecureCrisis1.00',
            'name': 'Pkcs1Sign.bat',
            'type': tool_type_insyde,
            'success_flag': [None, '', True],
        },
        'InsydeH2OFFT_x86_EFI_2.10.00': {
            'location': r'Tool\ToolInsyde\InsydeH2OFFT_x86_EFI_2.10.00',
            'name': 'H2OFFT-Sx64.efi',  # no support run in OS
            'type': tool_type_insyde,
            'success_flag': [None, '', False],
        },
        'InsydeH2OFFT_x86_WIN_6.25.00': {
            'location': r'Tool\ToolInsyde\InsydeH2OFFT_x86_WIN_6.25.00\tools',
            'name': 'iFdPacker.exe',
            'type': tool_type_insyde,
            'success_flag': [None, '', True],
        },
        'InsydeH2OFFT_x86_WIN_6.28.00': {
            'location': r'Tool\ToolInsyde\InsydeH2OFFT_x86_WIN_6.28.00\tools',
            'name': 'iFdPacker.exe',
            'type': tool_type_insyde,
            'success_flag': [None, '', True],
        },
        'SecureFlash2.29.1.0': {
            'location': r'Tool\ToolPhoenix\SecureFlash2.29.1.0',
            'name': 'UCWW.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },
        'FlashTools4.1.0.18': {
            'location': r'Tool\ToolPhoenix\FlashTools4.1.0.18\RELEASE',
            'name': 'WinFlash64.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },
        # 'FlashToolsGUI4.1.0.18': {
        #     'location': r'Tool\ToolPhoenix\FlashToolsGUI4.1.0.18\RELEASE\SctWinFlash64GUI',
        #     'name': 'SctWinFlash64.exe',
        #     'type': tool_type_phoenix,
        #     'success_flag': [None, '', True],  #
        # },
        'FlashTools4.1.0.64': {
            'location': r'Tool\ToolPhoenix\FlashTools4.1.0.64\RELEASE',
            'name': 'WinFlash64.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },
        'Packer2.3.0.0': {
            'location': r'Tool\ToolPhoenix\Packer2.3.0.0',
            'name': 'TdkPacker.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },
    }

    tool_common_info = {
        'SecurityFlash': {
            'location': r'Tool\ToolInsyde\SecurityFlash',
            'name': 'iEFIFlashSigner.exe',
            'type': tool_type_insyde,
            'success_flag': [0, '', False],
        },
        'SecureCrisis': {
            'location': r'Tool\ToolInsyde\SecureCrisis',
            'name': 'Pkcs1Sign.bat',
            'type': tool_type_insyde,
            'success_flag': [None, '', True],
        },
        'InsydeH2OFFT_x86_EFI_': {
            'location': r'Tool\ToolInsyde\InsydeH2OFFT_x86_EFI_',
            'name': 'H2OFFT-Sx64.efi',  # no support run in OS
            'type': tool_type_insyde,
            'success_flag': [None, '', False],
        },
        'InsydeH2OFFT_x86_WIN_': {
            'location': r'Tool\ToolInsyde\InsydeH2OFFT_x86_WIN_\tools',
            'name': 'iFdPacker.exe',
            'type': tool_type_insyde,
            'success_flag': [None, '', True],
        },
        'SecureFlash': {
            'location': r'Tool\ToolPhoenix\SecureFlash',
            'name': 'UCWW.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },
        'FlashTools': {
            'location': r'Tool\ToolPhoenix\FlashTools\RELEASE',
            'name': 'WinFlash64.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },

        'Packer': {
            'location': r'Tool\ToolPhoenix\Packer',
            'name': 'TdkPacker.exe',
            'type': tool_type_phoenix,
            'success_flag': [None, '', True],  #
        },
    }

    def __init__(self, **kwargs):
        super().__init__()
        self.sub_function = kwargs['sub_function']
        self.tool_name = kwargs['tool_name']
        if self.sub_function == ActionTool.tool_run_in_place:
            self.tool_para = kwargs['tool_para']
            self.output_file_name = kwargs['output_file_name']
        elif self.sub_function == ActionTool.tool_copy_to_new_then_run:
            self.new_place = kwargs['new_place']
            self.tool_para = kwargs['tool_para']
            self.output_file_name = kwargs['output_file_name']

        elif self.sub_function == ActionTool.tool_specific_place:
            self.specific_place = kwargs['specific_place']
            self.tool_para = kwargs['tool_para']
            self.output_file_name = kwargs['output_file_name']



    def action(self, count, action_str):
        super().action(count, action_str)
        this_tool_info = ActionTool.tool_info[self.tool_name]
        if self.sub_function == ActionTool.tool_run_in_place:
            self.tool_para = self.tool_para.replace('$output$',
                                                    os.path.join(g.working_path_abs,
                                                                 self.output_folder))
            os.chdir(os.path.join(g.tool_path_abs, this_tool_info['location']))
            with open(os.path.join(g.working_path_abs, self.output_folder, 'tool_cmd.txt'), 'w') as f:
                f.write(this_tool_info['name'] + ' ' + self.tool_para)
            return_code = \
                subprocess.call(this_tool_info['name'] + ' ' + self.tool_para + ' ' +
                                '>%s 2>&1' % os.path.join(g.working_path_abs, self.output_folder, 'result.txt'),
                                shell=True)
            os.chdir(g.working_path_abs)
            tool_run_status = check_tool_return(this_tool_info, return_code,
                                                os.path.join(self.output_folder, 'result.txt'),
                                                os.path.join(self.output_folder, self.output_file_name))
            if tool_run_status:
                self.action_output['file'] = os.path.join(self.output_folder, self.output_file_name)
            else:
                print('Tool run failed, please check the run command and result in:',
                      os.path.join(g.working_path_abs, self.output_folder))
                self.status = STATUS_ERROR
        elif self.sub_function == ActionTool.tool_copy_to_new_then_run:
            os.system(r'xcopy %s %s /E >%s' % (g.tool_path_abs+'\\'+ this_tool_info['location'] + '\\*.*', self.new_place + '\\',
                                               os.path.join(self.output_folder, 'copy_result.txt')))
            # shutil.copytree(this_tool_info['location'], self.new_place)
            os.chdir(self.new_place)
            with open(os.path.join(self.new_place, 'tool_cmd.txt'), 'w') as f:
                f.write(this_tool_info['name'] + ' ' + self.tool_para)
            return_code = os.system(this_tool_info['name'] + ' ' + self.tool_para +
                                    '>%s 2>&1' % 'result.txt')

            tool_run_status = check_tool_return(this_tool_info, return_code,
                                                'result.txt',
                                                self.output_file_name)
            if tool_run_status:
                self.action_output['file'] = os.path.join(self.new_place, self.output_file_name)
            else:
                print('Tool run failed, please check the run command and result in:',
                      os.path.join(self.new_place))
                self.status = STATUS_ERROR
            os.chdir(g.working_path_abs)
        if self.sub_function == ActionTool.tool_specific_place:
            #checksum.exe need to be run at its path,so jump to checksum.exe path
            os.chdir(self.specific_place)

            with open(os.path.join(self.specific_place, 'tool_cmd.txt'), 'w') as f:
                f.write(this_tool_info['name'] + ' ' + self.tool_para)

            return_code = os.system(this_tool_info['name'] + ' ' + self.tool_para +
                                    '>%s 2>&1' % 'result.txt')

            tool_run_status = check_tool_return(this_tool_info, return_code,
                                                'result.txt',
                                                self.output_file_name)
            if tool_run_status:
                self.action_output['file'] = os.path.join(self.specific_place, self.output_file_name)
            else:
                print('Tool run failed, please check the run command and result in:',
                      os.path.join(self.specific_place))
                self.status = STATUS_ERROR
            os.chdir(g.working_path_abs)
        return
