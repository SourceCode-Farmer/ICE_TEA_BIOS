from Lib.lcfc_lib import *
from Lib.action_base import ActionBase
import shutil


class ActionFile(ActionBase):
    file_folder_copy = 1
    file_folder_delete = 2
    file_folder_move = 3
    file_folder_rename = 4

    def __init__(self, sub_function, src, dst=''):
        super().__init__()
        self.sub_function = sub_function
        self.src = src
        self.dst = dst

    def action(self, count, action_str):
        super().action(count, action_str)
        if self.sub_function == ActionFile.file_folder_copy:
            if os.path.isfile(self.src):
                shutil.copyfile(self.src, self.dst)
            elif os.path.isdir(self.src):
                shutil.copytree(self.src, self.dst)
        elif self.sub_function == ActionFile.file_folder_delete:
            if os.path.isfile(self.src):
                os.remove(self.src)
            elif os.path.isdir(self.src):
                shutil.rmtree(self.src)
        elif self.sub_function == ActionFile.file_folder_move:
            if os.path.exists(self.src):
                shutil.move(self.src, self.dst)
        elif self.sub_function == ActionFile.file_folder_rename:
            if os.path.exists(self.src):
                os.rename(self.src, os.path.join(self.src.rsplit('\\', 1)[0], self.dst))
        return
