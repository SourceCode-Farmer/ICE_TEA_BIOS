from Lib.lcfc_lib import *
from Lib.action_base import ActionBase

class ActionHtml(ActionBase):
    string_tansfer_to_html = 1
    txt_tansfer_to_html = 2

    def __init__(self, **kwargs):
        super().__init__()
        self.sub_function = kwargs['sub_function']
        if self.sub_function == ActionHtml.string_tansfer_to_html:
            self.string = kwargs['string']
        if self.sub_function == ActionHtml.txt_tansfer_to_html:
            self.src = kwargs['src']
            if 'output_data' in kwargs.keys():
                self.output_data = kwargs['output_data']
            else:
                self.output_file = kwargs['output_file']


    def action(self, count, action_str):
        super().action(count, action_str)

        if self.sub_function == ActionHtml.string_tansfer_to_html:
            self.string_transfer()

        if self.sub_function == ActionHtml.txt_tansfer_to_html:
            self.txt_transfer()

        return

    def string_transfer(self):
        self.string = self.string +  '<br>'
        return self.string

    def txt_transfer(self):
        content = ''
        with open(self.src, 'r') as f:
            while 1:
                self.string = f.readline()
                if self.string =='':
                    break
                DebugPrint('self.string = %s' % self.string)
                content = content + self.string_transfer()
            DebugPrint('content = %s' % content)

            if hasattr(self, 'output_data'):
                self.output_data = content
            else:

                with open(os.path.join(self.output_folder, self.output_file), 'w') as f:
                    f.write(content)
                self.action_output[self.output_file.split('.')[0]] = os.path.join(self.output_folder, self.output_file)

        return
