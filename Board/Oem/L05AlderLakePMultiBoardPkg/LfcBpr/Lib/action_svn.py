from Lib.lcfc_lib import *
from Lib.action_base import ActionBase
import subprocess
import random
import shutil


class ActionSvn(ActionBase):
    svn_log = 1
    txt_cut_out = 2

    def __init__(self,**kwargs):
        super().__init__()
        self.sub_function = kwargs['sub_function']

        if self.sub_function == ActionSvn.svn_log:
            self.command = kwargs['command']
            self.project_path =  kwargs['project_path']
            print('project_path = %s' %self.project_path )
        elif self.sub_function == ActionSvn.txt_cut_out:
            self.input_file = kwargs['input_file']
            self.mode = kwargs['mode']
            self.encoding = kwargs['encoding']
            self.string_map = kwargs.get('string_map')

            self.CurrentLine = {'bios':0,'ec':0}  # Fix,Add,Moidfy items numbers
            self.CurrentLogContent = ''
            self. CurrentLogContentStrip = ''
            self.LineCount = 0


    def action(self, count, action_str):
        super().action(count, action_str)

        if self.sub_function == ActionSvn.svn_log:
            self.action_output['file'] = os.path.join(self.output_folder, 'svn_log.txt')
            #test SVN log
            if DEBUG_MODE :
                self.action_output['file'] = shutil.copy(os.path.join(g.tool_path_abs,
                                                                      r'Tool\Temp\svn_log.txt'),
                                                         os.path.join(self.output_folder, 'svn_log.txt'))
            else :
                fin_command = '{:s} {:s} > {:s}'.format(self.command,self.project_path,self.action_output['file'])
                print('fin command' + fin_command)
                proc = subprocess.Popen(fin_command, shell=True)
                g.build_proc = proc
                proc.communicate()
                status = proc.returncode
                if status:
                    self.status = STATUS_ERROR
                    return

        elif self.sub_function == ActionSvn.txt_cut_out:
            self.action_output['bios_file'] = os.path.join(self.output_folder, 'bios_svn_cutout.txt')
            self.action_output['ec_file'] = os.path.join(self.output_folder, 'ec_svn_cutout.txt')
            self.action_txt_cut_out()

        return


    def action_find_line_num(self):
        if self.input_file != None:
            with open(self.input_file, self.mode, encoding=self.encoding) as File:

                KeyWordLineNum = 0

                if self.string_map == None:
                    DebugPrint('CheckLogKeyWord:LastBiosVer=00')
                    while True:
                        KeyWordLineNum += 1
                        LogLine = File.readline()
                        # BIOSversion 0,get all log
                        # EOF
                        if LogLine == '':
                            break
                else:

                    while True:
                        KeyWordLineNum += 1
                        LogLine = File.readline()
                        for key, item in self.string_map.items():
                            if item in LogLine:
                                found = 1;
                                continue
                            else:
                                found = 0;
                                break

                        if found == 1:
                            DebugPrint(LogLine)
                            break
                        # EOF
                        elif not LogLine:
                            self.KeyWordLineNum = 0
                            break
                self.line_num = KeyWordLineNum
                print('KeyWordLineNum =%s' % KeyWordLineNum)

    def action_txt_cut_out(self):

        self.action_find_line_num()
        LineNum = self.line_num;
        if self.input_file != None:
            with open(self.input_file, self.mode, encoding=self.encoding) as File:

                LineCount = 0
                if LineNum >= 1:
                    while True:
                        LineCount += 1
                        if LineCount > LineNum:
                            break

                        LogLine = File.readline()
                        print(LogLine)
                        #self.CleanLog()

                        if LogLine.startswith('Publish'):  # publish means what to show in in the readme.txt  修改成Publish
                            DebugPrint('find Publish:' + LogLine)
                            self.CheckOutLogItem(LogLine, File, LineNum)
                            self.GiveValuesToCurrentLogContentStrip()
                            self.CreateFixedFile(self.CurrentLogContentStrip, self.action_output['bios_file'],'bios')

                        if LogLine.startswith('EC'):  # publish means what to show in in the readme.txt  修改成Publish
                            DebugPrint('find EC:' + LogLine)
                            self.CheckOutLogItem(LogLine, File, LineNum)
                            self.GiveValuesToCurrentLogContentStrip()
                            self.CreateFixedFile(self.CurrentLogContentStrip,self.action_output['ec_file'],'ec')
        return

    def CheckOutLogItem(self, CurrentLine, LogFile, LineNum):
        CurrentLog = CurrentLine.strip()
        DebugPrint('CurrentLog =' + CurrentLog)
        pattern1 = r'[a-zA-Z ]*:'
        pattern2 = r'------------------------------------------------------------------------'
        while True:
            NextLine = LogFile.readline().strip()
            self.LineCount = self.LineCount + 1
            if self.LineCount > LineNum:
                break
            if re.match(pattern1, NextLine) or re.match(pattern2, NextLine):
                break
            else:
                CurrentLog = CurrentLog + NextLine

        self.CurrentLogContent = CurrentLog
        # return NextLine  #contain the next item

    def CreateFixedFile(self, LogContext, File, flag):

        DebugPrint('File =' + File)
        DebugPrint('CreateFixedFile =' + LogContext)
        with open(File, 'a+') as FixedFile:

            FinalContent = DealWithLog(LogContext)

            self.CurrentLine[flag] = self.CurrentLine[flag] + 1
            AddLine = ''.join((str(self.CurrentLine[flag] ), ') ', FinalContent, '\n'))


            FixedFile.write(AddLine)

    def GiveValuesToCurrentLogContentStrip(self):

        self.CurrentLogContentStrip = SplitString(self.CurrentLogContent, ':')
        DebugPrint('svn_info.CurrentLogContentStrip =' + self.CurrentLogContentStrip)

    def CleanLog(self):

        self.CurrentLogContent = ''
        self.CurrentLogContentStrip = ''





#1.当log不是\n结尾时：去掉log末尾的所有空格和. ，再补上一个. 2.log首字母大写
def DealWithLog(LogContext):
    LogContext = LogContext.rstrip(' ')
    LogContext = LogContext.rstrip('.')
    LogContext = LogContext + '.'
    FirstWord = LogContext.split(' ')[0]
    FirstWord = FirstWord.capitalize()

    RestWords = LogContext.split(' ')[1:]

    WholeLog = []
    WholeLog.append(FirstWord)
    WholeLog.extend(RestWords)

    FinalContent = ' '.join(WholeLog)
    DebugPrint('FinalContent =' +FinalContent)
    return FinalContent
