"""
BSD 2-Clause License

Copyright (c) 2020, Hefei LCFC Information Technology Co.Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
"""

from Lib.lcfc_lib import *
import shutil


class ActionBase:
    def __init__(self):
        self.action_output = {}
        self.status = STATUS_NOT_STARTED
        self.output_folder = ''
        self.condition = True

    def pre_work(self, count, action_str):
        self.output_folder = os.path.join(g.output_path, '%02d_%s' % (count, action_str))
        if os.path.isdir(self.output_folder):
            shutil.rmtree(self.output_folder)
        os.makedirs(self.output_folder)

    def post_work(self, count, action_str):
        self.save_output(count, action_str)
        set_var_to_disk('%02d_%s_result' % (count, action_str), self.status)

    def action(self, count, action_str):
        if self.status == STATUS_ERROR:
            self.status = STATUS_PRE_WORK_ERROR
            return
        self.status = STATUS_SUCCESS

    def save_output(self, count, action_str):
        output_all = {}
        for output_name, output_value, in self.action_output.items():
            output_all[output_name] = output_value
        if output_all:
            set_var_to_disk('%02d_%s_output' % (count, action_str), output_all)

    def get_saved_output(self, count, action_str):
        output_all = get_var_from_disk('%02d_%s_output' % (count, action_str))
        if not output_all:
            return
        for output_name, output_value, in output_all.items():
            self.action_output[output_name] = output_value
