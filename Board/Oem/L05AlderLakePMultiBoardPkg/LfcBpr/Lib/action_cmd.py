"""
BSD 2-Clause License

Copyright (c) 2021, Hefei LCFC Information Technology Co.Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
"""

from Lib.lcfc_lib import *
from Lib.action_base import ActionBase
import shutil
import os
import subprocess

class ActionCmd(ActionBase):

    def __init__(self, build_id,cmd):
        self.build_id = build_id
        super().__init__()
        self.cmd = cmd

    def action(self, count, action_str):
        super().action(count, action_str)
        self.run()

    def run(self):
        proc = subprocess.Popen(self.cmd, shell=True)
        g.build_proc = proc
        proc.communicate()
        status = proc.returncode
        if status:
            self.status = STATUS_ERROR
            return