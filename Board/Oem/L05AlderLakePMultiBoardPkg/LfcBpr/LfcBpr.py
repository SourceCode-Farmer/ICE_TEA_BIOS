"""
BSD 2-Clause License

Copyright (c) 2020, Hefei LCFC Information Technology Co.Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

Name        : LfcBpr.py
Usage       : python LfcBpr.py
Description : LCFC Feature Code Build Pack Release
Author      : dahai.zhou@lcfuturecenter.com, feng.gu@lcfuturecenter.com
Change      :
Data           Author        Version    Description
2020/07/28     dahai.zhou    1.00       Initial release
2020/08/02     dahai.zhou    1.01       1. Modify tool output path mechanism
                                        2. Support Insyde sign tool
                                        3. Fix BIOS version get and set error
                                        4. Correct file name in Build
                                        5. Modify error message for pre-work
                                        6. Insert the whole file LfcReadMe_XX.txt to ReadMe.txt
                                        7. Support insert Readme.docx
                                        6. Add debug print feature
2020/08/03     dahai.zhou    1.02       1. Add error message for not setting correctly
2020/08/11     dahai.zhou    1.03       1. Fix debug build and clean build for Phoenix
                                        2. Add more debug tool path for Phoenix
                                        3. Add help message for GUI
2020/08/13     dahai.zhou    1.04       1. Fix the A command issue
                                        2. Fix the receive email issue
2020/08/15     dahai.zhou    1.05       1. Optimize for receive email
                                        2. Fix Insyde Windows flash issue
                                        3. Add SecurityFlash2.00.18.00 and InsydeH2OFFT_x86_WIN_6.28.00
2020/08/18     dahai.zhou    1.06       1. [Insyde]Fix the Shell nsh error
                                        2. [Insyde]Fix the 32bit exe cannot support 64 OS
                                        3. [All]Add warning message for normal build cmd + psw crisis code setting
2020/09/17     dahai.zhou    1.07       1. [All][Optimize]Remove input file before build
                                        2. [All][IssueFix]Program crash when meet some specific email sender
2021/01/07     feng.gu       1.08       1. [All][Modify] Crisis and Password Crisis bin from BuildIDCrisis.bin to BuildIDcn.bin
                                        2. [All][IssueFix]MFG rsp is same as Windows rsp.
                                        3. [All][IssueFix]Can not recognize mail
                                        4. [All][IssueFix]Password Crisis and Crisis are same bin.
                                        5. [Phoenix][IssueFix]Support FlashTools4.1.0.64
                                        6. [All][IssueFix]Change Uefi64 cap to bin
                                        7. [All][IssueFix]When flash BIOS, show BIOS image cannot found ,that is because the default.rsp call bios.cap but contain the bioss.cap
                                        8. [All][IssueFix]If message is calendar,it will cause deadloop
                                        9. [All][IssueFix]Inbox.item issue,rewrite action_email.py
                                        10. [All][IssueFix]When enable debug mode,the path is not correct:rootpath:\Tool\Temp\BiosMe_Phoenix.cap
                                        11. [All][IssueFix]32,,64 bit exe can not run,command is wrong,and replace force u with force in default.rsp and default_MFG.rsp
                                        12. [All][IssueFix]If message is calendar,it will cause deadloop
                                        13. [All][Add]Optimizeiss MyAppVersionInfoProductVersion should be 0.xx before golden ,1.xx when golden

2021/4/29     feng.gu        2.00       1. [ALL]Complete ReleaseNote.txt
                                        2. [ALL]Complete ReadMe.txt
                                        3. [ALL]Complete Crisis and Password Crisis package changed following 05_LCFC LNB BIOS&EC&ME Release Specification 2021.4.22
                                        4. [All]Add release mail
                                        5. [All]Add grab svn log and extract function for reservation
                                        6. [Fix] Loop check one specific mail if the mail is meeting type
                                        
2021/5/28     feng.gu        2.01       1. [ALL]Move ProjectConfig.ini ,ReleaseNote.txt,*.ini and *.rsp to project LfcBprInput document
                                        2. [ALL]Add note into ReleaseNote.txt and release-mail.
                                        3. [ALL]Modify format and items in ProjectConfig.ini and release mail.
(please search and modify tool_version and tool_release_date if update version)
"""

from Lib.lcfc_lib import *
from Lib.action_build import ActionBuild
from Lib.action_email import ActionEmail
from Lib.action_pack import ActionPack
from Lib.action_tool import ActionTool
from Lib.action_file import ActionFile
from Lib.action_txt import ActionTxt
from Lib.action_docx import ActionDocx
from Lib.action_ftp import ActionFtp
from Lib.action_svn import ActionSvn
from Lib.action_html import ActionHtml
from Lib.action_cmd import ActionCmd
from Lib.action_gui import LfcLabelFrame
from Lib.action_gui import LfcRadiobuttonGroup
from Lib.LcfcGetRawFileLib import lcfc_get_raw_file
import os
import datetime
from functools import reduce
import configparser
import argparse
import sys
import threading
import signal
import tkinter as tk
from tkinter.scrolledtext import ScrolledText
from tkinter import Radiobutton
from tkinter import IntVar
from tkinter import messagebox
from tkinter import filedialog
from tkinter import Listbox
import subprocess
import time

tool_version = 'V2.01'
tool_release_date = '2021/05/28'

fun = {}
project_config = {}
support_ini_version = ['1.05']

BIOS_VER_TEMPLATE = '29'
BIOS_VER_FULL_TEMPLATE = 'A7CN29WW'
BIOS_VER_FULL_TEST_TEMPLATE = 'A7CN29WWT01'
CRISIS_VER_FULL_TEMPLATE = 'A7Crisis'
MARKET_NAME_TEMPLATE = 'S540-13ARE'
RELEASE_DATE_TEMPLATE = '2000-00-00'


def get_action_common(n):
    global fun

    if n == 'pre_build':
        fun[n] = ActionCmd(build_id=g.build_id,cmd = 'b_%s.bat'%g.build_id)
        fun[n].condition = g.b_buildid_bat
        return fun[n]

    if n == 'clean':
        fun[n] = ActionBuild(build_type=ActionBuild.BUILD_TYPE_CLEAN, build_id=g.build_id)
        fun[n].condition = g.clean
        return fun[n]
        
    if n == 'BootGuardLSign_bat':
        fun[n] = ActionCmd(build_id=g.build_id,cmd = 'BootGuardLSign.bat '+ g.build_id)
        return fun[n]
        
    if n == 'build':
        fun[n] = ActionBuild(build_type=g.build_type, build_id=g.build_id)
        return fun[n]

    if n == 'build_psw_crisis':
        fun[n] = ActionBuild(build_type=ActionBuild.BUILD_TYPE_NORMAL, build_id=g.build_id,
                             psw_crisis=True)
        fun[n].condition = g.build_psw_crisis
        return fun[n]
        
    if n == 'hook_bat':
        fun[n] = ActionCmd(build_id=g.build_id,cmd = 'pswd_cris.bat '+ g.build_id)
        return fun[n]

    if n == 'tool_local_sign_rel_insyde':
        para = r'sign -f Mocca.pfx -bios %s -ini %s -o bioss.fd -opath $output$' \
               % (os.path.join(g.working_path_abs, fun['build'].action_output['bios_smt']),
                  'Platform_REL.ini')
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsignbiostool'],
                            tool_para=para,
                            output_file_name='bioss.fd')
        return fun[n]

    if n == 'tool_local_sign_mfg_insyde':
        para = r'sign -f Mocca.pfx -bios %s -ini %s -o bioss.fd -opath $output$' \
               % (os.path.join(g.working_path_abs, fun['build'].action_output['bios_smt']),
                  'Platform_MFG.ini')
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsignbiostool'],
                            tool_para=para,
                            output_file_name='bioss.fd')
        return fun[n]

    if n == 'tool_local_sign_nac_insyde':
        para = r'sign -f Mocca.pfx -bios %s -ini %s -o bioss.fd -opath $output$' \
               % (os.path.join(g.working_path_abs, fun['build'].action_output['bios_smt']),
                  'Platform_NAC.ini')
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsignbiostool'],
                            tool_para=para,
                            output_file_name='bioss.fd')
        return fun[n]

    if n == 'tool_local_sign_crisis_insyde':
        para = r'Mocca.pfx %s $output$\crisis_signed.bin' % (os.path.join(g.working_path_abs,
                                                                          fun['build'].action_output['crisis']))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsigncrisistool'],
                            tool_para=para,
                            output_file_name='crisis_signed.bin')
        return fun[n]

    if n == 'tool_local_sign_psw_crisis_insyde':
        para = r'Mocca.pfx %s $output$\crisis_signed.bin'\
               % (os.path.join(g.working_path_abs, fun['build_psw_crisis'].action_output.get('crisis', '')))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsigncrisistool'],
                            tool_para=para,
                            output_file_name='crisis_signed.bin')
        fun[n].condition = g.build_psw_crisis
        return fun[n]

    if n == 'tool_local_sign_phoenix':
        para = r'-bios %s -winsign Mocca -out $output$\bios.cap' \
               % (os.path.join(g.working_path_abs, fun['build'].action_output['bios_cap']))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsignbiostool'],
                            tool_para=para,
                            output_file_name='bios.cap')
        return fun[n]

    if n == 'tool_local_sign_psw_crisis_phoenix':
        para = r'-bios %s -winsign Mocca -out $output$\bios.cap' \
               % (os.path.join(g.working_path_abs,
                               fun['build_psw_crisis'].action_output.get('bios_cap', '')))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['localsigncrisistool'],
                            tool_para=para,
                            output_file_name='bios.cap')
        fun[n].condition = g.build_psw_crisis
        return fun[n]

    if n == 'copy_nac_ini_to_localsignbiostool':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_NAC_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['localsignbiostool']]['location'],
                                             'Platform_NAC.ini'))
        return fun[n]

    if n == 'copy_rel_ini_to_localsignbiostool':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_REL_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['localsignbiostool']]['location'],
                                             'Platform_REL.ini'))
        return fun[n]

    if n == 'copy_mfg_ini_to_localsignbiostool':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_MFG_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['localsignbiostool']]['location'],
                                             'Platform_MFG.ini'))
        return fun[n]

    if n == 'copy_rel_32_ini':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_REL_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['packwintool']]['location'],
                                             '..\\x86', 'Platform.ini'))
        return fun[n]

    if n == 'copy_mfg_32_ini':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_MFG_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['packwintool']]['location'],
                                             '..\\x86', 'Platform.ini'))
        return fun[n]

    if n == 'copy_mfg_64_ini':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_MFG_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['packwintool']]['location'],
                                             '..\\x86_64', 'Platform.ini'))
        return fun[n]

    if n == 'copy_nac_64_ini':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'Platform_NAC_%s.ini' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['packwintool']]['location'],
                                             '..\\x86_64', 'Platform.ini'))
        return fun[n]

    if n == 'copy_rel_rsp':

        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'default_REL_%s.rsp' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['flashtool']]['location'],
                                             'default.rsp'))


        return fun[n]

    if n == 'copy_mfg_rsp':


        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'default_MFG_%s.rsp' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['flashtool']]['location'],
                                             'default.rsp'))
        return fun[n]

    if n == 'copy_nac_rsp':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(g.working_path_abs,
                                             'LfcBprInput',
                                             'default_NAC_%s.rsp' %g.build_id),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['flashtool']]['location'],
                                             'default.rsp'))
        return fun[n]

    if n == 'tool_pack_exe_ia32_rel_phoenix' or n == 'tool_pack_exe_ia32_mfg_phoenix':
        para = r'/ext /os w3 /dir %s /exe1 %s /out $output$\bios.exe /c' % \
               (os.path.join(g.tool_path_abs,
                             ActionTool.tool_info[project_config['flashtool']]['location']),
                os.path.join(g.tool_path_abs,
                             ActionTool.tool_info[project_config['flashtool']]['location'],
                             'SctWinFlash32.exe'))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_x64_mfg_phoenix' or n == 'tool_pack_exe_x64_nac_phoenix':
        para = r'/ext /os w6 /dir %s /exe1 %s /out $output$\bios.exe /c' % \
               (os.path.join(g.tool_path_abs,
                             ActionTool.tool_info[project_config['flashtool']]['location']),
                os.path.join(g.tool_path_abs,
                             ActionTool.tool_info[project_config['flashtool']]['location'],
                             'SctWinFlash64.exe'))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]
    return None


def get_action_insyde_special(n):
    global fun

    if n == 'tool_pack_exe_ia32_local_sign_rel_insyde':
        para = r'-winsrc ..\x86 -winini -b 3264 -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_local_sign_rel_insyde'].action_output['file'])
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_ia32_local_sign_mfg_insyde':
        para = r'-winsrc ..\x86 -winini -b 3264 -winarg "-b" -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_local_sign_mfg_insyde'].action_output['file'])
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_x64_local_sign_mfg_insyde':
        para = r'-winsrc ..\x86_64 -winini -b 64 -winarg "-b" -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_local_sign_mfg_insyde'].action_output['file'])
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_x64_local_sign_nac_insyde':
        para = r'-winsrc ..\x86_64 -winini -b 64 -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_local_sign_nac_insyde'].action_output['file'])
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_ia32_lnv_sign_rel_insyde':
        para = r'-winsrc ..\x86 -winini -b 3264 -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_7z_uncompress2'].action_output['file'],
                            'Win%s.fd' % g.bios_ver_full)
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_ia32_lnv_sign_mfg_insyde':
        para = r'-winsrc ..\x86 -winini -b 3264 -winarg "-b" -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_7z_uncompress2'].action_output['file'],
                            'Mfg%s.fd' % g.bios_ver_full)
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_x64_lnv_sign_mfg_insyde':
        para = r'-winsrc ..\x86_64 -winini -b 64 -winarg "-b" -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_7z_uncompress2'].action_output['file'],
                            'Mfg%s.fd' % g.bios_ver_full)
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]

    if n == 'tool_pack_exe_x64_lnv_sign_nac_test_insyde':
        para = r'-winsrc ..\x86_64 -winini -b 64 -fv %s -output $output$\bios.exe' % \
               os.path.join(g.working_path_abs,
                            fun['tool_7z_uncompress2'].action_output['file'],
                            'Win%s.fd' % g.bios_ver_full_test)
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name=project_config['packwintool'],
                            tool_para=para,
                            output_file_name='bios.exe')
        return fun[n]
    return None


def get_action_phoenix_special(n):
    global fun
    if n == 'file_copy_bios_cap_local_sign_phoenix':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=fun['tool_local_sign_phoenix'].action_output['file'],
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['flashtool']]['location'],
                                             'bios.cap'))
        return fun[n]

    if n == 'file_copy_bios_cap_lnv_sign_phoenix':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(fun['tool_7z_uncompress2'].action_output['file'],
                                             g.bios_ver_full + '.cap'),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['flashtool']]['location'],
                                             'bios.cap'))
        return fun[n]

    if n == 'file_copy_bios_cap_lnv_sign_test_phoenix':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_copy,
                            src=os.path.join(fun['tool_7z_uncompress2'].action_output['file'],
                                             g.bios_ver_full_test + '.cap'),
                            dst=os.path.join(g.tool_path_abs,
                                             ActionTool.tool_info[project_config['flashtool']]['location'],
                                             'bios.cap'))
        return fun[n]
    return None


def get_action_lenovo_sign(n):
    global fun

    if n == 'pack_lnv_sign':
        pack_layout = []
        if g.formal_release:
            pack_layout = [
                [ActionPack.pack_create_folder, None, r'Input', True],
                [ActionPack.pack_copy_file,
                 fun['build'].action_output['bios_smt'] if g.insyde else
                 fun['build'].action_output['bios_cap'],
                 r'Input\A7CN29WW.fd' if g.insyde else r'Input\A7CN29WW.cap', True],
                [ActionPack.pack_copy_file,
                 fun['build'].action_output['crisis'] if g.insyde else
                 fun['build'].action_output['bios_cap'],
                 r'Input\A7Crisis.bin' if g.insyde else r'Input\A7Crisis.cap', True],
                [ActionPack.pack_copy_file,
                 fun['build_psw_crisis'].action_output['crisis'] if g.insyde else
                 fun['build_psw_crisis'].action_output['bios_cap'],
                 r'Input\A7Crisis_PSW.bin' if g.insyde else r'Input\A7Crisis_PSW.cap', g.build_psw_crisis],
                [ActionPack.pack_copy_file,
                 os.path.join(g.working_path_abs,'LfcBprInput','Platform_REL_%s.ini' % g.build_id),
                 r'Input\Platform_REL.ini', g.insyde],
                [ActionPack.pack_copy_file,
                 os.path.join(g.working_path_abs,'LfcBprInput','Platform_MFG_%s.ini' % g.build_id),
                 r'Input\Platform_MFG.ini', g.insyde],
                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\LenovoSign\Sign_Formal_Insyde.cfg') if g.insyde else
                 os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\LenovoSign\Sign_Formal_Phoenix.cfg'),
                 r'Input\Sign.cfg', True],
            ]
        elif g.test_release:
            pack_layout = [
                [ActionPack.pack_create_folder, None, r'Input', True],
                [ActionPack.pack_copy_file,
                 fun['build'].action_output['bios_smt'] if g.insyde else
                 fun['build'].action_output['bios_cap'],
                 r'Input\A7CN29WWT01.fd' if g.insyde else r'Input\A7CN29WWT01.cap', True],
                [ActionPack.pack_copy_file,
                 os.path.join(g.working_path_abs,'LfcBprInput','Platform_NAC_%s.ini' % g.build_id),
                 r'Input\Platform_NAC.ini', g.insyde],
                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\LenovoSign\Sign_Test_Insyde.cfg') if g.insyde else
                 os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\LenovoSign\Sign_Test_Phoenix.cfg'),
                 r'Input\Sign.cfg', True],
            ]
        for _i, _data in enumerate(pack_layout):
            for _j in range(len(_data)):
                if isinstance(_data[_j], str):
                    _data[_j] = _data[_j].replace(BIOS_VER_FULL_TEST_TEMPLATE, g.bios_ver_full_test)
                    _data[_j] = _data[_j].replace(BIOS_VER_FULL_TEMPLATE, g.bios_ver_full)
                    _data[_j] = _data[_j].replace(CRISIS_VER_FULL_TEMPLATE, g.crisis_ver_full)
        fun[n] = ActionPack(layout=pack_layout)
        return fun[n]

    if n == 'txt_modify_cfg':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                           file=os.path.join(fun['pack_lnv_sign'].action_output['packed_folder'],
                                             r'Sign.cfg'),
                           string_map={BIOS_VER_FULL_TEST_TEMPLATE: g.bios_ver_full_test,
                                       BIOS_VER_FULL_TEMPLATE: g.bios_ver_full,
                                       CRISIS_VER_FULL_TEMPLATE: g.crisis_ver_full,
                                       '2.00.16.00': project_config['lenovosignbiostool'],
                                       '2.29.1.0': project_config['lenovosignbiostool']},
                           )
        return fun[n]

    if n == 'tool_7z_compress':
        para = r'a $output$\Input.7z %s\*' % \
               os.path.join(g.working_path_abs, fun['pack_lnv_sign'].action_output['packed_folder'])
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='Input.7z')
        return fun[n]

    if n == 'prepare_time_stamp':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_create,
                           file= r'time_stamp.txt',
                           content = 'time stamp:'+str(time.time()))
        return fun[n]

    if n == 'get_time_stamp':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_read,
                           file=fun['prepare_time_stamp'].action_output['file'],
                           )
        return fun[n]

    if n == 'send_email_to_sign':

        send_para = {
            'email_receiver': 'biosromagent@lenovo.com',
            'email_cc': '',
            'email_bcc': '',
            'email_subject': 'sign BIOS %s %s' % (g.build_id, fun['get_time_stamp'].read_output),
            'email_content': '',
            'email_attachment': [os.path.join(g.working_path_abs,
                                              fun['tool_7z_compress'].action_output['file'])],
        }
        fun[n] = ActionEmail(sub_function=ActionEmail.email_send,
                             para_dict=send_para)
        return fun[n]



    if n == 'wait_for_sign_back':
        receive_para = {
            'email_sender': 'biosromagent@lenovo.com',
            'email_subject': 'RE: [External] sign BIOS %s %s'% (g.build_id, fun['get_time_stamp'].read_output),
            'email_deadline': datetime.datetime.now()
        }
        fun[n] = ActionEmail(sub_function=ActionEmail.email_receive,
                             para_dict=receive_para)
        return fun[n]

    if n == 'tool_7z_uncompress1':
        para = r'e %s -o"$output$\Unzipped1" -plenovo' % \
               (os.path.join(g.working_path_abs, fun['wait_for_sign_back'].action_output['file']))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='Unzipped1')
        return fun[n]

    if n == 'tool_7z_uncompress2':
        _input = os.path.join(g.working_path_abs, fun['tool_7z_uncompress1'].action_output['file'])
        file_name = [_i for _i in os.listdir(_input) if '.7z' in _i][0]
        para = r'e %s -o"$output$\Unzipped2"' % (os.path.join(_input, file_name))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='Unzipped2')
        return fun[n]

    if n == 'change_uncompress2_package_files_name':
        pack_layout = [
            [ActionPack.pack_rename_file,
             os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'BBCrisis.cap'),
             r'%scn.cap' %g.build_id.lower(), True],
            [ActionPack.pack_rename_file,
             os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'BBCN17WW.cap'),
             r'%s.cap' % g.bios_ver_full, True],
            [ActionPack.pack_rename_file,
             os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'BBCrisis_PSW.cap'),
             r'%scn_PSW.cap' % g.build_id.lower(), True],
        ]
        fun[n] = fun[n] = ActionPack(layout=pack_layout)
        return fun[n]
    return None


def get_action_final_pack(n):
    global fun

    if n == 'final_pack':
        pack_layout = []
        if g.formal_release:
            Crisis_folder = os.path.join(r'A7CN29WW\Crisis', g.crisis_zip_name)

            pack_layout = [
                #crisis
                [ActionPack.pack_create_folder, None, r'A7CN29WW', True],
                [ActionPack.pack_create_folder, None, Crisis_folder, g.pack_crisis],

                [ActionPack.pack_copy_file,
                 fun['tool_local_sign_crisis_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'CriA7Crisis.bin')
                 if g.insyde and g.lenovo_sign else
                 fun['tool_local_sign_phoenix'].action_output['file']
                 if g.phoenix and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'A7Crisis.cap'),
                 os.path.join(Crisis_folder,r'A7Crisis.bin'), g.pack_crisis],

                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\CrisisReadme.txt'),
                 os.path.join(Crisis_folder,r'Readme.txt'), g.pack_crisis],
                #mfg
                [ActionPack.pack_create_folder, None, r'A7CN29WW\Mfg', True],
                [ActionPack.pack_copy_file,
                 fun['tool_pack_exe_ia32_local_sign_mfg_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 fun['tool_pack_exe_ia32_lnv_sign_mfg_insyde'].action_output['file']
                 if g.insyde and g.lenovo_sign else
                 fun['tool_pack_exe_ia32_mfg_phoenix'].action_output['file'],
                 r'A7CN29WW\Mfg\A7CN29WW_IA32_SWDL.exe', True],
                [ActionPack.pack_copy_file, fun['build'].action_output['bios_smt'],
                 r'A7CN29WW\Mfg\A7CN29WW_SMT.bin', True],
                [ActionPack.pack_copy_file,
                 fun['tool_pack_exe_x64_local_sign_mfg_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 fun['tool_pack_exe_x64_lnv_sign_mfg_insyde'].action_output['file']
                 if g.insyde and g.lenovo_sign else
                 fun['tool_pack_exe_x64_mfg_phoenix'].action_output['file'],
                 r'A7CN29WW\Mfg\A7CN29WW_X64_SWDL.exe', True],
                [ActionPack.pack_create_folder, None, r'A7CN29WW\PasswordCrisis', g.build_psw_crisis],
                [ActionPack.pack_copy_file,
                 fun['tool_local_sign_psw_crisis_insyde'].action_output.get('file')
                 if g.insyde and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'PSWA7Crisis_PSW.bin')
                 if g.insyde and g.lenovo_sign else
                 fun['tool_local_sign_psw_crisis_phoenix'].action_output.get('file')
                 if g.phoenix and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'A7Crisis_PSW.cap'),
                 r'A7CN29WW\PasswordCrisis\A7Crisis.bin', g.build_psw_crisis],
                [ActionPack.pack_create_folder, None, r'A7CN29WW\Uefi64', True],
                [ActionPack.pack_copy_file,
                 fun['tool_local_sign_rel_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'WinA7CN29WW.fd')
                 if g.insyde and g.lenovo_sign else
                 fun['tool_local_sign_phoenix'].action_output['file']
                 if g.phoenix and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'A7CN29WW.cap'),
                 r'A7CN29WW\Uefi64\A7CN29WW.fd' if g.insyde else
                 r'A7CN29WW\Uefi64\A7CN29WW.bin', True],
                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, ActionTool.tool_info[project_config['shellflashtool']]['location'],
                              'flash.nsh') if g.insyde else
                 os.path.join(g.tool_path_abs, ActionTool.tool_info[project_config['flashtool']]['location'],
                              'flash.nsh'), r'A7CN29WW\Uefi64\flash.nsh', True],
                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs,
                              ActionTool.tool_info[project_config['shellflashtool']]['location'],
                              'H2OFFT-Sx64.efi') if g.insyde else
                 os.path.join(g.tool_path_abs,
                              ActionTool.tool_info[project_config['flashtool']]['location'],
                              'ShellFlash64.efi'),
                 r'A7CN29WW\Uefi64\H2OFFT-Sx64.efi' if g.insyde else
                 r'A7CN29WW\Uefi64\ShellFlash64.efi', True],
                [ActionPack.pack_copy_tree, os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\ISCC\Lenovo'),
                 r'A7CN29WW\Web_Release', g.golden],
                [ActionPack.pack_create_folder, None, r'A7CN29WW\Windows', g.pack_web_release],
                [ActionPack.pack_copy_file,
                 fun['tool_pack_exe_ia32_local_sign_rel_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 fun['tool_pack_exe_ia32_lnv_sign_rel_insyde'].action_output['file']
                 if g.insyde and g.lenovo_sign else
                 fun['tool_pack_exe_ia32_rel_phoenix'].action_output['file'],
                 r'A7CN29WW\Windows\A7CN29WW.exe', g.pack_web_release],
                [ActionPack.pack_copy_file, os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\ReadMe.txt'),
                 r'A7CN29WW\ReadMe.txt', True],

                [ActionPack.pack_copy_file, os.path.join(g.working_path_abs,'LfcBprInput', r'LfcReleaseNote_%s.txt'%g.build_id ),
                 r'A7CN29WW\ReleaseNote.txt', True],

#                [ActionPack.pack_copy_files, os.path.join(g.tool_path_abs, ActionTool.tool_info['CheckSumTools']['location']),
#                 r'A7CN29WW', True],

                 [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, r'Tool\ToolCommon\CheckSumTools\LfcChecksumV103.exe'), 
                 r'A7CN29WW\LfcChecksumV103.exe', True],

                 [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, r'Tool\ToolCommon\CheckSumTools\ChecksumGolden.ini') if g.golden else
                 os.path.join(g.tool_path_abs, r'Tool\ToolCommon\CheckSumTools\Checksum.ini'),
                 r'A7CN29WW\Checksum.ini', True],  

            ]
        elif g.test_release:
            pack_layout = [
                [ActionPack.pack_create_folder, None, r'A7CN29WWT01', True],
                [ActionPack.pack_copy_file,
                 fun['tool_pack_exe_x64_local_sign_nac_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 fun['tool_pack_exe_x64_lnv_sign_nac_test_insyde'].action_output['file']
                 if g.insyde and g.lenovo_sign else
                 fun['tool_pack_exe_x64_nac_phoenix'].action_output['file'],
                 r'A7CN29WWT01\A7CN29WWT01.exe', True],
                [ActionPack.pack_copy_file,
                 fun['tool_local_sign_nac_insyde'].action_output['file']
                 if g.insyde and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'WinA7CN29WWT01.fd')
                 if g.insyde and g.lenovo_sign else
                 fun['tool_local_sign_phoenix'].action_output['file']
                 if g.phoenix and g.local_sign else
                 os.path.join(fun['tool_7z_uncompress2'].action_output['file'], 'A7CN29WWT01.cap'),
                 r'A7CN29WWT01\A7CN29WWT01.fd' if g.insyde else
                 r'A7CN29WWT01\A7CN29WWT01.cap', True],
                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs, ActionTool.tool_info[project_config['shellflashtool']]['location'],
                              'flash_test.nsh') if g.insyde else
                 os.path.join(g.tool_path_abs, ActionTool.tool_info[project_config['flashtool']]['location'],
                              'flash_test.nsh'), r'A7CN29WWT01\flash.nsh', True],
                [ActionPack.pack_copy_file,
                 os.path.join(g.tool_path_abs,
                              ActionTool.tool_info[project_config['shellflashtool']]['location'],
                              'H2OFFT-Sx64.efi') if g.insyde else
                 os.path.join(g.tool_path_abs,
                              ActionTool.tool_info[project_config['flashtool']]['location'],
                              'ShellFlash64.efi'),
                 r'A7CN29WWT01\H2OFFT-Sx64.efi' if g.insyde else
                 r'A7CN29WWT01\ShellFlash64.efi', True],
                [ActionPack.pack_copy_file, fun['build'].action_output['bios_smt'],
                 r'A7CN29WWT01\A7CN29WWT01.bin', True],
            ]
        #copy project folder bios_smt1.bin ,bios_smt2.bin ... to pack    
        match_bios_bin = []
        for path in search(path=fun['build'].output_folder, name=r"bios_smt\d{1,}.bin"):
            match_bios_bin.append(path)

        if g.test_release:
            for bios_bin in match_bios_bin:

                pack_layout.append([ActionPack.pack_copy_file, bios_bin,
                 r'A7CN29WWT01\A7CN29WWT01_SMT(part%s).bin'%os.path.basename(bios_bin)[8:-4], True],)

        if g.formal_release:
            for bios_bin in match_bios_bin:
                pack_layout.append([ActionPack.pack_copy_file, bios_bin,
                 r'A7CN29WW\Mfg\A7CN29WW_SMT(part%s).bin'%os.path.basename(bios_bin)[8:-4], True],)

        DebugPrint('Orignal lIST = %s'% pack_layout)
        for _i, _data in enumerate(pack_layout):
            for _j in range(len(_data)):
                if isinstance(_data[_j], str):
                    _data[_j] = _data[_j].replace(BIOS_VER_FULL_TEST_TEMPLATE, g.bios_ver_full_test)
                    _data[_j] = _data[_j].replace(BIOS_VER_FULL_TEMPLATE, g.bios_ver_full)
                    _data[_j] = _data[_j].replace(CRISIS_VER_FULL_TEMPLATE, g.crisis_ver_full)
        DebugPrint('Final lIST = %s' % pack_layout)
        fun[n] = ActionPack(layout=pack_layout)
        return fun[n]

    if n == 'txt_modify_nsh':
        file = os.path.join(fun['final_pack'].action_output['packed_folder'], r'Uefi64\flash.nsh')
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                           file=file,
                           string_map={BIOS_VER_FULL_TEMPLATE: g.bios_ver_full})
        return fun[n]

    if n == 'txt_modify_nsh_test_release':
        file = os.path.join(fun['final_pack'].action_output['packed_folder'], r'flash.nsh')
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                           file=file,
                           string_map={BIOS_VER_FULL_TEST_TEMPLATE: g.bios_ver_full_test})
        return fun[n]

    if n == 'file_rename_iss':
        fun[n] = ActionFile(sub_function=ActionFile.file_folder_rename,
                            src=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                             r'Web_Release\%s.iss' % BIOS_VER_FULL_TEMPLATE),
                            dst=g.bios_ver_full + '.iss')
        fun[n].condition = g.golden
        return fun[n]

    if n == 'txt_modify_iss':
        if project_config['golden'] =='1':
            fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                           file=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                             r'Web_Release\%s.iss' % g.bios_ver_full),
                           string_map={BIOS_VER_FULL_TEMPLATE: g.bios_ver_full,
                                       'MyAppVersionInfoProductVersion "1.%s"' % BIOS_VER_TEMPLATE:
                                           'MyAppVersionInfoProductVersion "1.%s"' % g.bios_ver})
        else:
            fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                           file=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                             r'Web_Release\%s.iss' % g.bios_ver_full),
                           string_map={BIOS_VER_FULL_TEMPLATE: g.bios_ver_full,
                                       'MyAppVersionInfoProductVersion "1.%s"' % BIOS_VER_TEMPLATE:
                                           'MyAppVersionInfoProductVersion "0.%s"' % g.bios_ver})
         
        fun[n].condition = g.golden
        return fun[n]

    if n == 'svnlog_download':
        gather(g.working_path_abs,0,0,g.working_path_abs)
        print('g.workspace = %s' % g.workspace)
        fun[n] = ActionSvn(sub_function=ActionSvn.svn_log,command = '%s log'% os.path.join(g.tool_path_abs,
                           r'Tool\ToolCommon\SVNServer\svn'),project_path = g.workspace)
        return fun[n]

    if n == 'svnlog_extraction':
        BiosVerNum = '{:0>2s}'.format(str(int(g.bios_ver_full[4:6]) - 1))
        LastBiosVer = g.bios_ver_full[0:4] + BiosVerNum + g.bios_ver_full[6:]

        path =  os.path.join(g.working_path_abs, fun['svnlog_download'].action_output['file'])
        print('path = %s' % path)
        DebugPrint('CheckLogKeyWord:LastBiosVer=' + LastBiosVer)
        fun[n] = ActionSvn(sub_function=ActionSvn.txt_cut_out,input_file =path ,mode = 'r',
                           encoding= None,string_map = {'1':'release','2':LastBiosVer})
        return fun[n]

    if n == 'txt_modify_rtf':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                           file=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                             r'Web_Release\LenovoWarning_en.rtf'),
                           string_map={MARKET_NAME_TEMPLATE: project_config['marketingname']})
        fun[n].condition = g.golden
        return fun[n]

    if n == 'checksum_ini_modify':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_modify,
                  file=os.path.join(fun['final_pack'].action_output['packed_folder'], r'Checksum.ini'),
                  string_map={'Crisis': 'Crisis\\' + g.crisis_zip_name},
                  match_count = 1)
        return fun[n]

    if n == 'generate_checksum':
        para = r'> %s' % \
        os.path.join(r'checksum.txt')

        fun[n] = ActionTool(sub_function=ActionTool.tool_specific_place,
                            tool_name='CheckSumTools',
                            specific_place =os.path.join(g.working_path_abs,fun['final_pack'].action_output['packed_folder']),
                            tool_para=para,
                            output_file_name='checksum')
        return fun[n]

    if n == 'crisis_7z':
        print('crisiz----------------------')
        print(os.path.join(g.working_path_abs, fun['final_pack'].action_output['packed_folder']))
        print(fun['final_pack'].action_output['packed_folder'].split('\\')[-1])
        para = r'a $output$\%s.zip %s' % \
               (g.crisis_zip_name, #dir file name
                os.path.join(g.working_path_abs,fun['final_pack'].action_output['packed_folder'],'Crisis',g.crisis_zip_name))  #source
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='%s.zip' % g.crisis_zip_name)  # output_file_name is at the document of 'crisis_7z'
        return fun[n]

    if n == 'crisis_override':
        fun[n] = ActionPack(layout=[
                                   [ActionPack.pack_delete_folder,
                                    os.path.join(g.working_path_abs,fun['final_pack'].action_output['packed_folder'],'Crisis',g.crisis_zip_name),
                                    '',
                                    g.pack_crisis],
                                    [ActionPack.pack_copy_file,
                                    os.path.join(g.working_path_abs,fun['crisis_7z'].output_folder, g.crisis_zip_name +'.zip'),
                                     os.path.join(g.working_path_abs, fun['final_pack'].action_output['packed_folder'],'Crisis',g.crisis_zip_name +'.zip'),
                                    g.pack_crisis]
                                    ])
        return fun[n]

    if n == 'txt_modify_ReleaseNote1':

        if int(g.bios_ver) !=0:
            bios_previou_ver = g.bios_ver_full[:4] + str(int(g.bios_ver)-1) + g.bios_ver_full[-2:]
            ec_previou_ver = g.ec_ver_full[:4] + str(int(g.bios_ver)-1) + g.ec_ver_full[-2:]
        else:
            bios_previou_ver = g.bios_ver_full
            ec_previou_ver = g.ec_ver_full

        fun[n] = ActionTxt(sub_function=ActionTxt.txt_mix,
                           layout=[
                           {'sub_function':ActionTxt.txt_modify_line,
                            'modify_count' : 1,
                           'file':os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReleaseNote.txt'),
                           'insert_before' :False,
                           'string_map':{'Release Date          :':g.release_date,
                                       'System BIOS Release Notification ': project_config['marketingname'],
                                       'BIOS/EC Version    :':r'%s/%s' %(g.bios_ver_full,g.ec_ver_full),
                                       'Base On                 :':r'%s/%s' %(bios_previou_ver,ec_previou_ver)}
                            },
                               {'sub_function': ActionTxt.txt_modify_line,
                                'modify_count': 1,
                                'file': os.path.join(fun['final_pack'].action_output['packed_folder'],
                                                     r'ReleaseNote.txt'),
                                'insert_before': True,
                                'string_map': {
                                               'System BIOS Release Notification': project_config['marketingname'],
                                               }
                                },
                           ])
        return fun[n]

    if n == 'txt_grab_checksum':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_mix,
                           layout =[
                               {'sub_function': ActionTxt.txt_override_line,
                                'file': os.path.join(fun['final_pack'].action_output['packed_folder'], r'result.txt'),
                                'line_exclude': r'.exe|.bin',
                                'override_line':'',
                                'override_count': 0,
                                },
                           ]) 
        return fun[n]

    if n == 'txt_modify_ReleaseNote2':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_mix,
                           layout =[
                               {'sub_function': ActionTxt.txt_override_line,
                                'override_count': 7,
                                'file': os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReleaseNote.txt'),
                                'line_include': r'Checksum:',
                                'override_line':'',
                                },
                               {'sub_function': ActionTxt.txt_insert,
                                'input_file':os.path.join(g.working_path_abs,fun['final_pack'].action_output['packed_folder'],'result.txt'),
                                'dst_file':os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReleaseNote.txt'),
                                'start_str':'BIOS Checksum     :\n',
                                'insert_count':1,
                                },
                           ])
        return fun[n]
        
    if n == 'save_note_from_release_note':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_mix,
                           layout=[
                               {'sub_function': ActionTxt.txt_grab,
                                'file': os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReleaseNote.txt'),
                                'start_str': 'Flash Note           :',
                                'start_str_include': 0,
                                'end_str': 'BIOS Notification    :',
                                'end_str_include': 0,
                                'output_file': r'note.txt'},

                           ])
        return fun[n]

    if n == 'transfer_note_to_html':
        '''
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_read,

                            file=os.path.join(fun['save_note_from_release_note'].output_folder, r'note.txt'),

                           )
        '''
        fun[n] = ActionHtml(sub_function=ActionHtml.txt_tansfer_to_html,
                            src=os.path.join(fun['save_note_from_release_note'].output_folder, r'note.txt'),
                            output_data=1,
                            )
        return fun[n]

    if n == 'txt_grab_log':
        fun[n] = ActionTxt(sub_function=ActionTxt.txt_grab,
                           file=os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReleaseNote.txt'),
                           start_str='BIOS Notification    :',
                           start_str_include = len('BIOS Notification    :'),
                           end_str= 'How To Flash         :',
                           end_str_include = 0,
                           output_file='txt_grab_log.txt')
        return fun[n]

    if n == 'txt_modify_readme':
        fun[n] = ActionTxt(sub_function = ActionTxt.txt_mix,
                           layout = [
                            {'sub_function':ActionTxt.txt_modify,
                           'file':os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReadMe.txt'),
                           'string_map':{BIOS_VER_FULL_TEMPLATE: g.bios_ver_full,
                                       MARKET_NAME_TEMPLATE: project_config['marketingname'],
                                       RELEASE_DATE_TEMPLATE: g.release_date}},
                            {'sub_function':ActionTxt.txt_insert,
                           'input_file':fun['txt_grab_log'].action_output['txt_grab_log'],
                           'dst_file':os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReadMe.txt'),
                           'start_str':'General Information:\n',
                           'insert_count':1},
                            {'sub_function':ActionTxt.txt_insert,
                           'input_data':'\n'+ g.bios_ver_full +':'+'\n',
                           'dst_file':os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReadMe.txt'),
                           'start_str':'General Information:\n',
                           'insert_count':1},
                           ])
        return fun[n]

    if n == 'docx_modify_readme':
        fun[n] = ActionDocx(sub_function=ActionDocx.docx_modify,
                            file=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                              r'Windows\ReadMe.docx'),
                            string_map={BIOS_VER_FULL_TEMPLATE: g.bios_ver_full,
                                        MARKET_NAME_TEMPLATE: project_config['marketingname'],
                                        RELEASE_DATE_TEMPLATE: g.release_date})
        fun[n].condition = g.pack_web_release
        return fun[n]

    if n == 'docx_insert_readme_log':
        fun[n] = ActionDocx(sub_function=ActionDocx.docx_insert,
                            input_file=fun['txt_grab_log'].action_output['txt_grab_log'],
                            dst_file=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                                  r'Windows\ReadMe.docx'),
                            start_str='General Information:',
                            insert_count=1)
        return fun[n]

    if n == 'docx_insert_bios_version':
        fun[n] = ActionDocx(sub_function=ActionDocx.docx_insert,
                            input_data='\n'+ g.bios_ver_full +':'+'\n',
                           dst_file=os.path.join(fun['final_pack'].action_output['packed_folder'],
                                                 r'Windows\ReadMe.docx'),
                           start_str='General Information:',
                           insert_count=1)
        return fun[n]


    if n == 'clean_up_pack':
        path = fun['final_pack'].action_output['packed_folder']
        fun[n] = ActionPack(layout = [
                                [ActionPack.pack_delete_files,os.path.join(path,r'result.txt'),'None',True],
                                [ActionPack.pack_delete_files,os.path.join(path,r'Checksum.ini'),'None',True],
                                [ActionPack.pack_delete_files,os.path.join(path,r'LfcChecksumV103.exe'),'None',True],
                                [ActionPack.pack_delete_files,os.path.join(path,r'tool_cmd.txt'),'None',True],
                                #save the current modified for next build.
                                [ActionPack.pack_copy_file,os.path.join(path,r'ReleaseNote.txt'),os.path.join(g.working_path_abs,'LfcBprInput',r'LfcReleaseNote_%s.txt' %g.build_id),True],])
        return fun[n]

    if n == 'final_7z':
        para = r'a $output$\%s.7z %s' % \
               (fun['final_pack'].action_output['packed_folder'].split('\\')[-1],
                os.path.join(g.working_path_abs, fun['final_pack'].action_output['packed_folder']))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='%s.7z' % fun['final_pack'].action_output['packed_folder'].split('\\')[-1])
        return fun[n]

    if n == 'final_zip':
        para = r'a $output$\%s.zip %s' % \
               (fun['final_pack'].action_output['packed_folder'].split('\\')[-1],
                os.path.join(g.working_path_abs, fun['final_pack'].action_output['packed_folder']))
        fun[n] = ActionTool(sub_function=ActionTool.tool_run_in_place,
                            tool_name='7-Zip',
                            tool_para=para,
                            output_file_name='%s.zip'
                                             % fun['final_pack'].action_output['packed_folder'].split('\\')[-1])
        return fun[n]

    if n == 'ftp_upload':
        fun[n] = ActionFtp(sub_function=ActionFtp.ftp_upload_file,
                           ftp_ip='10.159.252.97', ftp_port=21, ftp_account='COMMON', ftp_password='Aa@12345',
                           ftp_file=os.path.join(project_config['ftppath'],
                                                 fun['final_zip'].action_output['file'].split('\\')[-1]),
                           local_file=fun['final_zip'].action_output['file'])
        fun[n].condition = g.upload_ftp
        return fun[n]

    if n == 'extract_bios_ec_log':

        fun[n] = ActionTxt (sub_function = ActionTxt.txt_mix,
                           layout = [
                            {'sub_function':ActionTxt.txt_grab,
                             'file':os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReadMe.txt'),
                             'start_str':'BIOS Notification    :',
                             'start_str_include':0,
                             'end_str':'EC Notification      :',
                             'end_str_include':0,
                             'output_file':r'bioslog.txt'},
                            {'sub_function': ActionTxt.txt_grab,
                             'file': os.path.join(fun['final_pack'].action_output['packed_folder'], r'ReadMe.txt'),
                             'start_str': 'EC Notification      :',
                             'start_str_include': 0,
                             'end_str': 'Trademarks',
                             'end_str_include': 0,
                             'output_file':r'eclog.txt'},
                           ])
        return fun[n]

    if n == 'get_email_draft':
        fun[n] =  ActionFile(sub_function = ActionFile.file_folder_copy,
                             src = os.path.join(g.tool_path_abs, r'Tool\TemplateLnb\ReleaseEmail.msg'),
                             dst =  os.path.join(g.output_path, r'ReleaseEmail.msg')
                             )

        return fun[n]

    if n == 'transfer_bios_log_to_html':
        fun[n] =  ActionHtml(sub_function = ActionHtml.txt_tansfer_to_html,
                             src = fun['extract_bios_ec_log'].action_output['bioslog'],
                             output_data = 1,
                             )
        return fun[n]

    if n == 'transfer_ec_log_to_html':
        fun[n] =  ActionHtml(sub_function = ActionHtml.txt_tansfer_to_html,
                             src = fun['extract_bios_ec_log'].action_output['eclog'],
                             output_data = 1,
                             )
        return fun[n]

    if n == 'prepare_email_content':
        print('g.ec_ver_full=%s' %g.ec_ver_full)
        note_content = fun['transfer_note_to_html'].output_data
        send_para = {
            'type': 'Html',
            'src':os.path.join(g.working_path_abs,g.output_path, r'ReleaseEmail.msg'),
            'items':{
                'Subject':'['+project_config['projectname'] +']'+ 'SW BIOS/EC Formal Release for '+ g.bios_ver_full + '\\' + g.ec_ver_full,
                'To':project_config['projectmailgroup'],
                'CC':project_config['cc']
            },
            'replace' :{
                'tofillin':'<br>'+ note_content if note_content !='N/A<br>' else '' ,
                'Note:': 'Note:' if note_content != 'N/A<br>' else '',
                'LcfcProjectName': project_config['projectname'],
                'LcfcPhase': project_config['phase'],
                'GoldenStatus':'Yes' if project_config['golden'] else 'No',
                'LcfcReleaseDate':g.release_date,
                'ProjectMailGroup':project_config['projectmailgroup'],
                'ServerSiteInSW':r'ftp://10.159.252.97/'+ project_config['ftppath'],
                'SupportedOs':project_config['supportedos'],
                'SenderName':project_config['sender'],
                'BIOSECVersion': g.bios_ver_full + '\\' + g.ec_ver_full,
                'BIOSVersion':g.bios_ver_full,
                'ECVersion':g.ec_ver_full,
                '1)BIOS Notification':fun['transfer_bios_log_to_html'].output_data,
                '1)EC Notification':fun['transfer_ec_log_to_html'].output_data,
            }
        }
        fun[n] = ActionEmail(sub_function=ActionEmail.email_modify,
                             para_dict=send_para
                            )
        return fun[n]

    if n == 'send_email_formal_release':
        send_para = {
            'src':os.path.join(g.working_path_abs,g.output_path, r'ReleaseEmail.msg')
        }
        fun[n] = ActionEmail(sub_function=ActionEmail.email_exist_send,
                             para_dict=send_para
                            )
        fun[n].condition = g.send_release_mail
        return fun[n]

    return None


def get_action(n):
    global fun
    DebugPrint('get_action n = '+ str(n))
    
    if fun.get(n):
        DebugPrint('found %s in fun.get ' % n)
        DebugPrint('fun.get(n) = ' + str(fun.get(n)))
        return fun.get(n)
    #get_action n = build
    #fun_list = [<Lib.action_build.ActionBuild object at 0x000002BA5799AB80>, None, None, None, None]

    # get_action n = pack_lnv_sign
    #fun_list = [None, None, None, None, < Lib.action_pack.ActionPack object at 0x0000021447296040 >]

    #get_action n = txt_modify_cfg
    #fun_list = [None, None, None, None, < Lib.action_txt.ActionTxt object at 0x0000021447296D30 >]

    #fun_list at any time,only has one member.
    fun_list = [
        get_action_common(n),
        get_action_insyde_special(n),
        get_action_phoenix_special(n),
        get_action_final_pack(n),
        get_action_lenovo_sign(n),
    ]

    
    DebugPrint('fun_list = '+ str(fun_list))
    for _i in fun_list:
        if _i:
            return _i


def initial_output_folder(_argv, initial=0):
    folder_name = 'LfcBprOutput'
    if initial == 0:
        now = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + '-' + \
              (g.bios_ver_full if g.formal_release else g.bios_ver_full_test)
        folder = os.path.join(folder_name, now)
        os.makedirs(folder)
        g.output_path = folder
        set_var_to_disk('current_cmd', _argv)
    elif initial == 1:
        if not os.path.exists(folder_name) or os.listdir(folder_name) == []:
            error_handle(0, "Can't found previous data.")
            return None
        previous_folder = os.path.join(folder_name, sorted(os.listdir(folder_name))[-1])
        g.output_path = previous_folder
        _argv = get_var_from_disk('current_cmd')
        print ("arg data jason = %s" %_argv)
        if not _argv:
            error_handle(0, "Previous data format wrong.")
            return None
        print('Found previous data: %s' % previous_folder)
    return _argv


def global_run(_action_list):
    if not _action_list:
        error_handle(0, 'Not a correct action list, please check it')
        return STATUS_ERROR
    print('------------------------------------------------------------')
    #_action_list = (lenovo_sign) + action + (formal_pack)/(test pack)
    #flat_action_list = _action_list ? on meaning
    flat_action_list = _action_list if len(_action_list) == 1 else \
        reduce(lambda x, y: (x if isinstance(x, list) else [x]) + (y if isinstance(y, list) else [y]), _action_list)

    DebugPrint('flat_action_list = '+ str(flat_action_list))
    for _count, _action_str in enumerate(flat_action_list):
        _action = get_action(_action_str)
        if get_var_from_disk('%02d_%s_result' % (_count, _action_str)) == STATUS_SUCCESS:
            # Executed and pass, should be previous result, so skip
            print('[Action %02d] %s executed, result success, so skipped' % (_count, _action_str))
            _action.get_saved_output(_count, _action_str)
            continue
        if not _action:
            error_handle(0, '[Action %02d] %s not exist' % (_count, _action_str))
            return STATUS_ERROR
        if not _action.condition:
            print('[Action %02d] %s condition not meet, skipped...' % (_count, _action_str))
            continue
        print('[Action %02d] %s executing...' % (_count, _action_str))
        _action.status = STATUS_ONGOING
        _action.pre_work(_count, _action_str)
        _action.action(_count, _action_str)
        _action.post_work(_count, _action_str)
        if _action.status == STATUS_SUCCESS:
            print('[Action %02d] %s success' % (_count, _action_str))
        elif _action.status == STATUS_PRE_WORK_ERROR:
            error_handle(0, '[Action %02d] %s pre-work failed' % (_count, _action_str))
            return STATUS_ERROR
        else:
            error_handle(0, '[Action %02d] %s failed' % (_count, _action_str))
            return STATUS_ERROR
    return STATUS_SUCCESS
'''
entry_1: Build ID
entry_2: BIOS version
entry_3: Project Folder
v_1: Sign Type: Mocca Sign or Lenovo Sign 
v_2: Release Type: Test or Formal Release
'''
def start_build(entry_1, entry_2, entry_3, v_1, v_2,v_3):
    argv = {'BuildId': entry_1.get(), 'BiosVer': entry_2.get(),
            'm': True if v_1.get() == 0 else False, 'l': True if v_1.get() == 1 else False,
            't': True if v_2.get() == 0 else False, 'f': True if v_2.get() == 1 else False,
            'c': True if v_3.get() == 1 else False, 'p': entry_3.get()}
    if not argv['BuildId']:
        error_handle(0, 'Build ID is a MUST')
        return

    t = threading.Thread(target=do_things, args=(argv,))
    t.setDaemon(True)
    t.start()
    g.gui_b_1['state'] = tk.DISABLED
    g.gui_b_3['state'] = tk.DISABLED


def b_4_process(entry_3_text, l_1):
    f = tk.filedialog.askdirectory()
    if f != '':
        entry_3_text.set(f)
        l_1.insert('0', f)
        if l_1.size() > 5:
            l_1.delete(tk.END)



def launch_gui():
    win = tk.Tk()
    win.title('LfcBpr')
    win.rowconfigure(1, weight=1)
    win.columnconfigure(1, weight=1)

    with open('tmp.png', 'wb') as tmp:
        tmp.write(lcfc_get_raw_file(5))
    win.iconphoto(True, tk.PhotoImage(file='tmp.png'))
    os.remove('tmp.png')
    #entry_1 Build ID
    f_1 = tk.LabelFrame(win, text="Setting")
    f_1.rowconfigure(4, pad=5)
    f_1.columnconfigure(2, pad=10)
    tk.Label(f_1, text='Build ID').grid(row=0, column=0, sticky=tk.NW)
    entry_1 = tk.Entry(f_1, width=30)
    entry_1.grid(row=0, column=1, columnspan=2, sticky=tk.NW)
    tk.Label(f_1, text='BIOS Version').grid(row=1, column=0, sticky=tk.NW)

    #entry_2 BIOS Version
    entry_2 = tk.Entry(f_1, width=30)
    entry_2.grid(row=1, column=1, columnspan=2, sticky=tk.NW)
    tk.Label(f_1, text='Sign Type').grid(row=2, column=0, sticky=tk.NW)

    # v_1 Sign Type checkbox
    sign_type_buttons = LfcRadiobuttonGroup(
                        window = tk,
                        container = f_1,
                        data_layout=[{'text':'Mocca Sign','value':0,'row':2,'column':1,'associated':None},
                                            {'text':'LENOVO Sign','value':1,'row':2,'column':2,'associated':None},
                                           ],
                        group_label={'text':'Sign Type','row':2,'column':0}
                                            )
    # v_3 clean Type checkbox
    clean_type_buttons = LfcRadiobuttonGroup(
                        window = tk,
                        container = f_1,
                        data_layout=[{'text':'Not Clean','value':0,'row':4,'column':1,'associated':None},
                                            {'text':'Clean','value':1,'row':4,'column':2,'associated':None},
                                           ],
                        group_label={'text':'Clean Type','row':4,'column':0}
                                            )
    # v_2 Release Type checkbox
    release_type_buttons = LfcRadiobuttonGroup(
                        window = tk,
                        container = f_1,
                        data_layout=[{'text':'Test Release','value':0,'row':3,'column':1,'associated':{'button_group':clean_type_buttons,'value':None,'state':['normal','normal']}},
                                    {'text':'Formal Release','value':1,'row':3,'column':2,'associated':{'button_group':clean_type_buttons,'value':1,'state':['disable','disable']}},
                                           ],
                        group_label={'text':'Release Type','row':3,'column':0}
                                            )

    #Build Button
    b_1 = tk.Button(f_1, width=6, text='Build',
                    command=lambda: start_build(entry_1, entry_2, entry_3,sign_type_buttons.v, release_type_buttons.v,clean_type_buttons.v))
    b_1.grid(row=5, column=0, sticky=tk.NW)

    #Help Button pop-window
    _help = '\n\n1. Build ID is a must.\n' \
            '2. BIOS Version is optional, if not input, ' \
            'will search the version in BIOS code, if input, will use the input value to override BIOS code.\n' \
            '3. Project Folder is optional, if not input, will use the tool folder as project folder.\n\n' \
            '%s    %s    by RD\\dahai.zhou&feng.gu\n' % (tool_version, tool_release_date)
    b_2 = tk.Button(f_1, width=6, text="Help",
                    command=lambda: tk.messagebox.showinfo('', help_str.replace('\n', ' ') + _help))
    b_2.grid(row=5, column=1, sticky=tk.NE)

    #Open Button
    b_3 = tk.Button(f_1, width=6, text="Open", state=tk.DISABLED,
                    command=lambda: subprocess.Popen('explorer %s' % g.output_path))
    b_3.grid(row=5, column=2, sticky=tk.NE)
    f_1.grid(row=0, column=0, padx=5, pady=5, sticky=tk.NW)

    #Project Folder
    f_2 = tk.LabelFrame(win, text="Project Folder")
    f_2.rowconfigure(0, weight=1)
    f_2.columnconfigure(0, weight=1)
    entry_3_text = tk.StringVar()
    entry_3_text.set('')
    entry_3 = tk.Entry(f_2, width=55, textvariable=entry_3_text)
    entry_3.grid(row=0, column=0, sticky=tk.EW + tk.N)
    b_4 = tk.Button(f_2, width=6, text="Browse", command=lambda: b_4_process(entry_3_text, l_1))
    b_4.grid(row=0, column=1, sticky=tk.NE)
    tk.Label(f_2, text='History').grid(row=1, column=0, sticky=tk.NW)
    l_1 = Listbox(f_2, height='5', exportselection=False)
    l_1.grid(row=2, column=0, columnspan=2, sticky=tk.EW + tk.N)
    l_1.bind("<<ListboxSelect>>", lambda e: entry_3_text.set(l_1.get(l_1.curselection()
                                                                     if l_1.curselection() else (0,))))
    f_2.grid(row=0, column=1, padx=5, pady=5, sticky=tk.NSEW)

    f_3 = tk.LabelFrame(win, text="Build Message")
    f_3.rowconfigure(0, weight=1)
    f_3.columnconfigure(0, weight=1)
    t_2 = ScrolledText(f_3, height=40, state='disabled')
    t_2.grid(row=0, column=0, sticky=tk.NSEW)
    f_3.grid(row=1, column=0, columnspan=2, padx=5, pady=5, sticky=tk.NSEW)

    if get_var_from_disk('BuildId', 1) is not None:
        entry_1.insert(0, get_var_from_disk('BuildId', 1))
    if get_var_from_disk('BiosVer', 1) is not None:
        entry_2.insert(0, get_var_from_disk('BiosVer', 1))
    _tmp = 1 if get_var_from_disk('l', 1) else 0
    sign_type_buttons.v.set(_tmp)
    _tmp = 1 if get_var_from_disk('f', 1) else 0
    release_type_buttons.v.set(_tmp)
    #initial 'clean type radiobutton value'
    _tmp = 1 if get_var_from_disk('c', 1) else 0
    clean_type_buttons.v.set(_tmp)
    #in 'clean type is set as clean before,should prey buttons'
    if clean_type_buttons.v.get() == 1 and release_type_buttons.v.get() == 1 :
        for i in clean_type_buttons.lfc_radiobutton_group:
            i.button['state'] = 'disable'

    if get_var_from_disk('p', 1) is not None:
        entry_3.insert(0, get_var_from_disk('p', 1))
    for _i in range(5):
        l_1.insert(tk.END, get_var_from_disk('p%d' % _i, 1))

    g.gui_t_2 = t_2
    g.gui_b_1 = b_1
    g.gui_b_3 = b_3
    g.gui = True
    sys.stdout = OutputTkinter(g.gui_t_2)
    sys.stderr = OutputTkinter(g.gui_t_2)

    win.protocol("WM_DELETE_WINDOW", lambda: quit_win(win, entry_1, entry_2, entry_3, sign_type_buttons.v, release_type_buttons.v,clean_type_buttons.v, l_1))
    win.withdraw()
    win.deiconify()
    win.mainloop()


def quit_win(win, entry_1, entry_2, entry_3, v_1, v_2,v_3, l_1):
    if g.build_proc:
        g.build_proc.send_signal(signal.CTRL_C_EVENT)
        g.build_proc.send_signal(signal.CTRL_C_EVENT)
        g.build_proc.send_signal(signal.CTRL_C_EVENT)

    argv = {'BuildId': entry_1.get(), 'BiosVer': entry_2.get(),
            'm': True if v_1.get() == 0 else False, 'l': True if v_1.get() == 1 else False,
            't': True if v_2.get() == 0 else False, 'f': True if v_2.get() == 1 else False,
            'c': True if v_3.get() == 1 else False,
            'p': entry_3.get()}
    set_var_to_disk(None, None, argv, 1)
    for _i in range(l_1.size()):
        set_var_to_disk('p%d' % _i, l_1.get(_i), None, 1)
    win.destroy()


help_str = '''\
Build ID=XX: build normal BIOS, XX1: COM port debug BIOS, XX2: DDT BIOS (Insyde only),
XX3: DDT with debug log BIOS (Insyde only), XX4: POST time performance BIOS (Insyde only),
XX5: build specific driver (TBD), XXC: build clean, A: continue your previous build again,
normally if your previous build failed, you can try this to save time
'''


def read_argv():
    parser = argparse.ArgumentParser(prefix_chars='/', add_help=False, description=help_str,
                                     usage='%(prog)s buildid biosver [/m or /l] [/t or /f] [c] /p "path", '
                                           'or %(prog)s /? for help')
    parser.add_argument('BuildId', metavar='buildid', nargs='?', type=str, help='The build ID to build')
    parser.add_argument('BiosVer', metavar='biosver', nargs='?', type=str, help='The BIOS version to use')
    parser.add_argument('/?', action='help', help='Show help')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('/m', action='store_true', help='Mocca/local sign (default)')
    group.add_argument('/l', action='store_true', help='LENOVO/server sign)')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('/t', action='store_true', help='Test build and release (default)')
    group.add_argument('/f', action='store_true', help='Formal build and release')
    parser.add_argument('/p', action='store', help='Indicate the project folder to build')
    parser.add_argument('/c', action='store_true', help='Indicate need to clean or not,formal relase will clean automatically whatever has /c')
    args = parser.parse_args()
    if len(sys.argv) == 1:
        return 1
    return vars(args)


def read_config():
    global support_ini_version

    config = configparser.ConfigParser()
    config.read(os.path.join(g.working_path_abs,'LfcBprInput','ProjectConfig.ini'))
    config_dict = {}
    for section in config.sections():
        config_dict = dict(config.items(section))
        if section == 'Common' and config_dict.get('iniversion') not in support_ini_version:
            return None
        if config_dict.get('buildid') == g.build_id:
            if config_dict.get('basedon') == '':
                return config_dict
            else:
                break
    if config_dict.get('buildid') == g.build_id:
        for section in config.sections():
            config_dict_new = dict(config.items(section))
            if config_dict.get('basedon') == config_dict_new.get('buildid'):
                for key, value in config_dict.items():
                    config_dict_new[key] = value
                DebugPrint("cofig_dict_new = %s" %config_dict_new)
                return config_dict_new
    return None
'''
show global class g data:
    Build ID             : GL
    BIOS version         : GLCN07WW
    IBV                  : Phoenix
    Build type           : PhMake.bat
    Sign type            : LENOVO sign (server sign)
    Release type         : Formal release
    Pack crisis password : Yes
Build psw crisis     : Yes
Pack web release     : Yes
Working folder       : D:\Projects\S360V1415T\Phoenix\Projects\CeladonLcn\000
'''
def print_policy():
    print('Build ID             :', g.build_id)
    print('BIOS version         :',
          g.bios_ver_full if g.formal_release else g.bios_ver_full_test)
    print('IBV                  :', project_config['ibv'])
    print('Build type           :',
          ActionBuild.cmd_list_insyde.get(g.build_type) if g.insyde else ActionBuild.cmd_list_phoenix.get(g.build_type))
    print('Sign type            :',
          'LENOVO sign (server sign)' if g.lenovo_sign else 'Mocca sign (local sign)')
    if g.formal_release:
        print('Release type         : Formal release')
        print('Pack crisis password :', 'Yes' if g.pack_crisis else 'No')
        print('Build psw crisis     :', 'Yes' if g.build_psw_crisis else 'No')
        print('Pack web release     :', 'Yes' if g.pack_web_release else 'No')
    else:
        print('Release type         : Test release')
    print('Working folder       :', g.working_path_abs)


def do_things(argv):
    global project_config
    global fun
    global support_ini_version

    DebugPrint (argv)
    
    fun = {}

    #initilize class g

    g.argv = argv
    if argv['p']:
        os.chdir(argv['p'])
    else:
        os.chdir(g.tool_path_abs)
    g.working_path_abs = os.getcwd()

    g.build_id = argv['BuildId'][:2].upper()
    build_previous = False
    if g.build_id == 'A':
        argv = initial_output_folder(argv, 1)
        if not argv:
            if g.gui:
                g.gui_b_1['state'] = tk.NORMAL
            return STATUS_ERROR
        g.build_id = argv['BuildId'][:2].upper()
        build_previous = True

    g.project_config = project_config = read_config()
    if project_config is None:
        error_handle(0, 'Read ProjectConfig.ini error, possible reasons:\n'
                        '1. Cannot find the config file, maybe you forget to set the Project Folder\n'
                        '2. Cannot find the Build ID in config file, please check the config file\n'
                        '3. Config file version not supported, this tool only support version '
                     + ' '.join(i for i in support_ini_version) + '\n')
        if g.gui:
            g.gui_b_1['state'] = tk.NORMAL
        return STATUS_ERROR
    # set build flag,
    g.insyde = ('Insyde' == project_config['ibv'])
    g.phoenix = ('Phoenix' == project_config['ibv'])
    g.lenovo_sign = (argv['l'] is True)
    g.local_sign = not (g.lenovo_sign is True)
    g.formal_release = (argv['f'] is True)
    g.test_release = not (g.formal_release is True)
    g.crisis_ver_full = (r'%sCN' % g.build_id)
    g.release_date = datetime.date.today().strftime("%Y-%m-%d")
    g.upload_ftp = (project_config['ftppath'] != '')
    g.pack_crisis = (project_config['packcrisis'] == '1') if g.formal_release else False
    g.build_psw_crisis = (project_config['buildpswcrisis'] == '1') if g.formal_release else False
    g.pack_web_release = (project_config['packwebrelease'] == '1') if g.formal_release else False
    g.golden = (project_config['golden'] == '1') if g.formal_release else False
    g.send_release_mail = (project_config['projectmailgroup'] != '')

    g.build_type = argv['BuildId'][2:].upper()

    g.b_buildid_bat = (True) if os.path.exists('b_%s.bat'%g.build_id) else False
    
    #update tool info according ProjectConfig.ini new tools
    need_update_tool_info =['localsignbiostool','localsigncrisistool','packwintool','flashtool','shellflashtool']
    for tool in need_update_tool_info:
        if project_config.get(tool):
            insert_tool_info(project_config[tool], ActionTool.tool_info, ActionTool.tool_common_info)
    DebugPrint('ActionTool.tool_info = %s' %ActionTool.tool_info)

    #gui choose clean radiobutton or 'b.bat /c'

    print('argv = %s'%argv)
    if argv.get('t'):
        g.clean = argv['c']
    elif argv.get('f'):
        g.clean = True

    #If gui buildid is XXC or command is "b c "
    if g.build_type == ActionBuild.BUILD_TYPE_CLEAN:
        print('Build Clean:')
        initial_output_folder(argv, 0)
        g.clean = True
        global_run(['clean'])
        print('BUILD PASS!')
        if g.gui:
            g.gui_b_1['state'] = tk.NORMAL
        return

    if not argv['BiosVer'] or len(argv['BiosVer']) == 1 or not argv['BiosVer'][:2].isdigit():
        # No BIOS ver input, get from code
        temp = process_bios_ver(fun=1)
        if not temp:
            error_handle(0, 'Cannot find BIOS version in project folder, maybe you should set the Project Folder?')
            if g.gui:
                g.gui_b_1['state'] = tk.NORMAL
            return STATUS_ERROR
        argv['BiosVer'] = temp[4:6] + (argv['BiosVer'] if argv['BiosVer'] else '')
    g.bios_ver = argv['BiosVer'][:2]
    g.bios_ver_full = r'%sCN%sWW' % (g.build_id, g.bios_ver)
    g.bios_ver_full_test = r'%sCN%sWW%s' % (g.build_id, g.bios_ver, argv['BiosVer'][2:])

    g.crisis_zip_name = (r'%s_crisisbios' % g.bios_ver_full).lower()
    if not build_previous:
        initial_output_folder(argv, 0)

    #show class g data:

    print_policy()

    action_list = []
    lenovo_sign = [
        'pre_build','clean','BootGuardLSign_bat','build_psw_crisis','hook_bat','clean','build', 'pack_lnv_sign', 'txt_modify_cfg',
        'tool_7z_compress','prepare_time_stamp','get_time_stamp', 'send_email_to_sign',
        'wait_for_sign_back', 'tool_7z_uncompress1', 'tool_7z_uncompress2']

    formal_pack = ['final_pack', 'checksum_ini_modify','generate_checksum','txt_grab_checksum','txt_modify_nsh', 'file_rename_iss', 'txt_modify_iss',
                   'crisis_7z','crisis_override','txt_modify_ReleaseNote1','txt_modify_ReleaseNote2','txt_grab_log','txt_modify_readme',
                   'docx_modify_readme', 'docx_insert_readme_log','docx_insert_bios_version',
                   'txt_modify_rtf','clean_up_pack', 'final_zip', 'extract_bios_ec_log','save_note_from_release_note','transfer_note_to_html','get_email_draft','transfer_bios_log_to_html',
                   'transfer_ec_log_to_html','prepare_email_content','send_email_formal_release', 'ftp_upload',]

    test_pack = ['final_pack', 'txt_modify_nsh_test_release', 'final_7z']

    if DEBUG_MODE and (not DEBUG_MAIL):
        lenovo_sign = lenovo_sign + ['change_uncompress2_package_files_name']

    if g.formal_release and g.local_sign:
        if g.phoenix:
            action_list = [
                              'pre_build','clean', 'build_psw_crisis','clean','build', 'tool_local_sign_phoenix',
                              'tool_local_sign_psw_crisis_phoenix', 'file_copy_bios_cap_local_sign_phoenix',
                              ['copy_rel_rsp', 'tool_pack_exe_ia32_rel_phoenix',
                               'copy_mfg_rsp', 'tool_pack_exe_ia32_mfg_phoenix',
                               'copy_mfg_rsp', 'tool_pack_exe_x64_mfg_phoenix'],
                          ] + formal_pack
        elif g.insyde:
            action_list = [
                              'pre_build','clean','build_psw_crisis','hook_bat','clean','build',
                              'copy_rel_ini_to_localsignbiostool','tool_local_sign_rel_insyde', 'copy_mfg_ini_to_localsignbiostool',
                              'tool_local_sign_mfg_insyde','tool_local_sign_crisis_insyde', 'tool_local_sign_psw_crisis_insyde',
                              ['copy_rel_32_ini', 'tool_pack_exe_ia32_local_sign_rel_insyde',
                               'copy_mfg_32_ini', 'tool_pack_exe_ia32_local_sign_mfg_insyde',
                               'copy_mfg_64_ini', 'tool_pack_exe_x64_local_sign_mfg_insyde'],
                          ] + formal_pack
    elif g.formal_release and g.lenovo_sign:
        if g.phoenix:
            action_list = lenovo_sign + [
                'file_copy_bios_cap_lnv_sign_phoenix',
                'copy_rel_rsp', 'tool_pack_exe_ia32_rel_phoenix',
                'copy_mfg_rsp', 'tool_pack_exe_ia32_mfg_phoenix',
                'copy_mfg_rsp', 'tool_pack_exe_x64_mfg_phoenix',
            ] + formal_pack
        elif g.insyde:
            action_list = lenovo_sign + [
                'copy_rel_32_ini', 'tool_pack_exe_ia32_lnv_sign_rel_insyde',
                'copy_mfg_32_ini', 'tool_pack_exe_ia32_lnv_sign_mfg_insyde',
                'copy_mfg_64_ini', 'tool_pack_exe_x64_lnv_sign_mfg_insyde',
            ] + formal_pack
    elif g.test_release and g.local_sign:
        if g.phoenix:
            action_list = [
                              'pre_build','clean','build', 'tool_local_sign_phoenix', 'file_copy_bios_cap_local_sign_phoenix',
                              'copy_nac_rsp', 'tool_pack_exe_x64_nac_phoenix',
                          ] + test_pack
        elif g.insyde:
            action_list = [
                              'pre_build','clean','build', 'copy_nac_ini_to_localsignbiostool','tool_local_sign_nac_insyde',
                              'copy_nac_64_ini', 'tool_pack_exe_x64_local_sign_nac_insyde',
                          ] + test_pack
    elif g.test_release and g.lenovo_sign:
        if g.phoenix:
            action_list = lenovo_sign + [
                'file_copy_bios_cap_lnv_sign_test_phoenix',
                'copy_nac_rsp', 'tool_pack_exe_x64_nac_phoenix',
            ] + test_pack
        elif g.insyde:
            action_list = lenovo_sign + [
                'copy_nac_64_ini', 'tool_pack_exe_x64_lnv_sign_nac_test_insyde',
            ] + test_pack
    #start to build
    if global_run(action_list) == STATUS_SUCCESS:
        print('------------------------------------------------------------')
        print('Everything is OK!')
        print('Output package:', fun['final_pack'].action_output['packed_folder'])
        if g.formal_release:
            print('Output zip file:', fun['final_zip'].action_output['file'], '\n')
        elif g.test_release:
            print('Output 7z file:', fun['final_7z'].action_output['file'], '\n')
        print('BUILD PASS!')
    else:
        print('------------------------------------------------------------')
        print('BUILD FAIL!\n')
    if g.gui:
        g.gui_b_1['state'] = tk.NORMAL
        g.gui_b_3['state'] = tk.NORMAL

    # flat_action_list = reduce(lambda x, y: (x if isinstance(x, list) else [x]) + (y if isinstance(y, list) else [y]),
    #                           action_list)
    # for _i, _data_i in enumerate(flat_action_list):
    #     for _j, _data_j in enumerate(flat_action_list[:_i]):
    #         for _k in fun[_data_i].__dict__.values():
    #             for _l in fun[_data_j].action_output.values():
    #                 if _l == _k:
    #                     print('%s-->%s' % (_data_j, _data_i))


# main
# F5 /f /p "F:\Project\TGL\Tag0014\Board\Oem\L05TigerLakeMultiBoardPkg"
# F0 /f /p "F:\Project\ARE\Codebase\Phoenix\Projects\Celadon\000"
if __name__ == '__main__':
    g.tool_path_abs = os.getcwd()
    ret = read_argv()
    if ret == 1:
        launch_gui()
    else:
        do_things(ret)
