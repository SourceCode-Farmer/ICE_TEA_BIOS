echo Off

set CurrentPath=%cd%

REM ---------------------------------------------------------------------------------------------
REM Auto setting of WORKSPACE environment variable
REM ---------------------------------------------------------------------------------------------
pushd \ && set ROOT_DIR=%cd% && popd && pushd .
:SetWorkSpace
if "%cd%" == "%ROOT_DIR%" goto Next
cd ..
if not exist %cd%\BaseTools goto SetWorkSpace
set WORKSPACE=%cd%
:Next
set ROOT_DIR= && popd

REM ---------------------------------------------------------------------------------------------
REM Set CHIPSET_PKG and CHIPSET_REL_PATH environment variables.
REM Chipset package could be placed under %WORKSPACE% or %WORKSPACE%\%CHIPSET_REL_PATH%.
REM ---------------------------------------------------------------------------------------------
set CHIPSET_PKG=AlderLakeChipsetPkg
set CHIPSET_REL_PATH=Intel\AlderLake
if exist %WORKSPACE%\%CHIPSET_PKG%\Tools\Bin\Win32 (
  set CHIPSET_PKG_TOOLS_PATH=%WORKSPACE%\%CHIPSET_PKG%\Tools\Bin\Win32
) else if exist %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32 (
  set CHIPSET_PKG_TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32
) else (
  echo CHIPSET_PKG_TOOLS_PATH is not exist. Please correct CHIPSET_PKG_TOOLS_PATH value.
  echo CHIPSET_PKG_TOOLS_PATH=%CHIPSET_PKG_TOOLS_PATH%
  goto Done
)

cd %CurrentPath%
for /f "tokens=3" %%a in ('find "FileName" BiosGuardSetting.ini') do set FileName=%%a
echo %FileName%
for /f "tokens=3" %%a in ('find "BiosGuardScriptFile " BiosGuardSetting.ini') do set BiosGuardScriptFile=%%a
echo %BiosGuardScriptFile%
for /f "tokens=3" %%a in ('find "PackageMode" BiosGuardSetting.ini') do set PackageMode=%%a
echo PackageMode : %PackageMode%

%CHIPSET_PKG_TOOLS_PATH%\GenBiosGuardHdr.exe -i %CurrentPath%\BiosGuardSetting.ini

if %PackageMode%==0x1 goto Gen_BIOS_BiosGuard
if %PackageMode%==0x2 goto Gen_EC_BiosGuard
if %PackageMode%==0x3 goto Gen_FW_BiosGuard

:Gen_BIOS_BiosGuard
if exist "InsydeBiosGuardHeader.bin" copy /b "%FileName%" + /b "InsydeBiosGuardHeader.bin" "BiosGuard_BIOS.fd"
goto Done

:Gen_EC_BiosGuard
if exist "InsydeBiosGuardHeader.bin" copy /b "%FileName%" + /b "InsydeBiosGuardHeader.bin" "BiosGuard_EC.bin"
goto Done

:Gen_FW_BiosGuard
if exist "InsydeBiosGuardHeader.bin" copy /b "%FileName%" + /b "InsydeBiosGuardHeader.bin" "BiosGuard_FW.bin"
goto Done

:Done
if exist "InsydeBiosGuardHeader.bin" del "InsydeBiosGuardHeader.bin"
echo On