/** @file

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_PROJECT_H_
#define _EFI_L05_PROJECT_H_
//================================================================================
//                         Setup Menu Start
//================================================================================
//_Start_L05_SETUP_MENU_
  //
  //         Boot Device Type,                     Bus, Device, Function, PortNumber (0 base)
  #define EFI_L05_SATA_LOCATION_LIST  \
            {L05_BOOT_TYPE_BBS_FIRST_SATA_HDD,    0x00,   0xFF,     0xFF,      0xFF},   /*    The location of the first HDD     */  \
            {L05_BOOT_TYPE_BBS_SECOND_SATA_HDD,   0x00,   0xFF,     0xFF,      0xFF},   /*    The location of the second HDD    */  \
            {L05_BOOT_TYPE_END_OF_LIST,           0xFF,   0xFF,     0xFF,      0xFF}    /* End of List. Please keep this entry. */

  //
  //         Boot Device Type,                    Name string
  #define EFI_L05_BOOT_DEVICE_NAME_LIST  \
            {L05_BOOT_TYPE_BBS_FIRST_SATA_HDD,    L"Internal HDD: "},   /* The name string of the first HDD     */  \
            {L05_BOOT_TYPE_BBS_SECOND_SATA_HDD,   L"SATA HDD    : "},   /* The name string of the second HDD    */  \
            {BBS_HARDDISK,                        L"SATA HDD    : "},   /* The name string of other unknown HDD */  \
            {BBS_CDROM,                           L"SATA ODD    : "},   /* The name string of the SATA CD-ROM   */  \
            {L05_BOOT_TYPE_BBS_USB_HDD,           L"USB HDD     : "},   /* The name string of the USB HDD       */  \
            {L05_BOOT_TYPE_BBS_USB_CDROM,         L"USB ODD     : "},   /* The name string of the USB CD-ROM    */  \
            {L05_BOOT_TYPE_BBS_USB_FLOPPY,        L"USB FDD     : "},   /* The name string of the USB FLOPPY    */  \
            {BBS_BEV_DEVICE,                      L"Network Boot: "},   /* The name string of the Network boot  */  \
            {L05_BOOT_TYPE_END_OF_LIST,           L""}                  /* End of List. Please keep this entry. */

  #define L05_SCU_RESOLUTION_X          640
  #define L05_SCU_RESOLUTION_Y          480
//_End_L05_SETUP_MENU_
//================================================================================
//                         Setup Menu End
//================================================================================

#endif
