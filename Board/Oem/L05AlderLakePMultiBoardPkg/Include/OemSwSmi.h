/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SMI_TABLE_DEFINE_H_
#define _OEM_SMI_TABLE_DEFINE_H_

#define EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI    0xC2  // EFI_L05_WMI_SETUP_UNDER_OS_CALLBACK
#define EFI_L05_WMI_ASSET_TAG_SW_SMI         0xC5  // EFI_L05_WMI_ASSET_TAG_CALLBACK

typedef enum {
//
// OEM must use 0xc0~0xDF as SW_SMI command number.
//
//_Start_L05_INT15_
  OEM_INT15_CALLBACK = 0xC9,
//_End_L05_INT15_
//_Start_L05_ACPI_SLP_20_
  OEM_SLP20_SAVE_MARKER_CALLBACK = 0x6F,
  OEM_SLP20_RETRIEVE_MARKER_CALLBACK = 0x70,
  OEM_SLP20_VALIDATE_MARKER_CALLBACK = 0x71,
  EFI_L05_UPDATE_SLP20_SET_MANUFACTURE_DONE_CALLBACK = 0x72,
//_End_L05_ACPI_SLP_20_

  OEM_EXAMPLE_SW_SMI  = 0xc0

//_Start_L05_SECURITY_SMI_
  ,
  L05_SECURITY_SW_SMI = 0x20,
//_End_L05_SECURITY_SMI_
//_Start_L05_VARIABLE_SERVICE_
  //
  // 1. Set EFI_L05_VARIABLE_CALLBACK to 0xC1, due to -
  //    (a) The SMI number 0x80 mentioned in Lenovo spec clashes with OS_COMMAND(or PPM_OS_COMMAND) which defined in ChipsetSmiTable.h
  //    (b) Insyde's Rule: OEM must use 0xc0~0xDF as SW_SMI command number.
  // 2. Project may modify this value if needed
  //
//[-start-210624-YUNLEI0105-modify]//
#ifdef LCFC_SUPPORT
  EFI_L05_VARIABLE_CALLBACK = 0x80,
#else
  EFI_L05_VARIABLE_CALLBACK = 0xC1,
#endif
//[-end-210624-YUNLEI0105-modify]//
//_End_L05_VARIABLE_SERVICE_
#if defined (L05_SETUP_UNDER_OS_SUPPORT)
  EFI_L05_WMI_SETUP_UNDER_OS_CALLBACK = EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI,
#endif
//_Start_L05_HDD_SPIN_DOWN_
  EFI_L05_HDD_SPIN_DOWN_CALLBACK = 0xC3,
//_End_L05_HDD_SPIN_DOWN_
//_Start_L05_ALL_FEATURE_
  EFI_L05_TEST_SW_SMI  = 0xC4,
//_End_L05_ALL_FEATURE_
//_Start_L05_WMI_ASSET_TAG_SUPPORT_
  EFI_L05_WMI_ASSET_TAG_CALLBACK = EFI_L05_WMI_ASSET_TAG_SW_SMI,
//_END_L05_WMI_ASSET_TAG_SUPPORT_
}OEM_SW_SMI_PORT_TABLE;

#endif
