Insyde H2OFFT (Flash Firmware Tool) Package Release Notes

Package Version 3.00.00 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.20
2. Security Flash Package v2.01.00


Package Version 2.00.13 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.12
2. Security Flash Package v2.00.20

Package Version 2.00.12 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.12
2. Security Flash Package v2.00.19

Package Version 2.00.11 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.11
2. Security Flash Package v2.00.18

Package Version 2.00.10 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.10
2. Security Flash Package v2.00.16

Package Version 2.00.09 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.09
2. Security Flash Package v2.00.14

Package Version 2.00.08 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.08
2. Security Flash Package v2.00.13

Package Version 2.00.07 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.07
2. Security Flash Package v2.00.12

Package Version 2.00.06 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.06
2. Security Flash Package v2.00.10

Package Version 2.00.05 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.05
2. Security Flash Package v2.00.09

Package Version 2.00.04 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.04
2. Security Flash Package v2.00.07

Package Version 2.00.03 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.03
2. Security Flash Package v2.00.06

Package Version 2.00.02 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.02
2. Security Flash Package v2.00.04

Package Version 2.00.01 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.01
2. Security Flash Package v2.00.02

Package Version 2.00.00 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v2.00
2. Security Flash Package v2.00.00



Package Version 1.00.55 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.4c
2. Security Flash Package v1.00.63

Package Version 1.00.54 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.4b
2. Security Flash Package v1.00.61

Package Version 1.00.53 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.4a
2. Security Flash Package v1.00.59

Package Version 1.00.52 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3z
2. Security Flash Package v1.00.59

Package Version 1.00.51 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3y
2. Security Flash Package v1.00.58

Package Version 1.00.50 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3y
2. Security Flash Package v1.00.57

Package Version 1.00.49 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3x
2. Security Flash Package v1.00.56

Package Version 1.00.48 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3w
2. Security Flash Package v1.00.56

Package Version 1.00.47 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3v
2. Security Flash Package v1.00.55

Package Version 1.00.46 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3u
2. Security Flash Package v1.00.55

Package Version 1.00.45 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3t
2. Security Flash Package v1.00.55

Package Version 1.00.44 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3s
2. Security Flash Package v1.00.55

Package Version 1.00.43 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3r
2. Security Flash Package v1.00.51

Package Version 1.00.42 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3q
2. Security Flash Package v1.00.51

Package Version 1.00.41 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3p
2. Security Flash Package v1.00.50

Package Version 1.00.40 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3o
2. Security Flash Package v1.00.47

Package Version 1.00.39 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3n
2. Security Flash Package v1.00.46

Package Version 1.00.38 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3m
2. Security Flash Package v1.00.46

Package Version 1.00.37 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3l
2. Security Flash Package v1.00.45

Package Version 1.00.36 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3k
2. Security Flash Package v1.00.43

Package Version 1.00.35 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3j
2. Security Flash Package v1.00.42

Package Version 1.00.34 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3i
2. Security Flash Package v1.00.41

Package Version 1.00.33 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3h
2. Security Flash Package v1.00.40

Package Version 1.00.32 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3g
2. Security Flash Package v1.00.37

Package Version 1.00.31 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3f
2. Security Flash Package v1.00.37

Package Version 1.00.30 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3e
2. Security Flash Package v1.00.36

Package Version 1.00.29 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3d
2. Security Flash Package v1.00.34

Package Version 1.00.28 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3c
2. Security Flash Package v1.00.33

Package Version 1.00.27 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.3a
2. Security Flash Package v1.00.31

Package Version 1.00.26 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.2z
2. Security Flash Package v1.00.30

Package Version 1.00.25 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.2y
2. Security Flash Package v1.00.29

Package Version 1.00.24 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.2x
2. Security Flash Package v1.00.26

Package Version 1.00.23 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde H2OFFT Package v1.2w
2. Security Flash Package v1.00.25

Package Version 1.00.22 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2v
2. Security Flash Package v1.00.24

Package Version 1.00.21 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2u
2. Security Flash Package v1.00.23

Package Version 1.00.20 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2t
2. Security Flash Package v1.00.21

Package Version 1.00.19 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2s
2. Security Flash Package v1.00.19

Package Version 1.00.18 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2r
2. Security Flash Package v1.00.17

Package Version 1.00.17 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2q
2. Security Flash Package v1.00.17

Package Version 1.00.16 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2p
2. Security Flash Package v1.00.16

Package Version 1.00.15 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2o
2. Security Flash Package v1.00.16

Package Version 1.00.14 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2n
2. Security Flash Package v1.00.16

Package Version 1.00.13 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2m
2. Security Flash Package v1.00.15

Package Version 1.00.12 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2l
2. Security Flash Package v1.00.14

Package Version 1.00.11 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2k
2. Security Flash Package v1.00.13

Package Version 1.00.10 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2j
2. Security Flash Package v1.00.13

Package Version 1.00.09 (Kernel version required: 3.72.45)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2i
2. Security Flash Package v1.00.12

Package Version 1.00.08 (Kernel version required: 3.72.20)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2h
2. Security Flash Package v1.00.11.01

Package Version 1.00.07 (Kernel version required: 3.72.20)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2h
2. Security Flash Package v1.00.11

Package Version 1.00.06 (Kernel version required: 3.72.20)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2g
2. Security Flash Package v1.00.10

Package Version 1.00.05 (Kernel version required: 3.72.20)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2f
2. Security Flash Package v1.00.10

Package Version 1.00.04 (Kernel version required: 3.72.20)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2e
2. Security Flash Package v1.00.10

Package Version 1.00.03 (Kernel version required: 3.72.20)
------------
Include following packages:
1. Insyde Shell Flash Package v1.2d
2. Security Flash Package v1.00.10

Package Version 1.00.02
------------
Include following packages:
1. Insyde Shell Flash Package v1.2c
2. Security Flash Package v1.00.08

Package Version 1.00.01
------------
Include following packages:
1. Insyde Shell Flash Package v1.2b
2. Security Flash Package v1.00.06

Package Version 1.00.00
------------
Include following packages:
1. Insyde Shell Flash Package v1.2a
2. Security Flash Package v1.00.04
