@REM
@REM Project pre-build batch file
@REM
@REM ******************************************************************************
@REM * Copyright (c) 2017 - 2021, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************

GetProjectEnv CHIPSET_REL_PATH > NUL && for /f %%a in ('GetProjectEnv CHIPSET_REL_PATH') do @set %%a
GetProjectEnv PROJECT_REL_PATH > NUL && for /f %%a in ('GetProjectEnv PROJECT_REL_PATH') do @set %%a

@REM
@REM  Override Split.exe.
@REM  This override should be removed when updating Kernel to Tag# 05.43.25.
@REM
set CHASMFALLGEN2=NO
findstr /C:"gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport|2" %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\Project.dsc >nul
if not errorlevel 1 set CHASMFALLGEN2=YES
if %CHASMFALLGEN2% equ NO goto NoOverrideSplit
copy %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Override\BaseTools\Bin\Win32\Split.exe %WORKSPACE%\BaseTools\Bin\Win32 /y > NUL
:NoOverrideSplit
@REM //[-start-210603-BAIN000007-add]//
@REM //[-start-211108-JAYAN00010-modify]//

setlocal EnableDelayedExpansion
set FSP_PRE_BUILD=YES

REM Build FSP
REM ---------
  if "%FSP_PRE_BUILD%"=="YES" (
    echo ----------------------------------------------------------
    echo                       Build ADL FSP                       
    echo ----------------------------------------------------------

    if "%1" == "uefi64" (
      if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd call %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd /r
    )
    if "%1" == "efidebug" (
      if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd call %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd /d
    )
    if "%1" == "uefi64ddt" (
      if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd call %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd /ddt
    )
    if "%1" == "uefi64perf" (
      if exist %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd call %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\BuildFsp.cmd /perf
    )
    if not !ERRORLEVEL! == 0 goto end
    if exist %WORKSPACE%\Conf\.cache rd %WORKSPACE%\Conf\.cache /s /q
  )
@REM //[-end-211108-JAYAN00010-modify]//
@REM //[-end-210603-BAIN000007-add]//

@REM ------------ Fixup BaseTools\Bin\Win32\GenVarrcBat.bat file -------------#
@if exist %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat_.bat (
  copy %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32\GenVarrcBat.bat %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat.bat /y
) else (
  copy %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat.bat %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat_.bat /y
  copy %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32\GenVarrcBat.bat %WORKSPACE%\BaseTools\Bin\Win32\GenVarrcBat.bat /y
)

@REM Run kernel pre-build process
@if exist %WORKSPACE%\BaseTools\KernelPreBuild.bat call %WORKSPACE%\BaseTools\KernelPreBuild.bat %1
@if not errorlevel 0 goto end

@REM Run Chipset specific pre-build process
GetProjectEnv CHIPSET_PKG > NUL && for /f %%a in ('GetProjectEnv CHIPSET_PKG') do set %%a
if exist %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\ChipsetPreBuild.bat call %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\ChipsetPreBuild.bat %1
:end
