;************************
;*  DO NOT EDIT
;*  FILE auto-generated. Single FD compatible 
;****

[FD.ALDERLAKES] 
       Resize      Size Taken        Offset         Size        End                      PCD
   ------------    ----------      -----------    --------   --------         --------------------------
        fixed            58               0          43000      43000          gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableBase
        fixed            20           43000           2000      45000          gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingBase
        fixed             0           45000          45000      8A000          gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase
        fixed         20000           8A000          20000      AA000          gInsydeTokenSpaceGuid.PcdFlashNvStorageFactoryCopyBase
        fixed           18B           AA000           1000      AB000          gInsydeTokenSpaceGuid.PcdFlashNvStorageBvdtBase
        fixed             0           AB000           1000      AC000          gInsydeTokenSpaceGuid.PcdFlashNvStorageDmiBase
        fixed             0           AC000          10000      BC000          gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataBase
        fixed         2D681           BC000          38000      F4000          gInsydeTokenSpaceGuid.PcdFlashNvStorageVariableDefaultsBase
        fixed         19000           F4000         280000     374000          gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalOffset
        fixed          1000          374000          70000     3E4000          gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecurityOffset
        fixed        2AF000          3E4000         418000     7FC000          gInsydeTokenSpaceGuid.PcdFlashFvMainBase
        fixed          36E1          7FC000           4000     800000                                                  
        fixed             0          800000         400000     C00000          gChipsetPkgTokenSpaceGuid.PcdFwResiliencyReservedBase
        fixed         65000          C00000          80000     C80000          gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesFvBase
        fixed         5B000          C80000          80000     D00000          gInsydeTokenSpaceGuid.PcdFlashNvStorageMicrocodeBase
        fixed         24000          D00000          3D000     D3D000          gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery2Base
        fixed         6B000          D3D000          86000     DC3000          gInsydeTokenSpaceGuid.PcdFlashFvRecoveryBase
        fixed         6F000          DC3000          AF000     E72000          gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSBase
        fixed         B0000          E72000         150000     FC2000          gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMBase
        fixed         10000          FC2000          10000     FD2000          gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTBase
        fixed           76C          FD2000           1000     FD3000          gInsydeTokenSpaceGuid.PcdH2OFlashDeviceMapStart
        fixed         13000          FD3000          2D000    1000000          gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery0Base
