/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SMBIOS_DEFINE_TYPE_H_
#define _OEM_SMBIOS_DEFINE_TYPE_H_

#include <IndustryStandard/SmBios.h>
#include <Protocol/MebxProtocol.h>
#include <Protocol/SetupUtility.h>
#include <Protocol/AmtPolicy.h>
#include <MeBiosPayloadData.h>
#include <Library/BaseLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <IndustryStandard/Pci.h>

#include <ChipsetSetupConfig.h>
#include <ChipsetAccess.h>
//[-start-200906-IB16740115-modify]// CpuAccess.h has removed in RC1362.
//#include <CpuAccess.h>
#include "CpuDataStruct.h"
//[-end-200906-IB16740115-modify]//
#include <Pi/PiStatusCode.h>
#include <Library/DxeMeLib.h>

#define EFI_SMBIOS_OEM_DEFINE_TYPE_AMT_VPRO 131
#define ENABLE  1

#define  VMX_SECURE                      0x00000002   // Secure Environment bit 1 in the MSR
#define  VMX_NON_SECURE                  0x00000004   // Non-Secure Environment bit 2 in the MSR
#define  EFI_MSR_IA32_FEATURE_CONTROL    0x3A

#define TXT_OPT_IN_VMX_AND_SMX_MSR_VALUE    0xFF03

//
// AmtCpuCapability
// 6:31 bits Reserved and must be set to "0"
//

#define EFI_AMT_CPU_VMX_CMP_ENABLE                   BIT0
#define EFI_AMT_CPU_SMX_ENABLE                       BIT1
#define EFI_AMT_CPU_TXT_CAPABILITY                   BIT2
#define EFI_AMT_CPU_TXT_ENABLE                       BIT3
#define EFI_AMT_CPU_VMX_CAPABILITY                   BIT4
#define EFI_AMT_CPU_VMX_ENABLE                        BIT5
//
// AmtMeCapability[3]
// Byte 0-3 SKU/Capabilities
// 16:31 bits Reserved and must be set to "0"
//
#define EFI_AMT_ME_ENABLE                            BIT0   // ME Engine
#define EFI_AMT_ME_AMT_SUPPORT                       BIT3   // AMT
#define EFI_AMT_ME_STD_SUPPORT                       BIT4   // Standard Manageability
#define EFI_AMT_ME_SBA_SUPPORT                       BIT5   // Small Business Technology
#define EFI_AMT_ME_UPD_SUPPORT                       BIT6   // Intel Manageability Upgrade
#define EFI_AMT_ME_ATP_SUPPORT                       BIT13  // AT
#define EFI_AMT_ME_KVM_SUPPORT                       BIT14  // KVM
#define EFI_AMT_ME_LWT_SUPPORT                       BIT15  // Local Wakeup Timer

//
// AmtBIOSCapability
// 10:31 bits Reserved and must be set to 0
//

#define EFI_AMT_BIOS_SETUP_SCREEN_VTD_SUPPORT        BIT1
#define EFI_AMT_BIOS_SETUP_SCREEN_TXT_SUPPORT        BIT2
#define EFI_AMT_BIOS_SETUP_SCREEN_VTX_SUPPORT        BIT5
#define EFI_AMT_BIOS_SETUP_SCREEN_AT_PBA_SUPPORT     BIT6
#define EFI_AMT_BIOS_SETUP_SCREEN_AT_WWAN_SUPPORT    BIT7

//
// AmtVproSignature
//

#define EFI_OEM_AMT_VPRO_TABLE_SIGNATURE             0x6f725076 // 'vPro'

typedef struct {
    UINT32  VMXState : 1;       //[0]     VMX state (On/Off)
    UINT32  SMXState : 1;       //[1]     SMX state (On/Off)
    UINT32  LtTxtCap : 1;       //[2]     LT/TXT capability
    UINT32  LtTxtEnabled : 1;   //[3]     LT/TXT Enabled state (Optional for vPro verification)
    UINT32  VTxCap : 1;         //[4]     VT-x capability
    UINT32  VTxEnabled : 1;     //[5]     VT-x Enabled state (Optional for vPro verification)
    UINT32  Reserved : 26;      //[31:6]  Reserved, set to 0
} CPU_CAP;

typedef struct {
  SMBIOS_STRUCTURE      Hdr;                //0x00
  UINT32                AmtCpuCapability;   //0x04
  UINT16                AmtMebxVersion[4];  // 0x08
  UINT64                AmtPchCapability;   //0x10
  UINT32                AmtMeCapability[3]; //0x18
  UINT8                 ATConfig;           //0x24
  UINT8                 Reserved2;          //0x25
  UINT16                Reserved4;          //0x26
  UINT32                AmtNetworkDevice[3]; //0x28
  UINT32                AmtBIOSCapability;  //0x34
  UINT32                AmtVproSignature;   //0x38
  UINT32                Reserved3;          //0x3C
} EFI_OEM_AMT_VPRO_TABLE;

VOID
UpdateAmtCpuCapability (
  UINT32* CpuCapability
  );

VOID
UpdateAmtPchCapability (
  UINT64* PchCapability
  );

VOID
UpdateAmtWireDevice (
  UINT32* AmtWireDevice
  );

VOID
UpdateAmtWirelessDevice (
  UINT32* AmtWirelessDevice
  );

EFI_STATUS
UpdateAmtMeVersionNumber (
  UINT32* AmtMeVersion,
  UINT32* AmtMeNumber
  );
#endif

