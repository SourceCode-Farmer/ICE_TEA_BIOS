@REM
@REM This bat file is used to generate EDK2 standard capsule image for Intel Chasm Falls
@REM
@REM ******************************************************************************
@REM * Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************

@echo off

@REM ====================== ME setting ===============================
@REM
@REM ME_VERSION = 0xMMHHBBBB
@REM ex: Intel CSME 16.0.0.1435 -> 0x0000059B
@REM
set ME_VERSION=0x0000059B
@REM
@REM ESRT GUID:
@REM Adl_LP_Cons = 23192307-d667-4bdf-af1a-6059db171246
@REM Adl_LP_Corp = 4e78ce68-5389-4a95-bf10-e3568c30caf8
@REM Adl_H_Cons  = 7aa69739-8f78-41cb-bf44-854e2cb516bd
@REM Adl_H_Corp  = 347efe23-9f9a-4b26-b4db-e2414872dd14
@REM   
set ME_ESRT_GUID=7aa69739-8f78-41cb-bf44-854e2cb516bd

set ME_LSV=0x00000001
@REM ===================== Micdoeocde setting ========================

set SLOT_SIZE=0x3B000
set FW_VERSION=0x0002
set LSV=0x0001
set FW_VERSION_STRING="Version 0.0.0.2"
@REM
@REM UC0DE_MODE = (ucodefull | ucodeslot)
@REM If bios guard enable, the UC0DE_MODE only support ucodefull mode.
@REM
set UCODE_MODE=ucodefull

@REM =================================================================
@REM
@REM Environment setting
@REM
@set PROJECT_FOLDER=%WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%
@for /f "tokens=3" %%a in ('find "TARGET " %WORKSPACE%\Conf\target.txt') do set TARGET=%%a
@if exist %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\Ffs (
@pushd %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\Ffs
@dir /a:d /b > temp.log
@for /f "tokens=1" %%a in ('findstr "CAPSULEPAYLOADBIOS" temp.log') do rd /s /q %%a 
@for /f "tokens=1" %%a in ('findstr "CAPSULEPAYLOADBTGACM" temp.log') do rd /s /q %%a
@del /q temp.log 

@if exist CAPSULEPAYLOADBIOS.inf del /q CAPSULEPAYLOADBIOS.inf
@if exist FvAddress.inf del /q FvAddress.inf
@if exist CAPSULEPAYLOADBTGACM.inf del /q CAPSULEPAYLOADBTGACM.inf
@popd
)
@if not exist %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN% mkdir %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%

set PLATFORMSAMPLE_PACKAGE=AlderLakePlatSamplePkg

set BUILD_BIOS_CAPSULE=FALSE
set BUILD_GEN1_CAPSULE=FALSE
set BUILD_GEN2_CAPSULE=FALSE
set BUILD_ME_CAPSULE=FALSE
set BUILD_BTGACM_CAPSULE=FALSE
set BIOS_GUARD_NEED=FALSE

@REM ============================= Main ==============================

@if /I "gen1" == "%1" (
  set BUILD_GEN1_CAPSULE=TRUE
  set BUILD_BIOS_CAPSULE=TRUE
  goto GenCapsule
)else if /I "gen2" == "%1" (
  set BUILD_GEN2_CAPSULE=TRUE
  set BUILD_BIOS_CAPSULE=TRUE
  goto GenCapsule
)else if /I "me" == "%1" (
  set BUILD_ME_CAPSULE=TRUE
  goto Setpath
)else if /I "ucode" == "%1" (
  goto GenUcode
)else if /I "btgacm" == "%1" (
  set BUILD_BTGACM_CAPSULE=TRUE
  goto GenBtGAcm
)else if /I "btgacmbg" == "%1" (
  set BUILD_BTGACM_CAPSULE=TRUE
  set BIOS_GUARD_NEED=TRUE
  set ERROR_OCCUR=FALSE
  goto GenBtGAcm
)else (
  goto CapsuleHelp
)

:GenBtGAcm
@REM
@REM Make BtGAcm binary 4K aligned for SPI flash update.
@REM 
  set FIRMWARE_BINARY=%PROJECT_FOLDER%\CapsuleUpdate\Binary
  call %WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\NewGenCap\PyScript\GenAligned.exe -a 4K -i %FIRMWARE_BINARY%\BIOSAC.bin -o %FIRMWARE_BINARY%\BtGAcm_pad.bin
  
  if %BIOS_GUARD_NEED% EQU FALSE goto BtgAcmSkipBiosgaurd
  set BIOSGUARD_SETTING_PATH=%PROJECT_FOLDER%\PlatformConfig\BiosGuard
  set CHIPSET_PKG_TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\AlderLakeChipsetPkg\Tools\Bin\Win32

  @REM
  @REM check Signtool exist.
  @REM
  set SIGNER_CHIPSET_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32\Signer
  set SIGNER_PATH=DEVTLS\SignTool
  set SIGNER_PRESET_PATH=C:\%SIGNER_PATH%;D:\%SIGNER_PATH%;E:\%SIGNER_PATH%;F:\%SIGNER_PATH%;G:\%SIGNER_PATH%;H:\%SIGNER_PATH%;%SIGNER_CHIPSET_PATH%
  set PATH=%SIGNER_PRESET_PATH%;%PATH%
  signtool /? 2>nul
  if errorlevel 1 goto SignTool_NotFound

  @REM
  @REM Get information (BiosAcm Base Address and Padding file size)
  @REM
  for /f "tokens=3" %%a in ('find "BaseAddress" BtGAcmUpdateConfig.ini') do set BtgAcmBaseAddress=%%a
  for /f "usebackq" %%A in ('%FIRMWARE_BINARY%\BtGAcm_pad.bin') do set BtgAcmLenght=%%~zA
  cd %FIRMWARE_BINARY%
  @REM
  @REM Gen BiosGuard Header and Certificate 
  @REM
  %CHIPSET_PKG_TOOLS_PATH%\GenBiosGuardHdr.exe -i %BIOSGUARD_SETTING_PATH%\BiosGuardSetting.ini -p %BIOSGUARD_SETTING_PATH%\BGSL\InsydeBiosGuardScript.bin -f %FIRMWARE_BINARY%\BtGAcm_pad.bin -m 5 -d %BtgAcmBaseAddress% -l %BtgAcmLenght% -o BtGAcm_BG_HDR.bin
  Copy /y /b BtGAcm_BG_HDR.bin+BtGAcm_pad.bin BtgAcm_BG_Sign.bin
  %CHIPSET_PKG_TOOLS_PATH%\Signer\iEFIFlashSigner.exe engineer -signbiosguard %FIRMWARE_BINARY%\BtgAcm_BG_Sign.bin -n "QA Certificate."
  Copy /y /b %CHIPSET_PKG_TOOLS_PATH%\Signer\*_Sign.bin.p7.cer *_Sign.cer
  if errorlevel 1 goto BGSignError
  del %CHIPSET_PKG_TOOLS_PATH%\Signer\*_Sign.bin*
  copy /y /b BtGAcm_BG_HDR.bin + BtgAcm_BG_Sign.cer BiosGuard_BtgAcm.bin
  del BtGAcm_BG_*.*
  cd ..
:BtgAcmSkipBiosgaurd


:GenCapsule
@echo. 
@echo "#### Generate Chasm Falls capsule fv ####"
@echo. 
@if %BUILD_GEN1_CAPSULE% EQU TRUE (
  call genfds.exe ^
       -f %PROJECT_PKG%\CapsuleUpdate\CapsulePkg.fdf ^
       -p %PROJECT_PKG%\CapsuleUpdate\CapsulePkg.dsc ^
       -i CapsulePayloadBios
)else if %BUILD_GEN2_CAPSULE% EQU TRUE (
  call genfds.exe ^
       -f %PROJECT_PKG%\CapsuleUpdate\CapsulePkgRes.fdf ^
       -p %PROJECT_PKG%\CapsuleUpdate\CapsulePkgRes.dsc ^
       -i CapsulePayloadBios
)else if %BUILD_BTGACM_CAPSULE% EQU TRUE (
  @REM
  @REM Update BtGAcmUpdateConfig.ini
  @REM Prepare BtGAcm Payload.
  @REM
  if %BIOS_GUARD_NEED% EQU TRUE (
       call %WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\GenBiosUpdateImages\CapsuleRecoveryGen\SplitFv.exe btgaacmbgconfig -p %FIRMWARE_BINARY%\BtGAcm_pad.bin -b %FIRMWARE_BINARY%\BiosGuard_BtgAcm.bin -i BtGAcmUpdateConfig.ini
       call genfds.exe ^
            -f %PROJECT_PKG%\CapsuleUpdate\BtGAcm_BiosGuardCapsulePkg.fdf ^
            -p %PROJECT_PKG%\CapsuleUpdate\BtGAcmCapsulePkg.dsc ^
            -i CapsulePayloadBtGAcm
       del %FIRMWARE_BINARY%\BiosGuard_BtgAcm.bin
  )else (
       call %WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\GenBiosUpdateImages\CapsuleRecoveryGen\SplitFv.exe btgaacmconfig -p %FIRMWARE_BINARY%\BtGAcm_pad.bin -i BtGAcmUpdateConfig.ini
       call genfds.exe ^
            -f %PROJECT_PKG%\CapsuleUpdate\BtGAcmCapsulePkg.fdf ^
            -p %PROJECT_PKG%\CapsuleUpdate\BtGAcmCapsulePkg.dsc ^
            -i CapsulePayloadBtGAcm
    )
)else (
  @echo "Wrong cmd"
  goto CapsuleBuildDone
)

:Setpath
@REM 
@REM Set path
@REM 
set TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\GenerateCapsule
set FIRMWARE_BINARY=%PROJECT_FOLDER%\CapsuleUpdate\Binary

@if %BUILD_BIOS_CAPSULE% EQU TRUE (
copy /y %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\CAPSULEPAYLOADBIOS.Fv %TOOLS_PATH%\CapsulePayloadBios.fv > NUL
)

@if %BUILD_ME_CAPSULE% EQU TRUE (
set FIRMWARE_BINARY=%PROJECT_FOLDER%\CapsuleUpdate\Binary\FWUpdate.bin
)

@if %BUILD_BTGACM_CAPSULE% EQU TRUE (
copy /y %WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%\FV\CAPSULEPAYLOADBTGACM.Fv %TOOLS_PATH%\CapsulePayloadBtGAcm.fv > NUL
)

pushd %TOOLS_PATH%

@echo. 
@echo "#### Sign capsule image ####"
@echo. 
@REM @if not exist %SIGN_TOOL% (
@REM  @echo.
@REM  @echo "*****ERROR: No OpenSSL in %PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\GenerateCapsule\OpenSSL. Fail to sign capsule image.*****"
@REM goto EndGenCapsule
@REM )

@REM 
@REM  Sign Bios capsule 
@REM 
@if %BUILD_BIOS_CAPSULE% EQU TRUE (

call GenCapsuleSysFwBios.bat CAPSULEPAYLOADBIOS.Fv %TOOLS_PATH%
@if %BUILD_GEN1_CAPSULE% EQU TRUE (
copy /y SystemFwBios.Cap %PROJECT_FOLDER%\CapsuleUpdate\OutPut\SystemFwBiosGen1.Cap > NUL
)
@if %BUILD_GEN2_CAPSULE% EQU TRUE (
copy /y SystemFwBios.Cap %PROJECT_FOLDER%\CapsuleUpdate\OutPut\SystemFwBiosGen2.Cap > NUL
)
if exist CapsulePayloadBios.fv del CapsulePayloadBios.fv
if exist SystemFwBios.Cap del SystemFwBios.Cap

@REM 
@REM  Sign ME capsule 
@REM 
)else if %BUILD_ME_CAPSULE% EQU TRUE (

@if not exist %FIRMWARE_BINARY% (
@echo.
@echo "*****ERROR: No FWUpdate.bin in %PROJECT_PKG%\CapsuleUpdate\Binary. Fail to sign ME capsule image.*****"
goto EndGenCapsule
)

call GenCapsuleSysFwMe.bat %FIRMWARE_BINARY% %ME_VERSION% %ME_ESRT_GUID% %ME_LSV% %TOOLS_PATH%
copy /y SystemMeFw.Cap %PROJECT_FOLDER%\CapsuleUpdate\OutPut > NUL
if exist SystemMeFw.Cap del SystemMeFw.Cap

@REM
@REM  Sign BtG ACM capsule
@REM 
)else if %BUILD_BTGACM_CAPSULE% EQU TRUE (

call GenCapsuleSysFwBtGAcm.bat CapsulePayloadBtGAcm.Fv %TOOLS_PATH%
copy /y SystemFwBtGAcm.Cap %PROJECT_FOLDER%\CapsuleUpdate\OutPut > NUL

if exist CapsulePayloadBtGAcm.fv del CapsulePayloadBtGAcm.fv
if exist SystemFwBtGAcm.Cap del SystemFwBtGAcm.Cap

@REM 
@REM  Error
@REM 
)else (
  @echo "Wrong cmd"
  goto CapsuleBuildDone
)


:EndGenCapsule
popd
goto CapsuleBuildDone


:GenUcode
set TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\GenerateCapsule
set MICROCODE_TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORMSAMPLE_PACKAGE%\Features\CapsuleUpdate\Tools\NewGenCap
set MICROCODE_PATH=%PROJECT_PKG%\CapsuleUpdate\Binary
set WORKSPACE_PLATFORM=%WORKSPACE%\%PROJECT_REL_PATH%
set PROJECT_BOARD_PACKAGE=%PROJECT_REL_PATH%\%PROJECT_PKG%
pushd %MICROCODE_TOOLS_PATH%
call NewGenCap.exe
popd

@REM 
@REM  UcodeFullModeFv.bin is Insyde ucode FV image with full mode.
@REM  UcodeSlotModeFv.bin is Insyde ucode FV image with slot mode.
@REM 
cd Temp 
if %UCODE_MODE% == ucodefull (
  copy /y XdrMicrocodeFv.data %PROJECT_FOLDER%\CapsuleUpdate\OutPut\UcodeFullModeFv.bin > NUL
) else if %UCODE_MODE% == ucodeslot (
  copy /y XdrSlotVersionXdrSlotuCodeArray.data %PROJECT_FOLDER%\CapsuleUpdate\OutPut\UcodeSlotModeFv.bin > NUL
)
@REM 
@REM  BiosGuardUcodeFv.bin is Insyde ucode FV image with bios guard enable.
@REM  Put this binary to %PROJECT_FOLDER%\PlatformConfig\BiosGuard, 
@REM  and run BiosGuardImage.bat to generate ucode FV image with bios guard header.
@REM 
cd FV
if %UCODE_MODE% == ucodefull (
  copy /y CAPSULEPAYLOADUCODE.Fv %PROJECT_FOLDER%\CapsuleUpdate\OutPut\BiosGuardUcodeFv.bin > NUL
) 
cd ..
cd ..
if exist Temp rd /s /q Temp
pushd Binary
if exist *.mcb_backup del *.mcb /q 
if exist *.mcb_backup ren *.mcb_backup *.mcb
if exist MicrocodeVersion.data del MicrocodeVersion.data /q 
popd
goto CapsuleBuildDone

:BGSignError
del %PROJECT_FOLDER%\CapsuleUpdate\Binary\*_BG_*.bin
echo.
echo !!! ERROR !!!
echo Create Capsule with Bios Guard process has been terminated 
echo Please check iEFIFlashSigner.exe command is correct in BuildCapsule.bat
echo !!! ERROR !!!
echo.
pushd %PROJECT_FOLDER%\CapsuleUpdate
goto CapsuleBuildDone

:SignTool_NotFound
start /WAIT %PROJECT_FOLDER%\CautionSiger.bat
echo.
echo "!!! ERROR !!! Create capsule image process has been terminated due to SignTool is missing!"
echo.
exit /b


:CapsuleHelp
@echo.
@echo. Generate Edk2 capsule image.
@echo  Run ProjectBuild.bat to initialize environment.
@echo  Cmd: BuildCapsule.bat [target:gen1/gen2/me/ucode/btgacm/btgacmbg]
@echo        target   Assign Capsule build target: gen1/gen2/me/ucode/btgacm/btgacmbg
@echo          gen1     : Build Intel GEN1 BIOS Capsule
@echo          gen2     : Build Intel GEN2 BIOS Capsule
@echo          me       : Build Intel ME Capsule
@echo          ucode    : Build Intel Ucode Capsule and Insyde Ucode payload
@echo          btgacm   : Build Intel BtG ACM Capsule
@echo          btgacmbg : Build Intel BtG ACM Capsule with Bios Guard
@echo.

:CapsuleBuildDone