/** @file
  Provide OEM to install the PCI Option ROM table and Non-PCI Option ROM table.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Register/PchRegs.h>
#include <Register/SataRegs.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
//#include <SaRegs.h>
#include <Library/DxeOemSvcKernelLib.h>

//@ OPROM defines
//
// Global variables for PCI Option Roms
//
#define NULL_ROM_FILE_GUID \
  { \
    0x00000000, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 \
  }

#ifdef CPU_CFL
#define ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID \
  { \
    0x8dfae5d4, 0xb50e, 0x4c10, 0x96, 0xe6, 0xf2, 0xc2, 0x66, 0xca, 0xcb, 0xb6 \
  }
#define ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID \
  { \
    0xeccceaae, 0xe65e, 0x41e9, 0xbd, 0x65, 0xcf, 0x00, 0xfb, 0x42, 0x46, 0x41 \
  }
#define ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID \
  { \
    0xbfef6019, 0x0e7f, 0x48cb, 0x93, 0xd6, 0x1d, 0x3e, 0x12, 0xb4, 0x52, 0x2a \
  }
#endif

#define IDE_RAID_OPTION_ROM_FILE_GUID \
  { \
    0x501737ab, 0x9d1a, 0x4856, 0x86, 0xd3, 0x7f, 0x12, 0x87, 0xfa, 0x5a, 0x55 \
  }

#define IDE_AHCI_OPTION_ROM_FILE_GUID \
  { \
    0xB017C09D, 0xEDC1, 0x4940, 0xB1, 0x3E, 0x57, 0xE9, 0x56, 0x60, 0xC9, 0x0F \
  }

#define PXE_UNDI_OPTION_ROM_FILE_GUID \
  { \
    0x4c316c9a, 0xafd9, 0x4e33, 0xae, 0xab, 0x26, 0xc4, 0xa4, 0xac, 0xc0, 0xf7 \
  }

#define PXE_UNDI_ULT_OPTION_ROM_FILE_GUID \
  { \
    0x72322192, 0x5DEE, 0x4F9C, 0xAD, 0xF3, 0x19, 0x0B, 0xD6, 0xAD, 0xC1, 0x25 \
  }

//
// Global variables for Non-PCI Option Roms
//
#define SYSTEM_ROM_FILE_GUID \
  { \
    0x1547B4F3, 0x3E8A, 0x4FEF, 0x81, 0xC8, 0x32, 0x8E, 0xD6, 0x47, 0xAB, 0x1A \
  }

#define NULL_ROM_FILE_GUID \
  { \
    0x00000000, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 \
  }

#define PXE_BASE_OPTION_ROM_FILE_GUID \
  { \
    0x6f2bc426, 0x8d83, 0x4f17, 0xbe, 0x2c, 0x6b, 0x1f, 0xcb, 0xd1, 0x4c, 0x80 \
  }

#define BIS_OPTION_ROM_FILE_GUID \
  { \
    0xAE21640A, 0x21D2, 0x4ec4, 0xA9, 0x96, 0x86, 0xC2, 0x6C, 0x9E, 0xD0, 0x91 \
  }

#define BIS_LOADER_FILE_GUID \
  { \
    0xf14aa356, 0x9774, 0x481c, 0xb5, 0xf1, 0x79, 0xc1, 0xb5, 0xea, 0xe8, 0xa1 \
  }

#define OEM_I15VARS_FILE_GUID \
  { \
    0x340AD445, 0x06EF, 0x43f1, 0x89, 0x97, 0xFC, 0xD6, 0x7F, 0xC2, 0x7E, 0xEF \
  }

#define OEM_INT15CB_FILE_GUID \
  { \
    0x340AD445, 0x06EF, 0x43f1, 0x89, 0x97, 0xFC, 0xD6, 0x7F, 0xC2, 0x7E, 0xF0 \
  }

#define TPMMP_GUID \
  { \
    0x4B8D2F76, 0xD9E7, 0x46dc, 0xB5, 0xED, 0xEF, 0xEF, 0x94, 0x76, 0xAF, 0x4A \
  }

//
// module variables
//

//
// Pci Option Rom Table
//
//@ PCI OPROM table
PCI_OPTION_ROM_TABLE      PciOptionRomTable[] = {
#ifdef CPU_CFL
  //
  // Video Option ROM for SKL IGD
  //
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_SULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT15F_SULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_SULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2F_SULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT3_SULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_SHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_SHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT3_SHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT4_SHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_SULXM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT15_SULXM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_SULXM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_SSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_SSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT3_SSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT4_SSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_SDT_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_SDT_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT15_SDT_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT4_SDT_ID
  },
  //
  // Video Option ROM for KBL IGD
  //
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_KULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT15F_KULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_KULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2F_KULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT3_KULTM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_KHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_KHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT3_KHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP11_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT4_KHALM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_KULXM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT15_KULXM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP3_RVP7_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_KULXM_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_KSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_KSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT3_KSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT4_KSR_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT1_KDT_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT2_KDT_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT15_KDT_ID
  },
  {
    ONBOARD_VIDEO_OPTION_ROM_RVP8_FILE_GUID,
    V_SA_IGD_VID,
    V_SA_PCI_DEV_2_GT4_KDT_ID
  },
#endif
//  //
//  // RAID Option Rom
//  //
//  {
//    IDE_RAID_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_CNL_PCH_LP_SATA_CFG_DEVICE_ID_M_RAID
//  },
//  {
//    IDE_RAID_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_PCH_H_SATA_CFG_DEVICE_ID_D_RAID_ALTDIS
//  },
//  {
//    IDE_RAID_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_CNL_PCH_LP_SATA_CFG_DEVICE_ID_M_RAID_PREM
//  },
////  {
////    IDE_RAID_OPTION_ROM_FILE_GUID,
////    V_SATA_CFG_VENDOR_ID,
////    V_CNL_PCH_LP_SATA_CFG_DEVICE_ID_M_RAID_RRT
////  },
//  {
//    IDE_RAID_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_CNL_PCH_H_SATA_CFG_DEVICE_ID_D_RAID
//  },
//   {
//    IDE_RAID_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_PCH_H_SATA_CFG_DEVICE_ID_D_RAID_ALTDIS
//  },
//   {
//    IDE_RAID_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_CNL_PCH_H_SATA_CFG_DEVICE_ID_D_RAID_PREM
//  },
////   {
////    IDE_RAID_OPTION_ROM_FILE_GUID,
////    V_SATA_CFG_VENDOR_ID,
////    V_CNL_PCH_H_SATA_CFG_DEVICE_ID_D_RAID_RRT
////  },
//  //
//  // AHCI Option Rom
//  //
//  {
//    IDE_AHCI_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_CNL_PCH_H_SATA_CFG_DEVICE_ID_D_AHCI
//  },
////  {
////    IDE_AHCI_OPTION_ROM_FILE_GUID,
////    V_SATA_CFG_VENDOR_ID,
////    V_CNL_PCH_H_SATA_CFG_DEVICE_ID_D_AHCI_A0
////  },
//  {
//    IDE_AHCI_OPTION_ROM_FILE_GUID,
//    V_SATA_CFG_VENDOR_ID,
//    V_CNL_PCH_LP_SATA_CFG_DEVICE_ID_M_AHCI
//  },
//  //
//  // PXE UNDI Option Rom
//  //
//  {
//    PXE_UNDI_OPTION_ROM_FILE_GUID,
//    V_PCH_LAN_VENDOR_ID,
//    V_PCH_H_LAN_DEVICE_ID
//  }, // Ibex Peak Internal LAN
//  //
//  // PXE UNDI Option Rom for LPT-LP
//  //
//  {
//    PXE_UNDI_ULT_OPTION_ROM_FILE_GUID,
//    V_PCH_LAN_VENDOR_ID,
//    V_PCH_LP_LAN_DEVICE_ID
//  }, // Ibex Peak Internal LAN
  {
    NULL_ROM_FILE_GUID,
    0xffff,
    0xffff
  }
};

//
// Non Pci Option Rom Table
//

//
// System Rom table
//
//@ Non-PCI OPROM table
SYSTEM_ROM_TABLE    SystemRomTable[] = {
  {
    //
    // CSM16 binary
    //
    SYSTEM_ROM_FILE_GUID,
    TRUE,
    SYSTEM_ROM
  },
  {
    NULL_ROM_FILE_GUID,
    FALSE,
    MAX_NUM
  }
};


//
// Oem Int table
//
SYSTEM_ROM_TABLE    SystemOemIntTable[] = {
  {
    NULL_ROM_FILE_GUID,
    FALSE,
    MAX_NUM
  }
};


//
//  Service ROM Table
//
SERVICE_ROM_TABLE  ServiceRomTable[] = {
  {
    NULL_ROM_FILE_GUID,
    FALSE,
    FALSE,
    MAX_NUM
  }
};

//
// TPM Rom table
//
SYSTEM_ROM_TABLE    TpmTable[] = {
  {
    //
    // TPMMP binary
    //
    TPMMP_GUID,
    TRUE,
    TPM_ROM
  },
  {
    NULL_ROM_FILE_GUID,
    FALSE,
    MAX_NUM
  }
};

/**
  Provide OEM to install the PCI Option ROM table and Non-PCI Option ROM table.
  The detail refers to the document "OptionRomTable Restructure User Guide".

  @param[in]   *RomType              The type of option rom. This parameter decides which kind of option ROM table will be access.
  @param[out]  **RomTable            A pointer to the option ROM table.

  @retval      EFI_SUCCESS           Get Option ROM Table info failed.
  @retval      EFI_MEDIA_CHANGED     Get Option ROM Table info success.
  @retval      Others                Depends on customization.
**/
EFI_STATUS
OemSvcInstallOptionRomTable (
  IN  UINT8                                 RomType,
  OUT VOID                                  **mOptionRomTable
  )
{
  VOID                                  *OptionRomTable;
  UINTN                                 OptionRomTableSize;

  switch (RomType) {
  case PCI_OPROM:
    OptionRomTable      = (void *)PciOptionRomTable;
    OptionRomTableSize  = sizeof(PciOptionRomTable);
    break;

  case SYSTEM_ROM:
    OptionRomTable      = (void *)SystemRomTable;
    OptionRomTableSize  = sizeof(SystemRomTable);
    break;

  case SYSTEM_OEM_INT_ROM:
    OptionRomTable      = (void *)SystemOemIntTable;
    OptionRomTableSize  = sizeof(SystemOemIntTable);
    break;

  case SERVICE_ROM:
    OptionRomTable      = (void *)ServiceRomTable;
    OptionRomTableSize  = sizeof(ServiceRomTable);
    break;

  case TPM_ROM:
    OptionRomTable      = (void *)TpmTable;
    OptionRomTableSize  = sizeof(TpmTable);
    break;

  default:
    return EFI_SUCCESS;
  }

  (*mOptionRomTable)   = AllocateZeroPool (OptionRomTableSize);
  CopyMem ((*mOptionRomTable), OptionRomTable, OptionRomTableSize);

  return EFI_MEDIA_CHANGED;
}

