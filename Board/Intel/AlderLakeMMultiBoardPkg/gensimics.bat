@REM ******************************************************************************
@REM * Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************

@REM 
@REM Note: this file is used by Chipset to generate simics relative binaries for Intel Simics.
@REM 

@set CURR_DIR=%CD%
@set BIOSDIR=%WORKSPACE%\Board\Intel\AlderLakePMultiBoardPkg\BIOS
@set IFWIPY_DIR=%WORKSPACE%\Intel\AlderLake\AlderLakePlatSamplePkg\Tools\RomImage\IfwiPatcher
@set IFWIBIN_DIR=%WORKSPACE%\Intel\AlderLake\AlderLakePlatSamplePkg\Tools\RomImage\PreSiIfwi
@rem Below code copy from Intel SetupRomDirs.bat
@rem @call %PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\RomImage\IfwiPatcher\IfwiPatcher.py --ifwi %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\RomImage\PreSiIfwi\ADLP_IFWI.bin --bios %BUILD_DIR%\FV\ClientBios.fd  --output %WORKSPACE%\RomImages\AlderLakeSimics\%BIOS_PREFIX%_FSPWRAPPER_%BIOS_MAJOR_VERSION%_%BIOS_MIN_VERSION%_%BUILD_TYPE%_Simics.bin

@rem ADL-P simics
@call %IFWIPY_DIR%\IfwiPatcher.exe --ifwi %IFWIBIN_DIR%\ADLP_IFWI.bin --bios %BIOSDIR%\AlderLakeM.fd  --output %BIOSDIR%\simics.bin