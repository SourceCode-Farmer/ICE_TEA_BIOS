@REM
@REM FSP build batch file
@REM
@REM ******************************************************************************
@REM * Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************
@REM Copyright (c) 2013 - 2016, Intel Corporation. All rights reserved.
@REM This software and associated documentation (if any) is furnished
@REM under a license and may only be used or copied in accordance
@REM with the terms of the license. Except as permitted by such
@REM license, no part of this software or documentation may be
@REM reproduced, stored in a retrieval system, or transmitted in any
@REM form or by any means without the express written consent of
@REM Intel Corporation.

@echo off

if "%~1" == "/?" goto USAGE
if "%~1" == "/h" goto USAGE
@set NOTIMESTAMP=1

  setlocal enableextensions enabledelayedexpansion
  set BUILD_ARGS=%*
  set WORKSPACE_CORE=%WORKSPACE%\Edk2
  set WORKSPACE_EDK_PLATFORM=%WORKSPACE%\Intel\Edk2Platforms
  set TOOL_CHAIN_TAG=%TOOL_CHAIN%
  set PROJECT_PKG_BAK=%PROJECT_PKG%
  rem Perform pre-build
  rem -----------------

  rem Remove shifted arguments by reconstructing BUILD_ARGS
  rem -----------------------------------------------------
  set BUILD_ARGS=AlderLakeP
  :CONSTRUCT_BUILD_ARGS_LOOP
    if "%~1"=="" goto CONSTRUCT_BUILD_ARGS_CONTINUE
    if /i "notimestamp" == "%1" (
      set NOTIMESTAMP=1
    ) else (
      set BUILD_ARGS=%BUILD_ARGS% %1
    )
    shift
    goto CONSTRUCT_BUILD_ARGS_LOOP
  :CONSTRUCT_BUILD_ARGS_CONTINUE

  call :PREBUILD
  if not %ERRORLEVEL% == 0 goto DIE

  rem Build package
  rem -------------
  rem Find build script.
  set BUILD_TARGET=%CHIPSET_REL_PATH%\AlderLakeFspPkg\BuildFv.cmd
  if not exist %WORKSPACE%\%BUILD_TARGET% (
    echo ERROR: Unable to find '%BUILD_TARGET%'.
    goto DIE
    )
  @if exist %WORKSPACE%\Conf\.cache @rd %WORKSPACE%\Conf\.cache /s /q
  @if exist %WORKSPACE%\Build\AlderLakeFspPkg\*.* @rd %WORKSPACE%\Build\AlderLakeFspPkg\ /s /q
  set PROJECT_PKG=DUMMY
@if "%WORKSPACE:~-1%"==":" ( pushd %WORKSPACE%\ ) ELSE ( pushd %WORKSPACE% )
  call %WORKSPACE%\%BUILD_TARGET% %BUILD_ARGS%
  popd
  if not %ERRORLEVEL% == 0 goto DIE

  rem Perform post-build
  rem ------------------
  call :POSTBUILD %BUILD_ARGS%
  if not %ERRORLEVEL% == 0 goto DIE

  rem Done
  rem ----
  goto END

  rem Store target for later
  echo Setting default target to '%TARGET%'.
  echo %TARGET%>%DEFAULT_TARGET_FILE%
  set TARGET=

  goto END

:POSTBUILD
rem ---------------------------------------------------------------------------
rem Perform post-build actions
rem ---------------------------------------------------------------------------
  rem echo Performing post-build actions.
  rem
  if /I "%1"=="/clean" goto END

  echo - PostBuild Done -
  echo =============================================================
  echo == Build FSP Done                                          ==
  echo ==                                                         ==
  echo ==                                                         ==
  echo ==                                                         ==
  echo ==                                                         ==
  echo ==                                                         ==
  echo ==                                                         ==
  echo =============================================================
  goto END

@REM
@REM Backward Compatibility End
@REM

:PREBUILD
@if not defined WORKSPACE (
  call %~dp0\EdkSetup.bat
)
set WORKSPACE_PLATFORM=%WORKSPACE%
set WORKSPACE_SILICON=%WORKSPACE%\%CHIPSET_REL_PATH%
@REM set PACKAGES_PATH=%WORKSPACE_CORE%;%WORKSPACE_PLATFORM%;%WORKSPACE_SILICON%

@REM
@REM Check for Python support
@REM
set PYTHON_MIN_VERSION=(2,5)
call :FIND_PYTHON
if not %ERRORLEVEL% == 0 goto DIE

@REM
@REM Setup Visual Studio environment. Order of presedence is 2015, 2013, 2012, 2010 and then 2008.
@REM
@REM NOTE: To override presedence set TOOL_CHAIN_TAG before calling.
@REM       Example: set TOOL_CHAIN_TAG=VS2008
@REM

@REM Check if tool chain has not been selected and Visual Studio 2015 is installed.
@if not defined TOOL_CHAIN_TAG (
  if defined VS140COMNTOOLS (
    set TOOL_CHAIN_TAG=VS2015
  )
)

@REM If Visual Studio 2015 is selected by priority or by preference, setup the environment variables.
@if /I "%TOOL_CHAIN_TAG%"=="VS2015" (
  echo.
  echo Prebuild:  Set the VS2015 environment.
  echo.
  if not defined VSINSTALLDIR call "%VS140COMNTOOLS%\vsvars32.bat"
  if /I "%VS140COMNTOOLS%" == "C:\Program Files\Microsoft Visual Studio 14.0\Common7\Tools\" (
    set TOOL_CHAIN_TAG=VS2015
  ) else (
    set TOOL_CHAIN_TAG=VS2015x86
  )
)

@REM Check if tool chain has not been selected and Visual Studio 2013 is installed.
@if not defined TOOL_CHAIN_TAG (
  if defined VS120COMNTOOLS (
    set TOOL_CHAIN_TAG=VS2013
  )
)

@REM If Visual Studio 2013 is selected by priority or by preference, setup the environment variables.
@if /I "%TOOL_CHAIN_TAG%"=="VS2013" (
  echo.
  echo Prebuild:  Set the VS2013 environment.
  echo.
  if not defined VSINSTALLDIR call "%VS120COMNTOOLS%\vsvars32.bat"
  if /I "%VS120COMNTOOLS%" == "C:\Program Files\Microsoft Visual Studio 12.0\Common7\Tools\" (
    set TOOL_CHAIN_TAG=VS2013
  ) else (
    set TOOL_CHAIN_TAG=VS2013x86
  )
)

@REM Check if tool chain has not been selected and Visual Studio 2012 is installed.
@if not defined TOOL_CHAIN_TAG (
  if defined VS110COMNTOOLS (
    set TOOL_CHAIN_TAG=VS2012
  )
)

@REM If Visual Studio 2012 is selected by priority or by preference, setup the environment variables.
@if /I "%TOOL_CHAIN_TAG%"=="VS2012" (
  echo.
  echo Prebuild:  Set the VS2012 environment.
  echo.
  if not defined VSINSTALLDIR call "%VS110COMNTOOLS%\vsvars32.bat"
  if /I "%VS110COMNTOOLS%" == "C:\Program Files\Microsoft Visual Studio 11.0\Common7\Tools\" (
    set TOOL_CHAIN_TAG=VS2012
  ) else (
    set TOOL_CHAIN_TAG=VS2012x86
  )
)

@REM Check if tool chain has not been selected and Visual Studio 2010 is installed.
@if not defined TOOL_CHAIN_TAG (
  if defined VS100COMNTOOLS (
    set TOOL_CHAIN_TAG=VS2010
  )
)

@REM If Visual Studio 2010 is selected by priority or by preference, setup the environment variables.
@if /I "%TOOL_CHAIN_TAG%"=="VS2010" (
  echo.
  echo Prebuild:  Set the VS2010 environment.
  echo.
  if not defined VSINSTALLDIR call "%VS100COMNTOOLS%\vsvars32.bat"
  if /I "%VS100COMNTOOLS%" == "C:\Program Files\Microsoft Visual Studio 10.0\Common7\Tools\" (
    set TOOL_CHAIN_TAG=VS2010
  ) else (
    set TOOL_CHAIN_TAG=VS2010x86
  )
)

@REM Check if tool chain has not been selected and Visual Studio 2008 is installed.
@if not defined TOOL_CHAIN_TAG (
  if defined VS90COMNTOOLS (
    set TOOL_CHAIN_TAG=VS2008
  )
)

@REM If Visual Studio 2008 is selected by priority or by preference, setup the environment variables.
@if /I "%TOOL_CHAIN_TAG%"=="VS2008" (
  echo.
  echo Prebuild:  Set the VS2008 environment.
  echo.
  if not defined VSINSTALLDIR call "%VS90COMNTOOLS%\vsvars32.bat"
  if /I "%VS90COMNTOOLS%" == "C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\" (
    set TOOL_CHAIN_TAG=VS2008
  ) else (
    set TOOL_CHAIN_TAG=VS2008x86
  )
)

@REM If no supported version of Visual Studio was detected, return an error.
@if not defined TOOL_CHAIN_TAG (
  echo.
  echo !!! ERROR !!! Visual Studio not installed correctly!!!
  echo.
  exit /b 1
)

  goto END

:FIND_PYTHON
rem ---------------------------------------------------------------------------
rem Looks for Python installed on the system.
rem ---------------------------------------------------------------------------
  set PYTHON_DEF_EXE=python.exe
  set PYTHON_EXE=
  set PYTHON_DIR=
  set PYTHON_FOUND_IN_PATH=0

  rem Check the standard installation location for Python 2.7
  if exist C:\Python27\%PYTHON_DEF_EXE% (
    set PYTHON_EXE=C:\Python27\%PYTHON_DEF_EXE%
    )
  if defined PYTHON_EXE goto FIND_PYTHON_FOUND

  rem Check the standard installation location for Python 2.6
  if exist C:\Python26\%PYTHON_DEF_EXE% (
    set PYTHON_EXE=C:\Python26\%PYTHON_DEF_EXE%
    )
  if defined PYTHON_EXE goto FIND_PYTHON_FOUND

  rem Check the standard installation location for Python 2.5
  if exist C:\Python25\%PYTHON_DEF_EXE% (
    set PYTHON_EXE=C:\Python25\%PYTHON_DEF_EXE%
    )
  if defined PYTHON_EXE goto FIND_PYTHON_FOUND

  rem Check the PATH environment variable
  for %%X in (%PYTHON_DEF_EXE%) do (set PYTHON_EXE=%%~$PATH:X)
  if defined PYTHON_EXE (
    set PYTHON_FOUND_IN_PATH=1
    goto FIND_PYTHON_FOUND
    )

  rem Python not found
  echo Error: %PYTHON_DEF_EXE% could not be located on this system. If^
  Python has been installed to a nonstandard location (the typical installation^
  location is C:\Python25\%PYTHON_DEF_EXE%), ensure that the directory^
  containing the Python executable has been added to the PATH environment^
  variable.
   call :FIND_PYTHON_CLEANUP
  goto DIE

  :FIND_PYTHON_FOUND
  echo Python was found at '%PYTHON_EXE%'.
  call :PYTHON_VERSION_TEST "%PYTHON_EXE%" "%PYTHON_MIN_VERSION%"
  if not %ERRORLEVEL% == 0 (
  echo Error: The version of Python installed on this system at^
  '%PYTHON_EXE%' is too old to run necessary scripts. Please upgrade^
  your Python installation to version !PYTHON_MIN_VERSION! or newer.
    call :FIND_PYTHON_CLEANUP
    goto DIE
    )

  rem Add Python to the PATH if necessary
  if not %PYTHON_FOUND_IN_PATH% == 0 goto FIND_PYTHON_CLEANUP
  for %%X in (%PYTHON_EXE%) do set PYTHON_DIR=%%~pdX
  set PATH=%PATH%;%PYTHON_DIR%

  :FIND_PYTHON_CLEANUP
  set PYTHON_DEF_EXE=
  set PYTHON_DEF_DIR=
  set PYTHON_EXE=
  set PYTHON_DIR=
  set PYTHON_FOUND_IN_PATH=

  goto END

:PYTHON_VERSION_TEST
rem ---------------------------------------------------------------------------
rem Test the Python executable %1 for version >= %2.
rem ---------------------------------------------------------------------------
  %1 -c "import sys; sys.exit(sys.version_info<%~2)"
  if not %ERRORLEVEL% == 0 goto DIE
  goto END

:USAGE
rem ---------------------------------------------------------------------------
rem Print usage and exit
rem ---------------------------------------------------------------------------
  echo Usage:
  echo BuildFsp [/h ^| /? ^| /r ^| /d ^| /ddt ^| /clean | notimestamp]
  echo.
  goto END

:DIE
rem ---------------------------------------------------------------------------
rem Exit returning a failure error code
rem ---------------------------------------------------------------------------
  set PROJECT_PKG=%PROJECT_PKG_BAK%
  exit /B 1

:END
rem ---------------------------------------------------------------------------
rem Exit returning a success error code
rem ---------------------------------------------------------------------------
  set PROJECT_PKG=%PROJECT_PKG_BAK%
  exit /B 0
