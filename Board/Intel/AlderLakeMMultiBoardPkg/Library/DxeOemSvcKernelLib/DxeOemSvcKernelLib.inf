## @file
#  Component description file for DxeOemSvcKernelLib instance.
#
#******************************************************************************
#* Copyright (c) 2012 - 2021, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = DxeOemSvcKernelLib
  FILE_GUID                      = 9AC5355F-90C2-4f08-B458-82791C55AE27
  MODULE_TYPE                    = UEFI_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = DxeOemSvcKernelLib|DXE_CORE DXE_DRIVER DXE_RUNTIME_DRIVER DXE_SAL_DRIVER DXE_SMM_DRIVER UEFI_APPLICATION UEFI_DRIVER SMM_CORE

[Sources]
  OemSvcCsm16ReferSwitch.c
  OemSvcGetHotplugBridgeInfo.c
  OemSvcInstallLegacyBiosOemSlp.c
  OemSvcInstallOptionRomTable.c
  OemSvcInstallPciSkipTable.c
  OemSvcInstallPostKeyTable.c
  OemSvcLogoResolution.c
  OemSvcUpdateFormLen.c
  OemSvcPrepareInstallMpTable.c
  OemSvcVariableForReclaimFailTable.c
  OemSvcDxeUpdateSmbiosRecord.c
  OemSvcUpdateAcpiFacsHardwareSignature.c
  OemSvcUpdateSsidSvidInfo.c
  OemSvcAdjustSetupMenu.c
  OemSvcChangeVbiosBootDisplay.c

[Packages]
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(PROJECT_PKG)/Project.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec

[LibraryClasses]
  MemoryAllocationLib
  UefiRuntimeServicesTableLib
  HobLib
  BaseMemoryLib
  UefiBootServicesTableLib
  PcdLib
  BaseLib
  DebugLib
  CommonPciLib
  PciExpressLib
  DxeMeLib
  S3BootScriptLib
  HobLib
  #
  # NOTE!!!:Please not use HiiExLib, HiiLib in DxeOemSvcKernelLib,
  # It will cause this library include UefiHiiServicesLib. UefiHiiServicesLib depends on
  # gEfiHiiStringProtocolGuid, gEfiHiiDatabaseProtocolGuid and gEfiHiiConfigRoutingProtocolGuid
  # and will make all of driver include DxeOemSvcKernelLib library need wait for these HII related
  # protocol are installed.
  #
  UefiLib
  VariableLib
  MeTypeLib
  PchInfoLib
  PchPcieRpLib
  CpuPcieRpLib
  H2OCpLib
  DevicePathLib

[Protocols]
  gEfiSetupUtilityProtocolGuid
  gEfiGraphicsOutputProtocolGuid
  gEfiOEMBadgingSupportProtocolGuid
  gEfiBmpDecoderProtocolGuid
  gEfiJpegDecoderProtocolGuid
  gEfiTgaDecoderProtocolGuid
  gEfiGifDecoderProtocolGuid
  gEfiPcxDecoderProtocolGuid
  gEfiHiiDatabaseProtocolGuid
  gMebxProtocolGuid
  gEfiChangeVbiosBootDisplayProtocolGuid
  gITbtPolicyProtocolGuid
  gDxeDTbtPolicyProtocolGuid
#[-start-180420-IB11270199-add]#
  gAmtWrapperProtocolGuid
#[-end-180420-IB11270199-add]#
  gTrustedDeviceSetupMainProtocolGuid

[Guids]
  gEfiGenericVariableGuid                      ## CONSUMES  ## Variable:L"VBIOS"
  gMeBiosExtensionSetupGuid
#  gSetupVariableHobGuid
  gMeBiosPayloadHobGuid
  gPchSetupVariableGuid
  gSystemConfigurationGuid
  gSetupVariableGuid
  gTbtInfoHobGuid
  gDxeITbtConfigGuid
  gDTbtInfoHobGuid
  gSaSetupVariableGuid
  gH2ODxeCpPciHpcGetResourcePaddingGuid

[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdFrontPageSupported
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag

[PCD]
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareReleaseDateString
  gInsydeTokenSpaceGuid.PcdFlashAreaSize
  gInsydeTokenSpaceGuid.PcdH2ORotateScreenSupported
  gInsydeTokenSpaceGuid.PcdH2ORotateScreenRotateLogo
  gSiPkgTokenSpaceGuid.PcdITbtEnable
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gBoardModuleTokenSpaceGuid.PcdEcMajorRevision
  gBoardModuleTokenSpaceGuid.PcdEcMinorRevision
  gInsydeTokenSpaceGuid.PcdH2ODxeCpPciHpcGetResourcePaddingSupported

[FixedPcd]
  gSiPkgTokenSpaceGuid.PcdAmtEnable

