;;******************************************************************************
;;* Copyright (c) 2019 - 2021, Insyde Software Corporation. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   BiosGuardSetting.ini
;;
;; Abstract:
;;
;;   The information file for Bios Guard
;;
[Platform]
  ;;
  ;; The BIOS Guard module will compare the PLAT_ID found in the BGPDT with the
  ;; PLAT_ID found in the BIOS Guard update package header to ensure a given update
  ;; package is appropriate for the platform. This prevents cross flashing of a platform
  ;; image to the wrong platform when a single platform signing key is used.
  ;;
  PlatId = AlderLakeP

[Image]
  ;;
  ;;  FileName : Image file name, (ex.TigerLake.fd, BiosGaurdUcodeFv.bin)
  ;;  BiosGuardScriptFile : BIOS Guard Script Language File(Use Intel BIOSGuardSL2Bin.exe, transfer *.bgsl to *.bin)
  ;;
  FileName = AlderLakeP.fd
  BiosGuardScriptFile = BGSL\InsydeBiosGuardScript.bin
  
  ;;
  ;;  PackageMode:
  ;;     0x1 : Package BIOS Image or Full Image.
  ;;     0x2 : Package EC (Non-Shared flash rom)
  ;;     0x3 : Package Firmware (ex: microcode)
  ;;
  PackageMode = 0x1

  ;;
  ;;  PackageMode= 0x2, Package EC (Non-Shared Rom)
  ;;
  ;;  EcSize : EC Image file size.
  ;;  EcOffset : SPI rom address of EC firmware.
  ;;
  EcSize = 0x80000
  EcOffset = 0x1000

  ;;  
  ;;  PackageMode= 0x3, Package Firmware (ex: microcode full mode only)
  ;;
  ;;  FwSize : Fw size.
  ;;  FwOffset : Fw firmware address (base on BIOS region) 
  ;;             (Chasmfalls gen2 support microcode update)
  ;;
  FwSize = 0x00080000
  FwOffset = 0x0C80000

[ChasmFalls]
  ;;
  ;; The data in this section will auto be updated in Postbuild Process
  ;;
  ;; ChasmFallsType : Chasm Fall is Disabled , or type is Gen1 or Gen2. (PcdChasmFallsSupport)
  ;;                  (Disable = 0, Gen1 = 0x1, Gen2 = 0x2. ChaseFalls didn't support Full Image, please set type as 0x0.)
  ;; PbbOffset      : PBB BIOS Offset.  (PcdFlashPbbBase  - PcdBiosAreaBaseAddress)
  ;; PbbSize        : PBB/PBBr size.    (PcdFlashPbbSize)
  ;;
  ;; Only For Gen2
  ;; SbbReservedOffset : The Offset had been reserved included in SBB.  (Ex.Variable Region, Refernce PcdFlashSbbReservedBase)
  ;; SbbReservedSize   : The size had been reserved includedin SBB.     (PcdFlashSbbReservedSize)
  ;; VarOffset         : The Offset of NV Storage                       (For SBB rollback to skip NV region)
  ;; VarSize           : The Size of NV Storage                         (NvVariableStore + NvFtwWorking + NvFtwSpare + NvFactoryCopy)
  ;;
  ChasmFallsType = 0x0
  PbbOffset = 0x0
  PbbSize = 0x0
  SbbReservedOffset = 0x0
  SbbReservedSize = 0x0
  VarOffset = 0x0
  VarSize = 0x0

[ExtendedBIOS]
  ExtendedBiosSupport = 0x0
  ExtendedBiosSize = 0x02000000

[BiosGuardHeader]

  ;;
  ;; [UPDPKG_ATTR]
  ;; Any bit set in this field indicates the update package that must be signed and also the update package must not be processed,
  ;; unless it is accompanied by a valid certificate.
  ;;
  ;; BIT0 - SFAM_CHECK              default : 1
  ;;                                      0 : No update of SFAM flash, no need to evaluate BIOS_SVN.
  ;;                                      1 : Host flash update of flash covered by the SFAM and the BIOS_SVN policy must be evaluated and applied.
  ;;
  ;; BIT1 - PROT_EC_OPR             default : 0
  ;;                                      0 : No protected EC operations included in script.
  ;;                                      1 : Protected EC operations included in script.
  ;;        If PROT_EC_OPR is set, the EC SVN policy (obtained via ECCMD_GetSecurityVersionNumber) must be evaluated and applied. Additionally, 
  ;;        the BIOS Guard module will issue EC_OPEN command to EC prior in beginning script execution; 
  ;;        similarly, it will issue EC_CLOSE command to EC after script execution terminates.
  ;;
  ;; BIT2 - GFX_MITIGATION_DISABLE  default : 0
  ;;                                      0 : All Intel(R) GFX related checks and security enhancements will be enforced.
  ;;                                      1 : No Intel(R) GFX related checks or security enhancements will be enforced.
  ;;
  ;; BIT3 - FAULT_TOLERANT_UPDATE   default : 0
  ;;                                      0 : Indicates the script will not manipulate TOP_SWAP or bypass Flash Protected Range Registers.
  ;;                                      1 : The script requires ability to manipulate TOP_SWAP, or bypass Flash Protected Range Registers. 
  ;;                                          If FTU is set, the BIOS_SVN policy must be evaluated and applied.
  ;;        NOTE: In order to utilize the Fault Tolerant Update feature, PCH Softstrap for BIOS Guard Protections override enable bit (offset 0x178h) must be set.
  ;;        Review the SPI Bring Up Guide as part of ME kit for more information.                                    
  SFAM_CHECK = 1
  PROT_EC_OPR = 0
  GFX_MITIGATION_DISABLE = 0
  FAULT_TOLERANT_UPDATE = 0
  
  ;;
  ;; If UPDPKG_ATTR[0] == 1, SFAM_CHECK, indicates the Security Version Number of the BIOS
  ;; update contained within this update package.The BIOS Guard module must verify
  ;; BGUP.Header.BIOS_SVN >=  BGPDT.BIOS__SVN. if UPDPKG_ATTR[0] == 0, BIOS_SVN checking is not done
  ;; and this field is ignored by the BIOS Guard module.
  ;; CRB default use Platform.ini ESRT value.
  ;;
  ;; Please follow Project Security Version Numbe controller design to update value in Project.dsc
  ;; BiosGuardSvn.exe will help to automatically update BiosGaurdSetting.ini in build process.  
  ;; Microcode fv is a part of bios, so svn is same as bios.
  ;;
  BiosSvn = 0x71024035

  ;;
  ;; If UPDPKG_ATTR[1] == 1, PROT_EC_OPR, indicates the Security Version Number of the EC FW
  ;; update if there is one contained within this update package. If the update
  ;; package contains protected EC operations, but does not contain an EC
  ;; firmware update, the BGUP.Header.EC_SVN >= EC_SVN retrieved from the EC via
  ;; ECCMD_GetSecurityVersionNumber. If UPDPKG_ATTR[1] == 0, EC_SVN checking is
  ;; not done and this files is ignored by the BIOS Guard module.
  ;;
  ;; For Non-shared EC flash component.
  ;; (1) Field as EC Security Version Number 
  ;; (2) Update BIOS or Full Image: Field as 0xFFFFFFFF
  ;;
  EcSvn = 0xFFFFFFFF

  ;;
  ;; 4 bytes available to script writer. This field is not referenced by the BIOS Guard
  ;; module. It is, however, included in digital signature of signed scripts. It is expected
  ;; that it will be used for vendor specific versioning or script identification.
  ;;
  VendorSpecific = 0x00000000

cific = 0x00000000

