/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  PEI Multi-Board Initialization in Pre-Memory PEI Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/EcMiscLib.h>
#include <PlatformBoardType.h>
#include <PlatformBoardId.h>
#include <Library/PeiDxeBoardIdsLib.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/MultiBoardSupportLib.h>
#include <Ppi/MultiBoardDetected.h>

EFI_PEI_PPI_DESCRIPTOR mCustomerMultiBoardDetectPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiCustomerMultiBoardDetectedPpiGuid,
  NULL
};


VOID
EFIAPI
UpdateRvpBoardConfig (
  IN OUT UINT16         BoardId
  )
{
  UINTN         Size;
  UINT8         BoardType;
  UINT8         PlatformType;
  UINT8         PlatformFlavor;
  EFI_STATUS    Status;
  
  if (BoardId < mSizeOfmBoardIndex) {
    Size = StrSize (mBoardIdIndex[BoardId]);
    Status = PcdSetPtrS (PcdBoardName, &Size, mBoardIdIndex[BoardId]);
    ASSERT_EFI_ERROR (Status);
  }

  //
  // Update Board Type/Platform Type/Platform Flavor
  //
  switch (BoardId) {
    case BoardIdAdlPSimics:
      BoardType = BoardTypeRvp;
      PlatformType = TypeUltUlx;
      PlatformFlavor = FlavorMobile;
      if(PcdSet64S(PcdAcpiDefaultOemTableId, ACPI_OEM_TABLE_ID_TGL_ULT) != EFI_SUCCESS)
      {
         DEBUG ((DEBUG_INFO, "Set PcdAcpiDefaultOemTableId error!!!\n"));
      }
      break;
    case BoardIdAdlSTgpHDdr4SODimm1DCrb:
      BoardType = BoardTypeRvp;
      PlatformType = TypeTrad;
      PlatformFlavor = FlavorMobile;
      if(PcdSet64S(PcdAcpiDefaultOemTableId, ACPI_OEM_TABLE_ID_TGL_ULT) != EFI_SUCCESS)
      {
         DEBUG ((DEBUG_INFO, "Set PcdAcpiDefaultOemTableId error!!!\n"));
      }
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    case BoardIdAdlSAdpSDdr5UDimm2DCrb:
    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
    case BoardIdAdlSAdpSDdr5UDimm1DSr:
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      BoardType = BoardTypeRvp;
      PlatformType = TypeTrad;
      PlatformFlavor = FlavorDesktop;
      if(PcdSet64S (PcdAcpiDefaultOemTableId, ACPI_OEM_TABLE_ID_ADL) != EFI_SUCCESS)
      {
         DEBUG ((DEBUG_INFO, "Set PcdAcpiDefaultOemTableId error!!!\n"));
      }
      break;
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPDdr5MRRvp:
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5MbAep:
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
    case BoardIdAdlMLp5Aep:
    case BoardIdAdlPMMAep:
    case BoardIdAdlPLp5Gcs:
      BoardType = BoardTypeRvp;
      PlatformType = TypeUltUlx;
      PlatformFlavor = FlavorMobile;
      if(PcdSet64S (PcdAcpiDefaultOemTableId, ACPI_OEM_TABLE_ID_ADL_P_M) != EFI_SUCCESS)
      {
         DEBUG ((DEBUG_INFO, "Set PcdAcpiDefaultOemTableId error!!!\n"));
      }
      break;

    default:
      BoardType = BoardTypeRvp;
      PlatformType = TypeUnknown;
      PlatformFlavor = FlavorUnknown;
      break;
  }

  Status = PcdSet8S (PcdBoardType, BoardType);
  ASSERT_EFI_ERROR (Status);
  
  Status = PcdSet8S (PcdPlatformType, PlatformType);
  ASSERT_EFI_ERROR (Status);
  
  Status = PcdSet8S (PcdPlatformFlavor, PlatformFlavor);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "PcdAcpiDefaultOemTableId is 0x%llX\n", PcdGet64 (PcdAcpiDefaultOemTableId)));
}

/**
  Update settings based on PcdBoardId provided by project.

**/
VOID
OemProjectReferenceIntelCrb (
  VOID
  )
{
  EFI_STATUS                             Status;
  UINT16                                 BoardId;
  EFI_STATUS                             CommandStatus;
  BOOLEAN                                EcPresent;
  UINT8                                  EcDock;

  EcDock = FALSE;
  EcPresent = FALSE;

  BoardId = PcdGet16 (PcdBoardId);
  DEBUG ((DEBUG_INFO, "[OemProjectReferenceIntelCrb] BoardId is 0x%x\n", BoardId));

  //
  // update RVP board config
  // The following PCDs will be set:
  // PcdBoardName/Board Type/Platform Type/Platform Flavor/PcdAcpiDefaultOemTableId
  //
  UpdateRvpBoardConfig (BoardId);

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcDetectEcPresent \n"));
  Status = OemSvcEcDetectEcPresent (&CommandStatus, &EcPresent);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcDetectEcPresent Status: %r\n", Status));
  Status = PcdSetBoolS (PcdEcPresent, EcPresent);
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_ERROR, "++Detect EcPresent by OemSvc = %a\n", EcPresent ? "TRUE" : "FALSE"));


  if ((EcPresent == TRUE) && (PcdGet8 (PcdPlatformFlavor) == FlavorMobile)) {
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcGetPcieDockStatus \n"));
    Status = OemSvcEcGetPcieDockStatus (&CommandStatus, &EcDock);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcGetPcieDockStatus Status: %r\n", Status));
    if ((CommandStatus == EFI_SUCCESS) && (EcDock & EC_B_DOCK_STATUS_ATTACH)) {
      EcDock = TRUE;
    }
    DEBUG ((DEBUG_ERROR, "++Detect EcDock by OemSvc = %a\n", EcDock ? "TRUE" : "FALSE"));
  }
  Status = PcdSetBoolS (PcdDockAttached, EcDock);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "Platform Information:\nPlatformType: 0x%x\nBoardName: %s\n", PcdGet8 (PcdPlatformType), PcdGetPtr (PcdBoardName)));
  DEBUG ((DEBUG_INFO, "PlatformFlavor: 0x%x\nBoardID: 0x%x\n", PcdGet8 (PcdPlatformFlavor), BoardId));
  DEBUG ((DEBUG_INFO, "BoardRev: 0x%x\nBoardBomId: 0x%x\nBoardType: 0x%x\n", PcdGet16 (PcdBoardRev), PcdGet16 (PcdBoardBomId), PcdGet8 (PcdBoardType)));
  DEBUG ((DEBUG_INFO, "PlatformGeneration: 0x%x\nEcPresent: %d\nSpdPresent: %d\n", PcdGet8 (PcdPlatformGeneration), PcdGetBool (PcdEcPresent), PcdGetBool (PcdSpdPresent)));

}

EFI_STATUS
EFIAPI
CustomerMultiBoardDetect (
  VOID
  )
{
  UINT16		  SkuId;
  UINT16		  BoardId;
  UINT16          BomId;
  UINT16          BoardRev;

  EFI_STATUS      Status;

  SkuId 	 = 0;
  BoardId	 = 0;
  BomId      = 0;
  BoardRev   = 0;

  DEBUG ((DEBUG_INFO, " In CustomerMultiBoardDetect \n"));

  DEBUG ((DEBUG_INFO, "[CustomerMultiBoardDetect] PcdCrbBoard: %x \n", FeaturePcdGet(PcdCrbBoard)));

  //
  // CRB should not run the following code, just return EFI_SUCCESS
  //
  if (!FeaturePcdGet(PcdCrbBoard)) {
	SkuId =  PcdGet16 (PcdCrbSkuId);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] before set PcdCrbSkuId, SKU_ID: 0x%x\n", PcdGet16 (PcdCrbSkuId)));
	//
	// ToDo: project can add code here to update PcdCrbSkuId dynamically
	//
	Status = PcdSet16S (PcdCrbSkuId, SkuId);
	ASSERT_EFI_ERROR (Status);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] after set PcdCrbSkuId, SKU_ID: 0x%x\n", PcdGet16 (PcdCrbSkuId)));

	BoardId = PcdGet16 (PcdBoardId);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] before set PcdBoardId, PcdBoardId: 0x%x\n", PcdGet16 (PcdBoardId)));
	//
	// ToDo: project can add code here to update PcdBoardId dynamically
	//
	Status = PcdSet16S (PcdBoardId, BoardId);
	ASSERT_EFI_ERROR (Status);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] after set PcdBoardId, PcdBoardId: 0x%x\n", PcdGet16 (PcdBoardId)));

	BomId = PcdGet16 (PcdBoardBomId);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] before set PcdBoardBomId, PcdBoardBomId: 0x%x\n", PcdGet16 (PcdBoardBomId)));
	//
	// ToDo: project can add code here to update PcdBoardBomId dynamically
	//
	Status = PcdSet16S (PcdBoardBomId, BomId);
	ASSERT_EFI_ERROR (Status);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] after set PcdBoardBomId, PcdBoardBomId: 0x%x\n", PcdGet16 (PcdBoardBomId)));


	BoardRev = PcdGet16 (PcdBoardRev);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] before set PcdBoardRev, PcdBoardRev: 0x%x\n", PcdGet16 (PcdBoardRev)));
	//
	// ToDo: project can add code here to update PcdBoardRev dynamically
	//
	Status = PcdSet16S (PcdBoardRev, BoardRev);
	ASSERT_EFI_ERROR (Status);
	DEBUG ((DEBUG_INFO, "[CustomerBoardDetect] after set PcdBoardRev, PcdBoardRev: 0x%x\n", PcdGet16 (PcdBoardRev)));

    OemProjectReferenceIntelCrb ();
  }

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
PeiMultiBoardSupportLibConstructor (
  VOID
  )
{
  EFI_STATUS              Status;
  VOID                    *PeiInstance;

  //
  // Check if customer MultiBoard already detected.
  //
  Status = PeiServicesLocatePpi (
             &gPeiCustomerMultiBoardDetectedPpiGuid,
             0,
             NULL,
             &PeiInstance
             );

  if (Status == EFI_NOT_FOUND) {
	  Status = PeiServicesInstallPpi (
				       &mCustomerMultiBoardDetectPpi
			         );
		DEBUG ((DEBUG_INFO, "Start to Detect CustomerMultiBoard\n"));

		Status = CustomerMultiBoardDetect();
  } else if (Status == EFI_SUCCESS){
		DEBUG ((DEBUG_INFO, "CustomerMultiBoard is detected\n"));
	} else {
		DEBUG ((DEBUG_INFO, "CustomerMultiBoard should not go to here!\n"));
	}

  return Status;
}
