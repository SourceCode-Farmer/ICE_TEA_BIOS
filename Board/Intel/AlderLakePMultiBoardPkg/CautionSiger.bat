@echo off
mode con cols=70 lines=15
color 0C
title  Caution! SignTool is missing!
@echo.
@echo  Caution! SignTool is missing!
@echo.
@echo    SignTool DOES NOT exist in the current source code.
@echo    Please follow instructions in
@echo    ChipsetPkg\Tools\Bin\Win32\Signer\Readme.txt 
@echo    or 
@echo    InsydeH2O Power On Technical Reference Guide
@echo    to get Signtool for avoiding compiler errors.
@echo.
@echo.
@echo.
pause
exit 1
