/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BOOT_GUARD_TABLE_DEFINITION_
#define _BOOT_GUARD_TABLE_DEFINITION_

#define BASE_FV_SIZE 8

#define FSP_WRAPPER_FV_SIZE 3

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  #define TSN_MAC_ADDRESS_FV_SIZE 1
#else
  #define TSN_MAC_ADDRESS_FV_SIZE 0
#endif

#define BIOS_INFO_STRUCT_SIZE (BASE_FV_SIZE + FSP_WRAPPER_FV_SIZE + TSN_MAC_ADDRESS_FV_SIZE)

#pragma pack (1)
typedef struct {
  BIOS_INFO_HEADER  Header;
  BIOS_INFO_STRUCT  Entry[BIOS_INFO_STRUCT_SIZE];
} BIOS_INFO;
#pragma pack ()

//
// According to BIOS_INFO entry count in mBiosInfo, you need to modify FIT_ENTRY_COUNT to sync it
// Otherwise FitGen.exe occur error because parse wrong data
//
GLOBAL_REMOVE_IF_UNREFERENCED BIOS_INFO mBiosInfo = {
  {
    BIOS_INFO_SIGNATURE,
    BIOS_INFO_STRUCT_SIZE,
    0,
  },
  {
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      0x00,
      0x0100,
      FixedPcdGet32 (PcdFlashFvRecovery0Size),
      FixedPcdGet32 (PcdFlashFvRecovery0Base)
    },
//[-start-190612-IB16990049-add]//
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      0x00,
      0x0100,
      FixedPcdGet32 (PcdH2OFdmSize),
      FixedPcdGet32 (PcdH2OFdmOffset)
    },
//[-end-190612-IB16990049-add]//
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      0x00,
      0x0100,
      FixedPcdGet32 (PcdFlashFvFspTSize),
      FixedPcdGet32 (PcdFlashFvFspTBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      0x00,
      0x0100,
      FixedPcdGet32 (PcdFlashFvFspMSize),
      FixedPcdGet32 (PcdFlashFvFspMBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      BIOS_INFO_STRUCT_ATTRIBUTE_BIOS_POST_IBB,
      0x0100,
      FixedPcdGet32 (PcdFlashFvFspSSize),
      FixedPcdGet32 (PcdFlashFvFspSBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      0x00,
      0x0100,
      FixedPcdGet32 (PcdFlashFvRecoverySize),
      FixedPcdGet32 (PcdFlashFvRecoveryBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      BIOS_INFO_STRUCT_ATTRIBUTE_BIOS_POST_IBB,
      0x0100,
      FixedPcdGet32 (PcdFlashFvRecovery2Size),
      FixedPcdGet32 (PcdFlashFvRecovery2Base)
    },
    {
      FIT_TYPE_01_MICROCODE,
      BIOS_INFO_STRUCT_ATTRIBUTE_MICROCODE_WHOLE_REGION,
      0x0100,
      FixedPcdGet32 (PcdFlashFvMicrocodeSize),
      FixedPcdGet32 (PcdFlashFvMicrocodeBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      BIOS_INFO_STRUCT_ATTRIBUTE_GENERAL_EXCLUDE_FROM_FIT,
      0x0100,
      FixedPcdGet32 (PcdFlashFirmwareBinariesFvSize),
      FixedPcdGet32 (PcdFlashFirmwareBinariesFvBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      BIOS_INFO_STRUCT_ATTRIBUTE_GENERAL_EXCLUDE_FROM_FIT,
      0x0100,
      FixedPcdGet32 (PcdFlashNvStorageVariableSize) + FixedPcdGet32 (PcdFlashNvStorageFtwWorkingSize) + FixedPcdGet32 (PcdFlashNvStorageFtwSpareSize),
      FixedPcdGet32 (PcdFlashNvStorageVariableBase)
    },
    {
      FIT_TYPE_07_BIOS_STARTUP_MODULE,
      BIOS_INFO_STRUCT_ATTRIBUTE_BIOS_POST_IBB,
      0x0100,
      FixedPcdGet32 (PcdFlashFvMainSize),
      FixedPcdGet32 (PcdFlashFvMainBase)
    }
  }
};

#endif
